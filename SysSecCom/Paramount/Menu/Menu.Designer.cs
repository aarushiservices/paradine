﻿//using Paramount.Menu;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace Paramount.Menu
{
    partial class Menu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        //private void InitializeComponent()
        //{
        //    this.button1 = new System.Windows.Forms.Button();
        //    this.SuspendLayout();
        //    // 
        //    // button1
        //    // 
        //    this.button1.Location = new System.Drawing.Point(109, 132);
        //    this.button1.Name = "button1";
        //    this.button1.Size = new System.Drawing.Size(75, 23);
        //    this.button1.TabIndex = 0;
        //    this.button1.Text = "button1";
        //    this.button1.UseVisualStyleBackColor = true;
        //    // 
        //    // Menu
        //    // 
        //    this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        //    this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        //    this.Controls.Add(this.button1);
        //    this.Name = "Menu";
        //    this.Size = new System.Drawing.Size(319, 268);
        //    this.ResumeLayout(false);

        //}

        protected internal Button btnBack;
        private Button btnCancel;
        protected internal Button btnNext;
        private Button button1;
        private Button button2;
        private Button button3;
        private Button button4;
        //private Container components = null;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Panel pnlButtons;
        private MenuPage vActivePage = null;
        private MenuCollection vPages;

        private void InitializeComponent()
        {

            this.btnCancel = new Button();
            this.btnNext = new Button();
            this.btnBack = new Button();
            this.pnlButtons = new Panel();
            this.label4 = new Label();
            this.button2 = new Button();
            this.button4 = new Button();
            this.label3 = new Label();
            this.button3 = new Button();
            this.label2 = new Label();
            this.label1 = new Label();
            this.button1 = new Button();
            this.pnlButtons.SuspendLayout();
            base.SuspendLayout();
            this.btnCancel.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            this.btnCancel.FlatStyle = FlatStyle.System;
            this.btnCancel.Location = new Point(0x63, 0xe2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new Size(0x1a, 0x17);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
            this.btnNext.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            this.btnNext.FlatStyle = FlatStyle.System;
            this.btnNext.Location = new Point(0x63, 0xc5);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new Size(0x18, 0x17);
            this.btnNext.TabIndex = 4;
            this.btnNext.Text = "&Next >";
            this.btnNext.Visible = false;
            this.btnNext.Click += new EventHandler(this.btnNext_Click);
            this.btnNext.MouseDown += new MouseEventHandler(this.btnNext_MouseDown);
            this.btnBack.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            this.btnBack.FlatStyle = FlatStyle.System;
            this.btnBack.Location = new Point(0x63, 0xff);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new Size(0x18, 0x17);
            this.btnBack.TabIndex = 3;
            this.btnBack.Text = "< &Back";
            this.btnBack.Visible = false;
            this.btnBack.Click += new EventHandler(this.btnBack_Click);
            this.btnBack.MouseDown += new MouseEventHandler(this.btnBack_MouseDown);
            this.pnlButtons.BackColor = Color.Gray;
            this.pnlButtons.Controls.Add(this.label4);
            this.pnlButtons.Controls.Add(this.button2);
            this.pnlButtons.Controls.Add(this.button4);
            this.pnlButtons.Controls.Add(this.label3);
            this.pnlButtons.Controls.Add(this.button3);
            this.pnlButtons.Controls.Add(this.label2);
            this.pnlButtons.Controls.Add(this.label1);
            this.pnlButtons.Controls.Add(this.btnCancel);
            this.pnlButtons.Controls.Add(this.button1);
            this.pnlButtons.Controls.Add(this.btnBack);
            this.pnlButtons.Controls.Add(this.btnNext);
            this.pnlButtons.Dock = DockStyle.Left;
            this.pnlButtons.Location = new Point(0, 0);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new Size(0x7e, 0x210);
            this.pnlButtons.TabIndex = 1;
            this.label4.AutoSize = true;
            this.label4.Font = new Font("Arial", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label4.ForeColor = Color.White;
            this.label4.Location = new Point(0x2a, 0x1dd);
            this.label4.Name = "label4";
            this.label4.Size = new Size(0x2a, 0x10);
            this.label4.TabIndex = 11;
            this.label4.Text = "Tools";
            this.label4.Click += new EventHandler(this.label4_Click);
            this.button2.BackgroundImageLayout = ImageLayout.Stretch;
            this.button2.FlatAppearance.BorderColor = Color.White;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.CheckedBackColor = Color.Transparent;
            this.button2.FlatAppearance.MouseDownBackColor = Color.Transparent;
            this.button2.FlatAppearance.MouseOverBackColor = Color.Transparent;
            this.button2.FlatStyle = FlatStyle.Flat;
            this.button2.Location = new Point(0x1a, 0x9d);
            this.button2.Name = "button2";
            this.button2.Size = new Size(0x4b, 0x3e);
            this.button2.TabIndex = 1;
            this.button2.Tag = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.MouseLeave += new EventHandler(this.GLB_MouseLeave);
            this.button2.Click += new EventHandler(this.button2_Click);
            this.button2.MouseEnter += new EventHandler(this.GLB_MouseEnter);
            this.button4.BackgroundImageLayout = ImageLayout.Zoom;
            this.button4.FlatAppearance.BorderColor = Color.White;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatAppearance.CheckedBackColor = Color.Transparent;
            this.button4.FlatAppearance.MouseDownBackColor = Color.Transparent;
            this.button4.FlatAppearance.MouseOverBackColor = Color.Transparent;
            this.button4.FlatStyle = FlatStyle.Flat;
            this.button4.Location = new Point(0x1a, 0x18f);
            this.button4.Name = "button4";
            this.button4.Size = new Size(0x4b, 0x3e);
            this.button4.TabIndex = 10;
            this.button4.Tag = "1";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.MouseLeave += new EventHandler(this.GLB_MouseLeave);
            this.button4.Click += new EventHandler(this.button4_Click);
            this.button4.MouseEnter += new EventHandler(this.GLB_MouseEnter);
            this.label3.AutoSize = true;
            this.label3.Font = new Font("Arial", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label3.ForeColor = Color.White;
            this.label3.Location = new Point(0x23, 0x163);
            this.label3.Name = "label3";
            this.label3.Size = new Size(0x38, 0x10);
            this.label3.TabIndex = 9;
            this.label3.Text = "Reports";
            this.button3.BackgroundImageLayout = ImageLayout.Zoom;
            this.button3.FlatAppearance.BorderColor = Color.White;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatAppearance.CheckedBackColor = Color.Transparent;
            this.button3.FlatAppearance.MouseDownBackColor = Color.Transparent;
            this.button3.FlatAppearance.MouseOverBackColor = Color.Transparent;
            this.button3.FlatStyle = FlatStyle.Flat;
            this.button3.Location = new Point(0x1a, 0x116);
            this.button3.Name = "button3";
            this.button3.Size = new Size(0x4b, 0x3e);
            this.button3.TabIndex = 8;
            this.button3.Tag = "1";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.MouseLeave += new EventHandler(this.GLB_MouseLeave);
            this.button3.Click += new EventHandler(this.button3_Click);
            this.button3.MouseEnter += new EventHandler(this.GLB_MouseEnter);
            this.label2.AutoSize = true;
            this.label2.Font = new Font("Arial", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label2.ForeColor = Color.White;
            this.label2.Location = new Point(0x13, 0xe9);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0x58, 0x10);
            this.label2.TabIndex = 7;
            this.label2.Text = "Transactions";
            this.label1.AutoSize = true;
            this.label1.Font = new Font("Tahoma", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label1.ForeColor = Color.White;
            this.label1.Location = new Point(0x21, 0x6f);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x3d, 0x10);
            this.label1.TabIndex = 6;
            this.label1.Text = "Masters";
            this.button1.BackgroundImageLayout = ImageLayout.Zoom;
            this.button1.FlatAppearance.BorderColor = Color.White;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.CheckedBackColor = Color.White;
            this.button1.FlatAppearance.MouseDownBackColor = Color.Transparent;
            this.button1.FlatAppearance.MouseOverBackColor = Color.Transparent;
            this.button1.FlatStyle = FlatStyle.Flat;
            this.button1.Location = new Point(0x1a, 0x24);
            this.button1.Name = "button1";
            this.button1.Size = new Size(0x4b, 0x3e);
            this.button1.TabIndex = 1;
            this.button1.Tag = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.MouseLeave += new EventHandler(this.GLB_MouseLeave);
            this.button1.Click += new EventHandler(this.button1_Click);
            this.button1.MouseEnter += new EventHandler(this.GLB_MouseEnter);
            this.BackColor = Color.Silver;
            base.Controls.Add(this.pnlButtons);
            this.Font = new Font("Tahoma", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            base.Name = "Menu";
            base.Size = new Size(0x108, 0x210);
            base.Load += new EventHandler(this.Wizard_Load);
            this.pnlButtons.ResumeLayout(false);
            this.pnlButtons.PerformLayout();
            base.ResumeLayout(false);
        }

        #endregion
    }
}
