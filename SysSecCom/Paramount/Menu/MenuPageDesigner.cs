﻿namespace Paramount.Menu
{
    using System;
    using System.ComponentModel.Design;
    using System.Windows.Forms.Design;

    public class MenuPageDesigner : ParentControlDesigner
    {
        private void handleRemovePage(object sender, EventArgs e)
        {
            MenuPage control = this.Control as MenuPage;
            IDesignerHost host = (IDesignerHost) this.GetService(typeof(IDesignerHost));
            IComponentChangeService service = (IComponentChangeService) this.GetService(typeof(IComponentChangeService));
            DesignerTransaction transaction = host.CreateTransaction("Remove Page");
            if (control.Parent is Paramount.Menu.Menu)
            {
                Paramount.Menu.Menu parent = control.Parent as Paramount.Menu.Menu;
                service.OnComponentChanging(parent, null);
                parent.Pages.Remove(control);
                parent.Controls.Remove(control);
                service.OnComponentChanged(parent, null, null, null);
                host.DestroyComponent(control);
            }
            else
            {
                service.OnComponentChanging(control, null);
                control.Dispose();
                service.OnComponentChanged(control, null, null, null);
            }
            transaction.Commit();
        }

        public override DesignerVerbCollection Verbs
        {
            get
            {
                DesignerVerbCollection verbs = new DesignerVerbCollection();
                verbs.Add(new DesignerVerb("Remove Page", new EventHandler(this.handleRemovePage)));
                return verbs;
            }
        }
    }
}

