﻿namespace Paramount.Menu
{
    using System;

    public class MenuPageEventArgs : EventArgs
    {
        private int vPage;
        private MenuCollection vPages;

        public MenuPageEventArgs(int index, MenuCollection pages)
        {
            this.vPage = index;
            this.vPages = pages;
        }

        public MenuPage Page
        {
            get
            {
                if ((this.vPage >= 0) && (this.vPage < this.vPages.Count))
                {
                    return this.vPages[this.vPage];
                }
                return null;
            }
            set
            {
                if (!this.vPages.Contains(value))
                {
                    throw new ArgumentOutOfRangeException("NextPage", value, "The page you tried to set was not found in the wizard.");
                }
                this.vPage = this.vPages.IndexOf(value);
            }
        }

        public int PageIndex
        {
            get
            {
                return this.vPage;
            }
        }
    }
}

