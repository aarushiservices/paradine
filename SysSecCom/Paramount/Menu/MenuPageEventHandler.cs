﻿namespace Paramount.Menu
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void MenuPageEventHandler(object sender, MenuPageEventArgs e);
}

