﻿namespace Paramount.Menu
{
    using System;
    using System.Collections;
    using System.Reflection;

    public class MenuCollection : CollectionBase
    {
        private Paramount.Menu.Menu vParent;

        public MenuCollection(Paramount.Menu.Menu parent)
        {
            this.vParent = parent;
        }

        public int Add(MenuPage value)
        {
            return base.List.Add(value);
        }

        public void AddRange(MenuPage[] pages)
        {
            foreach (MenuPage page in pages)
            {
                this.Add(page);
            }
        }

        public bool Contains(MenuPage value)
        {
            return base.List.Contains(value);
        }

        public int IndexOf(MenuPage value)
        {
            return base.List.IndexOf(value);
        }

        public void Insert(int index, MenuPage value)
        {
            base.List.Insert(index, value);
        }

        protected override void OnInsertComplete(int index, object value)
        {
            base.OnInsertComplete(index, value);
            this.vParent.PageIndex = index;
        }

        protected override void OnRemoveComplete(int index, object value)
        {
            base.OnRemoveComplete(index, value);
            if (this.vParent.PageIndex == index)
            {
                if (index < base.InnerList.Count)
                {
                    this.vParent.PageIndex = index;
                }
                else
                {
                    this.vParent.PageIndex = base.InnerList.Count - 1;
                }
            }
        }

        public void Remove(MenuPage value)
        {
            base.List.Remove(value);
        }

        public MenuPage this[int index]
        {
            get
            {
                return (MenuPage) base.List[index];
            }
            set
            {
                base.List[index] = value;
            }
        }

        public Paramount.Menu.Menu Parent
        {
            get
            {
                return this.vParent;
            }
        }
    }
}

