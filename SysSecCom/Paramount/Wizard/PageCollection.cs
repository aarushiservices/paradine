﻿namespace Paramount.Wizard
{
    using Paramount.Wizard;
    using System;
    using System.Collections;
    using System.Reflection;

    public class PageCollection : CollectionBase
    {
        private Paramount.Wizard.Wizard vParent;

        public PageCollection(Paramount.Wizard.Wizard parent)
        {
            this.vParent = parent;
        }

        public int Add(WizardPage value)
        {
            return base.List.Add(value);
        }

        public void AddRange(WizardPage[] pages)
        {
            foreach (WizardPage page in pages)
            {
                this.Add(page);
            }
        }

        public bool Contains(WizardPage value)
        {
            return base.List.Contains(value);
        }

        public int IndexOf(WizardPage value)
        {
            return base.List.IndexOf(value);
        }

        public void Insert(int index, WizardPage value)
        {
            base.List.Insert(index, value);
        }

        protected override void OnInsertComplete(int index, object value)
        {
            base.OnInsertComplete(index, value);
            this.vParent.PageIndex = index;
        }

        protected override void OnRemoveComplete(int index, object value)
        {
            base.OnRemoveComplete(index, value);
            if (this.vParent.PageIndex == index)
            {
                if (index < base.InnerList.Count)
                {
                    this.vParent.PageIndex = index;
                }
                else
                {
                    this.vParent.PageIndex = base.InnerList.Count - 1;
                }
            }
        }

        public void Remove(WizardPage value)
        {
            base.List.Remove(value);
        }

        public WizardPage this[int index]
        {
            get
            {
                return (WizardPage) base.List[index];
            }
            set
            {
                base.List[index] = value;
            }
        }

        public Paramount.Wizard.Wizard Parent
        {
            get
            {
                return this.vParent;
            }
        }
    }
}

