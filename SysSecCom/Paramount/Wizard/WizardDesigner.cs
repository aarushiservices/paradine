﻿namespace Paramount.Wizard
{
    using System;
    using System.ComponentModel.Design;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Windows.Forms.Design;

    public class WizardDesigner : ParentControlDesigner
    {
        private bool _allowGrid = true;

        public override bool CanParent(Control control)
        {
            return (control is WizardPage);
        }

        public override bool CanParent(ControlDesigner controlDesigner)
        {
            return (controlDesigner is WizardPageDesigner);
        }

        protected override bool GetHitTest(Point point)
        {
            Paramount.Wizard.Wizard control = this.Control as Paramount.Wizard.Wizard;
            return ((control.btnNext.Enabled && control.btnNext.ClientRectangle.Contains(control.btnNext.PointToClient(point))) || (control.btnBack.Enabled && control.btnBack.ClientRectangle.Contains(control.btnBack.PointToClient(point))));
        }

        private void handleAddPage(object sender, EventArgs e)
        {
            Paramount.Wizard.Wizard control = this.Control as Paramount.Wizard.Wizard;
            IDesignerHost host = (IDesignerHost) this.GetService(typeof(IDesignerHost));
            IComponentChangeService service = (IComponentChangeService) this.GetService(typeof(IComponentChangeService));
            DesignerTransaction transaction = host.CreateTransaction("Add Page");
            WizardPage page = (WizardPage) host.CreateComponent(typeof(WizardPage));
            service.OnComponentChanging(control, null);
            control.Pages.Add(page);
            control.Controls.Add(page);
            control.ActivatePage(page);
            service.OnComponentChanged(control, null, null, null);
            transaction.Commit();
        }

        protected override void OnPaintAdornments(PaintEventArgs pe)
        {
            this._allowGrid = false;
            base.OnPaintAdornments(pe);
            this._allowGrid = true;
        }

        protected override bool DrawGrid
        {
            get
            {
                return (base.DrawGrid && this._allowGrid);
            }
        }

        public override DesignerVerbCollection Verbs
        {
            get
            {
                DesignerVerbCollection verbs = new DesignerVerbCollection();
                verbs.Add(new DesignerVerb("Add Page", new EventHandler(this.handleAddPage)));
                return verbs;
            }
        }
    }
}

