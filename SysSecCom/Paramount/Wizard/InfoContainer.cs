﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Paramount.Wizard;

namespace Paramount.Wizard
{
      [Designer(typeof(InfoContainerDesigner))]
    public partial class InfoContainer : UserControl
    {
        public InfoContainer()
        {
            InitializeComponent();
        }

        private void InfoContainer_Load(object sender, EventArgs e)
        {
            this.lblTitle.Left = this.picImage.Width + 8;
            this.lblTitle.Width = (base.Width - 4) - this.lblTitle.Left;
        }
        [Category("Appearance")]
        public System.Drawing.Image Image
        {
            get
            {
                return this.picImage.Image;
            }
            set
            {
                this.picImage.Image = value;
            }
        }

        [Category("Appearance")]
        public string PageTitle
        {
            get
            {
                return this.lblTitle.Text;
            }
            set
            {
                this.lblTitle.Text = value;
            }
        }
    }
}
