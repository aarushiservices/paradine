﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Paramount.Wizard;
using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace Paramount.Wizard
{
     [ToolboxItem(true), Designer(typeof(WizardDesigner)), ToolboxBitmap(typeof(Paramount.Wizard.Wizard))]
    public partial class Wizard : UserControl
    {
        public event CancelEventHandler CloseFromCancel;
        public Wizard()
        {
            this.vPages = new PageCollection(this);
            this.InitializeComponent();

        }
        protected internal void ActivatePage(WizardPage page)
        {
            if (this.vActivePage != null)
            {
                this.vActivePage.Visible = false;
            }
            this.vActivePage = page;
            if (this.vActivePage != null)
            {
                this.vActivePage.Parent = this;
                if (!base.Contains(this.vActivePage))
                {
                    base.Container.Add(this.vActivePage);
                }
                this.vActivePage.Dock = DockStyle.Fill;
                this.vActivePage.Visible = true;
                this.vActivePage.BringToFront();
                this.vActivePage.FocusFirstTabIndex();
            }
            if (this.PageIndex > 0)
            {
                this.btnBack.Enabled = true;
            }
            else
            {
                this.btnBack.Enabled = false;
            }
            if (!((this.vPages.IndexOf(this.vActivePage) >= (this.vPages.Count - 1)) || this.vActivePage.IsFinishPage))
            {
                this.btnNext.Text = "&Next >";
                this.btnNext.Enabled = true;
                this.btnNext.DialogResult = DialogResult.None;
            }
            else
            {
                this.btnNext.Text = "Fi&nish";
                if (base.DesignMode && (this.vPages.IndexOf(this.vActivePage) == (this.vPages.Count - 1)))
                {
                    this.btnNext.Enabled = false;
                }
                else
                {
                    this.btnNext.Enabled = true;
                    this.btnNext.DialogResult = DialogResult.OK;
                }
            }
            if (this.vActivePage != null)
            {
                this.vActivePage.Invalidate();
            }
            else
            {
                base.Invalidate();
            }
        }

        protected internal void ActivatePage(int index)
        {
            if ((index < 0) || (index >= this.vPages.Count))
            {
                this.btnNext.Enabled = false;
                this.btnBack.Enabled = false;
            }
            else
            {
                WizardPage page = this.vPages[index];
                this.ActivatePage(page);
            }
        }

        public void Back()
        {
            Debug.Assert(this.PageIndex < this.vPages.Count, "Page Index was beyond Maximum pages");
            Debug.Assert((this.PageIndex > 0) && (this.PageIndex < this.vPages.Count), "Attempted to go back to a page that doesn't exist");
            int index = this.vActivePage.OnCloseFromBack(this);
            this.ActivatePage(index);
            this.vActivePage.OnShowFromBack(this);
        }

        public void BackTo(WizardPage page)
        {
            this.ActivatePage(page);
            this.vActivePage.OnShowFromNext(this);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Back();
        }

        private void btnBack_MouseDown(object sender, MouseEventArgs e)
        {
            if (base.DesignMode)
            {
                this.Back();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            CancelEventArgs args = new CancelEventArgs();
            if (this.CloseFromCancel != null)
            {
                this.CloseFromCancel(this, args);
            }
            if (!args.Cancel)
            {
                base.FindForm().Close();
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            this.Next();
        }

        private void btnNext_MouseDown(object sender, MouseEventArgs e)
        {
            if (base.DesignMode)
            {
                this.Next();
            }
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (this.components != null))
        //    {
        //        this.components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        public void Next()
        {
            Debug.Assert(this.PageIndex >= 0, "Page Index was below 0");
            int index = this.vActivePage.OnCloseFromNext(this);
            if ((this.PageIndex < (this.vPages.Count - 1)) && (!this.vActivePage.IsFinishPage || base.DesignMode))
            {
                this.ActivatePage(index);
                this.vActivePage.OnShowFromNext(this);
            }
            else
            {
                Debug.Assert(this.PageIndex < this.vPages.Count, "Error I've just gone past the finish", "btnNext_Click tried to go to page " + Convert.ToString((int)(this.PageIndex + 1)) + ", but I only have " + Convert.ToString(this.vPages.Count));
                if (!base.DesignMode)
                {
                    base.ParentForm.Close();
                }
            }
        }

        public void NextTo(WizardPage page)
        {
            this.ActivatePage(page);
            this.vActivePage.OnShowFromNext(this);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            if (base.DesignMode)
            {
                SizeF ef = e.Graphics.MeasureString("No wizard pages inside the wizard.", this.Font);
                RectangleF layoutRectangle = new RectangleF((base.Width - ef.Width) / 2f, (this.pnlButtons.Top - ef.Height) / 2f, ef.Width, ef.Height);
                Pen pen = (Pen)SystemPens.GrayText.Clone();
                pen.DashStyle = DashStyle.Dash;
                e.Graphics.DrawRectangle(pen, (int)(base.Left + 8), (int)(base.Top + 8), (int)(base.Width - 0x11), (int)(this.pnlButtons.Top - 0x11));
                e.Graphics.DrawString("No wizard pages inside the wizard.", this.Font, new SolidBrush(SystemColors.GrayText), layoutRectangle);
            }
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            if (base.DesignMode)
            {
                base.Invalidate();
            }
        }

        private void Wizard_Load(object sender, EventArgs e)
        {
            this.ActivatePage(0);
            Form form = base.FindForm();
            if (!((form == null) || base.DesignMode))
            {
                form.CancelButton = this.btnCancel;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Category("Wizard")]
        public bool BackEnabled
        {
            get
            {
                return this.btnBack.Enabled;
            }
            set
            {
                this.btnBack.Enabled = value;
            }
        }

        [Category("Wizard"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool CancelEnabled
        {
            get
            {
                return this.btnCancel.Enabled;
            }
            set
            {
                this.btnCancel.Enabled = value;
            }
        }

        [Category("Wizard"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool NextEnabled
        {
            get
            {
                return this.btnNext.Enabled;
            }
            set
            {
                this.btnNext.Enabled = value;
            }
        }

        public WizardPage Page
        {
            get
            {
                return this.vActivePage;
            }
        }

        [Category("Wizard")]
        internal int PageIndex
        {
            get
            {
                return this.vPages.IndexOf(this.vActivePage);
            }
            set
            {
                if (this.vPages.Count == 0)
                {
                    this.ActivatePage(-1);
                }
                else
                {
                    if ((value < -1) || (value >= this.vPages.Count))
                    {
                        throw new ArgumentOutOfRangeException("PageIndex", value, "The page index must be between 0 and " + Convert.ToString((int)(this.vPages.Count - 1)));
                    }
                    this.ActivatePage(value);
                }
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content), Category("Wizard")]
        public PageCollection Pages
        {
            get
            {
                return this.vPages;
            }
        }
    }
}
