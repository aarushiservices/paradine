﻿using Paramount.Wizard;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace Paramount.Wizard
{
    partial class Wizard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        /// 
        protected internal Button btnBack;
        private Button btnCancel;
        protected internal Button btnNext;
        //private Container components = null;
        private Header header1;
        private Panel pnlButtonBright3d;
        private Panel pnlButtonDark3d;
        protected internal Panel pnlButtons;
        private WizardPage vActivePage = null;
        private PageCollection vPages;
        private void InitializeComponent()
        {
            this.pnlButtons = new Panel();
            this.btnCancel = new Button();
            this.btnNext = new Button();
            this.btnBack = new Button();
            this.pnlButtonBright3d = new Panel();
            this.pnlButtonDark3d = new Panel();
            this.header1 = new Header();
            this.pnlButtons.SuspendLayout();
            base.SuspendLayout();
            this.pnlButtons.Controls.Add(this.btnCancel);
            this.pnlButtons.Controls.Add(this.btnNext);
            this.pnlButtons.Controls.Add(this.btnBack);
            this.pnlButtons.Controls.Add(this.pnlButtonBright3d);
            this.pnlButtons.Controls.Add(this.pnlButtonDark3d);
            this.pnlButtons.Dock = DockStyle.Bottom;
            this.pnlButtons.Location = new Point(0, 0x115);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new Size(0x1b1, 0x30);
            this.pnlButtons.TabIndex = 0;
            this.btnCancel.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            this.btnCancel.FlatStyle = FlatStyle.System;
            this.btnCancel.Location = new Point(0x159, 12);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new Size(0x4b, 0x17);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnNext.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            this.btnNext.FlatStyle = FlatStyle.System;
            this.btnNext.Location = new Point(0x105, 12);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new Size(0x4b, 0x17);
            this.btnNext.TabIndex = 4;
            this.btnNext.Text = "&Next >";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            this.btnNext.MouseDown += new MouseEventHandler(this.btnNext_MouseDown);
            this.btnBack.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            this.btnBack.FlatStyle = FlatStyle.System;
            this.btnBack.Location = new Point(0xb9, 12);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new Size(0x4b, 0x17);
            this.btnBack.TabIndex = 3;
            this.btnBack.Text = "< &Back";
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            this.btnBack.MouseDown += new MouseEventHandler(this.btnBack_MouseDown);
            this.pnlButtonBright3d.BackColor = SystemColors.ControlLightLight;
            this.pnlButtonBright3d.Dock = DockStyle.Top;
            this.pnlButtonBright3d.Location = new Point(0, 1);
            this.pnlButtonBright3d.Name = "pnlButtonBright3d";
            this.pnlButtonBright3d.Size = new Size(0x1b1, 1);
            this.pnlButtonBright3d.TabIndex = 1;
            this.pnlButtonDark3d.BackColor = SystemColors.ControlDark;
            this.pnlButtonDark3d.Dock = DockStyle.Top;
            this.pnlButtonDark3d.Location = new Point(0, 0);
            this.pnlButtonDark3d.Name = "pnlButtonDark3d";
            this.pnlButtonDark3d.Size = new Size(0x1b1, 1);
            this.pnlButtonDark3d.TabIndex = 2;
            this.header1.BackColor = SystemColors.Control;
            this.header1.CausesValidation = false;
            this.header1.Description = "Description";
            this.header1.Dock = DockStyle.Top;
            this.header1.Image = null;
            this.header1.Location = new Point(0, 0);
            this.header1.Name = "header1";
            this.header1.Size = new Size(0x1b1, 0x2e);
            this.header1.TabIndex = 1;
            this.header1.Title = "Title";
            base.Controls.Add(this.header1);
            base.Controls.Add(this.pnlButtons);
            this.Font = new Font("Tahoma", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            base.Name = "Wizard";
            base.Size = new Size(0x1b1, 0x145);
            base.Load += new System.EventHandler(this.Wizard_Load);
            this.pnlButtons.ResumeLayout(false);
            base.ResumeLayout(false);
        }

        #endregion
    }
}
