﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Paramount.Wizard;

namespace Paramount.Wizard
{
      [Designer(typeof(HeaderDesigner))]
    public partial class Header : UserControl
    {
        public Header()
        {
            this.InitializeComponent();
        }

        private void Header_SizeChanged(object sender, EventArgs e)
        {
        }

        private void ResizeImageAndText()
        {
        }

        [Category("Appearance")]
        public string Description
        {
            get
            {
                return this.lblDescription.Text;
            }
            set
            {
                this.lblDescription.Text = value;
            }
        }

        [Category("Appearance")]
        public System.Drawing.Image Image
        {
            get
            {
                return this.picIcon.Image;
            }
            set
            {
                this.picIcon.Image = value;
                this.ResizeImageAndText();
            }
        }

        [Category("Appearance")]
        public string Title
        {
            get
            {
                return this.lblTitle.Text;
            }
            set
            {
                this.lblTitle.Text = value;
            }
        }
    }
}
