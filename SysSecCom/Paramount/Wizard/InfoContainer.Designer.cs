﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace Paramount.Wizard
{
    partial class InfoContainer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        /// 
        //private Container components = null;
        private Label lblTitle;
        private System.Windows.Forms.PictureBox picImage;
        private void InitializeComponent()
        {
            this.picImage = new PictureBox();
            this.lblTitle = new Label();
            ((ISupportInitialize)this.picImage).BeginInit();
            base.SuspendLayout();
            this.picImage.BackgroundImageLayout = ImageLayout.Stretch;
            this.picImage.Dock = DockStyle.Left;
            this.picImage.Location = new Point(0, 0);
            this.picImage.Name = "picImage";
            this.picImage.Size = new Size(0xa4, 0x184);
            this.picImage.TabIndex = 0;
            this.picImage.TabStop = false;
            this.lblTitle.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Top;
            this.lblTitle.FlatStyle = FlatStyle.System;
            this.lblTitle.Font = new Font("Tahoma", 12f, System.Drawing.FontStyle.Bold, GraphicsUnit.Point, 0);
            this.lblTitle.Location = new Point(0xac, 4);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new Size(0x130, 0x180);
            this.lblTitle.TabIndex = 7;
            this.lblTitle.Text = "Welcome to the / Completing the <Title> Wizard";
            this.BackColor = Color.White;
            base.Controls.Add(this.lblTitle);
            base.Controls.Add(this.picImage);
            base.Name = "InfoContainer";
            base.Size = new Size(480, 0x184);
            base.Load += new System.EventHandler(this.InfoContainer_Load);
            ((ISupportInitialize)this.picImage).EndInit();
            base.ResumeLayout(false);
        }

        #endregion
    }
}
