﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace Paramount.Wizard
{
    partial class InfoPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        /// 
        //private IContainer components = null;
        private Label lblDescription;
        private void InitializeComponent()
        {
           // components = new System.ComponentModel.Container();
            this.lblDescription = new Label();
            base.SuspendLayout();
            this.lblDescription.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Top;
            this.lblDescription.FlatStyle = FlatStyle.System;
            this.lblDescription.Font = new Font("Tahoma", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.lblDescription.Location = new Point(0xac, 0x38);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new Size(0x145, 0x148);
            this.lblDescription.TabIndex = 8;
            this.lblDescription.Text = "This wizard enables you to...";
            base.Controls.Add(this.lblDescription);
            base.Name = "InfoPage";
            base.Size = new Size(0x1f5, 0x184);
            base.Controls.SetChildIndex(this.lblDescription, 0);
            base.ResumeLayout(false);
        }

        #endregion
    }
}
