﻿namespace Paramount.Wizard
{
    using System;
    using System.Collections;
    using System.Windows.Forms.Design;

    public class HeaderDesigner : ParentControlDesigner
    {
        protected override void PreFilterProperties(IDictionary properties)
        {
            base.PreFilterProperties(properties);
            if (properties.Contains("BackgroundImage"))
            {
                properties.Remove("BackgroundImage");
            }
            if (properties.Contains("DrawGrid"))
            {
                properties.Remove("DrawGrid");
            }
        }

        protected override bool DrawGrid
        {
            get
            {
                return false;
            }
        }
    }
}

