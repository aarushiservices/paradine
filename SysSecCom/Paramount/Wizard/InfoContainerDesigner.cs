﻿namespace Paramount.Wizard
{
    using System;
    using System.Collections;
    using System.Windows.Forms.Design;

    public class InfoContainerDesigner : ParentControlDesigner
    {
        protected override void PreFilterProperties(IDictionary properties)
        {
            base.PreFilterProperties(properties);
            if (properties.Contains("BackgroundImage"))
            {
                properties.Remove("BackgroundImage");
            }
        }
    }
}

