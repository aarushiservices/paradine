﻿
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace Paramount.Wizard
{
    
    partial class Header
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        /// 
        private Label lblDescription;
        private Label lblTitle;
        private PictureBox picIcon;
        private Panel pnl3dBright;
        private Panel pnl3dDark;
        private Panel pnlDockPadding;
        private void InitializeComponent()
        {
            this.pnlDockPadding = new Panel();
            this.lblDescription = new Label();
            this.lblTitle = new Label();
            this.picIcon = new PictureBox();
            this.pnl3dDark = new Panel();
            this.pnl3dBright = new Panel();
            this.pnlDockPadding.SuspendLayout();
            ((ISupportInitialize)this.picIcon).BeginInit();
            base.SuspendLayout();
            this.pnlDockPadding.BackColor = SystemColors.Window;
            this.pnlDockPadding.Controls.Add(this.lblTitle);
            this.pnlDockPadding.Controls.Add(this.picIcon);
            this.pnlDockPadding.Controls.Add(this.lblDescription);
            this.pnlDockPadding.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDockPadding.Font = new Font("Tahoma", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.pnlDockPadding.Location = new Point(0, 0);
            this.pnlDockPadding.Name = "pnlDockPadding";
            this.pnlDockPadding.Padding = new Padding(8, 6, 4, 4);
            this.pnlDockPadding.Size = new Size(0x144, 0x40);
            this.pnlDockPadding.TabIndex = 6;
            this.lblDescription.FlatStyle = FlatStyle.System;
            this.lblDescription.Location = new Point(20, 0x1a);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new Size(0x14b, 0x21);
            this.lblDescription.TabIndex = 5;
            this.lblDescription.Text = "Description";
            this.lblTitle.Dock = DockStyle.Top;
            this.lblTitle.FlatStyle = FlatStyle.System;
            this.lblTitle.Font = new Font("Tahoma", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.lblTitle.Location = new Point(8, 6);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new Size(0xa5, 0x15);
            this.lblTitle.TabIndex = 4;
            this.lblTitle.Text = "Title";
            this.picIcon.BackgroundImageLayout = ImageLayout.Zoom;
            this.picIcon.Dock = DockStyle.Right;
            this.picIcon.Location = new Point(0xad, 6);
            this.picIcon.Name = "picIcon";
            this.picIcon.Size = new Size(0x93, 0x36);
            this.picIcon.TabIndex = 3;
            this.picIcon.TabStop = false;
            this.pnl3dDark.BackColor = SystemColors.ControlDark;
            this.pnl3dDark.Dock = DockStyle.Bottom;
            this.pnl3dDark.Location = new Point(0, 0x3e);
            this.pnl3dDark.Name = "pnl3dDark";
            this.pnl3dDark.Size = new Size(0x144, 1);
            this.pnl3dDark.TabIndex = 7;
            this.pnl3dBright.BackColor = Color.White;
            this.pnl3dBright.Dock = DockStyle.Bottom;
            this.pnl3dBright.Location = new Point(0, 0x3f);
            this.pnl3dBright.Name = "pnl3dBright";
            this.pnl3dBright.Size = new Size(0x144, 1);
            this.pnl3dBright.TabIndex = 8;
            this.BackColor = SystemColors.Control;
            base.CausesValidation = false;
            base.Controls.Add(this.pnl3dDark);
            base.Controls.Add(this.pnl3dBright);
            base.Controls.Add(this.pnlDockPadding);
            base.Name = "Header";
            base.Size = new Size(0x144, 0x40);
            base.SizeChanged += new EventHandler(this.Header_SizeChanged);
            this.pnlDockPadding.ResumeLayout(false);
            ((ISupportInitialize)this.picIcon).EndInit();
            base.ResumeLayout(false);
        }

        #endregion
    }
}
