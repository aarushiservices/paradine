﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.CompilerServices;
using Paramount.Wizard;


namespace Paramount.Wizard
{
    [Designer(typeof(WizardPageDesigner))]
    public partial class WizardPage : Panel
    {

        public WizardPage()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }
        private bool _IsFinishPage = false;

        public event PageEventHandler CloseFromBack;

        public event PageEventHandler CloseFromNext;

        public event EventHandler ShowFromBack;

        public event EventHandler ShowFromNext;

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //    }
        //    base.Dispose(disposing);
        //}

        public void FocusFirstTabIndex()
        {
            Control control = null;
            foreach (Control control2 in base.Controls)
            {
                if (control2.CanFocus && ((control == null) || (control2.TabIndex < control.TabIndex)))
                {
                    control = control2;
                }
            }
            if (control != null)
            {
                control.Focus();
            }
            else
            {
                base.Focus();
            }
        }

        public int OnCloseFromBack(Paramount.Wizard.Wizard wiz)
        {
            PageEventArgs e = new PageEventArgs(wiz.PageIndex - 1, wiz.Pages);
            if (this.CloseFromBack != null)
            {
                this.CloseFromBack(wiz, e);
            }
            return e.PageIndex;
        }

        public int OnCloseFromNext(Paramount.Wizard.Wizard wiz)
        {
            PageEventArgs e = new PageEventArgs(wiz.PageIndex + 1, wiz.Pages);
            if (this.CloseFromNext != null)
            {
                this.CloseFromNext(wiz, e);
            }
            return e.PageIndex;
        }

        public void OnShowFromBack(Paramount.Wizard.Wizard wiz)
        {
            if (this.ShowFromBack != null)
            {
                this.ShowFromBack(wiz, EventArgs.Empty);
            }
        }

        public void OnShowFromNext(Paramount.Wizard.Wizard wiz)
        {
            if (this.ShowFromNext != null)
            {
                this.ShowFromNext(wiz, EventArgs.Empty);
            }
        }

        [Category("Wizard")]
        public bool IsFinishPage
        {
            get
            {
                return this._IsFinishPage;
            }
            set
            {
                this._IsFinishPage = value;
            }
        }
    }
}
