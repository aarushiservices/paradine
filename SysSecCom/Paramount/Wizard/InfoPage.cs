﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Paramount.Wizard
{
    public partial class InfoPage : InfoContainer
    {
        public InfoPage()
        {
            this.InitializeComponent();
        }

        
        [Category("Appearance")]
        public string PageText
        {
            get
            {
                return this.lblDescription.Text;
            }
            set
            {
                this.lblDescription.Text = value;
            }
        }
    }
}
