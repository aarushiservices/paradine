﻿namespace Paramount.Viewer
{
    using System;
    using System.Collections;
    using System.Reflection;

    public class ViewerCollection : CollectionBase
    {
        private Paramount.Viewer.Viewer vParent;

        public ViewerCollection(Paramount.Viewer.Viewer parent)
        {
            this.vParent = parent;
        }

        public int Add(ViewerPage value)
        {
            return base.List.Add(value);
        }

        public void AddRange(ViewerPage[] pages)
        {
            foreach (ViewerPage page in pages)
            {
                this.Add(page);
            }
        }

        public bool Contains(ViewerPage value)
        {
            return base.List.Contains(value);
        }

        public int IndexOf(ViewerPage value)
        {
            return base.List.IndexOf(value);
        }

        public void Insert(int index, ViewerPage value)
        {
            base.List.Insert(index, value);
        }

        protected override void OnInsertComplete(int index, object value)
        {
            base.OnInsertComplete(index, value);
            this.vParent.PageIndex = index;
        }

        protected override void OnRemoveComplete(int index, object value)
        {
            base.OnRemoveComplete(index, value);
            if (this.vParent.PageIndex == index)
            {
                if (index < base.InnerList.Count)
                {
                    this.vParent.PageIndex = index;
                }
                else
                {
                    this.vParent.PageIndex = base.InnerList.Count - 1;
                }
            }
        }

        public void Remove(ViewerPage value)
        {
            base.List.Remove(value);
        }

        public ViewerPage this[int index]
        {
            get
            {
                return (ViewerPage) base.List[index];
            }
            set
            {
                base.List[index] = value;
            }
        }

        public Paramount.Viewer.Viewer Parent
        {
            get
            {
                return this.vParent;
            }
        }
    }
}

