﻿namespace Paramount.Viewer
{
    using System;
    using System.ComponentModel.Design;
    using System.Windows.Forms.Design;

    public class ViewerPageDesigner : ParentControlDesigner
    {
        private void handleRemovePage(object sender, EventArgs e)
        {
            ViewerPage control = this.Control as ViewerPage;
            IDesignerHost host = (IDesignerHost) this.GetService(typeof(IDesignerHost));
            IComponentChangeService service = (IComponentChangeService) this.GetService(typeof(IComponentChangeService));
            DesignerTransaction transaction = host.CreateTransaction("Remove Page");
            if (control.Parent is Paramount.Viewer.Viewer)
            {
                Paramount.Viewer.Viewer parent = control.Parent as Paramount.Viewer.Viewer;
                service.OnComponentChanging(parent, null);
                parent.Pages.Remove(control);
                parent.Controls.Remove(control);
                service.OnComponentChanged(parent, null, null, null);
                host.DestroyComponent(control);
            }
            else
            {
                service.OnComponentChanging(control, null);
                control.Dispose();
                service.OnComponentChanged(control, null, null, null);
            }
            transaction.Commit();
        }

        public override DesignerVerbCollection Verbs
        {
            get
            {
                DesignerVerbCollection verbs = new DesignerVerbCollection();
                verbs.Add(new DesignerVerb("Remove Page", new EventHandler(this.handleRemovePage)));
                return verbs;
            }
        }
    }
}

