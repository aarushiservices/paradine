﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace Paramount.Viewer
{
    partial class Viewer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        protected internal Button btnBack;
        private Button btnCancel;
        protected internal Button btnNext;
        private Button button2;
        private Button button3;
        private Button button4;
        private Button button5;
        private Button button6;        
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Panel pnlButtons;
        private ViewerPage vActivePage = null;
        private ViewerCollection vPages;

        public event CancelEventHandler CloseFromCancel;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new Button();
            this.btnNext = new Button();
            this.btnBack = new Button();
            this.pnlButtons = new Panel();
            this.button6 = new Button();
            this.button4 = new Button();
            this.button3 = new Button();
            this.label5 = new Label();
            this.button5 = new Button();
            this.label4 = new Label();
            this.label3 = new Label();
            this.label2 = new Label();
            this.button2 = new Button();
            this.label1 = new Label();
            this.pnlButtons.SuspendLayout();
            base.SuspendLayout();
            this.btnCancel.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            this.btnCancel.FlatStyle = FlatStyle.System;
            this.btnCancel.Location = new Point(0x23d, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new Size(0x1a, 0x17);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
            this.btnNext.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            this.btnNext.FlatStyle = FlatStyle.System;
            this.btnNext.Location = new Point(0x21f, 3);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new Size(0x18, 0x17);
            this.btnNext.TabIndex = 4;
            this.btnNext.Text = "&Next >";
            this.btnNext.Visible = false;
            this.btnNext.Click += new EventHandler(this.btnNext_Click);
            this.btnNext.MouseDown += new MouseEventHandler(this.btnNext_MouseDown);
            this.btnBack.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            this.btnBack.FlatStyle = FlatStyle.System;
            this.btnBack.Location = new Point(0x201, 3);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new Size(0x18, 0x17);
            this.btnBack.TabIndex = 3;
            this.btnBack.Text = "< &Back";
            this.btnBack.Visible = false;
            this.btnBack.Click += new EventHandler(this.btnBack_Click);
            this.btnBack.MouseDown += new MouseEventHandler(this.btnBack_MouseDown);
            this.pnlButtons.BackColor = Color.Silver;
            this.pnlButtons.Controls.Add(this.button6);
            this.pnlButtons.Controls.Add(this.button4);
            this.pnlButtons.Controls.Add(this.button3);
            this.pnlButtons.Controls.Add(this.label5);
            this.pnlButtons.Controls.Add(this.button5);
            this.pnlButtons.Controls.Add(this.label4);
            this.pnlButtons.Controls.Add(this.label3);
            this.pnlButtons.Controls.Add(this.label2);
            this.pnlButtons.Controls.Add(this.button2);
            this.pnlButtons.Controls.Add(this.label1);
            this.pnlButtons.Controls.Add(this.btnCancel);
            this.pnlButtons.Controls.Add(this.btnBack);
            this.pnlButtons.Controls.Add(this.btnNext);
            this.pnlButtons.Dock = DockStyle.Bottom;
            this.pnlButtons.Location = new Point(0, 0x278);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new Size(0x25a, 0x4b);
            this.pnlButtons.TabIndex = 1;
            this.button6.BackgroundImageLayout = ImageLayout.Zoom;
            this.button6.FlatAppearance.BorderColor = Color.White;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatAppearance.CheckedBackColor = Color.White;
            this.button6.FlatAppearance.MouseDownBackColor = Color.Transparent;
            this.button6.FlatAppearance.MouseOverBackColor = Color.Transparent;
            this.button6.FlatStyle = FlatStyle.Flat;
            this.button6.Location = new Point(0xbc, 12);
            this.button6.Name = "button6";
            this.button6.Size = new Size(0x21, 30);
            this.button6.TabIndex = 10;
            this.button6.Tag = "1";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.MouseLeave += new EventHandler(this.GLB_MouseLeave);
            this.button6.Click += new EventHandler(this.button6_Click);
            this.button6.MouseEnter += new EventHandler(this.GLB_MouseEnter);
            this.button4.BackgroundImageLayout = ImageLayout.Zoom;
            this.button4.FlatAppearance.BorderColor = Color.White;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatAppearance.CheckedBackColor = Color.White;
            this.button4.FlatAppearance.MouseDownBackColor = Color.Transparent;
            this.button4.FlatAppearance.MouseOverBackColor = Color.Transparent;
            this.button4.FlatStyle = FlatStyle.Flat;
            this.button4.Location = new Point(0x69, 12);
            this.button4.Name = "button4";
            this.button4.Size = new Size(0x21, 30);
            this.button4.TabIndex = 11;
            this.button4.Tag = "1";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.MouseLeave += new EventHandler(this.GLB_MouseLeave);
            this.button4.Click += new EventHandler(this.button4_Click);
            this.button4.MouseEnter += new EventHandler(this.GLB_MouseEnter);
            this.button3.BackgroundImageLayout = ImageLayout.Zoom;
            this.button3.FlatAppearance.BorderColor = Color.White;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatAppearance.CheckedBackColor = Color.White;
            this.button3.FlatAppearance.MouseDownBackColor = Color.Transparent;
            this.button3.FlatAppearance.MouseOverBackColor = Color.Transparent;
            this.button3.FlatStyle = FlatStyle.Flat;
            this.button3.Location = new Point(0x113, 12);
            this.button3.Name = "button3";
            this.button3.Size = new Size(0x21, 30);
            this.button3.TabIndex = 9;
            this.button3.Tag = "1";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.MouseLeave += new EventHandler(this.GLB_MouseLeave);
            this.button3.Click += new EventHandler(this.button3_Click);
            this.button3.MouseEnter += new EventHandler(this.GLB_MouseEnter);
            this.label5.AutoSize = true;
            this.label5.Font = new Font("Tahoma", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.label5.Location = new Point(0x14d, 0x30);
            this.label5.Name = "label5";
            this.label5.Size = new Size(0x48, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Banquet View";
            this.button5.BackgroundImageLayout = ImageLayout.Zoom;
            this.button5.FlatAppearance.BorderColor = Color.White;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatAppearance.CheckedBackColor = Color.White;
            this.button5.FlatAppearance.MouseDownBackColor = Color.Transparent;
            this.button5.FlatAppearance.MouseOverBackColor = Color.Transparent;
            this.button5.FlatStyle = FlatStyle.Flat;
            this.button5.Location = new Point(0x162, 12);
            this.button5.Name = "button5";
            this.button5.Size = new Size(0x21, 30);
            this.button5.TabIndex = 13;
            this.button5.Tag = "1";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.MouseLeave += new EventHandler(this.GLB_MouseLeave);
            this.button5.Click += new EventHandler(this.button5_Click);
            this.button5.MouseEnter += new EventHandler(this.GLB_MouseEnter);
            this.label4.AutoSize = true;
            this.label4.Font = new Font("Tahoma", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.label4.Location = new Point(250, 0x30);
            this.label4.Name = "label4";
            this.label4.Size = new Size(0x4c, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Summary View";
            this.label3.AutoSize = true;
            this.label3.Font = new Font("Tahoma", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.label3.Location = new Point(0xa3, 0x30);
            this.label3.Name = "label3";
            this.label3.Size = new Size(80, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Inventory View";
            this.label2.AutoSize = true;
            this.label2.Font = new Font("Tahoma", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.label2.Location = new Point(0x4e, 0x30);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0x4e, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Room List View";
            this.button2.BackgroundImageLayout = ImageLayout.Zoom;
            this.button2.FlatAppearance.BorderColor = Color.White;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.CheckedBackColor = Color.White;
            this.button2.FlatAppearance.MouseDownBackColor = Color.Transparent;
            this.button2.FlatAppearance.MouseOverBackColor = Color.Transparent;
            this.button2.FlatStyle = FlatStyle.Flat;
            this.button2.Location = new Point(0x1c, 12);
            this.button2.Name = "button2";
            this.button2.Size = new Size(0x21, 30);
            this.button2.TabIndex = 7;
            this.button2.Tag = "1";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.MouseLeave += new EventHandler(this.GLB_MouseLeave);
            this.button2.Click += new EventHandler(this.button2_Click);
            this.button2.MouseEnter += new EventHandler(this.GLB_MouseEnter);
            this.label1.AutoSize = true;
            this.label1.Font = new Font("Tahoma", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.label1.Location = new Point(12, 0x30);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x3b, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Room View";
            this.BackColor = Color.White;
            base.Controls.Add(this.pnlButtons);
            this.Font = new Font("Tahoma", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            base.Name = "Viewer";
            base.Size = new Size(0x25a, 0x2c3);
            base.Load += new EventHandler(this.Wizard_Load);
            this.pnlButtons.ResumeLayout(false);
            this.pnlButtons.PerformLayout();
            base.ResumeLayout(false);
        }

        #endregion
    }
}
