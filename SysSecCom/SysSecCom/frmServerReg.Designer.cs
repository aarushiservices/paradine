﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace SysSecCom
{
    partial class frmServerReg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private Button button2;
        private Button button3;
        private ComboBox cmbdatabases;
        private ComboBox cmbServers;        
        public string DataBaseName;
        private Label label1;
        private Label label2;
        private Label label3;
        private OpenFileDialog ofd;
        public string ServerName;
        private TextBox txtDbname = new TextBox();
        private TextBox txtServerName = new TextBox();

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ofd = new OpenFileDialog();
            this.button2 = new Button();
            this.label3 = new Label();
            this.label2 = new Label();
            this.label1 = new Label();
            this.cmbServers = new ComboBox();
            this.button3 = new Button();
            this.cmbdatabases = new ComboBox();
            base.SuspendLayout();
            this.ofd.FileName = "openFileDialog1";
            this.button2.Location = new Point(0x9b, 0xf6);
            this.button2.Name = "button2";
            this.button2.Size = new Size(0x4b, 0x17);
            this.button2.TabIndex = 13;
            this.button2.Text = "Connect";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new EventHandler(this.button2_Click);
            this.label3.AutoSize = true;
            this.label3.Location = new Point(0x2f, 0x90);
            this.label3.Name = "label3";
            this.label3.Size = new Size(0x35, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Database";
            this.label3.Click += new EventHandler(this.label3_Click);
            this.label2.AutoSize = true;
            this.label2.BackColor = Color.Transparent;
            this.label2.Font = new Font("Arial", 11.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label2.ForeColor = Color.SaddleBrown;
            this.label2.Location = new Point(0x4c, 0x22);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0xad, 0x12);
            this.label2.TabIndex = 9;
            this.label2.Text = "Database Server Setup";
            this.label2.Click += new EventHandler(this.label2_Click);
            this.label1.AutoSize = true;
            this.label1.Location = new Point(0x1f, 0x62);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x45, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Server Name";
            this.cmbServers.FormattingEnabled = true;
            this.cmbServers.Location = new Point(0x70, 0x5e);
            this.cmbServers.Name = "cmbServers";
            this.cmbServers.Size = new Size(0xb8, 0x15);
            this.cmbServers.TabIndex = 0;
            this.cmbServers.SelectionChangeCommitted += new EventHandler(this.cmbServers_SelectionChangeCommitted);
            this.cmbServers.SelectedValueChanged += new EventHandler(this.cmbServers_SelectedValueChanged);
            this.cmbServers.Validated += new EventHandler(this.cmbServers_Validated);
            this.button3.Location = new Point(0xec, 0xf6);
            this.button3.Name = "button3";
            this.button3.Size = new Size(0x4b, 0x17);
            this.button3.TabIndex = 14;
            this.button3.Text = "Close";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new EventHandler(this.button3_Click);
            this.cmbdatabases.FormattingEnabled = true;
            this.cmbdatabases.Location = new Point(0x70, 140);
            this.cmbdatabases.Name = "cmbdatabases";
            this.cmbdatabases.Size = new Size(0xb8, 0x15);
            this.cmbdatabases.TabIndex = 15;
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            //            base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x142, 0x119);
            base.ControlBox = false;
            base.Controls.Add(this.cmbdatabases);
            base.Controls.Add(this.button3);
            base.Controls.Add(this.label3);
            base.Controls.Add(this.button2);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.label2);
            base.Controls.Add(this.cmbServers);
            //            base.FormBorderStyle = FormBorderStyle.FixedSingle;
            base.Name = "frmServerReg";
            base.StartPosition = FormStartPosition.CenterScreen;
            base.Load += new EventHandler(this.Form1_Load);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        #endregion
    }
}