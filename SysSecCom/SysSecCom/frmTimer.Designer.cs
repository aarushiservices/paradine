﻿using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace SysSecCom
{
    partial class frmTimer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private Label label1;
        private Label label2;
        private Label label3;
        private Label lblMinutes;
        private Label lblmseconds;
        private Label lblSeconds;
        private RegistryKey myKey = Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\PILCS", true);
        private Panel panel1;
        private Panel panel2;
        private Label Time;
        private Timer timer1;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new Container();
            this.timer1 = new Timer(this.components);
            this.Time = new Label();
            this.lblmseconds = new Label();
            this.lblSeconds = new Label();
            this.label1 = new Label();
            this.panel1 = new Panel();
            this.label2 = new Label();
            this.lblMinutes = new Label();
            this.panel2 = new Panel();
            this.label3 = new Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            base.SuspendLayout();
            this.timer1.Tick += new EventHandler(this.timer1_Tick);
            this.Time.AutoSize = true;
            this.Time.Font = new Font("Arial", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.Time.Location = new Point(0x36, 6);
            this.Time.Name = "Time";
            this.Time.Size = new Size(0x33, 14);
            this.Time.TabIndex = 0;
            this.Time.Text = "Time Left";
            this.Time.Click += new EventHandler(this.Time_Click);
            this.lblmseconds.AutoSize = true;
            this.lblmseconds.Font = new Font("Arial", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.lblmseconds.Location = new Point(0xaf, 6);
            this.lblmseconds.Name = "lblmseconds";
            this.lblmseconds.Size = new Size(13, 14);
            this.lblmseconds.TabIndex = 1;
            this.lblmseconds.Text = "0";
            this.lblmseconds.TextAlign = ContentAlignment.MiddleCenter;
            this.lblSeconds.AutoSize = true;
            this.lblSeconds.Font = new Font("Arial", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.lblSeconds.Location = new Point(150, 6);
            this.lblSeconds.Name = "lblSeconds";
            this.lblSeconds.Size = new Size(13, 14);
            this.lblSeconds.TabIndex = 2;
            this.lblSeconds.Text = "0";
            this.lblSeconds.TextAlign = ContentAlignment.MiddleCenter;
            this.label1.AutoSize = true;
            this.label1.Font = new Font("Arial", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.label1.Location = new Point(0xa7, 6);
            this.label1.Name = "label1";
            this.label1.Size = new Size(10, 14);
            this.label1.TabIndex = 3;
            this.label1.Text = ":";
            this.label1.TextAlign = ContentAlignment.MiddleCenter;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblMinutes);
            this.panel1.Controls.Add(this.lblmseconds);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.Time);
            this.panel1.Controls.Add(this.lblSeconds);
            this.panel1.Dock = DockStyle.Right;
            this.panel1.Location = new Point(0x187, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new Size(0xf8, 0x21);
            this.panel1.TabIndex = 4;
            this.label2.AutoSize = true;
            this.label2.Font = new Font("Arial", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.label2.Location = new Point(0x8e, 6);
            this.label2.Name = "label2";
            this.label2.Size = new Size(10, 14);
            this.label2.TabIndex = 5;
            this.label2.Text = ":";
            this.label2.TextAlign = ContentAlignment.MiddleCenter;
            this.lblMinutes.Font = new Font("Arial", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.lblMinutes.Location = new Point(0x6c, 6);
            this.lblMinutes.Name = "lblMinutes";
            this.lblMinutes.Size = new Size(0x21, 14);
            this.lblMinutes.TabIndex = 4;
            this.lblMinutes.Text = "0";
            this.lblMinutes.TextAlign = ContentAlignment.MiddleRight;
            this.panel2.Controls.Add(this.label3);
            this.panel2.Dock = DockStyle.Left;
            this.panel2.Location = new Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new Size(0x81, 0x21);
            this.panel2.TabIndex = 5;
            this.label3.Dock = DockStyle.Fill;
            this.label3.Font = new Font("Arial", 11.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new Size(0x81, 0x21);
            this.label3.TabIndex = 6;
            this.label3.Text = "Demo Version";
            this.label3.TextAlign = ContentAlignment.MiddleCenter;
            //            base.AutoScaleMode = AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Yellow;
            base.ClientSize = new Size(0x27f, 0x21);
            base.ControlBox = false;
            base.Controls.Add(this.panel2);
            base.Controls.Add(this.panel1);
            //            base.FormBorderStyle = FormBorderStyle.FixedSingle;
            base.Name = "frmTimer";
            base.Opacity = 0.85;
            //            base.SizeGripStyle = SizeGripStyle.Hide;
            base.StartPosition = FormStartPosition.Manual;
            base.TopMost = true;
            base.Load += new EventHandler(this.frmTimer_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            base.ResumeLayout(false);
        }

        #endregion
    }
}