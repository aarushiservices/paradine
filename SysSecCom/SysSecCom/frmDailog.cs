﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SysSecCom
{
    public partial class frmDailog : Form
    {
        public frmDailog()
        {
            this.components = null;
            this.InstallationNo = "";
            this.ActivationCode = "";
            this.InitializeComponent();
        }

        public frmDailog(string mInstallationNo, string mActivationCode)
        {
            this.components = null;
            this.InstallationNo = "";
            this.ActivationCode = "";
            this.InitializeComponent();
            this.InstallationNo = mInstallationNo;
            this.ActivationCode = mActivationCode;
            if (Convert.ToInt64(SystemSecurity.LeftMinutes) <= 0L)
            {
                this.btnRegisterLater.Enabled = false;
            }
            else
            {
                this.btnRegisterLater.Enabled = true;
            }
            this.label6.Text = this.label6.Text + " " + SystemSecurity.LeftMinutes.ToString() + " Minute(s)";
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            new frmRegistration(this.InstallationNo, this.ActivationCode).ShowDialog();
        }

        private void btnRegisterLater_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void frmDailog_Load(object sender, EventArgs e)
        {
        }
    }
}
