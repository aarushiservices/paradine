﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SysSecCom
{
    public partial class frmRegistration : Form
    {
        public frmRegistration()
        {
            this.InstallationNo = "";
            this.ActivationCode = "";
            this.components = null;
            this.InitializeComponent();
            SystemSecurity.LoadFields();
            this.InstallationNo = SystemSecurity.InstallationSerialNo;
            this.ActivationCode = SystemSecurity.ActivationCode;
        }

        public frmRegistration(string mInstallationNo, string mActivationCode)
        {
            this.InstallationNo = "";
            this.ActivationCode = "";
            this.components = null;
            this.InitializeComponent();
            this.InstallationNo = mInstallationNo;
            this.ActivationCode = mActivationCode;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            if (this.txtActivationCode.Text.Length > 0x26)
            {
                if (this.txtActivationCode.Text.ToString().Substring(0, 0x27) == this.ActivationCode)
                {
                    //if (Registry.LocalMachine.OpenSubKey(@"Software\Microsoft", true) == null)
                    //{
                    //    //Registry.LocalMachine.OpenSubKey(@"Software\Microsoft", true).CreateSubKey("PILCS").SetValue("LCS", this.txtActivationCode.Text);
                    //}
                    //else
                    //{
                    //    //Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\PILCS", true).SetValue("LCS", this.txtActivationCode.Text);
                    //}
                    if (Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\PILCS", true) == null)
                    {
                        Registry.LocalMachine.OpenSubKey(@"Software\Microsoft", true).CreateSubKey("PILCS").SetValue("LCS", this.txtActivationCode.Text);
                    }
                    else
                    {
                        Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\PILCS", true).SetValue("LCS", this.txtActivationCode.Text);
                    }
                    MessageBox.Show("Product Activated Successfully", "Registration");
                    Application.Exit();
                }
                else
                {
                    MessageBox.Show("Please Enter Valid Activation Code", "Registration");
                }
            }
            else
            {
                MessageBox.Show("Please Enter Valid Activation Code", "Registration");
            }
        }

        private void frmRegistration_Load(object sender, EventArgs e)
        {
            this.txtInsno.Text = this.InstallationNo;
        }
    }
}
