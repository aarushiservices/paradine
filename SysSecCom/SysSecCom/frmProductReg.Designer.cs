﻿using Paramount.Wizard;
using System;
using System.Drawing;
using System.Windows.Forms;
namespace SysSecCom
{
    partial class frmProductReg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private Paramount.Wizard.Wizard wizard1;
        private WizardPage wizardPage1;
        private WizardPage wizardPage2;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.wizard1 = new Paramount.Wizard.Wizard();
            this.wizardPage1 = new WizardPage();
            this.wizardPage2 = new WizardPage();
            this.wizard1.SuspendLayout();
            base.SuspendLayout();
            this.wizard1.Controls.Add(this.wizardPage2);
            this.wizard1.Controls.Add(this.wizardPage1);
            this.wizard1.Dock = DockStyle.Fill;
            this.wizard1.Font = new Font("Tahoma", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.wizard1.Location = new Point(0, 0);
            this.wizard1.Name = "wizard1";
            this.wizard1.Pages.AddRange(new WizardPage[] { this.wizardPage1, this.wizardPage2 });
            this.wizard1.Size = new Size(0x213, 0x134);
            this.wizard1.TabIndex = 0;
            this.wizard1.Load += new EventHandler(this.wizard1_Load);
            this.wizardPage1.Dock = DockStyle.Fill;
            this.wizardPage1.IsFinishPage = false;
            this.wizardPage1.Location = new Point(0, 0x2e);
            this.wizardPage1.Name = "wizardPage1";
            this.wizardPage1.Size = new Size(0x213, 0xd6);
            this.wizardPage1.TabIndex = 2;
            this.wizardPage2.Dock = DockStyle.Fill;
            this.wizardPage2.IsFinishPage = false;
            this.wizardPage2.Location = new Point(0, 0x2e);
            this.wizardPage2.Name = "wizardPage2";
            this.wizardPage2.Size = new Size(0x213, 0xd6);
            this.wizardPage2.TabIndex = 3;
            this.wizardPage2.Paint += new PaintEventHandler(this.wizardPage2_Paint);
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            //            base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x213, 0x134);
            base.ControlBox = false;
            base.Controls.Add(this.wizard1);
            //            base.FormBorderStyle = FormBorderStyle.FixedSingle;
            base.Name = "frmProductReg";
            base.StartPosition = FormStartPosition.CenterScreen;
            this.wizard1.ResumeLayout(false);
            base.ResumeLayout(false);
        }

        #endregion
    }
}