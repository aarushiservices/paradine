﻿using Paramount.Menu;
using System;
using System.Drawing;
using System.Windows.Forms;
namespace SysSecCom
{
    partial class frmAMCDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private string ActivationCode;
        private Button btnExit;
        private Button btnRegister;
        private Button btnRegisterLater;
        //private IContainer components;
        private GroupBox groupBox1;
        private string InstallationNo;
        private Label label1;
        private Label label3;
        private Label label4;
        private Label label5;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new GroupBox();
            this.label5 = new Label();
            this.label4 = new Label();
            this.label3 = new Label();
            this.label1 = new Label();
            this.btnExit = new Button();
            this.btnRegisterLater = new Button();
            this.btnRegister = new Button();
            this.groupBox1.SuspendLayout();
            base.SuspendLayout();
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new Point(11, 0x23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new Size(0x22d, 0xe5);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.label5.Font = new Font("Arial", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label5.Location = new Point(50, 0x39);
            this.label5.Name = "label5";
            this.label5.Size = new Size(0x1c1, 0x24);
            this.label5.TabIndex = 3;
            this.label5.Text = "We recommend to renual your AMC Certification.  That will provide product support and warrenty service.";
            this.label4.AutoSize = true;
            this.label4.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label4.Location = new Point(0x95, 0x9e);
            this.label4.Name = "label4";
            this.label4.Size = new Size(0x102, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "This Software is Protected by Copyright Law";
            this.label3.AutoSize = true;
            this.label3.Font = new Font("Arial", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label3.Location = new Point(50, 110);
            this.label3.Name = "label3";
            this.label3.Size = new Size(0x1c9, 14);
            this.label3.TabIndex = 1;
            this.label3.Text = "Please Note that your Warrenty rights are not dependent on Produict Registration.";
            this.label3.Click += new EventHandler(this.label3_Click);
            this.label1.AutoSize = true;
            this.label1.BackColor = Color.Transparent;
            this.label1.Font = new Font("Arial", 11.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label1.ForeColor = Color.SaddleBrown;
            this.label1.Location = new Point(0xd1, 9);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x6c, 0x12);
            this.label1.TabIndex = 8;
            this.label1.Text = "AMC Expired !";
            this.btnExit.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnExit.Location = new Point(0x171, 280);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new Size(0x4b, 0x17);
            this.btnExit.TabIndex = 7;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new EventHandler(this.btnExit_Click);
            this.btnRegisterLater.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnRegisterLater.Location = new Point(0xfc, 280);
            this.btnRegisterLater.Name = "btnRegisterLater";
            this.btnRegisterLater.Size = new Size(0x66, 0x17);
            this.btnRegisterLater.TabIndex = 6;
            this.btnRegisterLater.Text = "Register Later";
            this.btnRegisterLater.UseVisualStyleBackColor = true;
            this.btnRegisterLater.Click += new EventHandler(this.btnRegisterLater_Click);
            this.btnRegister.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnRegister.Location = new Point(0x87, 280);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new Size(0x66, 0x17);
            this.btnRegister.TabIndex = 5;
            this.btnRegister.Text = "Register Now";
            this.btnRegister.UseVisualStyleBackColor = true;
            this.btnRegister.Click += new EventHandler(this.btnRegister_Click);
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            //            base.AutoScaleMode = AutoScaleMode.Font;
            this.BackColor = Color.LightGoldenrodYellow;
            base.ClientSize = new Size(0x242, 0x138);
            base.ControlBox = false;
            base.Controls.Add(this.groupBox1);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.btnExit);
            base.Controls.Add(this.btnRegisterLater);
            base.Controls.Add(this.btnRegister);
            //            base.FormBorderStyle = FormBorderStyle.FixedSingle;
            base.Name = "frmAMCDialog";
            base.StartPosition = FormStartPosition.CenterScreen;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        #endregion
    }
}