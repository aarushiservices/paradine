﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace SysSecCom
{
    partial class frmDailog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private string ActivationCode;
        private Button btnExit;
        private Button btnRegister;
        private Button btnRegisterLater;
        private GroupBox groupBox1;
        private string InstallationNo;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRegister = new Button();
            this.btnRegisterLater = new Button();
            this.btnExit = new Button();
            this.label1 = new Label();
            this.groupBox1 = new GroupBox();
            this.label6 = new Label();
            this.label5 = new Label();
            this.label4 = new Label();
            this.label3 = new Label();
            this.label2 = new Label();
            this.groupBox1.SuspendLayout();
            base.SuspendLayout();
            this.btnRegister.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnRegister.Location = new Point(0x88, 0x11a);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new Size(0x66, 0x17);
            this.btnRegister.TabIndex = 0;
            this.btnRegister.Text = "Register Now";
            this.btnRegister.UseVisualStyleBackColor = true;
            this.btnRegister.Click += new EventHandler(this.btnRegister_Click);
            this.btnRegisterLater.Enabled = false;
            this.btnRegisterLater.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnRegisterLater.Location = new Point(0xfd, 0x11a);
            this.btnRegisterLater.Name = "btnRegisterLater";
            this.btnRegisterLater.Size = new Size(0x66, 0x17);
            this.btnRegisterLater.TabIndex = 1;
            this.btnRegisterLater.Text = "Register Later";
            this.btnRegisterLater.UseVisualStyleBackColor = true;
            this.btnRegisterLater.Click += new EventHandler(this.btnRegisterLater_Click);
            this.btnExit.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnExit.Location = new Point(370, 0x11a);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new Size(0x4b, 0x17);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new EventHandler(this.btnExit_Click);
            this.label1.AutoSize = true;
            this.label1.BackColor = Color.Transparent;
            this.label1.Font = new Font("Arial", 11.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label1.ForeColor = Color.Red;
            this.label1.Location = new Point(210, 11);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0xa8, 0x12);
            this.label1.TabIndex = 3;
            this.label1.Text = "Unregistered Version !";
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new Point(12, 0x20);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new Size(0x22d, 0xea);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.label6.AutoSize = true;
            this.label6.Location = new Point(0xa9, 0xce);
            this.label6.Name = "label6";
            this.label6.Size = new Size(120, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Demo Version Left Time";
            this.label5.Font = new Font("Arial", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label5.Location = new Point(0x20, 0x76);
            this.label5.Name = "label5";
            this.label5.Size = new Size(0x1c1, 0x24);
            this.label5.TabIndex = 3;
            this.label5.Text = "Unauthorised use or reproduction of this software or any part thereof may result in serve civil and criminal penalities.";
            this.label4.AutoSize = true;
            this.label4.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label4.Location = new Point(0x95, 0xaf);
            this.label4.Name = "label4";
            this.label4.Size = new Size(0x102, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "This Software is Protected by Copyright Law";
            this.label3.AutoSize = true;
            this.label3.Font = new Font("Arial", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label3.Location = new Point(0x20, 0x48);
            this.label3.Name = "label3";
            this.label3.Size = new Size(0x1fd, 14);
            this.label3.TabIndex = 1;
            this.label3.Text = "Please Register this product and you wish to receive product support or warrenty Service. ";
            this.label2.AutoSize = true;
            this.label2.Font = new Font("Arial", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label2.Location = new Point(0x20, 30);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0x1d5, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "Product detected from your System that have not been registered or Demo Version";
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            //            base.AutoScaleMode = AutoScaleMode.Font;
            this.BackColor = Color.LightGoldenrodYellow;
            base.ClientSize = new Size(0x245, 0x146);
            base.ControlBox = false;
            base.Controls.Add(this.groupBox1);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.btnExit);
            base.Controls.Add(this.btnRegisterLater);
            base.Controls.Add(this.btnRegister);
            //            base.FormBorderStyle = FormBorderStyle.FixedSingle;
            base.Name = "frmDailog";
            base.StartPosition = FormStartPosition.CenterScreen;
            base.Load += new EventHandler(this.frmDailog_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        #endregion
    }
}