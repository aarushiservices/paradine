﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace SysSecCom
{
    partial class frmRegistration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private string ActivationCode;
        private Button btnBack;
        private Button btnRegister;       
        private GroupBox groupBox1;
        private string InstallationNo;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private TextBox txtActivationCode;
        private TextBox txtInsno;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRegister = new Button();
            this.groupBox1 = new GroupBox();
            this.label4 = new Label();
            this.txtInsno = new TextBox();
            this.txtActivationCode = new TextBox();
            this.label3 = new Label();
            this.label2 = new Label();
            this.label1 = new Label();
            this.btnBack = new Button();
            this.groupBox1.SuspendLayout();
            base.SuspendLayout();
            this.btnRegister.Location = new Point(0x20a, 0x123);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new Size(0x62, 0x17);
            this.btnRegister.TabIndex = 2;
            this.btnRegister.Text = "Register";
            this.btnRegister.UseVisualStyleBackColor = true;
            this.btnRegister.Click += new EventHandler(this.btnRegister_Click);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtInsno);
            this.groupBox1.Controls.Add(this.txtActivationCode);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new Point(12, 0x2b);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new Size(0x285, 0xe5);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.label4.AutoSize = true;
            this.label4.Font = new Font("Arial", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label4.Location = new Point(0x17, 0x86);
            this.label4.Name = "label4";
            this.label4.Size = new Size(0x5d, 14);
            this.label4.TabIndex = 9;
            this.label4.Text = "Activation Code";
            this.txtInsno.BackColor = System.Drawing.Color.White;
            this.txtInsno.Font = new Font("Arial", 14.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.txtInsno.Location = new Point(0x1a, 0x40);
            this.txtInsno.Name = "txtInsno";
            this.txtInsno.ReadOnly = true;
            this.txtInsno.Size = new Size(0x265, 0x1d);
            this.txtInsno.TabIndex = 8;
            this.txtActivationCode.Font = new Font("Arial", 14.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.txtActivationCode.Location = new Point(0x1a, 0x97);
            this.txtActivationCode.Name = "txtActivationCode";
            this.txtActivationCode.Size = new Size(0x265, 0x1d);
            this.txtActivationCode.TabIndex = 7;
            this.label3.AutoSize = true;
            this.label3.Font = new Font("Arial", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label3.Location = new Point(0x17, 0x2f);
            this.label3.Name = "label3";
            this.label3.Size = new Size(0x93, 14);
            this.label3.TabIndex = 1;
            this.label3.Text = "Your Installation Serial No";
            this.label2.AutoSize = true;
            this.label2.Font = new Font("Arial", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.label2.Location = new Point(0x20, 0x1a);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0, 14);
            this.label2.TabIndex = 0;
            this.label1.AutoSize = true;
            this.label1.Font = new Font("Arial", 11.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new Point(0xae, 0x10);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x14d, 0x12);
            this.label1.TabIndex = 5;
            this.label1.Text = "Welcome to our Product Registration Program";
            this.btnBack.Location = new Point(0x199, 0x123);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new Size(0x62, 0x17);
            this.btnBack.TabIndex = 7;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new EventHandler(this.btnBack_Click);
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            this.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            base.ClientSize = new Size(680, 0x146);
            base.ControlBox = false;
            base.Controls.Add(this.btnBack);
            base.Controls.Add(this.groupBox1);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.btnRegister);
            base.Name = "frmRegistration";
            base.StartPosition = FormStartPosition.CenterScreen;
            base.Load += new EventHandler(this.frmRegistration_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        #endregion
    }
}