﻿using Microsoft.SqlServer.Management.Smo;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Sql;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SysSecCom
{
    public partial class frmServerReg : Form
    {
        public frmServerReg(SystemSecurity.SoftWares SoftwareType, TextBox tser, TextBox tdb)
        {
            if (SoftwareType == SystemSecurity.SoftWares.ParaDine)
            {
                this.DataBaseName = ParaDineSecurity.DataBaseName;
            }
            this.InitializeComponent();
            this.txtServerName = tser;
            this.txtDbname = tdb;
        }

        public void AttachDb(string SName, string mdffilename, string ldffilename)
        {
            Server server = new Server(SName);
            this.ServerName = SName;
            StringCollection files = new StringCollection();
            files.Add(mdffilename);
            files.Add(ldffilename);
            if (server.Databases.Contains(this.DataBaseName))
            {
                server.KillAllProcesses(this.DataBaseName);
                server.DetachDatabase(this.DataBaseName, true);
            }
            server.AttachDatabase(this.DataBaseName, files, AttachOptions.None);
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.txtServerName.Text = this.cmbServers.Text;
            this.txtDbname.Text = this.cmbdatabases.Text;
            base.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmbServers_SelectedValueChanged(object sender, EventArgs e)
        {
            this.cmbdatabases.Items.Clear();
            Server server = new Server(this.cmbServers.Text);
            foreach (Database database in server.Databases)
            {
                this.cmbdatabases.Items.Add(database.Name);
            }
        }

        private void cmbServers_SelectionChangeCommitted(object sender, EventArgs e)
        {

        }

        private void cmbServers_Validated(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DataTable dataSources = SqlDataSourceEnumerator.Instance.GetDataSources();
            foreach (DataRow row in dataSources.Rows)
            {
                this.cmbServers.Items.Add(Convert.ToString(row["SERVERNAME"]) + @"\" + Convert.ToString(row["INSTANCENAME"]));
            }
        }       

        private void label2_Click(object sender, EventArgs e)
        {
        }

        private void label3_Click(object sender, EventArgs e)
        {
        }

        private void wizard1_Load(object sender, EventArgs e)
        {
        }

        private void wizardPage1_Paint(object sender, PaintEventArgs e)
        {
        }
    }
}
