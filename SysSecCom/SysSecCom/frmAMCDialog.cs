﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SysSecCom
{
    public partial class frmAMCDialog : Form
    {
        public frmAMCDialog()
        {
            this.components = null;
            this.InstallationNo = "";
            this.ActivationCode = "";
            this.InitializeComponent();
        }

        public frmAMCDialog(string mInstallationNo, string mActivationCode)
        {
            this.components = null;
            this.InstallationNo = "";
            this.ActivationCode = "";
            this.InitializeComponent();
            this.InstallationNo = mInstallationNo;
            this.ActivationCode = mActivationCode;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            new frmRegistration(this.InstallationNo, this.ActivationCode).ShowDialog();
        }

        private void btnRegisterLater_Click(object sender, EventArgs e)
        {
            base.Close();
        }
        
        private void label3_Click(object sender, EventArgs e)
        {
        }
    }
}
