﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine
{
    partial class frmItemUpdation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUpdate = new Button();
            this.prgBar = new ProgressBar();
            this.label1 = new Label();
            base.SuspendLayout();
            this.btnUpdate.Location = new Point(0x90, 0x60);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new Size(0x4b, 0x17);
            this.btnUpdate.TabIndex = 0;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new EventHandler(this.btnUpdate_Click);
            this.prgBar.Location = new Point(0x20, 0x2f);
            this.prgBar.Name = "prgBar";
            this.prgBar.Size = new Size(0x13c, 0x17);
            this.prgBar.TabIndex = 1;
            this.label1.AutoSize = true;
            this.label1.Location = new Point(0x33, 0x1f);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x30, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Progress";
            base.AutoScaleDimensions = new SizeF(6f, 13f);
//          base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x178, 0x91);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.prgBar);
            base.Controls.Add(this.btnUpdate);
            base.Name = "frmItemUpdation";
            base.ShowIcon = false;
            base.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "Item Updation";
            base.ResumeLayout(false);
            base.PerformLayout();
        }
        #endregion
    }
}