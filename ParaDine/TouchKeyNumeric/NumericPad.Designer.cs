﻿using ParaDine.Properties;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace TouchKeyNumeric
{
    partial class NumericPad
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = (IContainer)new Container();
            ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(NumericPad));
            this.tlbPanel = new TableLayoutPanel();
            this.imglst = new ImageList(this.components);
            this.Close = new Panel();
            this.open = new Panel();
            this.backspace = new Panel();
            this.Num4 = new Panel();
            this.Num7 = new Panel();
            this.EnterKey = new Panel();
            this.Num8 = new Panel();
            this.Num9 = new Panel();
            this.CloseKey = new Panel();
            this.Num5 = new Panel();
            this.Num6 = new Panel();
            this.HomeKey = new Panel();
            this.Num1 = new Panel();
            this.Num2 = new Panel();
            this.num3 = new Panel();
            this.EndKey = new Panel();
            this.Num0 = new Panel();
            this.NumDot = new Panel();
            this.tlbPanel.SuspendLayout();
            this.SuspendLayout();
            this.tlbPanel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.tlbPanel.BackColor = Color.White;
            this.tlbPanel.ColumnCount = 4;
            this.tlbPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25f));
            this.tlbPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25f));
            this.tlbPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25f));
            this.tlbPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25f));
            this.tlbPanel.Controls.Add((Control)this.backspace, 2, 3);
            this.tlbPanel.Controls.Add((Control)this.Num4, 0, 1);
            this.tlbPanel.Controls.Add((Control)this.Num7, 0, 0);
            this.tlbPanel.Controls.Add((Control)this.EnterKey, 3, 3);
            this.tlbPanel.Controls.Add((Control)this.Num8, 1, 0);
            this.tlbPanel.Controls.Add((Control)this.Num9, 2, 0);
            this.tlbPanel.Controls.Add((Control)this.CloseKey, 3, 0);
            this.tlbPanel.Controls.Add((Control)this.Num5, 1, 1);
            this.tlbPanel.Controls.Add((Control)this.Num6, 2, 1);
            this.tlbPanel.Controls.Add((Control)this.HomeKey, 3, 1);
            this.tlbPanel.Controls.Add((Control)this.Num1, 0, 2);
            this.tlbPanel.Controls.Add((Control)this.Num2, 1, 2);
            this.tlbPanel.Controls.Add((Control)this.num3, 2, 2);
            this.tlbPanel.Controls.Add((Control)this.EndKey, 3, 2);
            this.tlbPanel.Controls.Add((Control)this.Num0, 0, 3);
            this.tlbPanel.Controls.Add((Control)this.NumDot, 1, 3);
            this.tlbPanel.Location = new Point(0, 0);
            this.tlbPanel.Margin = new Padding(0);
            this.tlbPanel.Name = "tlbPanel";
            this.tlbPanel.RowCount = 4;
            this.tlbPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 20f));
            this.tlbPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 20f));
            this.tlbPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 20f));
            this.tlbPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 20f));
            this.tlbPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 20f));
            this.tlbPanel.Size = new Size(395, 302);
            this.tlbPanel.TabIndex = 31;
            this.imglst.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("imglst.ImageStream");
            this.imglst.TransparentColor = Color.Transparent;
            this.imglst.Images.SetKeyName(0, "numer0.JPG");
            this.imglst.Images.SetKeyName(1, "numer9.JPG");
            this.imglst.Images.SetKeyName(2, "numer1.JPG");
            this.imglst.Images.SetKeyName(3, "numer2.JPG");
            this.imglst.Images.SetKeyName(4, "numer3.JPG");
            this.imglst.Images.SetKeyName(5, "numer4.JPG");
            this.imglst.Images.SetKeyName(6, "numer5.JPG");
            this.imglst.Images.SetKeyName(7, "numer6.JPG");
            this.imglst.Images.SetKeyName(8, "numer7.JPG");
            this.imglst.Images.SetKeyName(9, "numer8.JPG");
            this.imglst.Images.SetKeyName(10, "close.JPG");
            this.imglst.Images.SetKeyName(11, "dot.JPG");
            this.imglst.Images.SetKeyName(12, "home.JPG");
            this.imglst.Images.SetKeyName(13, "end.JPG");
            this.imglst.Images.SetKeyName(14, "enter.JPG");
           // this.Close.BackgroundImage = (Image)Resources.close;
            this.Close.BackgroundImageLayout = ImageLayout.Stretch;
            this.Close.Location = new Point(10, 317);
            this.Close.Name = "Close";
            this.Close.Size = new Size(48, 35);
            this.Close.TabIndex = 33;
            //this.open.BackgroundImage = (Image)Resources.openkey1;
            this.open.BackgroundImageLayout = ImageLayout.Stretch;
            this.open.Location = new Point(74, 317);
            this.open.Name = "open";
            this.open.Size = new Size(60, 44);
            this.open.TabIndex = 32;
          //  this.backspace.BackgroundImage = (Image)Resources.backspace;
            this.backspace.BackgroundImageLayout = ImageLayout.Stretch;
            this.backspace.Dock = DockStyle.Fill;
            this.backspace.Location = new Point(199, 228);
            this.backspace.Name = "backspace";
            this.backspace.Size = new Size(92, 71);
            this.backspace.TabIndex = 20;
            this.backspace.Paint += new PaintEventHandler(this.backspace_Paint);
            this.backspace.MouseClick += new MouseEventHandler(this.backspace_MouseClick);
            this.Num4.BackgroundImage = (Image)componentResourceManager.GetObject("Num4.BackgroundImage");
            this.Num4.BackgroundImageLayout = ImageLayout.Stretch;
            this.Num4.Dock = DockStyle.Fill;
            this.Num4.Location = new Point(3, 78);
            this.Num4.Name = "Num4";
            this.Num4.Size = new Size(92, 69);
            this.Num4.TabIndex = 8;
            this.Num4.Tag = (object)"4";
            this.Num7.BackgroundImage = (Image)componentResourceManager.GetObject("Num7.BackgroundImage");
            this.Num7.BackgroundImageLayout = ImageLayout.Stretch;
            this.Num7.Dock = DockStyle.Fill;
            this.Num7.Location = new Point(3, 3);
            this.Num7.Name = "Num7";
            this.Num7.Size = new Size(92, 69);
            this.Num7.TabIndex = 4;
            this.Num7.Tag = (object)"7";
            this.EnterKey.BackgroundImage = (Image)componentResourceManager.GetObject("EnterKey.BackgroundImage");
            this.EnterKey.BackgroundImageLayout = ImageLayout.Stretch;
            this.EnterKey.Dock = DockStyle.Fill;
            this.EnterKey.Location = new Point(297, 228);
            this.EnterKey.Name = "EnterKey";
            this.EnterKey.Size = new Size(95, 71);
            this.EnterKey.TabIndex = 17;
            this.EnterKey.Tag = (object)"13";
            this.Num8.BackgroundImage = (Image)componentResourceManager.GetObject("Num8.BackgroundImage");
            this.Num8.BackgroundImageLayout = ImageLayout.Stretch;
            this.Num8.Dock = DockStyle.Fill;
            this.Num8.Location = new Point(101, 3);
            this.Num8.Name = "Num8";
            this.Num8.Size = new Size(92, 69);
            this.Num8.TabIndex = 5;
            this.Num8.Tag = (object)"8";
            this.Num9.BackgroundImage = (Image)componentResourceManager.GetObject("Num9.BackgroundImage");
            this.Num9.BackgroundImageLayout = ImageLayout.Stretch;
            this.Num9.Dock = DockStyle.Fill;
            this.Num9.Location = new Point(199, 3);
            this.Num9.Name = "Num9";
            this.Num9.Size = new Size(92, 69);
            this.Num9.TabIndex = 6;
            this.Num9.Tag = (object)"9";
            this.Num9.Paint += new PaintEventHandler(this.Num9_Paint);
            this.CloseKey.BackgroundImage = (Image)componentResourceManager.GetObject("CloseKey.BackgroundImage");
            this.CloseKey.BackgroundImageLayout = ImageLayout.Stretch;
            this.CloseKey.Dock = DockStyle.Fill;
            this.CloseKey.Location = new Point(297, 3);
            this.CloseKey.Name = "CloseKey";
            this.CloseKey.Size = new Size(95, 69);
            this.CloseKey.TabIndex = 1;
            this.CloseKey.Tag = (object)"0";
            this.CloseKey.MouseClick += new MouseEventHandler(this.CloseKey_MouseClick);
            this.Num5.BackgroundImage = (Image)componentResourceManager.GetObject("Num5.BackgroundImage");
            this.Num5.BackgroundImageLayout = ImageLayout.Stretch;
            this.Num5.Dock = DockStyle.Fill;
            this.Num5.Location = new Point(101, 78);
            this.Num5.Name = "Num5";
            this.Num5.Size = new Size(92, 69);
            this.Num5.TabIndex = 9;
            this.Num5.Tag = (object)"5";
            this.Num6.BackgroundImage = (Image)componentResourceManager.GetObject("Num6.BackgroundImage");
            this.Num6.BackgroundImageLayout = ImageLayout.Stretch;
            this.Num6.Dock = DockStyle.Fill;
            this.Num6.Location = new Point(199, 78);
            this.Num6.Name = "Num6";
            this.Num6.Size = new Size(92, 69);
            this.Num6.TabIndex = 10;
            this.Num6.Tag = (object)"6";
           // this.HomeKey.BackgroundImage = (Image)Resources.home;
            this.HomeKey.BackgroundImageLayout = ImageLayout.Stretch;
            this.HomeKey.Dock = DockStyle.Fill;
            this.HomeKey.Location = new Point(297, 78);
            this.HomeKey.Name = "HomeKey";
            this.HomeKey.Size = new Size(95, 69);
            this.HomeKey.TabIndex = 11;
            this.HomeKey.Tag = (object)"154";
            this.Num1.BackgroundImage = (Image)componentResourceManager.GetObject("Num1.BackgroundImage");
            this.Num1.BackgroundImageLayout = ImageLayout.Stretch;
            this.Num1.Dock = DockStyle.Fill;
            this.Num1.Location = new Point(3, 153);
            this.Num1.Name = "Num1";
            this.Num1.Size = new Size(92, 69);
            this.Num1.TabIndex = 12;
            this.Num1.Tag = (object)"1";
            this.Num2.BackgroundImage = (Image)componentResourceManager.GetObject("Num2.BackgroundImage");
            this.Num2.BackgroundImageLayout = ImageLayout.Stretch;
            this.Num2.Dock = DockStyle.Fill;
            this.Num2.Location = new Point(101, 153);
            this.Num2.Name = "Num2";
            this.Num2.Size = new Size(92, 69);
            this.Num2.TabIndex = 13;
            this.Num2.Tag = (object)"2";
            this.num3.BackgroundImage = (Image)componentResourceManager.GetObject("num3.BackgroundImage");
            this.num3.BackgroundImageLayout = ImageLayout.Stretch;
            this.num3.Dock = DockStyle.Fill;
            this.num3.Location = new Point(199, 153);
            this.num3.Name = "num3";
            this.num3.Size = new Size(92, 69);
            this.num3.TabIndex = 14;
            this.num3.Tag = (object)"3";
            this.EndKey.BackgroundImage = (Image)componentResourceManager.GetObject("EndKey.BackgroundImage");
            this.EndKey.BackgroundImageLayout = ImageLayout.Stretch;
            this.EndKey.Dock = DockStyle.Fill;
            this.EndKey.Location = new Point(297, 153);
            this.EndKey.Name = "EndKey";
            this.EndKey.Size = new Size(95, 69);
            this.EndKey.TabIndex = 15;
            this.EndKey.Tag = (object)"155";
            this.Num0.BackgroundImage = (Image)componentResourceManager.GetObject("Num0.BackgroundImage");
            this.Num0.BackgroundImageLayout = ImageLayout.Stretch;
            this.Num0.Dock = DockStyle.Fill;
            this.Num0.Location = new Point(3, 228);
            this.Num0.Name = "Num0";
            this.Num0.Size = new Size(92, 71);
            this.Num0.TabIndex = 18;
            this.Num0.Tag = (object)"0";
            this.NumDot.BackgroundImage = (Image)componentResourceManager.GetObject("NumDot.BackgroundImage");
            this.NumDot.BackgroundImageLayout = ImageLayout.Stretch;
            this.NumDot.Dock = DockStyle.Fill;
            this.NumDot.Location = new Point(101, 228);
            this.NumDot.Name = "NumDot";
            this.NumDot.Size = new Size(92, 71);
            this.NumDot.TabIndex = 19;
            this.NumDot.Paint += new PaintEventHandler(this.NumDot_Paint);
            this.NumDot.MouseClick += new MouseEventHandler(this.NumDot_MouseClick);
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.BackColor = Color.White;
            this.BorderStyle = BorderStyle.FixedSingle;
            this.Controls.Add((Control)this.Close);
            this.Controls.Add((Control)this.open);
            this.Controls.Add((Control)this.tlbPanel);
            this.Name = "NumericPad";
            this.Size = new Size(438, 374);
            this.Load += new EventHandler(this.NumericPad_Load);
            this.Resize += new EventHandler(this.NumericPad_Resize);
            this.tlbPanel.ResumeLayout(false);
            this.ResumeLayout(false);
        }
        #endregion
    }
}
