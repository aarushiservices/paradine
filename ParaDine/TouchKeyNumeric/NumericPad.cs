﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TouchKeyNumeric
{
    public partial class NumericPad : UserControl
    {
        //private IContainer components = (IContainer)null;
        public string KeyPressed = "";
        private TableLayoutPanel tlbPanel;
        private Panel EndKey;
        private Panel num3;
        private Panel Num2;
        private Panel Num1;
        private Panel HomeKey;
        private Panel Num6;
        private Panel Num5;
        private Panel Num4;
        private Panel Num9;
        private Panel Num8;
        private Panel Num7;
        private Panel CloseKey;
        private Panel NumDot;
        private Panel Num0;
        private Panel EnterKey;
        private ImageList imglst;
        private Panel open;
        private Panel Close;
        private Panel backspace;
        public Size GlbSize;
        public Point GlbLocation;

        public NumericPad()
        {
            this.InitializeComponent();
        }       
        private void NumericPad_Resize(object sender, EventArgs e)
        {
            this.Visible = false;
            this.tlbPanel.Width = this.Width - 2;
            this.tlbPanel.Height = this.Height - 2;
            this.Visible = true;
        }

        private void NumericPad_Load(object sender, EventArgs e)
        {
            this.Num0.MouseClick += new MouseEventHandler(this.Clicked);
            this.Num1.MouseClick += new MouseEventHandler(this.Clicked);
            this.Num2.MouseClick += new MouseEventHandler(this.Clicked);
            this.num3.MouseClick += new MouseEventHandler(this.Clicked);
            this.Num4.MouseClick += new MouseEventHandler(this.Clicked);
            this.Num5.MouseClick += new MouseEventHandler(this.Clicked);
            this.Num6.MouseClick += new MouseEventHandler(this.Clicked);
            this.Num7.MouseClick += new MouseEventHandler(this.Clicked);
            this.Num8.MouseClick += new MouseEventHandler(this.Clicked);
            this.Num9.MouseClick += new MouseEventHandler(this.Clicked);
            this.EnterKey.MouseClick += new MouseEventHandler(this.Clicked);
            this.EndKey.MouseClick += new MouseEventHandler(this.Clicked);
            this.HomeKey.MouseClick += new MouseEventHandler(this.Clicked);
            this.CloseKey_MouseClick(new object(), new MouseEventArgs(MouseButtons.Left, 0, 0, 0, 0));
        }
        private void Clicked(object sender, MouseEventArgs e)
        {
            switch (Convert.ToInt32(((Control)sender).Tag.ToString()))
            {
                case 13:
                    this.KeyPressed = "{Enter}";
                    break;
                case 154:
                    this.KeyPressed = "{Home}";
                    break;
                case 155:
                    this.KeyPressed = "{End}";
                    break;
                default:
                    this.KeyPressed = ((Control)sender).Tag.ToString();
                    break;
            }
            SendKeys.Send(this.KeyPressed);
        }

        private void CloseKey_MouseClick(object sender, MouseEventArgs e)
        {
            if ((int)Convert.ToInt16(this.CloseKey.Tag) == 0)
            {
                this.tlbPanel.Visible = false;
                this.Visible = false;
                this.GlbLocation = this.Location;
                this.GlbSize = this.Size;
                int x = this.Location.X + this.CloseKey.Location.X;
                Point location = this.Location;
                int y1 = location.Y;
                location = this.NumDot.Location;
                int y2 = location.Y;
                int y3 = y1 + y2;
                this.Location = new Point(x, y3);
                this.tlbPanel.Controls.Clear();
                this.Size = this.CloseKey.Size;
                this.tlbPanel.RowCount = 1;
                this.tlbPanel.ColumnCount = 1;
                ((Control.ControlCollection)this.tlbPanel.Controls).Add((Control)this.CloseKey);
                this.CloseKey.Tag = (object)1;
                this.tlbPanel.Visible = true;
                this.CloseKey.BackgroundImage = this.open.BackgroundImage;
                this.Visible = true;
                this.BringToFront();
            }
            else
            {
                this.Visible = false;
                this.tlbPanel.Visible = false;
                this.tlbPanel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                this.tlbPanel.BackColor = Color.White;
                this.tlbPanel.ColumnCount = 4;
                this.tlbPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25f));
                this.tlbPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25f));
                this.tlbPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25f));
                this.tlbPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25f));
                this.tlbPanel.Controls.Add((Control)this.Num4, 0, 1);
                this.tlbPanel.Controls.Add((Control)this.Num7, 0, 0);
                this.tlbPanel.Controls.Add((Control)this.Num8, 1, 0);
                this.tlbPanel.Controls.Add((Control)this.Num9, 2, 0);
                this.tlbPanel.Controls.Add((Control)this.CloseKey, 3, 0);
                this.tlbPanel.Controls.Add((Control)this.Num5, 1, 1);
                this.tlbPanel.Controls.Add((Control)this.Num6, 2, 1);
                this.tlbPanel.Controls.Add((Control)this.HomeKey, 3, 1);
                this.tlbPanel.Controls.Add((Control)this.Num1, 0, 2);
                this.tlbPanel.Controls.Add((Control)this.Num2, 1, 2);
                this.tlbPanel.Controls.Add((Control)this.num3, 2, 2);
                this.tlbPanel.Controls.Add((Control)this.EndKey, 3, 2);
                this.tlbPanel.Controls.Add((Control)this.Num0, 0, 3);
                this.tlbPanel.Controls.Add((Control)this.NumDot, 1, 3);
                this.tlbPanel.Controls.Add((Control)this.backspace, 2, 3);
                this.tlbPanel.Controls.Add((Control)this.EnterKey, 3, 3);
                this.tlbPanel.Location = new Point(0, 0);
                this.tlbPanel.Margin = new Padding(0);
                this.tlbPanel.RowCount = 4;
                this.tlbPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 20f));
                this.tlbPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 20f));
                this.tlbPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 20f));
                this.tlbPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 20f));
                this.tlbPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 20f));
                this.CloseKey.BackgroundImage = this.Close.BackgroundImage;
                this.Size = this.GlbSize;
                this.Location = this.GlbLocation;
                this.Visible = true;
                this.tlbPanel.Visible = true;
                this.CloseKey.Tag = (object)0;
                this.BringToFront();
            }
        }

        private void NumDot_Paint(object sender, PaintEventArgs e)
        {
        }

        private void NumDot_MouseClick(object sender, MouseEventArgs e)
        {
            SendKeys.Send(".");
        }

        private void backspace_Paint(object sender, PaintEventArgs e)
        {
        }

        private void backspace_MouseClick(object sender, MouseEventArgs e)
        {
            SendKeys.Send("{BACKSPACE}");
        }

        private void Num9_Paint(object sender, PaintEventArgs e)
        {
        }
    }
}
