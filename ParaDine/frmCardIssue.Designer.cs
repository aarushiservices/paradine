﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine
{
    partial class frmCardIssue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new Container();
            this.pnlMain = new Panel();
            this.label7 = new Label();
            this.dtpCardExpDate = new DateTimePicker();
            this.nmCreditAmount = new NumControl();
            this.nmFaceValue = new NumControl();
            this.nmCardAmount = new NumControl();
            this.label6 = new Label();
            this.txtCardNumber = new TextBox();
            this.label5 = new Label();
            this.cmbCardType = new ComboBox();
            this.label4 = new Label();
            this.txtMobileNo = new TextBox();
            this.label3 = new Label();
            this.txtCustomer = new TextBox();
            this.label2 = new Label();
            this.dtpCardIssueDate = new DateTimePicker();
            this.label1 = new Label();
            this.txtIssueNo = new TextBox();
            this.btnExit = new ParButton();
            this.btnTrans = new ParButton();
            this.TTip = new ToolTip(this.components);
            this.pnlMain.SuspendLayout();
            base.SuspendLayout();
            this.pnlMain.BackColor = Color.Transparent;
            this.pnlMain.BorderStyle = BorderStyle.FixedSingle;
            this.pnlMain.Controls.Add(this.label7);
            this.pnlMain.Controls.Add(this.dtpCardExpDate);
            this.pnlMain.Controls.Add(this.nmCreditAmount);
            this.pnlMain.Controls.Add(this.nmFaceValue);
            this.pnlMain.Controls.Add(this.nmCardAmount);
            this.pnlMain.Controls.Add(this.label6);
            this.pnlMain.Controls.Add(this.txtCardNumber);
            this.pnlMain.Controls.Add(this.label5);
            this.pnlMain.Controls.Add(this.cmbCardType);
            this.pnlMain.Controls.Add(this.label4);
            this.pnlMain.Controls.Add(this.txtMobileNo);
            this.pnlMain.Controls.Add(this.label3);
            this.pnlMain.Controls.Add(this.txtCustomer);
            this.pnlMain.Controls.Add(this.label2);
            this.pnlMain.Controls.Add(this.dtpCardIssueDate);
            this.pnlMain.Controls.Add(this.label1);
            this.pnlMain.Controls.Add(this.txtIssueNo);
            this.pnlMain.Controls.Add(this.btnExit);
            this.pnlMain.Controls.Add(this.btnTrans);
            this.pnlMain.Dock = DockStyle.Fill;
            this.pnlMain.Location = new Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new Size(0x1db, 0x12e);
            this.pnlMain.TabIndex = 0;
            this.label7.AutoSize = true;
            this.label7.Location = new Point(290, 8);
            this.label7.Name = "label7";
            this.label7.Size = new Size(0x54, 13);
            this.label7.TabIndex = 0x1d;
            this.label7.Text = "Card Expired On";
            this.dtpCardExpDate.CustomFormat = "dd/MMMM/yyyy";
            this.dtpCardExpDate.Enabled = false;
            this.dtpCardExpDate.Format = DateTimePickerFormat.Custom;
            this.dtpCardExpDate.Location = new Point(290, 0x18);
            this.dtpCardExpDate.Name = "dtpCardExpDate";
            this.dtpCardExpDate.Size = new Size(0x91, 20);
            this.dtpCardExpDate.TabIndex = 0x1c;
            this.dtpCardExpDate.TabStop = false;
            this.nmCreditAmount.BackColor = Color.White;
            this.nmCreditAmount.BorderStyle = BorderStyle.FixedSingle;
            this.nmCreditAmount.DecimalRequired = true;
            this.nmCreditAmount.Font = new Font("Rupee Foradian", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.nmCreditAmount.Location = new Point(0x158, 0x89);
            this.nmCreditAmount.Name = "nmCreditAmount";
            this.nmCreditAmount.ReadOnly = true;
            this.nmCreditAmount.Size = new Size(0x5b, 0x1a);
            this.nmCreditAmount.SymbolRequired = true;
            this.nmCreditAmount.TabIndex = 5;
            this.nmCreditAmount.TabStop = false;
            this.nmCreditAmount.Text = "`0.00";
            this.nmCreditAmount.TextAlign = HorizontalAlignment.Right;
            int[] bits = new int[4];
            this.nmCreditAmount.Value = new decimal(bits);
            this.nmFaceValue.BackColor = Color.White;
            this.nmFaceValue.BorderStyle = BorderStyle.FixedSingle;
            this.nmFaceValue.DecimalRequired = true;
            this.nmFaceValue.Font = new Font("Rupee Foradian", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.nmFaceValue.Location = new Point(0x15a, 0x58);
            this.nmFaceValue.Name = "nmFaceValue";
            this.nmFaceValue.ReadOnly = true;
            this.nmFaceValue.Size = new Size(0x59, 0x1a);
            this.nmFaceValue.SymbolRequired = true;
            this.nmFaceValue.TabIndex = 2;
            this.nmFaceValue.TabStop = false;
            this.nmFaceValue.Text = "`0.00";
            this.nmFaceValue.TextAlign = HorizontalAlignment.Right;
            //bits = new int[4];
            this.nmFaceValue.Value = new decimal(bits);
            this.nmCardAmount.BackColor = Color.White;
            this.nmCardAmount.BorderStyle = BorderStyle.FixedSingle;
            this.nmCardAmount.DecimalRequired = true;
            this.nmCardAmount.Font = new Font("Rupee Foradian", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.nmCardAmount.Location = new Point(0xf9, 0x58);
            this.nmCardAmount.Name = "nmCardAmount";
            this.nmCardAmount.ReadOnly = true;
            this.nmCardAmount.Size = new Size(0x59, 0x1a);
            this.nmCardAmount.SymbolRequired = true;
            this.nmCardAmount.TabIndex = 1;
            this.nmCardAmount.TabStop = false;
            this.nmCardAmount.Text = "`0.00";
            this.nmCardAmount.TextAlign = HorizontalAlignment.Right;
            //bits = new int[4];
            this.nmCardAmount.Value = new decimal(bits);
            this.label6.AutoSize = true;
            this.label6.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label6.Location = new Point(0x23, 0x79);
            this.label6.Name = "label6";
            this.label6.Size = new Size(80, 13);
            this.label6.TabIndex = 0x18;
            this.label6.Text = "Card Number";
            this.txtCardNumber.BackColor = Color.Ivory;
            this.txtCardNumber.BorderStyle = BorderStyle.FixedSingle;
            this.txtCardNumber.Font = new Font("Arial", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.txtCardNumber.Location = new Point(0x25, 0x89);
            this.txtCardNumber.Name = "txtCardNumber";
            this.txtCardNumber.PasswordChar = '*';
            this.txtCardNumber.Size = new Size(0x12d, 0x1a);
            this.txtCardNumber.TabIndex = 4;
            this.TTip.SetToolTip(this.txtCardNumber, "Please Enter Card Number");
            this.txtCardNumber.Validating += new CancelEventHandler(this.txtCardNumber_Validating);
            this.label5.AutoSize = true;
            this.label5.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label5.Location = new Point(0x22, 0x48);
            this.label5.Name = "label5";
            this.label5.Size = new Size(0x69, 13);
            this.label5.TabIndex = 0x16;
            this.label5.Text = "Select Card Type";
            this.cmbCardType.BackColor = Color.Ivory;
            this.cmbCardType.Font = new Font("Verdana", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.cmbCardType.FormattingEnabled = true;
            this.cmbCardType.Location = new Point(0x25, 0x58);
            this.cmbCardType.Name = "cmbCardType";
            this.cmbCardType.Size = new Size(0xcc, 0x1a);
            this.cmbCardType.TabIndex = 0;
            this.TTip.SetToolTip(this.cmbCardType, "Please Select Card Type");
            this.cmbCardType.SelectionChangeCommitted += new EventHandler(this.cmbCardType_SelectionChangeCommitted);
            this.label4.AutoSize = true;
            this.label4.Location = new Point(0xf6, 0xb9);
            this.label4.Name = "label4";
            this.label4.Size = new Size(0x37, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Mobile No";
            this.txtMobileNo.BackColor = Color.White;
            this.txtMobileNo.BorderStyle = BorderStyle.FixedSingle;
            this.txtMobileNo.Location = new Point(0xf9, 0xc9);
            this.txtMobileNo.Name = "txtMobileNo";
            this.txtMobileNo.Size = new Size(0xba, 20);
            this.txtMobileNo.TabIndex = 7;
            this.label3.AutoSize = true;
            this.label3.Location = new Point(0x25, 0xb9);
            this.label3.Name = "label3";
            this.label3.Size = new Size(0x33, 13);
            this.label3.TabIndex = 0x12;
            this.label3.Text = "Customer";
            this.txtCustomer.BackColor = Color.White;
            this.txtCustomer.BorderStyle = BorderStyle.FixedSingle;
            this.txtCustomer.Location = new Point(0x25, 0xc9);
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.Size = new Size(0xcc, 20);
            this.txtCustomer.TabIndex = 6;
            this.label2.AutoSize = true;
            this.label2.Location = new Point(0x8b, 8);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0x53, 13);
            this.label2.TabIndex = 0x10;
            this.label2.Text = "Card Issue Date";
            this.dtpCardIssueDate.CustomFormat = "dd/MMMM/yyyy";
            this.dtpCardIssueDate.Enabled = false;
            this.dtpCardIssueDate.Format = DateTimePickerFormat.Custom;
            this.dtpCardIssueDate.Location = new Point(0x8b, 0x18);
            this.dtpCardIssueDate.Name = "dtpCardIssueDate";
            this.dtpCardIssueDate.Size = new Size(0x91, 20);
            this.dtpCardIssueDate.TabIndex = 15;
            this.dtpCardIssueDate.TabStop = false;
            this.label1.AutoSize = true;
            this.label1.Location = new Point(0x25, 8);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x4a, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Card Issue No";
            this.txtIssueNo.BorderStyle = BorderStyle.FixedSingle;
            this.txtIssueNo.Enabled = false;
            this.txtIssueNo.Location = new Point(0x25, 0x18);
            this.txtIssueNo.Name = "txtIssueNo";
            this.txtIssueNo.Size = new Size(0x60, 20);
            this.txtIssueNo.TabIndex = 13;
            this.txtIssueNo.TabStop = false;
            this.btnExit.BackColor = Color.Teal;
            this.btnExit.BackgroundImageLayout = ImageLayout.Stretch;
            this.btnExit.CausesValidation = false;
            //this.btnExit.DialogResult=DialogResult.Cancel;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = FlatStyle.Flat;
            this.btnExit.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnExit.Location = new Point(0x146, 0xf3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new Size(0x6d, 0x1b);
            this.btnExit.TabIndex = 12;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new EventHandler(this.btnExit_Click);
            this.btnTrans.BackColor = Color.Teal;
            this.btnTrans.BackgroundImageLayout = ImageLayout.Stretch;
            this.btnTrans.FlatAppearance.BorderSize = 0;
            this.btnTrans.FlatStyle = FlatStyle.Flat;
            this.btnTrans.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnTrans.Location = new Point(0xca, 0xf3);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new Size(0x6d, 0x1b);
            this.btnTrans.TabIndex = 11;
            this.btnTrans.Text = "Trans";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new EventHandler(this.btnTrans_Click);
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            //           base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x1db, 0x12e);
            base.Controls.Add(this.pnlMain);
            base.Name = "frmCardIssue";
            base.ShowIcon = false;
            base.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "Card Issue";
            base.Load += new EventHandler(this.frmCardIssue_Load);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            base.ResumeLayout(false);
        }
        #endregion
    }
}