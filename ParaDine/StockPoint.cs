// Generated by .NET Reflector from D:\Paradine\ParaDine\ParaDine.exe
namespace ParaDine
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    
    public class StockPoint : MasterBase
    {
        public short BillFormat;
        public bool IsBillPoint;
        public string Remarks;
        public bool ServiceTaxApplicable;
        public long StockPointId;
        public string StockPointName;
        
        public StockPoint()
        {
            this.BillFormat = 0;
        }
        
        public StockPoint(long ID)
        {
            this.BillFormat = 0;
            this.StockPointId = ID;
            this.LoadAttributes(ID);
        }
        
        public override long GetExecuteCommand(string TransType, SqlTransaction SqlTrans)
        {
            base.SqlCmd = new SqlCommand();
            base.SqlCmd.Connection = GlobalVariables.SqlConn;
            base.SqlCmd.Transaction = SqlTrans;
            base.SqlCmd.CommandText = "RES_PROC_STOCKPOINT";
            base.SqlCmd.CommandType = CommandType.StoredProcedure;
            base.SqlCmd.Parameters.AddWithValue("@TRANSTYPE", TransType);
            base.SqlCmd.Parameters.AddWithValue("@STOCKPOINTID", this.StockPointId);
            base.SqlCmd.Parameters.AddWithValue("@STOCKPOINTNAME", this.StockPointName);
            base.SqlCmd.Parameters.AddWithValue("@ISBILLPOINT", this.IsBillPoint);
            base.SqlCmd.Parameters.AddWithValue("@REMARKS", this.Remarks);
            base.SqlCmd.Parameters.AddWithValue("@SERVICETAXAPPLICABLE", this.ServiceTaxApplicable);
            SqlParameter parameter = base.SqlCmd.Parameters["@STOCKPOINTID"];
            parameter.Direction = ParameterDirection.InputOutput;
            if (base.SqlCmd.ExecuteNonQuery() > 0)
            {
                base.InsertLogDetails("StockPoint", Convert.ToInt64(parameter.Value), GlobalVariables.UserID, GlobalVariables.BusinessDate, TransType, this.StockPointName, SqlTrans);
                return (long) Convert.ToInt32(parameter.Value);
            }
            return 0L;
        }
        
        public override string GetViewQuery(string condStr)
        {
            base.StrSql = "SELECT * FROM RES_STOCKPOINT";
            if (condStr.Trim() != "")
            {
                base.StrSql = base.StrSql + " WHERE " + condStr;
            }
            return base.StrSql;
        }
        
        public override void LoadAttributes(long ID)
        {
            base.StrSql = "SELECT * FROM RES_STOCKPOINT WHERE STOCKPOINTID = " + ID;
            base.SqlCmd = new SqlCommand(base.StrSql, GlobalVariables.SqlConn);
            SqlDataReader reader = base.SqlCmd.ExecuteReader();
            if (reader.Read())
            {
                this.StockPointId = Convert.ToInt32(reader["StockPointID"]);
                this.StockPointName = Convert.ToString(reader["StockPointName"]);
                this.IsBillPoint = Convert.ToBoolean(reader["IsBillPoint"]);
                this.Remarks = Convert.ToString(reader["REMARKS"]);
                this.ServiceTaxApplicable = Convert.ToBoolean(reader["SERVICETAXAPPLICABLE"]);
                this.BillFormat = Convert.ToInt16(reader["BILLFORMAT"]);
            }
            reader.Close();
        }
    }
}
