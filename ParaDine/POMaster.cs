// Generated by .NET Reflector from D:\Paradine\ParaDine\ParaDine.exe
namespace ParaDine
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    
    public class POMaster : MasterBase
    {
        public int CompanyID;
        public DateTime DeliveryDate;
        public DateTime PODate;
        public long POMasterID;
        public string PONumber;
        public ParaDine.POTransCollection POTransCollection;
        public long SupplierID;
        
        public POMaster(long ID)
        {
            this.PODate = GlobalVariables.BusinessDate;
            this.DeliveryDate = GlobalVariables.BusinessDate.AddDays(1.0);
            this.POTransCollection = new ParaDine.POTransCollection();
            this.POMasterID = ID;
            this.LoadAttributes(ID);
        }
        
        public POMaster(string StrPONo)
        {
            this.PODate = GlobalVariables.BusinessDate;
            this.DeliveryDate = GlobalVariables.BusinessDate.AddDays(1.0);
            this.POTransCollection = new ParaDine.POTransCollection();
            base.StrSql = "SELECT POMASTERID FROM RES_POMASTER WHERE PONUMBER = '" + StrPONo + "'";
            base.SqlCmd = new SqlCommand(base.StrSql, GlobalVariables.SqlConn);
            this.LoadAttributes(Convert.ToInt64(base.SqlCmd.ExecuteScalar()));
        }
        
        public override long GetExecuteCommand(string TransType, SqlTransaction SqlTrans)
        {
            if (TransType == "DELETE")
            {
                foreach (POTrans trans in this.POTransCollection)
                {
                    trans.Delete(SqlTrans, false);
                }
            }
            base.SqlCmd = new SqlCommand();
            base.SqlCmd.Connection = GlobalVariables.SqlConn;
            base.SqlCmd.Transaction = SqlTrans;
            base.SqlCmd.CommandText = "RES_PROC_POMASTER";
            base.SqlCmd.CommandType = CommandType.StoredProcedure;
            base.SqlCmd.Parameters.AddWithValue("@TRANSTYPE", TransType);
            base.SqlCmd.Parameters.AddWithValue("@POMASTERID", this.POMasterID);
            base.SqlCmd.Parameters.AddWithValue("@PONUMBER", this.PONumber);
            base.SqlCmd.Parameters.AddWithValue("@PODATE", this.PODate);
            base.SqlCmd.Parameters.AddWithValue("@SUPPLIERID", this.SupplierID);
            base.SqlCmd.Parameters.AddWithValue("@DELIVERYDATE", this.DeliveryDate);
            base.SqlCmd.Parameters.AddWithValue("@COMPANYID", this.CompanyID);
            SqlParameter parameter = base.SqlCmd.Parameters.Add("RetVal", SqlDbType.BigInt);
            parameter.Direction = ParameterDirection.ReturnValue;
            if (base.SqlCmd.ExecuteNonQuery() > 0)
            {
                this.POMasterID = Convert.ToInt64(parameter.Value);
                foreach (POTrans trans in this.POTransCollection)
                {
                    trans.POMasterID = this.POMasterID;
                    if (trans.Deleted)
                    {
                        trans.Delete(SqlTrans, false);
                    }
                    else if (trans.Added)
                    {
                        trans.Add(SqlTrans, false);
                    }
                    else
                    {
                        trans.Modify(SqlTrans, false);
                    }
                }
                return Convert.ToInt64(parameter.Value);
            }
            return 0L;
        }
        
        public override string GetViewQuery(string condStr)
        {
            base.StrSql = "SELECT * FROM RES_VW_POMASTER";
            if (condStr.Trim() != "")
            {
                base.StrSql = base.StrSql + " WHERE " + condStr;
            }
            return base.StrSql;
        }
        
        public override void LoadAttributes(long ID)
        {
            base.StrSql = "SELECT * FROM RES_POMASTER WHERE POMASTERID = " + ID;
            base.SqlCmd = new SqlCommand(base.StrSql, GlobalVariables.SqlConn);
            SqlDataReader reader = base.SqlCmd.ExecuteReader();
            if (reader.Read())
            {
                this.POMasterID = Convert.ToInt64(reader["POMASTERID"]);
                this.PONumber = Convert.ToString(reader["PONUMBER"]);
                this.PODate = Convert.ToDateTime(reader["PODATE"]);
                this.SupplierID = Convert.ToInt64(reader["SUPPLIERID"]);
                this.DeliveryDate = Convert.ToDateTime(reader["DELIVERYDATE"]);
                this.CompanyID = Convert.ToInt32(reader["COMPANYID"]);
            }
            reader.Close();
            this.LoadPOTrans();
        }
        
        private void LoadPOTrans()
        {
            base.StrSql = " SELECT POTRANSID FROM RES_POTRANS WHERE POMASTERID = " + this.POMasterID;
            GlobalFill.FillDataSet(base.StrSql, "POTRANS", base.DS, base.SDA);
            if (base.DS.Tables["POTRANS"].Rows.Count > 0)
            {
                foreach (DataRow row in base.DS.Tables["POTRANS"].Rows)
                {
                    POTrans varEntityPOTrans = new POTrans(Convert.ToInt64(row["POTRANSID"]));
                    this.POTransCollection.Add(varEntityPOTrans);
                }
            }
        }
        
        public string GetMaxCode
        {
            get
            {
                base.StrSql = "SELECT DBO.SUP_FUNC_PADDING(CONVERT(VARCHAR, ISNULL(MAX(CONVERT(INT, PONUMBER)),0) + 1), 4, '0') FROM RES_POMASTER";
                base.SqlCmd = new SqlCommand(base.StrSql, GlobalVariables.SqlConn);
                return Convert.ToString(base.SqlCmd.ExecuteScalar());
            }
        }
    }
}
