﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine
{
    public partial class clsNumControl : TextBox
    {
        private void InitializeComponent()
        {
            base.SuspendLayout();
            base.ResumeLayout(false);
        }

        protected override void OnCreateControl()
        {
            base.OnCreateControl();
            base.Text = "0.00";
            base.TextAlign = HorizontalAlignment.Right;
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            if ((e.KeyChar == '.') && base.Text.Contains("."))
            {
                e.Handled = true;
            }
            else
            {
                this.ValidateKeys(e, false, true, false, ".");
            }
        }

        protected override void OnValidated(EventArgs e)
        {
            base.OnValidated(e);
            base.Text = string.Format("{0:#,##0.00}", Convert.ToDouble(base.Text));
        }

        private void ValidateKeys(KeyPressEventArgs paramEvent, bool AllowAlphabets, bool AllowNumerics, bool AllowSpecialChars, string OptionalString)
        {
            if ((OptionalString.Trim().Length > 0) && OptionalString.Contains(paramEvent.KeyChar.ToString()))
            {
                paramEvent.Handled = false;
            }
            else if (!(AllowAlphabets || (((paramEvent.KeyChar < 'A') || (paramEvent.KeyChar > 'Z')) && ((paramEvent.KeyChar < 'a') || (paramEvent.KeyChar > 'z')))))
            {
                paramEvent.Handled = true;
            }
            else if (!(AllowNumerics || ((paramEvent.KeyChar < '0') || (paramEvent.KeyChar > '9'))))
            {
                paramEvent.Handled = true;
            }
            else if (!(AllowSpecialChars || (((((paramEvent.KeyChar < '!') || (paramEvent.KeyChar > '/')) && ((paramEvent.KeyChar < ':') || (paramEvent.KeyChar > '@'))) && ((paramEvent.KeyChar < '[') || (paramEvent.KeyChar > '`'))) && ((paramEvent.KeyChar < '{') || (paramEvent.KeyChar > '~')))))
            {
                paramEvent.Handled = true;
            }
            else
            {
                paramEvent.Handled = false;
            }
        }
    }
}
