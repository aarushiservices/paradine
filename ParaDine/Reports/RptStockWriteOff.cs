﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Reports
{
    public partial class RptStockWriteOff : Form
    {
        DataTable dt = new DataTable();
        public RptStockWriteOff(DataTable dtp)
        {
            InitializeComponent();
            dt = dtp;
        }

        private void RptStockWriteOff_Load(object sender, EventArgs e)
        {

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(datasource);
            reportViewer1.RefreshReport();
        }
    }
}
