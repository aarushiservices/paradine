﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Reports
{
    public partial class RptWHItemHistory : Form
    {
        DataTable dt = new DataTable();
        public RptWHItemHistory( DataTable dtp)
        {
            InitializeComponent();
            dt = dtp;
        }

        public string Parameter_Heading
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("Heading", value, true));
            }
        }
        public string Parameter_StartDate
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("StartDate", value, true));
            }
        }

        public string Parameter_EndDate
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("EndDate", value, true));
            }
        }
        public string Parameter_Parameter
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("Parameter", value, true));
            }
        }
        public string Parameter_OPQTY
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("OPQTY", value, true));
            }
        }
        public string Parameter_CLQTY
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("CLQTY", value, true));
            }
        }

        private void RptWHItemHistory_Load(object sender, EventArgs e)
        {
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(datasource);
            reportViewer1.RefreshReport();
        }

        private void RptWHItemHistory_Load_1(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }
}
