﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Reports
{
    public partial class RptStockreturnItemsummary1 : Form
    {
        DataTable dt = new DataTable();
        public RptStockreturnItemsummary1(DataTable dtp)
        {
            InitializeComponent();
            dt = dtp;
        }
        public string Parameter_parameter
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("parameter", value, true));
            }
        }
        private void RptStockreturnItemsummary1_Load(object sender, EventArgs e)
        {

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(datasource);
            reportViewer1.RefreshReport();
        }
    }
}
