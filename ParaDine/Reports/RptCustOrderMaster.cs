﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Reports
{
    public partial class RptCustOrderMaster : Form
    {
        DataTable dt = new DataTable();
        public RptCustOrderMaster(DataTable dtp)
        {
            InitializeComponent();
            dt = dtp;
        }

        public string Parameter_CompanyName
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("CompanyName", value, true));
            }
        }

        public string Parameter_Address
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("Address", value, true));
            }
        }

        public string Parameter_RestaurantName
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("RestaurantName", value, true));
            }
        }

        public string Parameter_parameter
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("parameter", value, true));
            }
        }

        public string Parameter_Status
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("Status", value, true));
            }
        }      

        private void RptCustOrderMaster_Load(object sender, EventArgs e)
        {

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(datasource);
            reportViewer1.RefreshReport();
        }
        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}
