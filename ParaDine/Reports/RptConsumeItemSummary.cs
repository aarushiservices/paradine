﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Reports
{
    public partial class RptConsumeItemSummary : Form
    {
      
        static DataTable dt = new DataTable();

        private void RptConsumeItemSummary_Load(object sender, EventArgs e)
        {
            SetRptBill1();
        }

        public RptConsumeItemSummary(DataTable dttemp)
        {
            InitializeComponent();
            dt = dttemp;
        }
        public string Parameter_Parameter
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("Parameter", value, true));
            }
        }
       

        public void SetRptBill1()
        {
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(datasource);
            reportViewer1.RefreshReport();
        }
    }
}
