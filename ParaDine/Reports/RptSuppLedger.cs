﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Reports
{
    public partial class RptSuppLedger : Form
    {
        DataTable dt = new DataTable();
        public RptSuppLedger( DataTable dtp)
        {
            InitializeComponent();
            dt = dtp;
        }

        public string Parameter_CompName
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("CompName", value, true));
            }
        }

        public string Parameter_Parameter
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("Parameter", value, true));
            }
        }

        public string Parameter_Bal
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("Bal", value, true));
            }
        }
        public string Parameter_ClosingBal
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("ClosingBal", value, true));
            }
        }
        private void RptSuppLedger_Load(object sender, EventArgs e)
        {
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(datasource);
            reportViewer1.RefreshReport();
        }

        private void RptSuppLedger_Load_1(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }
}
