﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Reports
{
    public partial class RptItemProfitSummary : Form
    {
         DataTable dt = new DataTable();
         public RptItemProfitSummary(DataTable dtp)
        {
            InitializeComponent();
            dt = dtp;
        }
         public string Parameter_CompanyName
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("CompanyName", value, true));
            }
        }   
        public string Parameter_Parameter
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("Parameter", value, true));
            }
        }
        public string Parameter_Profit
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("Profit", value, true));
            }
        }
        public string Parameter_GrpSumProfit
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("GrpSumProfit", value, true));
            }
        }
        public string Parameter_GrpProfit
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("GrpProfit", value, true));
            }
        }
        public string Parameter_TotSumProfit
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("TotSumProfit", value, true));
            }
        }
        public string Parameter_TotProfit
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("TotProfit", value, true));
            }
        }
        public string Parameter_Profitpercentage
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("Profitpercentage", value, true));
            }
        }
        private void RptItemProfitSummary_Load(object sender, EventArgs e)
        {
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(datasource);
            reportViewer1.RefreshReport();
        }
        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}
