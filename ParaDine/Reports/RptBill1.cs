﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Reports
{
    public partial class RptBill1 : Form
    {
        DataTable dt = new DataTable();
        public string Parameter_BillHeader2
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("BillHeader2", value, true));
            }
        }
        public string Parameter_BillHeader3
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("BillHeader3", value, true));
            }
        }

        public string Parameter_BillHeader4
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("BillHeader4", value, true));
            }
        }
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        //public IParameterField Parameter_BillHeader1
        //{
        //    get
        //    {
        //        return this.DataDefinition.ParameterFields[0];
        //    }
        //}

        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        //public IParameterField Parameter_BillHeader2
        //{
        //    get
        //    {
        //        return this.DataDefinition.ParameterFields[1];
        //    }
        //}

        //[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public IParameterField Parameter_BillHeader3
        //{
        //    get
        //    {
        //        return this.DataDefinition.ParameterFields[2];
        //    }
        //}

        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        //public IParameterField Parameter_BillHeader4
        //{
        //    get
        //    {
        //        return this.DataDefinition.ParameterFields[3];
        //    }
        //}

        //public override string ResourceName
        //{
        //    get
        //    {
        //        return "RptBill1.rpt";
        //    }
        //    set
        //    {
        //    }
        //}

        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        //public Section Section1
        //{
        //    get
        //    {
        //        return this.ReportDefinition.Sections[0];
        //    }
        //}

        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        //public Section Section2
        //{
        //    get
        //    {
        //        return this.ReportDefinition.Sections[1];
        //    }
        //}

        //[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public Section Section3
        //{
        //    get
        //    {
        //        return this.ReportDefinition.Sections[2];
        //    }
        //}

        //[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public Section Section4
        //{
        //    get
        //    {
        //        return this.ReportDefinition.Sections[3];
        //    }
        //}

        //[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public Section Section5
        //{
        //    get
        //    {
        //        return this.ReportDefinition.Sections[4];
        //    }
        //}

       // static DataTable dt = new DataTable();

        private void RptBill1_Load(object sender, EventArgs e)
        {
            //System.Drawing.Printing.PageSettings pg = new System.Drawing.Printing.PageSettings();
            //pg.Margins.Top = 0;
            //pg.Margins.Bottom = 0;
            //pg.Margins.Left = 0;
            //pg.Margins.Right = 0;
            //pg.Landscape = false;
            //System.Drawing.Printing.PaperSize size = new System.Drawing.Printing.PaperSize();
            //size.RawKind = (int)System.Drawing.Printing.PaperKind.Custom;
            //pg.PaperSize = size;
            System.Drawing.Printing.PageSettings rptpg = reportViewer1.GetPageSettings();
            rptpg.Margins.Top = 0;
            rptpg.Margins.Bottom = 0;
            rptpg.Margins.Left = 0;
            rptpg.Margins.Right = 0;
            this.reportViewer1.SetPageSettings(rptpg);
            SetRptBill1();
        }

        public RptBill1(DataTable dttemp)
        {
            InitializeComponent();
            dt = dttemp;
        }

        public void SetRptBill1()
        {
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(datasource);
            reportViewer1.RefreshReport();
        }

        //public void SetDataSource(DataTable dt)
        //{
             
        //}
    }
}
