﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ParaDine.Reports
{
    public partial class RptBillView : Form
    {

        static DataTable dt = new DataTable();

        private void RptBillView_Load(object sender, EventArgs e)
        {
            SetRptBillView();
        }
        public RptBillView(DataTable dttemp)
        {
            InitializeComponent();
            dt = dttemp;
        }
        public void SetRptBillView()
        {
            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(datasource);
            reportViewer1.RefreshReport();
        }

}
}
      



