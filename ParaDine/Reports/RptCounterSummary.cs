﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Reports
{
    public partial class RptCounterSummary : Form
    {
        DataTable dt = new DataTable();
        public RptCounterSummary(DataTable dtp)
        {
            InitializeComponent();
            dt = dtp;
        }

        private void RptCounterSummary_Load(object sender, EventArgs e)
        {
           
        }
        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }

        private void RptCounterSummary_Load_1(object sender, EventArgs e)
        {

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(datasource);
            reportViewer1.RefreshReport();
        }
    }
}
