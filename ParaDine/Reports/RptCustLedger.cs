﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Reports
{
    public partial class RptCustLedger : Form
    {
        DataTable dt = new DataTable();
        public RptCustLedger(DataTable dtp)
        {
            InitializeComponent();
            dt = dtp;
        }

        public string Parameter_Pramstr
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("Pramstr", value, true));
            }
        }

        public string Parameter_bal
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("bal", value, true));
            }
        }

        public string Parameter_ClosingBal
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("ClosingBal", value, true));
            }
        }

        public string Parameter_CompName
        {
            set
            {
                reportViewer1.LocalReport.SetParameters(new ReportParameter("CompName", value, true));
            }
        }        

        private void RptCustLedger_Load(object sender, EventArgs e)
        {

            ReportDataSource datasource = new ReportDataSource("DataSet1", dt);
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(datasource);
            reportViewer1.RefreshReport();
        }
        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}
