// Generated by .NET Reflector from D:\Paradine\ParaDine\ParaDine.exe
namespace ParaDine
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    
    public class StockTrfMaster : TransactionBase
    {
        public long FrStockPointId;
        public long IndentMasterId;
        public string Remarks;
        public StockTrfTransCollection StkTrfTransCollection = new StockTrfTransCollection();
        public DateTime StockTrfDate = GlobalVariables.BusinessDate;
        public long StockTrfMasterId;
        public string StockTrfNumber;
        public long ToStockPointId;
        
        public StockTrfMaster(long ID)
        {
            this.StockTrfMasterId = ID;
            this.LoadAttributes(ID);
        }
        
        public override string GetDateQuery(string condStr)
        {
            base.DateColName = "STOCKTRFDATE";
            base.StrSql = "SELECT DISTINCT  CAST(YEAR(" + base.DateColName + ") AS VARCHAR) +  DBO.SUP_FUNC_PADDING(cast(month(" + base.DateColName + ") as varchar),2,'0'),  COUNT(*) FROM DBO.RES_STOCKTRFMASTER WHERE STOCKTRFMASTERID <> 0 ";
            if (condStr.Trim() != "")
            {
                base.StrSql = base.StrSql + " AND " + condStr;
            }
            string strSql = base.StrSql;
            base.StrSql = strSql + " GROUP BY CAST(YEAR(" + base.DateColName + ") AS VARCHAR) +  DBO.SUP_FUNC_PADDING(cast(month(" + base.DateColName + ") as varchar),2,'0') ORDER BY 1 DESC";
            return base.StrSql;
        }
        
        public override long GetExecuteCommand(string TransType, SqlTransaction SqlTrans)
        {
            if (TransType == "DELETE")
            {
                foreach (StockTrfTrans trans in this.StkTrfTransCollection)
                {
                    trans.Delete(SqlTrans, false);
                    trans.Deleted = true;
                }
            }
            base.SqlCmd = new SqlCommand();
            base.SqlCmd.Connection = GlobalVariables.SqlConn;
            base.SqlCmd.Transaction = SqlTrans;
            base.SqlCmd.CommandText = "RES_PROC_STOCKTRFMASTER";
            base.SqlCmd.CommandType = CommandType.StoredProcedure;
            base.SqlCmd.Parameters.AddWithValue("@TRANSTYPE", TransType);
            base.SqlCmd.Parameters.AddWithValue("@STOCKTRFMASTERID", this.StockTrfMasterId);
            base.SqlCmd.Parameters.AddWithValue("@STOCKTRFNUMBER", this.StockTrfNumber);
            base.SqlCmd.Parameters.AddWithValue("@STOCKTRFDATE", this.StockTrfDate);
            base.SqlCmd.Parameters.AddWithValue("@FRSTOCKPOINTID", this.FrStockPointId);
            base.SqlCmd.Parameters.AddWithValue("@TOSTOCKPOINTID", this.ToStockPointId);
            if (this.IndentMasterId > 0L)
            {
                base.SqlCmd.Parameters.AddWithValue("@INDENTMASTERID", this.IndentMasterId);
            }
            SqlParameter parameter = base.SqlCmd.Parameters["@STOCKTRFMASTERID"];
            parameter.Direction = ParameterDirection.InputOutput;
            if (base.SqlCmd.ExecuteNonQuery() > 0)
            {
                this.StockTrfMasterId = Convert.ToInt64(parameter.Value);
                foreach (StockTrfTrans trans in this.StkTrfTransCollection)
                {
                    trans.StockTrfMasterId = this.StockTrfMasterId;
                    if (trans.Deleted)
                    {
                        trans.Delete(SqlTrans, false);
                    }
                    else if (trans.Added)
                    {
                        trans.Add(SqlTrans, false);
                    }
                    else
                    {
                        trans.Modify(SqlTrans, false);
                    }
                }
                return Convert.ToInt64(parameter.Value);
            }
            return 0L;
        }
        
        public override string GetViewQuery(string condStr)
        {
            base.StrSql = "SELECT STOCKTRFMASTERID, STOCKTRFNUMBER, STOCKTRFDATE, FRSTOCKPOINTNAME, TOSTOCKPOINTNAME, AMOUNT  FROM RES_VW_STOCKTRFMASTER ";
            if (condStr.Trim() != "")
            {
                base.StrSql = base.StrSql + " WHERE " + condStr;
            }
            return base.StrSql;
        }
        
        public override void LoadAttributes(long ID)
        {
            base.StrSql = "SELECT * FROM RES_STOCKTRFMASTER WHERE STOCKTRFMASTERID = " + ID;
            base.SqlCmd = new SqlCommand(base.StrSql, GlobalVariables.SqlConn);
            SqlDataReader reader = base.SqlCmd.ExecuteReader();
            if (reader.Read())
            {
                this.StockTrfMasterId = Convert.ToInt64(reader["STOCKTRFMASTERID"]);
                this.StockTrfNumber = Convert.ToString(reader["STOCKTRFNUMBER"]);
                this.StockTrfDate = Convert.ToDateTime(reader["STOCKTRFDATE"]);
                this.FrStockPointId = Convert.ToInt32(reader["FRSTOCKPOINTID"]);
                this.ToStockPointId = Convert.ToInt32(reader["TOSTOCKPOINTID"]);
                if (reader["INDENTMASTERID"] != DBNull.Value)
                {
                    this.IndentMasterId = Convert.ToInt64(reader["INDENTMASTERID"]);
                }
                this.Remarks = Convert.ToString(reader["REMARKS"]);
            }
            reader.Close();
            this.LoadStockTrfTrans();
        }
        
        public void LoadStockTrfTrans()
        {
            this.StkTrfTransCollection.Clear();
            base.StrSql = "SELECT STOCKTRFTRANSID FROM RES_STOCKTRFTRANS WHERE STOCKTRFMASTERID = " + this.StockTrfMasterId;
            GlobalFill.FillDataSet(base.StrSql, "STOCKTRFTRANS", base.DS, base.SDA);
            if (base.DS.Tables["STOCKTRFTRANS"].Rows.Count > 0)
            {
                foreach (DataRow row in base.DS.Tables["STOCKTRFTRANS"].Rows)
                {
                    StockTrfTrans varStockTrfTrans = new StockTrfTrans(Convert.ToInt64(row["STOCKTRFTRANSID"]));
                    this.StkTrfTransCollection.Add(varStockTrfTrans);
                }
            }
        }      
        public string GetMaxCode
        {
            get
            {
                base.StrSql = "SELECT DBO.SUP_FUNC_PADDING(CONVERT(VARCHAR, ISNULL(MAX(CONVERT(INT, STOCKTRFMASTERID)),0) + 1), 4, '0') FROM RES_STOCKTRFMASTER";
                base.SqlCmd = new SqlCommand(base.StrSql, GlobalVariables.SqlConn);
                return Convert.ToString(base.SqlCmd.ExecuteScalar());
            }
        }
    }
}
