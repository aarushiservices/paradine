// Generated by .NET Reflector from D:\Paradine\ParaDine\ParaDine.exe
namespace ParaDine
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    
    public class Layout : MasterBase
    {
        public int LayoutID;
        public int SourceID;
        public char SourceType;
        public double Xpos;
        public double Ypos;
        
        public Layout()
        {
        }
        
        public Layout(int ID)
        {
            this.LayoutID = ID;
            this.LoadAttributes((long) ID);
        }
        
        ~Layout()
        {
        }
        
        public override long GetExecuteCommand(string TransType, SqlTransaction SqlTrans)
        {
            base.SqlCmd = new SqlCommand();
            base.SqlCmd.Connection = GlobalVariables.SqlConn;
            base.SqlCmd.Transaction = SqlTrans;
            base.SqlCmd.CommandText = "GLB_PROC_LAYOUT";
            base.SqlCmd.CommandType = CommandType.StoredProcedure;
            base.SqlCmd.Parameters.AddWithValue("@TRANSTYPE", TransType);
            base.SqlCmd.Parameters.AddWithValue("@LAYOUTID", this.LayoutID);
            base.SqlCmd.Parameters.AddWithValue("@SOURCEID", this.SourceID);
            base.SqlCmd.Parameters.AddWithValue("@XPOS", this.Xpos);
            base.SqlCmd.Parameters.AddWithValue("@YPOS", this.Ypos);
            base.SqlCmd.Parameters.AddWithValue("@SOURCETYPE", this.SourceType);
            SqlParameter parameter = base.SqlCmd.Parameters.Add("RetVal", SqlDbType.BigInt);
            parameter.Direction = ParameterDirection.ReturnValue;
            if (base.SqlCmd.ExecuteNonQuery() > 0)
            {
                return Convert.ToInt64(parameter.Value);
            }
            return 0L;
        }
        
        public override string GetViewQuery(string condStr)
        {
            base.StrSql = "SELECT * FROM GLB_LAYOUT";
            if (condStr.Trim() != "")
            {
                base.StrSql = base.StrSql + " WHERE " + condStr;
            }
            return base.StrSql;
        }
        
        public override void LoadAttributes(long ID)
        {
            base.StrSql = "SELECT * FROM GLB_LAYOUT";
            base.SqlCmd = new SqlCommand(base.StrSql, GlobalVariables.SqlConn);
            SqlDataReader reader = base.SqlCmd.ExecuteReader();
            if (reader.Read())
            {
                this.LayoutID = Convert.ToInt32(reader["LAYOUTID"]);
                this.SourceID = Convert.ToInt32(reader["SOURCEID"]);
                this.Xpos = Convert.ToDouble(reader["XPOS"]);
                this.Ypos = Convert.ToDouble(reader["YPOS"]);
                this.SourceType = Convert.ToChar(reader["SOURCETYPE"]);
            }
            reader.Close();
        }
        
        public int GetRoomCount
        {
            get
            {
                base.StrSql = "SELECT ISNULL(COUNT(*),0) FROM GLB_LAYOUT WHERE SOURCETYPE ='R'";
                base.SqlCmd = new SqlCommand(base.StrSql, GlobalVariables.SqlConn);
                return Convert.ToInt32(base.SqlCmd.ExecuteScalar());
            }
        }
        
        public int GetTableCount
        {
            get
            {
                base.StrSql = "SELECT ISNULL(COUNT(*),0) FROM GLB_LAYOUT WHERE SOURCETYPE ='T'";
                base.SqlCmd = new SqlCommand(base.StrSql, GlobalVariables.SqlConn);
                return Convert.ToInt32(base.SqlCmd.ExecuteScalar());
            }
        }
    }
}
