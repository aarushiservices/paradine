// Generated by .NET Reflector from D:\Paradine\ParaDine\ParaDine.exe
namespace ParaDine
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    
    public class RecipeTrans : MasterBase
    {
        public static DataTable DtRecipeTrans = new DataTable("RecipeTrans");
        public long IngrediantID;
        public double Qty;
        public long RecipeMasterID;
        public long RecipeTransId;
        public int UOMId;
        
        public RecipeTrans()
        {
            this.LoadAttributes(0L);
        }
        
        public RecipeTrans(long ID)
        {
            this.RecipeTransId = ID;
            this.LoadAttributes(ID);
        }
        
        public static void FillDtCl()
        {
            DtRecipeTrans.Columns.Clear();
            DtRecipeTrans.Columns.Add("RECIPETRANSID", typeof(string));
            DtRecipeTrans.Columns.Add("RECIPEMASTERID", typeof(string));
            DtRecipeTrans.Columns.Add("INGREDIANT", typeof(string));
            DtRecipeTrans.Columns.Add("UOMNAME", typeof(string));
            DtRecipeTrans.Columns.Add("QTY", typeof(double));
            DtRecipeTrans.Columns.Add("TABLEID", typeof(int));
        }
        
        public DataRow GetDataRow()
        {
            DataRow row = DtRecipeTrans.NewRow();
            Item item = new Item(this.IngrediantID);
            UOM uom = new UOM(this.UOMId);
            row["RECIPETRANSID"] = this.RecipeTransId;
            row["RECIPEMASTERID"] = this.RecipeMasterID;
            row["INGREDIANT"] = item.ItemName;
            row["UOMNAME"] = uom.UOMName;
            row["QTY"] = this.Qty;
            row["TABLEID"] = 0;
            return row;
        }
        
        public override long GetExecuteCommand(string TransType, SqlTransaction SqlTrans)
        {
            base.SqlCmd = new SqlCommand();
            base.SqlCmd.Connection = GlobalVariables.SqlConn;
            base.SqlCmd.Transaction = SqlTrans;
            base.SqlCmd.CommandText = "RES_PROC_RECIPETRANS";
            base.SqlCmd.CommandType = CommandType.StoredProcedure;
            base.SqlCmd.Parameters.AddWithValue("@TRANSTYPE", TransType);
            base.SqlCmd.Parameters.AddWithValue("@RECIPETRANSID", this.RecipeTransId);
            base.SqlCmd.Parameters.AddWithValue("@RECIPEMASTERID", this.RecipeMasterID);
            base.SqlCmd.Parameters.AddWithValue("@INGREDIANTID", this.IngrediantID);
            base.SqlCmd.Parameters.AddWithValue("@UOMID", this.UOMId);
            base.SqlCmd.Parameters.AddWithValue("@QTY", this.Qty);
            SqlParameter parameter = base.SqlCmd.Parameters.Add("RetVal", SqlDbType.BigInt);
            parameter.Direction = ParameterDirection.ReturnValue;
            if (base.SqlCmd.ExecuteNonQuery() > 0)
            {
                return Convert.ToInt64(parameter.Value);
            }
            return 0L;
        }
        
        public override string GetViewQuery(string condStr)
        {
            base.StrSql = "SELECT * FROM RES_RECIPETRANS";
            if (condStr.Trim() != "")
            {
                base.StrSql = base.StrSql + " WHERE " + condStr;
            }
            return base.StrSql;
        }
        
        public override void LoadAttributes(long ID)
        {
            base.StrSql = "SELECT * FROM RES_RECIPETRANS WHERE RECIPETRANSID = " + ID;
            base.SqlCmd = new SqlCommand(base.StrSql, GlobalVariables.SqlConn);
            SqlDataReader reader = base.SqlCmd.ExecuteReader();
            if (reader.Read())
            {
                this.RecipeTransId = Convert.ToInt64(reader["RECIPETRANSID"]);
                this.RecipeMasterID = Convert.ToInt64(reader["RECIPEMASTERID"]);
                this.IngrediantID = Convert.ToInt64(reader["INGREDIANTID"]);
                this.UOMId = Convert.ToInt32(reader["UOMID"]);
                this.Qty = Convert.ToDouble(reader["QTY"]);
            }
            reader.Close();
        }
    }
}
