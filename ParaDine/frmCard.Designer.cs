﻿namespace ParaDine
{
    partial class frmCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlDepartment = new System.Windows.Forms.Panel();
            this.dgViewCard = new System.Windows.Forms.DataGridView();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnTrans = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCardNumber = new System.Windows.Forms.TextBox();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.pnlDepartment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgViewCard)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlDepartment
            // 
            this.pnlDepartment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDepartment.Controls.Add(this.dgViewCard);
            this.pnlDepartment.Controls.Add(this.btnExit);
            this.pnlDepartment.Controls.Add(this.btnTrans);
            this.pnlDepartment.Controls.Add(this.label1);
            this.pnlDepartment.Controls.Add(this.txtCardNumber);
            this.pnlDepartment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDepartment.Location = new System.Drawing.Point(0, 0);
            this.pnlDepartment.Name = "pnlDepartment";
            this.pnlDepartment.Size = new System.Drawing.Size(382, 370);
            this.pnlDepartment.TabIndex = 4;
            // 
            // dgViewCard
            // 
            this.dgViewCard.AllowUserToAddRows = false;
            this.dgViewCard.AllowUserToDeleteRows = false;
            this.dgViewCard.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgViewCard.Location = new System.Drawing.Point(11, 11);
            this.dgViewCard.Name = "dgViewCard";
            this.dgViewCard.RowHeadersVisible = false;
            this.dgViewCard.Size = new System.Drawing.Size(358, 273);
            this.dgViewCard.TabIndex = 4;
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(197, 336);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(104, 336);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 2;
            this.btnTrans.Text = "Mode";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 302);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Card Number :";
            // 
            // txtCardNumber
            // 
            this.txtCardNumber.BackColor = System.Drawing.Color.White;
            this.txtCardNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCardNumber.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCardNumber.Location = new System.Drawing.Point(96, 300);
            this.txtCardNumber.Name = "txtCardNumber";
            this.txtCardNumber.Size = new System.Drawing.Size(273, 20);
            this.txtCardNumber.TabIndex = 0;
            this.TTip.SetToolTip(this.txtCardNumber, "Enter Card Number");
            this.txtCardNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCardNumber_KeyDown);
            // 
            // frmCard
            // 
            this.ClientSize = new System.Drawing.Size(382, 370);
            this.Controls.Add(this.pnlDepartment);
            this.Name = "frmCard";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Card Entry";
            this.Load += new System.EventHandler(this.frmCard_Load);
            this.pnlDepartment.ResumeLayout(false);
            this.pnlDepartment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgViewCard)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
    }
}