﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine
{
    partial class frmShiftBegin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
             private void InitializeComponent()
        {
            this.components = new Container();
            this.label1 = new Label();
            this.btnStartShift = new Button();
            this.btnClose = new Button();
            this.cmbShift = new ComboBox();
            this.dtpStartTime = new DateTimePicker();
            this.label3 = new Label();
            this.dtpEndTime = new DateTimePicker();
            this.label4 = new Label();
            this.dtpPresentTime = new DateTimePicker();
            this.cmbUser = new ComboBox();
            this.label5 = new Label();
            this.gvOpeningBal = new NumControl();
            this.label2 = new Label();
            this.timer1 = new Timer(this.components);
            this.btnCloseShift = new Button();
            this.txtShiftStartedTime = new TextBox();
            this.label6 = new Label();
            this.label7 = new Label();
            base.SuspendLayout();
            this.label1.AutoSize = true;
            this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label1.Location = new Point(0x30, 0x1a);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x21, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Shift";
            this.btnStartShift.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnStartShift.Location = new Point(0x3e, 0xf2);
            this.btnStartShift.Name = "btnStartShift";
            this.btnStartShift.Size = new Size(0x69, 0x1d);
            this.btnStartShift.TabIndex = 1;
            this.btnStartShift.Text = "Start Shift";
            this.btnStartShift.UseVisualStyleBackColor = true;
            this.btnStartShift.Click += new EventHandler(this.btnStartShift_Click);
            this.btnClose.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnClose.Location = new Point(0x11c, 0xf2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new Size(0x69, 0x1d);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new EventHandler(this.btnClose_Click);
            this.cmbShift.FormattingEnabled = true;
            this.cmbShift.Location = new Point(0x5b, 0x17);
            this.cmbShift.Name = "cmbShift";
            this.cmbShift.Size = new Size(0x148, 0x15);
            this.cmbShift.TabIndex = 3;
            this.cmbShift.SelectionChangeCommitted += new EventHandler(this.cmbShift_SelectionChangeCommitted);
            this.dtpStartTime.CalendarTitleBackColor = SystemColors.ControlText;
            this.dtpStartTime.CalendarTitleForeColor = Color.White;
            this.dtpStartTime.CustomFormat = "dd/MMM/yy hh:mm:ss tt";
            this.dtpStartTime.Format = DateTimePickerFormat.Custom;
            this.dtpStartTime.Location = new Point(0x33, 0x51);
            this.dtpStartTime.Name = "dtpStartTime";
            this.dtpStartTime.Size = new Size(0xab, 20);
            this.dtpStartTime.TabIndex = 4;
            this.label3.AutoSize = true;
            this.label3.Location = new Point(0xe4, 0x55);
            this.label3.Name = "label3";
            this.label3.Size = new Size(20, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "To";
            this.dtpEndTime.CustomFormat = "dd/MMM/yy hh:mm:ss tt";
            this.dtpEndTime.Format = DateTimePickerFormat.Custom;
            this.dtpEndTime.Location = new Point(0xfb, 0x51);
            this.dtpEndTime.Name = "dtpEndTime";
            this.dtpEndTime.Size = new Size(0xa8, 20);
            this.dtpEndTime.TabIndex = 6;
            this.label4.AutoSize = true;
            this.label4.Location = new Point(0xfb, 0xad);
            this.label4.Name = "label4";
            this.label4.Size = new Size(0x45, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Present Time";
            this.dtpPresentTime.CustomFormat = "dd/MMM/yy hh:mm:ss tt";
            this.dtpPresentTime.Format = DateTimePickerFormat.Custom;
            this.dtpPresentTime.Location = new Point(0xfb, 0xbc);
            this.dtpPresentTime.Name = "dtpPresentTime";
            this.dtpPresentTime.Size = new Size(0xa8, 20);
            this.dtpPresentTime.TabIndex = 9;
            this.cmbUser.Enabled = false;
            this.cmbUser.FormattingEnabled = true;
            this.cmbUser.Location = new Point(0xfb, 0x86);
            this.cmbUser.Name = "cmbUser";
            this.cmbUser.Size = new Size(0xa8, 0x15);
            this.cmbUser.TabIndex = 10;
            this.label5.AutoSize = true;
            this.label5.Location = new Point(0xfe, 0x76);
            this.label5.Name = "label5";
            this.label5.Size = new Size(0x42, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Current User";
            this.gvOpeningBal.DecimalRequired = true;
            this.gvOpeningBal.Enabled = false;
            this.gvOpeningBal.Font = new Font("Rupee Foradian", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.gvOpeningBal.Location = new Point(0x33, 0xbc);
            this.gvOpeningBal.Name = "gvOpeningBal";
            this.gvOpeningBal.Size = new Size(0xab, 20);
            this.gvOpeningBal.SymbolRequired = true;
            this.gvOpeningBal.TabIndex = 12;
            this.gvOpeningBal.TabStop = false;
            this.gvOpeningBal.Text = "`0.00";
            this.gvOpeningBal.TextAlign = HorizontalAlignment.Right;
            int[] bits = new int[4];
            this.gvOpeningBal.Value = new decimal(bits);
            this.label2.AutoSize = true;
            this.label2.Location = new Point(0x30, 0xac);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0x4a, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Opening Cash";
            this.timer1.Interval = 0x3e8;
            this.timer1.Tick += new EventHandler(this.timer1_Tick);
            this.btnCloseShift.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnCloseShift.Location = new Point(0xad, 0xf2);
            this.btnCloseShift.Name = "btnCloseShift";
            this.btnCloseShift.Size = new Size(0x69, 0x1d);
            this.btnCloseShift.TabIndex = 14;
            this.btnCloseShift.Text = "Close Shift";
            this.btnCloseShift.UseVisualStyleBackColor = true;
            this.btnCloseShift.Click += new EventHandler(this.btnCloseShift_Click);
            this.txtShiftStartedTime.BackColor = Color.White;
            this.txtShiftStartedTime.Location = new Point(0x33, 0x86);
            this.txtShiftStartedTime.Name = "txtShiftStartedTime";
            this.txtShiftStartedTime.ReadOnly = true;
            this.txtShiftStartedTime.Size = new Size(0xab, 20);
            this.txtShiftStartedTime.TabIndex = 15;
            this.label6.AutoSize = true;
            this.label6.Location = new Point(0x30, 0x41);
            this.label6.Name = "label6";
            this.label6.Size = new Size(0x43, 13);
            this.label6.TabIndex = 0x10;
            this.label6.Text = "Shift Timings";
            this.label7.AutoSize = true;
            this.label7.Location = new Point(0x30, 0x76);
            this.label7.Name = "label7";
            this.label7.Size = new Size(0x36, 13);
            this.label7.TabIndex = 0x11;
            this.label7.Text = "Started At";
            base.AutoScaleDimensions = new SizeF(6f, 13f);
//          base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x1ba, 0x12d);
            base.Controls.Add(this.label7);
            base.Controls.Add(this.label6);
            base.Controls.Add(this.txtShiftStartedTime);
            base.Controls.Add(this.btnCloseShift);
            base.Controls.Add(this.label2);
            base.Controls.Add(this.gvOpeningBal);
            base.Controls.Add(this.label5);
            base.Controls.Add(this.cmbUser);
            base.Controls.Add(this.dtpPresentTime);
            base.Controls.Add(this.label4);
            base.Controls.Add(this.label3);
            base.Controls.Add(this.dtpEndTime);
            base.Controls.Add(this.dtpStartTime);
            base.Controls.Add(this.cmbShift);
            base.Controls.Add(this.btnClose);
            base.Controls.Add(this.btnStartShift);
            base.Controls.Add(this.label1);
            base.Name = "frmShiftBegin";
            base.ShowIcon = false;
            base.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "Shift Begin";
            base.Load += new EventHandler(this.frmShiftBegin_Load);
            base.ResumeLayout(false);
            base.PerformLayout();
        }
        #endregion
    }
}