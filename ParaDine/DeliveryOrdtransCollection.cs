// Generated by .NET Reflector from D:\Paradine\ParaDine\ParaDine.exe
namespace ParaDine
{
    using System;
    using System.Collections;
    using System.Data;
    using System.Reflection;
    using System.Windows.Forms;
    
    public class DeliveryOrdtransCollection : CollectionBase
    {
        public void Add(DeliveryOrderTrans VarEntDeliveryOrderTrans)
        {
            foreach (DeliveryOrderTrans trans in base.List)
            {
                if ((((trans.ItemID == VarEntDeliveryOrderTrans.ItemID) && (trans.UOMID == VarEntDeliveryOrderTrans.UOMID)) && (trans.Price == VarEntDeliveryOrderTrans.Price)) && (trans.Deleted == VarEntDeliveryOrderTrans.Deleted))
                {
                    trans.Qty += VarEntDeliveryOrderTrans.Qty;
                    return;
                }
            }
            base.List.Add(VarEntDeliveryOrderTrans);
        }
        
        public DataTable GetCollectiontable()
        {
            DataTable dTdeliveryOrderTrans = new DataTable();
            DeliveryOrderTrans.FillDtCl();
            dTdeliveryOrderTrans = DeliveryOrderTrans.DTdeliveryOrderTrans;
            dTdeliveryOrderTrans.Rows.Clear();
            try
            {
                foreach (DeliveryOrderTrans trans in base.List)
                {
                    if (!trans.Deleted)
                    {
                        DataRow row = dTdeliveryOrderTrans.NewRow();
                        DataRow dataRow = trans.GetDataRow();
                        foreach (DataColumn column in dTdeliveryOrderTrans.Columns)
                        {
                            row[column.ColumnName] = dataRow[column.ColumnName];
                        }
                        row["TableID"] = base.List.IndexOf(trans) + 1;
                        dTdeliveryOrderTrans.Rows.Add(row);
                    }
                }
                return dTdeliveryOrderTrans;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "DeliveryOrder.GetCollectionTable");
                return dTdeliveryOrderTrans;
            }
        }
        
        public void InsertAt(DeliveryOrderTrans varEntDeliveryOrder, short intIndex)
        {
            varEntDeliveryOrder.Modified = true;
            base.List[intIndex] = varEntDeliveryOrder;
        }
        
        public virtual DeliveryOrderTrans this[int index]
        {
            get
            {
                return (DeliveryOrderTrans) base.List[index];
            }
            set
            {
                base.List[index] = value;
            }
        }
    }
}
