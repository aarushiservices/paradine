﻿using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine
{
    public partial class frmShowCard : Form
    {
        private ParButton btnExit;
        private ParButton btnTrans;
        private ComboBox cmbCardType;
      //  private IContainer components;
        private DataGridView dgTrans;
        public Card EntCard;
        public CardTrans EntId;
        public CardIssue EntIssue;
        private Label label1;
        private Label label6;
        private NumControl nmBalAmount;
        private NumControl nmTotCreditAmt;
        private NumControl nmTotDebitAmt;
        private Panel pnlMain;
        private ToolTip TTip;
        private TextBox txtCardNumber;
        private TextBox txtCustomer;

        public frmShowCard()
        {
            this.EntId = new CardTrans();
            this.EntCard = new Card();
            this.EntIssue = new CardIssue();
            this.components = null;
            this.InitializeComponent();
        }

        public frmShowCard(string Mode, int ID, SecurityClass paramSecurity)
        {
            this.EntId = new CardTrans();
            this.EntCard = new Card();
            this.EntIssue = new CardIssue();
            this.components = null;
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes((long)ID);
            this.EntId.Security = paramSecurity;
            this.EntIssue = new CardIssue(this.EntId.CardIssueId);
            this.EntCard = new Card(this.EntIssue.CardId);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.EntCard = new Card();
            base.DialogResult = DialogResult.Cancel;
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (ParaDine.GlobalValidations.ValidateFields(this.pnlMain, this.TTip))
            {
                base.Close();
            }
        }
        private void frmShowCard_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                this.RefreshData();
                this.LoadFields();
                ParaDine.GlobalFunctions.AddCompHandler(this.pnlMain);
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                base.Close();
            }
        }
        private void LoadEntities()
        {
        }

        private void LoadFields()
        {
            this.EntIssue = new CardIssue(this.EntId.CardIssueId);
            this.EntCard = new Card(this.EntIssue.CardId);
            this.txtCustomer.Text = this.EntIssue.CustomerName;
            this.cmbCardType.SelectedValue = this.EntIssue.CardTypeId;
            this.txtCardNumber.Text = this.EntCard.CardNumber;
            this.txtCardNumber.Tag = this.EntIssue.CardId;
            this.nmBalAmount.Value = Convert.ToDecimal(this.EntCard.CardBalance());
            this.nmTotCreditAmt.Value = Convert.ToDecimal(this.EntCard.TotCreditAmount());
            this.nmTotDebitAmt.Value = Convert.ToDecimal(this.EntCard.TotDebitAmount());
            this.dgTrans.DataSource = this.EntCard.CardTransaction();
            this.cmbCardType.SelectedValue = this.EntIssue.CardTypeId;
        }

        private void RefreshData()
        {
            try
            {
                ParaDine.GlobalFill.FillCombo("SELECT CARDTYPEID, CARDTYPENAME FROM RES_CARDTYPE", this.cmbCardType);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in RefreshData");
            }
        }

        private void txtCardNumber_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void txtCardNumber_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                this.EntCard = new Card(this.txtCardNumber.Text);
                if (this.EntCard.CardId <= 0L)
                {
                    throw new Exception("InValid Card");
                }
                if (this.EntCard.CardIssueId == 0L)
                {
                    throw new Exception("Card Not Issued. Please Check..");
                }
                this.EntIssue = new CardIssue(this.EntCard.CardIssueId);
                this.nmBalAmount.Value = Convert.ToDecimal(this.EntCard.CardBalance());
                this.nmTotCreditAmt.Value = Convert.ToDecimal(this.EntCard.TotCreditAmount());
                this.nmTotDebitAmt.Value = Convert.ToDecimal(this.EntCard.TotDebitAmount());
                this.dgTrans.DataSource = this.EntCard.CardTransaction();
                this.cmbCardType.SelectedValue = this.EntIssue.CardTypeId;
                this.txtCustomer.Text = this.EntIssue.CustomerName;
                this.txtCardNumber.SelectAll();
                e.Cancel = true;
            }
            catch (Exception exception)
            {
                e.Cancel = true;
                this.txtCardNumber.SelectAll();
                MessageBox.Show(exception.Message, "Error in Card Number Validation", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
    }
}
