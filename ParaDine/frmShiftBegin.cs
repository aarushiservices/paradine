﻿using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine
{
    public partial class frmShiftBegin : Form
    {
        private Button btnClose;
        private Button btnCloseShift;
        private Button btnStartShift;
        private ComboBox cmbShift;
        private ComboBox cmbUser;
      //private IContainer components = null;
        private DateTimePicker dtpEndTime;
        private DateTimePicker dtpPresentTime;
        private DateTimePicker dtpStartTime;
        private NumControl gvOpeningBal;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private string StrSql = "";
        private Timer timer1;
        private TextBox txtShiftStartedTime;

        public frmShiftBegin()
        {
            this.InitializeComponent();
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            base.Close();
        }
        private void btnCloseShift_Click(object sender, EventArgs e)
        {
            string shiftName = new Shift(Convert.ToInt32(this.cmbShift.SelectedValue)).ShiftName;
            ShiftTrans trans = new ShiftTrans();
            trans = new ShiftTrans(trans.GetCurrentShiftTransId());
            SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
            try
            {
                trans.ShiftId = Convert.ToInt32(this.cmbShift.SelectedValue);
                trans.EndedTime = this.dtpPresentTime.Value;
                trans.StartedTimeGiven = true;
                trans.EndedTimeGiven = true;
                trans.UserId = ParaDine.GlobalVariables.UserID;
                trans.ShiftName = shiftName;
                trans.Modify(sqlTrans, false);
                sqlTrans.Commit();
                MessageBox.Show("Shift Closed");
                ParaDine.GlobalFunctions.AssignCurrentShift();
                base.Close();
            }
            catch (Exception exception)
            {
                sqlTrans.Rollback();
                MessageBox.Show(exception.Message, "Error in Shift Start");
            }
        }

        private void btnStartShift_Click(object sender, EventArgs e)
        {
            string shiftName = new Shift(Convert.ToInt32(this.cmbShift.SelectedValue)).ShiftName;
            SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
            try
            {
                new ShiftTrans { ShiftId = Convert.ToInt32(this.cmbShift.SelectedValue), StartedTime = new DateTime(ParaDine.GlobalVariables.BusinessDate.Year, ParaDine.GlobalVariables.BusinessDate.Month, ParaDine.GlobalVariables.BusinessDate.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second), StartedTimeGiven = true, EndedTimeGiven = false, UserId = ParaDine.GlobalVariables.UserID, ShiftName = shiftName }.Add(sqlTrans, false);
                sqlTrans.Commit();
                MessageBox.Show("Shift Started");
                ParaDine.GlobalFunctions.AssignCurrentShift();
                base.Close();
            }
            catch (Exception exception)
            {
                sqlTrans.Rollback();
                MessageBox.Show(exception.Message, "Error in Shift Start");
            }
        }

        private void cmbShift_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Shift shift = new Shift(Convert.ToInt32(this.cmbShift.SelectedValue));
            this.dtpStartTime.Value = new DateTime(ParaDine.GlobalVariables.BusinessDate.Year, ParaDine.GlobalVariables.BusinessDate.Month, ParaDine.GlobalVariables.BusinessDate.Day, shift.StartingTime.Hour, shift.StartingTime.Minute, shift.StartingTime.Second);
            this.dtpEndTime.Value = new DateTime(ParaDine.GlobalVariables.BusinessDate.Year, ParaDine.GlobalVariables.BusinessDate.Month, ParaDine.GlobalVariables.BusinessDate.Day, shift.EndingTime.Hour, shift.EndingTime.Minute, shift.EndingTime.Second);
            this.dtpPresentTime.Value = new DateTime(ParaDine.GlobalVariables.BusinessDate.Year, ParaDine.GlobalVariables.BusinessDate.Month, ParaDine.GlobalVariables.BusinessDate.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
        }
        private void frmShiftBegin_Load(object sender, EventArgs e)
        {
            this.RefreshData();
            this.timer1.Start();
            if (ParaDine.GlobalVariables.CurrentShiftId > 0)
            {
                ShiftTrans trans = new ShiftTrans();
                trans = new ShiftTrans(trans.GetCurrentShiftTransId());
                this.cmbShift.SelectedValue = ParaDine.GlobalVariables.CurrentShiftId;
                this.cmbShift.Enabled = false;
                this.txtShiftStartedTime.Text = trans.StartedTime.ToString("dd/MMM/yy hh:mm:ss tt");
                this.btnStartShift.Enabled = false;
                this.btnCloseShift.Enabled = true;
            }
            else
            {
                this.btnStartShift.Enabled = true;
                this.btnCloseShift.Enabled = false;
            }
        }
        private void RefreshData()
        {
            if (ParaDine.GlobalVariables.CurrentShiftId > 0)
            {
                this.StrSql = "SELECT SHIFTID, SHIFTNAME FROM RES_SHIFT ORDER BY SHIFTID ";
            }
            else
            {
                this.StrSql = "SELECT SHIFTID, SHIFTNAME FROM RES_SHIFT  WHERE SHIFTID NOT IN   (SELECT SHIFTID FROM RES_SHIFTTRANSACTION WHERE USERID = " + ParaDine.GlobalVariables.UserID.ToString() + " AND CONVERT(DATETIME, CONVERT(VARCHAR, STARTEDTIME, 112)) = '" + ParaDine.GlobalVariables.BusinessDate.ToString("dd/MMM/yy") + "') ORDER BY SHIFTID ";
            }
            ParaDine.GlobalFill.FillCombo(this.StrSql, this.cmbShift);
            this.StrSql = "SELECT USERID, USERNAME FROM GLB_USER ORDER BY USERID";
            ParaDine.GlobalFill.FillCombo(this.StrSql, this.cmbUser);
            this.cmbUser.SelectedValue = ParaDine.GlobalVariables.UserID;
            if (this.cmbShift.Items.Count > 0)
            {
                this.cmbShift.SelectedIndex = 0;
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            this.dtpPresentTime.Value = new DateTime(ParaDine.GlobalVariables.BusinessDate.Year, ParaDine.GlobalVariables.BusinessDate.Month, ParaDine.GlobalVariables.BusinessDate.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
        }
    }
}
