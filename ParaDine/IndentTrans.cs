// Generated by .NET Reflector from D:\Paradine\ParaDine\ParaDine.exe
namespace ParaDine
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    
    public class IndentTrans : MasterBase
    {
        public double CostPrice;
        public static DataTable DtIndentTrans = new DataTable("IndentMaster");
        public long IndentMasterID;
        public long IndentTransID;
        public long ItemID;
        public double OrderQty;
        public string Remarks;
        public int UOMID;
        
        public IndentTrans()
        {
            this.LoadAttributes(0L);
        }
        
        public IndentTrans(long ID)
        {
            this.IndentTransID = ID;
            this.LoadAttributes(ID);
        }
        
        public static void FillDtCl()
        {
            DtIndentTrans.Columns.Clear();
            DtIndentTrans.Columns.Add("INDENTTRANSID", typeof(string));
            DtIndentTrans.Columns.Add("INDENTMASTERID", typeof(string));
            DtIndentTrans.Columns.Add("ITEMNAME", typeof(string));
            DtIndentTrans.Columns.Add("UOM", typeof(string));
            DtIndentTrans.Columns.Add("QTY", typeof(double));
            DtIndentTrans.Columns.Add("CPRICE", typeof(double));
            DtIndentTrans.Columns.Add("AMOUNT", typeof(double));
            DtIndentTrans.Columns.Add("REMARKS", typeof(string));
            DtIndentTrans.Columns.Add("TABLEID", typeof(string));
        }
        
        public double GetAmount()
        {
            return (this.OrderQty * this.CostPrice);
        }
        
        public DataRow GetDataRow()
        {
            DataRow row = DtIndentTrans.NewRow();
            Item item = new Item(this.ItemID);
            UOM uom = new UOM(this.UOMID);
            row["INDENTTRANSID"] = this.IndentTransID;
            row["INDENTMASTERID"] = this.IndentMasterID;
            row["ITEMNAME"] = item.ItemName;
            row["UOM"] = uom.UOMName;
            row["QTY"] = this.OrderQty;
            row["CPRICE"] = this.CostPrice;
            row["AMOUNT"] = this.GetAmount();
            row["REMARKS"] = this.Remarks;
            row["TABLEID"] = 0;
            return row;
        }
        
        public override long GetExecuteCommand(string TransType, SqlTransaction SqlTrans)
        {
            base.SqlCmd = new SqlCommand();
            base.SqlCmd.Connection = GlobalVariables.SqlConn;
            base.SqlCmd.Transaction = SqlTrans;
            base.SqlCmd.CommandText = "RES_PROC_INDENTTRANS";
            base.SqlCmd.CommandType = CommandType.StoredProcedure;
            base.SqlCmd.Parameters.AddWithValue("@TRANSTYPE", TransType);
            base.SqlCmd.Parameters.AddWithValue("@INDENTTRANSID", this.IndentTransID);
            base.SqlCmd.Parameters.AddWithValue("@INDENTMASTERID", this.IndentMasterID);
            base.SqlCmd.Parameters.AddWithValue("@ITEMID", this.ItemID);
            base.SqlCmd.Parameters.AddWithValue("@UOMID", this.UOMID);
            base.SqlCmd.Parameters.AddWithValue("@ORDERQTY", this.OrderQty);
            base.SqlCmd.Parameters.AddWithValue("@COSTPRICE", this.CostPrice);
            base.SqlCmd.Parameters.AddWithValue("@REMARKS", this.Remarks);
            SqlParameter parameter = base.SqlCmd.Parameters.Add("RetVal", SqlDbType.BigInt);
            parameter.Direction = ParameterDirection.ReturnValue;
            if (base.SqlCmd.ExecuteNonQuery() > 0)
            {
                return Convert.ToInt64(parameter.Value);
            }
            return 0L;
        }
        
        public override string GetViewQuery(string condStr)
        {
            base.StrSql = "SELECT * FROM RES_INDENTTRANS";
            if (condStr.Trim() != "")
            {
                base.StrSql = base.StrSql + " WHERE " + condStr;
            }
            return base.StrSql;
        }
        
        public override void LoadAttributes(long ID)
        {
            base.StrSql = "SELECT * FROM RES_INDENTTRANS WHERE INDENTTRANSID = " + ID;
            base.SqlCmd = new SqlCommand(base.StrSql, GlobalVariables.SqlConn);
            SqlDataReader reader = base.SqlCmd.ExecuteReader();
            if (reader.Read())
            {
                this.IndentTransID = Convert.ToInt64(reader["INDENTTRANSID"]);
                this.IndentMasterID = Convert.ToInt64(reader["INDENTMASTERID"]);
                this.ItemID = Convert.ToInt64(reader["ITEMID"]);
                this.UOMID = Convert.ToInt32(reader["UOMID"]);
                this.OrderQty = Convert.ToDouble(reader["ORDERQTY"]);
                this.CostPrice = Convert.ToDouble(reader["COSTPRICE"]);
                this.Remarks = Convert.ToString(reader["REMARKS"]);
            }
            reader.Close();
        }
    }
}
