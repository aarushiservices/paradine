// Generated by .NET Reflector from D:\Paradine\ParaDine\ParaDine.exe
namespace ParaDine
{
    using ParaSysCom;
    using System;
    using System.Data.SqlClient;
    using System.Drawing;
    using System.Windows.Forms;
    
    public class GlobalTheme : ParaDine.GlobalVariables
    {
        public void ApplyDataGridTheme(DataGridView dgView)
        {
            dgView.RowsDefaultCellStyle.SelectionBackColor = Color.LightGray;
            dgView.RowsDefaultCellStyle.SelectionForeColor = Color.Black;
            dgView.RowsDefaultCellStyle.Font = new Font("Calibri", 10f, FontStyle.Bold, GraphicsUnit.Point, 0);
            dgView.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10f, FontStyle.Bold, GraphicsUnit.Point, 0);
            dgView.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10f, FontStyle.Bold, GraphicsUnit.Point, 0);
            dgView.ColumnHeadersDefaultCellStyle.BackColor = Color.Maroon;
            dgView.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dgView.AllowUserToAddRows = false;
            dgView.AllowUserToDeleteRows = false;
            dgView.AllowUserToOrderColumns = false;
            dgView.AllowUserToResizeColumns = true;
            dgView.AllowUserToResizeRows = false;
            dgView.EditMode = DataGridViewEditMode.EditProgrammatically;
            dgView.ReadOnly = true;
            dgView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgView.RowTemplate.ReadOnly = true;
            dgView.StandardTab = false;
            dgView.AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke;
            dgView.AlternatingRowsDefaultCellStyle.ForeColor = ParaDine.GlobalVariables.GridAlternatingRow_ForeColor;
        }
        
        public void applyTheme(Form grpFields)
        {
            try
            {
                this.LoadTheme();
                if (ParaDine.GlobalVariables.Theme != 0)
                {
                    string str;
                    if (grpFields.Tag == null)  
                    {
                        str = "0";
                    }
                    else
                    {
                        str = Convert.ToString(grpFields.Tag);
                    }
                    if (str != "NoTheme")
                    {
                       // grpFields.BackColor = ParaDine.GlobalVariables.theme_color;
                    }
                    for (int i = 0; i <= (grpFields.Controls.Count - 1); i++)
                    {
                        if (grpFields.Tag == null)
                        {
                            str = "0";
                        }
                        else
                        {
                            str = Convert.ToString(grpFields.Tag);
                        }
                        if (str != "NoTheme")
                        {
                            if ((grpFields.Controls[i] is GroupBox) || (grpFields.Controls[i] is Panel))
                            {
                                for (int j = 0; j <= (grpFields.Controls[i].Controls.Count - 1); j++)
                                {
                                    if (grpFields.Controls[i].Tag == null)
                                    {
                                        str = "0";
                                    }
                                    else
                                    {
                                        str = Convert.ToString(grpFields.Controls[i].Tag);
                                    }
                                    if (str != "NoTheme")
                                    {
                                        this.ApplyThemeChild(grpFields.Controls[i].Controls[j]);
                                       // grpFields.Controls[i].BackColor = ParaDine.GlobalVariables.theme_color;
                                    }
                                }
                            }
                            else
                            {
                                this.ApplyThemeChild(grpFields.Controls[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Apply Theme", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }        
        public void ApplyThemeChild(Control cntrl)
        {
            try
            {
                if (cntrl.GetType() == typeof(TextBox))
                {
                    if (((TextBox) cntrl).BackColor == Color.Ivory)
                    {
                        ((TextBox) cntrl).BackColor = ParaDine.GlobalVariables.Compulsory_Color;
                    }
                }
                else if (cntrl.GetType() != typeof(PictureBox))
                {
                    if (cntrl.GetType() == typeof(ComboBox))
                    {
                        if (((ComboBox) cntrl).BackColor == Color.Ivory)
                        {
                            ((ComboBox) cntrl).BackColor = ParaDine.GlobalVariables.Compulsory_Color;
                        }
                    }
                    else if (cntrl.GetType() == typeof(NumControl))
                    {
                        if (((NumControl) cntrl).BackColor == Color.Ivory)
                        {
                            cntrl.BackColor = ParaDine.GlobalVariables.Compulsory_Color;
                            ((NumControl) cntrl).BackColor = ParaDine.GlobalVariables.Compulsory_Color;
                        }
                    }
                    else if (cntrl.GetType() == typeof(NumControl))
                    {
                        if (((NumControl) cntrl).BackColor == Color.Ivory)
                        {
                            cntrl.BackColor = ParaDine.GlobalVariables.Compulsory_Color;
                            ((NumControl) cntrl).BackColor = ParaDine.GlobalVariables.Compulsory_Color;
                        }
                    }
                    else if (((cntrl.GetType() != typeof(ListView)) && (cntrl.GetType() != typeof(DataGrid))) && (cntrl.GetType() != typeof(DataGridView)))
                    {
                        string str;
                        if (cntrl.GetType() == typeof(Button))
                        {
                            if (cntrl.Tag == null)
                            {
                                str = "0";
                            }
                            else
                            {
                                str = Convert.ToString(cntrl.Tag);
                            }
                            if (str == "NoTheme")
                            {
                                return;
                            }
                            ((Button) cntrl).FlatStyle = FlatStyle.Flat;
                            ((Button) cntrl).FlatAppearance.BorderSize = 0;
                            if (ParaDine.GlobalVariables.Theme == 0)
                            {
                                ((Button) cntrl).ForeColor = Color.White;
                            }
                            else
                            {
                                ((Button) cntrl).ForeColor = Color.Black;
                            }
                            ((Button) cntrl).Cursor = Cursors.Hand;
                            cntrl.GotFocus += new EventHandler(this.Btn_GotFocus);
                            cntrl.LostFocus += new EventHandler(this.Btn_LostFocus);
                            cntrl.MouseEnter += new EventHandler(this.Btn_GotFocus);
                            cntrl.MouseLeave += new EventHandler(this.Btn_LostFocus);
                            cntrl.BackgroundImage = ParaDine.GlobalVariables.Theme_img;
                            cntrl.BackgroundImageLayout = ImageLayout.Stretch;
                        }
                        else
                        {
                            if (cntrl.Tag == null)
                            {
                                str = "0";
                            }
                            else
                            {
                                str = Convert.ToString(cntrl.Tag);
                            }
                            if (!(str == "NoTheme"))
                            {
                               //cntrl.BackColor = ParaDine.GlobalVariables.theme_color;
                            }
                        }
                        for (int i = 0; i <= (cntrl.Controls.Count - 1); i++)
                        {
                            this.ApplyThemeChild(cntrl.Controls[i]);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Apply Theme", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
        public void Btn_GotFocus(object sender, EventArgs e)
        {
            ((Button) sender).FlatStyle = FlatStyle.Standard;
        }
        
        public void Btn_LostFocus(object sender, EventArgs e)
        {
            ((Button) sender).FlatStyle = FlatStyle.Flat;
        }
        
        public void LoadTheme()
        {
            if (ParaDine.GlobalVariables.FormTheme == 0)
            {
                ParaDine.GlobalVariables.SqlCmd = new SqlCommand("SELECT ISNULL(CURRENTTHEME,0) FROM GLB_SYSTEMSETTINGS", ParaDine.GlobalVariables.SqlConn);
                ParaDine.GlobalVariables.Theme = Convert.ToInt16(ParaDine.GlobalVariables.SqlCmd.ExecuteScalar());
            }
            else
            {
                ParaDine.GlobalVariables.Theme = ParaDine.GlobalVariables.FormTheme;
            }
            if (ParaDine.GlobalVariables.Theme == 1)
            {
                ParaDine.GlobalVariables.Theme_img = Image.FromFile(Application.StartupPath + @"\Theme_Homestead_main.gif");
                ParaDine.GlobalVariables.theme_color = Color.FromArgb(0xe8, 0xe7, 0xd7);
                ParaDine.GlobalVariables.Compulsory_Color = Color.FromArgb(0xe8, 0xea, 230);
            }
            else if (ParaDine.GlobalVariables.Theme == 2)
            {
                ParaDine.GlobalVariables.Theme_img = Image.FromFile(Application.StartupPath + @"\Theme_Metallic_Main.gif");
                ParaDine.GlobalVariables.theme_color = Color.FromArgb(0xe0, 0xe0, 0xe0);
                ParaDine.GlobalVariables.Compulsory_Color = Color.FromArgb(240, 240, 0xf9);
            }
            else if (ParaDine.GlobalVariables.Theme == 3)
            {
                ParaDine.GlobalVariables.Theme_img = Image.FromFile(Application.StartupPath + @"\Theme_Brick.gif");
                ParaDine.GlobalVariables.theme_color = Color.FromArgb(0xf5, 0xd8, 0xd8);
                ParaDine.GlobalVariables.Compulsory_Color = Color.FromArgb(0xfb, 0xf3, 0xfe);
            }
            else if (ParaDine.GlobalVariables.Theme == 4)
            {
                ParaDine.GlobalVariables.Theme_img = Image.FromFile(Application.StartupPath + @"\Theme_BlueMain.gif");
                ParaDine.GlobalVariables.theme_color = Color.FromArgb(0xd6, 0xe2, 250);
                ParaDine.GlobalVariables.Compulsory_Color = Color.FromArgb(0xd6, 0xe2, 250);
            }
        }
    }
}
