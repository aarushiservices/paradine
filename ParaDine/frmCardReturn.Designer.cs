﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine
{
    partial class frmCardReturn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new Container();
            this.label1 = new Label();
            this.nmTotCreditAmt = new NumControl();
            this.nmTotDebitAmt = new NumControl();
            this.pnlMain = new Panel();
            this.label2 = new Label();
            this.nmExpireAmount = new NumControl();
            this.dgTrans = new DataGridView();
            this.btnExit = new ParButton();
            this.btnTrans = new ParButton();
            this.txtCustomer = new TextBox();
            this.nmBalAmount = new NumControl();
            this.cmbCardType = new ComboBox();
            this.label6 = new Label();
            this.txtCardNumber = new TextBox();
            this.dtpReturnDate = new DateTimePicker();
            this.txtCardReturnId = new TextBox();
            this.TTip = new ToolTip(this.components);
            this.label3 = new Label();
            this.pnlMain.SuspendLayout();
            ((ISupportInitialize)this.dgTrans).BeginInit();
            base.SuspendLayout();
            this.label1.AutoSize = true;
            this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label1.Location = new Point(40, 0x6f);
            this.label1.Name = "label1";
            this.label1.Size = new Size(140, 13);
            this.label1.TabIndex = 0x21;
            this.label1.Text = "Card Balance to Return";
            this.nmTotCreditAmt.BackColor = Color.White;
            this.nmTotCreditAmt.BorderStyle = BorderStyle.FixedSingle;
            this.nmTotCreditAmt.DecimalRequired = true;
            this.nmTotCreditAmt.Font = new Font("Rupee Foradian", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.nmTotCreditAmt.Location = new Point(0x8a, 0x19b);
            this.nmTotCreditAmt.Name = "nmTotCreditAmt";
            this.nmTotCreditAmt.ReadOnly = true;
            this.nmTotCreditAmt.Size = new Size(100, 20);
            this.nmTotCreditAmt.SymbolRequired = true;
            this.nmTotCreditAmt.TabIndex = 7;
            this.nmTotCreditAmt.TabStop = false;
            this.nmTotCreditAmt.Text = "`0.00";
            this.nmTotCreditAmt.TextAlign = HorizontalAlignment.Right;
            int[] bits = new int[4];
            this.nmTotCreditAmt.Value = new decimal(bits);
            this.nmTotDebitAmt.BackColor = Color.White;
            this.nmTotDebitAmt.BorderStyle = BorderStyle.FixedSingle;
            this.nmTotDebitAmt.DecimalRequired = true;
            this.nmTotDebitAmt.Font = new Font("Rupee Foradian", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.nmTotDebitAmt.Location = new Point(0xf4, 0x19b);
            this.nmTotDebitAmt.Name = "nmTotDebitAmt";
            this.nmTotDebitAmt.ReadOnly = true;
            this.nmTotDebitAmt.Size = new Size(100, 20);
            this.nmTotDebitAmt.SymbolRequired = true;
            this.nmTotDebitAmt.TabIndex = 8;
            this.nmTotDebitAmt.TabStop = false;
            this.nmTotDebitAmt.Text = "`0.00";
            this.nmTotDebitAmt.TextAlign = HorizontalAlignment.Right;
           // bits = new int[4];
            this.nmTotDebitAmt.Value = new decimal(new int[4]);
            this.pnlMain.BackColor = Color.Transparent;
            this.pnlMain.BorderStyle = BorderStyle.FixedSingle;
            this.pnlMain.Controls.Add(this.label3);
            this.pnlMain.Controls.Add(this.label2);
            this.pnlMain.Controls.Add(this.nmExpireAmount);
            this.pnlMain.Controls.Add(this.label1);
            this.pnlMain.Controls.Add(this.nmTotCreditAmt);
            this.pnlMain.Controls.Add(this.nmTotDebitAmt);
            this.pnlMain.Controls.Add(this.dgTrans);
            this.pnlMain.Controls.Add(this.btnExit);
            this.pnlMain.Controls.Add(this.btnTrans);
            this.pnlMain.Controls.Add(this.txtCustomer);
            this.pnlMain.Controls.Add(this.nmBalAmount);
            this.pnlMain.Controls.Add(this.cmbCardType);
            this.pnlMain.Controls.Add(this.label6);
            this.pnlMain.Controls.Add(this.txtCardNumber);
            this.pnlMain.Controls.Add(this.dtpReturnDate);
            this.pnlMain.Controls.Add(this.txtCardReturnId);
            this.pnlMain.Dock = DockStyle.Fill;
            this.pnlMain.Location = new Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new Size(0x17f, 0x202);
            this.pnlMain.TabIndex = 1;
            this.label2.AutoSize = true;
            this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label2.Location = new Point(0xe4, 0x6f);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0x58, 13);
            this.label2.TabIndex = 0x23;
            this.label2.Text = "Expire Amount";
            this.nmExpireAmount.BackColor = Color.White;
            this.nmExpireAmount.BorderStyle = BorderStyle.FixedSingle;
            this.nmExpireAmount.DecimalRequired = true;
            this.nmExpireAmount.Font = new Font("Rupee Foradian", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.nmExpireAmount.Location = new Point(0xde, 0x7f);
            this.nmExpireAmount.Name = "nmExpireAmount";
            this.nmExpireAmount.ReadOnly = true;
            this.nmExpireAmount.Size = new Size(0x7a, 0x1a);
            this.nmExpireAmount.SymbolRequired = true;
            this.nmExpireAmount.TabIndex = 3;
            this.nmExpireAmount.TabStop = false;
            this.nmExpireAmount.Text = "`0.00";
            this.nmExpireAmount.TextAlign = HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.nmExpireAmount, "Please Enter Recharge Value");
           // bits = new int[4];
            this.nmExpireAmount.Value = new decimal(bits);
            this.dgTrans.AllowUserToAddRows = false;
            this.dgTrans.AllowUserToDeleteRows = false;
            this.dgTrans.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTrans.Location = new Point(0x2b, 0xdf);
            this.dgTrans.Name = "dgTrans";
            this.dgTrans.RowHeadersVisible = false;
            this.dgTrans.Size = new Size(0x12d, 0xb6);
            this.dgTrans.TabIndex = 6;
            this.btnExit.BackColor = SystemColors.ActiveBorder;
            this.btnExit.BackgroundImageLayout = ImageLayout.Stretch;
            this.btnExit.CausesValidation = false;
           //this.btnExit.DialogResult = DialogResult.Cancel;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = FlatStyle.Flat;
            this.btnExit.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnExit.Location = new Point(0xc5, 0x1da);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new Size(0x6d, 0x1b);
            this.btnExit.TabIndex = 8;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new EventHandler(this.btnExit_Click);
            this.btnTrans.BackColor = SystemColors.ActiveBorder;
            this.btnTrans.BackgroundImageLayout = ImageLayout.Stretch;
            this.btnTrans.FlatAppearance.BorderSize = 0;
            this.btnTrans.FlatStyle = FlatStyle.Flat;
            this.btnTrans.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnTrans.Location = new Point(0x49, 0x1da);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new Size(0x6d, 0x1b);
            this.btnTrans.TabIndex = 7;
            this.btnTrans.Text = "Trans";
            this.btnTrans.UseVisualStyleBackColor = false;
            this.btnTrans.Click += new EventHandler(this.btnTrans_Click);
            this.txtCustomer.BackColor = Color.White;
            this.txtCustomer.BorderStyle = BorderStyle.FixedSingle;
            this.txtCustomer.Font = new Font("Arial", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.txtCustomer.Location = new Point(0x2b, 0x9f);
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.ReadOnly = true;
            this.txtCustomer.Size = new Size(0x12d, 0x1a);
            this.txtCustomer.TabIndex = 4;
            this.txtCustomer.TabStop = false;
            this.nmBalAmount.BackColor = Color.White;
            this.nmBalAmount.BorderStyle = BorderStyle.FixedSingle;
            this.nmBalAmount.DecimalRequired = true;
            this.nmBalAmount.Font = new Font("Rupee Foradian", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.nmBalAmount.Location = new Point(0x2b, 0x7f);
            this.nmBalAmount.Name = "nmBalAmount";
            this.nmBalAmount.ReadOnly = true;
            this.nmBalAmount.Size = new Size(0x7a, 0x1a);
            this.nmBalAmount.SymbolRequired = true;
            this.nmBalAmount.TabIndex = 2;
            this.nmBalAmount.TabStop = false;
            this.nmBalAmount.Text = "`0.00";
            this.nmBalAmount.TextAlign = HorizontalAlignment.Right;
           // bits = new int[4];
            this.nmBalAmount.Value = new decimal(bits);
            this.cmbCardType.BackColor = Color.Ivory;
            this.cmbCardType.Font = new Font("Verdana", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.cmbCardType.FormattingEnabled = true;
            this.cmbCardType.Location = new Point(0x2b, 0xbf);
            this.cmbCardType.Name = "cmbCardType";
            this.cmbCardType.Size = new Size(0x12d, 0x1a);
            this.cmbCardType.TabIndex = 5;
            this.label6.AutoSize = true;
            this.label6.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label6.Location = new Point(40, 0x42);
            this.label6.Name = "label6";
            this.label6.Size = new Size(80, 13);
            this.label6.TabIndex = 0x1a;
            this.label6.Text = "Card Number";
            this.txtCardNumber.BackColor = Color.Ivory;
            this.txtCardNumber.BorderStyle = BorderStyle.FixedSingle;
            this.txtCardNumber.Font = new Font("Arial", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.txtCardNumber.Location = new Point(0x2b, 0x52);
            this.txtCardNumber.Name = "txtCardNumber";
            this.txtCardNumber.PasswordChar = '*';
            this.txtCardNumber.Size = new Size(0x12d, 0x1a);
            this.txtCardNumber.TabIndex = 1;
            this.txtCardNumber.Validating += new CancelEventHandler(this.txtCardNumber_Validating);
            this.dtpReturnDate.CustomFormat = "dd/MMMM/yyyy";
            this.dtpReturnDate.Enabled = false;
            this.dtpReturnDate.Format = DateTimePickerFormat.Custom;
            this.dtpReturnDate.Location = new Point(0x77, 0x58);
            this.dtpReturnDate.Name = "dtpReturnDate";
            this.dtpReturnDate.Size = new Size(0xa3, 20);
            this.dtpReturnDate.TabIndex = 1;
            this.dtpReturnDate.TabStop = false;
            this.txtCardReturnId.BorderStyle = BorderStyle.FixedSingle;
            this.txtCardReturnId.Enabled = false;
            this.txtCardReturnId.Location = new Point(0x2b, 0x58);
            this.txtCardReturnId.Name = "txtCardReturnId";
            this.txtCardReturnId.Size = new Size(70, 20);
            this.txtCardReturnId.TabIndex = 0;
            this.txtCardReturnId.TabStop = false;
            this.label3.AutoSize = true;
            this.label3.Font = new Font("Arial", 18f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label3.Location = new Point(0x26, 8);
            this.label3.Name = "label3";
            this.label3.Size = new Size(0x97, 0x1d);
            this.label3.TabIndex = 0x24;
            this.label3.Text = "Card Return";
            base.AutoScaleDimensions = new SizeF(6f, 13f);
           //base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x17f, 0x202);
            base.Controls.Add(this.pnlMain);
            base.Name = "frmCardReturn";
            base.ShowIcon = false;
            base.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "Card Return";
            base.Load += new EventHandler(this.frmCardReturn_Load);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            ((ISupportInitialize)this.dgTrans).EndInit();
            base.ResumeLayout(false);
        }
        #endregion
    }
}