﻿using ParaDinePrint;
using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine
{
    public partial class frmCardRecharge : Form
    {
        private ParButton btnExit;
        private ParButton btnTrans;
        private ComboBox cmbCardType;
       // private IContainer components = null;
        private DataGridView dgTrans;
        private DataSet DS = new DataSet();
        private DateTimePicker dtpTransDate;
        private Card EntCard = new Card();
        private CardTrans EntId = new CardTrans();
        private CardIssue EntIssue = new CardIssue();
        private Label label1;
        private Label label2;
        private Label label6;
        private NumControl nmBalAmount;
        private NumControl nmRechargeAmount;
        private NumControl nmTotCreditAmt;
        private NumControl nmTotDebitAmt;
        private PrintingFunctions pfunctions = new PrintingFunctions();
        private Panel pnlMain;
        private ClsColDosPrint[] prnItem;
        private ClsRptDosPrint prnrpt = new ClsRptDosPrint();
        private string rptname = "";
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private string StrSql = "";
        private ToolTip TTip;
        private TextBox txtCardNumber;
        private TextBox txtCardTransId;
        private TextBox txtCustomer;

        public frmCardRecharge(string Mode, int ID, SecurityClass paramSecurity)
        {
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes((long)ID);
            this.EntId.Security = paramSecurity;
            this.EntIssue = new CardIssue(this.EntId.CardIssueId);
            this.EntCard = new Card(this.EntIssue.CardId);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (ParaDine.GlobalValidations.ValidateFields(this.pnlMain, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str3 = this.btnTrans.Text.ToUpper();
                    if (str3 != null)
                    {
                        if (!(str3 == "&ADD"))
                        {
                            if (str3 == "&EDIT")
                            {
                                goto Label_0092;
                            }
                            if (str3 == "&DELETE")
                            {
                                goto Label_00A2;
                            }
                        }
                        else
                        {
                            this.EntId.Add(sqlTrans, true);
                            this.EntIssue.Modify(sqlTrans, false);
                        }
                    }
                    goto Label_00B4;
                Label_0092:
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_00B4;
                Label_00A2:
                    this.EntId.Delete(sqlTrans, true);
                Label_00B4:
                    sqlTrans.Commit();
                    this.StrSql = "SELECT * FROM RES_VW_CARDTRANS WHERE CARDTRANSID = '" + this.EntId.CardTransId.ToString() + "'";
                    ParaDine.GlobalFill.FillDataSet(this.StrSql, "CARDTRANS", this.DS, this.SDA);
                    this.rptname = "CARD_RECHARGE";
                    this.pfunctions.SETREPORTDATA(ref this.prnrpt, this.rptname, this.DS, this.SDA, ParaDine.GlobalVariables.PrinterId, ParaDine.GlobalVariables.SqlConn);
                    this.pfunctions.SETCOLDATA(ref this.prnItem, this.rptname, this.DS, this.SDA, ParaDine.GlobalVariables.PrinterId, ParaDine.GlobalVariables.SqlConn);
                    string str = "";
                    for (int i = 0; i <= (this.prnItem.Length - 1); i++)
                    {
                        string str2 = "";
                        if (this.prnItem[i].ISGroupHeader)
                        {
                            str2 = (this.prnItem[i].ColumnPosition + 1) + ",";
                        }
                        str = str + str2;
                    }
                    new ClsDosPrint(this.DS.Tables["CARDTRANS"], this.prnItem, this.prnrpt, ParaDine.GlobalVariables.SqlConn, ParaDine.GlobalVariables.PrinterId).PrintData("PRINTER", "");
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, exception.Source);
                    sqlTrans.Rollback();
                }
            }
        }  
        private void frmCardRecharge_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                this.RefreshData();
                this.LoadFields();
                ParaDine.GlobalFunctions.AddCompHandler(this.pnlMain);
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                base.Close();
            }
        }
        private void LoadEntities()
        {
            this.EntId.CardTransId = Convert.ToInt64(this.txtCardTransId.Tag);
            this.EntId.CardTransDate = Convert.ToDateTime(this.dtpTransDate.Value);
            this.EntId.CardIssueId = Convert.ToInt64(this.EntIssue.CardIssueId);
            this.EntId.FaceValue = 0.0;
            this.EntId.CreditAmount = Convert.ToDouble(this.nmRechargeAmount.Value);
            this.EntId.DebitAmount = 0.0;
            this.EntId.RefType = "RC";
            this.EntId.TerminalNo = ParaDine.GlobalVariables.TerminalNo;
            this.EntId.UserId = ParaDine.GlobalVariables.UserID;
            this.EntIssue.CardExpDate = ParaDine.GlobalVariables.BusinessDate.AddDays(Convert.ToDouble(new CardType(this.EntIssue.CardTypeId).ValidDays));
        }

        private void LoadFields()
        {
            if (this.EntId.CardTransId <= 0L)
            {
                this.txtCardTransId.Text = this.EntId.GetMaxCode;
                this.txtCardTransId.ReadOnly = true;
            }
            else
            {
                this.txtCardTransId.Text = Convert.ToString(this.EntId.CardTransId);
                this.txtCardTransId.Tag = this.EntId.CardTransId;
            }
            this.EntIssue = new CardIssue(this.EntId.CardIssueId);
            this.EntCard = new Card(this.EntIssue.CardId);
            this.dtpTransDate.Value = this.EntId.CardTransDate;
            this.txtCustomer.Text = this.EntIssue.CustomerName;
            this.cmbCardType.SelectedValue = this.EntIssue.CardTypeId;
            this.txtCardNumber.Text = this.EntCard.CardNumber;
            this.txtCardNumber.Tag = this.EntIssue.CardId;
            this.nmBalAmount.Value = Convert.ToDecimal(this.EntCard.CardBalance());
            this.nmTotCreditAmt.Value = Convert.ToDecimal(this.EntCard.TotCreditAmount());
            this.nmTotDebitAmt.Value = Convert.ToDecimal(this.EntCard.TotDebitAmount());
            this.dgTrans.DataSource = this.EntCard.CardTransaction();
            this.cmbCardType.SelectedValue = this.EntIssue.CardTypeId;
            this.nmRechargeAmount.Value = Convert.ToDecimal(this.EntId.CreditAmount);
        }

        private void RefreshData()
        {
            try
            {
                ParaDine.GlobalFill.FillCombo("SELECT CARDTYPEID, CARDTYPENAME FROM RES_CARDTYPE", this.cmbCardType);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in RefreshData");
            }
        }

        private void txtCardNumber_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                this.EntCard = new Card(this.txtCardNumber.Text);
                if (this.EntCard.CardId <= 0L)
                {
                    throw new Exception("InValid Card");
                }
                if (this.EntCard.CardIssueId == 0L)
                {
                    throw new Exception("Card Not Issued. Please Check..");
                }
                this.EntIssue = new CardIssue(this.EntCard.CardIssueId);
                this.nmBalAmount.Value = Convert.ToDecimal(this.EntCard.CardBalance());
                this.nmTotCreditAmt.Value = Convert.ToDecimal(this.EntCard.TotCreditAmount());
                this.nmTotDebitAmt.Value = Convert.ToDecimal(this.EntCard.TotDebitAmount());
                this.dgTrans.DataSource = this.EntCard.CardTransaction();
                this.cmbCardType.SelectedValue = this.EntIssue.CardTypeId;
                this.txtCustomer.Text = this.EntIssue.CustomerName;
                this.nmRechargeAmount.Focus();
                this.nmRechargeAmount.SelectAll();
            }
            catch (Exception exception)
            {
                e.Cancel = true;
                MessageBox.Show(exception.Message, "Error in Card Number Validation", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
    }
}
