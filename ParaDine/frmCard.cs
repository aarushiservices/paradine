﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine
{
    public partial class frmCard : Form
    {
        private Button btnExit;
        private Button btnTrans;
       // private IContainer components = null;
        private DataGridView dgViewCard;
        private DataTable DtCard = new DataTable("Card");
        private Card EntId = new Card();
        private Label label1;
        private Panel pnlDepartment;
        private ToolTip TTip;
        private TextBox txtCardNumber;

        public frmCard(string Mode, int ID, SecurityClass paramSecurity)
        {
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes((long)ID);
            this.EntId.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (GlobalValidations.ValidateFields(this.pnlDepartment, this.TTip))
            {
                SqlTransaction sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    foreach (DataRow row in this.DtCard.Rows)
                    {
                        this.LoadEntities(row);
                        if (this.EntId.CardId == 0L)
                        {
                            this.EntId.Add(sqlTrans, false);
                        }
                        else if (this.EntId.CardId > 0L)
                        {
                            this.EntId.Modify(sqlTrans, false);
                        }
                    }
                    sqlTrans.Commit();
                    MessageBox.Show("Successfully Cards are Updated", "Cards Entry");
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, exception.Source);
                    sqlTrans.Rollback();
                }
            }
        }
        private void frmCard_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                this.DtCard.Clear();
                this.DtCard.Columns.Add("CardId", typeof(long));
                this.DtCard.Columns.Add("CardNumber", typeof(string));
                this.DtCard.Columns.Add("InActive", typeof(bool));
                this.LoadFields();
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                base.Close();
            }
        }

  

        private void LoadEntities(DataRow dr)
        {
            this.EntId.CardId = Convert.ToInt32(dr["CARDID"]);
            this.EntId.CardNumber = Convert.ToString(dr["CARDNUMBER"]);
            this.EntId.Inactive = Convert.ToBoolean(dr["INACTIVE"]);
        }

        private void LoadFields()
        {
            if (this.EntId.CardId > 0L)
            {
                this.DtCard.Rows.Add(new object[] { this.EntId.CardId, this.EntId.CardNumber, this.EntId.Inactive });
                GlobalFill.FillGridView(this.dgViewCard, this.DtCard);
                this.SetColWidth();
            }
        }

        private void SetColWidth()
        {
            this.dgViewCard.Columns[0].Visible = false;
            this.dgViewCard.Columns[1].Width = 200;
            this.dgViewCard.Columns[2].Width = 100;
        }

        private void txtCardNumber_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    foreach (DataRow row in this.DtCard.Rows)
                    {
                        if (Convert.ToString(row["CARDNUMBER"]) == this.txtCardNumber.Text)
                        {
                            throw new Exception("Card Number Exists. Please Check");
                        }
                    }
                    if (this.EntId.CheckCardExists(this.txtCardNumber.Text))
                    {
                        throw new Exception("Card Number Exists. Please Check");
                    }
                    this.DtCard.Rows.Add(new object[] { 0, this.txtCardNumber.Text, false });
                    GlobalFill.FillGridView(this.dgViewCard, this.DtCard);
                    this.SetColWidth();
                    this.txtCardNumber.SelectAll();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Card Number keydown", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
    }
}
