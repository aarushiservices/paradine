﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine
{
    partial class frmShowCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new Container();
            this.label1 = new Label();
            this.nmTotCreditAmt = new NumControl();
            this.nmTotDebitAmt = new NumControl();
            this.pnlMain = new Panel();
            this.dgTrans = new DataGridView();
            this.btnExit = new ParButton();
            this.btnTrans = new ParButton();
            this.txtCustomer = new TextBox();
            this.nmBalAmount = new NumControl();
            this.cmbCardType = new ComboBox();
            this.label6 = new Label();
            this.txtCardNumber = new TextBox();
            this.TTip = new ToolTip(this.components);
            this.pnlMain.SuspendLayout();
            ((ISupportInitialize)this.dgTrans).BeginInit();
            base.SuspendLayout();
            this.label1.AutoSize = true;
            this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label1.Location = new Point(0x25, 0x4b);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x53, 13);
            this.label1.TabIndex = 0x21;
            this.label1.Text = "Card Balance";
            this.nmTotCreditAmt.BackColor = Color.White;
            this.nmTotCreditAmt.BorderStyle = BorderStyle.FixedSingle;
            this.nmTotCreditAmt.DecimalRequired = true;
            this.nmTotCreditAmt.Font = new Font("Rupee Foradian", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.nmTotCreditAmt.Location = new Point(0x87, 0x16b);
            this.nmTotCreditAmt.Name = "nmTotCreditAmt";
            this.nmTotCreditAmt.ReadOnly = true;
            this.nmTotCreditAmt.Size = new Size(100, 20);
            this.nmTotCreditAmt.SymbolRequired = true;
            this.nmTotCreditAmt.TabIndex = 7;
            this.nmTotCreditAmt.TabStop = false;
            this.nmTotCreditAmt.Text = "`0.00";
            this.nmTotCreditAmt.TextAlign = HorizontalAlignment.Right;
            int[] bits = new int[4];
            this.nmTotCreditAmt.Value = new decimal(bits);
            this.nmTotDebitAmt.BackColor = Color.White;
            this.nmTotDebitAmt.BorderStyle = BorderStyle.FixedSingle;
            this.nmTotDebitAmt.DecimalRequired = true;
            this.nmTotDebitAmt.Font = new Font("Rupee Foradian", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.nmTotDebitAmt.Location = new Point(0xf1, 0x16b);
            this.nmTotDebitAmt.Name = "nmTotDebitAmt";
            this.nmTotDebitAmt.ReadOnly = true;
            this.nmTotDebitAmt.Size = new Size(100, 20);
            this.nmTotDebitAmt.SymbolRequired = true;
            this.nmTotDebitAmt.TabIndex = 8;
            this.nmTotDebitAmt.TabStop = false;
            this.nmTotDebitAmt.Text = "`0.00";
            this.nmTotDebitAmt.TextAlign = HorizontalAlignment.Right;
            //bits = new int[4];
            this.nmTotDebitAmt.Value = new decimal(bits);
            this.pnlMain.BackColor = Color.Transparent;
            this.pnlMain.BorderStyle = BorderStyle.FixedSingle;
            this.pnlMain.Controls.Add(this.label1);
            this.pnlMain.Controls.Add(this.nmTotCreditAmt);
            this.pnlMain.Controls.Add(this.nmTotDebitAmt);
            this.pnlMain.Controls.Add(this.dgTrans);
            this.pnlMain.Controls.Add(this.btnExit);
            this.pnlMain.Controls.Add(this.btnTrans);
            this.pnlMain.Controls.Add(this.txtCustomer);
            this.pnlMain.Controls.Add(this.nmBalAmount);
            this.pnlMain.Controls.Add(this.cmbCardType);
            this.pnlMain.Controls.Add(this.label6);
            this.pnlMain.Controls.Add(this.txtCardNumber);
            this.pnlMain.Dock = DockStyle.Fill;
            this.pnlMain.Location = new Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new Size(0x180, 0x1c1);
            this.pnlMain.TabIndex = 1;
            this.dgTrans.AllowUserToAddRows = false;
            this.dgTrans.AllowUserToDeleteRows = false;
            this.dgTrans.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTrans.Location = new Point(40, 0xaf);
            this.dgTrans.Name = "dgTrans";
            this.dgTrans.RowHeadersVisible = false;
            this.dgTrans.Size = new Size(0x12d, 0xb6);
            this.dgTrans.TabIndex = 6;
            this.btnExit.BackColor = Color.Teal;
            this.btnExit.BackgroundImageLayout = ImageLayout.Stretch;
            this.btnExit.CausesValidation = false;
            //this.btnExit.DialogResult = DialogResult.Cancel;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = FlatStyle.Flat;
            this.btnExit.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnExit.Location = new Point(0xcf, 0x195);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new Size(0x6d, 0x1b);
            this.btnExit.TabIndex = 0x20;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new EventHandler(this.btnExit_Click);
            this.btnTrans.BackColor = Color.Teal;
            this.btnTrans.BackgroundImageLayout = ImageLayout.Stretch;
            this.btnTrans.CausesValidation = false;
            this.btnTrans.FlatAppearance.BorderSize = 0;
            this.btnTrans.FlatStyle = FlatStyle.Flat;
            this.btnTrans.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnTrans.Location = new Point(0x53, 0x195);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new Size(0x6d, 0x1b);
            this.btnTrans.TabIndex = 0x1f;
            this.btnTrans.Text = "&Accept";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new EventHandler(this.btnTrans_Click);
            this.txtCustomer.BackColor = Color.White;
            this.txtCustomer.BorderStyle = BorderStyle.FixedSingle;
            this.txtCustomer.Font = new Font("Arial", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.txtCustomer.Location = new Point(40, 0x6f);
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.ReadOnly = true;
            this.txtCustomer.Size = new Size(0x12d, 0x1a);
            this.txtCustomer.TabIndex = 4;
            this.txtCustomer.TabStop = false;
            this.nmBalAmount.BackColor = Color.White;
            this.nmBalAmount.BorderStyle = BorderStyle.FixedSingle;
            this.nmBalAmount.DecimalRequired = true;
            this.nmBalAmount.Font = new Font("Rupee Foradian", 14.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.nmBalAmount.Location = new Point(0x87, 0x42);
            this.nmBalAmount.Name = "nmBalAmount";
            this.nmBalAmount.ReadOnly = true;
            this.nmBalAmount.Size = new Size(0xce, 30);
            this.nmBalAmount.SymbolRequired = true;
            this.nmBalAmount.TabIndex = 10;
            this.nmBalAmount.TabStop = false;
            this.nmBalAmount.Text = "`0.00";
            this.nmBalAmount.TextAlign = HorizontalAlignment.Right;
            //bits = new int[4];
            this.nmBalAmount.Value = new decimal(bits);
            this.cmbCardType.BackColor = Color.Ivory;
            this.cmbCardType.Font = new Font("Verdana", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.cmbCardType.FormattingEnabled = true;
            this.cmbCardType.Location = new Point(40, 0x8f);
            this.cmbCardType.Name = "cmbCardType";
            this.cmbCardType.Size = new Size(0x12d, 0x1a);
            this.cmbCardType.TabIndex = 5;
            this.label6.AutoSize = true;
            this.label6.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label6.Location = new Point(0x25, 0x12);
            this.label6.Name = "label6";
            this.label6.Size = new Size(80, 13);
            this.label6.TabIndex = 0x1a;
            this.label6.Text = "Card Number";
            this.txtCardNumber.BackColor = Color.Ivory;
            this.txtCardNumber.BorderStyle = BorderStyle.FixedSingle;
            this.txtCardNumber.Font = new Font("Arial", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.txtCardNumber.Location = new Point(40, 0x22);
            this.txtCardNumber.Name = "txtCardNumber";
            this.txtCardNumber.PasswordChar = '*';
            this.txtCardNumber.Size = new Size(0x12d, 0x1a);
            this.txtCardNumber.TabIndex = 3;
            this.txtCardNumber.KeyDown += new KeyEventHandler(this.txtCardNumber_KeyDown);
            this.txtCardNumber.Validating += new CancelEventHandler(this.txtCardNumber_Validating);
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            //base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x180, 0x1c1);
            base.ControlBox = false;
            base.Controls.Add(this.pnlMain);
            base.Name = "frmShowCard";
            base.ShowIcon = false;
            base.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "Show Card";
            base.Load += new EventHandler(this.frmShowCard_Load);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            ((ISupportInitialize)this.dgTrans).EndInit();
            base.ResumeLayout(false);
        }
        #endregion
    }
}