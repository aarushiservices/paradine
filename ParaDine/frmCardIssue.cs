﻿using ParaDinePrint;
using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine
{
    public partial class frmCardIssue : Form
    {
        private ParButton btnExit;
        private ParButton btnTrans;
        private ComboBox cmbCardType;
       //private IContainer components = null;
        private DataSet DS = new DataSet();
        private DateTimePicker dtpCardExpDate;
        private DateTimePicker dtpCardIssueDate;
        private Card EntCard = new Card();
        private CardType EntCardType = new CardType();
        private CardIssue EntId = new CardIssue();
        private CardTrans EntTrans = new CardTrans();
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private NumControl nmCardAmount;
        private NumControl nmCreditAmount;
        private NumControl nmFaceValue;
        private PrintingFunctions pfunctions = new PrintingFunctions();
        private Panel pnlMain;
        private ClsColDosPrint[] prnItem;
        private ClsRptDosPrint prnrpt = new ClsRptDosPrint();
        private string rptname = "";
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private string StrSql = "";
        private ToolTip TTip;
        private TextBox txtCardNumber;
        private TextBox txtCustomer;
        private TextBox txtIssueNo;
        private TextBox txtMobileNo;

        public frmCardIssue(string Mode, int ID, SecurityClass paramSecurity)
        {
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes((long)ID);
            this.EntId.Security = paramSecurity;
            this.EntCard = new Card(this.EntId.CardId);
            this.EntCardType = new CardType(this.EntId.CardTypeId);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (ParaDine.GlobalValidations.ValidateFields(this.pnlMain, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str3 = this.btnTrans.Text.ToUpper();
                    if (str3 != null)
                    {
                        if (!(str3 == "&ADD"))
                        {
                            if (str3 == "&EDIT")
                            {
                                goto Label_00F8;
                            }
                            if (str3 == "&DELETE")
                            {
                                goto Label_0108;
                            }
                        }
                        else
                        {
                            this.EntId.CardIssueId = this.EntId.Add(sqlTrans, true);
                            this.EntCard.CardIssueId = this.EntId.CardIssueId;
                            this.EntCard.Modify(sqlTrans, false);
                            this.EntTrans.CardIssueId = this.EntId.CardIssueId;
                            this.EntTrans.RefId = this.EntId.CardIssueId;
                            this.EntTrans.Add(sqlTrans, false);
                        }
                    }
                    goto Label_011A;
                Label_00F8:
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_011A;
                Label_0108:
                    this.EntId.Delete(sqlTrans, true);
                Label_011A:
                    sqlTrans.Commit();
                    this.StrSql = "SELECT * FROM RES_VW_CARDISSUE WHERE CARDISSUEID = '" + this.EntId.CardIssueId.ToString() + "'";
                    ParaDine.GlobalFill.FillDataSet(this.StrSql, "CARDISSUE", this.DS, this.SDA);
                    this.rptname = "CARD_ISSUE";
                    this.pfunctions.SETREPORTDATA(ref this.prnrpt, this.rptname, this.DS, this.SDA, ParaDine.GlobalVariables.PrinterId, ParaDine.GlobalVariables.SqlConn);
                    bool res = this.pfunctions.SETCOLDATA(ref this.prnItem, this.rptname, this.DS, this.SDA, ParaDine.GlobalVariables.PrinterId, ParaDine.GlobalVariables.SqlConn);
                    string str = "";
                    for (int i = 0; i <= (this.prnItem.Length - 1); i++)
                    {
                        string str2 = "";
                        if (this.prnItem[i].ISGroupHeader)
                        {
                            str2 = (this.prnItem[i].ColumnPosition + 1) + ",";
                        }
                        str = str + str2;
                    }
                    new ClsDosPrint(this.DS.Tables["CARDISSUE"], this.prnItem, this.prnrpt, ParaDine.GlobalVariables.SqlConn, ParaDine.GlobalVariables.PrinterId).PrintData("PRINTER", "");
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, exception.Source);
                    sqlTrans.Rollback();
                }
            }
        }

        private void cmbCardType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            this.EntCardType = new CardType(Convert.ToInt32(this.cmbCardType.SelectedValue));
            this.nmCardAmount.Value = Convert.ToDecimal(this.EntCardType.CardAmount);
            this.nmCreditAmount.Value = Convert.ToDecimal((double)(this.EntCardType.CardAmount - this.EntCardType.FaceValue));
            this.nmFaceValue.Value = Convert.ToDecimal(this.EntCardType.FaceValue);
            this.dtpCardExpDate.Value = this.dtpCardIssueDate.Value.AddDays(Convert.ToDouble(this.EntCardType.ValidDays));
            this.txtCardNumber.Focus();
            this.txtCardNumber.SelectAll();
        }

      

        private void frmCardIssue_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                this.RefreshData();
                this.LoadFields();
                ParaDine.GlobalFunctions.AddCompHandler(this.pnlMain);
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                base.Close();
            }
        }
        private void LoadEntities()
        {
            this.EntId.CardIssueId = Convert.ToInt64(this.txtIssueNo.Tag);
            this.EntId.CardIssueNo = Convert.ToString(this.txtIssueNo.Text);
            this.EntId.CardIssueDate = Convert.ToDateTime(this.dtpCardIssueDate.Value);
            this.EntId.CardExpDate = Convert.ToDateTime(this.dtpCardExpDate.Value);
            this.EntId.CardTypeId = Convert.ToInt32(this.cmbCardType.SelectedValue);
            this.EntId.CardId = Convert.ToInt32(this.EntCard.CardId);
            this.EntId.CustomerName = Convert.ToString(this.txtCustomer.Text);
            this.EntId.MobileNo = Convert.ToString(this.txtMobileNo.Text);
            this.EntTrans.CardTransId = 0L;
            this.EntTrans.CardIssueId = 0L;
            this.EntTrans.FaceValue = Convert.ToDouble(this.nmFaceValue.Value);
            this.EntTrans.CreditAmount = Convert.ToDouble(this.nmCreditAmount.Value);
            this.EntTrans.DebitAmount = 0.0;
            this.EntTrans.RefType = "CI";
            this.EntTrans.UserId = ParaDine.GlobalVariables.UserID;
            this.EntTrans.TerminalNo = ParaDine.GlobalVariables.TerminalNo;
        }

        private void LoadFields()
        {
            if (this.EntId.CardIssueId <= 0L)
            {
                this.txtIssueNo.Text = this.EntId.GetMaxCode;
                this.txtIssueNo.ReadOnly = true;
            }
            else
            {
                this.txtIssueNo.Text = Convert.ToString(this.EntId.CardIssueNo);
                this.txtIssueNo.Tag = this.EntId.CardIssueId;
            }
            this.dtpCardIssueDate.Value = this.EntId.CardIssueDate;
            this.dtpCardExpDate.Value = this.EntId.CardExpDate;
            this.txtCustomer.Text = this.EntId.CustomerName;
            this.txtMobileNo.Text = this.EntId.MobileNo;
            this.cmbCardType.SelectedValue = this.EntId.CardTypeId;
            this.txtCardNumber.Text = this.EntCard.CardNumber;
            this.nmCardAmount.Value = Convert.ToDecimal(this.EntCardType.CardAmount);
            this.nmCreditAmount.Value = Convert.ToDecimal((double)(this.EntCardType.CardAmount - this.EntCardType.FaceValue));
            this.nmFaceValue.Value = Convert.ToDecimal(this.EntCardType.FaceValue);
        }

        private void RefreshData()
        {
            try
            {
                ParaDine.GlobalFill.FillCombo("SELECT CARDTYPEID, CARDTYPENAME FROM RES_CARDTYPE", this.cmbCardType);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in RefreshData");
            }
        }

        private void txtCardNumber_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.txtCardNumber.Text != "")
                {
                    this.EntCard = new Card(this.txtCardNumber.Text);
                    if (this.EntCard.CardId <= 0L)
                    {
                        throw new Exception("InValid Card");
                    }
                    if (this.EntCard.CardIssueId > 0L)
                    {
                        throw new Exception("Running Card Cannot be Issued");
                    }
                }
            }
            catch (Exception exception)
            {
                e.Cancel = true;
                this.txtCardNumber.SelectAll();
                MessageBox.Show(exception.Message, "Error in Card Number Validation");
            }
        }
    }
}
