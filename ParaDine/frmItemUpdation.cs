﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine
{
    public partial class frmItemUpdation : Form
    {
        private Button btnUpdate;
        //private IContainer components = null;
        private DataSet DS = new DataSet();
        private Label label1;
        private ProgressBar prgBar;
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private SqlCommand SqlCmd = new SqlCommand();

        public frmItemUpdation()
        {
            this.InitializeComponent();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            string str;
            Exception exception;
            try
            {
                SqlConnection selectConnection = new SqlConnection("data source = (Local) ; initial catalog = Restaurant; Trusted_Connection=true;");
                selectConnection.Open();
                str = "";
                str = "SELECT * FROM RES_ITEMMASTER";
                this.SDA = new SqlDataAdapter(str, selectConnection);
                this.SDA.Fill(this.DS, "ITEMMASTER");
                str = "SELECT * FROM RES_CATEGORY";
                this.SDA = new SqlDataAdapter(str, selectConnection);
                this.SDA.Fill(this.DS, "CATEGORY");
                str = "SELECT * FROM RES_DEPARTMENT";
                this.SDA = new SqlDataAdapter(str, selectConnection);
                this.SDA.Fill(this.DS, "DEPARTMENT");
                str = "SELECT * FROM RES_SECTION";
                this.SDA = new SqlDataAdapter(str, selectConnection);
                this.SDA.Fill(this.DS, "SECTION");
                str = "SELECT * FROM RES_UOM";
                this.SDA = new SqlDataAdapter(str, selectConnection);
                this.SDA.Fill(this.DS, "UOM");
                str = "SELECT * FROM RES_TAXMASTER";
                this.SDA = new SqlDataAdapter(str, selectConnection);
                this.SDA.Fill(this.DS, "TAX");
            }
            catch (Exception exception1)
            {
                exception = exception1;
                MessageBox.Show(exception.Message, "Error in Selecting Data");
            }
            GlobalFunctions.GetConnection();
            GlobalVariables.UserID = 1;
            SqlTransaction sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
            try
            {
                str = "";
                str = "DELETE FROM RES_ITEMUNITPRICE\nDELETE FROM RES_ITEMUNITSPEC\nDELETE FROM RES_ITEM\nDELETE FROM RES_SECTION\nDELETE FROM RES_CATEGORY\nDELETE FROM RES_DEPARTMENT\nDELETE FROM RES_UOM\nDELETE FROM RES_TAX";
                this.SqlCmd = new SqlCommand();
                this.SqlCmd.Connection = GlobalVariables.SqlConn;
                this.SqlCmd.CommandType = CommandType.Text;
                this.SqlCmd.Transaction = sqlTrans;
                this.SqlCmd.CommandText = str;
                this.SqlCmd.ExecuteNonQuery();
                this.label1.Text = "UPDATING TAX";
                this.label1.Refresh();
                this.prgBar.Maximum = this.DS.Tables["TAX"].Rows.Count;
                this.prgBar.Value = 0;
                this.prgBar.Refresh();
                foreach (DataRow row in this.DS.Tables["TAX"].Rows)
                {
                    new Tax { TaxID = Convert.ToInt32(row["TAXMASTERID"]), TaxName = Convert.ToString(row["TAXMASTER"]) }.Add(sqlTrans, false);
                    this.prgBar.Increment(1);
                }
                this.label1.Text = "UPDATING UOM";
                this.label1.Refresh();
                this.prgBar.Maximum = this.DS.Tables["UOM"].Rows.Count;
                this.prgBar.Value = 0;
                this.prgBar.Refresh();
                foreach (DataRow row in this.DS.Tables["UOM"].Rows)
                {
                    new UOM { UOMID = Convert.ToInt32(row["UOMID"]), UOMName = Convert.ToString(row["UNITNAME"]) }.Add(sqlTrans, false);
                    this.prgBar.Increment(1);
                }
                this.label1.Text = "UPDATING DEPARTMENT";
                this.label1.Refresh();
                this.prgBar.Maximum = this.DS.Tables["DEPARTMENT"].Rows.Count;
                this.prgBar.Value = 0;
                this.prgBar.Refresh();
                foreach (DataRow row in this.DS.Tables["DEPARTMENT"].Rows)
                {
                    new Department { DepartmentID = Convert.ToInt32(row["DEPARTMENTID"]), DepartmentName = Convert.ToString(row["DEPARTMENT"]) }.Add(sqlTrans, false);
                    this.prgBar.Increment(1);
                }
                this.label1.Text = "UPDATING CATEGORY";
                this.label1.Refresh();
                this.prgBar.Maximum = this.DS.Tables["CATEGORY"].Rows.Count;
                this.prgBar.Value = 0;
                this.prgBar.Refresh();
                foreach (DataRow row in this.DS.Tables["CATEGORY"].Rows)
                {
                    new Category { CategoryID = Convert.ToInt32(row["CATEGORYID"]), CategoryName = Convert.ToString(row["CATEGORYNAME"]), DepartmentID = Convert.ToInt32(row["DEPARTMENTID"]) }.Add(sqlTrans, false);
                    this.prgBar.Increment(1);
                }
                this.label1.Text = "UPDATING SECTION";
                this.label1.Refresh();
                this.prgBar.Maximum = this.DS.Tables["SECTION"].Rows.Count;
                this.prgBar.Value = 0;
                this.prgBar.Refresh();
                foreach (DataRow row in this.DS.Tables["SECTION"].Rows)
                {
                    new Section { SectionID = Convert.ToInt32(row["SECTIONID"]), SectionName = Convert.ToString(row["SECTIONNAME"]), PrinterName = Convert.ToString(row["PRINTERNAME"]) }.Add(sqlTrans, false);
                    this.prgBar.Increment(1);
                }
                this.label1.Text = "UPDATING ITEM";
                this.label1.Refresh();
                this.prgBar.Maximum = this.DS.Tables["ITEMMASTER"].Rows.Count;
                this.prgBar.Value = 0;
                this.prgBar.Refresh();
                foreach (DataRow row in this.DS.Tables["ITEMMASTER"].Rows)
                {
                    Item item = new Item
                    {
                        ItemID = Convert.ToInt32(row["ITEMID"]),
                        ItemCode = Convert.ToString(row["ITEMCODE"]),
                        ItemName = Convert.ToString(row["ITEMNAME"]),
                        CategoryID = Convert.ToInt32(row["CATEGORYID"]),
                        SectionID = Convert.ToInt32(row["SECTIONID"]),
                        TaxID = Convert.ToInt32(row["TAXID"]),
                        SalePrice = Convert.ToDouble(row["UNITPRICE"]),
                        UOMID = Convert.ToInt32(row["UNITID"]),
                        NonInventory = true,
                        CostPrice = Convert.ToDouble(row["COSTPRICE"])
                    };
                    ItemUnitPrice varEntityItemUnitPrice = new ItemUnitPrice
                    {
                        StockPointID = 1,
                        UOMID = Convert.ToInt32(row["UNITID"]),
                        SalePrice = Convert.ToDouble(row["UNITPRICE"]),
                        Added = true
                    };
                    item.ItemPriceCollection.Add(varEntityItemUnitPrice);
                    item.Add(sqlTrans, false);
                    this.prgBar.Increment(1);
                }
                sqlTrans.Commit();
            }
            catch (Exception exception2)
            {
                exception = exception2;
                MessageBox.Show(exception.Message, "Error in Update Data");
                sqlTrans.Rollback();
            }
        } 
    }
}
