// Generated by .NET Reflector from D:\Paradine\ParaDine\ParaDine.exe
namespace ParaDine
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    
    public class Hint : MasterBase
    {
        public short HintID;
        public string HintQuestion;
        
        public Hint()
        {

        }       
        public Hint(short ID)
        {
            this.HintID = ID;
            this.LoadAttributes((long) ID);
        }
        
        public override long GetExecuteCommand(string TransType, SqlTransaction SqlTrans)
        {
            base.SqlCmd = new SqlCommand();
            base.SqlCmd.Connection = GlobalVariables.SqlConn;
            base.SqlCmd.Transaction = SqlTrans;
            base.SqlCmd.CommandText = "GLB_PROC_HINT";
            base.SqlCmd.CommandType = CommandType.StoredProcedure;
            base.SqlCmd.Parameters.AddWithValue("@TRANSTYPE", TransType);
            base.SqlCmd.Parameters.AddWithValue("@HINTID", this.HintID);
            base.SqlCmd.Parameters.AddWithValue("@HINT", this.HintQuestion);
            SqlParameter parameter = base.SqlCmd.Parameters.Add("RetVal", SqlDbType.Int);
            parameter.Direction = ParameterDirection.ReturnValue;
            if (base.SqlCmd.ExecuteNonQuery() > 0)
            {
                this.HintID = Convert.ToInt16(parameter.Value);
                base.InsertLogDetails("HINT", Convert.ToInt64(parameter.Value), GlobalVariables.UserID, GlobalVariables.BusinessDate, TransType, this.HintQuestion, SqlTrans);
                return (long) Convert.ToInt32(parameter.Value);
            }
            return 0L;
        }
        
        public override string GetViewQuery(string condStr)
        {
            base.StrSql = "SELECT * FROM GLB_HINT";
            if (condStr.Trim() != "")
            {
                base.StrSql = base.StrSql + " WHERE " + condStr;
            }
            return base.StrSql;
        }
        
        public override void LoadAttributes(long ID)
        {
            base.StrSql = "SELECT * FROM GLB_HINT WHERE HINTID = " + ID;
            base.SqlCmd = new SqlCommand(base.StrSql, GlobalVariables.SqlConn);
            SqlDataReader reader = base.SqlCmd.ExecuteReader();
            if (reader.Read())
            {
                this.HintID = Convert.ToInt16(reader["HINTID"]);
                this.HintQuestion = Convert.ToString(reader["HINT"]);
            }
            reader.Close();
        }
    }
}
