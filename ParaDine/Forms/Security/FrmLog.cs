﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Security
{
    public partial class FrmLog : Form
    {
        private Button btnClearLog;
        private Button btnOk;
       //private IContainer components = null;
        private DataGridView datagrdLog;
        private Label label1;
        private Label label2;
        private LogCollection logc = new LogCollection();
        private NumericUpDown numUpDwn;
        private Panel pnlButtons;
        private Panel pnlHistoryPeriod;
        private Systemsettings sys = new Systemsettings();

        public FrmLog()
        {
            this.InitializeComponent();
        }
        private void btnClearLog_Click(object sender, EventArgs e)
        {
            this.logc.ClearAll();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.sys.History = Convert.ToInt16(this.numUpDwn.Value);
            this.sys.Dispose();
            base.Close();
        }
        private void FrmHistorySettings_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.loadLogDetails();
        }
        private void loadLogDetails()
        {
            this.datagrdLog.DataSource = this.logc.GetCollectionTable();
        }
    }
}
