﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Security
{
    partial class FrmLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.btnOk = new System.Windows.Forms.Button();
            this.pnlHistoryPeriod = new System.Windows.Forms.Panel();
            this.btnClearLog = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.numUpDwn = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.datagrdLog = new System.Windows.Forms.DataGridView();
            this.pnlButtons.SuspendLayout();
            this.pnlHistoryPeriod.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDwn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datagrdLog)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlButtons
            // 
            this.pnlButtons.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlButtons.Controls.Add(this.btnOk);
            this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlButtons.Location = new System.Drawing.Point(0, 475);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(696, 37);
            this.pnlButtons.TabIndex = 0;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(306, 3);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(85, 31);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // pnlHistoryPeriod
            // 
            this.pnlHistoryPeriod.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlHistoryPeriod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlHistoryPeriod.Controls.Add(this.btnClearLog);
            this.pnlHistoryPeriod.Controls.Add(this.label2);
            this.pnlHistoryPeriod.Controls.Add(this.numUpDwn);
            this.pnlHistoryPeriod.Controls.Add(this.label1);
            this.pnlHistoryPeriod.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlHistoryPeriod.Location = new System.Drawing.Point(0, 440);
            this.pnlHistoryPeriod.Name = "pnlHistoryPeriod";
            this.pnlHistoryPeriod.Size = new System.Drawing.Size(696, 35);
            this.pnlHistoryPeriod.TabIndex = 0;
            // 
            // btnClearLog
            // 
            this.btnClearLog.Location = new System.Drawing.Point(564, 3);
            this.btnClearLog.Name = "btnClearLog";
            this.btnClearLog.Size = new System.Drawing.Size(128, 27);
            this.btnClearLog.TabIndex = 4;
            this.btnClearLog.Text = "&Clear Log";
            this.btnClearLog.UseVisualStyleBackColor = true;
            this.btnClearLog.Click += new System.EventHandler(this.btnClearLog_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(238, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Day(s).";
            // 
            // numUpDwn
            // 
            this.numUpDwn.BackColor = System.Drawing.Color.White;
            this.numUpDwn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUpDwn.Location = new System.Drawing.Point(192, 7);
            this.numUpDwn.Maximum = new decimal(new int[] {
            75,
            0,
            0,
            0});
            this.numUpDwn.Minimum = new decimal(new int[] {
            75,
            0,
            0,
            0});
            this.numUpDwn.Name = "numUpDwn";
            this.numUpDwn.Size = new System.Drawing.Size(44, 20);
            this.numUpDwn.TabIndex = 1;
            this.numUpDwn.Value = new decimal(new int[] {
            75,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(93, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Keep log details for ";
            // 
            // datagrdLog
            // 
            this.datagrdLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datagrdLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.datagrdLog.Location = new System.Drawing.Point(0, 0);
            this.datagrdLog.Name = "datagrdLog";
            this.datagrdLog.Size = new System.Drawing.Size(696, 440);
            this.datagrdLog.TabIndex = 1;
            // 
            // FrmLog
            // 
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(696, 512);
            this.Controls.Add(this.datagrdLog);
            this.Controls.Add(this.pnlHistoryPeriod);
            this.Controls.Add(this.pnlButtons);
            this.Name = "FrmLog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Log Settings";
            this.Load += new System.EventHandler(this.FrmHistorySettings_Load);
            this.pnlButtons.ResumeLayout(false);
            this.pnlHistoryPeriod.ResumeLayout(false);
            this.pnlHistoryPeriod.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDwn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datagrdLog)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
    }
}