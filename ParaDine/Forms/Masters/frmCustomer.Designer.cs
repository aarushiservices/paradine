﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Masters
{
    partial class frmCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlCustomer = new System.Windows.Forms.Panel();
            this.chkInActive = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.gvCreditLimit = new ParaSysCom.NumControl();
            this.label6 = new System.Windows.Forms.Label();
            this.gvDiscPerc = new ParaSysCom.NumControl();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbGender = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtContNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCustName = new System.Windows.Forms.TextBox();
            this.btnTrans = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCustCode = new System.Windows.Forms.TextBox();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.pnlCustomer.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(157, 303);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 10;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pnlCustomer
            // 
            this.pnlCustomer.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlCustomer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCustomer.Controls.Add(this.chkInActive);
            this.pnlCustomer.Controls.Add(this.label8);
            this.pnlCustomer.Controls.Add(this.txtRemarks);
            this.pnlCustomer.Controls.Add(this.label7);
            this.pnlCustomer.Controls.Add(this.gvCreditLimit);
            this.pnlCustomer.Controls.Add(this.label6);
            this.pnlCustomer.Controls.Add(this.gvDiscPerc);
            this.pnlCustomer.Controls.Add(this.label5);
            this.pnlCustomer.Controls.Add(this.cmbGender);
            this.pnlCustomer.Controls.Add(this.label4);
            this.pnlCustomer.Controls.Add(this.txtContNo);
            this.pnlCustomer.Controls.Add(this.label3);
            this.pnlCustomer.Controls.Add(this.txtAddress);
            this.pnlCustomer.Controls.Add(this.label2);
            this.pnlCustomer.Controls.Add(this.txtCustName);
            this.pnlCustomer.Controls.Add(this.btnExit);
            this.pnlCustomer.Controls.Add(this.btnTrans);
            this.pnlCustomer.Controls.Add(this.label1);
            this.pnlCustomer.Controls.Add(this.txtCustCode);
            this.pnlCustomer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCustomer.Location = new System.Drawing.Point(0, 0);
            this.pnlCustomer.Name = "pnlCustomer";
            this.pnlCustomer.Size = new System.Drawing.Size(299, 341);
            this.pnlCustomer.TabIndex = 0;
            // 
            // chkInActive
            // 
            this.chkInActive.AutoSize = true;
            this.chkInActive.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkInActive.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInActive.Location = new System.Drawing.Point(76, 272);
            this.chkInActive.Name = "chkInActive";
            this.chkInActive.Size = new System.Drawing.Size(70, 17);
            this.chkInActive.TabIndex = 8;
            this.chkInActive.Text = "InActive";
            this.chkInActive.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 230);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Remarks :";
            // 
            // txtRemarks
            // 
            this.txtRemarks.BackColor = System.Drawing.Color.White;
            this.txtRemarks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRemarks.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRemarks.Location = new System.Drawing.Point(76, 228);
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(210, 38);
            this.txtRemarks.TabIndex = 7;
            this.TTip.SetToolTip(this.txtRemarks, "Please Enter Any Remarks");
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 202);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Credit Limit :";
            // 
            // gvCreditLimit
            // 
            this.gvCreditLimit.DecimalRequired = true;
            this.gvCreditLimit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvCreditLimit.Location = new System.Drawing.Point(76, 202);
            this.gvCreditLimit.Name = "gvCreditLimit";
            this.gvCreditLimit.Size = new System.Drawing.Size(95, 20);
            this.gvCreditLimit.SymbolRequired = true;
            this.gvCreditLimit.TabIndex = 6;
            this.gvCreditLimit.Text = "`0.00";
            this.gvCreditLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvCreditLimit, "Enter Credit Limit");
            this.gvCreditLimit.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 176);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Discount% :";
            // 
            // gvDiscPerc
            // 
            this.gvDiscPerc.DecimalRequired = true;
            this.gvDiscPerc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvDiscPerc.Location = new System.Drawing.Point(76, 176);
            this.gvDiscPerc.Name = "gvDiscPerc";
            this.gvDiscPerc.Size = new System.Drawing.Size(95, 20);
            this.gvDiscPerc.SymbolRequired = true;
            this.gvDiscPerc.TabIndex = 5;
            this.gvDiscPerc.Text = "`0.00";
            this.gvDiscPerc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvDiscPerc, "Enter Discount Percentage");
            this.gvDiscPerc.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Gender :";
            // 
            // cmbGender
            // 
            this.cmbGender.BackColor = System.Drawing.Color.Ivory;
            this.cmbGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGender.FormattingEnabled = true;
            this.cmbGender.Location = new System.Drawing.Point(76, 149);
            this.cmbGender.Name = "cmbGender";
            this.cmbGender.Size = new System.Drawing.Size(69, 21);
            this.cmbGender.TabIndex = 4;
            this.TTip.SetToolTip(this.cmbGender, "Select Gender");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 125);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Cont No :";
            // 
            // txtContNo
            // 
            this.txtContNo.BackColor = System.Drawing.Color.Ivory;
            this.txtContNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtContNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtContNo.Location = new System.Drawing.Point(76, 123);
            this.txtContNo.Name = "txtContNo";
            this.txtContNo.Size = new System.Drawing.Size(150, 20);
            this.txtContNo.TabIndex = 3;
            this.TTip.SetToolTip(this.txtContNo, "Enter Cont No");
            this.txtContNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContNo_KeyPress);
            this.txtContNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtContNo_Validating);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Address :";
            // 
            // txtAddress
            // 
            this.txtAddress.BackColor = System.Drawing.Color.White;
            this.txtAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddress.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAddress.Location = new System.Drawing.Point(76, 60);
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(210, 57);
            this.txtAddress.TabIndex = 2;
            this.TTip.SetToolTip(this.txtAddress, "Enter Address");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Cust Name :";
            // 
            // txtCustName
            // 
            this.txtCustName.BackColor = System.Drawing.Color.Ivory;
            this.txtCustName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCustName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCustName.Location = new System.Drawing.Point(76, 34);
            this.txtCustName.Name = "txtCustName";
            this.txtCustName.Size = new System.Drawing.Size(210, 20);
            this.txtCustName.TabIndex = 1;
            this.TTip.SetToolTip(this.txtCustName, "Enter Customer Name");
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(64, 303);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 9;
            this.btnTrans.Text = "Mode";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Cust Code :";
            // 
            // txtCustCode
            // 
            this.txtCustCode.BackColor = System.Drawing.Color.Ivory;
            this.txtCustCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCustCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCustCode.Enabled = false;
            this.txtCustCode.Location = new System.Drawing.Point(76, 8);
            this.txtCustCode.Name = "txtCustCode";
            this.txtCustCode.Size = new System.Drawing.Size(95, 20);
            this.txtCustCode.TabIndex = 0;
            this.TTip.SetToolTip(this.txtCustCode, "Displays Customer Code");
            // 
            // frmCustomer
            // 
            this.ClientSize = new System.Drawing.Size(299, 341);
            this.Controls.Add(this.pnlCustomer);
            this.Name = "frmCustomer";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customer";
            this.Load += new System.EventHandler(this.frmCustomer_Load);
            this.pnlCustomer.ResumeLayout(false);
            this.pnlCustomer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}