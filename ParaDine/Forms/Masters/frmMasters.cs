﻿using ParaDine.Forms.GlobalForms;
using ParaDine.Forms.Stock;
using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Masters
{
    public partial class frmMasters : Form
    {
        private Button btnAdd;
        private Button btnBack;
        private Button btnDelete;
        private Button btnDisplay;
        private Button btnEdit;
        private Button btnExit;
        private Button btnReset;
        private ComboBox cmbBoolean;
        private ComboBox cmbFilter;
        private ComboBox cmbPnlRealComparision;
       // private IContainer components;
        private DataGridView dgViewMaster;
        private DateTimePicker dtFrom;
        private DateTimePicker dtTo;
        private string EntName;
        private SecurityClass EntSecurity;
        private NumControl gvFilterAmount;
        private ImageList imageList1;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label lblFilter;
        private DataTable mstDT;
        private Panel panel1;
        private Panel pnlBoolean;
        private Panel pnlDate;
        private Panel pnlFilter;
        private Panel pnlReal;
        private Panel pnlString;
        private Panel pnlTrans;
        public DataTable SelDt;
        private string StrCond;
        private ToolTip TTip;
        private TextBox txtFilter;
        private MasterBase varLocalEntity;

        public frmMasters(string paramEntName)
        {
            this.components = null;
            this.EntName = "";
            this.SelDt = new DataTable();
            this.StrCond = "";
            this.InitializeComponent();
            ParaDine.GlobalFunctions.GetConnection();
            this.EntName = paramEntName;
        }

        public frmMasters(string paramEntName, SecurityClass paramSecurity)
        {
            this.components = null;
            this.EntName = "";
            this.SelDt = new DataTable();
            this.StrCond = "";
            this.InitializeComponent();
            ParaDine.GlobalFunctions.GetConnection();
            this.EntName = paramEntName;
            this.EntSecurity = paramSecurity;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.ProcessEntity(this.EntName, this.btnAdd.Text, 0);
                this.PopulateMasterTable();
                this.PopulateListView();
                this.dgViewMaster.FirstDisplayedScrollingRowIndex = this.dgViewMaster.Rows.Count - 1;
                this.dgViewMaster.Rows[this.dgViewMaster.Rows.Count - 1].Selected = true;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Add Click");
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (this.StrCond.Contains("AND"))
            {
                string strCond = this.StrCond;
                int startIndex = 0;
                while (strCond.IndexOf("AND") > 0)
                {
                    startIndex = this.StrCond.IndexOf("AND", startIndex) + 3;
                    if (startIndex < strCond.Length)
                    {
                        strCond = strCond.Substring(startIndex);
                    }
                }
                this.StrCond = this.StrCond.Substring(0, startIndex - 3);
            }
            else
            {
                this.StrCond = "";
            }
            this.mstDT.DefaultView.RowFilter = this.StrCond;
            this.lblFilter.Text = this.StrCond.Replace("%", "");
            ParaDine.GlobalFill.FillGridView(this.dgViewMaster, this.mstDT.DefaultView.ToTable());
            if (this.dgViewMaster.Columns.Count > 1)
            {
                this.dgViewMaster.Columns[0].Visible = false;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgViewMaster.SelectedRows.Count > 0)
                {
                    int firstDisplayedScrollingRowIndex = this.dgViewMaster.FirstDisplayedScrollingRowIndex;
                    int index = this.dgViewMaster.SelectedRows[0].Index;
                    this.ProcessEntity(this.EntName, this.btnDelete.Text, Convert.ToInt16(this.dgViewMaster.SelectedRows[0].Cells[0].Value));
                    this.PopulateMasterTable();
                    this.PopulateListView();
                    this.dgViewMaster.FirstDisplayedScrollingRowIndex = firstDisplayedScrollingRowIndex;
                    if (index < this.dgViewMaster.Rows.Count)
                    {
                        this.dgViewMaster.Rows[index].Selected = true;
                    }
                    else
                    {
                        this.dgViewMaster.Rows[this.dgViewMaster.Rows.Count - 1].Selected = true;
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Delete Click");
            }
        }

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            this.PopulateListView();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgViewMaster.SelectedRows.Count > 0)
                {
                    int firstDisplayedScrollingRowIndex = this.dgViewMaster.FirstDisplayedScrollingRowIndex;
                    int index = this.dgViewMaster.SelectedRows[0].Index;
                    this.ProcessEntity(this.EntName, this.btnEdit.Text, Convert.ToInt16(this.dgViewMaster.SelectedRows[0].Cells[0].Value));
                    this.PopulateMasterTable();
                    this.PopulateListView();
                    this.dgViewMaster.FirstDisplayedScrollingRowIndex = firstDisplayedScrollingRowIndex;
                    this.dgViewMaster.Rows[index].Selected = true;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Edit Click");
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            this.RefreshData();
            this.PopulateListView();
        }

        private void chkSelect_CheckedChanged(object sender, EventArgs e)
        {
            this.PopulateListView();
        }

        private void cmbBoolean_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (this.dgViewMaster.DataSource != null)
            {
                this.btnDisplay_Click(sender, e);
            }
        }

        private void cmbFilter_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (this.dgViewMaster.Columns[this.cmbFilter.SelectedItem.ToString()].ValueType == typeof(DateTime))
            {
                this.pnlDate.Visible = true;
                this.pnlString.Visible = false;
                this.pnlReal.Visible = false;
                this.pnlBoolean.Visible = false;
            }
            else if (this.dgViewMaster.Columns[this.cmbFilter.SelectedItem.ToString()].ValueType == typeof(string))
            {
                this.pnlDate.Visible = false;
                this.pnlString.Visible = true;
                this.pnlReal.Visible = false;
                this.pnlBoolean.Visible = false;
            }
            else if ((this.dgViewMaster.Columns[this.cmbFilter.SelectedItem.ToString()].ValueType == typeof(double)) || (this.dgViewMaster.Columns[this.cmbFilter.SelectedItem.ToString()].ValueType == typeof(decimal)))
            {
                this.pnlDate.Visible = false;
                this.pnlString.Visible = false;
                this.pnlReal.Visible = true;
                this.pnlBoolean.Visible = false;
            }
            else if (this.dgViewMaster.Columns[this.cmbFilter.SelectedItem.ToString()].ValueType == typeof(bool))
            {
                this.pnlBoolean.Visible = true;
                this.pnlDate.Visible = false;
                this.pnlString.Visible = false;
                this.pnlReal.Visible = false;
            }
        }

        private void dgViewMaster_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                this.btnEdit_Click(sender, new EventArgs());
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void dgViewMaster_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.dgViewMaster_DoubleClick(sender, new EventArgs());
            }
        }

        

        private void dtFrom_ValueChanged(object sender, EventArgs e)
        {
            if (this.dgViewMaster.DataSource != null)
            {
                this.btnDisplay_Click(sender, e);
            }
        }

        private void dtTo_ValueChanged(object sender, EventArgs e)
        {
            if (this.dgViewMaster.DataSource != null)
            {
                this.btnDisplay_Click(sender, e);
            }
        }

        private void frmMasters_Load(object sender, EventArgs e)
        {
            try
            {
                this.Text = this.EntName;
                if (this.EntName != "")
                {
                    this.ProcessEntity(this.EntName, "LOAD", 0);
                    this.RefreshData();
                    this.SetGridView();
                    this.PopulateMasterTable();
                    this.PopulateListView();
                    this.PopulateFilters();
                }
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Master Control Load");
            }
        }

        private void gvFilterAmount_Change(object sender, EventArgs e)
        {
            if (this.dgViewMaster.DataSource != null)
            {
                this.btnDisplay_Click(sender, e);
            }
        }

      

        private void lstVwMasters_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                this.btnEdit_Click(sender, new EventArgs());
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void lstVwMasters_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.lstVwMasters_DoubleClick(sender, new EventArgs());
            }
        }

        private void PopulateFilters()
        {
            try
            {
                if (!object.Equals(this.varLocalEntity, null))
                {
                    foreach (DataColumn column in this.mstDT.Columns)
                    {
                        if (column.Ordinal > 0)
                        {
                            this.cmbFilter.Items.Add(column.ColumnName);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "PopulateFilters");
            }
        }

        private void PopulateListView()
        {
            try
            {
                this.StrCond = "";
                if (!object.Equals(this.varLocalEntity, null))
                {
                    string strCond;
                    if (this.StrCond.Trim() != "")
                    {
                        this.StrCond = this.StrCond + " AND ";
                    }
                    if (this.pnlString.Visible && (this.txtFilter.Text.Trim() != ""))
                    {
                        strCond = this.StrCond;
                        this.StrCond = strCond + this.cmbFilter.Text + " LIKE '%" + this.txtFilter.Text.Trim() + "%'";
                    }
                    else if (this.pnlReal.Visible && (this.cmbPnlRealComparision.Text != ""))
                    {
                        this.StrCond = this.StrCond + this.cmbFilter.Text + this.cmbPnlRealComparision.Text + this.gvFilterAmount.Text;
                    }
                    else if (this.pnlDate.Visible && (this.dtFrom.Checked || this.dtTo.Checked))
                    {
                        string str = "";
                        if (this.dtFrom.Checked)
                        {
                            strCond = str;
                            str = strCond + this.cmbFilter.Text + " >= '" + this.dtFrom.Text + "'";
                        }
                        if (this.dtTo.Checked)
                        {
                            if (str.Trim() != "")
                            {
                                str = str + " AND ";
                            }
                            strCond = str;
                            str = strCond + this.cmbFilter.Text + " <= '" + this.dtTo.Text + "'";
                        }
                        this.StrCond = this.StrCond + str;
                    }
                    else if (this.pnlBoolean.Visible && (this.cmbBoolean.SelectedIndex >= 0))
                    {
                        object obj2 = this.StrCond;
                        this.StrCond = string.Concat(new object[] { obj2, this.cmbFilter.Text, "=", this.cmbBoolean.SelectedIndex });
                    }
                    this.mstDT.DefaultView.RowFilter = this.StrCond;
                    this.lblFilter.Text = this.StrCond.Replace("%", "");
                    ParaDine.GlobalFill.FillGridView(this.dgViewMaster, this.mstDT.DefaultView.ToTable());
                    if (this.dgViewMaster.Columns.Count > 1)
                    {
                        this.dgViewMaster.Columns[0].Visible = false;
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "PopulateListView");
            }
        }

        private void PopulateMasterTable()
        {
            try
            {
                if (!object.Equals(this.varLocalEntity, null))
                {
                    this.mstDT = new DataTable();
                    this.mstDT.Rows.Clear();
                    this.mstDT.Columns.Clear();
                    this.mstDT = this.varLocalEntity.GetDataList("");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "PopulateMasterTable");
            }
        }

        private void ProcessEntity(string paramEntName, string strProcess, int ProcessID)
        {
            switch (paramEntName)
            {
                case "Store":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmStore(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new Store();
                    break;

                case "Kitchen":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmKitchen(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new Kitchen();
                    break;

                case "Tax":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmTax(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new Tax();
                    break;

                case "UOM":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmUOM(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new UOM();
                    break;

                case "Department":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmDepartment(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new Department();
                    break;

                case "Section":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmSection(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new Section();
                    break;

                case "Category":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmCategory(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new Category();
                    break;

                case "Item":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmItem(strProcess, (long)ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new Item();
                    break;

                case "Customer":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmCustomer(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new Customer();
                    break;

                case "Supplier":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmSupplier(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new Supplier();
                    break;

                case "Steward":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmSteward(strProcess, (long)ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new Steward();
                    break;

                case "Table":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmTable(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new Table();
                    break;

                case "CardType":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmCardType(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new CardType();
                    break;

                case "Card":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmCard(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new Card();
                    break;

                case "PaymentMode":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmPaymentMode(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new PaymentMode();
                    break;

                case "Terminal":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmTerminal(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new Terminal();
                    break;

                case "CounterTrans":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmCounterTransactions(strProcess, (long)ProcessID).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new CounterTrans();
                    break;

                case "PurchaseOrder":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmPurchaseOrder(strProcess, (long)ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new POMaster(0L);
                    break;

                case "Hint":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmHint(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new Hint();
                    break;

                case "Level":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmLevel(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new Level();
                    break;

                case "User":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmUsers(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new User();
                    break;

                case "Shift":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmShift(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new Shift();
                    break;

                case "StockPoint":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmStockPoint(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new StockPoint(0L);
                    break;
            }
        }

        private void RefreshData()
        {
            this.StrCond = "";
            this.txtFilter.Text = "";
            this.cmbPnlRealComparision.Text = "";
            this.gvFilterAmount.Text = "0";
            this.dtFrom.Value = ParaDine.GlobalVariables.BusinessDate;
            this.dtTo.Value = ParaDine.GlobalVariables.BusinessDate;
            this.dtFrom.Checked = false;
            this.dtTo.Checked = false;
            this.cmbBoolean.Text = "";
        }

        private void SetGridView()
        {
            this.dgViewMaster.AllowUserToAddRows = false;
            this.dgViewMaster.AllowUserToDeleteRows = false;
            this.dgViewMaster.AllowUserToResizeRows = false;
            this.dgViewMaster.EditMode = DataGridViewEditMode.EditProgrammatically;
            this.dgViewMaster.ReadOnly = true;
            this.dgViewMaster.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgViewMaster.RowTemplate.ReadOnly = true;
            this.dgViewMaster.StandardTab = false;
            this.dgViewMaster.RowHeadersVisible = false;
        }

        private void txtFilter_TextChanged(object sender, EventArgs e)
        {
            if (this.dgViewMaster.DataSource != null)
            {
                this.btnDisplay_Click(sender, e);
            }
        }
    }
}
