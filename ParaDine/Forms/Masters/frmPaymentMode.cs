﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Masters
{
    public partial class frmPaymentMode : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private CheckBox chkCredit;
        //private IContainer components = null;
        private PaymentMode EntId = new PaymentMode();
        private Label label1;
        private Panel pnlPaymentMode;
        private ToolTip TTip;
        private TextBox txtPaymentMode;

        public frmPaymentMode(string Mode, int ID, SecurityClass paramSecurity)
        {
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes((long)ID);
            this.EntId.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (GlobalValidations.ValidateFields(this.pnlPaymentMode, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_007D;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_008D;
                            }
                        }
                        else
                        {
                            this.EntId.Add(sqlTrans, true);
                        }
                    }
                    goto Label_009F;
                Label_007D:
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_009F;
                Label_008D:
                    this.EntId.Delete(sqlTrans, true);
                Label_009F:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, exception.Source);
                    sqlTrans.Rollback();
                }
            }
        }

        

        private void frmPaymentMode_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                this.LoadFields();
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                base.Close();
            }
        }

       

        private void LoadEntities()
        {
            this.EntId.PaymentModeid = Convert.ToInt32(this.txtPaymentMode.Tag);
            this.EntId.PaymentModeName = Convert.ToString(this.txtPaymentMode.Text);
            this.EntId.Credit = this.chkCredit.Checked;
        }

        private void LoadFields()
        {
            this.txtPaymentMode.Tag = Convert.ToInt32(this.EntId.PaymentModeid);
            this.txtPaymentMode.Text = Convert.ToString(this.EntId.PaymentModeName);
            this.chkCredit.Checked = this.EntId.Credit;
        }
    }
}
