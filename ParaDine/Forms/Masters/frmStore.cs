﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Masters
{
    public partial class frmStore : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private ComboBox cmbCompany;
       // private IContainer components = null;
        private Store EntID = new Store();
        private Label label1;
        private Label label2;
        private Panel pnlStore;
        private ToolTip TTip;
        private TextBox txtStore;

        public frmStore(string Mode, int ID, SecurityClass paramSecurity)
        {
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntID.LoadAttributes((long)ID);
            this.EntID.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (GlobalValidations.ValidateFields(this.pnlStore, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_007D;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_008D;
                            }
                        }
                        else
                        {
                            this.EntID.Add(sqlTrans, true);
                        }
                    }
                    goto Label_009F;
                Label_007D:
                    this.EntID.Modify(sqlTrans, true);
                    goto Label_009F;
                Label_008D:
                    this.EntID.Delete(sqlTrans, true);
                Label_009F:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, exception.Source);
                    sqlTrans.Rollback();
                }
            }
        }

       
        private void frmStore_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntID.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                this.RefreshData();
                this.LoadFields();
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                base.Close();
            }
        }

     

        private void LoadEntities()
        {
            this.EntID.StoreID = Convert.ToInt32(this.txtStore.Tag);
            this.EntID.StoreName = Convert.ToString(this.txtStore.Text);
            this.EntID.CompanyID = Convert.ToInt32(this.cmbCompany.SelectedValue);
        }

        private void LoadFields()
        {
            this.txtStore.Tag = this.EntID.StoreID;
            this.txtStore.Text = this.EntID.StoreName;
            this.cmbCompany.SelectedValue = this.EntID.CompanyID;
        }

        private void RefreshData()
        {
            try
            {
                GlobalFill.FillCombo("SELECT COMPANYID, COMPANYNAME FROM GLB_COMPANY ORDER BY 2", this.cmbCompany);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
    }
}
