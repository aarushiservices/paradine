﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Masters
{
    public partial class frmCategory : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private ComboBox cmbDepartment;
       // private IContainer components = null;
        private Category EntId = new Category();
        private Label label1;
        private Label label2;
        private PictureBox picImage;
        private Panel pnlCategory;
        private ToolTip TTip;
        private TextBox txtCategory;

        public frmCategory(string Mode, int ID, SecurityClass paramSecurity)
        {
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes((long)ID);
            this.EntId.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (GlobalValidations.ValidateFields(this.pnlCategory, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_007D;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_008D;
                            }
                        }
                        else
                        {
                            this.EntId.Add(sqlTrans, true);
                        }
                    }
                    goto Label_009F;
                Label_007D:
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_009F;
                Label_008D:
                    this.EntId.Delete(sqlTrans, true);
                Label_009F:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, exception.Source);
                    sqlTrans.Rollback();
                }
            }
        }

        private void cmbDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

       

        private void frmCategory_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                this.RefreshData();
                this.LoadFields();
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                base.Close();
            }
        }

       

        private void LoadEntities()
        {
            this.EntId.CategoryID = Convert.ToInt32(this.txtCategory.Tag);
            this.EntId.CategoryName = Convert.ToString(this.txtCategory.Text);
            this.EntId.DepartmentID = Convert.ToInt32(this.cmbDepartment.SelectedValue);
            this.EntId.CatgImage = this.picImage;
        }

        private void LoadFields()
        {
            this.txtCategory.Tag = this.EntId.CategoryID;
            this.txtCategory.Text = this.EntId.CategoryName;
            this.cmbDepartment.SelectedValue = this.EntId.DepartmentID;
            this.picImage.Image = this.EntId.CatgImage.Image;
        }

        private void picImage_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.ShowDialog();
            this.picImage.ImageLocation = dialog.FileName;
            this.picImage.Refresh();
        }

        private void RefreshData()
        {
            try
            {
                GlobalFill.FillCombo("SELECT DEPARTMENTID, DEPARTMENTNAME FROM RES_DEPARTMENT", this.cmbDepartment);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in RefreshData");
            }
        }
    }
}
