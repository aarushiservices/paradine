﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Masters
{
    partial class frmItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.txtItemCode = new System.Windows.Forms.TextBox();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.txtDepartment = new System.Windows.Forms.TextBox();
            this.cmbUOM = new System.Windows.Forms.ComboBox();
            this.cmbTax = new System.Windows.Forms.ComboBox();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.cmbSection = new System.Windows.Forms.ComboBox();
            this.cmbNewUOM = new System.Windows.Forms.ComboBox();
            this.cmbBasicUOM = new System.Windows.Forms.ComboBox();
            this.cmbStockPoint = new System.Windows.Forms.ComboBox();
            this.gvItemDiscPerc = new ParaSysCom.NumControl();
            this.gvMBQ = new ParaSysCom.NumControl();
            this.gvCostPrice = new ParaSysCom.NumControl();
            this.gvSalePrice = new ParaSysCom.NumControl();
            this.gvNewUOMConv = new ParaSysCom.NumControl();
            this.gvNewCostPrice = new ParaSysCom.NumControl();
            this.gvNewSalePrice = new ParaSysCom.NumControl();
            this.cmbStockUOM = new System.Windows.Forms.ComboBox();
            this.cmbIngrUOM = new System.Windows.Forms.ComboBox();
            this.gvIngrQty = new ParaSysCom.NumControl();
            this.cmbSpUOM = new System.Windows.Forms.ComboBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlItem = new System.Windows.Forms.Panel();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tbProduct = new System.Windows.Forms.TabPage();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.grpItemSettings = new System.Windows.Forms.GroupBox();
            this.chkInactive = new System.Windows.Forms.CheckBox();
            this.chkRawMaterial = new System.Windows.Forms.CheckBox();
            this.chkStubReq = new System.Windows.Forms.CheckBox();
            this.chkNonInventory = new System.Windows.Forms.CheckBox();
            this.chkTaxable = new System.Windows.Forms.CheckBox();
            this.chkDiscountable = new System.Windows.Forms.CheckBox();
            this.chkOpenRate = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbUnit = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.pnlUnit = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.btnUnitTrans = new System.Windows.Forms.Button();
            this.grpConv = new System.Windows.Forms.GroupBox();
            this.lblNewUOM = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblBasicUOM = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lvwUnit = new System.Windows.Forms.ListView();
            this.tbPrice = new System.Windows.Forms.TabPage();
            this.pnlSprice = new System.Windows.Forms.Panel();
            this.btnSPTrans = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lvwSprice = new System.Windows.Forms.ListView();
            this.tbRecepe = new System.Windows.Forms.TabPage();
            this.chkCombo = new System.Windows.Forms.CheckBox();
            this.lblRecepeHead = new System.Windows.Forms.Label();
            this.pnlIngrediant = new System.Windows.Forms.Panel();
            this.btnIngrTrans = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtIngrName = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtIngrCode = new System.Windows.Forms.TextBox();
            this.lstIngr = new System.Windows.Forms.ListView();
            this.trvUOM = new System.Windows.Forms.TreeView();
            this.btnTrans = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.picImage = new System.Windows.Forms.PictureBox();
            this.pnlItem.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tbProduct.SuspendLayout();
            this.grpItemSettings.SuspendLayout();
            this.tbUnit.SuspendLayout();
            this.pnlUnit.SuspendLayout();
            this.grpConv.SuspendLayout();
            this.tbPrice.SuspendLayout();
            this.pnlSprice.SuspendLayout();
            this.tbRecepe.SuspendLayout();
            this.pnlIngrediant.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).BeginInit();
            this.SuspendLayout();
            // 
            // txtItemCode
            // 
            this.txtItemCode.BackColor = System.Drawing.Color.Ivory;
            this.txtItemCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtItemCode.Location = new System.Drawing.Point(84, 14);
            this.txtItemCode.Name = "txtItemCode";
            this.txtItemCode.Size = new System.Drawing.Size(124, 20);
            this.txtItemCode.TabIndex = 0;
            this.TTip.SetToolTip(this.txtItemCode, "Displays Code");
            // 
            // txtItemName
            // 
            this.txtItemName.BackColor = System.Drawing.Color.Ivory;
            this.txtItemName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtItemName.Location = new System.Drawing.Point(84, 46);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(318, 20);
            this.txtItemName.TabIndex = 1;
            this.TTip.SetToolTip(this.txtItemName, "Enter Item Name");
            // 
            // txtDepartment
            // 
            this.txtDepartment.BackColor = System.Drawing.Color.White;
            this.txtDepartment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDepartment.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDepartment.Location = new System.Drawing.Point(283, 111);
            this.txtDepartment.Name = "txtDepartment";
            this.txtDepartment.ReadOnly = true;
            this.txtDepartment.Size = new System.Drawing.Size(119, 20);
            this.txtDepartment.TabIndex = 28;
            this.txtDepartment.TabStop = false;
            this.TTip.SetToolTip(this.txtDepartment, "Displays Department");
            // 
            // cmbUOM
            // 
            this.cmbUOM.BackColor = System.Drawing.Color.Ivory;
            this.cmbUOM.FormattingEnabled = true;
            this.cmbUOM.Location = new System.Drawing.Point(83, 209);
            this.cmbUOM.Name = "cmbUOM";
            this.cmbUOM.Size = new System.Drawing.Size(125, 21);
            this.cmbUOM.TabIndex = 7;
            this.TTip.SetToolTip(this.cmbUOM, "Select UOM");
            this.cmbUOM.SelectedValueChanged += new System.EventHandler(this.cmbUOM_SelectedValueChanged);
            // 
            // cmbTax
            // 
            this.cmbTax.BackColor = System.Drawing.Color.Ivory;
            this.cmbTax.FormattingEnabled = true;
            this.cmbTax.Location = new System.Drawing.Point(283, 78);
            this.cmbTax.Name = "cmbTax";
            this.cmbTax.Size = new System.Drawing.Size(119, 21);
            this.cmbTax.TabIndex = 3;
            this.TTip.SetToolTip(this.cmbTax, "Select Tax");
            // 
            // cmbCategory
            // 
            this.cmbCategory.BackColor = System.Drawing.Color.Ivory;
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Location = new System.Drawing.Point(84, 111);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(124, 21);
            this.cmbCategory.TabIndex = 4;
            this.TTip.SetToolTip(this.cmbCategory, "Select Section");
            this.cmbCategory.SelectedValueChanged += new System.EventHandler(this.cmbCategory_SelectedValueChanged);
            // 
            // cmbSection
            // 
            this.cmbSection.BackColor = System.Drawing.Color.Ivory;
            this.cmbSection.FormattingEnabled = true;
            this.cmbSection.Location = new System.Drawing.Point(84, 78);
            this.cmbSection.Name = "cmbSection";
            this.cmbSection.Size = new System.Drawing.Size(124, 21);
            this.cmbSection.TabIndex = 2;
            this.TTip.SetToolTip(this.cmbSection, "Select Category");
            // 
            // cmbNewUOM
            // 
            this.cmbNewUOM.BackColor = System.Drawing.Color.Ivory;
            this.cmbNewUOM.FormattingEnabled = true;
            this.cmbNewUOM.Location = new System.Drawing.Point(246, 26);
            this.cmbNewUOM.Name = "cmbNewUOM";
            this.cmbNewUOM.Size = new System.Drawing.Size(141, 21);
            this.cmbNewUOM.TabIndex = 1;
            this.TTip.SetToolTip(this.cmbNewUOM, "Select New UOM");
            this.cmbNewUOM.SelectedIndexChanged += new System.EventHandler(this.cmbNewUOM_SelectedIndexChanged);
            // 
            // cmbBasicUOM
            // 
            this.cmbBasicUOM.BackColor = System.Drawing.Color.Ivory;
            this.cmbBasicUOM.FormattingEnabled = true;
            this.cmbBasicUOM.Location = new System.Drawing.Point(27, 26);
            this.cmbBasicUOM.Name = "cmbBasicUOM";
            this.cmbBasicUOM.Size = new System.Drawing.Size(143, 21);
            this.cmbBasicUOM.TabIndex = 0;
            this.TTip.SetToolTip(this.cmbBasicUOM, "Select Basic UOM");
            this.cmbBasicUOM.SelectedIndexChanged += new System.EventHandler(this.cmbBasicUOM_SelectedIndexChanged);
            // 
            // cmbStockPoint
            // 
            this.cmbStockPoint.BackColor = System.Drawing.Color.Ivory;
            this.cmbStockPoint.FormattingEnabled = true;
            this.cmbStockPoint.Location = new System.Drawing.Point(22, 31);
            this.cmbStockPoint.Name = "cmbStockPoint";
            this.cmbStockPoint.Size = new System.Drawing.Size(139, 21);
            this.cmbStockPoint.TabIndex = 0;
            this.TTip.SetToolTip(this.cmbStockPoint, "Select Restaurant");
            // 
            // gvItemDiscPerc
            // 
            this.gvItemDiscPerc.DecimalRequired = true;
            this.gvItemDiscPerc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemDiscPerc.Location = new System.Drawing.Point(283, 176);
            this.gvItemDiscPerc.Name = "gvItemDiscPerc";
            this.gvItemDiscPerc.Size = new System.Drawing.Size(119, 20);
            this.gvItemDiscPerc.SymbolRequired = false;
            this.gvItemDiscPerc.TabIndex = 9;
            this.gvItemDiscPerc.Text = "0.00";
            this.gvItemDiscPerc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvItemDiscPerc, "Enter Disc %");
            this.gvItemDiscPerc.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // gvMBQ
            // 
            this.gvMBQ.DecimalRequired = true;
            this.gvMBQ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvMBQ.Location = new System.Drawing.Point(84, 176);
            this.gvMBQ.Name = "gvMBQ";
            this.gvMBQ.Size = new System.Drawing.Size(124, 20);
            this.gvMBQ.SymbolRequired = false;
            this.gvMBQ.TabIndex = 8;
            this.gvMBQ.Text = "0.00";
            this.gvMBQ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvMBQ, "Enter MBQ");
            this.gvMBQ.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // gvCostPrice
            // 
            this.gvCostPrice.DecimalRequired = true;
            this.gvCostPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvCostPrice.Location = new System.Drawing.Point(283, 144);
            this.gvCostPrice.Name = "gvCostPrice";
            this.gvCostPrice.Size = new System.Drawing.Size(119, 20);
            this.gvCostPrice.SymbolRequired = true;
            this.gvCostPrice.TabIndex = 6;
            this.gvCostPrice.Text = "`0.00";
            this.gvCostPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvCostPrice, "Enter CostPrice");
            this.gvCostPrice.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // gvSalePrice
            // 
            this.gvSalePrice.DecimalRequired = true;
            this.gvSalePrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvSalePrice.Location = new System.Drawing.Point(84, 144);
            this.gvSalePrice.Name = "gvSalePrice";
            this.gvSalePrice.Size = new System.Drawing.Size(124, 20);
            this.gvSalePrice.SymbolRequired = true;
            this.gvSalePrice.TabIndex = 5;
            this.gvSalePrice.Text = "`0.00";
            this.gvSalePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvSalePrice, "Enter SalePrice");
            this.gvSalePrice.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // gvNewUOMConv
            // 
            this.gvNewUOMConv.DecimalRequired = true;
            this.gvNewUOMConv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvNewUOMConv.Location = new System.Drawing.Point(181, 25);
            this.gvNewUOMConv.Name = "gvNewUOMConv";
            this.gvNewUOMConv.Size = new System.Drawing.Size(69, 20);
            this.gvNewUOMConv.SymbolRequired = false;
            this.gvNewUOMConv.TabIndex = 1;
            this.gvNewUOMConv.Text = "0.00";
            this.gvNewUOMConv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvNewUOMConv, "Enter Conversion Factor");
            this.gvNewUOMConv.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // gvNewCostPrice
            // 
            this.gvNewCostPrice.DecimalRequired = true;
            this.gvNewCostPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvNewCostPrice.Location = new System.Drawing.Point(393, 26);
            this.gvNewCostPrice.Name = "gvNewCostPrice";
            this.gvNewCostPrice.Size = new System.Drawing.Size(96, 20);
            this.gvNewCostPrice.SymbolRequired = false;
            this.gvNewCostPrice.TabIndex = 2;
            this.gvNewCostPrice.Text = "0.00";
            this.gvNewCostPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvNewCostPrice, "Enter New UOMs CostPrice");
            this.gvNewCostPrice.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // gvNewSalePrice
            // 
            this.gvNewSalePrice.DecimalRequired = true;
            this.gvNewSalePrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvNewSalePrice.Location = new System.Drawing.Point(360, 31);
            this.gvNewSalePrice.Name = "gvNewSalePrice";
            this.gvNewSalePrice.Size = new System.Drawing.Size(97, 20);
            this.gvNewSalePrice.SymbolRequired = true;
            this.gvNewSalePrice.TabIndex = 2;
            this.gvNewSalePrice.Text = "`0.00";
            this.gvNewSalePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvNewSalePrice, "Enter SSS  SalePrice");
            this.gvNewSalePrice.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // cmbStockUOM
            // 
            this.cmbStockUOM.BackColor = System.Drawing.Color.Ivory;
            this.cmbStockUOM.FormattingEnabled = true;
            this.cmbStockUOM.Location = new System.Drawing.Point(283, 207);
            this.cmbStockUOM.Name = "cmbStockUOM";
            this.cmbStockUOM.Size = new System.Drawing.Size(119, 21);
            this.cmbStockUOM.TabIndex = 10;
            this.TTip.SetToolTip(this.cmbStockUOM, "Select UOM");
            // 
            // cmbIngrUOM
            // 
            this.cmbIngrUOM.BackColor = System.Drawing.Color.Ivory;
            this.cmbIngrUOM.FormattingEnabled = true;
            this.cmbIngrUOM.Location = new System.Drawing.Point(154, 19);
            this.cmbIngrUOM.Name = "cmbIngrUOM";
            this.cmbIngrUOM.Size = new System.Drawing.Size(111, 21);
            this.cmbIngrUOM.TabIndex = 17;
            this.TTip.SetToolTip(this.cmbIngrUOM, "Select UOM");
            // 
            // gvIngrQty
            // 
            this.gvIngrQty.DecimalRequired = true;
            this.gvIngrQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvIngrQty.Location = new System.Drawing.Point(304, 19);
            this.gvIngrQty.Name = "gvIngrQty";
            this.gvIngrQty.Size = new System.Drawing.Size(72, 20);
            this.gvIngrQty.SymbolRequired = false;
            this.gvIngrQty.TabIndex = 19;
            this.gvIngrQty.Text = "0.00";
            this.gvIngrQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvIngrQty, "Enter SalePrice");
            this.gvIngrQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // cmbSpUOM
            // 
            this.cmbSpUOM.BackColor = System.Drawing.Color.Ivory;
            this.cmbSpUOM.FormattingEnabled = true;
            this.cmbSpUOM.Location = new System.Drawing.Point(195, 30);
            this.cmbSpUOM.Name = "cmbSpUOM";
            this.cmbSpUOM.Size = new System.Drawing.Size(139, 21);
            this.cmbSpUOM.TabIndex = 6;
            this.TTip.SetToolTip(this.cmbSpUOM, "Select UOM");
            // 
            // btnExit
            // 
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(533, 302);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 3;
            this.btnExit.TabStop = false;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pnlItem
            // 
            this.pnlItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlItem.Controls.Add(this.tabControl);
            this.pnlItem.Controls.Add(this.btnExit);
            this.pnlItem.Controls.Add(this.btnTrans);
            this.pnlItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlItem.Location = new System.Drawing.Point(0, 0);
            this.pnlItem.Name = "pnlItem";
            this.pnlItem.Size = new System.Drawing.Size(622, 331);
            this.pnlItem.TabIndex = 5;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tbProduct);
            this.tabControl.Controls.Add(this.tbUnit);
            this.tabControl.Controls.Add(this.tbPrice);
            this.tabControl.Controls.Add(this.tbRecepe);
            this.tabControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl.HotTrack = true;
            this.tabControl.ItemSize = new System.Drawing.Size(148, 22);
            this.tabControl.Location = new System.Drawing.Point(11, 11);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(598, 272);
            this.tabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl.TabIndex = 3;
            this.tabControl.TabStop = false;
            // 
            // tbProduct
            // 
            this.tbProduct.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbProduct.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbProduct.Controls.Add(this.chkInactive);
            this.tbProduct.Controls.Add(this.label21);
            this.tbProduct.Controls.Add(this.cmbStockUOM);
            this.tbProduct.Controls.Add(this.label20);
            this.tbProduct.Controls.Add(this.gvItemDiscPerc);
            this.tbProduct.Controls.Add(this.grpItemSettings);
            this.tbProduct.Controls.Add(this.label10);
            this.tbProduct.Controls.Add(this.gvMBQ);
            this.tbProduct.Controls.Add(this.label9);
            this.tbProduct.Controls.Add(this.txtDepartment);
            this.tbProduct.Controls.Add(this.label8);
            this.tbProduct.Controls.Add(this.gvCostPrice);
            this.tbProduct.Controls.Add(this.label7);
            this.tbProduct.Controls.Add(this.gvSalePrice);
            this.tbProduct.Controls.Add(this.label6);
            this.tbProduct.Controls.Add(this.cmbUOM);
            this.tbProduct.Controls.Add(this.label5);
            this.tbProduct.Controls.Add(this.cmbTax);
            this.tbProduct.Controls.Add(this.label4);
            this.tbProduct.Controls.Add(this.cmbCategory);
            this.tbProduct.Controls.Add(this.label3);
            this.tbProduct.Controls.Add(this.cmbSection);
            this.tbProduct.Controls.Add(this.label2);
            this.tbProduct.Controls.Add(this.txtItemName);
            this.tbProduct.Controls.Add(this.label1);
            this.tbProduct.Controls.Add(this.txtItemCode);
            this.tbProduct.Controls.Add(this.picImage);
            this.tbProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbProduct.Location = new System.Drawing.Point(4, 26);
            this.tbProduct.Name = "tbProduct";
            this.tbProduct.Padding = new System.Windows.Forms.Padding(3);
            this.tbProduct.Size = new System.Drawing.Size(590, 242);
            this.tbProduct.TabIndex = 0;
            this.tbProduct.Text = "Product";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(210, 212);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(75, 15);
            this.label21.TabIndex = 35;
            this.label21.Text = "Stock UOM :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(231, 180);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(50, 15);
            this.label20.TabIndex = 33;
            this.label20.Text = "Disc(%)";
            // 
            // grpItemSettings
            // 
            this.grpItemSettings.Controls.Add(this.chkRawMaterial);
            this.grpItemSettings.Controls.Add(this.chkStubReq);
            this.grpItemSettings.Controls.Add(this.chkNonInventory);
            this.grpItemSettings.Controls.Add(this.chkTaxable);
            this.grpItemSettings.Controls.Add(this.chkDiscountable);
            this.grpItemSettings.Controls.Add(this.chkOpenRate);
            this.grpItemSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpItemSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpItemSettings.Location = new System.Drawing.Point(417, 101);
            this.grpItemSettings.Name = "grpItemSettings";
            this.grpItemSettings.Size = new System.Drawing.Size(169, 133);
            this.grpItemSettings.TabIndex = 11;
            this.grpItemSettings.TabStop = false;
            this.grpItemSettings.Text = "Item Settings";
            // 
            // chkInactive
            // 
            this.chkInactive.AutoSize = true;
            this.chkInactive.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkInactive.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInactive.Location = new System.Drawing.Point(341, 18);
            this.chkInactive.Name = "chkInactive";
            this.chkInactive.Size = new System.Drawing.Size(64, 19);
            this.chkInactive.TabIndex = 6;
            this.chkInactive.Text = "Inactive";
            this.chkInactive.UseVisualStyleBackColor = true;
            // 
            // chkRawMaterial
            // 
            this.chkRawMaterial.AutoSize = true;
            this.chkRawMaterial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkRawMaterial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRawMaterial.Location = new System.Drawing.Point(10, 111);
            this.chkRawMaterial.Name = "chkRawMaterial";
            this.chkRawMaterial.Size = new System.Drawing.Size(96, 19);
            this.chkRawMaterial.TabIndex = 5;
            this.chkRawMaterial.Text = "Raw Material";
            this.chkRawMaterial.UseVisualStyleBackColor = true;
            // 
            // chkStubReq
            // 
            this.chkStubReq.AutoSize = true;
            this.chkStubReq.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkStubReq.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkStubReq.Location = new System.Drawing.Point(10, 77);
            this.chkStubReq.Name = "chkStubReq";
            this.chkStubReq.Size = new System.Drawing.Size(102, 19);
            this.chkStubReq.TabIndex = 4;
            this.chkStubReq.Text = "Stub Required";
            this.chkStubReq.UseVisualStyleBackColor = true;
            // 
            // chkNonInventory
            // 
            this.chkNonInventory.AutoSize = true;
            this.chkNonInventory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkNonInventory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkNonInventory.Location = new System.Drawing.Point(11, 38);
            this.chkNonInventory.Name = "chkNonInventory";
            this.chkNonInventory.Size = new System.Drawing.Size(97, 19);
            this.chkNonInventory.TabIndex = 3;
            this.chkNonInventory.Text = "Non Inventory";
            this.chkNonInventory.UseVisualStyleBackColor = true;
            // 
            // chkTaxable
            // 
            this.chkTaxable.AutoSize = true;
            this.chkTaxable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkTaxable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTaxable.Location = new System.Drawing.Point(10, 94);
            this.chkTaxable.Name = "chkTaxable";
            this.chkTaxable.Size = new System.Drawing.Size(67, 19);
            this.chkTaxable.TabIndex = 2;
            this.chkTaxable.Text = "Taxable";
            this.chkTaxable.UseVisualStyleBackColor = true;
            // 
            // chkDiscountable
            // 
            this.chkDiscountable.AutoSize = true;
            this.chkDiscountable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkDiscountable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDiscountable.Location = new System.Drawing.Point(11, 58);
            this.chkDiscountable.Name = "chkDiscountable";
            this.chkDiscountable.Size = new System.Drawing.Size(95, 19);
            this.chkDiscountable.TabIndex = 1;
            this.chkDiscountable.Text = "Discountable";
            this.chkDiscountable.UseVisualStyleBackColor = true;
            // 
            // chkOpenRate
            // 
            this.chkOpenRate.AutoSize = true;
            this.chkOpenRate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkOpenRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOpenRate.Location = new System.Drawing.Point(11, 17);
            this.chkOpenRate.Name = "chkOpenRate";
            this.chkOpenRate.Size = new System.Drawing.Size(82, 19);
            this.chkOpenRate.TabIndex = 0;
            this.chkOpenRate.Text = "Open Rate";
            this.chkOpenRate.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(41, 180);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 15);
            this.label10.TabIndex = 31;
            this.label10.Text = "MBQ :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(241, 115);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 15);
            this.label9.TabIndex = 29;
            this.label9.Text = "Dept :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(250, 148);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 15);
            this.label8.TabIndex = 27;
            this.label8.Text = "CP :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(51, 148);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 15);
            this.label7.TabIndex = 25;
            this.label7.Text = "SP :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(41, 214);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 15);
            this.label6.TabIndex = 23;
            this.label6.Text = "UOM :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(251, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 15);
            this.label5.TabIndex = 21;
            this.label5.Text = "Tax :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(29, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 15);
            this.label4.TabIndex = 19;
            this.label4.Text = "Section :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(23, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 15);
            this.label3.TabIndex = 17;
            this.label3.Text = "Category :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 15);
            this.label2.TabIndex = 15;
            this.label2.Text = "Item Name :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 15);
            this.label1.TabIndex = 13;
            this.label1.Text = "Item Code :";
            // 
            // tbUnit
            // 
            this.tbUnit.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbUnit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbUnit.Controls.Add(this.button1);
            this.tbUnit.Controls.Add(this.pnlUnit);
            this.tbUnit.Controls.Add(this.lvwUnit);
            this.tbUnit.Location = new System.Drawing.Point(4, 26);
            this.tbUnit.Name = "tbUnit";
            this.tbUnit.Padding = new System.Windows.Forms.Padding(3);
            this.tbUnit.Size = new System.Drawing.Size(590, 242);
            this.tbUnit.TabIndex = 2;
            this.tbUnit.Text = "Units";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(450, 251);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(107, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Copy Units From ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pnlUnit
            // 
            this.pnlUnit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlUnit.Controls.Add(this.label16);
            this.pnlUnit.Controls.Add(this.btnUnitTrans);
            this.pnlUnit.Controls.Add(this.grpConv);
            this.pnlUnit.Controls.Add(this.gvNewCostPrice);
            this.pnlUnit.Controls.Add(this.label13);
            this.pnlUnit.Controls.Add(this.label12);
            this.pnlUnit.Controls.Add(this.cmbNewUOM);
            this.pnlUnit.Controls.Add(this.label11);
            this.pnlUnit.Controls.Add(this.cmbBasicUOM);
            this.pnlUnit.Location = new System.Drawing.Point(29, 20);
            this.pnlUnit.Name = "pnlUnit";
            this.pnlUnit.Size = new System.Drawing.Size(528, 109);
            this.pnlUnit.TabIndex = 0;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(186, 29);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(32, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "== >";
            // 
            // btnUnitTrans
            // 
            this.btnUnitTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUnitTrans.Location = new System.Drawing.Point(405, 71);
            this.btnUnitTrans.Name = "btnUnitTrans";
            this.btnUnitTrans.Size = new System.Drawing.Size(107, 23);
            this.btnUnitTrans.TabIndex = 4;
            this.btnUnitTrans.Text = "Trans";
            this.btnUnitTrans.UseVisualStyleBackColor = true;
            this.btnUnitTrans.Click += new System.EventHandler(this.btnUnitTrans_Click);
            // 
            // grpConv
            // 
            this.grpConv.Controls.Add(this.lblNewUOM);
            this.grpConv.Controls.Add(this.gvNewUOMConv);
            this.grpConv.Controls.Add(this.label15);
            this.grpConv.Controls.Add(this.lblBasicUOM);
            this.grpConv.Controls.Add(this.label14);
            this.grpConv.Location = new System.Drawing.Point(27, 53);
            this.grpConv.Name = "grpConv";
            this.grpConv.Size = new System.Drawing.Size(358, 51);
            this.grpConv.TabIndex = 3;
            this.grpConv.TabStop = false;
            this.grpConv.Text = "Conversion";
            // 
            // lblNewUOM
            // 
            this.lblNewUOM.AutoSize = true;
            this.lblNewUOM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNewUOM.Location = new System.Drawing.Point(256, 28);
            this.lblNewUOM.Name = "lblNewUOM";
            this.lblNewUOM.Size = new System.Drawing.Size(60, 13);
            this.lblNewUOM.TabIndex = 7;
            this.lblNewUOM.Text = "NewUOM";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(150, 28);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(14, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "=";
            // 
            // lblBasicUOM
            // 
            this.lblBasicUOM.AutoSize = true;
            this.lblBasicUOM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBasicUOM.Location = new System.Drawing.Point(73, 28);
            this.lblBasicUOM.Name = "lblBasicUOM";
            this.lblBasicUOM.Size = new System.Drawing.Size(66, 13);
            this.lblBasicUOM.TabIndex = 0;
            this.lblBasicUOM.Text = "BasicUOM";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(57, 28);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(14, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(397, 10);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "CostPrice";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(250, 10);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(57, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "New UOM";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(31, 10);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Basic UOM";
            // 
            // lvwUnit
            // 
            this.lvwUnit.BackColor = System.Drawing.Color.White;
            this.lvwUnit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lvwUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvwUnit.ForeColor = System.Drawing.Color.Navy;
            this.lvwUnit.Location = new System.Drawing.Point(29, 135);
            this.lvwUnit.MultiSelect = false;
            this.lvwUnit.Name = "lvwUnit";
            this.lvwUnit.Size = new System.Drawing.Size(528, 99);
            this.lvwUnit.TabIndex = 1;
            this.lvwUnit.UseCompatibleStateImageBehavior = false;
            this.lvwUnit.DoubleClick += new System.EventHandler(this.lvwUnit_DoubleClick);
            this.lvwUnit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lvwUnit_KeyDown);
            // 
            // tbPrice
            // 
            this.tbPrice.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbPrice.Controls.Add(this.pnlSprice);
            this.tbPrice.Controls.Add(this.lvwSprice);
            this.tbPrice.Location = new System.Drawing.Point(4, 26);
            this.tbPrice.Name = "tbPrice";
            this.tbPrice.Padding = new System.Windows.Forms.Padding(3);
            this.tbPrice.Size = new System.Drawing.Size(590, 242);
            this.tbPrice.TabIndex = 1;
            this.tbPrice.Text = "Price";
            // 
            // pnlSprice
            // 
            this.pnlSprice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSprice.Controls.Add(this.cmbSpUOM);
            this.pnlSprice.Controls.Add(this.btnSPTrans);
            this.pnlSprice.Controls.Add(this.label19);
            this.pnlSprice.Controls.Add(this.gvNewSalePrice);
            this.pnlSprice.Controls.Add(this.label18);
            this.pnlSprice.Controls.Add(this.label17);
            this.pnlSprice.Controls.Add(this.cmbStockPoint);
            this.pnlSprice.Location = new System.Drawing.Point(29, 10);
            this.pnlSprice.Name = "pnlSprice";
            this.pnlSprice.Size = new System.Drawing.Size(528, 100);
            this.pnlSprice.TabIndex = 0;
            // 
            // btnSPTrans
            // 
            this.btnSPTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSPTrans.Location = new System.Drawing.Point(346, 63);
            this.btnSPTrans.Name = "btnSPTrans";
            this.btnSPTrans.Size = new System.Drawing.Size(111, 23);
            this.btnSPTrans.TabIndex = 3;
            this.btnSPTrans.Text = "Trans";
            this.btnSPTrans.UseVisualStyleBackColor = true;
            this.btnSPTrans.Click += new System.EventHandler(this.btnSPTrans_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(362, 15);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(55, 13);
            this.label19.TabIndex = 5;
            this.label19.Text = "Sale Price";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(192, 15);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(32, 13);
            this.label18.TabIndex = 3;
            this.label18.Text = "UOM";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(25, 15);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(59, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "StockPoint";
            // 
            // lvwSprice
            // 
            this.lvwSprice.BackColor = System.Drawing.Color.White;
            this.lvwSprice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lvwSprice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvwSprice.ForeColor = System.Drawing.Color.Navy;
            this.lvwSprice.Location = new System.Drawing.Point(29, 116);
            this.lvwSprice.MultiSelect = false;
            this.lvwSprice.Name = "lvwSprice";
            this.lvwSprice.Size = new System.Drawing.Size(528, 118);
            this.lvwSprice.TabIndex = 2;
            this.lvwSprice.UseCompatibleStateImageBehavior = false;
            this.lvwSprice.DoubleClick += new System.EventHandler(this.lvwSprice_DoubleClick);
            this.lvwSprice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lvwSprice_KeyDown);
            // 
            // tbRecepe
            // 
            this.tbRecepe.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbRecepe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbRecepe.Controls.Add(this.chkCombo);
            this.tbRecepe.Controls.Add(this.lblRecepeHead);
            this.tbRecepe.Controls.Add(this.pnlIngrediant);
            this.tbRecepe.Controls.Add(this.lstIngr);
            this.tbRecepe.Controls.Add(this.trvUOM);
            this.tbRecepe.Location = new System.Drawing.Point(4, 26);
            this.tbRecepe.Name = "tbRecepe";
            this.tbRecepe.Size = new System.Drawing.Size(590, 242);
            this.tbRecepe.TabIndex = 3;
            this.tbRecepe.Text = "Recipe/Combo";
            // 
            // chkCombo
            // 
            this.chkCombo.AutoSize = true;
            this.chkCombo.Location = new System.Drawing.Point(495, 6);
            this.chkCombo.Name = "chkCombo";
            this.chkCombo.Size = new System.Drawing.Size(85, 17);
            this.chkCombo.TabIndex = 6;
            this.chkCombo.Text = "Combo Offer";
            this.chkCombo.UseVisualStyleBackColor = true;
            // 
            // lblRecepeHead
            // 
            this.lblRecepeHead.AutoSize = true;
            this.lblRecepeHead.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecepeHead.Location = new System.Drawing.Point(168, 11);
            this.lblRecepeHead.Name = "lblRecepeHead";
            this.lblRecepeHead.Size = new System.Drawing.Size(31, 13);
            this.lblRecepeHead.TabIndex = 5;
            this.lblRecepeHead.Text = "Item";
            this.lblRecepeHead.Visible = false;
            // 
            // pnlIngrediant
            // 
            this.pnlIngrediant.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlIngrediant.Controls.Add(this.btnIngrTrans);
            this.pnlIngrediant.Controls.Add(this.label24);
            this.pnlIngrediant.Controls.Add(this.gvIngrQty);
            this.pnlIngrediant.Controls.Add(this.label23);
            this.pnlIngrediant.Controls.Add(this.cmbIngrUOM);
            this.pnlIngrediant.Controls.Add(this.txtIngrName);
            this.pnlIngrediant.Controls.Add(this.label22);
            this.pnlIngrediant.Controls.Add(this.txtIngrCode);
            this.pnlIngrediant.Location = new System.Drawing.Point(167, 29);
            this.pnlIngrediant.Name = "pnlIngrediant";
            this.pnlIngrediant.Size = new System.Drawing.Size(413, 72);
            this.pnlIngrediant.TabIndex = 4;
            // 
            // btnIngrTrans
            // 
            this.btnIngrTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIngrTrans.Location = new System.Drawing.Point(291, 44);
            this.btnIngrTrans.Name = "btnIngrTrans";
            this.btnIngrTrans.Size = new System.Drawing.Size(111, 23);
            this.btnIngrTrans.TabIndex = 21;
            this.btnIngrTrans.Text = "Trans";
            this.btnIngrTrans.UseVisualStyleBackColor = true;
            this.btnIngrTrans.Click += new System.EventHandler(this.btnIngrTrans_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(311, 3);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(23, 13);
            this.label24.TabIndex = 20;
            this.label24.Text = "Qty";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(162, 3);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(32, 13);
            this.label23.TabIndex = 18;
            this.label23.Text = "UOM";
            // 
            // txtIngrName
            // 
            this.txtIngrName.BackColor = System.Drawing.Color.White;
            this.txtIngrName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIngrName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIngrName.Location = new System.Drawing.Point(9, 45);
            this.txtIngrName.Name = "txtIngrName";
            this.txtIngrName.ReadOnly = true;
            this.txtIngrName.Size = new System.Drawing.Size(207, 20);
            this.txtIngrName.TabIndex = 16;
            this.txtIngrName.TabStop = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(11, 3);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(38, 13);
            this.label22.TabIndex = 15;
            this.label22.Text = "Code :";
            // 
            // txtIngrCode
            // 
            this.txtIngrCode.BackColor = System.Drawing.Color.Ivory;
            this.txtIngrCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIngrCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIngrCode.Location = new System.Drawing.Point(9, 19);
            this.txtIngrCode.Name = "txtIngrCode";
            this.txtIngrCode.Size = new System.Drawing.Size(103, 20);
            this.txtIngrCode.TabIndex = 14;
            this.txtIngrCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtIngrCode_KeyDown);
            this.txtIngrCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtIngrCode_Validating);
            // 
            // lstIngr
            // 
            this.lstIngr.BackColor = System.Drawing.Color.White;
            this.lstIngr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstIngr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstIngr.ForeColor = System.Drawing.Color.Navy;
            this.lstIngr.Location = new System.Drawing.Point(167, 107);
            this.lstIngr.MultiSelect = false;
            this.lstIngr.Name = "lstIngr";
            this.lstIngr.Size = new System.Drawing.Size(413, 130);
            this.lstIngr.TabIndex = 3;
            this.lstIngr.UseCompatibleStateImageBehavior = false;
            this.lstIngr.DoubleClick += new System.EventHandler(this.lstIngr_DoubleClick);
            this.lstIngr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstIngr_KeyDown);
            // 
            // trvUOM
            // 
            this.trvUOM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.trvUOM.Location = new System.Drawing.Point(9, 13);
            this.trvUOM.Name = "trvUOM";
            this.trvUOM.ShowPlusMinus = false;
            this.trvUOM.ShowRootLines = false;
            this.trvUOM.Size = new System.Drawing.Size(152, 224);
            this.trvUOM.TabIndex = 0;
            this.trvUOM.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.trvUOM_AfterSelect);
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(429, 302);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 2;
            this.btnTrans.TabStop = false;
            this.btnTrans.Text = "Mode";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // picImage
            // 
            this.picImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picImage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picImage.InitialImage = null;
            this.picImage.Location = new System.Drawing.Point(417, 10);
            this.picImage.Name = "picImage";
            this.picImage.Size = new System.Drawing.Size(165, 85);
            this.picImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picImage.TabIndex = 11;
            this.picImage.TabStop = false;
            this.picImage.Click += new System.EventHandler(this.picImage_Click);
            // 
            // frmItem
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.CancelButton = this.btnExit;
            this.ClientSize = new System.Drawing.Size(622, 331);
            this.Controls.Add(this.pnlItem);
            this.Name = "frmItem";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Item";
            this.Load += new System.EventHandler(this.frmItem_Load);
            this.pnlItem.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tbProduct.ResumeLayout(false);
            this.tbProduct.PerformLayout();
            this.grpItemSettings.ResumeLayout(false);
            this.grpItemSettings.PerformLayout();
            this.tbUnit.ResumeLayout(false);
            this.pnlUnit.ResumeLayout(false);
            this.pnlUnit.PerformLayout();
            this.grpConv.ResumeLayout(false);
            this.grpConv.PerformLayout();
            this.tbPrice.ResumeLayout(false);
            this.pnlSprice.ResumeLayout(false);
            this.pnlSprice.PerformLayout();
            this.tbRecepe.ResumeLayout(false);
            this.tbRecepe.PerformLayout();
            this.pnlIngrediant.ResumeLayout(false);
            this.pnlIngrediant.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
    }
}