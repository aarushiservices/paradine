﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Masters
{
    partial class frmTax
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTax = new System.Windows.Forms.TextBox();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.gvTaxPerc = new ParaSysCom.NumControl();
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlTax = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.btnTrans = new System.Windows.Forms.Button();
            this.pnlTax.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tax :";
            // 
            // txtTax
            // 
            this.txtTax.BackColor = System.Drawing.Color.Ivory;
            this.txtTax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTax.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTax.Location = new System.Drawing.Point(75, 13);
            this.txtTax.Name = "txtTax";
            this.txtTax.Size = new System.Drawing.Size(227, 20);
            this.txtTax.TabIndex = 0;
            this.TTip.SetToolTip(this.txtTax, "Enter Tax Name");
            // 
            // gvTaxPerc
            // 
            this.gvTaxPerc.DecimalRequired = true;
            this.gvTaxPerc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvTaxPerc.Location = new System.Drawing.Point(75, 39);
            this.gvTaxPerc.Name = "gvTaxPerc";
            this.gvTaxPerc.Size = new System.Drawing.Size(77, 20);
            this.gvTaxPerc.SymbolRequired = false;
            this.gvTaxPerc.TabIndex = 1;
            this.gvTaxPerc.Tag = "NoTheme";
            this.gvTaxPerc.Text = "0.00";
            this.gvTaxPerc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvTaxPerc, "Enter Tax Percentage");
            this.gvTaxPerc.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(167, 74);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pnlTax
            // 
            this.pnlTax.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlTax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTax.Controls.Add(this.gvTaxPerc);
            this.pnlTax.Controls.Add(this.label2);
            this.pnlTax.Controls.Add(this.btnExit);
            this.pnlTax.Controls.Add(this.btnTrans);
            this.pnlTax.Controls.Add(this.label1);
            this.pnlTax.Controls.Add(this.txtTax);
            this.pnlTax.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTax.Location = new System.Drawing.Point(0, 0);
            this.pnlTax.Name = "pnlTax";
            this.pnlTax.Size = new System.Drawing.Size(332, 109);
            this.pnlTax.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Tax % :";
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(74, 74);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 2;
            this.btnTrans.Text = "Mode";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // frmTax
            // 
            this.ClientSize = new System.Drawing.Size(332, 109);
            this.Controls.Add(this.pnlTax);
            this.Name = "frmTax";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tax";
            this.Load += new System.EventHandler(this.frmtax_Load);
            this.pnlTax.ResumeLayout(false);
            this.pnlTax.PerformLayout();
            this.ResumeLayout(false);

        }


        #endregion
    }
}