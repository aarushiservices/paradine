﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Masters
{
    partial class frmStockPoint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlStockPoint = new System.Windows.Forms.Panel();
            this.chkServTaxApplicable = new System.Windows.Forms.CheckBox();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.chkIsBillPoint = new System.Windows.Forms.CheckBox();
            this.txtStockPoint = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnTrans = new System.Windows.Forms.Button();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.pnlStockPoint.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlStockPoint
            // 
            this.pnlStockPoint.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlStockPoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlStockPoint.Controls.Add(this.chkServTaxApplicable);
            this.pnlStockPoint.Controls.Add(this.txtRemarks);
            this.pnlStockPoint.Controls.Add(this.chkIsBillPoint);
            this.pnlStockPoint.Controls.Add(this.txtStockPoint);
            this.pnlStockPoint.Controls.Add(this.label3);
            this.pnlStockPoint.Controls.Add(this.label1);
            this.pnlStockPoint.Controls.Add(this.btnExit);
            this.pnlStockPoint.Controls.Add(this.btnTrans);
            this.pnlStockPoint.Location = new System.Drawing.Point(1, 4);
            this.pnlStockPoint.Name = "pnlStockPoint";
            this.pnlStockPoint.Size = new System.Drawing.Size(401, 180);
            this.pnlStockPoint.TabIndex = 0;
            this.pnlStockPoint.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlStockPoint_Paint);
            // 
            // chkServTaxApplicable
            // 
            this.chkServTaxApplicable.AutoSize = true;
            this.chkServTaxApplicable.Location = new System.Drawing.Point(244, 97);
            this.chkServTaxApplicable.Name = "chkServTaxApplicable";
            this.chkServTaxApplicable.Size = new System.Drawing.Size(135, 17);
            this.chkServTaxApplicable.TabIndex = 9;
            this.chkServTaxApplicable.Text = "Service Tax Applicable";
            this.chkServTaxApplicable.UseVisualStyleBackColor = true;
            // 
            // txtRemarks
            // 
            this.txtRemarks.BackColor = System.Drawing.Color.White;
            this.txtRemarks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRemarks.Location = new System.Drawing.Point(143, 62);
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(189, 20);
            this.txtRemarks.TabIndex = 1;
            // 
            // chkIsBillPoint
            // 
            this.chkIsBillPoint.AutoSize = true;
            this.chkIsBillPoint.Location = new System.Drawing.Point(143, 97);
            this.chkIsBillPoint.Name = "chkIsBillPoint";
            this.chkIsBillPoint.Size = new System.Drawing.Size(88, 17);
            this.chkIsBillPoint.TabIndex = 2;
            this.chkIsBillPoint.Text = "IsBilling Point";
            this.chkIsBillPoint.UseVisualStyleBackColor = true;
            // 
            // txtStockPoint
            // 
            this.txtStockPoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStockPoint.Location = new System.Drawing.Point(143, 24);
            this.txtStockPoint.Name = "txtStockPoint";
            this.txtStockPoint.Size = new System.Drawing.Size(189, 20);
            this.txtStockPoint.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(76, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Remarks";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(66, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "StockPoint";
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(233, 143);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(140, 143);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 3;
            this.btnTrans.Text = "Mode";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // frmStockPoint
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(402, 185);
            this.Controls.Add(this.pnlStockPoint);
            this.Name = "frmStockPoint";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stock Point";
            this.Load += new System.EventHandler(this.frmStockPoint_Load);
            this.pnlStockPoint.ResumeLayout(false);
            this.pnlStockPoint.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}