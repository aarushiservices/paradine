﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Masters
{
    partial class frmCardType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlDepartment = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.nmValidDays = new ParaSysCom.NumControl();
            this.label3 = new System.Windows.Forms.Label();
            this.nmFaceValue = new ParaSysCom.NumControl();
            this.label2 = new System.Windows.Forms.Label();
            this.gvCardAmount = new ParaSysCom.NumControl();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnTrans = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCardType = new System.Windows.Forms.TextBox();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.pnlDepartment.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDepartment
            // 
            this.pnlDepartment.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlDepartment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDepartment.Controls.Add(this.label4);
            this.pnlDepartment.Controls.Add(this.nmValidDays);
            this.pnlDepartment.Controls.Add(this.label3);
            this.pnlDepartment.Controls.Add(this.nmFaceValue);
            this.pnlDepartment.Controls.Add(this.label2);
            this.pnlDepartment.Controls.Add(this.gvCardAmount);
            this.pnlDepartment.Controls.Add(this.btnExit);
            this.pnlDepartment.Controls.Add(this.btnTrans);
            this.pnlDepartment.Controls.Add(this.label1);
            this.pnlDepartment.Controls.Add(this.txtCardType);
            this.pnlDepartment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDepartment.Location = new System.Drawing.Point(0, 0);
            this.pnlDepartment.Name = "pnlDepartment";
            this.pnlDepartment.Size = new System.Drawing.Size(515, 220);
            this.pnlDepartment.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Valid Days :";
            // 
            // nmValidDays
            // 
            this.nmValidDays.BackColor = System.Drawing.Color.Ivory;
            this.nmValidDays.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmValidDays.DecimalRequired = true;
            this.nmValidDays.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nmValidDays.Location = new System.Drawing.Point(103, 113);
            this.nmValidDays.Name = "nmValidDays";
            this.nmValidDays.Size = new System.Drawing.Size(134, 20);
            this.nmValidDays.SymbolRequired = false;
            this.nmValidDays.TabIndex = 3;
            this.nmValidDays.Text = "0.00";
            this.nmValidDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.nmValidDays, "Enter Valid Days");
            this.nmValidDays.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Face Value :";
            // 
            // nmFaceValue
            // 
            this.nmFaceValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nmFaceValue.DecimalRequired = true;
            this.nmFaceValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nmFaceValue.Location = new System.Drawing.Point(103, 87);
            this.nmFaceValue.Name = "nmFaceValue";
            this.nmFaceValue.Size = new System.Drawing.Size(134, 20);
            this.nmFaceValue.SymbolRequired = true;
            this.nmFaceValue.TabIndex = 2;
            this.nmFaceValue.Text = "`0.00";
            this.nmFaceValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.nmFaceValue, "Enter Face Value");
            this.nmFaceValue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Card Amount :";
            // 
            // gvCardAmount
            // 
            this.gvCardAmount.BackColor = System.Drawing.Color.Ivory;
            this.gvCardAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvCardAmount.DecimalRequired = true;
            this.gvCardAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvCardAmount.Location = new System.Drawing.Point(103, 61);
            this.gvCardAmount.Name = "gvCardAmount";
            this.gvCardAmount.Size = new System.Drawing.Size(134, 20);
            this.gvCardAmount.SymbolRequired = true;
            this.gvCardAmount.TabIndex = 1;
            this.gvCardAmount.Text = "`0.00";
            this.gvCardAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvCardAmount, "Enter Card Amount");
            this.gvCardAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(194, 160);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(101, 160);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 4;
            this.btnTrans.Text = "Mode";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Card Type :";
            // 
            // txtCardType
            // 
            this.txtCardType.BackColor = System.Drawing.Color.Ivory;
            this.txtCardType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCardType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCardType.Location = new System.Drawing.Point(103, 35);
            this.txtCardType.Name = "txtCardType";
            this.txtCardType.Size = new System.Drawing.Size(134, 20);
            this.txtCardType.TabIndex = 0;
            this.TTip.SetToolTip(this.txtCardType, "Enter Card Type");
            // 
            // frmCardType
            // 
            this.ClientSize = new System.Drawing.Size(515, 220);
            this.Controls.Add(this.pnlDepartment);
            this.Name = "frmCardType";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Card Type";
            this.Load += new System.EventHandler(this.frmCardType_Load);
            this.pnlDepartment.ResumeLayout(false);
            this.pnlDepartment.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}