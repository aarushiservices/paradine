﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Masters
{
    partial class frmKitchen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlKitchen = new System.Windows.Forms.Panel();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnTrans = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtKitchen = new System.Windows.Forms.TextBox();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.pnlKitchen.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlKitchen
            // 
            this.pnlKitchen.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlKitchen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlKitchen.Controls.Add(this.btnExit);
            this.pnlKitchen.Controls.Add(this.btnTrans);
            this.pnlKitchen.Controls.Add(this.label1);
            this.pnlKitchen.Controls.Add(this.txtKitchen);
            this.pnlKitchen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlKitchen.Location = new System.Drawing.Point(0, 0);
            this.pnlKitchen.Name = "pnlKitchen";
            this.pnlKitchen.Size = new System.Drawing.Size(322, 108);
            this.pnlKitchen.TabIndex = 1;
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(168, 60);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(75, 60);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 2;
            this.btnTrans.Text = "Mode";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Kitchen :";
            // 
            // txtKitchen
            // 
            this.txtKitchen.BackColor = System.Drawing.Color.Ivory;
            this.txtKitchen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtKitchen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtKitchen.Location = new System.Drawing.Point(75, 28);
            this.txtKitchen.Name = "txtKitchen";
            this.txtKitchen.Size = new System.Drawing.Size(227, 20);
            this.txtKitchen.TabIndex = 0;
            this.TTip.SetToolTip(this.txtKitchen, "Please Enter Kitchen Name");
            // 
            // frmKitchen
            // 
            this.ClientSize = new System.Drawing.Size(322, 108);
            this.Controls.Add(this.pnlKitchen);
            this.Name = "frmKitchen";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kitchen";
            this.Load += new System.EventHandler(this.frmKitchen_Load);
            this.pnlKitchen.ResumeLayout(false);
            this.pnlKitchen.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}