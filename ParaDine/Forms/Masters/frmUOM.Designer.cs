﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Masters
{
    partial class frmUOM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUOM = new System.Windows.Forms.TextBox();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlUOM = new System.Windows.Forms.Panel();
            this.btnTrans = new System.Windows.Forms.Button();
            this.pnlUOM.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "UOM :";
            // 
            // txtUOM
            // 
            this.txtUOM.BackColor = System.Drawing.Color.Ivory;
            this.txtUOM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUOM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUOM.Location = new System.Drawing.Point(73, 19);
            this.txtUOM.Name = "txtUOM";
            this.txtUOM.Size = new System.Drawing.Size(227, 20);
            this.txtUOM.TabIndex = 0;
            this.TTip.SetToolTip(this.txtUOM, "Please Enter UOM");
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(166, 51);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pnlUOM
            // 
            this.pnlUOM.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlUOM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlUOM.Controls.Add(this.btnExit);
            this.pnlUOM.Controls.Add(this.btnTrans);
            this.pnlUOM.Controls.Add(this.label1);
            this.pnlUOM.Controls.Add(this.txtUOM);
            this.pnlUOM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlUOM.Location = new System.Drawing.Point(0, 0);
            this.pnlUOM.Name = "pnlUOM";
            this.pnlUOM.Size = new System.Drawing.Size(320, 91);
            this.pnlUOM.TabIndex = 2;
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(73, 51);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 2;
            this.btnTrans.Text = "Mode";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // frmUOM
            // 
            this.ClientSize = new System.Drawing.Size(320, 91);
            this.Controls.Add(this.pnlUOM);
            this.Name = "frmUOM";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UOM";
            this.Load += new System.EventHandler(this.frmUOM_Load);
            this.pnlUOM.ResumeLayout(false);
            this.pnlUOM.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
    }
}