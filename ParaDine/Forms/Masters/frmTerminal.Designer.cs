﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Masters
{
    partial class frmTerminal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnTrans = new System.Windows.Forms.Button();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.txtTerminal = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlTerminal = new System.Windows.Forms.Panel();
            this.cmbStockPoint = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpBusinessDate = new System.Windows.Forms.DateTimePicker();
            this.pnlTerminal.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(211, 110);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(118, 110);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 2;
            this.btnTrans.Text = "Mode";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // txtTerminal
            // 
            this.txtTerminal.BackColor = System.Drawing.Color.Ivory;
            this.txtTerminal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTerminal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTerminal.Location = new System.Drawing.Point(118, 18);
            this.txtTerminal.Name = "txtTerminal";
            this.txtTerminal.Size = new System.Drawing.Size(203, 20);
            this.txtTerminal.TabIndex = 0;
            this.TTip.SetToolTip(this.txtTerminal, "Please Enter Payment Mode");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Terminal :";
            // 
            // pnlTerminal
            // 
            this.pnlTerminal.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlTerminal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTerminal.Controls.Add(this.cmbStockPoint);
            this.pnlTerminal.Controls.Add(this.label3);
            this.pnlTerminal.Controls.Add(this.label2);
            this.pnlTerminal.Controls.Add(this.dtpBusinessDate);
            this.pnlTerminal.Controls.Add(this.btnExit);
            this.pnlTerminal.Controls.Add(this.btnTrans);
            this.pnlTerminal.Controls.Add(this.label1);
            this.pnlTerminal.Controls.Add(this.txtTerminal);
            this.pnlTerminal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTerminal.Location = new System.Drawing.Point(0, 0);
            this.pnlTerminal.Name = "pnlTerminal";
            this.pnlTerminal.Size = new System.Drawing.Size(342, 145);
            this.pnlTerminal.TabIndex = 0;
            // 
            // cmbStockPoint
            // 
            this.cmbStockPoint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStockPoint.FormattingEnabled = true;
            this.cmbStockPoint.Location = new System.Drawing.Point(118, 71);
            this.cmbStockPoint.Name = "cmbStockPoint";
            this.cmbStockPoint.Size = new System.Drawing.Size(171, 21);
            this.cmbStockPoint.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(50, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Stock Point :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Business Date :";
            // 
            // dtpBusinessDate
            // 
            this.dtpBusinessDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpBusinessDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBusinessDate.Location = new System.Drawing.Point(118, 45);
            this.dtpBusinessDate.Name = "dtpBusinessDate";
            this.dtpBusinessDate.Size = new System.Drawing.Size(98, 20);
            this.dtpBusinessDate.TabIndex = 1;
            this.dtpBusinessDate.Value = new System.DateTime(2008, 5, 26, 0, 0, 0, 0);
            // 
            // frmTerminal
            // 
            this.ClientSize = new System.Drawing.Size(342, 145);
            this.Controls.Add(this.pnlTerminal);
            this.Name = "frmTerminal";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Terminal";
            this.Load += new System.EventHandler(this.frmTerminal_Load);
            this.pnlTerminal.ResumeLayout(false);
            this.pnlTerminal.PerformLayout();
            this.ResumeLayout(false);

        }


        #endregion
    }
}