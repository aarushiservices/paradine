﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Masters
{
    public partial class frmPrepaidCard : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private CheckBox chkInActive;
        //private IContainer components;
        private DateTimePicker dtpIssueDate;
        private DateTimePicker dtpValidUpto;
        public PrepaidCard EntId;
        private Label label1;
        private Label label3;
        private Label label4;
        private Label label6;
        private Panel pnlSteward;
        private ToolTip TTip;
        private TextBox txtCardID;
        private TextBox txtReason;

        public frmPrepaidCard()
        {
            this.EntId = new PrepaidCard();
            this.components = null;
            this.InitializeComponent();
        }

        public frmPrepaidCard(string Mode, long ID, SecurityClass paramSecurity)
        {
            this.EntId = new PrepaidCard();
            this.components = null;
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes(ID);
            this.EntId.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (GlobalValidations.ValidateFields(this.pnlSteward, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_007D;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_008D;
                            }
                        }
                        else
                        {
                            this.EntId.Add(sqlTrans, true);
                        }
                    }
                    goto Label_009F;
                Label_007D:
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_009F;
                Label_008D:
                    this.EntId.Delete(sqlTrans, true);
                Label_009F:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, exception.Source);
                    sqlTrans.Rollback();
                }
            }
        }

       
        private void frmPrepaidCard_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                this.RefreshData();
                this.LoadFields();
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                base.Close();
            }
        }

     

        private void LoadEntities()
        {
            this.EntId.PrepaidCardID = Convert.ToInt64(this.txtCardID.Tag);
            this.EntId.CardNo = Convert.ToInt64(this.txtCardID.Text);
            this.EntId.IssueDate = Convert.ToDateTime(this.dtpIssueDate.Text);
            this.EntId.ValidUpto = Convert.ToDateTime(this.dtpValidUpto.Text);
            this.EntId.Reason = Convert.ToString(this.txtReason.Text);
            this.EntId.Inactive = this.chkInActive.Checked;
        }

        private void LoadFields()
        {
            this.txtCardID.Tag = Convert.ToInt64(this.EntId.PrepaidCardID);
            this.txtCardID.Text = Convert.ToString(this.EntId.CardNo);
            this.dtpIssueDate.Value = Convert.ToDateTime(this.EntId.IssueDate);
            this.dtpValidUpto.Value = Convert.ToDateTime(this.EntId.ValidUpto);
            this.txtReason.Text = Convert.ToString(this.EntId.Reason);
            this.chkInActive.Checked = this.EntId.Inactive;
        }

        private void RefreshData()
        {
            try
            {
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Refresh Data");
                throw;
            }
        }
    }
}
