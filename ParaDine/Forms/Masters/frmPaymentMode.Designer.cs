﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Masters
{
    partial class frmPaymentMode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPaymentMode = new System.Windows.Forms.TextBox();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlPaymentMode = new System.Windows.Forms.Panel();
            this.chkCredit = new System.Windows.Forms.CheckBox();
            this.btnTrans = new System.Windows.Forms.Button();
            this.pnlPaymentMode.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Payment Mode :";
            // 
            // txtPaymentMode
            // 
            this.txtPaymentMode.BackColor = System.Drawing.Color.Ivory;
            this.txtPaymentMode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPaymentMode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPaymentMode.Location = new System.Drawing.Point(95, 24);
            this.txtPaymentMode.Name = "txtPaymentMode";
            this.txtPaymentMode.Size = new System.Drawing.Size(227, 20);
            this.txtPaymentMode.TabIndex = 0;
            this.TTip.SetToolTip(this.txtPaymentMode, "Please Enter Payment Mode");
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(164, 78);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pnlPaymentMode
            // 
            this.pnlPaymentMode.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlPaymentMode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPaymentMode.Controls.Add(this.chkCredit);
            this.pnlPaymentMode.Controls.Add(this.btnExit);
            this.pnlPaymentMode.Controls.Add(this.btnTrans);
            this.pnlPaymentMode.Controls.Add(this.label1);
            this.pnlPaymentMode.Controls.Add(this.txtPaymentMode);
            this.pnlPaymentMode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPaymentMode.Location = new System.Drawing.Point(0, 0);
            this.pnlPaymentMode.Name = "pnlPaymentMode";
            this.pnlPaymentMode.Size = new System.Drawing.Size(336, 113);
            this.pnlPaymentMode.TabIndex = 0;
            // 
            // chkCredit
            // 
            this.chkCredit.AutoSize = true;
            this.chkCredit.Location = new System.Drawing.Point(96, 51);
            this.chkCredit.Name = "chkCredit";
            this.chkCredit.Size = new System.Drawing.Size(53, 17);
            this.chkCredit.TabIndex = 1;
            this.chkCredit.Text = "Credit";
            this.chkCredit.UseVisualStyleBackColor = true;
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(71, 78);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 2;
            this.btnTrans.Text = "Mode";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // frmPaymentMode
            // 
            this.ClientSize = new System.Drawing.Size(336, 113);
            this.Controls.Add(this.pnlPaymentMode);
            this.Name = "frmPaymentMode";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Payment Mode";
            this.Load += new System.EventHandler(this.frmPaymentMode_Load);
            this.pnlPaymentMode.ResumeLayout(false);
            this.pnlPaymentMode.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}