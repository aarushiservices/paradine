﻿using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Masters
{
    public partial class frmCardType : Form
    {
        private Button btnExit;
        private Button btnTrans;
        //private IContainer components = null;
        private CardType EntId = new CardType();
        private NumControl gvCardAmount;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private NumControl nmFaceValue;
        private NumControl nmValidDays;
        private Panel pnlDepartment;
        private ToolTip TTip;
        private TextBox txtCardType;

        public frmCardType(string Mode, int ID, SecurityClass paramSecurity)
        {
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes((long)ID);
            this.EntId.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (ParaDine.GlobalValidations.ValidateFields(this.pnlDepartment, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_007D;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_008D;
                            }
                        }
                        else
                        {
                            this.EntId.Add(sqlTrans, true);
                        }
                    }
                    goto Label_009F;
                Label_007D:
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_009F;
                Label_008D:
                    this.EntId.Delete(sqlTrans, true);
                Label_009F:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, exception.Source);
                    sqlTrans.Rollback();
                }
            }
        }

       

        private void frmCardType_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                this.LoadFields();
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                base.Close();
            }
        }

        

        private void LoadEntities()
        {
            this.EntId.CardTypeId = Convert.ToInt32(this.txtCardType.Tag);
            this.EntId.CardTypeName = Convert.ToString(this.txtCardType.Text);
            this.EntId.CardAmount = Convert.ToDouble(this.gvCardAmount.Value);
            this.EntId.FaceValue = Convert.ToDouble(this.nmFaceValue.Value);
            this.EntId.ValidDays = Convert.ToInt32(this.nmValidDays.Value);
        }

        private void LoadFields()
        {
            this.txtCardType.Tag = this.EntId.CardTypeId;
            this.txtCardType.Text = this.EntId.CardTypeName;
            this.gvCardAmount.Value = Convert.ToDecimal(this.EntId.CardAmount);
            this.nmFaceValue.Value = Convert.ToDecimal(this.EntId.FaceValue);
            this.nmValidDays.Value = this.EntId.ValidDays;
        }
    }
}
