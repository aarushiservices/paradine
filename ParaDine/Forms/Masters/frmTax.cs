﻿using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Masters
{
    public partial class frmTax : Form
    {
        private Button btnExit;
        private Button btnTrans;
        //private IContainer components = null;
        private Tax EntId = new Tax();
        private NumControl gvTaxPerc;
        private Label label1;
        private Label label2;
        private Panel pnlTax;
        private ToolTip TTip;
        private TextBox txtTax;

        public frmTax(string Mode, int ID, SecurityClass paramSecurity)
        {
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes((long)ID);
            this.EntId.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (ParaDine.GlobalValidations.ValidateFields(this.pnlTax, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_007D;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_008D;
                            }
                        }
                        else
                        {
                            this.EntId.Add(sqlTrans, true);
                        }
                    }
                    goto Label_009F;
                Label_007D:
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_009F;
                Label_008D:
                    this.EntId.Delete(sqlTrans, true);
                Label_009F:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, exception.Source);
                    sqlTrans.Rollback();
                }
            }
        }

       

        private void frmtax_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                this.LoadFields();
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                base.Close();
            }
        }

     
        private void LoadEntities()
        {
            this.EntId.TaxID = Convert.ToInt32(this.txtTax.Tag);
            this.EntId.TaxName = Convert.ToString(this.txtTax.Text);
            this.EntId.TaxPerc = Convert.ToDouble(this.gvTaxPerc.Value);
        }

        private void LoadFields()
        {
            this.txtTax.Tag = Convert.ToInt32(this.EntId.TaxID);
            this.txtTax.Text = Convert.ToString(this.EntId.TaxName);
            this.gvTaxPerc.Value = Convert.ToDecimal(this.EntId.TaxPerc);
        }
    }
}
