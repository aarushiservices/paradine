﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Masters
{
    partial class frmSupplier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlSupplier = new System.Windows.Forms.Panel();
            this.chkInActive = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtCSTNo = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtGSTNo = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTinNumber = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCellNo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtContPerson = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtFaxNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPhNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSuppName = new System.Windows.Forms.TextBox();
            this.btnTrans = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSuppCode = new System.Windows.Forms.TextBox();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.pnlSupplier.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(163, 416);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 14;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pnlSupplier
            // 
            this.pnlSupplier.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlSupplier.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSupplier.Controls.Add(this.chkInActive);
            this.pnlSupplier.Controls.Add(this.label12);
            this.pnlSupplier.Controls.Add(this.txtRemarks);
            this.pnlSupplier.Controls.Add(this.label11);
            this.pnlSupplier.Controls.Add(this.txtCSTNo);
            this.pnlSupplier.Controls.Add(this.label10);
            this.pnlSupplier.Controls.Add(this.txtGSTNo);
            this.pnlSupplier.Controls.Add(this.label9);
            this.pnlSupplier.Controls.Add(this.txtTinNumber);
            this.pnlSupplier.Controls.Add(this.label8);
            this.pnlSupplier.Controls.Add(this.txtEmail);
            this.pnlSupplier.Controls.Add(this.label7);
            this.pnlSupplier.Controls.Add(this.txtCellNo);
            this.pnlSupplier.Controls.Add(this.label6);
            this.pnlSupplier.Controls.Add(this.txtContPerson);
            this.pnlSupplier.Controls.Add(this.label5);
            this.pnlSupplier.Controls.Add(this.txtFaxNo);
            this.pnlSupplier.Controls.Add(this.label4);
            this.pnlSupplier.Controls.Add(this.txtPhNo);
            this.pnlSupplier.Controls.Add(this.label3);
            this.pnlSupplier.Controls.Add(this.txtAddress);
            this.pnlSupplier.Controls.Add(this.label2);
            this.pnlSupplier.Controls.Add(this.txtSuppName);
            this.pnlSupplier.Controls.Add(this.btnExit);
            this.pnlSupplier.Controls.Add(this.btnTrans);
            this.pnlSupplier.Controls.Add(this.label1);
            this.pnlSupplier.Controls.Add(this.txtSuppCode);
            this.pnlSupplier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSupplier.Location = new System.Drawing.Point(0, 0);
            this.pnlSupplier.Name = "pnlSupplier";
            this.pnlSupplier.Size = new System.Drawing.Size(310, 452);
            this.pnlSupplier.TabIndex = 0;
            // 
            // chkInActive
            // 
            this.chkInActive.AutoSize = true;
            this.chkInActive.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkInActive.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInActive.Location = new System.Drawing.Point(100, 381);
            this.chkInActive.Name = "chkInActive";
            this.chkInActive.Size = new System.Drawing.Size(70, 17);
            this.chkInActive.TabIndex = 12;
            this.chkInActive.Text = "InActive";
            this.chkInActive.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(38, 338);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 25;
            this.label12.Text = "Remarks :";
            // 
            // txtRemarks
            // 
            this.txtRemarks.BackColor = System.Drawing.Color.White;
            this.txtRemarks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRemarks.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRemarks.Location = new System.Drawing.Point(100, 336);
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(197, 39);
            this.txtRemarks.TabIndex = 11;
            this.TTip.SetToolTip(this.txtRemarks, "Please Enter Remarks");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(55, 312);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "CST #";
            // 
            // txtCSTNo
            // 
            this.txtCSTNo.BackColor = System.Drawing.Color.White;
            this.txtCSTNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCSTNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCSTNo.Location = new System.Drawing.Point(100, 310);
            this.txtCSTNo.Name = "txtCSTNo";
            this.txtCSTNo.Size = new System.Drawing.Size(197, 20);
            this.txtCSTNo.TabIndex = 10;
            this.TTip.SetToolTip(this.txtCSTNo, "Please Enter CST No");
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(55, 286);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "GST #";
            // 
            // txtGSTNo
            // 
            this.txtGSTNo.BackColor = System.Drawing.Color.White;
            this.txtGSTNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGSTNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtGSTNo.Location = new System.Drawing.Point(100, 284);
            this.txtGSTNo.Name = "txtGSTNo";
            this.txtGSTNo.Size = new System.Drawing.Size(197, 20);
            this.txtGSTNo.TabIndex = 9;
            this.TTip.SetToolTip(this.txtGSTNo, "Please Enter GST Number");
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(60, 260);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "TIN #";
            // 
            // txtTinNumber
            // 
            this.txtTinNumber.BackColor = System.Drawing.Color.White;
            this.txtTinNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTinNumber.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTinNumber.Location = new System.Drawing.Point(100, 258);
            this.txtTinNumber.Name = "txtTinNumber";
            this.txtTinNumber.Size = new System.Drawing.Size(197, 20);
            this.txtTinNumber.TabIndex = 8;
            this.TTip.SetToolTip(this.txtTinNumber, "Please Enter Tin Number");
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(56, 234);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Email :";
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.Color.Ivory;
            this.txtEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.txtEmail.Location = new System.Drawing.Point(100, 232);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(197, 20);
            this.txtEmail.TabIndex = 7;
            this.TTip.SetToolTip(this.txtEmail, "Please Enter Email");
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(60, 208);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Cell #";
            // 
            // txtCellNo
            // 
            this.txtCellNo.BackColor = System.Drawing.Color.White;
            this.txtCellNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCellNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCellNo.Location = new System.Drawing.Point(100, 206);
            this.txtCellNo.Name = "txtCellNo";
            this.txtCellNo.Size = new System.Drawing.Size(197, 20);
            this.txtCellNo.TabIndex = 6;
            this.TTip.SetToolTip(this.txtCellNo, "Please Enter Cell No");
            this.txtCellNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCellNo_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 182);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Contact Person :";
            // 
            // txtContPerson
            // 
            this.txtContPerson.BackColor = System.Drawing.Color.Ivory;
            this.txtContPerson.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtContPerson.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtContPerson.Location = new System.Drawing.Point(100, 180);
            this.txtContPerson.Name = "txtContPerson";
            this.txtContPerson.Size = new System.Drawing.Size(197, 20);
            this.txtContPerson.TabIndex = 5;
            this.TTip.SetToolTip(this.txtContPerson, "Please Enter Cont Person");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(60, 156);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Fax #";
            // 
            // txtFaxNo
            // 
            this.txtFaxNo.BackColor = System.Drawing.Color.White;
            this.txtFaxNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFaxNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFaxNo.Location = new System.Drawing.Point(100, 154);
            this.txtFaxNo.Name = "txtFaxNo";
            this.txtFaxNo.Size = new System.Drawing.Size(197, 20);
            this.txtFaxNo.TabIndex = 4;
            this.TTip.SetToolTip(this.txtFaxNo, "Please Enter FaxNo");
            this.txtFaxNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFaxNo_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(49, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Phone #";
            // 
            // txtPhNo
            // 
            this.txtPhNo.BackColor = System.Drawing.Color.Ivory;
            this.txtPhNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPhNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPhNo.Location = new System.Drawing.Point(100, 128);
            this.txtPhNo.Name = "txtPhNo";
            this.txtPhNo.Size = new System.Drawing.Size(197, 20);
            this.txtPhNo.TabIndex = 3;
            this.TTip.SetToolTip(this.txtPhNo, "Please Enter Phone No");
            this.txtPhNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhNo_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(43, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Address :";
            // 
            // txtAddress
            // 
            this.txtAddress.BackColor = System.Drawing.Color.White;
            this.txtAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddress.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAddress.Location = new System.Drawing.Point(100, 59);
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(197, 63);
            this.txtAddress.TabIndex = 2;
            this.TTip.SetToolTip(this.txtAddress, "Please Enter Supplier Code");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Supplier Name :";
            // 
            // txtSuppName
            // 
            this.txtSuppName.BackColor = System.Drawing.Color.Ivory;
            this.txtSuppName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSuppName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSuppName.Location = new System.Drawing.Point(100, 33);
            this.txtSuppName.Name = "txtSuppName";
            this.txtSuppName.Size = new System.Drawing.Size(197, 20);
            this.txtSuppName.TabIndex = 1;
            this.TTip.SetToolTip(this.txtSuppName, "Please Enter Supplier Name");
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(70, 416);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 13;
            this.btnTrans.Text = "Mode";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Supplier Code :";
            // 
            // txtSuppCode
            // 
            this.txtSuppCode.BackColor = System.Drawing.Color.Ivory;
            this.txtSuppCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSuppCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSuppCode.Enabled = false;
            this.txtSuppCode.Location = new System.Drawing.Point(100, 7);
            this.txtSuppCode.Name = "txtSuppCode";
            this.txtSuppCode.Size = new System.Drawing.Size(77, 20);
            this.txtSuppCode.TabIndex = 0;
            this.TTip.SetToolTip(this.txtSuppCode, "Please Enter Supplier Code");
            // 
            // frmSupplier
            // 
            this.ClientSize = new System.Drawing.Size(310, 452);
            this.Controls.Add(this.pnlSupplier);
            this.Name = "frmSupplier";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Supplier";
            this.Load += new System.EventHandler(this.frmSupplier_Load);
            this.pnlSupplier.ResumeLayout(false);
            this.pnlSupplier.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}