﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Masters
{
    public partial class frmSection : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private ComboBox cmbKotPrinter;
        private ComboBox cmbPrinter;
        //private IContainer components = null;
        private Section EntId = new Section();
        private Label label1;
        private Label label2;
        private Label label3;
        private Panel pnlSection;
        private ToolTip TTip;
        private TextBox txtSection;

        public frmSection(string Mode, int ID, SecurityClass paramSecurity)
        {
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes((long)ID);
            this.EntId.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (GlobalValidations.ValidateFields(this.pnlSection, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_007D;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_008D;
                            }
                        }
                        else
                        {
                            this.EntId.Add(sqlTrans, true);
                        }
                    }
                    goto Label_009F;
                Label_007D:
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_009F;
                Label_008D:
                    this.EntId.Delete(sqlTrans, true);
                Label_009F:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, exception.Source);
                    sqlTrans.Rollback();
                }
            }
        }

       

        private void frmSection_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                this.RefreshData();
                this.LoadFields();
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                base.Close();
            }
        }

    

        private void LoadEntities()
        {
            this.EntId.SectionID = Convert.ToInt32(this.txtSection.Tag);
            this.EntId.SectionName = Convert.ToString(this.txtSection.Text);
            this.EntId.PrinterName = Convert.ToString(this.cmbPrinter.Text);
            this.EntId.KotPrinterName = Convert.ToString(this.cmbKotPrinter.Text);
        }

        private void LoadFields()
        {
            this.txtSection.Tag = this.EntId.SectionID;
            this.txtSection.Text = this.EntId.SectionName;
            this.cmbPrinter.Text = this.EntId.PrinterName;
            this.cmbKotPrinter.Text = this.EntId.KotPrinterName;
        }

        private void RefreshData()
        {
            try
            {
                this.cmbPrinter.Items.Clear();
                foreach (string str in PrinterSettings.InstalledPrinters)
                {
                    this.cmbPrinter.Items.Add(str);
                    this.cmbKotPrinter.Items.Add(str);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "RefreshData");
                throw;
            }
        }
    }
}
