﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Masters
{
    partial class frmStore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlStore = new System.Windows.Forms.Panel();
            this.cmbCompany = new System.Windows.Forms.ComboBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnTrans = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStore = new System.Windows.Forms.TextBox();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.pnlStore.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlStore
            // 
            this.pnlStore.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlStore.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlStore.Controls.Add(this.cmbCompany);
            this.pnlStore.Controls.Add(this.btnExit);
            this.pnlStore.Controls.Add(this.btnTrans);
            this.pnlStore.Controls.Add(this.label2);
            this.pnlStore.Controls.Add(this.label1);
            this.pnlStore.Controls.Add(this.txtStore);
            this.pnlStore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlStore.Location = new System.Drawing.Point(0, 0);
            this.pnlStore.Name = "pnlStore";
            this.pnlStore.Size = new System.Drawing.Size(318, 117);
            this.pnlStore.TabIndex = 1;
            // 
            // cmbCompany
            // 
            this.cmbCompany.FormattingEnabled = true;
            this.cmbCompany.Location = new System.Drawing.Point(75, 39);
            this.cmbCompany.Name = "cmbCompany";
            this.cmbCompany.Size = new System.Drawing.Size(227, 21);
            this.cmbCompany.TabIndex = 4;
            this.TTip.SetToolTip(this.cmbCompany, "Please Select Company");
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(168, 77);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(75, 77);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 2;
            this.btnTrans.Text = "Mode";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Company :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Store Name :";
            // 
            // txtStore
            // 
            this.txtStore.BackColor = System.Drawing.Color.Ivory;
            this.txtStore.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStore.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStore.Location = new System.Drawing.Point(75, 12);
            this.txtStore.Name = "txtStore";
            this.txtStore.Size = new System.Drawing.Size(227, 20);
            this.txtStore.TabIndex = 0;
            this.TTip.SetToolTip(this.txtStore, "Please Enter Section Name");
            // 
            // frmStore
            // 
            this.ClientSize = new System.Drawing.Size(318, 117);
            this.Controls.Add(this.pnlStore);
            this.Name = "frmStore";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Store";
            this.Load += new System.EventHandler(this.frmStore_Load);
            this.pnlStore.ResumeLayout(false);
            this.pnlStore.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}