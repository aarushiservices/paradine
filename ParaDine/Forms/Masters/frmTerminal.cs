﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Masters
{
    public partial class frmTerminal : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private ComboBox cmbStockPoint;
        //private IContainer components = null;
        private DateTimePicker dtpBusinessDate;
        private Terminal EntId = new Terminal();
        private Label label1;
        private Label label2;
        private Label label3;
        private Panel pnlTerminal;
        private ToolTip TTip;
        private TextBox txtTerminal;

        public frmTerminal(string Mode, int ID, SecurityClass paramSecurity)
        {
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes((long)ID);
            this.EntId.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (GlobalValidations.ValidateFields(this.pnlTerminal, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_007D;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_008D;
                            }
                        }
                        else
                        {
                            this.EntId.Add(sqlTrans, true);
                        }
                    }
                    goto Label_009F;
                Label_007D:
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_009F;
                Label_008D:
                    this.EntId.Delete(sqlTrans, true);
                Label_009F:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, exception.Source);
                    sqlTrans.Rollback();
                }
            }
        }

       

        private void frmTerminal_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                GlobalFill.FillCombo("SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT ORDER BY 2", this.cmbStockPoint);
                this.LoadFields();
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                base.Close();
            }
        }


        private void LoadEntities()
        {
            this.EntId.TerminalID = Convert.ToInt32(this.txtTerminal.Tag);
            this.EntId.TerminalName = Convert.ToString(this.txtTerminal.Text);
            this.EntId.BusinessDate = Convert.ToDateTime(this.dtpBusinessDate.Text);
            this.EntId.StockPointID = Convert.ToInt32(this.cmbStockPoint.SelectedValue);
        }

        private void LoadFields()
        {
            this.txtTerminal.Tag = Convert.ToInt32(this.EntId.TerminalID);
            this.txtTerminal.Text = Convert.ToString(this.EntId.TerminalName);
            if (this.EntId.TerminalID > 0)
            {
                this.dtpBusinessDate.Value = Convert.ToDateTime(this.EntId.BusinessDate);
            }
            if (this.EntId.StockPointID > 0)
            {
                this.cmbStockPoint.SelectedValue = Convert.ToInt32(this.EntId.StockPointID);
            }
        }
    }
}
