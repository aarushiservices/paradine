﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Masters
{
    partial class frmSteward
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlSteward = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbGender = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dtpDOJ = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpDOB = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.chkInActive = new System.Windows.Forms.CheckBox();
            this.txtContactNo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.PbxImage = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSteward = new System.Windows.Forms.TextBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnTrans = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStewardCode = new System.Windows.Forms.TextBox();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.pnlSteward.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSteward
            // 
            this.pnlSteward.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlSteward.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSteward.Controls.Add(this.label7);
            this.pnlSteward.Controls.Add(this.cmbGender);
            this.pnlSteward.Controls.Add(this.groupBox2);
            this.pnlSteward.Controls.Add(this.chkInActive);
            this.pnlSteward.Controls.Add(this.txtContactNo);
            this.pnlSteward.Controls.Add(this.label6);
            this.pnlSteward.Controls.Add(this.txtAddress);
            this.pnlSteward.Controls.Add(this.label5);
            this.pnlSteward.Controls.Add(this.PbxImage);
            this.pnlSteward.Controls.Add(this.label2);
            this.pnlSteward.Controls.Add(this.txtSteward);
            this.pnlSteward.Controls.Add(this.btnExit);
            this.pnlSteward.Controls.Add(this.btnTrans);
            this.pnlSteward.Controls.Add(this.label1);
            this.pnlSteward.Controls.Add(this.txtStewardCode);
            this.pnlSteward.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSteward.Location = new System.Drawing.Point(0, 0);
            this.pnlSteward.Name = "pnlSteward";
            this.pnlSteward.Size = new System.Drawing.Size(400, 347);
            this.pnlSteward.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(26, 261);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Gender :";
            // 
            // cmbGender
            // 
            this.cmbGender.BackColor = System.Drawing.Color.Ivory;
            this.cmbGender.FormattingEnabled = true;
            this.cmbGender.Location = new System.Drawing.Point(81, 256);
            this.cmbGender.Name = "cmbGender";
            this.cmbGender.Size = new System.Drawing.Size(73, 21);
            this.cmbGender.TabIndex = 5;
            this.TTip.SetToolTip(this.cmbGender, "Please Select Gender");
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dtpDOJ);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.dtpDOB);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(80, 182);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(245, 69);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Days To Remember";
            // 
            // dtpDOJ
            // 
            this.dtpDOJ.CustomFormat = "dd/MMM/yyyy";
            this.dtpDOJ.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDOJ.Location = new System.Drawing.Point(14, 37);
            this.dtpDOJ.Name = "dtpDOJ";
            this.dtpDOJ.Size = new System.Drawing.Size(105, 20);
            this.dtpDOJ.TabIndex = 0;
            this.TTip.SetToolTip(this.dtpDOJ, "Please Enter Date Of Join");
            this.dtpDOJ.Value = new System.DateTime(2008, 5, 24, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Date Of Join :";
            // 
            // dtpDOB
            // 
            this.dtpDOB.CustomFormat = "dd/MMM/yyyy";
            this.dtpDOB.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDOB.Location = new System.Drawing.Point(128, 37);
            this.dtpDOB.Name = "dtpDOB";
            this.dtpDOB.Size = new System.Drawing.Size(105, 20);
            this.dtpDOB.TabIndex = 1;
            this.TTip.SetToolTip(this.dtpDOB, "Please Enter Date Of Birth");
            this.dtpDOB.Value = new System.DateTime(2008, 5, 24, 0, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(125, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Date Of Birth :";
            // 
            // chkInActive
            // 
            this.chkInActive.AutoSize = true;
            this.chkInActive.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkInActive.Location = new System.Drawing.Point(160, 259);
            this.chkInActive.Name = "chkInActive";
            this.chkInActive.Size = new System.Drawing.Size(62, 17);
            this.chkInActive.TabIndex = 6;
            this.chkInActive.Text = "InActive";
            this.chkInActive.UseVisualStyleBackColor = true;
            // 
            // txtContactNo
            // 
            this.txtContactNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtContactNo.Location = new System.Drawing.Point(80, 156);
            this.txtContactNo.Name = "txtContactNo";
            this.txtContactNo.Size = new System.Drawing.Size(186, 20);
            this.txtContactNo.TabIndex = 3;
            this.TTip.SetToolTip(this.txtContactNo, "Please Enter Contact No.");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Contact No :";
            // 
            // txtAddress
            // 
            this.txtAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddress.Location = new System.Drawing.Point(80, 79);
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(186, 72);
            this.txtAddress.TabIndex = 2;
            this.TTip.SetToolTip(this.txtAddress, "Please Enter Address");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Address :";
            // 
            // PbxImage
            // 
            this.PbxImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PbxImage.Location = new System.Drawing.Point(272, 24);
            this.PbxImage.Name = "PbxImage";
            this.PbxImage.Size = new System.Drawing.Size(111, 127);
            this.PbxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PbxImage.TabIndex = 6;
            this.PbxImage.TabStop = false;
            this.PbxImage.Click += new System.EventHandler(this.PbxImage_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Steward :";
            // 
            // txtSteward
            // 
            this.txtSteward.BackColor = System.Drawing.Color.Ivory;
            this.txtSteward.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSteward.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSteward.Location = new System.Drawing.Point(81, 53);
            this.txtSteward.Name = "txtSteward";
            this.txtSteward.Size = new System.Drawing.Size(185, 20);
            this.txtSteward.TabIndex = 1;
            this.TTip.SetToolTip(this.txtSteward, "Please Enter Steward Name");
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(208, 311);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 8;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(115, 311);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 7;
            this.btnTrans.Text = "Mode";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Code :";
            // 
            // txtStewardCode
            // 
            this.txtStewardCode.BackColor = System.Drawing.Color.Ivory;
            this.txtStewardCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStewardCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStewardCode.Enabled = false;
            this.txtStewardCode.Location = new System.Drawing.Point(81, 24);
            this.txtStewardCode.Name = "txtStewardCode";
            this.txtStewardCode.Size = new System.Drawing.Size(91, 20);
            this.txtStewardCode.TabIndex = 0;
            this.TTip.SetToolTip(this.txtStewardCode, "Displays Steward Code");
            // 
            // frmSteward
            // 
            this.ClientSize = new System.Drawing.Size(400, 347);
            this.Controls.Add(this.pnlSteward);
            this.Name = "frmSteward";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Steward";
            this.Load += new System.EventHandler(this.frmSteward_Load);
            this.pnlSteward.ResumeLayout(false);
            this.pnlSteward.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbxImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
    }
}