﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Masters
{
    public partial class frmSupplier : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private CheckBox chkInActive;
        //private IContainer components = null;
        private Supplier EntId = new Supplier();
        private Label label1;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private Panel pnlSupplier;
        private ToolTip TTip;
        private TextBox txtAddress;
        private TextBox txtCellNo;
        private TextBox txtContPerson;
        private TextBox txtCSTNo;
        private TextBox txtEmail;
        private TextBox txtFaxNo;
        private TextBox txtGSTNo;
        private TextBox txtPhNo;
        private TextBox txtRemarks;
        private TextBox txtSuppCode;
        private TextBox txtSuppName;
        private TextBox txtTinNumber;

        public frmSupplier(string Mode, int ID, SecurityClass paramSecurity)
        {
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes((long)ID);
            this.EntId.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (GlobalValidations.ValidateFields(this.pnlSupplier, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_007D;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_008D;
                            }
                        }
                        else
                        {
                            this.EntId.Add(sqlTrans, true);
                        }
                    }
                    goto Label_009F;
                Label_007D:
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_009F;
                Label_008D:
                    this.EntId.Delete(sqlTrans, true);
                Label_009F:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, exception.Source);
                    sqlTrans.Rollback();
                }
            }
        }

       

        private void frmSupplier_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                this.LoadFields();
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                base.Close();
            }
        }

     

        private void LoadEntities()
        {
            this.EntId.SupplierID = Convert.ToInt32(this.txtSuppCode.Tag);
            this.EntId.SupplierCode = Convert.ToString(this.txtSuppCode.Text);
            this.EntId.SupplierName = Convert.ToString(this.txtSuppName.Text);
            this.EntId.Address = Convert.ToString(this.txtAddress.Text);
            this.EntId.PhoneNo = Convert.ToString(this.txtPhNo.Text);
            this.EntId.FaxNo = Convert.ToString(this.txtFaxNo.Text);
            this.EntId.Contactperson = Convert.ToString(this.txtContPerson.Text);
            this.EntId.CellNo = Convert.ToString(this.txtCellNo.Text);
            this.EntId.Email = Convert.ToString(this.txtEmail.Text);
            this.EntId.TINNumber = Convert.ToString(this.txtTinNumber.Text);
            this.EntId.GSTNumber = Convert.ToString(this.txtGSTNo.Text);
            this.EntId.CSTNumber = Convert.ToString(this.txtCSTNo.Text);
            this.EntId.Remarks = Convert.ToString(this.txtRemarks.Text);
            this.EntId.InActive = Convert.ToBoolean(this.chkInActive.Checked);
        }

        private void LoadFields()
        {
            this.txtSuppCode.Tag = Convert.ToInt32(this.EntId.SupplierID);
            if (this.EntId.SupplierID > 0L)
            {
                this.txtSuppCode.Text = Convert.ToString(this.EntId.SupplierCode);
            }
            else
            {
                this.txtSuppCode.Text = this.EntId.MaxCode();
            }
            this.txtSuppName.Text = Convert.ToString(this.EntId.SupplierName);
            this.txtAddress.Text = Convert.ToString(this.EntId.Address);
            this.txtPhNo.Text = Convert.ToString(this.EntId.PhoneNo);
            this.txtFaxNo.Text = Convert.ToString(this.EntId.FaxNo);
            this.txtContPerson.Text = Convert.ToString(this.EntId.Contactperson);
            this.txtCellNo.Text = Convert.ToString(this.EntId.CellNo);
            this.txtEmail.Text = Convert.ToString(this.EntId.Email);
            this.txtTinNumber.Text = Convert.ToString(this.EntId.TINNumber);
            this.txtGSTNo.Text = Convert.ToString(this.EntId.GSTNumber);
            this.txtCSTNo.Text = Convert.ToString(this.EntId.CSTNumber);
            this.txtRemarks.Text = Convert.ToString(this.EntId.Remarks);
            this.chkInActive.Checked = Convert.ToBoolean(this.EntId.InActive);
        }

        private void txtCellNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            GlobalValidations.ValidateKeys(e, false, true, false, "-");
        }

        private void txtFaxNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            GlobalValidations.ValidateKeys(e, false, true, false, "-");
        }

        private void txtPhNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            GlobalValidations.ValidateKeys(e, false, true, false, "-");
        }
    }
}
