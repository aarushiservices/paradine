﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Masters
{
    partial class frmSection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlSection = new System.Windows.Forms.Panel();
            this.cmbKotPrinter = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbPrinter = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnTrans = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSection = new System.Windows.Forms.TextBox();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.pnlSection.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(203, 209);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pnlSection
            // 
            this.pnlSection.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlSection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSection.Controls.Add(this.cmbKotPrinter);
            this.pnlSection.Controls.Add(this.label2);
            this.pnlSection.Controls.Add(this.cmbPrinter);
            this.pnlSection.Controls.Add(this.label3);
            this.pnlSection.Controls.Add(this.btnExit);
            this.pnlSection.Controls.Add(this.btnTrans);
            this.pnlSection.Controls.Add(this.label1);
            this.pnlSection.Controls.Add(this.txtSection);
            this.pnlSection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSection.Location = new System.Drawing.Point(0, 0);
            this.pnlSection.Name = "pnlSection";
            this.pnlSection.Size = new System.Drawing.Size(391, 254);
            this.pnlSection.TabIndex = 0;
            // 
            // cmbKotPrinter
            // 
            this.cmbKotPrinter.FormattingEnabled = true;
            this.cmbKotPrinter.Location = new System.Drawing.Point(85, 157);
            this.cmbKotPrinter.Name = "cmbKotPrinter";
            this.cmbKotPrinter.Size = new System.Drawing.Size(250, 21);
            this.cmbKotPrinter.TabIndex = 6;
            this.TTip.SetToolTip(this.cmbKotPrinter, "Please Select Printer");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(82, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Kot Printer Driver :";
            this.TTip.SetToolTip(this.label2, "Select Printer Driver");
            // 
            // cmbPrinter
            // 
            this.cmbPrinter.FormattingEnabled = true;
            this.cmbPrinter.Location = new System.Drawing.Point(85, 98);
            this.cmbPrinter.Name = "cmbPrinter";
            this.cmbPrinter.Size = new System.Drawing.Size(250, 21);
            this.cmbPrinter.TabIndex = 2;
            this.TTip.SetToolTip(this.cmbPrinter, "Please Select Printer");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(82, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Stub Printer Driver :";
            this.TTip.SetToolTip(this.label3, "Select Printer Driver");
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(110, 209);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 3;
            this.btnTrans.Text = "Mode";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(82, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Section :";
            // 
            // txtSection
            // 
            this.txtSection.BackColor = System.Drawing.Color.Ivory;
            this.txtSection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSection.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSection.Location = new System.Drawing.Point(85, 46);
            this.txtSection.Name = "txtSection";
            this.txtSection.Size = new System.Drawing.Size(150, 20);
            this.txtSection.TabIndex = 0;
            this.TTip.SetToolTip(this.txtSection, "Please Enter Section");
            // 
            // frmSection
            // 
            this.ClientSize = new System.Drawing.Size(391, 254);
            this.Controls.Add(this.pnlSection);
            this.Name = "frmSection";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Section";
            this.Load += new System.EventHandler(this.frmSection_Load);
            this.pnlSection.ResumeLayout(false);
            this.pnlSection.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}