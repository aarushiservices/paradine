﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Masters
{
    partial class frmShift
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlShift = new System.Windows.Forms.Panel();
            this.nmStartDay = new System.Windows.Forms.NumericUpDown();
            this.nmEndDay = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpStartTime = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnTrans = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtShift = new System.Windows.Forms.TextBox();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.pnlShift.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmStartDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmEndDay)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlShift
            // 
            this.pnlShift.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlShift.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlShift.Controls.Add(this.nmStartDay);
            this.pnlShift.Controls.Add(this.nmEndDay);
            this.pnlShift.Controls.Add(this.label5);
            this.pnlShift.Controls.Add(this.label4);
            this.pnlShift.Controls.Add(this.dtpEndTime);
            this.pnlShift.Controls.Add(this.label3);
            this.pnlShift.Controls.Add(this.dtpStartTime);
            this.pnlShift.Controls.Add(this.label2);
            this.pnlShift.Controls.Add(this.btnExit);
            this.pnlShift.Controls.Add(this.btnTrans);
            this.pnlShift.Controls.Add(this.label1);
            this.pnlShift.Controls.Add(this.txtShift);
            this.pnlShift.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlShift.Location = new System.Drawing.Point(0, 0);
            this.pnlShift.Name = "pnlShift";
            this.pnlShift.Size = new System.Drawing.Size(334, 192);
            this.pnlShift.TabIndex = 4;
            // 
            // nmStartDay
            // 
            this.nmStartDay.Location = new System.Drawing.Point(253, 60);
            this.nmStartDay.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.nmStartDay.Name = "nmStartDay";
            this.nmStartDay.Size = new System.Drawing.Size(40, 20);
            this.nmStartDay.TabIndex = 19;
            // 
            // nmEndDay
            // 
            this.nmEndDay.Location = new System.Drawing.Point(253, 101);
            this.nmEndDay.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.nmEndDay.Name = "nmEndDay";
            this.nmEndDay.Size = new System.Drawing.Size(40, 20);
            this.nmEndDay.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(215, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Day :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(215, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Day :";
            // 
            // dtpEndTime
            // 
            this.dtpEndTime.CustomFormat = "h:mm:ss tt";
            this.dtpEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndTime.Location = new System.Drawing.Point(110, 101);
            this.dtpEndTime.Name = "dtpEndTime";
            this.dtpEndTime.ShowUpDown = true;
            this.dtpEndTime.Size = new System.Drawing.Size(89, 20);
            this.dtpEndTime.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(46, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "End Time :";
            // 
            // dtpStartTime
            // 
            this.dtpStartTime.CustomFormat = "h:mm:ss tt";
            this.dtpStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartTime.Location = new System.Drawing.Point(110, 60);
            this.dtpStartTime.Name = "dtpStartTime";
            this.dtpStartTime.ShowUpDown = true;
            this.dtpStartTime.Size = new System.Drawing.Size(89, 20);
            this.dtpStartTime.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Start Time :";
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(183, 151);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(90, 151);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 2;
            this.btnTrans.Text = "Mode";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Shift Name :";
            // 
            // txtShift
            // 
            this.txtShift.BackColor = System.Drawing.Color.Ivory;
            this.txtShift.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtShift.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtShift.Location = new System.Drawing.Point(110, 20);
            this.txtShift.Name = "txtShift";
            this.txtShift.Size = new System.Drawing.Size(183, 20);
            this.txtShift.TabIndex = 0;
            // 
            // frmShift
            // 
            this.ClientSize = new System.Drawing.Size(334, 192);
            this.Controls.Add(this.pnlShift);
            this.Name = "frmShift";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Shift";
            this.Load += new System.EventHandler(this.frmShift_Load);
            this.pnlShift.ResumeLayout(false);
            this.pnlShift.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmStartDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmEndDay)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
    }
}