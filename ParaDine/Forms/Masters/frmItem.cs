﻿using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ParaDine.Forms.GlobalForms;

namespace ParaDine.Forms.Masters
{
    public partial class frmItem : Form
    {
        private Button btnExit;
        private Button btnIngrTrans;
        private Button btnSPTrans;
        private Button btnTrans;
        private Button btnUnitTrans;
        private Button button1;
        private CheckBox chkCombo;
        private CheckBox chkDiscountable;
        private CheckBox chkInactive;
        private CheckBox chkNonInventory;
        private CheckBox chkOpenRate;
        private CheckBox chkRawMaterial;
        private CheckBox chkStubReq;
        private CheckBox chkTaxable;
        private ComboBox cmbBasicUOM;
        private ComboBox cmbCategory;
        private ComboBox cmbIngrUOM;
        private ComboBox cmbNewUOM;
        private ComboBox cmbSection;
        private ComboBox cmbSpUOM;
        private ComboBox cmbStockPoint;
        private ComboBox cmbStockUOM;
        private ComboBox cmbTax;
        private ComboBox cmbUOM;
      //  private IContainer components = null;
        private Item EntId = new Item();
        private GroupBox grpConv;
        private GroupBox grpItemSettings;
        private NumControl gvCostPrice;
        private NumControl gvIngrQty;
        private NumControl gvItemDiscPerc;
        private NumControl gvMBQ;
        private NumControl gvNewCostPrice;
        private NumControl gvNewSalePrice;
        private NumControl gvNewUOMConv;
        private NumControl gvSalePrice;
        private ImageList imageList1;
        private Label label1;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label13;
        private Label label14;
        private Label label15;
        private Label label16;
        private Label label17;
        private Label label18;
        private Label label19;
        private Label label2;
        private Label label20;
        private Label label21;
        private Label label22;
        private Label label23;
        private Label label24;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label lblBasicUOM;
        private Label lblNewUOM;
        private Label lblRecepeHead;
        internal ListView lstIngr;
        internal ListView lvwSprice;
        internal ListView lvwUnit;
        private PictureBox picImage;
        private Panel pnlIngrediant;
        private Panel pnlItem;
        private Panel pnlSprice;
        private Panel pnlUnit;
        private TabControl tabControl;
        private TabPage tbPrice;
        private TabPage tbProduct;
        private TabPage tbRecepe;
        private TabPage tbUnit;
        private TreeView trvUOM;
        private ToolTip TTip;
        private TextBox txtDepartment;
        private TextBox txtIngrCode;
        private TextBox txtIngrName;
        private TextBox txtItemCode;
        private TextBox txtItemName;

        public frmItem(string Mode, long ID, SecurityClass paramSecurity)
        {
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes(ID);
            this.EntId.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnIngrTrans_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.trvUOM.SelectedNode == null)
                {
                    throw new Exception("Please Select UOM");
                }
                if (this.btnIngrTrans.Text == "Add")
                {
                    if (ParaDine.GlobalValidations.ValidateFields(this.pnlIngrediant, this.TTip))
                    {
                        ItemIngredient paramIngrediant = new ItemIngredient
                        {
                            Added = true,
                            ItemIngredientID = 0L
                        };
                        this.LoadIngrEntities(paramIngrediant);
                        this.EntId.ItemIngredCollection.Add(paramIngrediant);
                        this.PopulateIngr(Convert.ToInt32(this.trvUOM.SelectedNode.Name));
                        ParaDine.GlobalFunctions.ClearFields(this.pnlIngrediant);
                    }
                }
                else if (this.btnIngrTrans.Text == "Modify")
                {
                    this.LoadIngrEntities(this.EntId.ItemIngredCollection[Convert.ToInt32(this.pnlIngrediant.Tag)]);
                    this.PopulateIngr(Convert.ToInt32(this.trvUOM.SelectedNode.Name));
                    ParaDine.GlobalFunctions.ClearFields(this.pnlIngrediant);
                    this.btnIngrTrans.Text = "Add";
                }
                this.txtIngrCode.Focus();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in btnIngrTrans Click");
            }
        }

        private void btnSPTrans_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.btnSPTrans.Text == "Add")
                {
                    if (ParaDine.GlobalValidations.ValidateFields(this.pnlSprice, this.TTip))
                    {
                        ItemUnitPrice paramUnitPrice = new ItemUnitPrice
                        {
                            Added = true,
                            ItemID = this.EntId.ItemID
                        };
                        this.LoadPriceEntites(paramUnitPrice);
                        this.EntId.ItemPriceCollection.Add(paramUnitPrice);
                        this.PopulatePrice();
                        ParaDine.GlobalFunctions.ClearFields(this.pnlSprice);
                    }
                }
                else if (this.btnSPTrans.Text == "Modify")
                {
                    this.LoadPriceEntites(this.EntId.ItemPriceCollection[Convert.ToInt32(this.pnlSprice.Tag)]);
                    this.PopulatePrice();
                    ParaDine.GlobalFunctions.ClearFields(this.pnlSprice);
                    this.btnSPTrans.Text = "Add";
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In btnSPTrans_Click");
                throw;
            }
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (ParaDine.GlobalValidations.ValidateFields(this.tbProduct, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_007D;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_008D;
                            }
                        }
                        else
                        {
                            this.EntId.Add(sqlTrans, true);
                        }
                    }
                    goto Label_009F;
                Label_007D:
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_009F;
                Label_008D:
                    this.EntId.Delete(sqlTrans, true);
                Label_009F:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, exception.Source);
                    sqlTrans.Rollback();
                }
            }
        }

        private void btnUnitTrans_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.btnUnitTrans.Text == "Add")
                {
                    if (ParaDine.GlobalValidations.ValidateFields(this.pnlUnit, this.TTip))
                    {
                        ItemUnitSpec paramUnitSpec = new ItemUnitSpec
                        {
                            Added = true,
                            ItemID = this.EntId.ItemID
                        };
                        this.LoadSpecEntities(paramUnitSpec);
                        this.EntId.ItemSpecCollection.Add(paramUnitSpec);
                        this.PopulateSpec();
                        ParaDine.GlobalFunctions.ClearFields(this.pnlUnit);
                    }
                }
                else if (this.btnUnitTrans.Text == "Modify")
                {
                    this.LoadSpecEntities(this.EntId.ItemSpecCollection[Convert.ToInt32(this.pnlUnit.Tag)]);
                    this.PopulateSpec();
                    ParaDine.GlobalFunctions.ClearFields(this.pnlUnit);
                    this.btnUnitTrans.Text = "Add";
                }
                this.LoadBasicUOM();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In btnUnitTrans_Click");
                throw;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.lvwUnit.Items.Count <= 0)
            {
                string paramSql = "SELECT ITEMID, ITEMCODE, ITEMNAME FROM RES_ITEM ";
                new frmBrowseData(ParaDine.GlobalFill.FillDataTable(paramSql), this.button1, "ITEMS", 2).ShowDialog();
                Item item = new Item(Convert.ToInt64(this.button1.Tag));
                DataTable collectionTable = item.ItemSpecCollection.GetCollectionTable();
                foreach (ItemUnitSpec spec in item.ItemSpecCollection)
                {
                    ItemUnitSpec varEntityItemUnitSpec = new ItemUnitSpec
                    {
                        Added = true,
                        ItemID = this.EntId.ItemID,
                        BasicUOMID = spec.BasicUOMID,
                        NewUOMID = spec.NewUOMID,
                        CostPrice = spec.CostPrice,
                        ConvFactor = spec.ConvFactor
                    };
                    this.EntId.ItemSpecCollection.Add(varEntityItemUnitSpec);
                }
                this.PopulateSpec();
                this.LoadBasicUOM();
            }
        }

        private void cmbBasicUOM_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.lblBasicUOM.Text = this.cmbBasicUOM.Text;
        }

        private void cmbCategory_SelectedValueChanged(object sender, EventArgs e)
        {
            if ((this.cmbCategory.SelectedIndex != -1) && (this.cmbCategory.ValueMember != ""))
            {
                Department department = new Department(new Category(Convert.ToInt32(this.cmbCategory.SelectedValue)).DepartmentID);
                this.txtDepartment.Text = Convert.ToString(department.DepartmentName);
            }
        }

        private void cmbNewUOM_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.lblNewUOM.Text = this.cmbNewUOM.Text;
        }

        private void cmbUOM_SelectedValueChanged(object sender, EventArgs e)
        {
            this.LoadBasicUOM();
        }

        

        private void frmItem_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                new GlobalTheme().applyTheme(this);
                ParaDine.GlobalFunctions.AddCompHandler(this.tbProduct);
                ParaDine.GlobalFunctions.AddCompHandler(this.tbPrice);
                ParaDine.GlobalFunctions.AddCompHandler(this.tbUnit);
                ParaDine.GlobalFunctions.AddCompHandler(this.tbRecepe);
                this.RefreshData();
                this.LoadFields();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                base.Close();
            }
        }

        private int GetIngrTableId()
        {
            if (this.lstIngr.SelectedItems.Count > 0)
            {
                return (Convert.ToInt32(this.lstIngr.SelectedItems[0].SubItems[this.lstIngr.SelectedItems[0].SubItems.Count - 1].Text) - 1);
            }
            return 0;
        }

        private int GetPriceTableId()
        {
            if (this.lvwSprice.SelectedItems.Count > 0)
            {
                return (Convert.ToInt32(this.lvwSprice.SelectedItems[0].SubItems[this.lvwSprice.SelectedItems[0].SubItems.Count - 1].Text) - 1);
            }
            return 0;
        }

        private int GetSpecTableId()
        {
            if (this.lvwUnit.SelectedItems.Count > 0)
            {
                return (Convert.ToInt32(this.lvwUnit.SelectedItems[0].SubItems[this.lvwUnit.SelectedItems[0].SubItems.Count - 1].Text) - 1);
            }
            return 0;
        }

       

        private void LoadBasicUOM()
        {
            DataTable table = new DataTable();
            table.Columns.Add("BASICUOMID");
            table.Columns.Add("BASICUOMNAME");
            table.Rows.Add(new object[] { this.cmbUOM.SelectedValue, this.cmbUOM.Text });
            foreach (ItemUnitSpec spec in this.EntId.ItemSpecCollection)
            {
                if (!spec.Deleted)
                {
                    table.Rows.Add(new object[] { spec.NewUOMID, new UOM(spec.NewUOMID).UOMName });
                }
            }
            ParaDine.GlobalFill.FillCombo(table.Copy(), this.cmbBasicUOM);
            ParaDine.GlobalFill.FillCombo(table.Copy(), this.cmbSpUOM);
            short num = Convert.ToInt16(this.cmbStockUOM.SelectedValue);
            ParaDine.GlobalFill.FillCombo(table.Copy(), this.cmbStockUOM);
            this.cmbStockUOM.SelectedValue = num;
            this.PopulateUOMTree();
        }

        private void LoadEntities()
        {
            this.EntId.ItemID = Convert.ToInt64(this.txtItemCode.Tag);
            this.EntId.ItemCode = Convert.ToString(this.txtItemCode.Text);
            this.EntId.ItemName = Convert.ToString(this.txtItemName.Text);
            this.EntId.Pic = this.picImage;
            this.EntId.CategoryID = Convert.ToInt32(this.cmbCategory.SelectedValue);
            this.EntId.TaxID = Convert.ToInt32(this.cmbTax.SelectedValue);
            this.EntId.SectionID = Convert.ToInt32(this.cmbSection.SelectedValue);
            this.EntId.SalePrice = Convert.ToDouble(this.gvSalePrice.Value);
            this.EntId.CostPrice = Convert.ToDouble(this.gvCostPrice.Value);
            this.EntId.UOMID = Convert.ToInt32(this.cmbUOM.SelectedValue);
            this.EntId.MBQ = Convert.ToInt32(this.gvMBQ.Value);
            this.EntId.OpenRate = Convert.ToBoolean(this.chkOpenRate.Checked);
            this.EntId.Discountable = Convert.ToBoolean(this.chkDiscountable.Checked);
            this.EntId.ItemDiscPerc = Convert.ToDouble(this.gvItemDiscPerc.Value);
            this.EntId.Taxable = Convert.ToBoolean(this.chkTaxable.Checked);
            this.EntId.NonInventory = Convert.ToBoolean(this.chkNonInventory.Checked);
            this.EntId.StubReq = Convert.ToBoolean(this.chkStubReq.Checked);
            this.EntId.RawMaterial = Convert.ToBoolean(this.chkRawMaterial.Checked);
            this.EntId.StockUOMID = Convert.ToInt32(this.cmbStockUOM.SelectedValue);
            this.EntId.Inactive = Convert.ToBoolean(this.chkInactive.Checked);
            this.EntId.ComboReq = Convert.ToBoolean(this.chkCombo.Checked);
        }

        private void LoadFields()
        {
            this.txtItemCode.Tag = Convert.ToInt64(this.EntId.ItemID);
            if (Convert.ToInt32(this.EntId.ItemID) > 0)
            {
                this.txtItemCode.Text = Convert.ToString(this.EntId.ItemCode);
            }
            else
            {
                this.txtItemCode.Text = this.EntId.MaxCode();
            }
            this.txtItemName.Text = Convert.ToString(this.EntId.ItemName);
            this.picImage.Image = this.EntId.Pic.Image;
            this.cmbSection.SelectedValue = Convert.ToInt32(this.EntId.SectionID);
            this.cmbTax.SelectedValue = Convert.ToInt32(this.EntId.TaxID);
            this.cmbCategory.SelectedValue = Convert.ToInt32(this.EntId.CategoryID);
            this.gvSalePrice.Value = Convert.ToDecimal(this.EntId.SalePrice);
            this.gvCostPrice.Value = Convert.ToDecimal(this.EntId.CostPrice);
            this.cmbUOM.SelectedValue = Convert.ToInt32(this.EntId.UOMID);
            this.cmbStockUOM.SelectedValue = Convert.ToInt32(this.EntId.StockUOMID);
            this.gvMBQ.Value = Convert.ToDecimal(this.EntId.MBQ);
            this.chkOpenRate.Checked = Convert.ToBoolean(this.EntId.OpenRate);
            this.chkDiscountable.Checked = Convert.ToBoolean(this.EntId.Discountable);
            this.gvItemDiscPerc.Value = Convert.ToDecimal(this.EntId.ItemDiscPerc);
            this.chkTaxable.Checked = Convert.ToBoolean(this.EntId.Taxable);
            this.chkNonInventory.Checked = Convert.ToBoolean(this.EntId.NonInventory);
            this.chkStubReq.Checked = Convert.ToBoolean(this.EntId.StubReq);
            this.chkRawMaterial.Checked = Convert.ToBoolean(this.EntId.RawMaterial);
            this.chkInactive.Checked = Convert.ToBoolean(this.EntId.Inactive);
            this.chkCombo.Checked = Convert.ToBoolean(this.EntId.ComboReq);
            this.PopulateSpec();
            this.PopulatePrice();
        }

        private void LoadIngrEntities(ItemIngredient paramIngrediant)
        {
            paramIngrediant.ItemID = Convert.ToInt32(this.EntId.ItemID);
            paramIngrediant.ItemUOMID = Convert.ToInt32(this.trvUOM.SelectedNode.Tag);
            paramIngrediant.RawMaterialItemID = Convert.ToInt32(this.txtIngrCode.Tag);
            paramIngrediant.RawmaterialUOMID = Convert.ToInt32(this.cmbIngrUOM.SelectedValue);
            paramIngrediant.Qty = Convert.ToDouble(this.gvIngrQty.Value);
        }

        private void LoadIngrFields(ItemIngredient paramIngrediant)
        {
            Item item = new Item(paramIngrediant.RawMaterialItemID);
            this.txtIngrCode.Tag = item.ItemID;
            this.txtIngrCode.Text = item.ItemCode;
            this.txtIngrName.Text = item.ItemName;
            this.txtIngrCode_Validating(new object(), new CancelEventArgs());
            this.cmbIngrUOM.SelectedValue = Convert.ToInt32(paramIngrediant.RawmaterialUOMID);
            this.gvIngrQty.Value = Convert.ToDecimal(paramIngrediant.Qty);
        }

        private void LoadPriceEntites(ItemUnitPrice paramUnitPrice)
        {
            paramUnitPrice.ItemID = this.EntId.ItemID;
            paramUnitPrice.StockPointID = Convert.ToInt32(this.cmbStockPoint.SelectedValue);
            paramUnitPrice.SalePrice = Convert.ToDouble(this.gvNewSalePrice.Value);
            paramUnitPrice.UOMID = Convert.ToInt32(this.cmbSpUOM.SelectedValue);
        }
        private void LoadPriceFields(ItemUnitPrice paramUnitPrice)
        {
            this.cmbStockPoint.SelectedValue = Convert.ToInt32(paramUnitPrice.StockPointID);
            this.cmbSpUOM.SelectedValue = Convert.ToInt32(paramUnitPrice.UOMID);
            this.gvNewSalePrice.Value = Convert.ToDecimal(paramUnitPrice.SalePrice);
        }

        private void LoadSpecEntities(ItemUnitSpec paramUnitSpec)
        {
            paramUnitSpec.ItemID = this.EntId.ItemID;
            paramUnitSpec.BasicUOMID = Convert.ToInt32(this.cmbBasicUOM.SelectedValue);
            paramUnitSpec.NewUOMID = Convert.ToInt32(this.cmbNewUOM.SelectedValue);
            paramUnitSpec.CostPrice = Convert.ToDouble(this.gvNewCostPrice.Value);
            paramUnitSpec.ConvFactor = Convert.ToDouble(this.gvNewUOMConv.Value);
        }

        private void LoadSpecFields(ItemUnitSpec paramUnitSpec)
        {
            this.cmbBasicUOM.SelectedValue = Convert.ToInt32(paramUnitSpec.BasicUOMID);
            this.cmbNewUOM.SelectedValue = Convert.ToInt32(paramUnitSpec.NewUOMID);
            this.gvNewCostPrice.Value = Convert.ToDecimal(paramUnitSpec.CostPrice);
            this.gvNewUOMConv.Value = Convert.ToDecimal(paramUnitSpec.ConvFactor);
        }

        private void lstIngr_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (this.lstIngr.SelectedItems.Count > 0)
                {
                    this.pnlIngrediant.Tag = this.GetIngrTableId();
                    this.LoadIngrFields(this.EntId.ItemIngredCollection[Convert.ToInt32(this.pnlIngrediant.Tag)]);
                    this.btnIngrTrans.Text = "Modify";
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In lstIngr_DoubleClick");
                throw;
            }
        }

        private void lstIngr_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    this.EntId.ItemIngredCollection[this.GetIngrTableId()].Deleted = true;
                    this.PopulateIngr(Convert.ToInt32(this.trvUOM.SelectedNode.Name));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In lstIngr_Keydown");
                throw;
            }
        }

        private void lvwSprice_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (this.lvwSprice.SelectedItems.Count > 0)
                {
                    this.pnlSprice.Tag = this.GetPriceTableId();
                    this.LoadPriceFields(this.EntId.ItemPriceCollection[Convert.ToInt32(this.pnlSprice.Tag)]);
                    this.btnSPTrans.Text = "Modify";
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In lvwSPrice_DoubleClick");
                throw;
            }
        }

        private void lvwSprice_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    this.EntId.ItemPriceCollection[this.GetPriceTableId()].Deleted = true;
                    this.PopulatePrice();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In lvwSPrice_Keydown");
                throw;
            }
        }

        private void lvwUnit_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (this.lvwUnit.SelectedItems.Count > 0)
                {
                    this.pnlUnit.Tag = this.GetSpecTableId();
                    this.LoadSpecFields(this.EntId.ItemSpecCollection[Convert.ToInt32(this.pnlUnit.Tag)]);
                    this.btnUnitTrans.Text = "Modify";
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In lvwUnit_DoubleClick");
                throw;
            }
        }

        private void lvwUnit_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    this.EntId.ItemSpecCollection[this.GetSpecTableId()].Deleted = true;
                    this.PopulateSpec();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In lvwUnit_Keydown");
                throw;
            }
        }

        private void picImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (DialogResult.OK == dialog.ShowDialog())
            {
                FileInfo info = new FileInfo(dialog.FileName);
                if (info.Length > 0x3c00L)
                {
                    MessageBox.Show("File Size Should be Less than 15KB", "ParaDine");
                }
                else
                {
                    this.picImage.ImageLocation = dialog.FileName;
                    this.picImage.Refresh();
                }
            }
        }

        private void PopulateIngr(int pUomId)
        {
            ItemIngredientCollection itemIngredCollection = this.EntId.ItemIngredCollection;
            ParaDine.GlobalFill.FillListView(this.lstIngr, itemIngredCollection.GetCollectionTable(Convert.ToInt32(pUomId)));
            this.SetColWidth();
        }

        private void PopulatePrice()
        {
            ParaDine.GlobalFill.FillListView(this.lvwSprice, this.EntId.ItemPriceCollection.GetCollectionTable());
            this.cmbStockPoint.Focus();
        }

        private void PopulateSpec()
        {
            ParaDine.GlobalFill.FillListView(this.lvwUnit, this.EntId.ItemSpecCollection.GetCollectionTable());
            this.cmbBasicUOM.Focus();
        }

        private void PopulateUOMTree()
        {
            try
            {
                this.trvUOM.Nodes.Clear();
                TreeNode node = new TreeNode(this.txtItemName.Text);
                node.Nodes.Add(this.cmbUOM.Text);
                node.Nodes[0].Name = Convert.ToString(this.cmbUOM.SelectedValue);
                node.Nodes[0].Tag = this.cmbUOM.SelectedValue;
                foreach (ItemUnitSpec spec in this.EntId.ItemSpecCollection)
                {
                    if (!spec.Deleted)
                    {
                        TreeNode node2 = new TreeNode
                        {
                            Tag = spec.NewUOMID,
                            Name = Convert.ToString(spec.NewUOMID),
                            Text = new UOM(spec.NewUOMID).UOMName
                        };
                        node.Nodes.Add(node2);
                    }
                }
                this.trvUOM.Nodes.Add(node);
                this.trvUOM.ExpandAll();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in PopulateUOMTree");
            }
        }

        private void RefreshData()
        {
            try
            {
                ParaDine.GlobalFill.FillCombo("SELECT CATEGORYID, CATEGORYNAME FROM RES_CATEGORY", this.cmbCategory);
                ParaDine.GlobalFill.FillCombo("SELECT UOMID, UOMNAME FROM RES_UOM", this.cmbUOM);
                ParaDine.GlobalFill.FillCombo("SELECT TAXID, TAXNAME FROM RES_TAX", this.cmbTax);
                ParaDine.GlobalFill.FillCombo("SELECT SECTIONID, SECTIONNAME FROM RES_SECTION", this.cmbSection);
                ParaDine.GlobalFill.FillCombo(this.EntId.GetUOM(), this.cmbStockUOM);
                ParaDine.GlobalFill.FillCombo("SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT WHERE ISBILLPOINT = 1", this.cmbStockPoint);
                ParaDine.GlobalFill.FillCombo("SELECT UOMID, UOMNAME FROM RES_UOM", this.cmbSpUOM);
                ParaDine.GlobalFill.FillCombo("SELECT UOMID, UOMNAME FROM RES_UOM", this.cmbNewUOM);
                this.btnUnitTrans.Text = "Add";
                this.btnSPTrans.Text = "Add";
                this.btnIngrTrans.Text = "Add";
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In RefreshData");
            }
        }

        private void SetColWidth()
        {
            this.lstIngr.Columns[0].Width = 0;
            this.lstIngr.Columns[1].Width = 0;
            this.lstIngr.Columns[5].Width = 0;
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.LoadBasicUOM();
        }

        private void trvUOM_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                this.lblRecepeHead.Visible = true;
                this.lblRecepeHead.Text = this.EntId.ItemName + " - " + e.Node.Text;
                this.PopulateIngr(Convert.ToInt32(e.Node.Name));
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in trvUOM AfterSelect");
            }
        }

        private void txtIngrCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                string paramSql = "SELECT ITEMID, ITEMCODE, ITEMNAME FROM RES_ITEM WHERE ";
                if (this.chkCombo.Checked)
                {
                    paramSql = paramSql + " RAWMATERIAL = 0 ";
                }
                else
                {
                    paramSql = paramSql + " RAWMATERIAL = 1 ";
                }
                new frmBrowseData(ParaDine.GlobalFill.FillDataTable(paramSql), this.txtIngrCode, "ITEMS", 2).ShowDialog();
            }
        }

        private void txtIngrCode_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.txtIngrCode.Text != "")
                {
                    Item item = new Item(this.txtIngrCode.Text);
                    if (item.ItemID <= 0L)
                    {
                        throw new Exception("Ingrediant Code Not Found");
                    }
                    this.txtIngrCode.Tag = item.ItemID;
                    this.txtIngrName.Text = item.ItemName;
                    ParaDine.GlobalFill.FillCombo(item.GetUOM(), this.cmbIngrUOM);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in txtIngrCode Validating");
                e.Cancel = true;
            }
        }
    }
}
