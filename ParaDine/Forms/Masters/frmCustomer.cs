﻿using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Masters
{
    public partial class frmCustomer : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private CheckBox chkInActive;
        private ComboBox cmbGender;
       // private IContainer components = null;
        private Customer EntId = new Customer();
        private NumControl gvCreditLimit;
        private NumControl gvDiscPerc;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Panel pnlCustomer;
        private ToolTip TTip;
        private TextBox txtAddress;
        private TextBox txtContNo;
        private TextBox txtCustCode;
        private TextBox txtCustName;
        private TextBox txtRemarks;

        public frmCustomer(string Mode, int ID, SecurityClass paramSecurity)
        {
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes((long)ID);
            this.EntId.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (ParaDine.GlobalValidations.ValidateFields(this.pnlCustomer, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_007D;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_008D;
                            }
                        }
                        else
                        {
                            this.EntId.Add(sqlTrans, true);
                        }
                    }
                    goto Label_009F;
                Label_007D:
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_009F;
                Label_008D:
                    this.EntId.Delete(sqlTrans, true);
                Label_009F:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, exception.Source);
                    sqlTrans.Rollback();
                }
            }
        }
        private void frmCustomer_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                this.RefreshData();
                this.LoadFields();
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                base.Close();
            }
        }
        private void LoadEntities()
        {
            this.EntId.Customerid = Convert.ToInt32(this.txtCustCode.Tag);
            this.EntId.CustomerCode = Convert.ToString(this.txtCustCode.Text);
            this.EntId.CustomerName = Convert.ToString(this.txtCustName.Text);
            this.EntId.Address = Convert.ToString(this.txtAddress.Text);
            this.EntId.ContactNo = Convert.ToInt64(this.txtContNo.Text);
            this.EntId.Gender = Convert.ToChar(this.cmbGender.Text.Substring(0, 1));
            this.EntId.DiscPerc = Convert.ToDouble(this.gvDiscPerc.Value);
            this.EntId.CreditLimit = Convert.ToDouble(this.gvCreditLimit.Value);
            this.EntId.Remarks = Convert.ToString(this.txtRemarks.Text);
            this.EntId.InActive = Convert.ToBoolean(this.chkInActive.Checked);
        }
        private void LoadFields()
        {
            this.txtCustCode.Tag = Convert.ToInt32(this.EntId.Customerid);
            if (Convert.ToInt32(this.EntId.Customerid) > 0)
            {
                this.txtCustCode.Text = Convert.ToString(this.EntId.CustomerCode);
            }
            else
            {
                this.txtCustCode.Text = this.EntId.MaxCode();
            }
            this.txtCustName.Text = Convert.ToString(this.EntId.CustomerName);
            this.txtAddress.Text = Convert.ToString(this.EntId.Address);
            this.txtContNo.Text = Convert.ToString(this.EntId.ContactNo);
            if (this.EntId.Gender == Convert.ToChar("M"))
            {
                this.cmbGender.Text = "MALE";
            }
            else
            {
                this.cmbGender.Text = "FEMALE";
            }
            this.gvDiscPerc.Value = Convert.ToDecimal(this.EntId.DiscPerc);
            this.gvCreditLimit.Value = Convert.ToDecimal(this.EntId.CreditLimit);
            this.txtRemarks.Text = Convert.ToString(this.EntId.Remarks);
            this.chkInActive.Checked = Convert.ToBoolean(this.EntId.InActive);
        }

        private void RefreshData()
        {
            try
            {
                this.cmbGender.Items.Clear();
                this.cmbGender.Items.Add("MALE");
                this.cmbGender.Items.Add("FEMALE");
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Refresh Data");
                throw;
            }
        }

        private void txtContNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            ParaDine.GlobalValidations.ValidateKeys(e, false, true, false, "-");
        }

        private void txtContNo_Validating(object sender, CancelEventArgs e)
        {
        }
    }
}
