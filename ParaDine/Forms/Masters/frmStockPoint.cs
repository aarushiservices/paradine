﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Masters
{
    public partial class frmStockPoint : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private CheckBox chkIsBillPoint;
        private CheckBox chkServTaxApplicable;
     //private IContainer components = null;
        private StockPoint EntId = new StockPoint();
        private Label label1;
        private Label label3;
        private Panel pnlStockPoint;
        private ToolTip TTip;
        private TextBox txtRemarks;
        private TextBox txtStockPoint;

        public frmStockPoint(string Mode, int ID, SecurityClass paramSecurity)
        {
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes((long)ID);
            this.EntId.Security = paramSecurity;
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }
        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (GlobalValidations.ValidateFields(this.pnlStockPoint, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_007D;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_008D;
                            }
                        }
                        else
                        {
                            this.EntId.Add(sqlTrans, true);
                        }
                    }
                    goto Label_009F;
                Label_007D:
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_009F;
                Label_008D:
                    this.EntId.Delete(sqlTrans, true);
                Label_009F:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, exception.Source);
                    sqlTrans.Rollback();
                }
            }
        }
        private void frmStockPoint_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                this.LoadFields();
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                base.Close();
            }
        }
        private void LoadEntities()
        {
            this.EntId.StockPointId = Convert.ToInt32(this.txtStockPoint.Tag);
            this.EntId.StockPointName = Convert.ToString(this.txtStockPoint.Text);
            this.EntId.IsBillPoint = Convert.ToBoolean(this.chkIsBillPoint.Checked);
            this.EntId.Remarks = Convert.ToString(this.txtRemarks.Text);
            this.EntId.ServiceTaxApplicable = Convert.ToBoolean(this.chkServTaxApplicable.Checked);
        }
        private void LoadFields()
        {
            this.txtStockPoint.Tag = this.EntId.StockPointId;
            this.txtStockPoint.Text = this.EntId.StockPointName;
            this.chkIsBillPoint.Checked = this.EntId.IsBillPoint;
            this.txtRemarks.Text = this.EntId.Remarks;
            this.chkServTaxApplicable.Checked = this.EntId.ServiceTaxApplicable;
        }

        private void pnlStockPoint_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
