﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Masters
{
    public partial class frmShift : Form
    {
        private Button btnExit;
        private Button btnTrans;
        //private IContainer components = null;
        private DateTimePicker dtpEndTime;
        private DateTimePicker dtpStartTime;
        private Shift EntId = new Shift();
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private NumericUpDown nmEndDay;
        private NumericUpDown nmStartDay;
        private Panel pnlShift;
        private ToolTip TTip;
        private TextBox txtShift;

        public frmShift(string Mode, int ID, SecurityClass paramSecurity)
        {
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes((long)ID);
            this.EntId.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (GlobalValidations.ValidateFields(this.pnlShift, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_007D;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_008D;
                            }
                        }
                        else
                        {
                            this.EntId.Add(sqlTrans, true);
                        }
                    }
                    goto Label_009F;
                Label_007D:
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_009F;
                Label_008D:
                    this.EntId.Delete(sqlTrans, true);
                Label_009F:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, exception.Source);
                    sqlTrans.Rollback();
                }
            }
        }

   

        private void frmShift_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                this.LoadFields();
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                base.Close();
            }
        }

   

        private void LoadEntities()
        {
            this.EntId.ShiftId = Convert.ToInt32(this.txtShift.Tag);
            this.EntId.ShiftName = Convert.ToString(this.txtShift.Text);
            this.EntId.StartingTime = Convert.ToDateTime(this.dtpStartTime.Value);
            this.EntId.EndingTime = Convert.ToDateTime(this.dtpEndTime.Value);
            this.EntId.StartingDay = Convert.ToInt32(this.nmStartDay.Value);
            this.EntId.EndingDay = Convert.ToInt32(this.nmEndDay.Value);
        }

        private void LoadFields()
        {
            this.txtShift.Tag = this.EntId.ShiftId;
            this.txtShift.Text = this.EntId.ShiftName;
            this.dtpStartTime.Value = this.EntId.StartingTime;
            this.nmStartDay.Value = this.EntId.StartingDay;
            this.dtpEndTime.Value = this.EntId.EndingTime;
            this.nmEndDay.Value = this.EntId.EndingDay;
        }
    }
}
