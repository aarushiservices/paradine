﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Masters
{
    public partial class frmSteward : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private CheckBox chkInActive;
        private ComboBox cmbGender;
       // private IContainer components = null;
        private DateTimePicker dtpDOB;
        private DateTimePicker dtpDOJ;
        private Steward EntId = new Steward();
        private GroupBox groupBox2;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private PictureBox PbxImage;
        private Panel pnlSteward;
        private ToolTip TTip;
        private TextBox txtAddress;
        private TextBox txtContactNo;
        private TextBox txtSteward;
        private TextBox txtStewardCode;

        public frmSteward(string Mode, long ID, SecurityClass paramSecurity)
        {
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes(ID);
            this.EntId.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (GlobalValidations.ValidateFields(this.pnlSteward, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_007D;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_008D;
                            }
                        }
                        else
                        {
                            this.EntId.Add(sqlTrans, true);
                        }
                    }
                    goto Label_009F;
                Label_007D:
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_009F;
                Label_008D:
                    this.EntId.Delete(sqlTrans, true);
                Label_009F:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, exception.Source);
                    sqlTrans.Rollback();
                }
            }
        }

        

        private void frmSteward_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                this.RefreshData();
                this.LoadFields();
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                base.Close();
            }
        }

   

        private void LoadEntities()
        {
            this.EntId.StewardID = Convert.ToInt64(this.txtStewardCode.Tag);
            this.EntId.StewardCode = Convert.ToString(this.txtStewardCode.Text);
            this.EntId.StewardName = Convert.ToString(this.txtSteward.Text);
            this.EntId.StewardPic = this.PbxImage;
            this.EntId.DOJ = Convert.ToDateTime(this.dtpDOJ.Text);
            this.EntId.DOB = Convert.ToDateTime(this.dtpDOB.Text);
            this.EntId.Address = Convert.ToString(this.txtAddress.Text);
            this.EntId.ContactNo = Convert.ToString(this.txtContactNo.Text);
            this.EntId.Gender = Convert.ToChar(this.cmbGender.Text.Substring(0, 1));
            this.EntId.InActive = this.chkInActive.Checked;
        }

        private void LoadFields()
        {
            this.txtStewardCode.Tag = Convert.ToInt64(this.EntId.StewardID);
            if (this.EntId.StewardID > 0L)
            {
                this.txtStewardCode.Text = Convert.ToString(this.EntId.StewardCode);
            }
            else
            {
                this.txtStewardCode.Text = this.EntId.MaxCode();
            }
            this.txtSteward.Text = Convert.ToString(this.EntId.StewardName);
            this.PbxImage.Image = this.EntId.StewardPic.Image;
            if (this.EntId.StewardID > 0L)
            {
                this.dtpDOJ.Value = Convert.ToDateTime(this.EntId.DOJ);
                this.dtpDOB.Value = Convert.ToDateTime(this.EntId.DOB);
            }
            this.txtAddress.Text = Convert.ToString(this.EntId.Address);
            this.txtContactNo.Text = Convert.ToString(this.EntId.ContactNo);
            if (this.EntId.Gender == Convert.ToChar("M"))
            {
                this.cmbGender.Text = "Male";
            }
            else
            {
                this.cmbGender.Text = "Female";
            }
            this.chkInActive.Checked = this.EntId.InActive;
        }

        private void PbxImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.ShowDialog();
            this.PbxImage.ImageLocation = dialog.FileName;
            this.PbxImage.Refresh();
        }

        private void RefreshData()
        {
            try
            {
                this.cmbGender.Items.Clear();
                this.cmbGender.Items.Add("MALE");
                this.cmbGender.Items.Add("FEMALE");
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Refresh Data");
                throw;
            }
        }
    }
}
