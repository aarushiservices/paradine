﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Masters
{
    partial class frmPrepaidCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlSteward = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpIssueDate = new System.Windows.Forms.DateTimePicker();
            this.dtpValidUpto = new System.Windows.Forms.DateTimePicker();
            this.chkInActive = new System.Windows.Forms.CheckBox();
            this.txtReason = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnTrans = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCardID = new System.Windows.Forms.TextBox();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.pnlSteward.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSteward
            // 
            this.pnlSteward.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlSteward.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSteward.Controls.Add(this.label4);
            this.pnlSteward.Controls.Add(this.label3);
            this.pnlSteward.Controls.Add(this.dtpIssueDate);
            this.pnlSteward.Controls.Add(this.dtpValidUpto);
            this.pnlSteward.Controls.Add(this.chkInActive);
            this.pnlSteward.Controls.Add(this.txtReason);
            this.pnlSteward.Controls.Add(this.label6);
            this.pnlSteward.Controls.Add(this.btnExit);
            this.pnlSteward.Controls.Add(this.btnTrans);
            this.pnlSteward.Controls.Add(this.label1);
            this.pnlSteward.Controls.Add(this.txtCardID);
            this.pnlSteward.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSteward.Location = new System.Drawing.Point(0, 0);
            this.pnlSteward.Name = "pnlSteward";
            this.pnlSteward.Size = new System.Drawing.Size(335, 334);
            this.pnlSteward.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Valid Upto";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Issue Date";
            // 
            // dtpIssueDate
            // 
            this.dtpIssueDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpIssueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpIssueDate.Location = new System.Drawing.Point(115, 76);
            this.dtpIssueDate.Name = "dtpIssueDate";
            this.dtpIssueDate.Size = new System.Drawing.Size(105, 20);
            this.dtpIssueDate.TabIndex = 0;
            this.dtpIssueDate.Value = new System.DateTime(2008, 5, 24, 0, 0, 0, 0);
            // 
            // dtpValidUpto
            // 
            this.dtpValidUpto.CustomFormat = "dd/MMM/yyyy";
            this.dtpValidUpto.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpValidUpto.Location = new System.Drawing.Point(115, 126);
            this.dtpValidUpto.Name = "dtpValidUpto";
            this.dtpValidUpto.Size = new System.Drawing.Size(105, 20);
            this.dtpValidUpto.TabIndex = 1;
            this.dtpValidUpto.Value = new System.DateTime(2008, 5, 24, 0, 0, 0, 0);
            // 
            // chkInActive
            // 
            this.chkInActive.AutoSize = true;
            this.chkInActive.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkInActive.Location = new System.Drawing.Point(116, 170);
            this.chkInActive.Name = "chkInActive";
            this.chkInActive.Size = new System.Drawing.Size(62, 17);
            this.chkInActive.TabIndex = 6;
            this.chkInActive.Text = "InActive";
            this.chkInActive.UseVisualStyleBackColor = true;
            // 
            // txtReason
            // 
            this.txtReason.BackColor = System.Drawing.Color.White;
            this.txtReason.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtReason.Location = new System.Drawing.Point(115, 193);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(186, 20);
            this.txtReason.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(46, 196);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Reason";
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(180, 270);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 8;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(87, 270);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 7;
            this.btnTrans.Text = "Mode";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Card ID ";
            // 
            // txtCardID
            // 
            this.txtCardID.BackColor = System.Drawing.Color.Ivory;
            this.txtCardID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCardID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCardID.Location = new System.Drawing.Point(115, 26);
            this.txtCardID.Name = "txtCardID";
            this.txtCardID.Size = new System.Drawing.Size(140, 20);
            this.txtCardID.TabIndex = 0;
            this.TTip.SetToolTip(this.txtCardID, "Please Enter Card ID");
            // 
            // frmPrepaidCard
            // 
            this.ClientSize = new System.Drawing.Size(335, 334);
            this.Controls.Add(this.pnlSteward);
            this.Name = "frmPrepaidCard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PrepaidCard";
            this.Load += new System.EventHandler(this.frmPrepaidCard_Load);
            this.pnlSteward.ResumeLayout(false);
            this.pnlSteward.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}