﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Billing
{
    partial class frmorderMerge
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Pnlordermerge = new System.Windows.Forms.Panel();
            this.lblordermerge1 = new System.Windows.Forms.Label();
            this.lvworderno = new System.Windows.Forms.ListView();
            this.lblorderno = new System.Windows.Forms.Label();
            this.lbltableno = new System.Windows.Forms.Label();
            this.cmbTableNo = new System.Windows.Forms.ComboBox();
            this.btnok = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.Pnlordermerge.SuspendLayout();
            this.SuspendLayout();
            // 
            // Pnlordermerge
            // 
            this.Pnlordermerge.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.Pnlordermerge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Pnlordermerge.Controls.Add(this.lblordermerge1);
            this.Pnlordermerge.Controls.Add(this.lvworderno);
            this.Pnlordermerge.Controls.Add(this.lblorderno);
            this.Pnlordermerge.Controls.Add(this.lbltableno);
            this.Pnlordermerge.Controls.Add(this.cmbTableNo);
            this.Pnlordermerge.Location = new System.Drawing.Point(12, 12);
            this.Pnlordermerge.Name = "Pnlordermerge";
            this.Pnlordermerge.Size = new System.Drawing.Size(336, 228);
            this.Pnlordermerge.TabIndex = 3;
            // 
            // lblordermerge1
            // 
            this.lblordermerge1.AutoSize = true;
            this.lblordermerge1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblordermerge1.Location = new System.Drawing.Point(116, 9);
            this.lblordermerge1.Name = "lblordermerge1";
            this.lblordermerge1.Size = new System.Drawing.Size(81, 13);
            this.lblordermerge1.TabIndex = 21;
            this.lblordermerge1.Text = "Order Merge ";
            // 
            // lvworderno
            // 
            this.lvworderno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lvworderno.CheckBoxes = true;
            this.lvworderno.Location = new System.Drawing.Point(12, 47);
            this.lvworderno.Name = "lvworderno";
            this.lvworderno.Size = new System.Drawing.Size(166, 131);
            this.lvworderno.TabIndex = 14;
            this.lvworderno.UseCompatibleStateImageBehavior = false;
            // 
            // lblorderno
            // 
            this.lblorderno.AutoSize = true;
            this.lblorderno.Location = new System.Drawing.Point(21, 32);
            this.lblorderno.Name = "lblorderno";
            this.lblorderno.Size = new System.Drawing.Size(56, 13);
            this.lblorderno.TabIndex = 20;
            this.lblorderno.Text = "Order No :";
            // 
            // lbltableno
            // 
            this.lbltableno.AutoSize = true;
            this.lbltableno.Location = new System.Drawing.Point(215, 38);
            this.lbltableno.Name = "lbltableno";
            this.lbltableno.Size = new System.Drawing.Size(71, 13);
            this.lbltableno.TabIndex = 17;
            this.lbltableno.Text = "Table Name :";
            // 
            // cmbTableNo
            // 
            this.cmbTableNo.FormattingEnabled = true;
            this.cmbTableNo.Location = new System.Drawing.Point(214, 60);
            this.cmbTableNo.Name = "cmbTableNo";
            this.cmbTableNo.Size = new System.Drawing.Size(104, 21);
            this.cmbTableNo.TabIndex = 16;
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(107, 254);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(61, 23);
            this.btnok.TabIndex = 4;
            this.btnok.Text = "&Ok";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click_1);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(194, 254);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(61, 23);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click_1);
            // 
            // frmorderMerge
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(361, 286);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnok);
            this.Controls.Add(this.Pnlordermerge);
            this.Name = "frmorderMerge";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OrderMerge";
            this.Load += new System.EventHandler(this.frmorderMerge_Load);
            this.Pnlordermerge.ResumeLayout(false);
            this.Pnlordermerge.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}