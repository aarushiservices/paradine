﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Billing
{
    public partial class frmorderMerge : Form
    {
        private Button btnExit;
        private Button btnok;
        private ComboBox cmbTableNo;
       // private IContainer components;
        private DataSet DS;
        private CustOrderMaster EntId;
        private CustOrderMasterCollection EntIdCollection;
        private Control GlbCntrl;
        public Label Lblordermerge;
        private Label LblOrderMerge;
        private Label lblordermerge1;
        private Label lblorderno;
        private Label lbltableno;
        private ListView lvworderno;
        private Panel Pnlordermerge;
        private SqlDataAdapter sda;
        private DataTable searchDT;
        private string strCondition;
        private string StrSql;

        public frmorderMerge()
        {
            this.components = null;
            this.EntIdCollection = new CustOrderMasterCollection();
            this.EntId = new CustOrderMaster();
            this.DS = new DataSet();
            this.StrSql = "";
            this.LblOrderMerge = new Label();
            this.searchDT = new DataTable();
            this.sda = new SqlDataAdapter();
            this.strCondition = "";
            this.Lblordermerge = new Label();
            this.InitializeComponent();
        }

        public frmorderMerge(Label LOrderMerge)
        {
            this.components = null;
            this.EntIdCollection = new CustOrderMasterCollection();
            this.EntId = new CustOrderMaster();
            this.DS = new DataSet();
            this.StrSql = "";
            this.LblOrderMerge = new Label();
            this.searchDT = new DataTable();
            this.sda = new SqlDataAdapter();
            this.strCondition = "";
            this.Lblordermerge = new Label();
            this.InitializeComponent();
            this.Lblordermerge = LOrderMerge;
        }

        private void btnExit_Click_1(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnok_Click_1(object sender, EventArgs e)
        {
            if (this.lvworderno.CheckedItems.Count >= 2)
            {
                if (this.cmbTableNo.Text != "")
                {
                    if (this.lvworderno.CheckedItems.Count > 0)
                    {
                        int num;
                        string str = "";
                        for (num = 0; num < this.lvworderno.CheckedItems.Count; num++)
                        {
                            if (str != "")
                            {
                                str = str + ",";
                            }
                            str = str + Convert.ToString(this.lvworderno.CheckedItems[num].Tag);
                        }
                        if (str.Length > 6)
                        {
                            this.Lblordermerge.Text = str;
                        }
                        string[] strArray = this.Lblordermerge.Text.Split(new char[] { ',' });
                        for (num = 0; num < strArray.Length; num++)
                        {
                            strArray[num] = string.Format("{0}", strArray[num]);
                        }
                        for (num = 0; num < strArray.Length; num++)
                        {
                            if (this.cmbTableNo.SelectedValue.ToString().Trim() != strArray[num].Trim())
                            {
                                this.StrSql = string.Concat(new object[] { " UPDATE RES_CUSTORDERMASTER SET COVER  = ( select COVER from res_vw_custordermaster where CUSTORDERMASTERID = ", strArray[num], ") +  (select COVER from res_vw_custordermaster where CUSTORDERMASTERID = ", this.cmbTableNo.SelectedValue, " ) where custordermasterid = ", this.cmbTableNo.SelectedValue, " " });
                                new SqlCommand(this.StrSql, GlobalVariables.SqlConn).ExecuteNonQuery();
                            }
                        }
                        for (num = 0; num < strArray.Length; num++)
                        {
                            new SqlCommand(string.Concat(new object[] { " Update res_custorderkot set custordermasterid = ", this.cmbTableNo.SelectedValue, " Where custordermasterid = ", strArray[num], " " }), GlobalVariables.SqlConn).ExecuteNonQuery();
                            if (this.cmbTableNo.SelectedValue.ToString().Trim() != strArray[num].Trim())
                            {
                                new SqlCommand(" Update RES_CUSTORDERMASTER  set cancelledstatus = 1  Where custordermasterid = " + strArray[num] + " ", GlobalVariables.SqlConn).ExecuteNonQuery();
                            }
                        }
                        MessageBox.Show("Order merged successfully ");
                        base.Close();
                    }
                }
                else
                {
                    MessageBox.Show(" PLEASE SELECT TABLE NO ");
                }
            }
            else
            {
                MessageBox.Show("PLEASE CHECK MORE THAN ONE ");
            }
        }

     

        private void frmorderMerge_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.RefreshData();
        }

    

        private void RefreshData()
        {
            this.StrSql = " SELECT CUSTORDERMASTERID,CUSTORDERNO FROM RES_VW_CUSTORDERMASTER WHERE SETTLED = 'FALSE' AND CUSTORDERDATE >= '" + Convert.ToDateTime(GlobalVariables.BusinessDate.ToString("dd/MMM/yyyy")) + "'";
            GlobalFill.FillDataSet(this.StrSql, "CUSTORDERNO", this.DS, this.sda);
            if (this.DS.Tables["CUSTORDERNO"].Rows.Count > 0)
            {
                foreach (DataRow row in this.DS.Tables["CUSTORDERNO"].Rows)
                {
                    this.lvworderno.Items.Add(Convert.ToString(row["CUSTORDERNO"]));
                }
            }
            GlobalFill.FillListView(this.lvworderno, this.DS.Tables["CUSTORDERNO"]);
            GlobalFill.FillCombo("select custordermasterid,tablename from res_table t  inner join res_vw_custordermaster com on com.sourcetypeid = t.tableid where custorderdate >= '" + Convert.ToDateTime(GlobalVariables.BusinessDate.ToString("dd/MMM/yyyy")) + "' and settled = 0", this.cmbTableNo);
        }
    }
}
