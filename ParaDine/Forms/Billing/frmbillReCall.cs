﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Billing
{
    public partial class frmbillReCall : Form
    {
        private Button btncancel;
        private Button btnok;
       // private IContainer components;
        private DataGridView dgvwrecall;
        private DataTable dtpark;
        private Label LblParking;

        public frmbillReCall()
        {
            this.LblParking = new Label();
            this.dtpark = new DataTable();
            this.components = null;
            this.InitializeComponent();
        }

        public frmbillReCall(Label LParking, DataTable dtp)
        {
            this.LblParking = new Label();
            this.dtpark = new DataTable();
            this.components = null;
            this.InitializeComponent();
            this.LblParking = LParking;
            this.dtpark = dtp;
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            if (this.dgvwrecall.SelectedRows.Count > 0)
            {
                this.LblParking.Text = this.dgvwrecall.SelectedRows[0].Cells[0].Value.ToString();
            }
            else
            {
                this.LblParking.Text = "";
            }
            base.Close();
        }

      

        private void frmbillReCall_Load(object sender, EventArgs e)
        {
            this.dgvwrecall.DataSource = this.dtpark;
            this.SetDataGridView();
        }

     

        private void SetDataGridView()
        {
            this.dgvwrecall.RowHeadersVisible = false;
            this.dgvwrecall.Columns[0].Width = 0xeb;
        }
    }
}
