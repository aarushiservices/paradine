﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Billing
{
    public partial class frmBillParking : Form
    {
        private Button btnexit;
        private Button btnok;
        //private IContainer components;
        public Label LblParking;
        private Label lblparkinno;
        private TextBox txtparkingNo;

        public frmBillParking()
        {
            this.components = null;
            this.LblParking = new Label();
            this.InitializeComponent();
        }

        public frmBillParking(Label LParking)
        {
            this.components = null;
            this.LblParking = new Label();
            this.InitializeComponent();
            this.LblParking = LParking;
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            if (this.txtparkingNo.TextLength != 0)
            {
                if (this.txtparkingNo.Text != "")
                {
                    this.LblParking.Text = this.txtparkingNo.Text;
                    base.Close();
                }
                else
                {
                    MessageBox.Show("Invalid Parking No", "Paradine");
                }
            }
            else
            {
                MessageBox.Show(" Please enter Number");
            }
        }



        private void frmParking_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.txtparkingNo.Text = this.txtparkingNo.Text + e.KeyChar;
        }
    }
}
