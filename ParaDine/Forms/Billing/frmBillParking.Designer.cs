﻿namespace ParaDine.Forms.Billing
{
    partial class frmBillParking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnexit = new System.Windows.Forms.Button();
            this.txtparkingNo = new System.Windows.Forms.TextBox();
            this.lblparkinno = new System.Windows.Forms.Label();
            this.btnok = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnexit
            // 
            this.btnexit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Location = new System.Drawing.Point(281, 51);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(61, 23);
            this.btnexit.TabIndex = 2;
            this.btnexit.Text = "&Exit";
            this.btnexit.UseVisualStyleBackColor = true;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // txtparkingNo
            // 
            this.txtparkingNo.Location = new System.Drawing.Point(12, 99);
            this.txtparkingNo.Name = "txtparkingNo";
            this.txtparkingNo.Size = new System.Drawing.Size(336, 20);
            this.txtparkingNo.TabIndex = 0;
            // 
            // lblparkinno
            // 
            this.lblparkinno.AutoSize = true;
            this.lblparkinno.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblparkinno.Location = new System.Drawing.Point(18, 24);
            this.lblparkinno.Name = "lblparkinno";
            this.lblparkinno.Size = new System.Drawing.Size(104, 13);
            this.lblparkinno.TabIndex = 3;
            this.lblparkinno.Text = "Enter Parking No";
            // 
            // btnok
            // 
            this.btnok.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnok.Location = new System.Drawing.Point(282, 17);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(61, 23);
            this.btnok.TabIndex = 1;
            this.btnok.Text = "&Ok";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // frmBillParking
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(360, 155);
            this.Controls.Add(this.btnexit);
            this.Controls.Add(this.txtparkingNo);
            this.Controls.Add(this.lblparkinno);
            this.Controls.Add(this.btnok);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBillParking";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Park Bill";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}