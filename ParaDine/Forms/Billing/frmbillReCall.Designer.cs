﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Billing
{
    partial class frmbillReCall
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvwrecall = new System.Windows.Forms.DataGridView();
            this.btnok = new System.Windows.Forms.Button();
            this.btncancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwrecall)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvwrecall
            // 
            this.dgvwrecall.AllowUserToAddRows = false;
            this.dgvwrecall.AllowUserToDeleteRows = false;
            this.dgvwrecall.AllowUserToOrderColumns = true;
            this.dgvwrecall.AllowUserToResizeColumns = false;
            this.dgvwrecall.AllowUserToResizeRows = false;
            this.dgvwrecall.BackgroundColor = System.Drawing.SystemColors.ScrollBar;
            this.dgvwrecall.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvwrecall.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvwrecall.Location = new System.Drawing.Point(12, 12);
            this.dgvwrecall.Name = "dgvwrecall";
            this.dgvwrecall.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvwrecall.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvwrecall.Size = new System.Drawing.Size(274, 232);
            this.dgvwrecall.TabIndex = 0;
            // 
            // btnok
            // 
            this.btnok.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnok.Location = new System.Drawing.Point(78, 261);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(57, 23);
            this.btnok.TabIndex = 1;
            this.btnok.Text = "&Ok";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // btncancel
            // 
            this.btncancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncancel.Location = new System.Drawing.Point(164, 261);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(57, 23);
            this.btncancel.TabIndex = 2;
            this.btncancel.Text = "&Cancel";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // frmbillReCall
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(298, 303);
            this.Controls.Add(this.btncancel);
            this.Controls.Add(this.btnok);
            this.Controls.Add(this.dgvwrecall);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmbillReCall";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bill ReCall";
            this.Load += new System.EventHandler(this.frmbillReCall_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvwrecall)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
    }
}