﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.ReportForms
{
    partial class frmRptCustOrderKOT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnExit = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.grpfields = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCustOrderNoTo = new System.Windows.Forms.TextBox();
            this.txtCustOrderNoFrom = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dtpStartDateTo = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpStartDateFrom = new System.Windows.Forms.DateTimePicker();
            this.chkRefund = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbMachineNo = new System.Windows.Forms.ComboBox();
            this.cmbSteward = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbSourceName = new System.Windows.Forms.ComboBox();
            this.chkPendingBills = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbStockPoint = new System.Windows.Forms.ComboBox();
            this.grpfields.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnExit
            // 
            this.BtnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExit.Location = new System.Drawing.Point(186, 276);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(79, 24);
            this.BtnExit.TabIndex = 5;
            this.BtnExit.Text = "E&xit";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(79, 276);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(79, 24);
            this.btnPrint.TabIndex = 4;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // grpfields
            // 
            this.grpfields.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.grpfields.Controls.Add(this.groupBox2);
            this.grpfields.Controls.Add(this.groupBox1);
            this.grpfields.Controls.Add(this.chkRefund);
            this.grpfields.Controls.Add(this.label7);
            this.grpfields.Controls.Add(this.cmbMachineNo);
            this.grpfields.Controls.Add(this.cmbSteward);
            this.grpfields.Controls.Add(this.label4);
            this.grpfields.Controls.Add(this.label8);
            this.grpfields.Controls.Add(this.cmbSourceName);
            this.grpfields.Controls.Add(this.chkPendingBills);
            this.grpfields.Controls.Add(this.label6);
            this.grpfields.Controls.Add(this.cmbStockPoint);
            this.grpfields.Location = new System.Drawing.Point(3, 2);
            this.grpfields.Name = "grpfields";
            this.grpfields.Size = new System.Drawing.Size(318, 268);
            this.grpfields.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.txtCustOrderNoTo);
            this.groupBox2.Controls.Add(this.txtCustOrderNoFrom);
            this.groupBox2.Location = new System.Drawing.Point(8, 11);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(301, 54);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Customer Order No :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(157, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "To :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 21);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "from :";
            // 
            // txtCustOrderNoTo
            // 
            this.txtCustOrderNoTo.Location = new System.Drawing.Point(185, 17);
            this.txtCustOrderNoTo.Name = "txtCustOrderNoTo";
            this.txtCustOrderNoTo.Size = new System.Drawing.Size(104, 20);
            this.txtCustOrderNoTo.TabIndex = 1;
            // 
            // txtCustOrderNoFrom
            // 
            this.txtCustOrderNoFrom.Location = new System.Drawing.Point(51, 17);
            this.txtCustOrderNoFrom.Name = "txtCustOrderNoFrom";
            this.txtCustOrderNoFrom.Size = new System.Drawing.Size(104, 20);
            this.txtCustOrderNoFrom.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.dtpStartDateTo);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dtpStartDateFrom);
            this.groupBox1.Location = new System.Drawing.Point(8, 71);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(301, 54);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Start Date :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(158, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "To :";
            // 
            // dtpStartDateTo
            // 
            this.dtpStartDateTo.Checked = false;
            this.dtpStartDateTo.CustomFormat = "dd/MMM/yyyy";
            this.dtpStartDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDateTo.Location = new System.Drawing.Point(183, 17);
            this.dtpStartDateTo.Name = "dtpStartDateTo";
            this.dtpStartDateTo.ShowCheckBox = true;
            this.dtpStartDateTo.Size = new System.Drawing.Size(104, 20);
            this.dtpStartDateTo.TabIndex = 1;
            this.dtpStartDateTo.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "from :";
            // 
            // dtpStartDateFrom
            // 
            this.dtpStartDateFrom.Checked = false;
            this.dtpStartDateFrom.CustomFormat = "dd/MMM/yyyy";
            this.dtpStartDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDateFrom.Location = new System.Drawing.Point(50, 17);
            this.dtpStartDateFrom.Name = "dtpStartDateFrom";
            this.dtpStartDateFrom.ShowCheckBox = true;
            this.dtpStartDateFrom.Size = new System.Drawing.Size(104, 20);
            this.dtpStartDateFrom.TabIndex = 0;
            this.dtpStartDateFrom.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // chkRefund
            // 
            this.chkRefund.AutoSize = true;
            this.chkRefund.Location = new System.Drawing.Point(165, 230);
            this.chkRefund.Name = "chkRefund";
            this.chkRefund.Size = new System.Drawing.Size(82, 17);
            this.chkRefund.TabIndex = 25;
            this.chkRefund.Text = "Refund Bills";
            this.chkRefund.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 129);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Machine No :";
            // 
            // cmbMachineNo
            // 
            this.cmbMachineNo.FormattingEnabled = true;
            this.cmbMachineNo.Location = new System.Drawing.Point(8, 145);
            this.cmbMachineNo.Name = "cmbMachineNo";
            this.cmbMachineNo.Size = new System.Drawing.Size(140, 21);
            this.cmbMachineNo.TabIndex = 21;
            // 
            // cmbSteward
            // 
            this.cmbSteward.FormattingEnabled = true;
            this.cmbSteward.Location = new System.Drawing.Point(8, 191);
            this.cmbSteward.Name = "cmbSteward";
            this.cmbSteward.Size = new System.Drawing.Size(140, 21);
            this.cmbSteward.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Steward :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(169, 176);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Source Name :";
            // 
            // cmbSourceName
            // 
            this.cmbSourceName.FormattingEnabled = true;
            this.cmbSourceName.Location = new System.Drawing.Point(169, 191);
            this.cmbSourceName.Name = "cmbSourceName";
            this.cmbSourceName.Size = new System.Drawing.Size(140, 21);
            this.cmbSourceName.TabIndex = 5;
            // 
            // chkPendingBills
            // 
            this.chkPendingBills.AutoSize = true;
            this.chkPendingBills.Location = new System.Drawing.Point(62, 230);
            this.chkPendingBills.Name = "chkPendingBills";
            this.chkPendingBills.Size = new System.Drawing.Size(95, 17);
            this.chkPendingBills.TabIndex = 9;
            this.chkPendingBills.Text = "Pending KOTs";
            this.chkPendingBills.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(169, 128);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "StockPoint";
            // 
            // cmbStockPoint
            // 
            this.cmbStockPoint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStockPoint.FormattingEnabled = true;
            this.cmbStockPoint.Location = new System.Drawing.Point(169, 144);
            this.cmbStockPoint.Name = "cmbStockPoint";
            this.cmbStockPoint.Size = new System.Drawing.Size(140, 21);
            this.cmbStockPoint.TabIndex = 4;
            // 
            // frmRptCustOrderKOT
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(322, 307);
            this.Controls.Add(this.BtnExit);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.grpfields);
            this.Name = "frmRptCustOrderKOT";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KOTs Summary";
            this.Load += new System.EventHandler(this.frmRptCustOrderKOT_Load);
            this.grpfields.ResumeLayout(false);
            this.grpfields.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
    }
}