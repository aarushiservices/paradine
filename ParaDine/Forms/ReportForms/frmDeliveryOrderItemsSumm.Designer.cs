﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.ReportForms
{
    partial class frmDeliveryOrderItemsSumm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label15 = new System.Windows.Forms.Label();
            this.cmbDepartment = new System.Windows.Forms.ComboBox();
            this.cmbSendTo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cmbTax = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbSection = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.dtpToDeliveryDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpFrDeliveryDate = new System.Windows.Forms.DateTimePicker();
            this.cmbCustomer = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.DtpToOrderDate = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.dtpFrOrderDate = new System.Windows.Forms.DateTimePicker();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtToCode = new System.Windows.Forms.TextBox();
            this.txtFrCode = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.cmbRptType = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlSalesSummary = new System.Windows.Forms.Panel();
            this.CHKPendingOrd = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.pnlSalesSummary.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(179, 320);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 13);
            this.label15.TabIndex = 46;
            this.label15.Text = "Send To :";
            // 
            // cmbDepartment
            // 
            this.cmbDepartment.FormattingEnabled = true;
            this.cmbDepartment.Location = new System.Drawing.Point(14, 246);
            this.cmbDepartment.Name = "cmbDepartment";
            this.cmbDepartment.Size = new System.Drawing.Size(149, 21);
            this.cmbDepartment.TabIndex = 48;
            // 
            // cmbSendTo
            // 
            this.cmbSendTo.FormattingEnabled = true;
            this.cmbSendTo.Location = new System.Drawing.Point(175, 336);
            this.cmbSendTo.Name = "cmbSendTo";
            this.cmbSendTo.Size = new System.Drawing.Size(159, 21);
            this.cmbSendTo.TabIndex = 45;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(178, 274);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 13);
            this.label14.TabIndex = 44;
            this.label14.Text = "Tax :";
            // 
            // cmbTax
            // 
            this.cmbTax.FormattingEnabled = true;
            this.cmbTax.Location = new System.Drawing.Point(175, 288);
            this.cmbTax.Name = "cmbTax";
            this.cmbTax.Size = new System.Drawing.Size(159, 21);
            this.cmbTax.TabIndex = 43;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 274);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 42;
            this.label2.Text = "Section :";
            // 
            // cmbSection
            // 
            this.cmbSection.FormattingEnabled = true;
            this.cmbSection.Location = new System.Drawing.Point(14, 288);
            this.cmbSection.Name = "cmbSection";
            this.cmbSection.Size = new System.Drawing.Size(149, 21);
            this.cmbSection.TabIndex = 40;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(178, 232);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 41;
            this.label13.Text = "Category :";
            // 
            // dtpToDeliveryDate
            // 
            this.dtpToDeliveryDate.Checked = false;
            this.dtpToDeliveryDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpToDeliveryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDeliveryDate.Location = new System.Drawing.Point(197, 19);
            this.dtpToDeliveryDate.Name = "dtpToDeliveryDate";
            this.dtpToDeliveryDate.ShowCheckBox = true;
            this.dtpToDeliveryDate.Size = new System.Drawing.Size(104, 20);
            this.dtpToDeliveryDate.TabIndex = 1;
            this.dtpToDeliveryDate.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "From :";
            // 
            // dtpFrDeliveryDate
            // 
            this.dtpFrDeliveryDate.Checked = false;
            this.dtpFrDeliveryDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpFrDeliveryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFrDeliveryDate.Location = new System.Drawing.Point(58, 17);
            this.dtpFrDeliveryDate.Name = "dtpFrDeliveryDate";
            this.dtpFrDeliveryDate.ShowCheckBox = true;
            this.dtpFrDeliveryDate.Size = new System.Drawing.Size(104, 20);
            this.dtpFrDeliveryDate.TabIndex = 0;
            this.dtpFrDeliveryDate.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // cmbCustomer
            // 
            this.cmbCustomer.FormattingEnabled = true;
            this.cmbCustomer.Location = new System.Drawing.Point(71, 201);
            this.cmbCustomer.Name = "cmbCustomer";
            this.cmbCustomer.Size = new System.Drawing.Size(263, 21);
            this.cmbCustomer.TabIndex = 29;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 204);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Customer :";
            // 
            // cmbCategory
            // 
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Location = new System.Drawing.Point(175, 246);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(159, 21);
            this.cmbCategory.TabIndex = 39;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(172, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "To :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.dtpToDeliveryDate);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dtpFrDeliveryDate);
            this.groupBox1.Location = new System.Drawing.Point(13, 71);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(323, 56);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "DeliveryDate";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(170, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 13);
            this.label11.TabIndex = 8;
            this.label11.Text = "To :";
            // 
            // DtpToOrderDate
            // 
            this.DtpToOrderDate.Checked = false;
            this.DtpToOrderDate.CustomFormat = "dd/MMM/yyyy";
            this.DtpToOrderDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpToOrderDate.Location = new System.Drawing.Point(195, 17);
            this.DtpToOrderDate.Name = "DtpToOrderDate";
            this.DtpToOrderDate.ShowCheckBox = true;
            this.DtpToOrderDate.Size = new System.Drawing.Size(104, 20);
            this.DtpToOrderDate.TabIndex = 1;
            this.DtpToOrderDate.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(23, 21);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "From :";
            // 
            // dtpFrOrderDate
            // 
            this.dtpFrOrderDate.Checked = false;
            this.dtpFrOrderDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpFrOrderDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFrOrderDate.Location = new System.Drawing.Point(59, 17);
            this.dtpFrOrderDate.Name = "dtpFrOrderDate";
            this.dtpFrOrderDate.ShowCheckBox = true;
            this.dtpFrOrderDate.Size = new System.Drawing.Size(104, 20);
            this.dtpFrOrderDate.TabIndex = 0;
            this.dtpFrOrderDate.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(172, 23);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(26, 13);
            this.label16.TabIndex = 25;
            this.label16.Text = "To :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(31, 23);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(36, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "From :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.DtpToOrderDate);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.dtpFrOrderDate);
            this.groupBox2.Location = new System.Drawing.Point(16, 134);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(323, 49);
            this.groupBox2.TabIndex = 38;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Order Date :";
            // 
            // txtToCode
            // 
            this.txtToCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtToCode.Location = new System.Drawing.Point(205, 19);
            this.txtToCode.Name = "txtToCode";
            this.txtToCode.Size = new System.Drawing.Size(86, 20);
            this.txtToCode.TabIndex = 1;
            // 
            // txtFrCode
            // 
            this.txtFrCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFrCode.Location = new System.Drawing.Point(67, 19);
            this.txtFrCode.Name = "txtFrCode";
            this.txtFrCode.Size = new System.Drawing.Size(86, 20);
            this.txtFrCode.TabIndex = 0;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(17, 322);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(82, 13);
            this.label19.TabIndex = 51;
            this.label19.Text = "Display Criteria :";
            // 
            // cmbRptType
            // 
            this.cmbRptType.FormattingEnabled = true;
            this.cmbRptType.Location = new System.Drawing.Point(14, 336);
            this.cmbRptType.Name = "cmbRptType";
            this.cmbRptType.Size = new System.Drawing.Size(149, 21);
            this.cmbRptType.TabIndex = 50;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(17, 232);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(68, 13);
            this.label18.TabIndex = 49;
            this.label18.Text = "Department :";
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(202, 409);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 14;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pnlSalesSummary
            // 
            this.pnlSalesSummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSalesSummary.Controls.Add(this.CHKPendingOrd);
            this.pnlSalesSummary.Controls.Add(this.groupBox2);
            this.pnlSalesSummary.Controls.Add(this.label19);
            this.pnlSalesSummary.Controls.Add(this.cmbRptType);
            this.pnlSalesSummary.Controls.Add(this.label18);
            this.pnlSalesSummary.Controls.Add(this.groupBox3);
            this.pnlSalesSummary.Controls.Add(this.label15);
            this.pnlSalesSummary.Controls.Add(this.cmbDepartment);
            this.pnlSalesSummary.Controls.Add(this.cmbSendTo);
            this.pnlSalesSummary.Controls.Add(this.label14);
            this.pnlSalesSummary.Controls.Add(this.cmbTax);
            this.pnlSalesSummary.Controls.Add(this.label2);
            this.pnlSalesSummary.Controls.Add(this.cmbSection);
            this.pnlSalesSummary.Controls.Add(this.label13);
            this.pnlSalesSummary.Controls.Add(this.cmbCategory);
            this.pnlSalesSummary.Controls.Add(this.groupBox1);
            this.pnlSalesSummary.Controls.Add(this.cmbCustomer);
            this.pnlSalesSummary.Controls.Add(this.label5);
            this.pnlSalesSummary.Location = new System.Drawing.Point(4, 3);
            this.pnlSalesSummary.Name = "pnlSalesSummary";
            this.pnlSalesSummary.Size = new System.Drawing.Size(354, 397);
            this.pnlSalesSummary.TabIndex = 12;
            // 
            // CHKPendingOrd
            // 
            this.CHKPendingOrd.AutoSize = true;
            this.CHKPendingOrd.Location = new System.Drawing.Point(127, 368);
            this.CHKPendingOrd.Name = "CHKPendingOrd";
            this.CHKPendingOrd.Size = new System.Drawing.Size(99, 17);
            this.CHKPendingOrd.TabIndex = 65;
            this.CHKPendingOrd.Text = "Pending Orders";
            this.CHKPendingOrd.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.txtToCode);
            this.groupBox3.Controls.Add(this.txtFrCode);
            this.groupBox3.Location = new System.Drawing.Point(13, 14);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(323, 50);
            this.groupBox3.TabIndex = 47;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Item Code :";
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(105, 409);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 13;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // frmDeliveryOrderItemsSumm
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(359, 436);
            this.ControlBox = false;
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.pnlSalesSummary);
            this.Controls.Add(this.btnPrint);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDeliveryOrderItemsSumm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Delivery Order Items Summary";
            this.Load += new System.EventHandler(this.frmDeliveryOrderItemsSumm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.pnlSalesSummary.ResumeLayout(false);
            this.pnlSalesSummary.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}