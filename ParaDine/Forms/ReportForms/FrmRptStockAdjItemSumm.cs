﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class FrmRptStockAdjItemSumm : Form
    {
        private Button btnExit;
        private Button btnPrint;
        private ComboBox cmbCategory;
        private ComboBox cmbDepartment;
        private ComboBox cmbOutLetTypa;
        private ComboBox cmbRptType;
        private ComboBox cmbSection;
        private ComboBox cmbSendTo;
        private ComboBox cmbTax;
       // private IContainer components = null;
        private DataSet DS = new DataSet();
        private DateTimePicker dtpFrDate;
        private DateTimePicker dtpToDate;
        private GroupBox groupBox1;
        private GroupBox groupBox3;
        private Label label1;
        private Label label10;
        private Label label13;
        private Label label14;
        private Label label15;
        private Label label16;
        private Label label17;
        private Label label18;
        private Label label19;
        private Label label2;
        private Label label3;
        private Panel panel1;
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private SqlCommand SqlCmd = new SqlCommand();
        private TextBox txtFrCode;
        private TextBox txtToCode;

        public FrmRptStockAdjItemSumm()
        {
            this.InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                string str = "";
                this.SqlCmd = new SqlCommand();
                this.SqlCmd.Connection = GlobalVariables.SqlConn;
                this.SqlCmd.CommandText = "RES_PROC_STOCKADJITEMSSUMM";
                this.SqlCmd.CommandType = CommandType.StoredProcedure;
                if (this.cmbRptType.SelectedIndex == 1)
                {
                    this.SqlCmd.Parameters.AddWithValue("@RPTTYPE", "CATGWISE");
                    str = str + "  'Category Wise Report' ; \n";
                }
                else if (this.cmbRptType.SelectedIndex == 2)
                {
                    this.SqlCmd.Parameters.AddWithValue("@RPTTYPE", "DEPTWISE");
                    str = str + "  'Department Wise Report' ; \n";
                }
                else if (this.cmbRptType.SelectedIndex == 3)
                {
                    this.SqlCmd.Parameters.AddWithValue("@RPTTYPE", "SECWISE");
                    str = str + "  'Section Wise Report' ; \n";
                }
                else if (this.cmbRptType.SelectedIndex == 4)
                {
                    this.SqlCmd.Parameters.AddWithValue("@RPTTYPE", "TAXWISE");
                    str = str + "  'Tax Wise Report' ; \n";
                }
                else
                {
                    this.SqlCmd.Parameters.AddWithValue("@RPTTYPE", "ITEMWISE");
                    str = str + "  'Item Wise Report' ; \n";
                }
                if (this.cmbOutLetTypa.SelectedIndex != -1)
                {
                    this.SqlCmd.Parameters.AddWithValue("@OTYPE", this.cmbOutLetTypa.Text.Substring(0, 1));
                    str = str + "Outlet :" + this.cmbOutLetTypa.Text + "; ";
                }
                if (this.txtFrCode.Text.Trim() != "")
                {
                    this.SqlCmd.Parameters.AddWithValue("@FRITEMCODE", this.txtFrCode.Text);
                    str = str + "ItemCode From: " + this.txtFrCode.Text + "; ";
                }
                if (this.txtToCode.Text.Trim() != "")
                {
                    this.SqlCmd.Parameters.AddWithValue("@TOITEMCODE", this.txtToCode.Text);
                    str = str + "Item Code UpTo : " + this.txtToCode.Text + "; ";
                }
                if (this.dtpFrDate.Checked)
                {
                    this.SqlCmd.Parameters.AddWithValue("@FRRTOKDATE", Convert.ToDateTime(this.dtpFrDate.Text));
                    str = str + " RestToKitchendate From : " + this.dtpFrDate.Text + "; ";
                }
                if (this.dtpToDate.Checked)
                {
                    this.SqlCmd.Parameters.AddWithValue("@TORTOKDATE", Convert.ToDateTime(this.dtpToDate.Text));
                    str = str + " RestToKitchenDate UpTo : " + this.dtpToDate.Text + "; ";
                }
                if (Convert.ToInt16(this.cmbDepartment.SelectedValue) > 0)
                {
                    this.SqlCmd.Parameters.AddWithValue("@DEPARTMENTID", this.cmbDepartment.SelectedValue);
                    str = str + "Department :" + this.cmbDepartment.Text + "; ";
                }
                if (Convert.ToInt16(this.cmbCategory.SelectedValue) > 0)
                {
                    this.SqlCmd.Parameters.AddWithValue("@CATEGORYID", this.cmbCategory.SelectedValue);
                    str = str + "Category : " + this.cmbCategory.Text + "; ";
                }
                if (Convert.ToInt16(this.cmbSection.SelectedValue) > 0)
                {
                    this.SqlCmd.Parameters.AddWithValue("@SECTIONID", this.cmbSection.SelectedValue);
                    str = str + "Section : " + this.cmbSection.Text + "; ";
                }
                if (Convert.ToInt16(this.cmbTax.SelectedValue) > 0)
                {
                    this.SqlCmd.Parameters.AddWithValue("@TAXID", this.cmbTax.SelectedValue);
                    str = str + "Tax : " + this.cmbTax.Text + "; ";
                }
                this.SDA.SelectCommand = this.SqlCmd;
                if (!object.ReferenceEquals(this.DS.Tables["RES_PROC_STOCKADJITEMSSUMM"], null))
                {
                    this.DS.Tables["RES_PROC_STOCKADJITEMSSUMM"].Clear();
                }
                this.SDA.Fill(this.DS, "RES_PROC_STOCKADJITEMSSUMM");
                //ParameterFields pfields = new ParameterFields();
                //ParameterField parameterField = new ParameterField
                //{
                //    Name = "Parameter"
                //};
                //ParameterDiscreteValue value2 = new ParameterDiscreteValue
                //{
                //    Value = str
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                if (this.DS.Tables["RES_PROC_STOCKADJITEMSSUMM"].Rows.Count > 0)
                {
                    if (this.cmbSendTo.Text == "SCREEN")
                    {
                        ///By Bharat we have to change Reports to itextsparp pdf
                        ///
                        //RPTSTOCKADJITEMSSUMM rpt = new RPTSTOCKADJITEMSSUMM();
                        //rpt.SetDataSource(this.DS.Tables["RES_PROC_STOCKADJITEMSSUMM"]);
                        //rpt.Refresh();
                        //new FrmRptViewer(rpt, pfields).ShowDialog();
                    }
                }
                else
                {
                    MessageBox.Show("DATA NOT FOUND!");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Print_Click");
            }
        }

      

        private void FrmRptStockAdjItemSumm_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.RefreshData();
        }

        

        private void RefreshData()
        {
            GlobalFill.FillCombo("SELECT CATEGORYID, CATEGORYNAME FROM RES_CATEGORY ORDER BY 2", this.cmbCategory);
            GlobalFill.FillCombo("SELECT SECTIONID, SECTIONNAME FROM RES_SECTION ORDER BY 2", this.cmbSection);
            GlobalFill.FillCombo("SELECT TAXID, TAXNAME FROM RES_TAX ORDER BY 2", this.cmbTax);
            GlobalFill.FillCombo("SELECT DEPARTMENTID, DEPARTMENTNAME FROM RES_DEPARTMENT ORDER BY 2", this.cmbDepartment);
            this.cmbSendTo.Items.Clear();
            this.cmbSendTo.Items.Add("SCREEN");
            this.cmbSendTo.Items.Add("PRINTER");
            this.cmbSendTo.Items.Add("DISK");
            this.cmbSendTo.SelectedIndex = 0;
            this.cmbRptType.Items.Clear();
            this.cmbRptType.Items.Add("Item Wise");
            this.cmbRptType.Items.Add("Category Wise");
            this.cmbRptType.Items.Add("Department Wise");
            this.cmbRptType.Items.Add("Section Wise");
            this.cmbRptType.Items.Add("Tax Wise");
            this.cmbOutLetTypa.Items.Clear();
            this.cmbOutLetTypa.Items.Add("Warehouse");
            this.cmbOutLetTypa.Items.Add("Store");
            this.cmbOutLetTypa.Items.Add("Restaurant");
            this.cmbOutLetTypa.Items.Add("Kitchen");
        }
    }
}
