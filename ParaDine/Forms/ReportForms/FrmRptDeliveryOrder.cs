﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class FrmRptDeliveryOrder : Form
    {
        private Button btnCancel;
        private Button btnPrint;
        private CheckBox ChkCancelled;
        private CheckBox chkorderdelivered;
        private CheckBox CHKPendingOrd;
        private ComboBox cmbCustomer;
        private ComboBox cmbSendTo;
       // private IContainer components = null;
        private DataSet DS = new DataSet();
        private DateTimePicker dtpDeliveryDate;
        private DateTimePicker dtpOrderDate;
        private Label label1;
        private Label label15;
        private Label label2;
        private Label label3;
        private Panel PnlMain;
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private SqlCommand SqlCmd = new SqlCommand();

        public FrmRptDeliveryOrder()
        {
            this.InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            string str = "";
            string str2 = "";
            this.SqlCmd = new SqlCommand();
            this.SqlCmd.Connection = GlobalVariables.SqlConn;
            this.SqlCmd.CommandType = CommandType.Text;
            str2 = "SELECT * FROM RES_VW_DELIVERYORDER WHERE ORDERID <> 0 ";
            if (this.dtpDeliveryDate.Checked)
            {
                str2 = str2 + " AND CONVERT(DATETIME,CONVERT(VARCHAR,DELIVERYDATE,112)) = '" + this.dtpDeliveryDate.Value.ToString("dd/MMM/yy") + "'";
                str = str + " DeliveryDate From : " + this.dtpDeliveryDate.Text + "; ";
            }
            if (this.dtpOrderDate.Checked)
            {
                str2 = str2 + " AND CONVERT(DATETIME,CONVERT(VARCHAR,ORDERDATE,112)) = '" + this.dtpOrderDate.Value.ToString("dd/MMM/yy") + "'";
                str = str + " OrderDate UpTo : " + this.dtpOrderDate.Text + "; ";
            }
            if (this.ChkCancelled.Checked)
            {
                str2 = str2 + " AND CANCELLED = 1";
                str = str + "Cancelled Orders";
            }
            if (this.chkorderdelivered.Checked)
            {
                str2 = str2 + " AND ORDERDELIVERED = 1";
                str = str + "DELIVERED ORDERS ";
            }
            else
            {
                str2 = str2 + "AND ORDERDELIVERED = 0 ";
            }
            if (this.CHKPendingOrd.Checked)
            {
                str2 = str2 + " AND ORDERID NOT IN (SELECT ORDERID FROM RES_CUSTORDERMASTER WHERE ORDERID <> 0) ";
                str = str + " Pending Orders";
            }
            if (Convert.ToInt64(this.cmbCustomer.SelectedValue) > 0L)
            {
                str2 = str2 + " AND CONTACTNAME LIKE '%" + this.cmbCustomer.Text + "%'";
            }
            this.SqlCmd.CommandText = str2;
            this.SDA.SelectCommand = this.SqlCmd;
            if (this.DS.Tables.Contains("PENDINGORDERS"))
            {
                this.DS.Tables.Remove("PENDINGORDERS");
            }
            this.SDA.Fill(this.DS, "PENDINGORDERS");
            //ParameterFields pfields = new ParameterFields();
            //ParameterField parameterField = new ParameterField
            //{
            //    Name = "Parameter"
            //};
            //ParameterDiscreteValue value2 = new ParameterDiscreteValue
            //{
            //    Value = str
            //};
            //parameterField.CurrentValues.Add((ParameterValue)value2);
            //pfields.Add(parameterField);
            if (this.DS.Tables["PENDINGORDERS"].Rows.Count > 0)
            {
                if (this.cmbSendTo.Text == "SCREEN")
                {
                    ///By Bharat we have to change Reports to itextsparp pdf
                    ///
                    //RptDeliveryOrder rpt = new RptDeliveryOrder();
                    //rpt.SetDataSource(this.DS.Tables["PENDINGORDERS"]);
                    //rpt.Refresh();
                    //new FrmRptViewer(rpt, pfields).ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("DATA NOT FOUND!");
            }
        }

       

        private void FrmRptPendingDeliveryOrder_Load(object sender, EventArgs e)
        {
            this.RefreshData();
        }

       

        private void RefreshData()
        {
            GlobalFill.FillCombo("SELECT MAX(ORDERID) AS ORDERID, CONTACTNAME FROM RES_DELIVERYORDER GROUP BY CONTACTNAME ORDER BY 2", this.cmbCustomer);
            this.cmbSendTo.Items.Clear();
            this.cmbSendTo.Items.Add("SCREEN");
            this.cmbSendTo.Items.Add("PRINTER");
            this.cmbSendTo.Items.Add("DISK");
            this.cmbSendTo.SelectedIndex = 0;
            this.dtpDeliveryDate.Value = GlobalVariables.BusinessDate;
            this.dtpOrderDate.Value = GlobalVariables.BusinessDate;
            this.dtpOrderDate.Checked = false;
            this.dtpDeliveryDate.Checked = false;
        }
    }
}
