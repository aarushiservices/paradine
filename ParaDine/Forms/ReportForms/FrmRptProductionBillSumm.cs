﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using ParaDine.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class FrmRptProductionBillSumm : Form
    {
        private Button btnExit;
        private Button btnPrint;
        private ComboBox cmbKitchen;
        private ComboBox cmbPrintOn;
       // private IContainer components = null;
        private DataSet DS = new DataSet();
        private DateTimePicker DtpFrDate;
        private DateTimePicker DtpToDate;
        private GroupBox groupBox1;
        private Panel grpfields;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label5;
        private Label label7;
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private string strSql = "";
        private TextBox txtProductionID;

        public FrmRptProductionBillSumm()
        {
            this.InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                string str = "";
                this.strSql = "SELECT * FROM RES_VW_PRODUCTIONSUMM WHERE 1=1 ";
                if (this.txtProductionID.Text.Trim() != "")
                {
                    this.strSql = string.Concat(new object[] { this.strSql, " AND PRODUCTIONID = ", Convert.ToInt64(this.txtProductionID.Text.Trim()), "" });
                    str = str + " Prod. ID : " + this.txtProductionID.Text + "   ";
                }
                if (this.DtpFrDate.Checked)
                {
                    this.strSql = this.strSql + " AND PRODUCTIONDATE >= '" + this.DtpFrDate.Text + "'";
                    str = str + " Production Date From : " + this.DtpFrDate.Text + "   ";
                }
                if (this.DtpToDate.Checked)
                {
                    this.strSql = this.strSql + " AND PRODUCTIONDATE <= '" + this.DtpToDate.Text + "'";
                    str = str + " Production UpTo : " + this.DtpToDate.Text + "   ";
                }
                if (this.cmbKitchen.Text.Trim() != "")
                {
                    this.strSql = string.Concat(new object[] { this.strSql, " AND KITCHENID = '", this.cmbKitchen.SelectedValue, "'" });
                    str = str + " Kitchen : " + this.cmbKitchen.Text + "  ";
                }
                GlobalFill.FillDataSet(this.strSql, "RES_VW_PRODUCTIONSUMM", this.DS, this.SDA);
                //ParameterFields pfields = new ParameterFields();
                //ParameterField parameterField = new ParameterField
                //{
                //    Name = "Parameter"
                //};
                //ParameterDiscreteValue value2 = new ParameterDiscreteValue
                //{
                //    Value = str
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                if (this.DS.Tables["RES_VW_PRODUCTIONSUMM"].Rows.Count > 0)
                {
                    if (this.cmbPrintOn.Text.Trim() == "SCREEN")
                    {
                        ///By Bharat we have to change Reports to itextsparp pdf
                        ///
                        RPTPRODUCTIONBILLSUMM rpt = new RPTPRODUCTIONBILLSUMM(this.DS.Tables["RES_VW_PRODUCTIONSUMM"]);
                        rpt.Parameter_Parameter = str;
                        rpt.ShowDialog();
                        //rpt.SetDataSource(this.DS.Tables["RES_VW_PRODUCTIONSUMM"]);
                        //rpt.Refresh();
                        //new FrmRptViewer(rpt, pfields).ShowDialog();
                    }
                }
                else
                {
                    MessageBox.Show("Data Not Found!", "Bills Summ", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Print_Click", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

    
        private void FrmRptProductionBillSumm_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.RefreshData();
        }

     

        private void RefreshData()
        {
            GlobalFill.FillCombo("SELECT KITCHENID, KITCHENNAME FROM RES_KITCHEN ORDER BY 2", this.cmbKitchen);
            this.DtpFrDate.Value = GlobalVariables.BusinessDate;
            this.DtpToDate.Value = GlobalVariables.BusinessDate;
            this.cmbPrintOn.Items.Clear();
            this.cmbPrintOn.Items.Add("SCREEN");
            this.cmbPrintOn.Items.Add("PRINTER");
            this.cmbPrintOn.Items.Add("DISK");
            this.cmbPrintOn.SelectedIndex = 0;
        }
    }
}
