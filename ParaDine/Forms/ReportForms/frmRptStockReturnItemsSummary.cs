﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using ParaDine.Reports;
using ParaDinePrint;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmRptStockReturnItemsSummary : Form
    {
        private Button btnExit;
        private Button btnPrint;
        private CheckBox chkCPNotReq;
        private CheckBox chkUOMNotReq;
       // private IContainer components = null;
        private DataSet DS = new DataSet();
        private DateTimePicker dtpreturnDateTo;
        private DateTimePicker dtpRuurnDateFrom;
        private GroupBox gpcategory;
        private GroupBox gpdepartment;
        private GroupBox gpItem;
        private GroupBox gpsection;
        private GroupBox gpstockpoint;
        private GroupBox gpsuppilers;
        private GroupBox groupBox1;
        private GroupBox grpDisplay;
        private GroupBox grpGroup;
        private Label label10;
        private Label label3;
        private Label lblstockentryno;
        private Label lblto;
        internal ListView lstcategory;
        internal ListView lstdepartment;
        internal ListView lstitems;
        internal ListView lstsection;
        internal ListView lststockpoint;
        internal ListView lstsuppliers;
        private PrintingFunctions pfunctions = new PrintingFunctions();
        private Panel pnlSalesSummary;
        private ClsColDosPrint[] prnItem;
        private ClsRptDosPrint prnrpt = new ClsRptDosPrint();
        private RadioButton rdAllCategory;
        private RadioButton rdalldepatment;
        private RadioButton rdAllItems;
        private RadioButton rdAllSection;
        private RadioButton rdallstockpoint;
        private RadioButton rdallsuppliers;
        private RadioButton rdDispCategory;
        private RadioButton rdDispCostprice;
        private RadioButton rdDispDepartment;
        private RadioButton rdDispEntryDate;
        private RadioButton rdDispEntryNo;
        private RadioButton rdDispItem;
        private RadioButton rdDispMonth;
        private RadioButton rdDispSection;
        private RadioButton rdDispStockpoint;
        private RadioButton rdDispYear;
        private RadioButton rdGrpCategory;
        private RadioButton rdGrpCostPrice;
        private RadioButton rdGrpDepartment;
        private RadioButton rdGrpEntryDate;
        private RadioButton rdGrpItem;
        private RadioButton rdGrpMonth;
        private RadioButton rdGrpreturnNo;
        private RadioButton rdGrpSection;
        private RadioButton rdGrpStockpoint;
        private RadioButton rdGrpYear;
        private RadioButton rdselectedcategory;
        private RadioButton rdselectedDepartment;
        private RadioButton rdselectedItems;
        private RadioButton rdselectedsection;
        private RadioButton rdSelectedStockpoint;
        private RadioButton rdselectedsupplier;
        private string rptname = "";
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private SqlCommand SqlCmd = new SqlCommand();
        private string StrSql = "";
        private TextBox txtstockfromNo;
        private TextBox txtstockreturntoNO;

        public frmRptStockReturnItemsSummary()
        {
            this.InitializeComponent();
        }

        private void AddHandlers()
        {
            this.lstdepartment.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstcategory.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstitems.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstsection.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lststockpoint.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstsuppliers.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.rdalldepatment.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllCategory.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllItems.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllSection.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdallstockpoint.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdallsuppliers.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                object obj2;
                this.DS.Tables.Clear();
                string str = "";
                this.StrSql = "SELECT  ";
                this.StrSql = this.StrSql + this.GetField(this.grpGroup) + " categoryname,";
                this.StrSql = this.StrSql + this.GetField(this.grpDisplay) + " ITEMNAME,";
                if (!this.chkUOMNotReq.Checked)
                {
                    this.StrSql = this.StrSql + "U.UOMNAME, ";
                }
                if (!this.chkCPNotReq.Checked)
                {
                    this.StrSql = this.StrSql + "SRT.COSTPRICE, ";
                }
                this.StrSql = this.StrSql + " SUM(SRT.RETURNQTY) AS RETURNQTY,  SUM (SRT.COSTPRICE * SRT.RETURNQTY) AS AMOUNT ";
                this.StrSql = this.StrSql + " FROM RES_STOCKRETURNTRANS SRT  INNER JOIN RES_STOCKRETURNMASTER SRM ON SRM.STOCKRETURNMASTERID = SRT.STOCKRETURNMASTERID  INNER JOIN RES_ITEM I ON I.ITEMID = SRT.ITEMID  INNER JOIN RES_UOM U ON U.UOMID = SRT.UOMID  INNER JOIN RES_SECTION SC ON SC.SECTIONID = I.SECTIONID  INNER JOIN RES_CATEGORY CG ON CG.CATEGORYID = I.CATEGORYID  INNER JOIN RES_DEPARTMENT DP ON DP.DEPARTMENTID = CG.DEPARTMENTID  INNER JOIN RES_STOCKPOINT SP ON SP.STOCKPOINTID = SRM.STOCKPOINTID INNER JOIN RES_SUPPLIER S ON S.SUPPLIERID = SRM.SUPPLIERID ";
                this.StrSql = this.StrSql + "WHERE I.ITEMID <> 0 ";
                if (this.dtpRuurnDateFrom.Checked)
                {
                    obj2 = this.StrSql;
                    this.StrSql = string.Concat(new object[] { obj2, "AND SRM.STOCKRETURNDATE >= '", this.dtpRuurnDateFrom.Value, "'" });
                    str = str + "DATE :" + this.dtpRuurnDateFrom.Text;
                }
                if (this.dtpreturnDateTo.Checked)
                {
                    obj2 = this.StrSql;
                    this.StrSql = string.Concat(new object[] { obj2, "AND SRM .STOCKRETURNDATE <= '", this.dtpreturnDateTo.Value, " '" });
                    str = str + "TO :" + this.dtpreturnDateTo.Text;
                }
                if (this.rdselectedDepartment.Checked && (this.GetCollString(this.lstdepartment) != ""))
                {
                    this.StrSql = this.StrSql + " AND DP.DEPARTMENTID IN (" + this.GetCollString(this.lstdepartment) + ")";
                    str = str + " Depts: " + this.GetRetParam(this.lstdepartment);
                }
                if (this.rdselectedcategory.Checked && (this.GetCollString(this.lstcategory) != ""))
                {
                    this.StrSql = this.StrSql + " AND CG.CATEGORYID IN (" + this.GetCollString(this.lstcategory) + ")";
                    str = str + " Catg: " + this.GetRetParam(this.lstcategory);
                }
                if (this.rdSelectedStockpoint.Checked && (this.GetCollString(this.lststockpoint) != ""))
                {
                    this.StrSql = this.StrSql + " AND SRM.STOCKPOINTID IN (" + this.GetCollString(this.lststockpoint) + ")";
                    str = str + " StockPoints: " + this.GetRetParam(this.lststockpoint);
                }
                if (this.rdselectedsection.Checked && (this.GetCollString(this.lstsection) != ""))
                {
                    this.StrSql = this.StrSql + " AND SC.SECTIONID IN (" + this.GetCollString(this.lstsection) + ")";
                    str = str + " Sections: " + this.GetRetParam(this.lstsection);
                }
                if (this.rdselectedItems.Checked && (this.GetCollString(this.lstitems) != ""))
                {
                    this.StrSql = this.StrSql + " AND I.ITEMID IN (" + this.GetCollString(this.lstitems) + ")";
                    str = str + " Items: " + this.GetRetParam(this.lstitems);
                }
                if (this.rdselectedsupplier.Checked && (this.GetCollString(this.lstsuppliers) != ""))
                {
                    this.StrSql = this.StrSql + " AND SRM.SUPPLIERID IN (" + this.GetCollString(this.lstsuppliers) + ")";
                    str = str + " Suppliers: " + this.GetRetParam(this.lstsuppliers);
                }
                if (this.txtstockfromNo.Text != "")
                {
                    this.StrSql = this.StrSql + " AND SRM.STOCKRETURNNO >= '" + this.txtstockfromNo.Text + "'";
                    str = str + " StockRETURNNo: " + this.txtstockfromNo.Text;
                }
                if (this.txtstockreturntoNO.Text != "")
                {
                    this.StrSql = this.StrSql + " AND SRM.STOCKRETURNNO <= '" + this.txtstockreturntoNO.Text + "'";
                    str = str + " To: " + this.txtstockreturntoNO.Text;
                }
                string strSql = this.StrSql;
                this.StrSql = strSql + " GROUP BY " + this.GetField(this.grpGroup) + "," + this.GetField(this.grpDisplay);
                if (!this.chkUOMNotReq.Checked)
                {
                    this.StrSql = this.StrSql + " ,U.UOMNAME ";
                }
                if (!this.chkCPNotReq.Checked)
                {
                    this.StrSql = this.StrSql + ", SRT.COSTPRICE ";
                }
                GlobalFill.FillDataSet(this.StrSql, "SUMMARY", this.DS, this.SDA);
                //ParameterFields pfields = new ParameterFields();
                //ParameterField parameterField = new ParameterField
                //{
                //    Name = "Parameter"
                //};
                //ParameterDiscreteValue value2 = new ParameterDiscreteValue
                //{
                //    Value = str
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                if (this.DS.Tables["SUMMARY"].Rows.Count > 0)
                {
                    ///By Bharat we have to change Reports to itextsparp pdf
                    ///
                    RptStockreturnItemsummary1 rpt = new RptStockreturnItemsummary1(this.DS.Tables["SUMMARY"]);
                    rpt.Parameter_parameter = str;
                    rpt.ShowDialog();
                    
                    //rpt.SetDataSource(this.DS.Tables["SUMMARY"]);
                    //rpt.Refresh();
                    //new FrmRptViewer(rpt, pfields).ShowDialog();
                }
                else
                {
                    MessageBox.Show("DATA NOT FOUND");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Print_Click");
            }
        }

     

        private void frmRptStockReturnItemsSummary_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.RefreshData();
            this.AddHandlers();
        }

        private string GetCollString(ListView ParamLvw)
        {
            try
            {
                string str = "";
                for (int i = 0; i < ParamLvw.CheckedItems.Count; i++)
                {
                    if (str != "")
                    {
                        str = str + ", ";
                    }
                    str = str + Convert.ToString(ParamLvw.CheckedItems[i].Tag);
                }
                return str;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in function Collection String");
                return "";
            }
        }

        private string GetField(GroupBox paramGrp)
        {
            try
            {
                foreach (RadioButton button in paramGrp.Controls)
                {
                    if (button.Checked)
                    {
                        if (button.Text.ToUpper() == "DEPARTMENT")
                        {
                            return "DP.DEPARTMENTNAME";
                        }
                        if (button.Text.ToUpper() == "CATEGORY")
                        {
                            return "CG.CATEGORYNAME";
                        }
                        if (button.Text.ToUpper() == "STOCKPOINT")
                        {
                            return "SP.STOCKPOINTNAME";
                        }
                        if (button.Text.ToUpper() == "SECTION")
                        {
                            return "SC.SECTIONNAME";
                        }
                        if (button.Text.ToUpper() == "ITEM")
                        {
                            return "I.ITEMNAME";
                        }
                        if (button.Text.ToUpper() == "COSTPRICE")
                        {
                            return "SRT.COSTPRICE";
                        }
                        if (button.Text.ToUpper() == "RETURNNO")
                        {
                            return "SRM.STOCKRETURNNO";
                        }
                        if (button.Text.ToUpper() == "RETURNDATE")
                        {
                            return "CONVERT(VARCHAR, SRM.STOCKRETURNDATE, 102)";
                        }
                        if (button.Text.ToUpper() == "MONTH")
                        {
                            return "'MONTH-' + CONVERT(VARCHAR, MONTH(SRM.STOCKRETURNDATE)) + '/' + CONVERT(VARCHAR, YEAR(SRM.STOCKRETURNDATE))";
                        }
                        if (button.Text.ToUpper() == "YEAR")
                        {
                            return "'YEAR-' + CONVERT(VARCHAR, YEAR(SRM.STOCKRETURNDATE))";
                        }
                        if (button.Text.ToUpper() == "SUPPLIER")
                        {
                            return "S.SUPPLIERNAME";
                        }
                        if (button.Text.ToUpper() == "LRNO")
                        {
                            return "SRM.LRNO";
                        }
                    }
                }
                return "";
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in GetFied");
                return "";
            }
        }

        private string GetParamField(GroupBox paramGrp)
        {
            try
            {
                foreach (RadioButton button in paramGrp.Controls)
                {
                    if (button.Checked)
                    {
                        if (button.Text.ToUpper() == "DEPARTMENT")
                        {
                            return "DEPARTMENT";
                        }
                        if (button.Text.ToUpper() == "CATEGORY")
                        {
                            return "CATEGORY";
                        }
                        if (button.Text.ToUpper() == "STOCKPOINT")
                        {
                            return "STOCKPOINT";
                        }
                        if (button.Text.ToUpper() == "SECTION")
                        {
                            return "SECTION";
                        }
                        if (button.Text.ToUpper() == "ITEM")
                        {
                            return "ITEM";
                        }
                        if (button.Text.ToUpper() == "COSTPRICE")
                        {
                            return "COSTPRICE";
                        }
                        if (button.Text.ToUpper() == "RETURNNO")
                        {
                            return "RETURNNO";
                        }
                        if (button.Text.ToUpper() == "RETURNDATEDATE")
                        {
                            return "RETURNDATE";
                        }
                        if (button.Text.ToUpper() == "MONTH")
                        {
                            return "MONTH";
                        }
                        if (button.Text.ToUpper() == "YEAR")
                        {
                            return "YEAR";
                        }
                        if (button.Text.ToUpper() == "SUPPLIER")
                        {
                            return "SUPPLIER";
                        }
                        if (button.Text.ToUpper() == "LRNO")
                        {
                            return "SRM.LRNO";
                        }
                        if (button.Text.ToUpper() == "STOCKRETURNDATE")
                        {
                            return "CONVERT(VARCHAR, SRM.STOCKRETURNDATE, 102)";
                        }
                    }
                }
                return "";
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in GetFied");
                return "";
            }
        }

        private string GetRetParam(ListView ParamLstVw)
        {
            try
            {
                string str = "";
                for (int i = 0; i < ParamLstVw.CheckedItems.Count; i++)
                {
                    if (str != "")
                    {
                        str = str + ",";
                    }
                    str = str + Convert.ToString(ParamLstVw.CheckedItems[i].Text);
                }
                return str;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In Function Collection String");
                return "";
            }
        }

      

        public void lst_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            try
            {
                if (e.Item.Checked)
                {
                    ListView view = (ListView)sender;
                    if (view.CheckedItems.Count > 0)
                    {
                        foreach (Control control in view.Parent.Controls)
                        {
                            if ((control.GetType() == typeof(RadioButton)) && (control.GetType() == typeof(RadioButton)))
                            {
                                RadioButton button = (RadioButton)control;
                                if (button.Text.ToUpper().Contains("SELECTED"))
                                {
                                    button.Checked = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "ERROR IN  LIST VIEW CHECKED");
            }
        }

        public void rdAll_checkchanged(object Sender, EventArgs e)
        {
            try
            {
                RadioButton button = (RadioButton)Sender;
                if (button.Checked)
                {
                    foreach (Control control in button.Parent.Controls)
                    {
                        if (control.GetType() == typeof(ListView))
                        {
                            ListView view = (ListView)control;
                            foreach (ListViewItem item in view.CheckedItems)
                            {
                                item.Checked = false;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Eror in AllRd");
            }
        }

        private void RefreshData()
        {
            this.StrSql = " SELECT DEPARTMENTID, DEPARTMENTNAME FROM RES_DEPARTMENT";
            GlobalFill.FillListView(this.lstdepartment, GlobalFill.FillDataTable(this.StrSql));
            this.StrSql = "SELECT CATEGORYID,CATEGORYNAME FROM RES_CATEGORY";
            GlobalFill.FillListView(this.lstcategory, GlobalFill.FillDataTable(this.StrSql));
            this.StrSql = "SELECT SECTIONID, SECTIONNAME FROM RES_SECTION";
            GlobalFill.FillListView(this.lstsection, GlobalFill.FillDataTable(this.StrSql));
            this.StrSql = "SELECT ITEMID, ITEMNAME FROM RES_ITEM";
            GlobalFill.FillListView(this.lstitems, GlobalFill.FillDataTable(this.StrSql));
            this.StrSql = "SELECT SUPPLIERID, SUPPLIERNAME FROM RES_SUPPLIER";
            GlobalFill.FillListView(this.lstsuppliers, GlobalFill.FillDataTable(this.StrSql));
            this.StrSql = "SELECT STOCKPOINTID,STOCKPOINTNAME FROM RES_STOCKPOINT";
            GlobalFill.FillListView(this.lststockpoint, GlobalFill.FillDataTable(this.StrSql));
            this.dtpRuurnDateFrom.Value = GlobalVariables.BusinessDate;
            this.dtpreturnDateTo.Value = GlobalVariables.BusinessDate;
        }
    }
}
