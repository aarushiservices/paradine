﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.ReportForms
{
    partial class frmRptRestaurantStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label17 = new System.Windows.Forms.Label();
            this.txtItemCode = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.cmbRptType = new System.Windows.Forms.ComboBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlSalesSummary = new System.Windows.Forms.Panel();
            this.ChkShowAllItemStock = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbRestaurant = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbDepartment = new System.Windows.Forms.ComboBox();
            this.cmbSendTo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cmbTax = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbSection = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pnlSalesSummary.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(104, 9);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(61, 13);
            this.label17.TabIndex = 53;
            this.label17.Text = "Item Code :";
            // 
            // txtItemCode
            // 
            this.txtItemCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemCode.Location = new System.Drawing.Point(173, 5);
            this.txtItemCode.Name = "txtItemCode";
            this.txtItemCode.Size = new System.Drawing.Size(69, 20);
            this.txtItemCode.TabIndex = 0;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(21, 216);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(82, 13);
            this.label19.TabIndex = 51;
            this.label19.Text = "Display Criteria :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(179, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "To :";
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Checked = false;
            this.dtpEndDate.CustomFormat = "dd/MMM/yy";
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDate.Location = new System.Drawing.Point(206, 23);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(87, 20);
            this.dtpEndDate.TabIndex = 1;
            this.dtpEndDate.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "From :";
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Checked = false;
            this.dtpStartDate.CustomFormat = "dd/MMM/yy";
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Location = new System.Drawing.Point(65, 23);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(87, 20);
            this.dtpStartDate.TabIndex = 0;
            this.dtpStartDate.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // cmbRptType
            // 
            this.cmbRptType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRptType.FormattingEnabled = true;
            this.cmbRptType.Location = new System.Drawing.Point(18, 230);
            this.cmbRptType.Name = "cmbRptType";
            this.cmbRptType.Size = new System.Drawing.Size(149, 21);
            this.cmbRptType.TabIndex = 7;
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(93, 300);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(76, 27);
            this.btnPrint.TabIndex = 1;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(190, 300);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 27);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pnlSalesSummary
            // 
            this.pnlSalesSummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSalesSummary.Controls.Add(this.ChkShowAllItemStock);
            this.pnlSalesSummary.Controls.Add(this.label1);
            this.pnlSalesSummary.Controls.Add(this.cmbRestaurant);
            this.pnlSalesSummary.Controls.Add(this.label17);
            this.pnlSalesSummary.Controls.Add(this.txtItemCode);
            this.pnlSalesSummary.Controls.Add(this.label19);
            this.pnlSalesSummary.Controls.Add(this.cmbRptType);
            this.pnlSalesSummary.Controls.Add(this.label18);
            this.pnlSalesSummary.Controls.Add(this.label15);
            this.pnlSalesSummary.Controls.Add(this.cmbDepartment);
            this.pnlSalesSummary.Controls.Add(this.cmbSendTo);
            this.pnlSalesSummary.Controls.Add(this.label14);
            this.pnlSalesSummary.Controls.Add(this.cmbTax);
            this.pnlSalesSummary.Controls.Add(this.label2);
            this.pnlSalesSummary.Controls.Add(this.cmbSection);
            this.pnlSalesSummary.Controls.Add(this.label13);
            this.pnlSalesSummary.Controls.Add(this.cmbCategory);
            this.pnlSalesSummary.Controls.Add(this.groupBox1);
            this.pnlSalesSummary.Location = new System.Drawing.Point(4, 4);
            this.pnlSalesSummary.Name = "pnlSalesSummary";
            this.pnlSalesSummary.Size = new System.Drawing.Size(354, 290);
            this.pnlSalesSummary.TabIndex = 0;
            // 
            // ChkShowAllItemStock
            // 
            this.ChkShowAllItemStock.AutoSize = true;
            this.ChkShowAllItemStock.Checked = true;
            this.ChkShowAllItemStock.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChkShowAllItemStock.Location = new System.Drawing.Point(112, 260);
            this.ChkShowAllItemStock.Name = "ChkShowAllItemStock";
            this.ChkShowAllItemStock.Size = new System.Drawing.Size(128, 17);
            this.ChkShowAllItemStock.TabIndex = 58;
            this.ChkShowAllItemStock.Text = "Show All Item\'s Stock";
            this.ChkShowAllItemStock.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 57;
            this.label1.Text = "Restaurant :";
            // 
            // cmbRestaurant
            // 
            this.cmbRestaurant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRestaurant.FormattingEnabled = true;
            this.cmbRestaurant.Location = new System.Drawing.Point(105, 34);
            this.cmbRestaurant.Name = "cmbRestaurant";
            this.cmbRestaurant.Size = new System.Drawing.Size(207, 21);
            this.cmbRestaurant.TabIndex = 1;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(21, 126);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(68, 13);
            this.label18.TabIndex = 49;
            this.label18.Text = "Department :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(182, 214);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 13);
            this.label15.TabIndex = 46;
            this.label15.Text = "Send To :";
            // 
            // cmbDepartment
            // 
            this.cmbDepartment.FormattingEnabled = true;
            this.cmbDepartment.Location = new System.Drawing.Point(18, 140);
            this.cmbDepartment.Name = "cmbDepartment";
            this.cmbDepartment.Size = new System.Drawing.Size(149, 21);
            this.cmbDepartment.TabIndex = 3;
            // 
            // cmbSendTo
            // 
            this.cmbSendTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSendTo.FormattingEnabled = true;
            this.cmbSendTo.Location = new System.Drawing.Point(179, 230);
            this.cmbSendTo.Name = "cmbSendTo";
            this.cmbSendTo.Size = new System.Drawing.Size(159, 21);
            this.cmbSendTo.TabIndex = 8;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(182, 168);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 13);
            this.label14.TabIndex = 44;
            this.label14.Text = "Tax :";
            // 
            // cmbTax
            // 
            this.cmbTax.FormattingEnabled = true;
            this.cmbTax.Location = new System.Drawing.Point(179, 182);
            this.cmbTax.Name = "cmbTax";
            this.cmbTax.Size = new System.Drawing.Size(159, 21);
            this.cmbTax.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 168);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 42;
            this.label2.Text = "Section :";
            // 
            // cmbSection
            // 
            this.cmbSection.FormattingEnabled = true;
            this.cmbSection.Location = new System.Drawing.Point(18, 182);
            this.cmbSection.Name = "cmbSection";
            this.cmbSection.Size = new System.Drawing.Size(149, 21);
            this.cmbSection.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(182, 126);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 41;
            this.label13.Text = "Category :";
            // 
            // cmbCategory
            // 
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Location = new System.Drawing.Point(179, 140);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(159, 21);
            this.cmbCategory.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.dtpEndDate);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dtpStartDate);
            this.groupBox1.Location = new System.Drawing.Point(15, 61);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(323, 56);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Date Range";
            // 
            // frmRptRestaurantStock
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(362, 334);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.pnlSalesSummary);
            this.Name = "frmRptRestaurantStock";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Restaurant Stock";
            this.Load += new System.EventHandler(this.frmRptRestaurantStock_Load);
            this.pnlSalesSummary.ResumeLayout(false);
            this.pnlSalesSummary.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}