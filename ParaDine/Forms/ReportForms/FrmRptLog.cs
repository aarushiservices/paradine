﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using ParaDine.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class FrmRptLog : Form
    {
        private Button btnExit;
        private Button btnPrint;
        private ComboBox cmbSendTo;
        private ComboBox cmbUser;
       // private IContainer components = null;
        private DataSet DS = new DataSet();
        private DateTimePicker dtpfrom;
        private DateTimePicker dtpTo;
        private Label label1;
        private Label label15;
        private Label label2;
        private Label label3;
        private Panel panel1;
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private SqlCommand SqlCmd = new SqlCommand();
        private string str;

        public FrmRptLog()
        {
            this.InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                object obj2;
                string str = "";
                string str2 = "";
                this.SqlCmd = new SqlCommand();
                this.SqlCmd.Connection = GlobalVariables.SqlConn;
                str2 = "SELECT * FROM RES_VW_LOG WHERE LOGID <> 0 ";
                if (this.dtpfrom.Checked)
                {
                    obj2 = str2;
                    str2 = string.Concat(new object[] { obj2, " AND MODIFIEDDATE >= '", this.dtpfrom.Value, "'" });
                    str = str + " Log Date From : " + this.dtpfrom.Text + "; ";
                }
                if (this.dtpTo.Checked)
                {
                    obj2 = str2;
                    str2 = string.Concat(new object[] { obj2, " AND MODIFIEDDATE <= '", this.dtpTo.Value, "'" });
                    str = str + " Log Date UpTo : " + this.dtpTo.Text + "; ";
                }
                if (Convert.ToInt32(this.cmbUser.SelectedValue) > 0)
                {
                    str2 = str2 + " AND USERID = " + this.cmbUser.SelectedValue;
                    str = str + " User : " + this.cmbUser.Text + "; ";
                }
                this.SqlCmd.CommandText = str2;
                this.SqlCmd.CommandType = CommandType.Text;
                this.SDA.SelectCommand = this.SqlCmd;
                if (!object.ReferenceEquals(this.DS.Tables["RES_VW_LOG"], null))
                {
                    this.DS.Tables["RES_VW_LOG"].Clear();
                }
                this.SDA.Fill(this.DS, "RES_VW_LOG");
                //ParameterFields pfields = new ParameterFields();
                //ParameterField parameterField = new ParameterField
                //{
                //    Name = "Parameter"
                //};
                //ParameterDiscreteValue value2 = new ParameterDiscreteValue
                //{
                //    Value = str
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                if (this.DS.Tables["RES_VW_LOG"].Rows.Count > 0)
                {
                    if (this.cmbSendTo.Text == "SCREEN")
                    {
                        ///By Bharat we have to change Reports to itextsparp pdf
                        ///
                        RptLogHistory rpt = new RptLogHistory(this.DS.Tables["RES_VW_LOG"]);
                        rpt.Parameter_Parameter = str;
                        rpt.ShowDialog();
                        //rpt.SetDataSource(this.DS.Tables["RES_VW_LOG"]);
                        //rpt.Refresh();
                        //new FrmRptViewer(rpt, pfields).ShowDialog();
                    }
                }
                else
                {
                    MessageBox.Show("DATA NOT FOUND");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "btnPrint.Click");
            }
        }

      

        private void FrmRptLog_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.RefreshData();
        }

        

        private void RefreshData()
        {
            this.dtpfrom.Value = GlobalVariables.BusinessDate;
            this.dtpTo.Value = GlobalVariables.BusinessDate;
            this.str = "SELECT USERID, USERNAME FROM GLB_USER";
            GlobalFill.FillCombo(this.str, this.cmbUser);
            this.cmbSendTo.Items.Clear();
            this.cmbSendTo.Items.Add("SCREEN");
            this.cmbSendTo.Items.Add("PRINTER");
            this.cmbSendTo.Items.Add("DISK");
            this.cmbSendTo.SelectedIndex = 0;
        }
    }
}
