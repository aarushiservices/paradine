﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.ReportForms
{
    partial class frmRptDayReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
         {
            this.dtpDayReport = new System.Windows.Forms.DateTimePicker();
            this.btnPrint = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dtpDayReport
            // 
            this.dtpDayReport = new DateTimePicker();
            this.btnPrint = new Button();
            this.label1 = new Label();
            base.SuspendLayout();
            this.dtpDayReport.CustomFormat = "dd/MMMM/yyyy";
            this.dtpDayReport.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDayReport.Location = new System.Drawing.Point(71, 30);
            this.dtpDayReport.Name = "dtpDayReport";
            this.dtpDayReport.Size = new System.Drawing.Size(143, 20);
            this.dtpDayReport.TabIndex = 0;
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(95, 75);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(94, 30);
            this.btnPrint.TabIndex = 1;
            this.btnPrint.Text = "&Display";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(71, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Select Date";
            // 
            // frmRptDayReport
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(284, 151);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.dtpDayReport);
            this.Name = "frmRptDayReport";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Day Report";
            this.Load += new System.EventHandler(this.frmRptDayReport_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

            base.AutoScaleDimensions = new SizeF(6f, 13f);
//          base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x11c, 0x97);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.btnPrint);
            base.Controls.Add(this.dtpDayReport);
            base.Name = "frmRptDayReport";
            base.ShowIcon = false;
            base.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "Day Report";
            base.Load += new EventHandler(this.frmRptDayReport_Load);
            base.ResumeLayout(false);
            base.PerformLayout();
        }
        #endregion
    }
}