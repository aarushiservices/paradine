﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using ParaDine.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmRptCustOrderKOT : Form
    {
        private Button BtnExit;
        private Button btnPrint;
        private CheckBox chkPendingBills;
        private CheckBox chkRefund;
        private ComboBox cmbMachineNo;
        private ComboBox cmbSourceName;
        private ComboBox cmbSteward;
        private ComboBox cmbStockPoint;
        //private IContainer components = null;
        private DataSet Ds = new DataSet();
        private DateTimePicker dtpStartDateFrom;
        private DateTimePicker dtpStartDateTo;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private Panel grpfields;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label3;
        private Label label4;
        private Label label6;
        private Label label7;
        private Label label8;
        private SqlDataAdapter Sda = new SqlDataAdapter();
        private string StrSql = "";
        private TextBox txtCustOrderNoFrom;
        private TextBox txtCustOrderNoTo;

        public frmRptCustOrderKOT()
        {
            this.InitializeComponent();
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                this.StrSql = "SELECT COK.* FROM RES_VW_CUSTORDERKOT COK  INNER JOIN RES_VW_CUSTORDERMASTER COM ON COK.CUSTORDERMASTERID = COM.CUSTORDERMASTERID  WHERE COK.CUSTORDERKOTID <> 0 ";
                string str = "";
                string str2 = "";
                string text = "";
                if (this.txtCustOrderNoFrom.Text != "")
                {
                    str = str + " AND CUSTORDERNO >= '" + this.txtCustOrderNoFrom.Text + "'";
                    str2 = str2 + "CustomerOrderNo From " + this.txtCustOrderNoFrom.Text + "; ";
                }
                if (this.txtCustOrderNoTo.Text != "")
                {
                    str = str + " AND CUSTORDERNO <= '" + this.txtCustOrderNoTo.Text + "'";
                    str2 = str2 + "CustomerOrderNo Upto " + this.txtCustOrderNoTo.Text + "; ";
                }
                if (this.cmbMachineNo.Text != "")
                {
                    str = str + " AND MACHINENO = " + this.cmbMachineNo.SelectedValue;
                    str2 = str2 + "Machine = " + this.cmbMachineNo.Text + " ";
                }
                if (this.dtpStartDateFrom.Checked)
                {
                    str = str + " AND CONVERT(DATETIME, CONVERT(VARCHAR, STARTTIME, 112))  >= '" + this.dtpStartDateFrom.Text + "'";
                    str2 = str2 + "StartTime From " + this.dtpStartDateFrom.Text + "; ";
                }
                if (this.dtpStartDateTo.Checked)
                {
                    str = str + " AND CONVERT(DATETIME, CONVERT(VARCHAR, STARTTIME, 112)) <= '" + this.dtpStartDateTo.Text + "'";
                    str2 = str2 + "StartTime UpTo " + this.dtpStartDateTo.Text + "; ";
                }
                if (this.cmbSteward.Text != "")
                {
                    str = str + " AND STEWARDNAME = '" + this.cmbSteward.Text + "'";
                    str2 = str2 + "Steward is " + this.cmbSteward.Text + "; ";
                }
                if (this.cmbStockPoint.Text != "")
                {
                    str = str + " AND STOCKPOINTNAME = '" + this.cmbStockPoint.Text + "'";
                    text = this.cmbStockPoint.Text;
                }
                if (this.cmbSourceName.Text != "")
                {
                    str = str + " AND SOURCENAME = '" + this.cmbSourceName.Text + "'";
                    str2 = str2 + " Source - " + this.cmbSourceName.Text + " ";
                }
                if (this.chkPendingBills.Checked)
                {
                    str = str + " AND SETTLED = 0 ";
                    str2 = str2 + " Bill is Pending; ";
                }
                if (this.chkRefund.Checked)
                {
                    this.StrSql = this.StrSql + " AND COK.CUSTORDERKOTID IN (SELECT COK.CUSTORDERKOTID FROM RES_CUSTORDERITEM COI WHERE COI.CUSTORDERKOTID = COK.CUSTORDERKOTID AND SALEQTY < 0)" + str;
                    str2 = str2 + " Return KOTs";
                }
                else
                {
                    this.StrSql = this.StrSql + str;
                }
                //ParameterFields pfields = new ParameterFields();
                //ParameterField parameterField = new ParameterField
                //{
                //    Name = "Parameter"
                //};
                //ParameterDiscreteValue value2 = new ParameterDiscreteValue
                //{
                //    Value = str2
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                //parameterField = new ParameterField
                //{
                //    Name = "RestaurantName"
                //};
                //value2 = new ParameterDiscreteValue
                //{
                //    Value = text
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                //parameterField = new ParameterField
                //{
                //    Name = "CompanyName"
                //};
                //value2 = new ParameterDiscreteValue
                //{
                //    Value = GlobalVariables.StrCompName
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                GlobalFill.FillDataSet(this.StrSql, "CUSTORDERKOT", this.Ds, this.Sda);
                if (this.Ds.Tables["CUSTORDERKOT"].Rows.Count > 0)
                {
                    ///By Bharat we have to change Reports to itextsparp pdf
                    ///
                    RptCustOrderKOT rpt = new RptCustOrderKOT((this.Ds.Tables["CUSTORDERKOT"]));
                    rpt.Parameter_CompanyName = GlobalVariables.StrCompName;
                    rpt.Parameter_Parameter = str2;
                    rpt.Parameter_RestaurantName = text;
                    rpt.Show();
                    //rpt.SetDataSource(this.Ds.Tables["CUSTORDERKOT"]);
                    //new FrmRptViewer(rpt, pfields).Show();
                }
                else
                {
                    MessageBox.Show("Data Not Found!");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

      

        private void frmRptCustOrderKOT_Load(object sender, EventArgs e)
        {
            try
            {
                new GlobalTheme().applyTheme(this);
                this.RefreshData();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

   

        private void RefreshData()
        {
            GlobalFill.FillCombo("SELECT TERMINALID, TERMINALNAME FROM GLB_TERMINAL ORDER BY 2", this.cmbMachineNo);
            GlobalFill.FillCombo("SELECT STEWARDID, STEWARDNAME FROM RES_STEWARD ORDER BY 2", this.cmbSteward);
            GlobalFill.FillCombo("SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT WHERE ISBILLPOINT = 1 ORDER BY 2", this.cmbStockPoint);
            GlobalFill.FillCombo("SELECT TABLEID, TABLENAME FROM RES_TABLE ORDER BY 2", this.cmbSourceName);
            this.dtpStartDateFrom.Value = GlobalVariables.BusinessDate;
            this.dtpStartDateTo.Value = GlobalVariables.BusinessDate;
            this.cmbStockPoint.SelectedIndex = 0;
        }
    }
}
