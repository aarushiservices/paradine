﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.ReportForms
{
    partial class frmRptSalesSummary2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlSaleSaummary = new System.Windows.Forms.Panel();
            this.grpCondition = new System.Windows.Forms.GroupBox();
            this.chkrawandrecipe = new System.Windows.Forms.CheckBox();
            this.chkCrossTab = new System.Windows.Forms.CheckBox();
            this.chkProfitReport = new System.Windows.Forms.CheckBox();
            this.chkDispRwMaterial = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbSendTo = new System.Windows.Forms.ComboBox();
            this.chkUOMNotReq = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dtpOrderDateTo = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpOrderDateFrom = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtFrCustOrderNo = new System.Windows.Forms.TextBox();
            this.txtToCustOrderNo = new System.Windows.Forms.TextBox();
            this.grpDisplay = new System.Windows.Forms.GroupBox();
            this.rdDispTax = new System.Windows.Forms.RadioButton();
            this.rdDispStockpoint = new System.Windows.Forms.RadioButton();
            this.rdDispSection = new System.Windows.Forms.RadioButton();
            this.rdDispSaleprice = new System.Windows.Forms.RadioButton();
            this.rdDispPaymode = new System.Windows.Forms.RadioButton();
            this.rdDispYear = new System.Windows.Forms.RadioButton();
            this.rdDispMonth = new System.Windows.Forms.RadioButton();
            this.rdDispMachine = new System.Windows.Forms.RadioButton();
            this.rdDispBillDate = new System.Windows.Forms.RadioButton();
            this.rdDispBillNo = new System.Windows.Forms.RadioButton();
            this.rdDispKOT = new System.Windows.Forms.RadioButton();
            this.rdDispSteward = new System.Windows.Forms.RadioButton();
            this.rdDispItem = new System.Windows.Forms.RadioButton();
            this.rdDispCategory = new System.Windows.Forms.RadioButton();
            this.rdDispDepartment = new System.Windows.Forms.RadioButton();
            this.grpGroup = new System.Windows.Forms.GroupBox();
            this.rdGrpStockpoint = new System.Windows.Forms.RadioButton();
            this.rdGrpSection = new System.Windows.Forms.RadioButton();
            this.rdGrpSalePrice = new System.Windows.Forms.RadioButton();
            this.rdGrpPaymode = new System.Windows.Forms.RadioButton();
            this.rdGrpYear = new System.Windows.Forms.RadioButton();
            this.rdGrpMonth = new System.Windows.Forms.RadioButton();
            this.rdGrpMachine = new System.Windows.Forms.RadioButton();
            this.rdGrpBillDate = new System.Windows.Forms.RadioButton();
            this.rdGrpBillNo = new System.Windows.Forms.RadioButton();
            this.rdGrpKot = new System.Windows.Forms.RadioButton();
            this.rdGrpSteward = new System.Windows.Forms.RadioButton();
            this.rdGrpTax = new System.Windows.Forms.RadioButton();
            this.rdGrpItem = new System.Windows.Forms.RadioButton();
            this.rdGrpCategory = new System.Windows.Forms.RadioButton();
            this.rdGrpDepartment = new System.Windows.Forms.RadioButton();
            this.grpStockPoint = new System.Windows.Forms.GroupBox();
            this.lstStockpoint = new System.Windows.Forms.ListView();
            this.rdSelStockpoint = new System.Windows.Forms.RadioButton();
            this.rdAllStockpoint = new System.Windows.Forms.RadioButton();
            this.grpSteward = new System.Windows.Forms.GroupBox();
            this.lstSteward = new System.Windows.Forms.ListView();
            this.rdSelSteward = new System.Windows.Forms.RadioButton();
            this.rdAllSteward = new System.Windows.Forms.RadioButton();
            this.grpSection = new System.Windows.Forms.GroupBox();
            this.lstSection = new System.Windows.Forms.ListView();
            this.rdSelSection = new System.Windows.Forms.RadioButton();
            this.rdAllSection = new System.Windows.Forms.RadioButton();
            this.grpMachine = new System.Windows.Forms.GroupBox();
            this.lstMachine = new System.Windows.Forms.ListView();
            this.rdSelMachine = new System.Windows.Forms.RadioButton();
            this.rdAllMachine = new System.Windows.Forms.RadioButton();
            this.grpTax = new System.Windows.Forms.GroupBox();
            this.lstTax = new System.Windows.Forms.ListView();
            this.rdSelTax = new System.Windows.Forms.RadioButton();
            this.rdAllTax = new System.Windows.Forms.RadioButton();
            this.grpPayments = new System.Windows.Forms.GroupBox();
            this.lstPayments = new System.Windows.Forms.ListView();
            this.rdSelPayments = new System.Windows.Forms.RadioButton();
            this.rdAllPayments = new System.Windows.Forms.RadioButton();
            this.grpItems = new System.Windows.Forms.GroupBox();
            this.lstItem = new System.Windows.Forms.ListView();
            this.rdSelItem = new System.Windows.Forms.RadioButton();
            this.rdAllItem = new System.Windows.Forms.RadioButton();
            this.grpCategory = new System.Windows.Forms.GroupBox();
            this.lstCategory = new System.Windows.Forms.ListView();
            this.rdSelCatg = new System.Windows.Forms.RadioButton();
            this.rdAllCatg = new System.Windows.Forms.RadioButton();
            this.grpDepartment = new System.Windows.Forms.GroupBox();
            this.lstDepartment = new System.Windows.Forms.ListView();
            this.rdSelDept = new System.Windows.Forms.RadioButton();
            this.rdAllDept = new System.Windows.Forms.RadioButton();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnGrid = new System.Windows.Forms.Button();
            this.pnlSaleSaummary.SuspendLayout();
            this.grpCondition.SuspendLayout();
            this.grpDisplay.SuspendLayout();
            this.grpGroup.SuspendLayout();
            this.grpStockPoint.SuspendLayout();
            this.grpSteward.SuspendLayout();
            this.grpSection.SuspendLayout();
            this.grpMachine.SuspendLayout();
            this.grpTax.SuspendLayout();
            this.grpPayments.SuspendLayout();
            this.grpItems.SuspendLayout();
            this.grpCategory.SuspendLayout();
            this.grpDepartment.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSaleSaummary
            // 
            this.pnlSaleSaummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSaleSaummary.Controls.Add(this.grpCondition);
            this.pnlSaleSaummary.Controls.Add(this.grpDisplay);
            this.pnlSaleSaummary.Controls.Add(this.grpGroup);
            this.pnlSaleSaummary.Controls.Add(this.grpStockPoint);
            this.pnlSaleSaummary.Controls.Add(this.grpSteward);
            this.pnlSaleSaummary.Controls.Add(this.grpSection);
            this.pnlSaleSaummary.Controls.Add(this.grpMachine);
            this.pnlSaleSaummary.Controls.Add(this.grpTax);
            this.pnlSaleSaummary.Controls.Add(this.grpPayments);
            this.pnlSaleSaummary.Controls.Add(this.grpItems);
            this.pnlSaleSaummary.Controls.Add(this.grpCategory);
            this.pnlSaleSaummary.Controls.Add(this.grpDepartment);
            this.pnlSaleSaummary.Location = new System.Drawing.Point(7, 1);
            this.pnlSaleSaummary.Name = "pnlSaleSaummary";
            this.pnlSaleSaummary.Size = new System.Drawing.Size(947, 454);
            this.pnlSaleSaummary.TabIndex = 0;
            // 
            // grpCondition
            // 
            this.grpCondition.Controls.Add(this.chkrawandrecipe);
            this.grpCondition.Controls.Add(this.chkCrossTab);
            this.grpCondition.Controls.Add(this.chkProfitReport);
            this.grpCondition.Controls.Add(this.chkDispRwMaterial);
            this.grpCondition.Controls.Add(this.label15);
            this.grpCondition.Controls.Add(this.cmbSendTo);
            this.grpCondition.Controls.Add(this.chkUOMNotReq);
            this.grpCondition.Controls.Add(this.label10);
            this.grpCondition.Controls.Add(this.dtpOrderDateTo);
            this.grpCondition.Controls.Add(this.label3);
            this.grpCondition.Controls.Add(this.dtpOrderDateFrom);
            this.grpCondition.Controls.Add(this.label11);
            this.grpCondition.Controls.Add(this.label12);
            this.grpCondition.Controls.Add(this.txtFrCustOrderNo);
            this.grpCondition.Controls.Add(this.txtToCustOrderNo);
            this.grpCondition.Location = new System.Drawing.Point(6, 364);
            this.grpCondition.Name = "grpCondition";
            this.grpCondition.Size = new System.Drawing.Size(930, 85);
            this.grpCondition.TabIndex = 19;
            this.grpCondition.TabStop = false;
            this.grpCondition.Text = "Conditions";
            // 
            // chkrawandrecipe
            // 
            this.chkrawandrecipe.AutoSize = true;
            this.chkrawandrecipe.Location = new System.Drawing.Point(788, 22);
            this.chkrawandrecipe.Name = "chkrawandrecipe";
            this.chkrawandrecipe.Size = new System.Drawing.Size(136, 17);
            this.chkrawandrecipe.TabIndex = 62;
            this.chkrawandrecipe.Text = "Recipe and Raw report";
            this.chkrawandrecipe.UseVisualStyleBackColor = true;
            // 
            // chkCrossTab
            // 
            this.chkCrossTab.AutoSize = true;
            this.chkCrossTab.Location = new System.Drawing.Point(647, 50);
            this.chkCrossTab.Name = "chkCrossTab";
            this.chkCrossTab.Size = new System.Drawing.Size(146, 17);
            this.chkCrossTab.TabIndex = 61;
            this.chkCrossTab.Text = "Display Cross Tab Report";
            this.chkCrossTab.UseVisualStyleBackColor = true;
            this.chkCrossTab.CheckedChanged += new System.EventHandler(this.chkCrossTab_CheckedChanged);
            // 
            // chkProfitReport
            // 
            this.chkProfitReport.AutoSize = true;
            this.chkProfitReport.Location = new System.Drawing.Point(647, 22);
            this.chkProfitReport.Name = "chkProfitReport";
            this.chkProfitReport.Size = new System.Drawing.Size(122, 17);
            this.chkProfitReport.TabIndex = 60;
            this.chkProfitReport.Text = "Display Profit Report";
            this.chkProfitReport.UseVisualStyleBackColor = true;
            // 
            // chkDispRwMaterial
            // 
            this.chkDispRwMaterial.AutoSize = true;
            this.chkDispRwMaterial.Location = new System.Drawing.Point(508, 22);
            this.chkDispRwMaterial.Name = "chkDispRwMaterial";
            this.chkDispRwMaterial.Size = new System.Drawing.Size(125, 17);
            this.chkDispRwMaterial.TabIndex = 59;
            this.chkDispRwMaterial.Text = "Display Raw Material";
            this.chkDispRwMaterial.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(352, 29);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 13);
            this.label15.TabIndex = 58;
            this.label15.Text = "Send To :";
            // 
            // cmbSendTo
            // 
            this.cmbSendTo.FormattingEnabled = true;
            this.cmbSendTo.Location = new System.Drawing.Point(355, 45);
            this.cmbSendTo.Name = "cmbSendTo";
            this.cmbSendTo.Size = new System.Drawing.Size(140, 21);
            this.cmbSendTo.TabIndex = 57;
            // 
            // chkUOMNotReq
            // 
            this.chkUOMNotReq.AutoSize = true;
            this.chkUOMNotReq.Location = new System.Drawing.Point(508, 48);
            this.chkUOMNotReq.Name = "chkUOMNotReq";
            this.chkUOMNotReq.Size = new System.Drawing.Size(117, 17);
            this.chkUOMNotReq.TabIndex = 56;
            this.chkUOMNotReq.Text = "UOM Not Required";
            this.chkUOMNotReq.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(145, 50);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 13);
            this.label10.TabIndex = 29;
            this.label10.Text = "To :";
            // 
            // dtpOrderDateTo
            // 
            this.dtpOrderDateTo.CustomFormat = "dd/MMM/yyyy hh:mm tt";
            this.dtpOrderDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOrderDateTo.Location = new System.Drawing.Point(173, 47);
            this.dtpOrderDateTo.Name = "dtpOrderDateTo";
            this.dtpOrderDateTo.ShowCheckBox = true;
            this.dtpOrderDateTo.Size = new System.Drawing.Size(159, 20);
            this.dtpOrderDateTo.TabIndex = 27;
            this.dtpOrderDateTo.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(137, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "From :";
            // 
            // dtpOrderDateFrom
            // 
            this.dtpOrderDateFrom.CustomFormat = "dd/MMM/yyyy hh:mm tt";
            this.dtpOrderDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOrderDateFrom.Location = new System.Drawing.Point(173, 19);
            this.dtpOrderDateFrom.Name = "dtpOrderDateFrom";
            this.dtpOrderDateFrom.ShowCheckBox = true;
            this.dtpOrderDateFrom.Size = new System.Drawing.Size(159, 20);
            this.dtpOrderDateFrom.TabIndex = 26;
            this.dtpOrderDateFrom.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(14, 47);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Bill To :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "Bill From :";
            // 
            // txtFrCustOrderNo
            // 
            this.txtFrCustOrderNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFrCustOrderNo.Location = new System.Drawing.Point(62, 19);
            this.txtFrCustOrderNo.Name = "txtFrCustOrderNo";
            this.txtFrCustOrderNo.Size = new System.Drawing.Size(69, 20);
            this.txtFrCustOrderNo.TabIndex = 0;
            // 
            // txtToCustOrderNo
            // 
            this.txtToCustOrderNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtToCustOrderNo.Location = new System.Drawing.Point(62, 45);
            this.txtToCustOrderNo.Name = "txtToCustOrderNo";
            this.txtToCustOrderNo.Size = new System.Drawing.Size(69, 20);
            this.txtToCustOrderNo.TabIndex = 1;
            // 
            // grpDisplay
            // 
            this.grpDisplay.Controls.Add(this.rdDispTax);
            this.grpDisplay.Controls.Add(this.rdDispStockpoint);
            this.grpDisplay.Controls.Add(this.rdDispSection);
            this.grpDisplay.Controls.Add(this.rdDispSaleprice);
            this.grpDisplay.Controls.Add(this.rdDispPaymode);
            this.grpDisplay.Controls.Add(this.rdDispYear);
            this.grpDisplay.Controls.Add(this.rdDispMonth);
            this.grpDisplay.Controls.Add(this.rdDispMachine);
            this.grpDisplay.Controls.Add(this.rdDispBillDate);
            this.grpDisplay.Controls.Add(this.rdDispBillNo);
            this.grpDisplay.Controls.Add(this.rdDispKOT);
            this.grpDisplay.Controls.Add(this.rdDispSteward);
            this.grpDisplay.Controls.Add(this.rdDispItem);
            this.grpDisplay.Controls.Add(this.rdDispCategory);
            this.grpDisplay.Controls.Add(this.rdDispDepartment);
            this.grpDisplay.Location = new System.Drawing.Point(847, 7);
            this.grpDisplay.Name = "grpDisplay";
            this.grpDisplay.Size = new System.Drawing.Size(91, 356);
            this.grpDisplay.TabIndex = 18;
            this.grpDisplay.TabStop = false;
            this.grpDisplay.Text = "Display";
            // 
            // rdDispTax
            // 
            this.rdDispTax.AutoSize = true;
            this.rdDispTax.Location = new System.Drawing.Point(8, 107);
            this.rdDispTax.Name = "rdDispTax";
            this.rdDispTax.Size = new System.Drawing.Size(43, 17);
            this.rdDispTax.TabIndex = 31;
            this.rdDispTax.Text = "Tax";
            this.rdDispTax.UseVisualStyleBackColor = true;
            // 
            // rdDispStockpoint
            // 
            this.rdDispStockpoint.AutoSize = true;
            this.rdDispStockpoint.Location = new System.Drawing.Point(8, 27);
            this.rdDispStockpoint.Name = "rdDispStockpoint";
            this.rdDispStockpoint.Size = new System.Drawing.Size(77, 17);
            this.rdDispStockpoint.TabIndex = 29;
            this.rdDispStockpoint.Text = "StockPoint";
            this.rdDispStockpoint.UseVisualStyleBackColor = true;
            // 
            // rdDispSection
            // 
            this.rdDispSection.AutoSize = true;
            this.rdDispSection.Location = new System.Drawing.Point(8, 87);
            this.rdDispSection.Name = "rdDispSection";
            this.rdDispSection.Size = new System.Drawing.Size(61, 17);
            this.rdDispSection.TabIndex = 28;
            this.rdDispSection.Text = "Section";
            this.rdDispSection.UseVisualStyleBackColor = true;
            // 
            // rdDispSaleprice
            // 
            this.rdDispSaleprice.AutoSize = true;
            this.rdDispSaleprice.Location = new System.Drawing.Point(8, 147);
            this.rdDispSaleprice.Name = "rdDispSaleprice";
            this.rdDispSaleprice.Size = new System.Drawing.Size(70, 17);
            this.rdDispSaleprice.TabIndex = 27;
            this.rdDispSaleprice.Text = "SalePrice";
            this.rdDispSaleprice.UseVisualStyleBackColor = true;
            // 
            // rdDispPaymode
            // 
            this.rdDispPaymode.AutoSize = true;
            this.rdDispPaymode.Location = new System.Drawing.Point(8, 307);
            this.rdDispPaymode.Name = "rdDispPaymode";
            this.rdDispPaymode.Size = new System.Drawing.Size(69, 17);
            this.rdDispPaymode.TabIndex = 26;
            this.rdDispPaymode.Text = "Paymode";
            this.rdDispPaymode.UseVisualStyleBackColor = true;
            // 
            // rdDispYear
            // 
            this.rdDispYear.AutoSize = true;
            this.rdDispYear.Location = new System.Drawing.Point(8, 267);
            this.rdDispYear.Name = "rdDispYear";
            this.rdDispYear.Size = new System.Drawing.Size(47, 17);
            this.rdDispYear.TabIndex = 25;
            this.rdDispYear.Text = "Year";
            this.rdDispYear.UseVisualStyleBackColor = true;
            // 
            // rdDispMonth
            // 
            this.rdDispMonth.AutoSize = true;
            this.rdDispMonth.Location = new System.Drawing.Point(8, 247);
            this.rdDispMonth.Name = "rdDispMonth";
            this.rdDispMonth.Size = new System.Drawing.Size(55, 17);
            this.rdDispMonth.TabIndex = 24;
            this.rdDispMonth.Text = "Month";
            this.rdDispMonth.UseVisualStyleBackColor = true;
            // 
            // rdDispMachine
            // 
            this.rdDispMachine.AutoSize = true;
            this.rdDispMachine.Location = new System.Drawing.Point(8, 287);
            this.rdDispMachine.Name = "rdDispMachine";
            this.rdDispMachine.Size = new System.Drawing.Size(66, 17);
            this.rdDispMachine.TabIndex = 23;
            this.rdDispMachine.Text = "Machine";
            this.rdDispMachine.UseVisualStyleBackColor = true;
            // 
            // rdDispBillDate
            // 
            this.rdDispBillDate.AutoSize = true;
            this.rdDispBillDate.Location = new System.Drawing.Point(8, 227);
            this.rdDispBillDate.Name = "rdDispBillDate";
            this.rdDispBillDate.Size = new System.Drawing.Size(61, 17);
            this.rdDispBillDate.TabIndex = 22;
            this.rdDispBillDate.Text = "BillDate";
            this.rdDispBillDate.UseVisualStyleBackColor = true;
            // 
            // rdDispBillNo
            // 
            this.rdDispBillNo.AutoSize = true;
            this.rdDispBillNo.Location = new System.Drawing.Point(8, 207);
            this.rdDispBillNo.Name = "rdDispBillNo";
            this.rdDispBillNo.Size = new System.Drawing.Size(52, 17);
            this.rdDispBillNo.TabIndex = 21;
            this.rdDispBillNo.Text = "BillNo";
            this.rdDispBillNo.UseVisualStyleBackColor = true;
            // 
            // rdDispKOT
            // 
            this.rdDispKOT.AutoSize = true;
            this.rdDispKOT.Location = new System.Drawing.Point(8, 187);
            this.rdDispKOT.Name = "rdDispKOT";
            this.rdDispKOT.Size = new System.Drawing.Size(47, 17);
            this.rdDispKOT.TabIndex = 20;
            this.rdDispKOT.Text = "KOT";
            this.rdDispKOT.UseVisualStyleBackColor = true;
            // 
            // rdDispSteward
            // 
            this.rdDispSteward.AutoSize = true;
            this.rdDispSteward.Location = new System.Drawing.Point(8, 167);
            this.rdDispSteward.Name = "rdDispSteward";
            this.rdDispSteward.Size = new System.Drawing.Size(64, 17);
            this.rdDispSteward.TabIndex = 19;
            this.rdDispSteward.Text = "Steward";
            this.rdDispSteward.UseVisualStyleBackColor = true;
            // 
            // rdDispItem
            // 
            this.rdDispItem.AutoSize = true;
            this.rdDispItem.Checked = true;
            this.rdDispItem.Location = new System.Drawing.Point(8, 127);
            this.rdDispItem.Name = "rdDispItem";
            this.rdDispItem.Size = new System.Drawing.Size(45, 17);
            this.rdDispItem.TabIndex = 18;
            this.rdDispItem.TabStop = true;
            this.rdDispItem.Text = "Item";
            this.rdDispItem.UseVisualStyleBackColor = true;
            // 
            // rdDispCategory
            // 
            this.rdDispCategory.AutoSize = true;
            this.rdDispCategory.Location = new System.Drawing.Point(8, 67);
            this.rdDispCategory.Name = "rdDispCategory";
            this.rdDispCategory.Size = new System.Drawing.Size(67, 17);
            this.rdDispCategory.TabIndex = 17;
            this.rdDispCategory.Text = "Category";
            this.rdDispCategory.UseVisualStyleBackColor = true;
            // 
            // rdDispDepartment
            // 
            this.rdDispDepartment.AutoSize = true;
            this.rdDispDepartment.Location = new System.Drawing.Point(8, 47);
            this.rdDispDepartment.Name = "rdDispDepartment";
            this.rdDispDepartment.Size = new System.Drawing.Size(80, 17);
            this.rdDispDepartment.TabIndex = 16;
            this.rdDispDepartment.Text = "Department";
            this.rdDispDepartment.UseVisualStyleBackColor = true;
            // 
            // grpGroup
            // 
            this.grpGroup.Controls.Add(this.rdGrpStockpoint);
            this.grpGroup.Controls.Add(this.rdGrpSection);
            this.grpGroup.Controls.Add(this.rdGrpSalePrice);
            this.grpGroup.Controls.Add(this.rdGrpPaymode);
            this.grpGroup.Controls.Add(this.rdGrpYear);
            this.grpGroup.Controls.Add(this.rdGrpMonth);
            this.grpGroup.Controls.Add(this.rdGrpMachine);
            this.grpGroup.Controls.Add(this.rdGrpBillDate);
            this.grpGroup.Controls.Add(this.rdGrpBillNo);
            this.grpGroup.Controls.Add(this.rdGrpKot);
            this.grpGroup.Controls.Add(this.rdGrpSteward);
            this.grpGroup.Controls.Add(this.rdGrpTax);
            this.grpGroup.Controls.Add(this.rdGrpItem);
            this.grpGroup.Controls.Add(this.rdGrpCategory);
            this.grpGroup.Controls.Add(this.rdGrpDepartment);
            this.grpGroup.Location = new System.Drawing.Point(758, 7);
            this.grpGroup.Name = "grpGroup";
            this.grpGroup.Size = new System.Drawing.Size(91, 356);
            this.grpGroup.TabIndex = 17;
            this.grpGroup.TabStop = false;
            this.grpGroup.Text = "Group";
            // 
            // rdGrpStockpoint
            // 
            this.rdGrpStockpoint.AutoSize = true;
            this.rdGrpStockpoint.Checked = true;
            this.rdGrpStockpoint.Location = new System.Drawing.Point(8, 25);
            this.rdGrpStockpoint.Name = "rdGrpStockpoint";
            this.rdGrpStockpoint.Size = new System.Drawing.Size(77, 17);
            this.rdGrpStockpoint.TabIndex = 14;
            this.rdGrpStockpoint.TabStop = true;
            this.rdGrpStockpoint.Text = "StockPoint";
            this.rdGrpStockpoint.UseVisualStyleBackColor = true;
            // 
            // rdGrpSection
            // 
            this.rdGrpSection.AutoSize = true;
            this.rdGrpSection.Location = new System.Drawing.Point(8, 85);
            this.rdGrpSection.Name = "rdGrpSection";
            this.rdGrpSection.Size = new System.Drawing.Size(61, 17);
            this.rdGrpSection.TabIndex = 13;
            this.rdGrpSection.Text = "Section";
            this.rdGrpSection.UseVisualStyleBackColor = true;
            // 
            // rdGrpSalePrice
            // 
            this.rdGrpSalePrice.AutoSize = true;
            this.rdGrpSalePrice.Location = new System.Drawing.Point(8, 145);
            this.rdGrpSalePrice.Name = "rdGrpSalePrice";
            this.rdGrpSalePrice.Size = new System.Drawing.Size(70, 17);
            this.rdGrpSalePrice.TabIndex = 12;
            this.rdGrpSalePrice.Text = "SalePrice";
            this.rdGrpSalePrice.UseVisualStyleBackColor = true;
            // 
            // rdGrpPaymode
            // 
            this.rdGrpPaymode.AutoSize = true;
            this.rdGrpPaymode.Location = new System.Drawing.Point(8, 305);
            this.rdGrpPaymode.Name = "rdGrpPaymode";
            this.rdGrpPaymode.Size = new System.Drawing.Size(69, 17);
            this.rdGrpPaymode.TabIndex = 11;
            this.rdGrpPaymode.Text = "Paymode";
            this.rdGrpPaymode.UseVisualStyleBackColor = true;
            // 
            // rdGrpYear
            // 
            this.rdGrpYear.AutoSize = true;
            this.rdGrpYear.Location = new System.Drawing.Point(8, 265);
            this.rdGrpYear.Name = "rdGrpYear";
            this.rdGrpYear.Size = new System.Drawing.Size(47, 17);
            this.rdGrpYear.TabIndex = 10;
            this.rdGrpYear.Text = "Year";
            this.rdGrpYear.UseVisualStyleBackColor = true;
            // 
            // rdGrpMonth
            // 
            this.rdGrpMonth.AutoSize = true;
            this.rdGrpMonth.Location = new System.Drawing.Point(8, 245);
            this.rdGrpMonth.Name = "rdGrpMonth";
            this.rdGrpMonth.Size = new System.Drawing.Size(55, 17);
            this.rdGrpMonth.TabIndex = 9;
            this.rdGrpMonth.Text = "Month";
            this.rdGrpMonth.UseVisualStyleBackColor = true;
            // 
            // rdGrpMachine
            // 
            this.rdGrpMachine.AutoSize = true;
            this.rdGrpMachine.Location = new System.Drawing.Point(8, 285);
            this.rdGrpMachine.Name = "rdGrpMachine";
            this.rdGrpMachine.Size = new System.Drawing.Size(66, 17);
            this.rdGrpMachine.TabIndex = 8;
            this.rdGrpMachine.Text = "Machine";
            this.rdGrpMachine.UseVisualStyleBackColor = true;
            // 
            // rdGrpBillDate
            // 
            this.rdGrpBillDate.AutoSize = true;
            this.rdGrpBillDate.Location = new System.Drawing.Point(8, 225);
            this.rdGrpBillDate.Name = "rdGrpBillDate";
            this.rdGrpBillDate.Size = new System.Drawing.Size(61, 17);
            this.rdGrpBillDate.TabIndex = 7;
            this.rdGrpBillDate.Text = "BillDate";
            this.rdGrpBillDate.UseVisualStyleBackColor = true;
            // 
            // rdGrpBillNo
            // 
            this.rdGrpBillNo.AutoSize = true;
            this.rdGrpBillNo.Location = new System.Drawing.Point(8, 205);
            this.rdGrpBillNo.Name = "rdGrpBillNo";
            this.rdGrpBillNo.Size = new System.Drawing.Size(52, 17);
            this.rdGrpBillNo.TabIndex = 6;
            this.rdGrpBillNo.Text = "BillNo";
            this.rdGrpBillNo.UseVisualStyleBackColor = true;
            // 
            // rdGrpKot
            // 
            this.rdGrpKot.AutoSize = true;
            this.rdGrpKot.Location = new System.Drawing.Point(8, 185);
            this.rdGrpKot.Name = "rdGrpKot";
            this.rdGrpKot.Size = new System.Drawing.Size(47, 17);
            this.rdGrpKot.TabIndex = 5;
            this.rdGrpKot.Text = "KOT";
            this.rdGrpKot.UseVisualStyleBackColor = true;
            // 
            // rdGrpSteward
            // 
            this.rdGrpSteward.AutoSize = true;
            this.rdGrpSteward.Location = new System.Drawing.Point(8, 165);
            this.rdGrpSteward.Name = "rdGrpSteward";
            this.rdGrpSteward.Size = new System.Drawing.Size(64, 17);
            this.rdGrpSteward.TabIndex = 4;
            this.rdGrpSteward.Text = "Steward";
            this.rdGrpSteward.UseVisualStyleBackColor = true;
            // 
            // rdGrpTax
            // 
            this.rdGrpTax.AutoSize = true;
            this.rdGrpTax.Location = new System.Drawing.Point(8, 105);
            this.rdGrpTax.Name = "rdGrpTax";
            this.rdGrpTax.Size = new System.Drawing.Size(43, 17);
            this.rdGrpTax.TabIndex = 3;
            this.rdGrpTax.Text = "Tax";
            this.rdGrpTax.UseVisualStyleBackColor = true;
            // 
            // rdGrpItem
            // 
            this.rdGrpItem.AutoSize = true;
            this.rdGrpItem.Location = new System.Drawing.Point(8, 125);
            this.rdGrpItem.Name = "rdGrpItem";
            this.rdGrpItem.Size = new System.Drawing.Size(45, 17);
            this.rdGrpItem.TabIndex = 2;
            this.rdGrpItem.Text = "Item";
            this.rdGrpItem.UseVisualStyleBackColor = true;
            // 
            // rdGrpCategory
            // 
            this.rdGrpCategory.AutoSize = true;
            this.rdGrpCategory.Location = new System.Drawing.Point(8, 65);
            this.rdGrpCategory.Name = "rdGrpCategory";
            this.rdGrpCategory.Size = new System.Drawing.Size(67, 17);
            this.rdGrpCategory.TabIndex = 1;
            this.rdGrpCategory.Text = "Category";
            this.rdGrpCategory.UseVisualStyleBackColor = true;
            // 
            // rdGrpDepartment
            // 
            this.rdGrpDepartment.AutoSize = true;
            this.rdGrpDepartment.Location = new System.Drawing.Point(8, 45);
            this.rdGrpDepartment.Name = "rdGrpDepartment";
            this.rdGrpDepartment.Size = new System.Drawing.Size(80, 17);
            this.rdGrpDepartment.TabIndex = 0;
            this.rdGrpDepartment.Text = "Department";
            this.rdGrpDepartment.UseVisualStyleBackColor = true;
            // 
            // grpStockPoint
            // 
            this.grpStockPoint.Controls.Add(this.lstStockpoint);
            this.grpStockPoint.Controls.Add(this.rdSelStockpoint);
            this.grpStockPoint.Controls.Add(this.rdAllStockpoint);
            this.grpStockPoint.Location = new System.Drawing.Point(307, 7);
            this.grpStockPoint.Name = "grpStockPoint";
            this.grpStockPoint.Size = new System.Drawing.Size(148, 178);
            this.grpStockPoint.TabIndex = 15;
            this.grpStockPoint.TabStop = false;
            this.grpStockPoint.Text = "Stock Point";
            // 
            // lstStockpoint
            // 
            this.lstStockpoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstStockpoint.CheckBoxes = true;
            this.lstStockpoint.Location = new System.Drawing.Point(6, 55);
            this.lstStockpoint.Name = "lstStockpoint";
            this.lstStockpoint.Size = new System.Drawing.Size(137, 116);
            this.lstStockpoint.TabIndex = 2;
            this.lstStockpoint.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelStockpoint
            // 
            this.rdSelStockpoint.AutoSize = true;
            this.rdSelStockpoint.Location = new System.Drawing.Point(11, 36);
            this.rdSelStockpoint.Name = "rdSelStockpoint";
            this.rdSelStockpoint.Size = new System.Drawing.Size(121, 17);
            this.rdSelStockpoint.TabIndex = 1;
            this.rdSelStockpoint.Text = "Selected Stockpoint";
            this.rdSelStockpoint.UseVisualStyleBackColor = true;
            // 
            // rdAllStockpoint
            // 
            this.rdAllStockpoint.AutoSize = true;
            this.rdAllStockpoint.Checked = true;
            this.rdAllStockpoint.Location = new System.Drawing.Point(11, 18);
            this.rdAllStockpoint.Name = "rdAllStockpoint";
            this.rdAllStockpoint.Size = new System.Drawing.Size(90, 17);
            this.rdAllStockpoint.TabIndex = 0;
            this.rdAllStockpoint.TabStop = true;
            this.rdAllStockpoint.Text = "All Stockpoint";
            this.rdAllStockpoint.UseVisualStyleBackColor = true;
            // 
            // grpSteward
            // 
            this.grpSteward.Controls.Add(this.lstSteward);
            this.grpSteward.Controls.Add(this.rdSelSteward);
            this.grpSteward.Controls.Add(this.rdAllSteward);
            this.grpSteward.Location = new System.Drawing.Point(308, 185);
            this.grpSteward.Name = "grpSteward";
            this.grpSteward.Size = new System.Drawing.Size(148, 178);
            this.grpSteward.TabIndex = 14;
            this.grpSteward.TabStop = false;
            this.grpSteward.Text = "Steward";
            // 
            // lstSteward
            // 
            this.lstSteward.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstSteward.CheckBoxes = true;
            this.lstSteward.Location = new System.Drawing.Point(6, 55);
            this.lstSteward.Name = "lstSteward";
            this.lstSteward.Size = new System.Drawing.Size(137, 116);
            this.lstSteward.TabIndex = 2;
            this.lstSteward.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelSteward
            // 
            this.rdSelSteward.AutoSize = true;
            this.rdSelSteward.Location = new System.Drawing.Point(11, 36);
            this.rdSelSteward.Name = "rdSelSteward";
            this.rdSelSteward.Size = new System.Drawing.Size(109, 17);
            this.rdSelSteward.TabIndex = 1;
            this.rdSelSteward.Text = "Selected Steward";
            this.rdSelSteward.UseVisualStyleBackColor = true;
            // 
            // rdAllSteward
            // 
            this.rdAllSteward.AutoSize = true;
            this.rdAllSteward.Checked = true;
            this.rdAllSteward.Location = new System.Drawing.Point(11, 18);
            this.rdAllSteward.Name = "rdAllSteward";
            this.rdAllSteward.Size = new System.Drawing.Size(78, 17);
            this.rdAllSteward.TabIndex = 0;
            this.rdAllSteward.TabStop = true;
            this.rdAllSteward.Text = "All Steward";
            this.rdAllSteward.UseVisualStyleBackColor = true;
            // 
            // grpSection
            // 
            this.grpSection.Controls.Add(this.lstSection);
            this.grpSection.Controls.Add(this.rdSelSection);
            this.grpSection.Controls.Add(this.rdAllSection);
            this.grpSection.Location = new System.Drawing.Point(157, 185);
            this.grpSection.Name = "grpSection";
            this.grpSection.Size = new System.Drawing.Size(148, 178);
            this.grpSection.TabIndex = 13;
            this.grpSection.TabStop = false;
            this.grpSection.Text = "Section";
            // 
            // lstSection
            // 
            this.lstSection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstSection.CheckBoxes = true;
            this.lstSection.Location = new System.Drawing.Point(6, 55);
            this.lstSection.Name = "lstSection";
            this.lstSection.Size = new System.Drawing.Size(137, 116);
            this.lstSection.TabIndex = 2;
            this.lstSection.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelSection
            // 
            this.rdSelSection.AutoSize = true;
            this.rdSelSection.Location = new System.Drawing.Point(11, 36);
            this.rdSelSection.Name = "rdSelSection";
            this.rdSelSection.Size = new System.Drawing.Size(106, 17);
            this.rdSelSection.TabIndex = 1;
            this.rdSelSection.Text = "Selected Section";
            this.rdSelSection.UseVisualStyleBackColor = true;
            // 
            // rdAllSection
            // 
            this.rdAllSection.AutoSize = true;
            this.rdAllSection.Checked = true;
            this.rdAllSection.Location = new System.Drawing.Point(11, 18);
            this.rdAllSection.Name = "rdAllSection";
            this.rdAllSection.Size = new System.Drawing.Size(75, 17);
            this.rdAllSection.TabIndex = 0;
            this.rdAllSection.TabStop = true;
            this.rdAllSection.Text = "All Section";
            this.rdAllSection.UseVisualStyleBackColor = true;
            // 
            // grpMachine
            // 
            this.grpMachine.Controls.Add(this.lstMachine);
            this.grpMachine.Controls.Add(this.rdSelMachine);
            this.grpMachine.Controls.Add(this.rdAllMachine);
            this.grpMachine.Location = new System.Drawing.Point(6, 185);
            this.grpMachine.Name = "grpMachine";
            this.grpMachine.Size = new System.Drawing.Size(148, 178);
            this.grpMachine.TabIndex = 12;
            this.grpMachine.TabStop = false;
            this.grpMachine.Text = "Machine";
            // 
            // lstMachine
            // 
            this.lstMachine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstMachine.CheckBoxes = true;
            this.lstMachine.Location = new System.Drawing.Point(6, 55);
            this.lstMachine.Name = "lstMachine";
            this.lstMachine.Size = new System.Drawing.Size(137, 116);
            this.lstMachine.TabIndex = 2;
            this.lstMachine.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelMachine
            // 
            this.rdSelMachine.AutoSize = true;
            this.rdSelMachine.Location = new System.Drawing.Point(9, 36);
            this.rdSelMachine.Name = "rdSelMachine";
            this.rdSelMachine.Size = new System.Drawing.Size(111, 17);
            this.rdSelMachine.TabIndex = 1;
            this.rdSelMachine.Text = "Selected Machine";
            this.rdSelMachine.UseVisualStyleBackColor = true;
            // 
            // rdAllMachine
            // 
            this.rdAllMachine.AutoSize = true;
            this.rdAllMachine.Checked = true;
            this.rdAllMachine.Location = new System.Drawing.Point(9, 18);
            this.rdAllMachine.Name = "rdAllMachine";
            this.rdAllMachine.Size = new System.Drawing.Size(80, 17);
            this.rdAllMachine.TabIndex = 0;
            this.rdAllMachine.TabStop = true;
            this.rdAllMachine.Text = "All Machine";
            this.rdAllMachine.UseVisualStyleBackColor = true;
            // 
            // grpTax
            // 
            this.grpTax.Controls.Add(this.lstTax);
            this.grpTax.Controls.Add(this.rdSelTax);
            this.grpTax.Controls.Add(this.rdAllTax);
            this.grpTax.Location = new System.Drawing.Point(610, 7);
            this.grpTax.Name = "grpTax";
            this.grpTax.Size = new System.Drawing.Size(148, 178);
            this.grpTax.TabIndex = 11;
            this.grpTax.TabStop = false;
            this.grpTax.Text = "Tax";
            // 
            // lstTax
            // 
            this.lstTax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstTax.CheckBoxes = true;
            this.lstTax.Location = new System.Drawing.Point(6, 55);
            this.lstTax.Name = "lstTax";
            this.lstTax.Size = new System.Drawing.Size(137, 116);
            this.lstTax.TabIndex = 2;
            this.lstTax.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelTax
            // 
            this.rdSelTax.AutoSize = true;
            this.rdSelTax.Location = new System.Drawing.Point(11, 36);
            this.rdSelTax.Name = "rdSelTax";
            this.rdSelTax.Size = new System.Drawing.Size(99, 17);
            this.rdSelTax.TabIndex = 1;
            this.rdSelTax.Text = "Selected Taxes";
            this.rdSelTax.UseVisualStyleBackColor = true;
            // 
            // rdAllTax
            // 
            this.rdAllTax.AutoSize = true;
            this.rdAllTax.Checked = true;
            this.rdAllTax.Location = new System.Drawing.Point(11, 18);
            this.rdAllTax.Name = "rdAllTax";
            this.rdAllTax.Size = new System.Drawing.Size(68, 17);
            this.rdAllTax.TabIndex = 0;
            this.rdAllTax.TabStop = true;
            this.rdAllTax.Text = "All Taxes";
            this.rdAllTax.UseVisualStyleBackColor = true;
            // 
            // grpPayments
            // 
            this.grpPayments.Controls.Add(this.lstPayments);
            this.grpPayments.Controls.Add(this.rdSelPayments);
            this.grpPayments.Controls.Add(this.rdAllPayments);
            this.grpPayments.Location = new System.Drawing.Point(459, 7);
            this.grpPayments.Name = "grpPayments";
            this.grpPayments.Size = new System.Drawing.Size(148, 178);
            this.grpPayments.TabIndex = 10;
            this.grpPayments.TabStop = false;
            this.grpPayments.Text = "Payments";
            // 
            // lstPayments
            // 
            this.lstPayments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstPayments.CheckBoxes = true;
            this.lstPayments.Location = new System.Drawing.Point(6, 54);
            this.lstPayments.Name = "lstPayments";
            this.lstPayments.Size = new System.Drawing.Size(137, 116);
            this.lstPayments.TabIndex = 2;
            this.lstPayments.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelPayments
            // 
            this.rdSelPayments.AutoSize = true;
            this.rdSelPayments.Location = new System.Drawing.Point(11, 35);
            this.rdSelPayments.Name = "rdSelPayments";
            this.rdSelPayments.Size = new System.Drawing.Size(116, 17);
            this.rdSelPayments.TabIndex = 1;
            this.rdSelPayments.Text = "Selected Payments";
            this.rdSelPayments.UseVisualStyleBackColor = true;
            // 
            // rdAllPayments
            // 
            this.rdAllPayments.AutoSize = true;
            this.rdAllPayments.Checked = true;
            this.rdAllPayments.Location = new System.Drawing.Point(11, 17);
            this.rdAllPayments.Name = "rdAllPayments";
            this.rdAllPayments.Size = new System.Drawing.Size(85, 17);
            this.rdAllPayments.TabIndex = 0;
            this.rdAllPayments.TabStop = true;
            this.rdAllPayments.Text = "All Payments";
            this.rdAllPayments.UseVisualStyleBackColor = true;
            // 
            // grpItems
            // 
            this.grpItems.Controls.Add(this.lstItem);
            this.grpItems.Controls.Add(this.rdSelItem);
            this.grpItems.Controls.Add(this.rdAllItem);
            this.grpItems.Location = new System.Drawing.Point(459, 185);
            this.grpItems.Name = "grpItems";
            this.grpItems.Size = new System.Drawing.Size(293, 178);
            this.grpItems.TabIndex = 4;
            this.grpItems.TabStop = false;
            this.grpItems.Text = "Items";
            // 
            // lstItem
            // 
            this.lstItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstItem.CheckBoxes = true;
            this.lstItem.Location = new System.Drawing.Point(6, 55);
            this.lstItem.Name = "lstItem";
            this.lstItem.Size = new System.Drawing.Size(281, 116);
            this.lstItem.TabIndex = 2;
            this.lstItem.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelItem
            // 
            this.rdSelItem.AutoSize = true;
            this.rdSelItem.Location = new System.Drawing.Point(9, 36);
            this.rdSelItem.Name = "rdSelItem";
            this.rdSelItem.Size = new System.Drawing.Size(95, 17);
            this.rdSelItem.TabIndex = 1;
            this.rdSelItem.Text = "Selected Items";
            this.rdSelItem.UseVisualStyleBackColor = true;
            // 
            // rdAllItem
            // 
            this.rdAllItem.AutoSize = true;
            this.rdAllItem.Checked = true;
            this.rdAllItem.Location = new System.Drawing.Point(9, 18);
            this.rdAllItem.Name = "rdAllItem";
            this.rdAllItem.Size = new System.Drawing.Size(64, 17);
            this.rdAllItem.TabIndex = 0;
            this.rdAllItem.TabStop = true;
            this.rdAllItem.Text = "All Items";
            this.rdAllItem.UseVisualStyleBackColor = true;
            // 
            // grpCategory
            // 
            this.grpCategory.Controls.Add(this.lstCategory);
            this.grpCategory.Controls.Add(this.rdSelCatg);
            this.grpCategory.Controls.Add(this.rdAllCatg);
            this.grpCategory.Location = new System.Drawing.Point(157, 7);
            this.grpCategory.Name = "grpCategory";
            this.grpCategory.Size = new System.Drawing.Size(148, 178);
            this.grpCategory.TabIndex = 3;
            this.grpCategory.TabStop = false;
            this.grpCategory.Text = "Category";
            // 
            // lstCategory
            // 
            this.lstCategory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstCategory.CheckBoxes = true;
            this.lstCategory.Location = new System.Drawing.Point(6, 55);
            this.lstCategory.Name = "lstCategory";
            this.lstCategory.Size = new System.Drawing.Size(137, 116);
            this.lstCategory.TabIndex = 2;
            this.lstCategory.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelCatg
            // 
            this.rdSelCatg.AutoSize = true;
            this.rdSelCatg.Location = new System.Drawing.Point(6, 36);
            this.rdSelCatg.Name = "rdSelCatg";
            this.rdSelCatg.Size = new System.Drawing.Size(120, 17);
            this.rdSelCatg.TabIndex = 1;
            this.rdSelCatg.Text = "Selected Categories";
            this.rdSelCatg.UseVisualStyleBackColor = true;
            // 
            // rdAllCatg
            // 
            this.rdAllCatg.AutoSize = true;
            this.rdAllCatg.Checked = true;
            this.rdAllCatg.Location = new System.Drawing.Point(6, 18);
            this.rdAllCatg.Name = "rdAllCatg";
            this.rdAllCatg.Size = new System.Drawing.Size(89, 17);
            this.rdAllCatg.TabIndex = 0;
            this.rdAllCatg.TabStop = true;
            this.rdAllCatg.Text = "All Categories";
            this.rdAllCatg.UseVisualStyleBackColor = true;
            // 
            // grpDepartment
            // 
            this.grpDepartment.Controls.Add(this.lstDepartment);
            this.grpDepartment.Controls.Add(this.rdSelDept);
            this.grpDepartment.Controls.Add(this.rdAllDept);
            this.grpDepartment.Location = new System.Drawing.Point(6, 7);
            this.grpDepartment.Name = "grpDepartment";
            this.grpDepartment.Size = new System.Drawing.Size(148, 178);
            this.grpDepartment.TabIndex = 1;
            this.grpDepartment.TabStop = false;
            this.grpDepartment.Text = "Department";
            // 
            // lstDepartment
            // 
            this.lstDepartment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstDepartment.CheckBoxes = true;
            this.lstDepartment.Location = new System.Drawing.Point(6, 55);
            this.lstDepartment.Name = "lstDepartment";
            this.lstDepartment.Size = new System.Drawing.Size(137, 116);
            this.lstDepartment.TabIndex = 2;
            this.lstDepartment.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelDept
            // 
            this.rdSelDept.AutoSize = true;
            this.rdSelDept.Location = new System.Drawing.Point(6, 36);
            this.rdSelDept.Name = "rdSelDept";
            this.rdSelDept.Size = new System.Drawing.Size(125, 17);
            this.rdSelDept.TabIndex = 1;
            this.rdSelDept.Text = "Selected Department";
            this.rdSelDept.UseVisualStyleBackColor = true;
            // 
            // rdAllDept
            // 
            this.rdAllDept.AutoSize = true;
            this.rdAllDept.Checked = true;
            this.rdAllDept.Location = new System.Drawing.Point(6, 18);
            this.rdAllDept.Name = "rdAllDept";
            this.rdAllDept.Size = new System.Drawing.Size(99, 17);
            this.rdAllDept.TabIndex = 0;
            this.rdAllDept.TabStop = true;
            this.rdAllDept.Text = "All Departments";
            this.rdAllDept.UseVisualStyleBackColor = true;
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(489, 462);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 27);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(392, 462);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(76, 27);
            this.btnPrint.TabIndex = 3;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnGrid
            // 
            this.btnGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrid.Location = new System.Drawing.Point(295, 462);
            this.btnGrid.Name = "btnGrid";
            this.btnGrid.Size = new System.Drawing.Size(76, 27);
            this.btnGrid.TabIndex = 5;
            this.btnGrid.Text = "&Grid";
            this.btnGrid.UseVisualStyleBackColor = true;
            this.btnGrid.Click += new System.EventHandler(this.btnGrid_Click);
            // 
            // frmRptSalesSummary2
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(956, 501);
            this.Controls.Add(this.btnGrid);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.pnlSaleSaummary);
            this.Name = "frmRptSalesSummary2";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sales Summary";
            this.Load += new System.EventHandler(this.frmRptSalesSummary2_Load);
            this.pnlSaleSaummary.ResumeLayout(false);
            this.grpCondition.ResumeLayout(false);
            this.grpCondition.PerformLayout();
            this.grpDisplay.ResumeLayout(false);
            this.grpDisplay.PerformLayout();
            this.grpGroup.ResumeLayout(false);
            this.grpGroup.PerformLayout();
            this.grpStockPoint.ResumeLayout(false);
            this.grpStockPoint.PerformLayout();
            this.grpSteward.ResumeLayout(false);
            this.grpSteward.PerformLayout();
            this.grpSection.ResumeLayout(false);
            this.grpSection.PerformLayout();
            this.grpMachine.ResumeLayout(false);
            this.grpMachine.PerformLayout();
            this.grpTax.ResumeLayout(false);
            this.grpTax.PerformLayout();
            this.grpPayments.ResumeLayout(false);
            this.grpPayments.PerformLayout();
            this.grpItems.ResumeLayout(false);
            this.grpItems.PerformLayout();
            this.grpCategory.ResumeLayout(false);
            this.grpCategory.PerformLayout();
            this.grpDepartment.ResumeLayout(false);
            this.grpDepartment.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}