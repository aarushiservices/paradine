﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.ReportForms
{
    partial class frmRptStockEntryBillsSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExit = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.pnlPO = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbPrintOn = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.DtpToInvDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.DtpFrInvDate = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.DtpToStockEntryDate = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.DtpFrStockEntryDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbSupplier = new System.Windows.Forms.ComboBox();
            this.txtStkEntryNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlPO.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(174, 245);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 28);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(80, 245);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(76, 28);
            this.btnPrint.TabIndex = 4;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // pnlPO
            // 
            this.pnlPO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPO.Controls.Add(this.label7);
            this.pnlPO.Controls.Add(this.cmbPrintOn);
            this.pnlPO.Controls.Add(this.groupBox2);
            this.pnlPO.Controls.Add(this.groupBox1);
            this.pnlPO.Controls.Add(this.label2);
            this.pnlPO.Controls.Add(this.cmbSupplier);
            this.pnlPO.Controls.Add(this.txtStkEntryNo);
            this.pnlPO.Controls.Add(this.label1);
            this.pnlPO.Location = new System.Drawing.Point(3, 3);
            this.pnlPO.Name = "pnlPO";
            this.pnlPO.Size = new System.Drawing.Size(325, 236);
            this.pnlPO.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(70, 203);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Print On :";
            // 
            // cmbPrintOn
            // 
            this.cmbPrintOn.BackColor = System.Drawing.Color.White;
            this.cmbPrintOn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPrintOn.FormattingEnabled = true;
            this.cmbPrintOn.Location = new System.Drawing.Point(127, 199);
            this.cmbPrintOn.Name = "cmbPrintOn";
            this.cmbPrintOn.Size = new System.Drawing.Size(126, 21);
            this.cmbPrintOn.TabIndex = 4;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.DtpToInvDate);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.DtpFrInvDate);
            this.groupBox2.Location = new System.Drawing.Point(9, 135);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(305, 55);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Invoice Date :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(161, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "To";
            // 
            // DtpToInvDate
            // 
            this.DtpToInvDate.Checked = false;
            this.DtpToInvDate.CustomFormat = "dd/MMM/yyyy";
            this.DtpToInvDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpToInvDate.Location = new System.Drawing.Point(187, 19);
            this.DtpToInvDate.Name = "DtpToInvDate";
            this.DtpToInvDate.ShowCheckBox = true;
            this.DtpToInvDate.Size = new System.Drawing.Size(101, 20);
            this.DtpToInvDate.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "From";
            // 
            // DtpFrInvDate
            // 
            this.DtpFrInvDate.Checked = false;
            this.DtpFrInvDate.CustomFormat = "dd/MMM/yyyy";
            this.DtpFrInvDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpFrInvDate.Location = new System.Drawing.Point(49, 19);
            this.DtpFrInvDate.Name = "DtpFrInvDate";
            this.DtpFrInvDate.ShowCheckBox = true;
            this.DtpFrInvDate.Size = new System.Drawing.Size(101, 20);
            this.DtpFrInvDate.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.DtpToStockEntryDate);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.DtpFrStockEntryDate);
            this.groupBox1.Location = new System.Drawing.Point(9, 74);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(305, 55);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Stock Entry Date :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(162, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "To";
            // 
            // DtpToStockEntryDate
            // 
            this.DtpToStockEntryDate.Checked = false;
            this.DtpToStockEntryDate.CustomFormat = "dd/MMM/yyyy";
            this.DtpToStockEntryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpToStockEntryDate.Location = new System.Drawing.Point(188, 19);
            this.DtpToStockEntryDate.Name = "DtpToStockEntryDate";
            this.DtpToStockEntryDate.ShowCheckBox = true;
            this.DtpToStockEntryDate.Size = new System.Drawing.Size(101, 20);
            this.DtpToStockEntryDate.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "From";
            // 
            // DtpFrStockEntryDate
            // 
            this.DtpFrStockEntryDate.Checked = false;
            this.DtpFrStockEntryDate.CustomFormat = "dd/MMM/yyyy";
            this.DtpFrStockEntryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpFrStockEntryDate.Location = new System.Drawing.Point(50, 19);
            this.DtpFrStockEntryDate.Name = "DtpFrStockEntryDate";
            this.DtpFrStockEntryDate.ShowCheckBox = true;
            this.DtpFrStockEntryDate.Size = new System.Drawing.Size(101, 20);
            this.DtpFrStockEntryDate.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Supplier :";
            // 
            // cmbSupplier
            // 
            this.cmbSupplier.BackColor = System.Drawing.Color.White;
            this.cmbSupplier.FormattingEnabled = true;
            this.cmbSupplier.Location = new System.Drawing.Point(98, 41);
            this.cmbSupplier.Name = "cmbSupplier";
            this.cmbSupplier.Size = new System.Drawing.Size(197, 21);
            this.cmbSupplier.TabIndex = 1;
            // 
            // txtStkEntryNo
            // 
            this.txtStkEntryNo.BackColor = System.Drawing.Color.White;
            this.txtStkEntryNo.Location = new System.Drawing.Point(98, 8);
            this.txtStkEntryNo.Name = "txtStkEntryNo";
            this.txtStkEntryNo.Size = new System.Drawing.Size(93, 20);
            this.txtStkEntryNo.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Stock Entry No :";
            // 
            // frmRptStockEntryBillsSummary
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(331, 274);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.pnlPO);
            this.Name = "frmRptStockEntryBillsSummary";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stock Entry Bills Summary :";
            this.Load += new System.EventHandler(this.frmRptStockEntryBillsSummary_Load);
            this.pnlPO.ResumeLayout(false);
            this.pnlPO.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}