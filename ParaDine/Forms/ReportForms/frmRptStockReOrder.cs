﻿using ParaDine.Forms.GlobalForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmRptStockReOrder : Form
    {
        private Button btnExit;
        private Button btnPrint;
        private ComboBox cmbPrintOn;
        //private IContainer components = null;
        private DataSet DS = new DataSet();
        private Label label1;
        private Label label7;
        private Panel pnlPO;
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private string StrSql = "";
        private TextBox txtItemCode;

        public frmRptStockReOrder()
        {
            this.InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                this.StrSql = "SELECT  * FROM RES_VW_WHSTOCKREORDER WHERE ITEMCODE <> ''";
                if (this.txtItemCode.Text != "")
                {
                    this.StrSql = this.StrSql + " AND ITEMCODE = '" + this.txtItemCode.Text.Trim() + "'";
                }
                GlobalFill.FillDataSet(this.StrSql, "REORDER", this.DS, this.SDA);
                if (this.DS.Tables["REORDER"].Rows.Count > 0)
                {
                    if (this.cmbPrintOn.Text.Trim() == "SCREEN")
                    {
                        ///By Bharat we have to change Reports to itextsparp pdf
                        ///
                        //RptReOrder rpt = new RptReOrder();
                        //rpt.SetDataSource(this.DS.Tables["REORDER"]);
                        //rpt.Refresh();
                        //new FrmRptViewer(rpt).ShowDialog();
                    }
                }
                else
                {
                    MessageBox.Show("DATA NOT FOUND!");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Print Click");
            }
        }

      

        private void frmRptStockReOrder_Load(object sender, EventArgs e)
        {
            try
            {
                new GlobalTheme().applyTheme(this);
                this.RefreshData();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Loading");
            }
        }

      

        private void RefreshData()
        {
            this.cmbPrintOn.Items.Add("SCREEN");
            this.cmbPrintOn.Items.Add("PRINTER");
            this.cmbPrintOn.Items.Add("DISK");
            this.cmbPrintOn.SelectedIndex = 0;
        }
    }
}
