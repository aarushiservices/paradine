﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.ReportForms
{
    partial class frmRptPOBillsSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlPO = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbPrintOn = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.DtpToDeliveryDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.DtpFrDeliveryDate = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.DtpToPODate = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.DtpFrPODate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbSupplier = new System.Windows.Forms.ComboBox();
            this.txtPONo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.chkPndgPO = new System.Windows.Forms.CheckBox();
            this.pnlPO.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlPO
            // 
            this.pnlPO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPO.Controls.Add(this.label7);
            this.pnlPO.Controls.Add(this.cmbPrintOn);
            this.pnlPO.Controls.Add(this.groupBox2);
            this.pnlPO.Controls.Add(this.groupBox1);
            this.pnlPO.Controls.Add(this.label2);
            this.pnlPO.Controls.Add(this.cmbSupplier);
            this.pnlPO.Controls.Add(this.txtPONo);
            this.pnlPO.Controls.Add(this.label1);
            this.pnlPO.Location = new System.Drawing.Point(4, 3);
            this.pnlPO.Name = "pnlPO";
            this.pnlPO.Size = new System.Drawing.Size(330, 208);
            this.pnlPO.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 176);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Print On :";
            // 
            // cmbPrintOn
            // 
            this.cmbPrintOn.BackColor = System.Drawing.Color.White;
            this.cmbPrintOn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPrintOn.FormattingEnabled = true;
            this.cmbPrintOn.Location = new System.Drawing.Point(87, 173);
            this.cmbPrintOn.Name = "cmbPrintOn";
            this.cmbPrintOn.Size = new System.Drawing.Size(126, 21);
            this.cmbPrintOn.TabIndex = 4;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.DtpToDeliveryDate);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.DtpFrDeliveryDate);
            this.groupBox2.Location = new System.Drawing.Point(12, 117);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(305, 50);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Delivery Date";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(162, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "To";
            // 
            // DtpToDeliveryDate
            // 
            this.DtpToDeliveryDate.Checked = false;
            this.DtpToDeliveryDate.CustomFormat = "dd/MMM/yyyy";
            this.DtpToDeliveryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpToDeliveryDate.Location = new System.Drawing.Point(187, 17);
            this.DtpToDeliveryDate.Name = "DtpToDeliveryDate";
            this.DtpToDeliveryDate.ShowCheckBox = true;
            this.DtpToDeliveryDate.Size = new System.Drawing.Size(104, 20);
            this.DtpToDeliveryDate.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "From";
            // 
            // DtpFrDeliveryDate
            // 
            this.DtpFrDeliveryDate.Checked = false;
            this.DtpFrDeliveryDate.CustomFormat = "dd/MMM/yyyy";
            this.DtpFrDeliveryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpFrDeliveryDate.Location = new System.Drawing.Point(49, 17);
            this.DtpFrDeliveryDate.Name = "DtpFrDeliveryDate";
            this.DtpFrDeliveryDate.ShowCheckBox = true;
            this.DtpFrDeliveryDate.Size = new System.Drawing.Size(104, 20);
            this.DtpFrDeliveryDate.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.DtpToPODate);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.DtpFrPODate);
            this.groupBox1.Location = new System.Drawing.Point(12, 66);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(305, 50);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "PO Date :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(162, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "To";
            // 
            // DtpToPODate
            // 
            this.DtpToPODate.Checked = false;
            this.DtpToPODate.CustomFormat = "dd/MMM/yyyy";
            this.DtpToPODate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpToPODate.Location = new System.Drawing.Point(187, 15);
            this.DtpToPODate.Name = "DtpToPODate";
            this.DtpToPODate.ShowCheckBox = true;
            this.DtpToPODate.Size = new System.Drawing.Size(104, 20);
            this.DtpToPODate.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "From";
            // 
            // DtpFrPODate
            // 
            this.DtpFrPODate.Checked = false;
            this.DtpFrPODate.CustomFormat = "dd/MMM/yyyy";
            this.DtpFrPODate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpFrPODate.Location = new System.Drawing.Point(49, 15);
            this.DtpFrPODate.Name = "DtpFrPODate";
            this.DtpFrPODate.ShowCheckBox = true;
            this.DtpFrPODate.Size = new System.Drawing.Size(104, 20);
            this.DtpFrPODate.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Supplier :";
            // 
            // cmbSupplier
            // 
            this.cmbSupplier.BackColor = System.Drawing.Color.White;
            this.cmbSupplier.FormattingEnabled = true;
            this.cmbSupplier.Location = new System.Drawing.Point(87, 38);
            this.cmbSupplier.Name = "cmbSupplier";
            this.cmbSupplier.Size = new System.Drawing.Size(211, 21);
            this.cmbSupplier.TabIndex = 1;
            // 
            // txtPONo
            // 
            this.txtPONo.BackColor = System.Drawing.Color.White;
            this.txtPONo.Location = new System.Drawing.Point(86, 10);
            this.txtPONo.Name = "txtPONo";
            this.txtPONo.Size = new System.Drawing.Size(74, 20);
            this.txtPONo.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "PO Number :";
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(171, 220);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 23);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(89, 220);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(76, 23);
            this.btnPrint.TabIndex = 1;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // chkPndgPO
            // 
            this.chkPndgPO.AutoSize = true;
            this.chkPndgPO.Location = new System.Drawing.Point(170, 17);
            this.chkPndgPO.Name = "chkPndgPO";
            this.chkPndgPO.Size = new System.Drawing.Size(147, 17);
            this.chkPndgPO.TabIndex = 13;
            this.chkPndgPO.Text = "Display Only Pending Bills";
            this.chkPndgPO.UseVisualStyleBackColor = true;
            // 
            // frmRptPOBillsSummary
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(336, 251);
            this.Controls.Add(this.chkPndgPO);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.pnlPO);
            this.Name = "frmRptPOBillsSummary";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PO Bills Summary :";
            this.Load += new System.EventHandler(this.frmRptPOBillsSummary_Load);
            this.pnlPO.ResumeLayout(false);
            this.pnlPO.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}