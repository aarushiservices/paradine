﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.ReportForms
{
    partial class frmRptStockEntryItemsSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpEntryDateTo = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpEntryDateFrom = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.DtpToInvoiceDate = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.dtpFrInvoiceDate = new System.Windows.Forms.DateTimePicker();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.pnlSalesSummary = new System.Windows.Forms.Panel();
            this.grpDisplay = new System.Windows.Forms.GroupBox();
            this.rdDispInvoiceDate = new System.Windows.Forms.RadioButton();
            this.rdDispInvoiceNo = new System.Windows.Forms.RadioButton();
            this.rdDispSupplier = new System.Windows.Forms.RadioButton();
            this.rdDispStockpoint = new System.Windows.Forms.RadioButton();
            this.rdDispSection = new System.Windows.Forms.RadioButton();
            this.rdDispCostprice = new System.Windows.Forms.RadioButton();
            this.rdDispYear = new System.Windows.Forms.RadioButton();
            this.rdDispMonth = new System.Windows.Forms.RadioButton();
            this.rdDispEntryDate = new System.Windows.Forms.RadioButton();
            this.rdDispEntryNo = new System.Windows.Forms.RadioButton();
            this.rdDispItem = new System.Windows.Forms.RadioButton();
            this.rdDispCategory = new System.Windows.Forms.RadioButton();
            this.rdDispDepartment = new System.Windows.Forms.RadioButton();
            this.grpGroup = new System.Windows.Forms.GroupBox();
            this.rdGrpInvoiceDate = new System.Windows.Forms.RadioButton();
            this.rdGrpInvoiceNo = new System.Windows.Forms.RadioButton();
            this.rdGrpSupplier = new System.Windows.Forms.RadioButton();
            this.rdGrpStockpoint = new System.Windows.Forms.RadioButton();
            this.rdGrpSection = new System.Windows.Forms.RadioButton();
            this.rdGrpCostPrice = new System.Windows.Forms.RadioButton();
            this.rdGrpYear = new System.Windows.Forms.RadioButton();
            this.rdGrpMonth = new System.Windows.Forms.RadioButton();
            this.rdGrpEntryDate = new System.Windows.Forms.RadioButton();
            this.rdGrpEntryNo = new System.Windows.Forms.RadioButton();
            this.rdGrpItem = new System.Windows.Forms.RadioButton();
            this.rdGrpCategory = new System.Windows.Forms.RadioButton();
            this.rdGrpDepartment = new System.Windows.Forms.RadioButton();
            this.grpSupplier = new System.Windows.Forms.GroupBox();
            this.lstSupplier = new System.Windows.Forms.ListView();
            this.rdSelSupplier = new System.Windows.Forms.RadioButton();
            this.rdAllSupplier = new System.Windows.Forms.RadioButton();
            this.grpStockPoint = new System.Windows.Forms.GroupBox();
            this.lstStockpoint = new System.Windows.Forms.ListView();
            this.rdSelStockpoint = new System.Windows.Forms.RadioButton();
            this.rdAllStockpoint = new System.Windows.Forms.RadioButton();
            this.grpSection = new System.Windows.Forms.GroupBox();
            this.lstSection = new System.Windows.Forms.ListView();
            this.rdSelSection = new System.Windows.Forms.RadioButton();
            this.rdAllSection = new System.Windows.Forms.RadioButton();
            this.grpItems = new System.Windows.Forms.GroupBox();
            this.lstItem = new System.Windows.Forms.ListView();
            this.rdSelItem = new System.Windows.Forms.RadioButton();
            this.rdAllItem = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chkCPNotReq = new System.Windows.Forms.CheckBox();
            this.chkUOMNotReq = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtInvoiceNo = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtToCode = new System.Windows.Forms.TextBox();
            this.txtFrCode = new System.Windows.Forms.TextBox();
            this.grpCategory = new System.Windows.Forms.GroupBox();
            this.lstCategory = new System.Windows.Forms.ListView();
            this.rdSelCatg = new System.Windows.Forms.RadioButton();
            this.rdAllCatg = new System.Windows.Forms.RadioButton();
            this.grpDepartment = new System.Windows.Forms.GroupBox();
            this.lstDepartment = new System.Windows.Forms.ListView();
            this.rdSelDept = new System.Windows.Forms.RadioButton();
            this.rdAllDept = new System.Windows.Forms.RadioButton();
            this.pnlSalesSummary.SuspendLayout();
            this.grpDisplay.SuspendLayout();
            this.grpGroup.SuspendLayout();
            this.grpSupplier.SuspendLayout();
            this.grpStockPoint.SuspendLayout();
            this.grpSection.SuspendLayout();
            this.grpItems.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.grpCategory.SuspendLayout();
            this.grpDepartment.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtpEntryDateTo
            // 
            this.dtpEntryDateTo.Checked = false;
            this.dtpEntryDateTo.CustomFormat = "dd/MMM/yyyy";
            this.dtpEntryDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEntryDateTo.Location = new System.Drawing.Point(198, 80);
            this.dtpEntryDateTo.Name = "dtpEntryDateTo";
            this.dtpEntryDateTo.ShowCheckBox = true;
            this.dtpEntryDateTo.Size = new System.Drawing.Size(104, 20);
            this.dtpEntryDateTo.TabIndex = 1;
            this.dtpEntryDateTo.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Stock Entry";
            // 
            // dtpEntryDateFrom
            // 
            this.dtpEntryDateFrom.Checked = false;
            this.dtpEntryDateFrom.CustomFormat = "dd/MMM/yyyy";
            this.dtpEntryDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEntryDateFrom.Location = new System.Drawing.Point(68, 80);
            this.dtpEntryDateFrom.Name = "dtpEntryDateFrom";
            this.dtpEntryDateFrom.ShowCheckBox = true;
            this.dtpEntryDateFrom.Size = new System.Drawing.Size(104, 20);
            this.dtpEntryDateFrom.TabIndex = 0;
            this.dtpEntryDateFrom.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(175, 84);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(20, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "To";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(176, 122);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(20, 13);
            this.label11.TabIndex = 8;
            this.label11.Text = "To";
            // 
            // DtpToInvoiceDate
            // 
            this.DtpToInvoiceDate.Checked = false;
            this.DtpToInvoiceDate.CustomFormat = "dd/MMM/yyyy";
            this.DtpToInvoiceDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpToInvoiceDate.Location = new System.Drawing.Point(198, 118);
            this.DtpToInvoiceDate.Name = "DtpToInvoiceDate";
            this.DtpToInvoiceDate.ShowCheckBox = true;
            this.DtpToInvoiceDate.Size = new System.Drawing.Size(104, 20);
            this.DtpToInvoiceDate.TabIndex = 1;
            this.DtpToInvoiceDate.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(26, 122);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "Invoice";
            // 
            // dtpFrInvoiceDate
            // 
            this.dtpFrInvoiceDate.Checked = false;
            this.dtpFrInvoiceDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpFrInvoiceDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFrInvoiceDate.Location = new System.Drawing.Point(70, 118);
            this.dtpFrInvoiceDate.Name = "dtpFrInvoiceDate";
            this.dtpFrInvoiceDate.ShowCheckBox = true;
            this.dtpFrInvoiceDate.Size = new System.Drawing.Size(104, 20);
            this.dtpFrInvoiceDate.TabIndex = 0;
            this.dtpFrInvoiceDate.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(455, 411);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 8;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(358, 411);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 7;
            this.btnPrint.Text = "&Show";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // pnlSalesSummary
            // 
            this.pnlSalesSummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSalesSummary.Controls.Add(this.grpDisplay);
            this.pnlSalesSummary.Controls.Add(this.grpGroup);
            this.pnlSalesSummary.Controls.Add(this.grpSupplier);
            this.pnlSalesSummary.Controls.Add(this.grpStockPoint);
            this.pnlSalesSummary.Controls.Add(this.grpSection);
            this.pnlSalesSummary.Controls.Add(this.grpItems);
            this.pnlSalesSummary.Controls.Add(this.groupBox3);
            this.pnlSalesSummary.Controls.Add(this.grpCategory);
            this.pnlSalesSummary.Controls.Add(this.grpDepartment);
            this.pnlSalesSummary.Location = new System.Drawing.Point(2, 2);
            this.pnlSalesSummary.Name = "pnlSalesSummary";
            this.pnlSalesSummary.Size = new System.Drawing.Size(884, 392);
            this.pnlSalesSummary.TabIndex = 6;
            // 
            // grpDisplay
            // 
            this.grpDisplay.Controls.Add(this.rdDispInvoiceDate);
            this.grpDisplay.Controls.Add(this.rdDispInvoiceNo);
            this.grpDisplay.Controls.Add(this.rdDispSupplier);
            this.grpDisplay.Controls.Add(this.rdDispStockpoint);
            this.grpDisplay.Controls.Add(this.rdDispSection);
            this.grpDisplay.Controls.Add(this.rdDispCostprice);
            this.grpDisplay.Controls.Add(this.rdDispYear);
            this.grpDisplay.Controls.Add(this.rdDispMonth);
            this.grpDisplay.Controls.Add(this.rdDispEntryDate);
            this.grpDisplay.Controls.Add(this.rdDispEntryNo);
            this.grpDisplay.Controls.Add(this.rdDispItem);
            this.grpDisplay.Controls.Add(this.rdDispCategory);
            this.grpDisplay.Controls.Add(this.rdDispDepartment);
            this.grpDisplay.Location = new System.Drawing.Point(783, 9);
            this.grpDisplay.Name = "grpDisplay";
            this.grpDisplay.Size = new System.Drawing.Size(91, 362);
            this.grpDisplay.TabIndex = 54;
            this.grpDisplay.TabStop = false;
            this.grpDisplay.Text = "Display";
            // 
            // rdDispInvoiceDate
            // 
            this.rdDispInvoiceDate.AutoSize = true;
            this.rdDispInvoiceDate.Location = new System.Drawing.Point(8, 277);
            this.rdDispInvoiceDate.Name = "rdDispInvoiceDate";
            this.rdDispInvoiceDate.Size = new System.Drawing.Size(83, 17);
            this.rdDispInvoiceDate.TabIndex = 32;
            this.rdDispInvoiceDate.Text = "InvoiceDate";
            this.rdDispInvoiceDate.UseVisualStyleBackColor = true;
            // 
            // rdDispInvoiceNo
            // 
            this.rdDispInvoiceNo.AutoSize = true;
            this.rdDispInvoiceNo.Location = new System.Drawing.Point(8, 252);
            this.rdDispInvoiceNo.Name = "rdDispInvoiceNo";
            this.rdDispInvoiceNo.Size = new System.Drawing.Size(74, 17);
            this.rdDispInvoiceNo.TabIndex = 31;
            this.rdDispInvoiceNo.Text = "InvoiceNo";
            this.rdDispInvoiceNo.UseVisualStyleBackColor = true;
            // 
            // rdDispSupplier
            // 
            this.rdDispSupplier.AutoSize = true;
            this.rdDispSupplier.Location = new System.Drawing.Point(8, 177);
            this.rdDispSupplier.Name = "rdDispSupplier";
            this.rdDispSupplier.Size = new System.Drawing.Size(63, 17);
            this.rdDispSupplier.TabIndex = 30;
            this.rdDispSupplier.Text = "Supplier";
            this.rdDispSupplier.UseVisualStyleBackColor = true;
            // 
            // rdDispStockpoint
            // 
            this.rdDispStockpoint.AutoSize = true;
            this.rdDispStockpoint.Location = new System.Drawing.Point(8, 27);
            this.rdDispStockpoint.Name = "rdDispStockpoint";
            this.rdDispStockpoint.Size = new System.Drawing.Size(77, 17);
            this.rdDispStockpoint.TabIndex = 29;
            this.rdDispStockpoint.Text = "StockPoint";
            this.rdDispStockpoint.UseVisualStyleBackColor = true;
            // 
            // rdDispSection
            // 
            this.rdDispSection.AutoSize = true;
            this.rdDispSection.Location = new System.Drawing.Point(8, 102);
            this.rdDispSection.Name = "rdDispSection";
            this.rdDispSection.Size = new System.Drawing.Size(61, 17);
            this.rdDispSection.TabIndex = 28;
            this.rdDispSection.Text = "Section";
            this.rdDispSection.UseVisualStyleBackColor = true;
            // 
            // rdDispCostprice
            // 
            this.rdDispCostprice.AutoSize = true;
            this.rdDispCostprice.Location = new System.Drawing.Point(8, 152);
            this.rdDispCostprice.Name = "rdDispCostprice";
            this.rdDispCostprice.Size = new System.Drawing.Size(70, 17);
            this.rdDispCostprice.TabIndex = 27;
            this.rdDispCostprice.Text = "CostPrice";
            this.rdDispCostprice.UseVisualStyleBackColor = true;
            // 
            // rdDispYear
            // 
            this.rdDispYear.AutoSize = true;
            this.rdDispYear.Location = new System.Drawing.Point(8, 327);
            this.rdDispYear.Name = "rdDispYear";
            this.rdDispYear.Size = new System.Drawing.Size(47, 17);
            this.rdDispYear.TabIndex = 25;
            this.rdDispYear.Text = "Year";
            this.rdDispYear.UseVisualStyleBackColor = true;
            // 
            // rdDispMonth
            // 
            this.rdDispMonth.AutoSize = true;
            this.rdDispMonth.Location = new System.Drawing.Point(8, 302);
            this.rdDispMonth.Name = "rdDispMonth";
            this.rdDispMonth.Size = new System.Drawing.Size(55, 17);
            this.rdDispMonth.TabIndex = 24;
            this.rdDispMonth.Text = "Month";
            this.rdDispMonth.UseVisualStyleBackColor = true;
            // 
            // rdDispEntryDate
            // 
            this.rdDispEntryDate.AutoSize = true;
            this.rdDispEntryDate.Location = new System.Drawing.Point(8, 227);
            this.rdDispEntryDate.Name = "rdDispEntryDate";
            this.rdDispEntryDate.Size = new System.Drawing.Size(72, 17);
            this.rdDispEntryDate.TabIndex = 22;
            this.rdDispEntryDate.Text = "EntryDate";
            this.rdDispEntryDate.UseVisualStyleBackColor = true;
            // 
            // rdDispEntryNo
            // 
            this.rdDispEntryNo.AutoSize = true;
            this.rdDispEntryNo.Location = new System.Drawing.Point(8, 202);
            this.rdDispEntryNo.Name = "rdDispEntryNo";
            this.rdDispEntryNo.Size = new System.Drawing.Size(63, 17);
            this.rdDispEntryNo.TabIndex = 21;
            this.rdDispEntryNo.Text = "EntryNo";
            this.rdDispEntryNo.UseVisualStyleBackColor = true;
            // 
            // rdDispItem
            // 
            this.rdDispItem.AutoSize = true;
            this.rdDispItem.Checked = true;
            this.rdDispItem.Location = new System.Drawing.Point(8, 127);
            this.rdDispItem.Name = "rdDispItem";
            this.rdDispItem.Size = new System.Drawing.Size(45, 17);
            this.rdDispItem.TabIndex = 18;
            this.rdDispItem.TabStop = true;
            this.rdDispItem.Text = "Item";
            this.rdDispItem.UseVisualStyleBackColor = true;
            // 
            // rdDispCategory
            // 
            this.rdDispCategory.AutoSize = true;
            this.rdDispCategory.Location = new System.Drawing.Point(8, 77);
            this.rdDispCategory.Name = "rdDispCategory";
            this.rdDispCategory.Size = new System.Drawing.Size(67, 17);
            this.rdDispCategory.TabIndex = 17;
            this.rdDispCategory.Text = "Category";
            this.rdDispCategory.UseVisualStyleBackColor = true;
            // 
            // rdDispDepartment
            // 
            this.rdDispDepartment.AutoSize = true;
            this.rdDispDepartment.Location = new System.Drawing.Point(8, 52);
            this.rdDispDepartment.Name = "rdDispDepartment";
            this.rdDispDepartment.Size = new System.Drawing.Size(80, 17);
            this.rdDispDepartment.TabIndex = 16;
            this.rdDispDepartment.Text = "Department";
            this.rdDispDepartment.UseVisualStyleBackColor = true;
            // 
            // grpGroup
            // 
            this.grpGroup.Controls.Add(this.rdGrpInvoiceDate);
            this.grpGroup.Controls.Add(this.rdGrpInvoiceNo);
            this.grpGroup.Controls.Add(this.rdGrpSupplier);
            this.grpGroup.Controls.Add(this.rdGrpStockpoint);
            this.grpGroup.Controls.Add(this.rdGrpSection);
            this.grpGroup.Controls.Add(this.rdGrpCostPrice);
            this.grpGroup.Controls.Add(this.rdGrpYear);
            this.grpGroup.Controls.Add(this.rdGrpMonth);
            this.grpGroup.Controls.Add(this.rdGrpEntryDate);
            this.grpGroup.Controls.Add(this.rdGrpEntryNo);
            this.grpGroup.Controls.Add(this.rdGrpItem);
            this.grpGroup.Controls.Add(this.rdGrpCategory);
            this.grpGroup.Controls.Add(this.rdGrpDepartment);
            this.grpGroup.Location = new System.Drawing.Point(686, 9);
            this.grpGroup.Name = "grpGroup";
            this.grpGroup.Size = new System.Drawing.Size(91, 362);
            this.grpGroup.TabIndex = 53;
            this.grpGroup.TabStop = false;
            this.grpGroup.Text = "Group";
            // 
            // rdGrpInvoiceDate
            // 
            this.rdGrpInvoiceDate.AutoSize = true;
            this.rdGrpInvoiceDate.Location = new System.Drawing.Point(8, 275);
            this.rdGrpInvoiceDate.Name = "rdGrpInvoiceDate";
            this.rdGrpInvoiceDate.Size = new System.Drawing.Size(83, 17);
            this.rdGrpInvoiceDate.TabIndex = 17;
            this.rdGrpInvoiceDate.Text = "InvoiceDate";
            this.rdGrpInvoiceDate.UseVisualStyleBackColor = true;
            // 
            // rdGrpInvoiceNo
            // 
            this.rdGrpInvoiceNo.AutoSize = true;
            this.rdGrpInvoiceNo.Location = new System.Drawing.Point(8, 250);
            this.rdGrpInvoiceNo.Name = "rdGrpInvoiceNo";
            this.rdGrpInvoiceNo.Size = new System.Drawing.Size(74, 17);
            this.rdGrpInvoiceNo.TabIndex = 16;
            this.rdGrpInvoiceNo.Text = "InvoiceNo";
            this.rdGrpInvoiceNo.UseVisualStyleBackColor = true;
            // 
            // rdGrpSupplier
            // 
            this.rdGrpSupplier.AutoSize = true;
            this.rdGrpSupplier.Location = new System.Drawing.Point(8, 175);
            this.rdGrpSupplier.Name = "rdGrpSupplier";
            this.rdGrpSupplier.Size = new System.Drawing.Size(63, 17);
            this.rdGrpSupplier.TabIndex = 15;
            this.rdGrpSupplier.Text = "Supplier";
            this.rdGrpSupplier.UseVisualStyleBackColor = true;
            // 
            // rdGrpStockpoint
            // 
            this.rdGrpStockpoint.AutoSize = true;
            this.rdGrpStockpoint.Checked = true;
            this.rdGrpStockpoint.Location = new System.Drawing.Point(8, 25);
            this.rdGrpStockpoint.Name = "rdGrpStockpoint";
            this.rdGrpStockpoint.Size = new System.Drawing.Size(77, 17);
            this.rdGrpStockpoint.TabIndex = 14;
            this.rdGrpStockpoint.TabStop = true;
            this.rdGrpStockpoint.Text = "StockPoint";
            this.rdGrpStockpoint.UseVisualStyleBackColor = true;
            // 
            // rdGrpSection
            // 
            this.rdGrpSection.AutoSize = true;
            this.rdGrpSection.Location = new System.Drawing.Point(8, 100);
            this.rdGrpSection.Name = "rdGrpSection";
            this.rdGrpSection.Size = new System.Drawing.Size(61, 17);
            this.rdGrpSection.TabIndex = 13;
            this.rdGrpSection.Text = "Section";
            this.rdGrpSection.UseVisualStyleBackColor = true;
            // 
            // rdGrpCostPrice
            // 
            this.rdGrpCostPrice.AutoSize = true;
            this.rdGrpCostPrice.Location = new System.Drawing.Point(8, 150);
            this.rdGrpCostPrice.Name = "rdGrpCostPrice";
            this.rdGrpCostPrice.Size = new System.Drawing.Size(70, 17);
            this.rdGrpCostPrice.TabIndex = 12;
            this.rdGrpCostPrice.Text = "CostPrice";
            this.rdGrpCostPrice.UseVisualStyleBackColor = true;
            // 
            // rdGrpYear
            // 
            this.rdGrpYear.AutoSize = true;
            this.rdGrpYear.Location = new System.Drawing.Point(8, 325);
            this.rdGrpYear.Name = "rdGrpYear";
            this.rdGrpYear.Size = new System.Drawing.Size(47, 17);
            this.rdGrpYear.TabIndex = 10;
            this.rdGrpYear.Text = "Year";
            this.rdGrpYear.UseVisualStyleBackColor = true;
            // 
            // rdGrpMonth
            // 
            this.rdGrpMonth.AutoSize = true;
            this.rdGrpMonth.Location = new System.Drawing.Point(8, 300);
            this.rdGrpMonth.Name = "rdGrpMonth";
            this.rdGrpMonth.Size = new System.Drawing.Size(55, 17);
            this.rdGrpMonth.TabIndex = 9;
            this.rdGrpMonth.Text = "Month";
            this.rdGrpMonth.UseVisualStyleBackColor = true;
            // 
            // rdGrpEntryDate
            // 
            this.rdGrpEntryDate.AutoSize = true;
            this.rdGrpEntryDate.Location = new System.Drawing.Point(8, 225);
            this.rdGrpEntryDate.Name = "rdGrpEntryDate";
            this.rdGrpEntryDate.Size = new System.Drawing.Size(72, 17);
            this.rdGrpEntryDate.TabIndex = 7;
            this.rdGrpEntryDate.Text = "EntryDate";
            this.rdGrpEntryDate.UseVisualStyleBackColor = true;
            // 
            // rdGrpEntryNo
            // 
            this.rdGrpEntryNo.AutoSize = true;
            this.rdGrpEntryNo.Location = new System.Drawing.Point(8, 200);
            this.rdGrpEntryNo.Name = "rdGrpEntryNo";
            this.rdGrpEntryNo.Size = new System.Drawing.Size(63, 17);
            this.rdGrpEntryNo.TabIndex = 6;
            this.rdGrpEntryNo.Text = "EntryNo";
            this.rdGrpEntryNo.UseVisualStyleBackColor = true;
            // 
            // rdGrpItem
            // 
            this.rdGrpItem.AutoSize = true;
            this.rdGrpItem.Location = new System.Drawing.Point(8, 125);
            this.rdGrpItem.Name = "rdGrpItem";
            this.rdGrpItem.Size = new System.Drawing.Size(45, 17);
            this.rdGrpItem.TabIndex = 2;
            this.rdGrpItem.Text = "Item";
            this.rdGrpItem.UseVisualStyleBackColor = true;
            // 
            // rdGrpCategory
            // 
            this.rdGrpCategory.AutoSize = true;
            this.rdGrpCategory.Location = new System.Drawing.Point(8, 75);
            this.rdGrpCategory.Name = "rdGrpCategory";
            this.rdGrpCategory.Size = new System.Drawing.Size(67, 17);
            this.rdGrpCategory.TabIndex = 1;
            this.rdGrpCategory.Text = "Category";
            this.rdGrpCategory.UseVisualStyleBackColor = true;
            // 
            // rdGrpDepartment
            // 
            this.rdGrpDepartment.AutoSize = true;
            this.rdGrpDepartment.Location = new System.Drawing.Point(8, 50);
            this.rdGrpDepartment.Name = "rdGrpDepartment";
            this.rdGrpDepartment.Size = new System.Drawing.Size(80, 17);
            this.rdGrpDepartment.TabIndex = 0;
            this.rdGrpDepartment.Text = "Department";
            this.rdGrpDepartment.UseVisualStyleBackColor = true;
            // 
            // grpSupplier
            // 
            this.grpSupplier.Controls.Add(this.lstSupplier);
            this.grpSupplier.Controls.Add(this.rdSelSupplier);
            this.grpSupplier.Controls.Add(this.rdAllSupplier);
            this.grpSupplier.Location = new System.Drawing.Point(474, 9);
            this.grpSupplier.Name = "grpSupplier";
            this.grpSupplier.Size = new System.Drawing.Size(206, 178);
            this.grpSupplier.TabIndex = 51;
            this.grpSupplier.TabStop = false;
            this.grpSupplier.Text = "Supplier";
            // 
            // lstSupplier
            // 
            this.lstSupplier.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstSupplier.CheckBoxes = true;
            this.lstSupplier.Location = new System.Drawing.Point(6, 55);
            this.lstSupplier.Name = "lstSupplier";
            this.lstSupplier.Size = new System.Drawing.Size(193, 116);
            this.lstSupplier.TabIndex = 2;
            this.lstSupplier.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelSupplier
            // 
            this.rdSelSupplier.AutoSize = true;
            this.rdSelSupplier.Location = new System.Drawing.Point(9, 36);
            this.rdSelSupplier.Name = "rdSelSupplier";
            this.rdSelSupplier.Size = new System.Drawing.Size(113, 17);
            this.rdSelSupplier.TabIndex = 1;
            this.rdSelSupplier.Text = "Selected Suppliers";
            this.rdSelSupplier.UseVisualStyleBackColor = true;
            // 
            // rdAllSupplier
            // 
            this.rdAllSupplier.AutoSize = true;
            this.rdAllSupplier.Checked = true;
            this.rdAllSupplier.Location = new System.Drawing.Point(9, 18);
            this.rdAllSupplier.Name = "rdAllSupplier";
            this.rdAllSupplier.Size = new System.Drawing.Size(82, 17);
            this.rdAllSupplier.TabIndex = 0;
            this.rdAllSupplier.TabStop = true;
            this.rdAllSupplier.Text = "All Suppliers";
            this.rdAllSupplier.UseVisualStyleBackColor = true;
            // 
            // grpStockPoint
            // 
            this.grpStockPoint.Controls.Add(this.lstStockpoint);
            this.grpStockPoint.Controls.Add(this.rdSelStockpoint);
            this.grpStockPoint.Controls.Add(this.rdAllStockpoint);
            this.grpStockPoint.Location = new System.Drawing.Point(320, 9);
            this.grpStockPoint.Name = "grpStockPoint";
            this.grpStockPoint.Size = new System.Drawing.Size(148, 178);
            this.grpStockPoint.TabIndex = 52;
            this.grpStockPoint.TabStop = false;
            this.grpStockPoint.Text = "Stock Point";
            // 
            // lstStockpoint
            // 
            this.lstStockpoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstStockpoint.CheckBoxes = true;
            this.lstStockpoint.Location = new System.Drawing.Point(6, 55);
            this.lstStockpoint.Name = "lstStockpoint";
            this.lstStockpoint.Size = new System.Drawing.Size(137, 116);
            this.lstStockpoint.TabIndex = 2;
            this.lstStockpoint.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelStockpoint
            // 
            this.rdSelStockpoint.AutoSize = true;
            this.rdSelStockpoint.Location = new System.Drawing.Point(11, 36);
            this.rdSelStockpoint.Name = "rdSelStockpoint";
            this.rdSelStockpoint.Size = new System.Drawing.Size(121, 17);
            this.rdSelStockpoint.TabIndex = 1;
            this.rdSelStockpoint.Text = "Selected Stockpoint";
            this.rdSelStockpoint.UseVisualStyleBackColor = true;
            // 
            // rdAllStockpoint
            // 
            this.rdAllStockpoint.AutoSize = true;
            this.rdAllStockpoint.Checked = true;
            this.rdAllStockpoint.Location = new System.Drawing.Point(11, 18);
            this.rdAllStockpoint.Name = "rdAllStockpoint";
            this.rdAllStockpoint.Size = new System.Drawing.Size(90, 17);
            this.rdAllStockpoint.TabIndex = 0;
            this.rdAllStockpoint.TabStop = true;
            this.rdAllStockpoint.Text = "All Stockpoint";
            this.rdAllStockpoint.UseVisualStyleBackColor = true;
            // 
            // grpSection
            // 
            this.grpSection.Controls.Add(this.lstSection);
            this.grpSection.Controls.Add(this.rdSelSection);
            this.grpSection.Controls.Add(this.rdAllSection);
            this.grpSection.Location = new System.Drawing.Point(9, 193);
            this.grpSection.Name = "grpSection";
            this.grpSection.Size = new System.Drawing.Size(148, 178);
            this.grpSection.TabIndex = 51;
            this.grpSection.TabStop = false;
            this.grpSection.Text = "Section";
            // 
            // lstSection
            // 
            this.lstSection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstSection.CheckBoxes = true;
            this.lstSection.Location = new System.Drawing.Point(6, 55);
            this.lstSection.Name = "lstSection";
            this.lstSection.Size = new System.Drawing.Size(137, 116);
            this.lstSection.TabIndex = 2;
            this.lstSection.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelSection
            // 
            this.rdSelSection.AutoSize = true;
            this.rdSelSection.Location = new System.Drawing.Point(11, 36);
            this.rdSelSection.Name = "rdSelSection";
            this.rdSelSection.Size = new System.Drawing.Size(106, 17);
            this.rdSelSection.TabIndex = 1;
            this.rdSelSection.Text = "Selected Section";
            this.rdSelSection.UseVisualStyleBackColor = true;
            // 
            // rdAllSection
            // 
            this.rdAllSection.AutoSize = true;
            this.rdAllSection.Checked = true;
            this.rdAllSection.Location = new System.Drawing.Point(11, 18);
            this.rdAllSection.Name = "rdAllSection";
            this.rdAllSection.Size = new System.Drawing.Size(75, 17);
            this.rdAllSection.TabIndex = 0;
            this.rdAllSection.TabStop = true;
            this.rdAllSection.Text = "All Section";
            this.rdAllSection.UseVisualStyleBackColor = true;
            // 
            // grpItems
            // 
            this.grpItems.Controls.Add(this.lstItem);
            this.grpItems.Controls.Add(this.rdSelItem);
            this.grpItems.Controls.Add(this.rdAllItem);
            this.grpItems.Location = new System.Drawing.Point(163, 193);
            this.grpItems.Name = "grpItems";
            this.grpItems.Size = new System.Drawing.Size(188, 178);
            this.grpItems.TabIndex = 50;
            this.grpItems.TabStop = false;
            this.grpItems.Text = "Items";
            // 
            // lstItem
            // 
            this.lstItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstItem.CheckBoxes = true;
            this.lstItem.Location = new System.Drawing.Point(6, 55);
            this.lstItem.Name = "lstItem";
            this.lstItem.Size = new System.Drawing.Size(176, 116);
            this.lstItem.TabIndex = 2;
            this.lstItem.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelItem
            // 
            this.rdSelItem.AutoSize = true;
            this.rdSelItem.Location = new System.Drawing.Point(9, 36);
            this.rdSelItem.Name = "rdSelItem";
            this.rdSelItem.Size = new System.Drawing.Size(95, 17);
            this.rdSelItem.TabIndex = 1;
            this.rdSelItem.Text = "Selected Items";
            this.rdSelItem.UseVisualStyleBackColor = true;
            // 
            // rdAllItem
            // 
            this.rdAllItem.AutoSize = true;
            this.rdAllItem.Checked = true;
            this.rdAllItem.Location = new System.Drawing.Point(9, 18);
            this.rdAllItem.Name = "rdAllItem";
            this.rdAllItem.Size = new System.Drawing.Size(64, 17);
            this.rdAllItem.TabIndex = 0;
            this.rdAllItem.TabStop = true;
            this.rdAllItem.Text = "All Items";
            this.rdAllItem.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chkCPNotReq);
            this.groupBox3.Controls.Add(this.chkUOMNotReq);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.txtInvoiceNo);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.DtpToInvoiceDate);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.dtpFrInvoiceDate);
            this.groupBox3.Controls.Add(this.dtpEntryDateTo);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.dtpEntryDateFrom);
            this.groupBox3.Controls.Add(this.txtToCode);
            this.groupBox3.Controls.Add(this.txtFrCode);
            this.groupBox3.Location = new System.Drawing.Point(357, 193);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(323, 178);
            this.groupBox3.TabIndex = 47;
            this.groupBox3.TabStop = false;
            // 
            // chkCPNotReq
            // 
            this.chkCPNotReq.AutoSize = true;
            this.chkCPNotReq.Location = new System.Drawing.Point(198, 153);
            this.chkCPNotReq.Name = "chkCPNotReq";
            this.chkCPNotReq.Size = new System.Drawing.Size(106, 17);
            this.chkCPNotReq.TabIndex = 29;
            this.chkCPNotReq.Text = "CP Not Required";
            this.chkCPNotReq.UseVisualStyleBackColor = true;
            // 
            // chkUOMNotReq
            // 
            this.chkUOMNotReq.AutoSize = true;
            this.chkUOMNotReq.Location = new System.Drawing.Point(70, 153);
            this.chkUOMNotReq.Name = "chkUOMNotReq";
            this.chkUOMNotReq.Size = new System.Drawing.Size(117, 17);
            this.chkUOMNotReq.TabIndex = 28;
            this.chkUOMNotReq.Text = "UOM Not Required";
            this.chkUOMNotReq.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "Invoice No";
            // 
            // txtInvoiceNo
            // 
            this.txtInvoiceNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInvoiceNo.Location = new System.Drawing.Point(109, 47);
            this.txtInvoiceNo.Name = "txtInvoiceNo";
            this.txtInvoiceNo.Size = new System.Drawing.Size(69, 20);
            this.txtInvoiceNo.TabIndex = 26;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(186, 18);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(20, 13);
            this.label16.TabIndex = 25;
            this.label16.Text = "To";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(22, 18);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(79, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "Stock Entry No";
            // 
            // txtToCode
            // 
            this.txtToCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtToCode.Location = new System.Drawing.Point(214, 14);
            this.txtToCode.Name = "txtToCode";
            this.txtToCode.Size = new System.Drawing.Size(69, 20);
            this.txtToCode.TabIndex = 1;
            // 
            // txtFrCode
            // 
            this.txtFrCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFrCode.Location = new System.Drawing.Point(109, 14);
            this.txtFrCode.Name = "txtFrCode";
            this.txtFrCode.Size = new System.Drawing.Size(69, 20);
            this.txtFrCode.TabIndex = 0;
            // 
            // grpCategory
            // 
            this.grpCategory.Controls.Add(this.lstCategory);
            this.grpCategory.Controls.Add(this.rdSelCatg);
            this.grpCategory.Controls.Add(this.rdAllCatg);
            this.grpCategory.Location = new System.Drawing.Point(160, 9);
            this.grpCategory.Name = "grpCategory";
            this.grpCategory.Size = new System.Drawing.Size(148, 178);
            this.grpCategory.TabIndex = 49;
            this.grpCategory.TabStop = false;
            this.grpCategory.Text = "Category";
            // 
            // lstCategory
            // 
            this.lstCategory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstCategory.CheckBoxes = true;
            this.lstCategory.Location = new System.Drawing.Point(6, 55);
            this.lstCategory.Name = "lstCategory";
            this.lstCategory.Size = new System.Drawing.Size(137, 116);
            this.lstCategory.TabIndex = 2;
            this.lstCategory.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelCatg
            // 
            this.rdSelCatg.AutoSize = true;
            this.rdSelCatg.Location = new System.Drawing.Point(6, 36);
            this.rdSelCatg.Name = "rdSelCatg";
            this.rdSelCatg.Size = new System.Drawing.Size(120, 17);
            this.rdSelCatg.TabIndex = 1;
            this.rdSelCatg.Text = "Selected Categories";
            this.rdSelCatg.UseVisualStyleBackColor = true;
            // 
            // rdAllCatg
            // 
            this.rdAllCatg.AutoSize = true;
            this.rdAllCatg.Checked = true;
            this.rdAllCatg.Location = new System.Drawing.Point(6, 18);
            this.rdAllCatg.Name = "rdAllCatg";
            this.rdAllCatg.Size = new System.Drawing.Size(89, 17);
            this.rdAllCatg.TabIndex = 0;
            this.rdAllCatg.TabStop = true;
            this.rdAllCatg.Text = "All Categories";
            this.rdAllCatg.UseVisualStyleBackColor = true;
            // 
            // grpDepartment
            // 
            this.grpDepartment.Controls.Add(this.lstDepartment);
            this.grpDepartment.Controls.Add(this.rdSelDept);
            this.grpDepartment.Controls.Add(this.rdAllDept);
            this.grpDepartment.Location = new System.Drawing.Point(9, 9);
            this.grpDepartment.Name = "grpDepartment";
            this.grpDepartment.Size = new System.Drawing.Size(148, 178);
            this.grpDepartment.TabIndex = 48;
            this.grpDepartment.TabStop = false;
            this.grpDepartment.Text = "Department";
            // 
            // lstDepartment
            // 
            this.lstDepartment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstDepartment.CheckBoxes = true;
            this.lstDepartment.Location = new System.Drawing.Point(6, 55);
            this.lstDepartment.Name = "lstDepartment";
            this.lstDepartment.Size = new System.Drawing.Size(137, 116);
            this.lstDepartment.TabIndex = 2;
            this.lstDepartment.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelDept
            // 
            this.rdSelDept.AutoSize = true;
            this.rdSelDept.Location = new System.Drawing.Point(6, 36);
            this.rdSelDept.Name = "rdSelDept";
            this.rdSelDept.Size = new System.Drawing.Size(125, 17);
            this.rdSelDept.TabIndex = 1;
            this.rdSelDept.Text = "Selected Department";
            this.rdSelDept.UseVisualStyleBackColor = true;
            // 
            // rdAllDept
            // 
            this.rdAllDept.AutoSize = true;
            this.rdAllDept.Checked = true;
            this.rdAllDept.Location = new System.Drawing.Point(6, 18);
            this.rdAllDept.Name = "rdAllDept";
            this.rdAllDept.Size = new System.Drawing.Size(99, 17);
            this.rdAllDept.TabIndex = 0;
            this.rdAllDept.TabStop = true;
            this.rdAllDept.Text = "All Departments";
            this.rdAllDept.UseVisualStyleBackColor = true;
            // 
            // frmRptStockEntryItemsSummary
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(889, 441);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.pnlSalesSummary);
            this.Name = "frmRptStockEntryItemsSummary";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stock Entry Items Summary";
            this.Load += new System.EventHandler(this.frmRptStockEntryItemsSummary_Load);
            this.pnlSalesSummary.ResumeLayout(false);
            this.grpDisplay.ResumeLayout(false);
            this.grpDisplay.PerformLayout();
            this.grpGroup.ResumeLayout(false);
            this.grpGroup.PerformLayout();
            this.grpSupplier.ResumeLayout(false);
            this.grpSupplier.PerformLayout();
            this.grpStockPoint.ResumeLayout(false);
            this.grpStockPoint.PerformLayout();
            this.grpSection.ResumeLayout(false);
            this.grpSection.PerformLayout();
            this.grpItems.ResumeLayout(false);
            this.grpItems.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.grpCategory.ResumeLayout(false);
            this.grpCategory.PerformLayout();
            this.grpDepartment.ResumeLayout(false);
            this.grpDepartment.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}