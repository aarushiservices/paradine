﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.ReportForms
{
    partial class frmRptStockReturnBillsSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.DtpToStkreturnDate = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.DtpFrStkreturnDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbSupplier = new System.Windows.Forms.ComboBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.txtStkReturnNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.DtpToReturnBillDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.DtpFrReturnBillDate = new System.Windows.Forms.DateTimePicker();
            this.cmbPrintOn = new System.Windows.Forms.ComboBox();
            this.pnlPO = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.pnlPO.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(162, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "To";
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(176, 243);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 25);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.DtpToStkreturnDate);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.DtpFrStkreturnDate);
            this.groupBox1.Location = new System.Drawing.Point(12, 74);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(305, 55);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Stock Return Date :";
            // 
            // DtpToStkreturnDate
            // 
            this.DtpToStkreturnDate.Checked = false;
            this.DtpToStkreturnDate.CustomFormat = "dd/MMM/yyyy";
            this.DtpToStkreturnDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpToStkreturnDate.Location = new System.Drawing.Point(188, 19);
            this.DtpToStkreturnDate.Name = "DtpToStkreturnDate";
            this.DtpToStkreturnDate.ShowCheckBox = true;
            this.DtpToStkreturnDate.Size = new System.Drawing.Size(101, 20);
            this.DtpToStkreturnDate.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "From";
            // 
            // DtpFrStkreturnDate
            // 
            this.DtpFrStkreturnDate.Checked = false;
            this.DtpFrStkreturnDate.CustomFormat = "dd/MMM/yyyy";
            this.DtpFrStkreturnDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpFrStkreturnDate.Location = new System.Drawing.Point(50, 19);
            this.DtpFrStkreturnDate.Name = "DtpFrStkreturnDate";
            this.DtpFrStkreturnDate.ShowCheckBox = true;
            this.DtpFrStkreturnDate.Size = new System.Drawing.Size(101, 20);
            this.DtpFrStkreturnDate.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Supplier :";
            // 
            // cmbSupplier
            // 
            this.cmbSupplier.BackColor = System.Drawing.Color.White;
            this.cmbSupplier.FormattingEnabled = true;
            this.cmbSupplier.Location = new System.Drawing.Point(96, 41);
            this.cmbSupplier.Name = "cmbSupplier";
            this.cmbSupplier.Size = new System.Drawing.Size(200, 21);
            this.cmbSupplier.TabIndex = 1;
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(82, 243);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(76, 25);
            this.btnPrint.TabIndex = 4;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // txtStkReturnNo
            // 
            this.txtStkReturnNo.BackColor = System.Drawing.Color.White;
            this.txtStkReturnNo.Location = new System.Drawing.Point(96, 10);
            this.txtStkReturnNo.Name = "txtStkReturnNo";
            this.txtStkReturnNo.Size = new System.Drawing.Size(93, 20);
            this.txtStkReturnNo.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Stock Rtrn No :";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.DtpToReturnBillDate);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.DtpFrReturnBillDate);
            this.groupBox2.Location = new System.Drawing.Point(12, 135);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(305, 55);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Return Date :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(161, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "To";
            // 
            // DtpToReturnBillDate
            // 
            this.DtpToReturnBillDate.Checked = false;
            this.DtpToReturnBillDate.CustomFormat = "dd/MMM/yyyy";
            this.DtpToReturnBillDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpToReturnBillDate.Location = new System.Drawing.Point(187, 19);
            this.DtpToReturnBillDate.Name = "DtpToReturnBillDate";
            this.DtpToReturnBillDate.ShowCheckBox = true;
            this.DtpToReturnBillDate.Size = new System.Drawing.Size(101, 20);
            this.DtpToReturnBillDate.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "From";
            // 
            // DtpFrReturnBillDate
            // 
            this.DtpFrReturnBillDate.Checked = false;
            this.DtpFrReturnBillDate.CustomFormat = "dd/MMM/yyyy";
            this.DtpFrReturnBillDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpFrReturnBillDate.Location = new System.Drawing.Point(49, 19);
            this.DtpFrReturnBillDate.Name = "DtpFrReturnBillDate";
            this.DtpFrReturnBillDate.ShowCheckBox = true;
            this.DtpFrReturnBillDate.Size = new System.Drawing.Size(101, 20);
            this.DtpFrReturnBillDate.TabIndex = 0;
            // 
            // cmbPrintOn
            // 
            this.cmbPrintOn.BackColor = System.Drawing.Color.White;
            this.cmbPrintOn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPrintOn.FormattingEnabled = true;
            this.cmbPrintOn.Location = new System.Drawing.Point(130, 199);
            this.cmbPrintOn.Name = "cmbPrintOn";
            this.cmbPrintOn.Size = new System.Drawing.Size(126, 21);
            this.cmbPrintOn.TabIndex = 4;
            // 
            // pnlPO
            // 
            this.pnlPO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPO.Controls.Add(this.label7);
            this.pnlPO.Controls.Add(this.cmbPrintOn);
            this.pnlPO.Controls.Add(this.groupBox2);
            this.pnlPO.Controls.Add(this.groupBox1);
            this.pnlPO.Controls.Add(this.label2);
            this.pnlPO.Controls.Add(this.cmbSupplier);
            this.pnlPO.Controls.Add(this.txtStkReturnNo);
            this.pnlPO.Controls.Add(this.label1);
            this.pnlPO.Location = new System.Drawing.Point(2, 4);
            this.pnlPO.Name = "pnlPO";
            this.pnlPO.Size = new System.Drawing.Size(330, 231);
            this.pnlPO.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(73, 203);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Print On :";
            // 
            // frmRptStockReturnBillsSummary
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(335, 276);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.pnlPO);
            this.Name = "frmRptStockReturnBillsSummary";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stock Return Bills Summary";
            this.Load += new System.EventHandler(this.frmRptStockReturnBillsSummary_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.pnlPO.ResumeLayout(false);
            this.pnlPO.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}