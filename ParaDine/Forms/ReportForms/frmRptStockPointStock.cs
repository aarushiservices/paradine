﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using ParaDinePrint;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmRptStockPointStock : Form
    {
        private Button btnExit;
        private Button btnPrint;
        private CheckBox chkDispInOut;
       // private IContainer components = null;
        private DataSet DS = new DataSet();
        private DateTimePicker dtpEndDate;
        private DateTimePicker dtpStartDate;
        private GroupBox groupBox1;
        internal GroupBox grpCategory;
        internal GroupBox grpDepartment;
        private GroupBox grpDisplay;
        private GroupBox grpGroup;
        internal GroupBox grpItems;
        internal GroupBox grpSection;
        internal GroupBox grpStockPoint;
        internal GroupBox grpTax;
        private Label label10;
        private Label label3;
        internal ListView lstCategory;
        internal ListView lstDepartment;
        internal ListView lstItem;
        internal ListView lstSection;
        internal ListView lstStockpoint;
        internal ListView lstTax;
        private PrintingFunctions pfunctions = new PrintingFunctions();
        private Panel pnlSalesSummary;
        private ClsColDosPrint[] prnItem;
        private ClsRptDosPrint prnrpt = new ClsRptDosPrint();
        internal RadioButton rdAllCatg;
        internal RadioButton rdAllDept;
        internal RadioButton rdAllItem;
        internal RadioButton rdAllSection;
        internal RadioButton rdAllStockpoint;
        internal RadioButton rdAllTax;
        private RadioButton rdDispCategory;
        private RadioButton rdDispCostprice;
        private RadioButton rdDispDepartment;
        private RadioButton rdDispItem;
        private RadioButton rdDispSection;
        private RadioButton rdDispStockpoint;
        private RadioButton rdGrpCategory;
        private RadioButton rdGrpCostPrice;
        private RadioButton rdGrpDepartment;
        private RadioButton rdGrpItem;
        private RadioButton rdGrpSection;
        private RadioButton rdGrpStockpoint;
        internal RadioButton rdSelCatg;
        internal RadioButton rdSelDept;
        internal RadioButton rdSelItem;
        internal RadioButton rdSelSection;
        internal RadioButton rdSelStockpoint;
        internal RadioButton rdSelTax;
        private string rptname = "";
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private SqlCommand SqlCmd = new SqlCommand();
        private string StrSql = "";

        public frmRptStockPointStock()
        {
            this.InitializeComponent();
        }

        private void AddHandlers()
        {
            this.lstDepartment.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstCategory.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstItem.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstSection.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstStockpoint.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstTax.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.rdAllDept.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllCatg.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllItem.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllSection.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllStockpoint.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllTax.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                string str = "";
                this.SqlCmd = new SqlCommand();
                this.SqlCmd.Connection = GlobalVariables.SqlConn;
                this.SqlCmd.CommandText = "RES_PROC_STOCKPOINTSTOCK";
                this.SqlCmd.CommandType = CommandType.StoredProcedure;
                this.SqlCmd.CommandTimeout = 0x186a0;
                this.SqlCmd.Parameters.AddWithValue("@TRANSTYPE", "STOCK");
                this.SqlCmd.Parameters.AddWithValue("@GROUPBY", this.GetField(this.grpGroup));
                this.SqlCmd.Parameters.AddWithValue("@DISPLAYBY", this.GetField(this.grpDisplay));
                this.SqlCmd.Parameters.AddWithValue("@STDATE", this.dtpStartDate.Value.ToString("dd/MMM/yy"));
                str = str + " From : " + this.dtpStartDate.Text + " ";
                this.SqlCmd.Parameters.AddWithValue("@ENDDATE", this.dtpEndDate.Value.ToString("dd/MMM/yy"));
                str = str + " UpTo : " + this.dtpEndDate.Text + " ; ";
                if (this.GetCollString(this.lstItem) != "")
                {
                    this.SqlCmd.Parameters.AddWithValue("@ITEMID", this.GetCollString(this.lstItem));
                    str = str + " ItemCode = " + this.GetRetParam(this.lstItem) + "; ";
                }
                if (this.GetCollString(this.lstDepartment) != "")
                {
                    this.SqlCmd.Parameters.AddWithValue("@DEPARTMENTID", this.GetCollString(this.lstDepartment));
                    str = str + "Department :" + this.GetRetParam(this.lstDepartment) + "; ";
                }
                if (this.GetCollString(this.lstCategory) != "")
                {
                    this.SqlCmd.Parameters.AddWithValue("@CATEGORYID", this.GetCollString(this.lstCategory));
                    str = str + "Category : " + this.GetRetParam(this.lstCategory) + "; ";
                }
                if (this.GetCollString(this.lstSection) != "")
                {
                    this.SqlCmd.Parameters.AddWithValue("@SECTIONID", this.GetCollString(this.lstSection));
                    str = str + "Section : " + this.GetRetParam(this.lstSection) + "; ";
                }
                if (this.GetCollString(this.lstTax) != "")
                {
                    this.SqlCmd.Parameters.AddWithValue("@TAXID", this.GetCollString(this.lstTax));
                    str = str + "Tax : " + this.GetRetParam(this.lstTax) + "; ";
                }
                if (this.GetCollString(this.lstStockpoint) != "")
                {
                    this.SqlCmd.Parameters.AddWithValue("@STOCKPOINTID", this.GetCollString(this.lstStockpoint));
                    str = str + "\tStockPoint : " + this.GetRetParam(this.lstStockpoint) + "; ";
                }
                this.SDA.SelectCommand = this.SqlCmd;
                if (this.DS.Tables.Contains("STOCKPOINT"))
                {
                    this.DS.Tables.Remove("STOCKPOINT");
                }
                this.SDA.Fill(this.DS, "STOCKPOINT");
                //ParameterFields pfields = new ParameterFields();
                //ParameterField parameterField = new ParameterField
                //{
                //    Name = "CompanyName"
                //};
                //ParameterDiscreteValue value2 = new ParameterDiscreteValue
                //{
                //    Value = GlobalVariables.StrCompName
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                //parameterField = new ParameterField
                //{
                //    Name = "Parameter"
                //};
                //value2 = new ParameterDiscreteValue
                //{
                //    Value = str
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                if (this.DS.Tables["STOCKPOINT"].Rows.Count > 0)
                {
                    if (this.chkDispInOut.Checked)
                    {
                        ///By Bharat we have to change Reports to itextsparp pdf
                        ///
                        //RptStockPointStockInOut rpt = new RptStockPointStockInOut();
                        //rpt.SetDataSource(this.DS.Tables["STOCKPOINT"]);
                        //rpt.Refresh();
                        //FrmRptViewer viewer = new FrmRptViewer(rpt, pfields);
                        //viewer.ShowDialog();
                    }
                    else
                    {
                        ///By Bharat we have to change Reports to itextsparp pdf
                        ///
                        //RptStockPointStock stock = new RptStockPointStock();
                        //stock.SetDataSource(this.DS.Tables["STOCKPOINT"]);
                        //stock.Refresh();
                        //new FrmRptViewer(stock, pfields).ShowDialog();
                    }
                }
                else
                {
                    MessageBox.Show("DATA NOT FOUND!");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Print_ClicSk");
            }
        }

    

        private void frmRptStockPointStock_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.RefreshData();
            this.AddHandlers();
        }

        private string GetCollString(ListView paramLvw)
        {
            try
            {
                string str = "";
                for (int i = 0; i < paramLvw.CheckedItems.Count; i++)
                {
                    if (str != "")
                    {
                        str = str + ", ";
                    }
                    str = str + Convert.ToString(paramLvw.CheckedItems[i].Tag);
                }
                return str;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in function Collection String");
                return "";
            }
        }

        private string GetField(GroupBox paramGrp)
        {
            try
            {
                foreach (RadioButton button in paramGrp.Controls)
                {
                    if (button.Checked)
                    {
                        if (button.Text.ToUpper() == "DEPARTMENT")
                        {
                            return "D.DEPARTMENTNAME";
                        }
                        if (button.Text.ToUpper() == "CATEGORY")
                        {
                            return "C.CATEGORYNAME";
                        }
                        if (button.Text.ToUpper() == "STOCKPOINT")
                        {
                            return "ST.STOCKPOINTNAME";
                        }
                        if (button.Text.ToUpper() == "SECTION")
                        {
                            return "SC.SECTIONNAME";
                        }
                        if (button.Text.ToUpper() == "ITEM")
                        {
                            return "IT.ITEMNAME";
                        }
                        if (button.Text.ToUpper() == "COSTPRICE")
                        {
                            return "IT.COSTPRICE";
                        }
                        if (button.Text.ToUpper() == "TAX")
                        {
                            return "T.TAXNAME";
                        }
                    }
                }
                return "";
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in GetFied");
                return "";
            }
        }

        private string GetParamField(GroupBox paramGrp)
        {
            try
            {
                foreach (RadioButton button in paramGrp.Controls)
                {
                    if (button.Checked)
                    {
                        if (button.Text.ToUpper() == "DEPARTMENT")
                        {
                            return "DEPARTMENT";
                        }
                        if (button.Text.ToUpper() == "CATEGORY")
                        {
                            return "CATEGORY";
                        }
                        if (button.Text.ToUpper() == "STOCKPOINT")
                        {
                            return "STOCKPOINT";
                        }
                        if (button.Text.ToUpper() == "SECTION")
                        {
                            return "SECTION";
                        }
                        if (button.Text.ToUpper() == "ITEM")
                        {
                            return "ITEM";
                        }
                        if (button.Text.ToUpper() == "COSTPRICE")
                        {
                            return "COSTPRICE";
                        }
                        if (button.Text.ToUpper() == "TAX")
                        {
                            return "TAX";
                        }
                    }
                }
                return "";
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in GetFied");
                return "";
            }
        }

        private string GetRetParam(ListView ParamLstVw)
        {
            try
            {
                string str = "";
                for (int i = 0; i < ParamLstVw.CheckedItems.Count; i++)
                {
                    if (str != "")
                    {
                        str = str + ", ";
                    }
                    str = str + Convert.ToString(ParamLstVw.CheckedItems[i].Text);
                }
                return str;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in function Collection String");
                return "";
            }
        }

    

        private void lst_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            try
            {
                if (e.Item.Checked)
                {
                    ListView view = (ListView)sender;
                    if (view.CheckedItems.Count > 0)
                    {
                        foreach (Control control in view.Parent.Controls)
                        {
                            if (control.GetType() == typeof(RadioButton))
                            {
                                RadioButton button = (RadioButton)control;
                                if (button.Text.ToUpper().Contains("SELECTED"))
                                {
                                    button.Checked = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in ListViewChecked");
            }
        }

        private void rdAll_checkchanged(object sender, EventArgs e)
        {
            try
            {
                RadioButton button = (RadioButton)sender;
                if (button.Checked)
                {
                    foreach (Control control in button.Parent.Controls)
                    {
                        if (control.GetType() == typeof(ListView))
                        {
                            ListView view = (ListView)control;
                            foreach (ListViewItem item in view.CheckedItems)
                            {
                                item.Checked = false;
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in AllRd");
            }
        }

        private void RefreshData()
        {
            this.StrSql = "SELECT DEPARTMENTID, DEPARTMENTNAME FROM RES_DEPARTMENT";
            GlobalFill.FillListView(this.lstDepartment, GlobalFill.FillDataTable(this.StrSql));
            this.StrSql = "SELECT CATEGORYID, CATEGORYNAME FROM RES_CATEGORY";
            GlobalFill.FillListView(this.lstCategory, GlobalFill.FillDataTable(this.StrSql));
            this.StrSql = "SELECT SECTIONID, SECTIONNAME FROM RES_SECTION";
            GlobalFill.FillListView(this.lstSection, GlobalFill.FillDataTable(this.StrSql));
            this.StrSql = "SELECT ITEMID, ITEMNAME FROM RES_ITEM";
            GlobalFill.FillListView(this.lstItem, GlobalFill.FillDataTable(this.StrSql));
            this.StrSql = "SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT";
            GlobalFill.FillListView(this.lstStockpoint, GlobalFill.FillDataTable(this.StrSql));
            this.StrSql = "SELECT TAXID, TAXNAME FROM RES_TAX";
            GlobalFill.FillListView(this.lstTax, GlobalFill.FillDataTable(this.StrSql));
            this.dtpEndDate.Value = GlobalVariables.BusinessDate;
            this.dtpStartDate.Value = GlobalVariables.BusinessDate;
        }
    }
}
