﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.ReportForms
{
    partial class frmRptStockTrans
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grpfields = new System.Windows.Forms.Panel();
            this.LBLOUTLET = new System.Windows.Forms.Label();
            this.CMBOUTLET = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbSendTo = new System.Windows.Forms.ComboBox();
            this.txtType = new System.Windows.Forms.TextBox();
            this.lblType = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.grpfields.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpfields
            // 
            this.grpfields.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.grpfields.Controls.Add(this.LBLOUTLET);
            this.grpfields.Controls.Add(this.CMBOUTLET);
            this.grpfields.Controls.Add(this.label15);
            this.grpfields.Controls.Add(this.cmbSendTo);
            this.grpfields.Controls.Add(this.txtType);
            this.grpfields.Controls.Add(this.lblType);
            this.grpfields.Location = new System.Drawing.Point(11, 9);
            this.grpfields.Name = "grpfields";
            this.grpfields.Size = new System.Drawing.Size(227, 164);
            this.grpfields.TabIndex = 0;
            // 
            // LBLOUTLET
            // 
            this.LBLOUTLET.AutoSize = true;
            this.LBLOUTLET.Location = new System.Drawing.Point(27, 107);
            this.LBLOUTLET.Name = "LBLOUTLET";
            this.LBLOUTLET.Size = new System.Drawing.Size(71, 13);
            this.LBLOUTLET.TabIndex = 66;
            this.LBLOUTLET.Text = "Outlet Type : ";
            this.LBLOUTLET.Visible = false;
            // 
            // CMBOUTLET
            // 
            this.CMBOUTLET.BackColor = System.Drawing.Color.White;
            this.CMBOUTLET.FormattingEnabled = true;
            this.CMBOUTLET.Location = new System.Drawing.Point(30, 123);
            this.CMBOUTLET.Name = "CMBOUTLET";
            this.CMBOUTLET.Size = new System.Drawing.Size(168, 21);
            this.CMBOUTLET.TabIndex = 65;
            this.CMBOUTLET.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(26, 61);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 13);
            this.label15.TabIndex = 64;
            this.label15.Text = "Send To :";
            // 
            // cmbSendTo
            // 
            this.cmbSendTo.BackColor = System.Drawing.Color.White;
            this.cmbSendTo.FormattingEnabled = true;
            this.cmbSendTo.Location = new System.Drawing.Point(29, 77);
            this.cmbSendTo.Name = "cmbSendTo";
            this.cmbSendTo.Size = new System.Drawing.Size(168, 21);
            this.cmbSendTo.TabIndex = 1;
            // 
            // txtType
            // 
            this.txtType.BackColor = System.Drawing.Color.Ivory;
            this.txtType.Location = new System.Drawing.Point(29, 34);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(94, 20);
            this.txtType.TabIndex = 0;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(27, 18);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(53, 13);
            this.lblType.TabIndex = 0;
            this.lblType.Text = " Number :";
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(46, 182);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(70, 27);
            this.btnPrint.TabIndex = 1;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(132, 182);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(70, 27);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmRptStockTrans
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(249, 220);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.grpfields);
            this.Name = "frmRptStockTrans";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Print";
            this.Load += new System.EventHandler(this.frmRptPurchaseOrder_Load);
            this.grpfields.ResumeLayout(false);
            this.grpfields.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
    }
}