﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.ReportForms
{
    partial class frmRptStockTransferItemSumm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label10 = new System.Windows.Forms.Label();
            this.dtpTrfDateTo = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlSalesSummary = new System.Windows.Forms.Panel();
            this.grpToStockPoint = new System.Windows.Forms.GroupBox();
            this.lstToStockPoint = new System.Windows.Forms.ListView();
            this.rdSelToStockPoint = new System.Windows.Forms.RadioButton();
            this.rdAllToStockPoint = new System.Windows.Forms.RadioButton();
            this.grpDisplay = new System.Windows.Forms.GroupBox();
            this.rdDispToStockPoint = new System.Windows.Forms.RadioButton();
            this.rdDispFrStockpoint = new System.Windows.Forms.RadioButton();
            this.rdDispSection = new System.Windows.Forms.RadioButton();
            this.rdDispCostprice = new System.Windows.Forms.RadioButton();
            this.rdDispYear = new System.Windows.Forms.RadioButton();
            this.rdDispMonth = new System.Windows.Forms.RadioButton();
            this.rdDispTrfDate = new System.Windows.Forms.RadioButton();
            this.rdDispTrfNo = new System.Windows.Forms.RadioButton();
            this.rdDispItem = new System.Windows.Forms.RadioButton();
            this.rdDispCategory = new System.Windows.Forms.RadioButton();
            this.rdDispDepartment = new System.Windows.Forms.RadioButton();
            this.grpGroup = new System.Windows.Forms.GroupBox();
            this.rdGrpToStockPoint = new System.Windows.Forms.RadioButton();
            this.rdGrpFrStockpoint = new System.Windows.Forms.RadioButton();
            this.rdGrpSection = new System.Windows.Forms.RadioButton();
            this.rdGrpCostPrice = new System.Windows.Forms.RadioButton();
            this.rdGrpYear = new System.Windows.Forms.RadioButton();
            this.rdGrpMonth = new System.Windows.Forms.RadioButton();
            this.rdGrpTrfDate = new System.Windows.Forms.RadioButton();
            this.rdGrpTrfNo = new System.Windows.Forms.RadioButton();
            this.rdGrpItem = new System.Windows.Forms.RadioButton();
            this.rdGrpCategory = new System.Windows.Forms.RadioButton();
            this.rdGrpDepartment = new System.Windows.Forms.RadioButton();
            this.grpFrStockPoint = new System.Windows.Forms.GroupBox();
            this.lstFrStockpoint = new System.Windows.Forms.ListView();
            this.rdSelFrStockpoint = new System.Windows.Forms.RadioButton();
            this.rdAllFrStockpoint = new System.Windows.Forms.RadioButton();
            this.grpSection = new System.Windows.Forms.GroupBox();
            this.lstSection = new System.Windows.Forms.ListView();
            this.rdSelSection = new System.Windows.Forms.RadioButton();
            this.rdAllSection = new System.Windows.Forms.RadioButton();
            this.grpItems = new System.Windows.Forms.GroupBox();
            this.lstItem = new System.Windows.Forms.ListView();
            this.rdSelItem = new System.Windows.Forms.RadioButton();
            this.rdAllItem = new System.Windows.Forms.RadioButton();
            this.grpCategory = new System.Windows.Forms.GroupBox();
            this.lstCategory = new System.Windows.Forms.ListView();
            this.rdSelCatg = new System.Windows.Forms.RadioButton();
            this.rdAllCatg = new System.Windows.Forms.RadioButton();
            this.grpDepartment = new System.Windows.Forms.GroupBox();
            this.lstDepartment = new System.Windows.Forms.ListView();
            this.rdSelDept = new System.Windows.Forms.RadioButton();
            this.rdAllDept = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.dtpTrfDateFrom = new System.Windows.Forms.DateTimePicker();
            this.txtToTrfNo = new System.Windows.Forms.TextBox();
            this.txtFrTrfNo = new System.Windows.Forms.TextBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlSalesSummary.SuspendLayout();
            this.grpToStockPoint.SuspendLayout();
            this.grpDisplay.SuspendLayout();
            this.grpGroup.SuspendLayout();
            this.grpFrStockPoint.SuspendLayout();
            this.grpSection.SuspendLayout();
            this.grpItems.SuspendLayout();
            this.grpCategory.SuspendLayout();
            this.grpDepartment.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(141, 91);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "To :";
            // 
            // dtpTrfDateTo
            // 
            this.dtpTrfDateTo.Checked = false;
            this.dtpTrfDateTo.CustomFormat = "dd/MMM/yyyy";
            this.dtpTrfDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTrfDateTo.Location = new System.Drawing.Point(144, 107);
            this.dtpTrfDateTo.Name = "dtpTrfDateTo";
            this.dtpTrfDateTo.ShowCheckBox = true;
            this.dtpTrfDateTo.Size = new System.Drawing.Size(104, 20);
            this.dtpTrfDateTo.TabIndex = 1;
            this.dtpTrfDateTo.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "From :";
            // 
            // pnlSalesSummary
            // 
            this.pnlSalesSummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSalesSummary.Controls.Add(this.grpToStockPoint);
            this.pnlSalesSummary.Controls.Add(this.grpDisplay);
            this.pnlSalesSummary.Controls.Add(this.grpGroup);
            this.pnlSalesSummary.Controls.Add(this.grpFrStockPoint);
            this.pnlSalesSummary.Controls.Add(this.grpSection);
            this.pnlSalesSummary.Controls.Add(this.grpItems);
            this.pnlSalesSummary.Controls.Add(this.grpCategory);
            this.pnlSalesSummary.Controls.Add(this.grpDepartment);
            this.pnlSalesSummary.Controls.Add(this.groupBox3);
            this.pnlSalesSummary.Location = new System.Drawing.Point(10, 11);
            this.pnlSalesSummary.Name = "pnlSalesSummary";
            this.pnlSalesSummary.Size = new System.Drawing.Size(841, 392);
            this.pnlSalesSummary.TabIndex = 12;
            // 
            // grpToStockPoint
            // 
            this.grpToStockPoint.Controls.Add(this.lstToStockPoint);
            this.grpToStockPoint.Controls.Add(this.rdSelToStockPoint);
            this.grpToStockPoint.Controls.Add(this.rdAllToStockPoint);
            this.grpToStockPoint.Location = new System.Drawing.Point(470, 20);
            this.grpToStockPoint.Name = "grpToStockPoint";
            this.grpToStockPoint.Size = new System.Drawing.Size(148, 178);
            this.grpToStockPoint.TabIndex = 60;
            this.grpToStockPoint.TabStop = false;
            this.grpToStockPoint.Text = "To Stock Point";
            // 
            // lstToStockPoint
            // 
            this.lstToStockPoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstToStockPoint.CheckBoxes = true;
            this.lstToStockPoint.Location = new System.Drawing.Point(6, 55);
            this.lstToStockPoint.Name = "lstToStockPoint";
            this.lstToStockPoint.Size = new System.Drawing.Size(137, 116);
            this.lstToStockPoint.TabIndex = 2;
            this.lstToStockPoint.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelToStockPoint
            // 
            this.rdSelToStockPoint.AutoSize = true;
            this.rdSelToStockPoint.Location = new System.Drawing.Point(11, 36);
            this.rdSelToStockPoint.Name = "rdSelToStockPoint";
            this.rdSelToStockPoint.Size = new System.Drawing.Size(121, 17);
            this.rdSelToStockPoint.TabIndex = 1;
            this.rdSelToStockPoint.Text = "Selected Stockpoint";
            this.rdSelToStockPoint.UseVisualStyleBackColor = true;
            // 
            // rdAllToStockPoint
            // 
            this.rdAllToStockPoint.AutoSize = true;
            this.rdAllToStockPoint.Checked = true;
            this.rdAllToStockPoint.Location = new System.Drawing.Point(11, 18);
            this.rdAllToStockPoint.Name = "rdAllToStockPoint";
            this.rdAllToStockPoint.Size = new System.Drawing.Size(90, 17);
            this.rdAllToStockPoint.TabIndex = 0;
            this.rdAllToStockPoint.TabStop = true;
            this.rdAllToStockPoint.Text = "All Stockpoint";
            this.rdAllToStockPoint.UseVisualStyleBackColor = true;
            // 
            // grpDisplay
            // 
            this.grpDisplay.Controls.Add(this.rdDispToStockPoint);
            this.grpDisplay.Controls.Add(this.rdDispFrStockpoint);
            this.grpDisplay.Controls.Add(this.rdDispSection);
            this.grpDisplay.Controls.Add(this.rdDispCostprice);
            this.grpDisplay.Controls.Add(this.rdDispYear);
            this.grpDisplay.Controls.Add(this.rdDispMonth);
            this.grpDisplay.Controls.Add(this.rdDispTrfDate);
            this.grpDisplay.Controls.Add(this.rdDispTrfNo);
            this.grpDisplay.Controls.Add(this.rdDispItem);
            this.grpDisplay.Controls.Add(this.rdDispCategory);
            this.grpDisplay.Controls.Add(this.rdDispDepartment);
            this.grpDisplay.Location = new System.Drawing.Point(729, 20);
            this.grpDisplay.Name = "grpDisplay";
            this.grpDisplay.Size = new System.Drawing.Size(100, 362);
            this.grpDisplay.TabIndex = 61;
            this.grpDisplay.TabStop = false;
            this.grpDisplay.Text = "Display";
            // 
            // rdDispToStockPoint
            // 
            this.rdDispToStockPoint.AutoSize = true;
            this.rdDispToStockPoint.Location = new System.Drawing.Point(8, 51);
            this.rdDispToStockPoint.Name = "rdDispToStockPoint";
            this.rdDispToStockPoint.Size = new System.Drawing.Size(90, 17);
            this.rdDispToStockPoint.TabIndex = 30;
            this.rdDispToStockPoint.Text = "ToStockPoint";
            this.rdDispToStockPoint.UseVisualStyleBackColor = true;
            // 
            // rdDispFrStockpoint
            // 
            this.rdDispFrStockpoint.AutoSize = true;
            this.rdDispFrStockpoint.Location = new System.Drawing.Point(8, 27);
            this.rdDispFrStockpoint.Name = "rdDispFrStockpoint";
            this.rdDispFrStockpoint.Size = new System.Drawing.Size(86, 17);
            this.rdDispFrStockpoint.TabIndex = 29;
            this.rdDispFrStockpoint.Text = "FrStockPoint";
            this.rdDispFrStockpoint.UseVisualStyleBackColor = true;
            // 
            // rdDispSection
            // 
            this.rdDispSection.AutoSize = true;
            this.rdDispSection.Location = new System.Drawing.Point(8, 123);
            this.rdDispSection.Name = "rdDispSection";
            this.rdDispSection.Size = new System.Drawing.Size(61, 17);
            this.rdDispSection.TabIndex = 28;
            this.rdDispSection.Text = "Section";
            this.rdDispSection.UseVisualStyleBackColor = true;
            // 
            // rdDispCostprice
            // 
            this.rdDispCostprice.AutoSize = true;
            this.rdDispCostprice.Location = new System.Drawing.Point(8, 171);
            this.rdDispCostprice.Name = "rdDispCostprice";
            this.rdDispCostprice.Size = new System.Drawing.Size(70, 17);
            this.rdDispCostprice.TabIndex = 27;
            this.rdDispCostprice.Text = "CostPrice";
            this.rdDispCostprice.UseVisualStyleBackColor = true;
            // 
            // rdDispYear
            // 
            this.rdDispYear.AutoSize = true;
            this.rdDispYear.Location = new System.Drawing.Point(8, 267);
            this.rdDispYear.Name = "rdDispYear";
            this.rdDispYear.Size = new System.Drawing.Size(47, 17);
            this.rdDispYear.TabIndex = 25;
            this.rdDispYear.Text = "Year";
            this.rdDispYear.UseVisualStyleBackColor = true;
            // 
            // rdDispMonth
            // 
            this.rdDispMonth.AutoSize = true;
            this.rdDispMonth.Location = new System.Drawing.Point(8, 243);
            this.rdDispMonth.Name = "rdDispMonth";
            this.rdDispMonth.Size = new System.Drawing.Size(55, 17);
            this.rdDispMonth.TabIndex = 24;
            this.rdDispMonth.Text = "Month";
            this.rdDispMonth.UseVisualStyleBackColor = true;
            // 
            // rdDispTrfDate
            // 
            this.rdDispTrfDate.AutoSize = true;
            this.rdDispTrfDate.Location = new System.Drawing.Point(8, 219);
            this.rdDispTrfDate.Name = "rdDispTrfDate";
            this.rdDispTrfDate.Size = new System.Drawing.Size(87, 17);
            this.rdDispTrfDate.TabIndex = 22;
            this.rdDispTrfDate.Text = "TransferDate";
            this.rdDispTrfDate.UseVisualStyleBackColor = true;
            // 
            // rdDispTrfNo
            // 
            this.rdDispTrfNo.AutoSize = true;
            this.rdDispTrfNo.Location = new System.Drawing.Point(8, 195);
            this.rdDispTrfNo.Name = "rdDispTrfNo";
            this.rdDispTrfNo.Size = new System.Drawing.Size(78, 17);
            this.rdDispTrfNo.TabIndex = 21;
            this.rdDispTrfNo.Text = "TransferNo";
            this.rdDispTrfNo.UseVisualStyleBackColor = true;
            // 
            // rdDispItem
            // 
            this.rdDispItem.AutoSize = true;
            this.rdDispItem.Checked = true;
            this.rdDispItem.Location = new System.Drawing.Point(8, 147);
            this.rdDispItem.Name = "rdDispItem";
            this.rdDispItem.Size = new System.Drawing.Size(45, 17);
            this.rdDispItem.TabIndex = 18;
            this.rdDispItem.TabStop = true;
            this.rdDispItem.Text = "Item";
            this.rdDispItem.UseVisualStyleBackColor = true;
            // 
            // rdDispCategory
            // 
            this.rdDispCategory.AutoSize = true;
            this.rdDispCategory.Location = new System.Drawing.Point(8, 99);
            this.rdDispCategory.Name = "rdDispCategory";
            this.rdDispCategory.Size = new System.Drawing.Size(67, 17);
            this.rdDispCategory.TabIndex = 17;
            this.rdDispCategory.Text = "Category";
            this.rdDispCategory.UseVisualStyleBackColor = true;
            // 
            // rdDispDepartment
            // 
            this.rdDispDepartment.AutoSize = true;
            this.rdDispDepartment.Location = new System.Drawing.Point(8, 75);
            this.rdDispDepartment.Name = "rdDispDepartment";
            this.rdDispDepartment.Size = new System.Drawing.Size(80, 17);
            this.rdDispDepartment.TabIndex = 16;
            this.rdDispDepartment.Text = "Department";
            this.rdDispDepartment.UseVisualStyleBackColor = true;
            // 
            // grpGroup
            // 
            this.grpGroup.Controls.Add(this.rdGrpToStockPoint);
            this.grpGroup.Controls.Add(this.rdGrpFrStockpoint);
            this.grpGroup.Controls.Add(this.rdGrpSection);
            this.grpGroup.Controls.Add(this.rdGrpCostPrice);
            this.grpGroup.Controls.Add(this.rdGrpYear);
            this.grpGroup.Controls.Add(this.rdGrpMonth);
            this.grpGroup.Controls.Add(this.rdGrpTrfDate);
            this.grpGroup.Controls.Add(this.rdGrpTrfNo);
            this.grpGroup.Controls.Add(this.rdGrpItem);
            this.grpGroup.Controls.Add(this.rdGrpCategory);
            this.grpGroup.Controls.Add(this.rdGrpDepartment);
            this.grpGroup.Location = new System.Drawing.Point(624, 20);
            this.grpGroup.Name = "grpGroup";
            this.grpGroup.Size = new System.Drawing.Size(100, 362);
            this.grpGroup.TabIndex = 60;
            this.grpGroup.TabStop = false;
            this.grpGroup.Text = "Group";
            // 
            // rdGrpToStockPoint
            // 
            this.rdGrpToStockPoint.AutoSize = true;
            this.rdGrpToStockPoint.Location = new System.Drawing.Point(9, 50);
            this.rdGrpToStockPoint.Name = "rdGrpToStockPoint";
            this.rdGrpToStockPoint.Size = new System.Drawing.Size(90, 17);
            this.rdGrpToStockPoint.TabIndex = 15;
            this.rdGrpToStockPoint.Text = "ToStockPoint";
            this.rdGrpToStockPoint.UseVisualStyleBackColor = true;
            // 
            // rdGrpFrStockpoint
            // 
            this.rdGrpFrStockpoint.AutoSize = true;
            this.rdGrpFrStockpoint.Checked = true;
            this.rdGrpFrStockpoint.Location = new System.Drawing.Point(9, 25);
            this.rdGrpFrStockpoint.Name = "rdGrpFrStockpoint";
            this.rdGrpFrStockpoint.Size = new System.Drawing.Size(86, 17);
            this.rdGrpFrStockpoint.TabIndex = 14;
            this.rdGrpFrStockpoint.TabStop = true;
            this.rdGrpFrStockpoint.Text = "FrStockPoint";
            this.rdGrpFrStockpoint.UseVisualStyleBackColor = true;
            // 
            // rdGrpSection
            // 
            this.rdGrpSection.AutoSize = true;
            this.rdGrpSection.Location = new System.Drawing.Point(9, 125);
            this.rdGrpSection.Name = "rdGrpSection";
            this.rdGrpSection.Size = new System.Drawing.Size(61, 17);
            this.rdGrpSection.TabIndex = 13;
            this.rdGrpSection.Text = "Section";
            this.rdGrpSection.UseVisualStyleBackColor = true;
            // 
            // rdGrpCostPrice
            // 
            this.rdGrpCostPrice.AutoSize = true;
            this.rdGrpCostPrice.Location = new System.Drawing.Point(9, 175);
            this.rdGrpCostPrice.Name = "rdGrpCostPrice";
            this.rdGrpCostPrice.Size = new System.Drawing.Size(70, 17);
            this.rdGrpCostPrice.TabIndex = 12;
            this.rdGrpCostPrice.Text = "CostPrice";
            this.rdGrpCostPrice.UseVisualStyleBackColor = true;
            // 
            // rdGrpYear
            // 
            this.rdGrpYear.AutoSize = true;
            this.rdGrpYear.Location = new System.Drawing.Point(9, 275);
            this.rdGrpYear.Name = "rdGrpYear";
            this.rdGrpYear.Size = new System.Drawing.Size(47, 17);
            this.rdGrpYear.TabIndex = 10;
            this.rdGrpYear.Text = "Year";
            this.rdGrpYear.UseVisualStyleBackColor = true;
            // 
            // rdGrpMonth
            // 
            this.rdGrpMonth.AutoSize = true;
            this.rdGrpMonth.Location = new System.Drawing.Point(9, 250);
            this.rdGrpMonth.Name = "rdGrpMonth";
            this.rdGrpMonth.Size = new System.Drawing.Size(55, 17);
            this.rdGrpMonth.TabIndex = 9;
            this.rdGrpMonth.Text = "Month";
            this.rdGrpMonth.UseVisualStyleBackColor = true;
            // 
            // rdGrpTrfDate
            // 
            this.rdGrpTrfDate.AutoSize = true;
            this.rdGrpTrfDate.Location = new System.Drawing.Point(9, 225);
            this.rdGrpTrfDate.Name = "rdGrpTrfDate";
            this.rdGrpTrfDate.Size = new System.Drawing.Size(87, 17);
            this.rdGrpTrfDate.TabIndex = 7;
            this.rdGrpTrfDate.Text = "TransferDate";
            this.rdGrpTrfDate.UseVisualStyleBackColor = true;
            // 
            // rdGrpTrfNo
            // 
            this.rdGrpTrfNo.AutoSize = true;
            this.rdGrpTrfNo.Location = new System.Drawing.Point(9, 200);
            this.rdGrpTrfNo.Name = "rdGrpTrfNo";
            this.rdGrpTrfNo.Size = new System.Drawing.Size(78, 17);
            this.rdGrpTrfNo.TabIndex = 6;
            this.rdGrpTrfNo.Text = "TransferNo";
            this.rdGrpTrfNo.UseVisualStyleBackColor = true;
            // 
            // rdGrpItem
            // 
            this.rdGrpItem.AutoSize = true;
            this.rdGrpItem.Location = new System.Drawing.Point(9, 150);
            this.rdGrpItem.Name = "rdGrpItem";
            this.rdGrpItem.Size = new System.Drawing.Size(45, 17);
            this.rdGrpItem.TabIndex = 2;
            this.rdGrpItem.Text = "Item";
            this.rdGrpItem.UseVisualStyleBackColor = true;
            // 
            // rdGrpCategory
            // 
            this.rdGrpCategory.AutoSize = true;
            this.rdGrpCategory.Location = new System.Drawing.Point(9, 100);
            this.rdGrpCategory.Name = "rdGrpCategory";
            this.rdGrpCategory.Size = new System.Drawing.Size(67, 17);
            this.rdGrpCategory.TabIndex = 1;
            this.rdGrpCategory.Text = "Category";
            this.rdGrpCategory.UseVisualStyleBackColor = true;
            // 
            // rdGrpDepartment
            // 
            this.rdGrpDepartment.AutoSize = true;
            this.rdGrpDepartment.Location = new System.Drawing.Point(9, 75);
            this.rdGrpDepartment.Name = "rdGrpDepartment";
            this.rdGrpDepartment.Size = new System.Drawing.Size(80, 17);
            this.rdGrpDepartment.TabIndex = 0;
            this.rdGrpDepartment.Text = "Department";
            this.rdGrpDepartment.UseVisualStyleBackColor = true;
            // 
            // grpFrStockPoint
            // 
            this.grpFrStockPoint.Controls.Add(this.lstFrStockpoint);
            this.grpFrStockPoint.Controls.Add(this.rdSelFrStockpoint);
            this.grpFrStockPoint.Controls.Add(this.rdAllFrStockpoint);
            this.grpFrStockPoint.Location = new System.Drawing.Point(316, 20);
            this.grpFrStockPoint.Name = "grpFrStockPoint";
            this.grpFrStockPoint.Size = new System.Drawing.Size(148, 178);
            this.grpFrStockPoint.TabIndex = 59;
            this.grpFrStockPoint.TabStop = false;
            this.grpFrStockPoint.Text = "From Stock Point";
            // 
            // lstFrStockpoint
            // 
            this.lstFrStockpoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstFrStockpoint.CheckBoxes = true;
            this.lstFrStockpoint.Location = new System.Drawing.Point(6, 55);
            this.lstFrStockpoint.Name = "lstFrStockpoint";
            this.lstFrStockpoint.Size = new System.Drawing.Size(137, 116);
            this.lstFrStockpoint.TabIndex = 2;
            this.lstFrStockpoint.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelFrStockpoint
            // 
            this.rdSelFrStockpoint.AutoSize = true;
            this.rdSelFrStockpoint.Location = new System.Drawing.Point(11, 36);
            this.rdSelFrStockpoint.Name = "rdSelFrStockpoint";
            this.rdSelFrStockpoint.Size = new System.Drawing.Size(121, 17);
            this.rdSelFrStockpoint.TabIndex = 1;
            this.rdSelFrStockpoint.Text = "Selected Stockpoint";
            this.rdSelFrStockpoint.UseVisualStyleBackColor = true;
            // 
            // rdAllFrStockpoint
            // 
            this.rdAllFrStockpoint.AutoSize = true;
            this.rdAllFrStockpoint.Checked = true;
            this.rdAllFrStockpoint.Location = new System.Drawing.Point(11, 18);
            this.rdAllFrStockpoint.Name = "rdAllFrStockpoint";
            this.rdAllFrStockpoint.Size = new System.Drawing.Size(90, 17);
            this.rdAllFrStockpoint.TabIndex = 0;
            this.rdAllFrStockpoint.TabStop = true;
            this.rdAllFrStockpoint.Text = "All Stockpoint";
            this.rdAllFrStockpoint.UseVisualStyleBackColor = true;
            // 
            // grpSection
            // 
            this.grpSection.Controls.Add(this.lstSection);
            this.grpSection.Controls.Add(this.rdSelSection);
            this.grpSection.Controls.Add(this.rdAllSection);
            this.grpSection.Location = new System.Drawing.Point(11, 204);
            this.grpSection.Name = "grpSection";
            this.grpSection.Size = new System.Drawing.Size(148, 178);
            this.grpSection.TabIndex = 58;
            this.grpSection.TabStop = false;
            this.grpSection.Text = "Section";
            // 
            // lstSection
            // 
            this.lstSection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstSection.CheckBoxes = true;
            this.lstSection.Location = new System.Drawing.Point(6, 55);
            this.lstSection.Name = "lstSection";
            this.lstSection.Size = new System.Drawing.Size(137, 116);
            this.lstSection.TabIndex = 2;
            this.lstSection.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelSection
            // 
            this.rdSelSection.AutoSize = true;
            this.rdSelSection.Location = new System.Drawing.Point(11, 36);
            this.rdSelSection.Name = "rdSelSection";
            this.rdSelSection.Size = new System.Drawing.Size(106, 17);
            this.rdSelSection.TabIndex = 1;
            this.rdSelSection.Text = "Selected Section";
            this.rdSelSection.UseVisualStyleBackColor = true;
            // 
            // rdAllSection
            // 
            this.rdAllSection.AutoSize = true;
            this.rdAllSection.Checked = true;
            this.rdAllSection.Location = new System.Drawing.Point(11, 18);
            this.rdAllSection.Name = "rdAllSection";
            this.rdAllSection.Size = new System.Drawing.Size(75, 17);
            this.rdAllSection.TabIndex = 0;
            this.rdAllSection.TabStop = true;
            this.rdAllSection.Text = "All Section";
            this.rdAllSection.UseVisualStyleBackColor = true;
            // 
            // grpItems
            // 
            this.grpItems.Controls.Add(this.lstItem);
            this.grpItems.Controls.Add(this.rdSelItem);
            this.grpItems.Controls.Add(this.rdAllItem);
            this.grpItems.Location = new System.Drawing.Point(165, 204);
            this.grpItems.Name = "grpItems";
            this.grpItems.Size = new System.Drawing.Size(188, 178);
            this.grpItems.TabIndex = 57;
            this.grpItems.TabStop = false;
            this.grpItems.Text = "Items";
            // 
            // lstItem
            // 
            this.lstItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstItem.CheckBoxes = true;
            this.lstItem.Location = new System.Drawing.Point(6, 55);
            this.lstItem.Name = "lstItem";
            this.lstItem.Size = new System.Drawing.Size(176, 116);
            this.lstItem.TabIndex = 2;
            this.lstItem.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelItem
            // 
            this.rdSelItem.AutoSize = true;
            this.rdSelItem.Location = new System.Drawing.Point(9, 36);
            this.rdSelItem.Name = "rdSelItem";
            this.rdSelItem.Size = new System.Drawing.Size(95, 17);
            this.rdSelItem.TabIndex = 1;
            this.rdSelItem.Text = "Selected Items";
            this.rdSelItem.UseVisualStyleBackColor = true;
            // 
            // rdAllItem
            // 
            this.rdAllItem.AutoSize = true;
            this.rdAllItem.Checked = true;
            this.rdAllItem.Location = new System.Drawing.Point(9, 18);
            this.rdAllItem.Name = "rdAllItem";
            this.rdAllItem.Size = new System.Drawing.Size(64, 17);
            this.rdAllItem.TabIndex = 0;
            this.rdAllItem.TabStop = true;
            this.rdAllItem.Text = "All Items";
            this.rdAllItem.UseVisualStyleBackColor = true;
            // 
            // grpCategory
            // 
            this.grpCategory.Controls.Add(this.lstCategory);
            this.grpCategory.Controls.Add(this.rdSelCatg);
            this.grpCategory.Controls.Add(this.rdAllCatg);
            this.grpCategory.Location = new System.Drawing.Point(162, 20);
            this.grpCategory.Name = "grpCategory";
            this.grpCategory.Size = new System.Drawing.Size(148, 178);
            this.grpCategory.TabIndex = 56;
            this.grpCategory.TabStop = false;
            this.grpCategory.Text = "Category";
            // 
            // lstCategory
            // 
            this.lstCategory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstCategory.CheckBoxes = true;
            this.lstCategory.Location = new System.Drawing.Point(6, 55);
            this.lstCategory.Name = "lstCategory";
            this.lstCategory.Size = new System.Drawing.Size(137, 116);
            this.lstCategory.TabIndex = 2;
            this.lstCategory.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelCatg
            // 
            this.rdSelCatg.AutoSize = true;
            this.rdSelCatg.Location = new System.Drawing.Point(6, 36);
            this.rdSelCatg.Name = "rdSelCatg";
            this.rdSelCatg.Size = new System.Drawing.Size(120, 17);
            this.rdSelCatg.TabIndex = 1;
            this.rdSelCatg.Text = "Selected Categories";
            this.rdSelCatg.UseVisualStyleBackColor = true;
            // 
            // rdAllCatg
            // 
            this.rdAllCatg.AutoSize = true;
            this.rdAllCatg.Checked = true;
            this.rdAllCatg.Location = new System.Drawing.Point(6, 18);
            this.rdAllCatg.Name = "rdAllCatg";
            this.rdAllCatg.Size = new System.Drawing.Size(89, 17);
            this.rdAllCatg.TabIndex = 0;
            this.rdAllCatg.TabStop = true;
            this.rdAllCatg.Text = "All Categories";
            this.rdAllCatg.UseVisualStyleBackColor = true;
            // 
            // grpDepartment
            // 
            this.grpDepartment.Controls.Add(this.lstDepartment);
            this.grpDepartment.Controls.Add(this.rdSelDept);
            this.grpDepartment.Controls.Add(this.rdAllDept);
            this.grpDepartment.Location = new System.Drawing.Point(11, 20);
            this.grpDepartment.Name = "grpDepartment";
            this.grpDepartment.Size = new System.Drawing.Size(148, 178);
            this.grpDepartment.TabIndex = 55;
            this.grpDepartment.TabStop = false;
            this.grpDepartment.Text = "Department";
            // 
            // lstDepartment
            // 
            this.lstDepartment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstDepartment.CheckBoxes = true;
            this.lstDepartment.Location = new System.Drawing.Point(6, 55);
            this.lstDepartment.Name = "lstDepartment";
            this.lstDepartment.Size = new System.Drawing.Size(137, 116);
            this.lstDepartment.TabIndex = 2;
            this.lstDepartment.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelDept
            // 
            this.rdSelDept.AutoSize = true;
            this.rdSelDept.Location = new System.Drawing.Point(6, 36);
            this.rdSelDept.Name = "rdSelDept";
            this.rdSelDept.Size = new System.Drawing.Size(125, 17);
            this.rdSelDept.TabIndex = 1;
            this.rdSelDept.Text = "Selected Department";
            this.rdSelDept.UseVisualStyleBackColor = true;
            // 
            // rdAllDept
            // 
            this.rdAllDept.AutoSize = true;
            this.rdAllDept.Checked = true;
            this.rdAllDept.Location = new System.Drawing.Point(6, 18);
            this.rdAllDept.Name = "rdAllDept";
            this.rdAllDept.Size = new System.Drawing.Size(99, 17);
            this.rdAllDept.TabIndex = 0;
            this.rdAllDept.TabStop = true;
            this.rdAllDept.Text = "All Departments";
            this.rdAllDept.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.dtpTrfDateTo);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.dtpTrfDateFrom);
            this.groupBox3.Controls.Add(this.txtToTrfNo);
            this.groupBox3.Controls.Add(this.txtFrTrfNo);
            this.groupBox3.Location = new System.Drawing.Point(359, 204);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(259, 178);
            this.groupBox3.TabIndex = 47;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Item Code :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(141, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(26, 13);
            this.label16.TabIndex = 25;
            this.label16.Text = "To :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(20, 25);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(36, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "From :";
            // 
            // dtpTrfDateFrom
            // 
            this.dtpTrfDateFrom.Checked = false;
            this.dtpTrfDateFrom.CustomFormat = "dd/MMM/yyyy";
            this.dtpTrfDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTrfDateFrom.Location = new System.Drawing.Point(23, 107);
            this.dtpTrfDateFrom.Name = "dtpTrfDateFrom";
            this.dtpTrfDateFrom.ShowCheckBox = true;
            this.dtpTrfDateFrom.Size = new System.Drawing.Size(104, 20);
            this.dtpTrfDateFrom.TabIndex = 0;
            this.dtpTrfDateFrom.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // txtToTrfNo
            // 
            this.txtToTrfNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtToTrfNo.Location = new System.Drawing.Point(144, 41);
            this.txtToTrfNo.Name = "txtToTrfNo";
            this.txtToTrfNo.Size = new System.Drawing.Size(86, 20);
            this.txtToTrfNo.TabIndex = 1;
            // 
            // txtFrTrfNo
            // 
            this.txtFrTrfNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFrTrfNo.Location = new System.Drawing.Point(23, 41);
            this.txtFrTrfNo.Name = "txtFrTrfNo";
            this.txtFrTrfNo.Size = new System.Drawing.Size(86, 20);
            this.txtFrTrfNo.TabIndex = 0;
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(344, 409);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 13;
            this.btnPrint.Text = "&Show";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(441, 409);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 14;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmRptStockTransferItemSumm
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(855, 444);
            this.Controls.Add(this.pnlSalesSummary);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnExit);
            this.Name = "frmRptStockTransferItemSumm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stock Transfer Item Summary";
            this.Load += new System.EventHandler(this.frmRptStockTransferItemSumm_Load);
            this.pnlSalesSummary.ResumeLayout(false);
            this.grpToStockPoint.ResumeLayout(false);
            this.grpToStockPoint.PerformLayout();
            this.grpDisplay.ResumeLayout(false);
            this.grpDisplay.PerformLayout();
            this.grpGroup.ResumeLayout(false);
            this.grpGroup.PerformLayout();
            this.grpFrStockPoint.ResumeLayout(false);
            this.grpFrStockPoint.PerformLayout();
            this.grpSection.ResumeLayout(false);
            this.grpSection.PerformLayout();
            this.grpItems.ResumeLayout(false);
            this.grpItems.PerformLayout();
            this.grpCategory.ResumeLayout(false);
            this.grpCategory.PerformLayout();
            this.grpDepartment.ResumeLayout(false);
            this.grpDepartment.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}