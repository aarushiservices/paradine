﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using ParaDine.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmRptDayReport : Form
    {
        private bool blnExportReq;
        private Button btnPrint;
        //private IContainer components;
        private DataSet Ds;
        private DataTable dtDayReport;
        private DateTimePicker dtpDayReport;
        private Label label1;
        private SqlDataAdapter Sda;
        private string StrSql;

        public frmRptDayReport()
        {
            this.components = null;
            this.Sda = new SqlDataAdapter();
            this.Ds = new DataSet();
            this.blnExportReq = false;
            this.InitializeComponent();
            this.dtpDayReport.Value = GlobalVariables.BusinessDate;
        }
        public frmRptDayReport(DateTime pReportDt, bool pExportReq)
        {
            this.components = null;
            this.Sda = new SqlDataAdapter();
            this.Ds = new DataSet();
            this.blnExportReq = false;
            this.InitializeComponent();
            this.dtpDayReport.Value = pReportDt;
            this.blnExportReq = pExportReq;
        } 
        public void btnPrint_Click(object sender, EventArgs e)
         {
            try
            {
                this.StrSql = "DECLARE @STDATE DATETIME \n";
                this.StrSql = this.StrSql + " SET @STDATE = '" + this.dtpDayReport.Value.ToString("dd/MMM/yy") + "'\n";
                this.StrSql = this.StrSql + "CREATE TABLE #TMPTABLE (DESCRIPTION VARCHAR(50), TOTVALUE MONEY, REMARKS VARCHAR(50)) \n";
                this.StrSql = this.StrSql + "INSERT INTO #TMPTABLE ";
                this.StrSql = this.StrSql + "SELECT 'TOTAL SALES' AS DESCRIPTION, SUM(NETAMOUNT) AS TOTVALUE, '' AS REMARKS  FROM RES_VW_CUSTORDERMASTER CM  WHERE SETTLED = 1 AND CONVERT(DATETIME, CONVERT(VARCHAR, CM.CUSTORDERDATE, 106)) = '" + this.dtpDayReport.Value.ToString("dd/MMM/yy") + "'";
                this.StrSql = this.StrSql + "INSERT INTO #TMPTABLE ";
                this.StrSql = this.StrSql + "SELECT P.PAYMENTMODE + ' SALES' AS DESCRIPTION, SUM(PAIDAMOUNT) AS TOTVALUE, '' AS REMARKS  FROM GLB_MONEYMASTER MM  INNER JOIN GLB_MONEYTRANS MT ON MT.MONEYMASTERID = MM.MONEYMASTERID  INNER JOIN GLB_PAYMENTMODE P ON P.PAYMENTMODEID = MT.PAYMENTMODEID  WHERE TRANSACTIONSOURCE = 'BILL' AND CONVERT(DATETIME, CONVERT(VARCHAR, TRANSACTIONDATE, 106)) = '" + this.dtpDayReport.Value.ToString("dd/MMM/yy") + "' GROUP BY P.PAYMENTMODE \n";
                this.StrSql = this.StrSql + "INSERT INTO #TMPTABLE ";
                this.StrSql = this.StrSql + "SELECT 'DISCOUNT', SUM(DISCAMOUNT+KOTDISCAMOUNT), '' FROM RES_VW_CUSTORDERMASTER CM WHERE CONVERT(DATETIME, CONVERT(VARCHAR, CM.CUSTORDERDATE, 106)) = '" + this.dtpDayReport.Value.ToString("dd/MMM/yy") + "'";
                this.StrSql = this.StrSql + "INSERT INTO #TMPTABLE ";
                this.StrSql = this.StrSql + "SELECT 'STEP IN' AS DESCRIPTION, SUM(COVER) AS TOTVALUE, '' AS REMARKS FROM RES_CUSTORDERMASTER  WHERE CONVERT(DATETIME, CONVERT(VARCHAR, CUSTORDERDATE, 106)) = '" + this.dtpDayReport.Value.ToString("dd/MMM/yy") + "'";
                this.StrSql = this.StrSql + "INSERT INTO #TMPTABLE ";
                this.StrSql = this.StrSql + "SELECT 'TOTAL KOTs', COUNT(*), '' FROM RES_CUSTORDERMASTER CM INNER JOIN RES_CUSTORDERKOT CK ON CK.CUSTORDERMASTERID = CM.CUSTORDERMASTERID WHERE CONVERT(DATETIME, CONVERT(VARCHAR, CM.CUSTORDERDATE, 106)) = '" + this.dtpDayReport.Value.ToString("dd/MMM/yy") + "'";
                this.StrSql = this.StrSql + "INSERT INTO #TMPTABLE ";
                this.StrSql = this.StrSql + "SELECT 'TOTAL Bills', COUNT(*), '' FROM RES_CUSTORDERMASTER CM WHERE CONVERT(DATETIME, CONVERT(VARCHAR, CM.CUSTORDERDATE, 106)) = '" + this.dtpDayReport.Value.ToString("dd/MMM/yy") + "'";
                this.StrSql = this.StrSql + "INSERT INTO #TMPTABLE ";
                this.StrSql = this.StrSql + "SELECT 'Cancel Bills', COUNT(*), '' FROM RES_CUSTORDERMASTER CM WHERE CM.CANCELLEDSTATUS = 1 AND CONVERT(DATETIME, CONVERT(VARCHAR, CM.CUSTORDERDATE, 106)) = '" + this.dtpDayReport.Value.ToString("dd/MMM/yy") + "'";
                this.StrSql = this.StrSql + "INSERT INTO #TMPTABLE ";
                this.StrSql = this.StrSql + "SELECT 'Discount Bills', COUNT(*), '' FROM RES_CUSTORDERMASTER CM WHERE CM.DISCAMOUNT > 0 AND CONVERT(DATETIME, CONVERT(VARCHAR, CM.CUSTORDERDATE, 106)) = '" + this.dtpDayReport.Value.ToString("dd/MMM/yy") + "'";
                this.StrSql = this.StrSql + "INSERT INTO #TMPTABLE ";
                this.StrSql = this.StrSql + "SELECT 'Return Items', SUM(CI.SALEQTY), '' FROM RES_CUSTORDERMASTER CM  INNER JOIN RES_CUSTORDERKOT CK ON CK.CUSTORDERMASTERID = CM.CUSTORDERMASTERID  INNER JOIN RES_CUSTORDERITEM CI ON CI.CUSTORDERKOTID = CK.CUSTORDERKOTID WHERE CI.SALEQTY < 0 AND CONVERT(DATETIME, CONVERT(VARCHAR, CM.CUSTORDERDATE, 106)) = '" + this.dtpDayReport.Value.ToString("dd/MMM/yy") + "'";
                this.StrSql = this.StrSql + "SELECT * FROM #TMPTABLE\nDROP TABLE #TMPTABLE";
                GlobalFill.FillDataSet(this.StrSql, "DAYREPORT", this.Ds, this.Sda);
                SqlCommand selectCommand = new SqlCommand
                {
                    Connection = GlobalVariables.SqlConn,
                    CommandText = "RES_PROC_STOCKPOINTSTOCK",
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = 0x186a0
                };
                selectCommand.Parameters.AddWithValue("@TRANSTYPE", "DAYSTOCK");
                selectCommand.Parameters.AddWithValue("@GROUPBY", "STOCKPOINTNAME");
                selectCommand.Parameters.AddWithValue("@DISPLAYBY", "ITEMNAME");
                selectCommand.Parameters.AddWithValue("@STDATE", this.dtpDayReport.Value.ToString("dd/MMM/yy"));
                selectCommand.Parameters.AddWithValue("@ENDDATE", this.dtpDayReport.Value.ToString("dd/MMM/yy"));
                this.Sda = new SqlDataAdapter(selectCommand);
                if (this.Ds.Tables.Contains("DAYSTOCK"))
                {
                    this.Ds.Tables.Remove("DAYSTOCK");
                }
                this.Sda.Fill(this.Ds, "DAYSTOCK");
                if (this.Ds.Tables["DAYREPORT"].Rows.Count > 0)
                {
                    ///By Bharat we have to change Reports to itextsparp pdf
                    ///
                    RptDayReport rpt = new RptDayReport(this.Ds.Tables["DAYREPORT"]);
                    rpt.Parameter_DayReportDate = this.dtpDayReport.Value.ToShortDateString();
                    //rpt.Subreports[0].SetDataSource(this.Ds.Tables["DAYSTOCK"]);
                    //rpt.SetDataSource(this.Ds.Tables["DAYREPORT"]);
                    rpt.ShowDialog();
                    //ParameterFields parameterFields = rpt.ParameterFields;
                    //ParameterDiscreteValue value2 = new ParameterDiscreteValue
                    //{
                    //    Value = this.dtpDayReport.Value
                    //};
                    //parameterFields["DayReportDate"].CurrentValues.Add((ParameterValue)value2);
                    //if (this.blnExportReq)
                    //{
                    //    rpt.ExportToDisk(ExportFormatType.PortableDocFormat, Application.StartupPath + @"\Temp\DAYREPORT" + this.dtpDayReport.Value.ToString("ddMMMyyyy") + ".pdf");
                    //}
                    //else
                    //{
                    //    new FrmRptViewer(rpt, parameterFields).Show();
                    //}
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Printing Day Report");
            }
        }

        

        private void frmRptDayReport_Load(object sender, EventArgs e)
        {

        }

       
    }
}
