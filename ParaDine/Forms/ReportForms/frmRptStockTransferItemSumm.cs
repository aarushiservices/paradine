﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using ParaDine.Reports;
using ParaDinePrint;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmRptStockTransferItemSumm : Form
    {
        private Button btnExit;
        private Button btnPrint;
       // private IContainer components = null;
        private DataSet DS = new DataSet();
        private DateTimePicker dtpTrfDateFrom;
        private DateTimePicker dtpTrfDateTo;
        private GroupBox groupBox3;
        internal GroupBox grpCategory;
        internal GroupBox grpDepartment;
        private GroupBox grpDisplay;
        internal GroupBox grpFrStockPoint;
        private GroupBox grpGroup;
        internal GroupBox grpItems;
        internal GroupBox grpSection;
        internal GroupBox grpToStockPoint;
        private Label label10;
        private Label label16;
        private Label label17;
        private Label label3;
        internal ListView lstCategory;
        internal ListView lstDepartment;
        internal ListView lstFrStockpoint;
        internal ListView lstItem;
        internal ListView lstSection;
        internal ListView lstToStockPoint;
        private PrintingFunctions pfunctions = new PrintingFunctions();
        private Panel pnlSalesSummary;
        private ClsColDosPrint[] prnItem;
        private ClsRptDosPrint prnrpt = new ClsRptDosPrint();
        internal RadioButton rdAllCatg;
        internal RadioButton rdAllDept;
        internal RadioButton rdAllFrStockpoint;
        internal RadioButton rdAllItem;
        internal RadioButton rdAllSection;
        internal RadioButton rdAllToStockPoint;
        private RadioButton rdDispCategory;
        private RadioButton rdDispCostprice;
        private RadioButton rdDispDepartment;
        private RadioButton rdDispFrStockpoint;
        private RadioButton rdDispItem;
        private RadioButton rdDispMonth;
        private RadioButton rdDispSection;
        private RadioButton rdDispToStockPoint;
        private RadioButton rdDispTrfDate;
        private RadioButton rdDispTrfNo;
        private RadioButton rdDispYear;
        private RadioButton rdGrpCategory;
        private RadioButton rdGrpCostPrice;
        private RadioButton rdGrpDepartment;
        private RadioButton rdGrpFrStockpoint;
        private RadioButton rdGrpItem;
        private RadioButton rdGrpMonth;
        private RadioButton rdGrpSection;
        private RadioButton rdGrpToStockPoint;
        private RadioButton rdGrpTrfDate;
        private RadioButton rdGrpTrfNo;
        private RadioButton rdGrpYear;
        internal RadioButton rdSelCatg;
        internal RadioButton rdSelDept;
        internal RadioButton rdSelFrStockpoint;
        internal RadioButton rdSelItem;
        internal RadioButton rdSelSection;
        internal RadioButton rdSelToStockPoint;
        private string rptname = "";
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private SqlCommand SqlCmd = new SqlCommand();
        private string StrSql;
        private TextBox txtFrTrfNo;
        private TextBox txtToTrfNo;

        public frmRptStockTransferItemSumm()
        {
            this.InitializeComponent();
        }

        private void AddHandlers()
        {
            this.lstDepartment.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstCategory.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstItem.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstSection.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstFrStockpoint.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstToStockPoint.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.rdAllDept.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllCatg.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllItem.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllSection.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllFrStockpoint.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllToStockPoint.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                string str = "";
                string str2 = "";
                str2 = "SELECT ";
                str2 = ((str2 + this.GetField(this.grpGroup) + " AS CATEGORYNAME,") + this.GetField(this.grpDisplay) + " AS ITEMNAME,") + " U.UOMNAME, " + " MAX(ST.COSTPRICE) AS COSTPRICE, SUM(TRANSFERQTY) AS TRANSFERQTY , SUM(ST.AMOUNT) AS AMOUNT  FROM RES_VW_STOCKTRFTRANS ST  INNER JOIN RES_VW_STOCKTRFMASTER SM ON SM.STOCKTRFMASTERID = ST.STOCKTRFMASTERID  INNER JOIN RES_ITEM IT ON IT.ITEMID = ST.ITEMID  INNER JOIN RES_UOM U ON U.UOMID = ST.UOMID  INNER JOIN RES_SECTION SC ON SC.SECTIONID = IT.SECTIONID  INNER JOIN RES_CATEGORY CG ON CG.CATEGORYID = IT.CATEGORYID  INNER JOIN RES_DEPARTMENT DP ON DP.DEPARTMENTID = CG.DEPARTMENTID  WHERE ST.ITEMID <> 0 ";
                if (this.txtFrTrfNo.Text.Trim() != "")
                {
                    str2 = str2 + " AND SM.STOCKTRFNUMBER >= '" + this.txtFrTrfNo.Text + "'";
                    str = str + "Transfer From: " + this.txtFrTrfNo.Text + "; ";
                }
                if (this.txtToTrfNo.Text.Trim() != "")
                {
                    str2 = str2 + " AND SM.STOCKTRFNUMBER <= '" + this.txtToTrfNo.Text + "'";
                    str = str + "Transfer UpTo: " + this.txtToTrfNo.Text + "; ";
                }
                if (this.dtpTrfDateFrom.Checked)
                {
                    str2 = str2 + " AND SM.STOCKTRFDATE >= '" + this.dtpTrfDateFrom.Text + "'";
                    str = str + "From : " + this.dtpTrfDateFrom.Text + "; ";
                }
                if (this.dtpTrfDateTo.Checked)
                {
                    str2 = str2 + " AND SM.STOCKTRFDATE <= '" + this.dtpTrfDateTo.Text + "'";
                    str = str + "To : " + this.dtpTrfDateTo.Text + "; ";
                }
                if (this.rdSelDept.Checked && (this.GetCollString(this.lstDepartment) != ""))
                {
                    str2 = str2 + " AND DP.DEPARTMENTID IN (" + this.GetCollString(this.lstDepartment) + ")";
                    str = str + " Depts: " + this.GetRetParam(this.lstDepartment);
                }
                if (this.rdSelCatg.Checked && (this.GetCollString(this.lstCategory) != ""))
                {
                    str2 = str2 + " AND CG.CATEGORYID IN (" + this.GetCollString(this.lstCategory) + ")";
                    str = str + " Catg: " + this.GetRetParam(this.lstCategory);
                }
                if (this.rdSelFrStockpoint.Checked && (this.GetCollString(this.lstFrStockpoint) != ""))
                {
                    str2 = str2 + " AND SM.FRSTOCKPOINTID IN (" + this.GetCollString(this.lstFrStockpoint) + ")";
                    str = str + " From: " + this.GetRetParam(this.lstFrStockpoint);
                }
                if (this.rdSelToStockPoint.Checked && (this.GetCollString(this.lstToStockPoint) != ""))
                {
                    str2 = str2 + " AND SM.TOSTOCKPOINTID IN (" + this.GetCollString(this.lstToStockPoint) + ")";
                    str = str + " To: " + this.GetRetParam(this.lstToStockPoint);
                }
                if (this.rdSelSection.Checked && (this.GetCollString(this.lstSection) != ""))
                {
                    str2 = str2 + " AND SC.SECTIONID IN (" + this.GetCollString(this.lstSection) + ")";
                    str = str + " Sections: " + this.GetRetParam(this.lstSection);
                }
                if (this.rdSelItem.Checked && (this.GetCollString(this.lstItem) != ""))
                {
                    str2 = str2 + " AND IT.ITEMID IN (" + this.GetCollString(this.lstItem) + ")";
                    str = str + " Items: " + this.GetRetParam(this.lstItem);
                }
                string str3 = str2;
                str2 = (str3 + " GROUP BY " + this.GetField(this.grpGroup) + "," + this.GetField(this.grpDisplay)) + " ,U.UOMNAME " + ", ST.COSTPRICE ";
                this.SqlCmd = new SqlCommand();
                this.SqlCmd.Connection = GlobalVariables.SqlConn;
                this.SqlCmd.CommandText = str2;
                this.SqlCmd.CommandType = CommandType.Text;
                this.SDA.SelectCommand = this.SqlCmd;
                if (!object.ReferenceEquals(this.DS.Tables["STOCKTRFITEMSSUMMARY"], null))
                {
                    this.DS.Tables["STOCKTRFITEMSSUMMARY"].Clear();
                }
                this.SDA.Fill(this.DS, "STOCKTRFITEMSSUMMARY");
                //ParameterFields pfields = new ParameterFields();
                //ParameterField parameterField = new ParameterField
                //{
                //    Name = "Parameter"
                //};
                //ParameterDiscreteValue value2 = new ParameterDiscreteValue
                //{
                //    Value = str
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                if (this.DS.Tables["STOCKTRFITEMSSUMMARY"].Rows.Count > 0)
                {
                    ///By Bharat we have to change Reports to itextsparp pdf
                    ///
                    RptStockTrfItemSummary rpt = new RptStockTrfItemSummary(this.DS.Tables["STOCKTRFITEMSSUMMARY"]);
                    rpt.Parameter_parameter = str;
                    rpt.ShowDialog();
                    //rpt.SetDataSource(this.DS.Tables["STOCKTRFITEMSSUMMARY"]);
                    //rpt.Refresh();
                    //new FrmRptViewer(rpt, pfields).ShowDialog();
                }
                else
                {
                    MessageBox.Show("DATA NOT FOUND");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Print_Click");
            }
        }

       

        private void frmRptStockTransferItemSumm_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.RefreshData();
            this.AddHandlers();
        }

        private string GetCollString(ListView paramLvw)
        {
            try
            {
                string str = "";
                for (int i = 0; i < paramLvw.CheckedItems.Count; i++)
                {
                    if (str != "")
                    {
                        str = str + ", ";
                    }
                    str = str + Convert.ToString(paramLvw.CheckedItems[i].Tag);
                }
                return str;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in function Collection String");
                return "";
            }
        }

        private string GetField(GroupBox paramGrp)
        {
            try
            {
                foreach (RadioButton button in paramGrp.Controls)
                {
                    if (button.Checked)
                    {
                        if (button.Text.ToUpper() == "DEPARTMENT")
                        {
                            return "DP.DEPARTMENTNAME";
                        }
                        if (button.Text.ToUpper() == "CATEGORY")
                        {
                            return "CG.CATEGORYNAME";
                        }
                        if (button.Text.ToUpper() == "FRSTOCKPOINT")
                        {
                            return "SM.FRSTOCKPOINTNAME";
                        }
                        if (button.Text.ToUpper() == "TOSTOCKPOINT")
                        {
                            return "SM.TOSTOCKPOINTNAME";
                        }
                        if (button.Text.ToUpper() == "SECTION")
                        {
                            return "SC.SECTIONNAME";
                        }
                        if (button.Text.ToUpper() == "ITEM")
                        {
                            return "IT.ITEMNAME";
                        }
                        if (button.Text.ToUpper() == "COSTPRICE")
                        {
                            return "ST.COSTPRICE";
                        }
                        if (button.Text.ToUpper() == "TRANSFERNO")
                        {
                            return "SM.STOCKTRFNUMBER";
                        }
                        if (button.Text.ToUpper() == "TRANSFERDATE")
                        {
                            return "CONVERT(VARCHAR, SM.STOCKTRFDATE, 102)";
                        }
                        if (button.Text.ToUpper() == "MONTH")
                        {
                            return "'MONTH-' + CONVERT(VARCHAR, MONTH(SM.STOCKTRFDATE)) + '/' + CONVERT(VARCHAR, YEAR(SM.STOCKTRFDATE))";
                        }
                        if (button.Text.ToUpper() == "YEAR")
                        {
                            return "'YEAR-' + CONVERT(VARCHAR, YEAR(SM.STOCKTRFDATE))";
                        }
                    }
                }
                return "";
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in GetFied");
                return "";
            }
        }

        private string GetParamField(GroupBox paramGrp)
        {
            try
            {
                foreach (RadioButton button in paramGrp.Controls)
                {
                    if (button.Checked)
                    {
                        if (button.Text.ToUpper() == "DEPARTMENT")
                        {
                            return "DEPARTMENT";
                        }
                        if (button.Text.ToUpper() == "CATEGORY")
                        {
                            return "CATEGORY";
                        }
                        if (button.Text.ToUpper() == "FRSTOCKPOINT")
                        {
                            return "FRSTOCKPOINT";
                        }
                        if (button.Text.ToUpper() == "TOSTOCKPOINT")
                        {
                            return "TOSTOCKPOINT";
                        }
                        if (button.Text.ToUpper() == "SECTION")
                        {
                            return "SECTION";
                        }
                        if (button.Text.ToUpper() == "ITEM")
                        {
                            return "ITEM";
                        }
                        if (button.Text.ToUpper() == "COSTPRICE")
                        {
                            return "COSTPRICE";
                        }
                        if (button.Text.ToUpper() == "TRANSFERNO")
                        {
                            return "TRASNFERNO";
                        }
                        if (button.Text.ToUpper() == "TRANSFERDATE")
                        {
                            return "TRANSFERDATE";
                        }
                        if (button.Text.ToUpper() == "MONTH")
                        {
                            return "MONTH";
                        }
                        if (button.Text.ToUpper() == "YEAR")
                        {
                            return "YEAR";
                        }
                    }
                }
                return "";
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in GetFied");
                return "";
            }
        }

        private string GetRetParam(ListView ParamLstVw)
        {
            try
            {
                string str = "";
                for (int i = 0; i < ParamLstVw.CheckedItems.Count; i++)
                {
                    if (str != "")
                    {
                        str = str + ", ";
                    }
                    str = str + Convert.ToString(ParamLstVw.CheckedItems[i].Text);
                }
                return str;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in function Collection String");
                return "";
            }
        }

  

        private void lst_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            try
            {
                if (e.Item.Checked)
                {
                    ListView view = (ListView)sender;
                    if (view.CheckedItems.Count > 0)
                    {
                        foreach (Control control in view.Parent.Controls)
                        {
                            if (control.GetType() == typeof(RadioButton))
                            {
                                RadioButton button = (RadioButton)control;
                                if (button.Text.ToUpper().Contains("SELECTED"))
                                {
                                    button.Checked = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in ListViewChecked");
            }
        }

        private void rdAll_checkchanged(object sender, EventArgs e)
        {
            try
            {
                RadioButton button = (RadioButton)sender;
                if (button.Checked)
                {
                    foreach (Control control in button.Parent.Controls)
                    {
                        if (control.GetType() == typeof(ListView))
                        {
                            ListView view = (ListView)control;
                            foreach (ListViewItem item in view.CheckedItems)
                            {
                                item.Checked = false;
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in AllRd");
            }
        }

        private void RefreshData()
        {
            this.StrSql = "SELECT DEPARTMENTID, DEPARTMENTNAME FROM RES_DEPARTMENT";
            GlobalFill.FillListView(this.lstDepartment, GlobalFill.FillDataTable(this.StrSql));
            this.StrSql = "SELECT CATEGORYID, CATEGORYNAME FROM RES_CATEGORY";
            GlobalFill.FillListView(this.lstCategory, GlobalFill.FillDataTable(this.StrSql));
            this.StrSql = "SELECT SECTIONID, SECTIONNAME FROM RES_SECTION";
            GlobalFill.FillListView(this.lstSection, GlobalFill.FillDataTable(this.StrSql));
            this.StrSql = "SELECT ITEMID, ITEMNAME FROM RES_ITEM";
            GlobalFill.FillListView(this.lstItem, GlobalFill.FillDataTable(this.StrSql));
            this.StrSql = "SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT";
            GlobalFill.FillListView(this.lstFrStockpoint, GlobalFill.FillDataTable(this.StrSql));
            this.StrSql = "SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT";
            GlobalFill.FillListView(this.lstToStockPoint, GlobalFill.FillDataTable(this.StrSql));
            this.dtpTrfDateFrom.Value = GlobalVariables.BusinessDate;
            this.dtpTrfDateTo.Value = GlobalVariables.BusinessDate;
        }
    }
}
