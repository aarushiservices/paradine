﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.ReportForms
{
    partial class frmItemSalesSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlSalesSummary = new System.Windows.Forms.Panel();
            this.chkUOMNotReq = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.dtpToTime = new System.Windows.Forms.DateTimePicker();
            this.label22 = new System.Windows.Forms.Label();
            this.dtpFrTime = new System.Windows.Forms.DateTimePicker();
            this.label20 = new System.Windows.Forms.Label();
            this.CmbSumm = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.cmbRptType = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtToCode = new System.Windows.Forms.TextBox();
            this.txtFrCode = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbDepartment = new System.Windows.Forms.ComboBox();
            this.cmbSendTo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cmbTax = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbSection = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtToCustOrderNo = new System.Windows.Forms.TextBox();
            this.txtFrCustOrderNo = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dtpOrderDateTo = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpOrderDateFrom = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbMachineNo = new System.Windows.Forms.ComboBox();
            this.cmbCustomer = new System.Windows.Forms.ComboBox();
            this.cmbSteward = new System.Windows.Forms.ComboBox();
            this.chkPaymentList = new System.Windows.Forms.ListView();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbOrderType = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbSourceName = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbStockPoint = new System.Windows.Forms.ComboBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlSalesSummary.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSalesSummary
            // 
            this.pnlSalesSummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSalesSummary.Controls.Add(this.chkUOMNotReq);
            this.pnlSalesSummary.Controls.Add(this.groupBox4);
            this.pnlSalesSummary.Controls.Add(this.label20);
            this.pnlSalesSummary.Controls.Add(this.CmbSumm);
            this.pnlSalesSummary.Controls.Add(this.label19);
            this.pnlSalesSummary.Controls.Add(this.cmbRptType);
            this.pnlSalesSummary.Controls.Add(this.label18);
            this.pnlSalesSummary.Controls.Add(this.groupBox3);
            this.pnlSalesSummary.Controls.Add(this.label15);
            this.pnlSalesSummary.Controls.Add(this.cmbDepartment);
            this.pnlSalesSummary.Controls.Add(this.cmbSendTo);
            this.pnlSalesSummary.Controls.Add(this.label14);
            this.pnlSalesSummary.Controls.Add(this.cmbTax);
            this.pnlSalesSummary.Controls.Add(this.label2);
            this.pnlSalesSummary.Controls.Add(this.cmbSection);
            this.pnlSalesSummary.Controls.Add(this.label13);
            this.pnlSalesSummary.Controls.Add(this.cmbCategory);
            this.pnlSalesSummary.Controls.Add(this.groupBox2);
            this.pnlSalesSummary.Controls.Add(this.groupBox1);
            this.pnlSalesSummary.Controls.Add(this.label7);
            this.pnlSalesSummary.Controls.Add(this.cmbMachineNo);
            this.pnlSalesSummary.Controls.Add(this.cmbCustomer);
            this.pnlSalesSummary.Controls.Add(this.cmbSteward);
            this.pnlSalesSummary.Controls.Add(this.chkPaymentList);
            this.pnlSalesSummary.Controls.Add(this.label4);
            this.pnlSalesSummary.Controls.Add(this.label1);
            this.pnlSalesSummary.Controls.Add(this.label5);
            this.pnlSalesSummary.Controls.Add(this.label9);
            this.pnlSalesSummary.Controls.Add(this.cmbOrderType);
            this.pnlSalesSummary.Controls.Add(this.label8);
            this.pnlSalesSummary.Controls.Add(this.cmbSourceName);
            this.pnlSalesSummary.Controls.Add(this.label6);
            this.pnlSalesSummary.Controls.Add(this.cmbStockPoint);
            this.pnlSalesSummary.Location = new System.Drawing.Point(10, 12);
            this.pnlSalesSummary.Name = "pnlSalesSummary";
            this.pnlSalesSummary.Size = new System.Drawing.Size(469, 370);
            this.pnlSalesSummary.TabIndex = 0;
            // 
            // chkUOMNotReq
            // 
            this.chkUOMNotReq.AutoSize = true;
            this.chkUOMNotReq.Location = new System.Drawing.Point(305, 342);
            this.chkUOMNotReq.Name = "chkUOMNotReq";
            this.chkUOMNotReq.Size = new System.Drawing.Size(117, 17);
            this.chkUOMNotReq.TabIndex = 55;
            this.chkUOMNotReq.Text = "UOM Not Required";
            this.chkUOMNotReq.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.dtpToTime);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.dtpFrTime);
            this.groupBox4.Location = new System.Drawing.Point(321, 11);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(143, 77);
            this.groupBox4.TabIndex = 54;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Order Time :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(13, 49);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(26, 13);
            this.label21.TabIndex = 8;
            this.label21.Text = "To :";
            // 
            // dtpToTime
            // 
            this.dtpToTime.Checked = false;
            this.dtpToTime.CustomFormat = "";
            this.dtpToTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpToTime.Location = new System.Drawing.Point(40, 45);
            this.dtpToTime.Name = "dtpToTime";
            this.dtpToTime.ShowCheckBox = true;
            this.dtpToTime.Size = new System.Drawing.Size(98, 20);
            this.dtpToTime.TabIndex = 1;
            this.dtpToTime.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(3, 20);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(36, 13);
            this.label22.TabIndex = 6;
            this.label22.Text = "From :";
            // 
            // dtpFrTime
            // 
            this.dtpFrTime.Checked = false;
            this.dtpFrTime.CustomFormat = "";
            this.dtpFrTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpFrTime.Location = new System.Drawing.Point(40, 17);
            this.dtpFrTime.Name = "dtpFrTime";
            this.dtpFrTime.ShowCheckBox = true;
            this.dtpFrTime.Size = new System.Drawing.Size(98, 20);
            this.dtpFrTime.TabIndex = 0;
            this.dtpFrTime.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(160, 299);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(72, 13);
            this.label20.TabIndex = 53;
            this.label20.Text = "Report Type :";
            // 
            // CmbSumm
            // 
            this.CmbSumm.FormattingEnabled = true;
            this.CmbSumm.Location = new System.Drawing.Point(156, 315);
            this.CmbSumm.Name = "CmbSumm";
            this.CmbSumm.Size = new System.Drawing.Size(140, 21);
            this.CmbSumm.TabIndex = 52;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(308, 255);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(82, 13);
            this.label19.TabIndex = 51;
            this.label19.Text = "Display Criteria :";
            // 
            // cmbRptType
            // 
            this.cmbRptType.FormattingEnabled = true;
            this.cmbRptType.Location = new System.Drawing.Point(305, 269);
            this.cmbRptType.Name = "cmbRptType";
            this.cmbRptType.Size = new System.Drawing.Size(140, 21);
            this.cmbRptType.TabIndex = 50;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(159, 216);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(68, 13);
            this.label18.TabIndex = 49;
            this.label18.Text = "Department :";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.txtToCode);
            this.groupBox3.Controls.Add(this.txtFrCode);
            this.groupBox3.Location = new System.Drawing.Point(15, 88);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(127, 71);
            this.groupBox3.TabIndex = 47;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Item Code :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(18, 49);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(26, 13);
            this.label16.TabIndex = 25;
            this.label16.Text = "To :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 23);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(36, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "From :";
            // 
            // txtToCode
            // 
            this.txtToCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtToCode.Location = new System.Drawing.Point(46, 45);
            this.txtToCode.Name = "txtToCode";
            this.txtToCode.Size = new System.Drawing.Size(69, 20);
            this.txtToCode.TabIndex = 1;
            // 
            // txtFrCode
            // 
            this.txtFrCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFrCode.Location = new System.Drawing.Point(46, 19);
            this.txtFrCode.Name = "txtFrCode";
            this.txtFrCode.Size = new System.Drawing.Size(69, 20);
            this.txtFrCode.TabIndex = 0;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(309, 299);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 13);
            this.label15.TabIndex = 46;
            this.label15.Text = "Send To :";
            // 
            // cmbDepartment
            // 
            this.cmbDepartment.FormattingEnabled = true;
            this.cmbDepartment.Location = new System.Drawing.Point(156, 230);
            this.cmbDepartment.Name = "cmbDepartment";
            this.cmbDepartment.Size = new System.Drawing.Size(140, 21);
            this.cmbDepartment.TabIndex = 48;
            this.cmbDepartment.SelectionChangeCommitted += new System.EventHandler(this.cmbDepartment_SelectionChangeCommitted);
            this.cmbDepartment.TextChanged += new System.EventHandler(this.cmbDepartment_TextChanged);
            // 
            // cmbSendTo
            // 
            this.cmbSendTo.FormattingEnabled = true;
            this.cmbSendTo.Location = new System.Drawing.Point(305, 315);
            this.cmbSendTo.Name = "cmbSendTo";
            this.cmbSendTo.Size = new System.Drawing.Size(140, 21);
            this.cmbSendTo.TabIndex = 45;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(159, 255);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 13);
            this.label14.TabIndex = 44;
            this.label14.Text = "Tax :";
            // 
            // cmbTax
            // 
            this.cmbTax.FormattingEnabled = true;
            this.cmbTax.Location = new System.Drawing.Point(156, 269);
            this.cmbTax.Name = "cmbTax";
            this.cmbTax.Size = new System.Drawing.Size(140, 21);
            this.cmbTax.TabIndex = 43;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(308, 216);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 42;
            this.label2.Text = "Section :";
            // 
            // cmbSection
            // 
            this.cmbSection.FormattingEnabled = true;
            this.cmbSection.Location = new System.Drawing.Point(305, 230);
            this.cmbSection.Name = "cmbSection";
            this.cmbSection.Size = new System.Drawing.Size(140, 21);
            this.cmbSection.TabIndex = 40;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(13, 301);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 41;
            this.label13.Text = "Category :";
            // 
            // cmbCategory
            // 
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Location = new System.Drawing.Point(10, 315);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(140, 21);
            this.cmbCategory.TabIndex = 39;
            this.cmbCategory.SelectedIndexChanged += new System.EventHandler(this.cmbCategory_SelectedIndexChanged);
            this.cmbCategory.SelectionChangeCommitted += new System.EventHandler(this.cmbCategory_SelectionChangeCommitted);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.txtToCustOrderNo);
            this.groupBox2.Controls.Add(this.txtFrCustOrderNo);
            this.groupBox2.Location = new System.Drawing.Point(15, 11);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(127, 71);
            this.groupBox2.TabIndex = 38;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Customer Order No :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 49);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "To :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "From :";
            // 
            // txtToCustOrderNo
            // 
            this.txtToCustOrderNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtToCustOrderNo.Location = new System.Drawing.Point(46, 45);
            this.txtToCustOrderNo.Name = "txtToCustOrderNo";
            this.txtToCustOrderNo.Size = new System.Drawing.Size(69, 20);
            this.txtToCustOrderNo.TabIndex = 1;
            // 
            // txtFrCustOrderNo
            // 
            this.txtFrCustOrderNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFrCustOrderNo.Location = new System.Drawing.Point(46, 19);
            this.txtFrCustOrderNo.Name = "txtFrCustOrderNo";
            this.txtFrCustOrderNo.Size = new System.Drawing.Size(69, 20);
            this.txtFrCustOrderNo.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.dtpOrderDateTo);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dtpOrderDateFrom);
            this.groupBox1.Location = new System.Drawing.Point(150, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(165, 77);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Order Date :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(19, 48);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "To :";
            // 
            // dtpOrderDateTo
            // 
            this.dtpOrderDateTo.Checked = false;
            this.dtpOrderDateTo.CustomFormat = "dd/MMM/yyyy";
            this.dtpOrderDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOrderDateTo.Location = new System.Drawing.Point(47, 45);
            this.dtpOrderDateTo.Name = "dtpOrderDateTo";
            this.dtpOrderDateTo.ShowCheckBox = true;
            this.dtpOrderDateTo.Size = new System.Drawing.Size(104, 20);
            this.dtpOrderDateTo.TabIndex = 1;
            this.dtpOrderDateTo.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "From :";
            // 
            // dtpOrderDateFrom
            // 
            this.dtpOrderDateFrom.Checked = false;
            this.dtpOrderDateFrom.CustomFormat = "dd/MMM/yyyy";
            this.dtpOrderDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOrderDateFrom.Location = new System.Drawing.Point(47, 17);
            this.dtpOrderDateFrom.Name = "dtpOrderDateFrom";
            this.dtpOrderDateFrom.ShowCheckBox = true;
            this.dtpOrderDateFrom.Size = new System.Drawing.Size(104, 20);
            this.dtpOrderDateFrom.TabIndex = 0;
            this.dtpOrderDateFrom.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(156, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 36;
            this.label7.Text = "Machine No :";
            // 
            // cmbMachineNo
            // 
            this.cmbMachineNo.FormattingEnabled = true;
            this.cmbMachineNo.Location = new System.Drawing.Point(156, 113);
            this.cmbMachineNo.Name = "cmbMachineNo";
            this.cmbMachineNo.Size = new System.Drawing.Size(140, 21);
            this.cmbMachineNo.TabIndex = 35;
            // 
            // cmbCustomer
            // 
            this.cmbCustomer.FormattingEnabled = true;
            this.cmbCustomer.Location = new System.Drawing.Point(305, 152);
            this.cmbCustomer.Name = "cmbCustomer";
            this.cmbCustomer.Size = new System.Drawing.Size(140, 21);
            this.cmbCustomer.TabIndex = 29;
            // 
            // cmbSteward
            // 
            this.cmbSteward.FormattingEnabled = true;
            this.cmbSteward.Location = new System.Drawing.Point(305, 113);
            this.cmbSteward.Name = "cmbSteward";
            this.cmbSteward.Size = new System.Drawing.Size(140, 21);
            this.cmbSteward.TabIndex = 28;
            // 
            // chkPaymentList
            // 
            this.chkPaymentList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.chkPaymentList.CheckBoxes = true;
            this.chkPaymentList.Location = new System.Drawing.Point(10, 181);
            this.chkPaymentList.Name = "chkPaymentList";
            this.chkPaymentList.Size = new System.Drawing.Size(124, 117);
            this.chkPaymentList.TabIndex = 23;
            this.chkPaymentList.UseCompatibleStateImageBehavior = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(309, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 31;
            this.label4.Text = "Steward :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 166);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "Payment Mode :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(308, 139);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Customer :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(308, 178);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 13);
            this.label9.TabIndex = 34;
            this.label9.Text = "Order Type :";
            // 
            // cmbOrderType
            // 
            this.cmbOrderType.FormattingEnabled = true;
            this.cmbOrderType.Location = new System.Drawing.Point(305, 191);
            this.cmbOrderType.Name = "cmbOrderType";
            this.cmbOrderType.Size = new System.Drawing.Size(140, 21);
            this.cmbOrderType.TabIndex = 27;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(159, 178);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 33;
            this.label8.Text = "Source Name :";
            // 
            // cmbSourceName
            // 
            this.cmbSourceName.FormattingEnabled = true;
            this.cmbSourceName.Location = new System.Drawing.Point(156, 191);
            this.cmbSourceName.Name = "cmbSourceName";
            this.cmbSourceName.Size = new System.Drawing.Size(140, 21);
            this.cmbSourceName.TabIndex = 26;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(158, 139);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "StockPoint";
            // 
            // cmbStockPoint
            // 
            this.cmbStockPoint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStockPoint.FormattingEnabled = true;
            this.cmbStockPoint.Location = new System.Drawing.Point(156, 152);
            this.cmbStockPoint.Name = "cmbStockPoint";
            this.cmbStockPoint.Size = new System.Drawing.Size(140, 21);
            this.cmbStockPoint.TabIndex = 25;
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(164, 396);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(76, 27);
            this.btnPrint.TabIndex = 1;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(261, 396);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 27);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmItemSalesSummary
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(491, 435);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.pnlSalesSummary);
            this.Name = "frmItemSalesSummary";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sales Summary";
            this.Load += new System.EventHandler(this.frmItemSalesSummary_Load);
            this.pnlSalesSummary.ResumeLayout(false);
            this.pnlSalesSummary.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
    }
}