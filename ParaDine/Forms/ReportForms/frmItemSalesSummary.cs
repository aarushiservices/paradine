﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using ParaDine.Reports;
using ParaDinePrint;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmItemSalesSummary : Form
    {
        private Button btnExit;
        private Button btnPrint;
        private ListView chkPaymentList;
        private CheckBox chkUOMNotReq;
        private ComboBox cmbCategory;
        private ComboBox cmbCustomer;
        private ComboBox cmbDepartment;
        private ComboBox cmbMachineNo;
        private ComboBox cmbOrderType;
        private ComboBox cmbRptType;
        private ComboBox cmbSection;
        private ComboBox cmbSendTo;
        private ComboBox cmbSourceName;
        private ComboBox cmbSteward;
        private ComboBox cmbStockPoint;
        private ComboBox CmbSumm;
        private ComboBox cmbTax;
       // private IContainer components = null;
        private DataSet DS = new DataSet();
        private DateTimePicker dtpFrTime;
        private DateTimePicker dtpOrderDateFrom;
        private DateTimePicker dtpOrderDateTo;
        private DateTimePicker dtpToTime;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private GroupBox groupBox3;
        private GroupBox groupBox4;
        private Label label1;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label13;
        private Label label14;
        private Label label15;
        private Label label16;
        private Label label17;
        private Label label18;
        private Label label19;
        private Label label2;
        private Label label20;
        private Label label21;
        private Label label22;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private PrintingFunctions pfunctions = new PrintingFunctions();
        private Panel pnlSalesSummary;
        private ClsColDosPrint[] prnItem;
        private ClsRptDosPrint prnrpt = new ClsRptDosPrint();
        private string rptname = "";
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private SqlCommand SqlCmd;
        private string StrSql = "";
        private TextBox txtFrCode;
        private TextBox txtFrCustOrderNo;
        private TextBox txtToCode;
        private TextBox txtToCustOrderNo;

        public frmItemSalesSummary()
        {
            this.InitializeComponent();
        }

        private bool BillPageHeader(ref ClsRptDosPrint ParamClsRpt, string ParamWhere)
        {
            try
            {
                return true;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Bill Page Header");
                return false;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                int num;
                string str = "";
                string paramSql = "";
                paramSql = "SELECT ";
                if (this.cmbRptType.SelectedIndex == 1)
                {
                    paramSql = paramSql + " CG.CATEGORYID AS ID, CG.CATEGORYNAME AS PRODUCT, ";
                }
                else if (this.cmbRptType.SelectedIndex == 2)
                {
                    paramSql = paramSql + " DP.DEPARTMENTID AS ID, DP.DEPARTMENTNAME AS PRODUCT, ";
                }
                else if (this.cmbRptType.SelectedIndex == 3)
                {
                    paramSql = paramSql + " SC.SECTIONID AS ID, SC.SECTIONNAME AS PRODUCT, ";
                }
                else if (this.cmbRptType.SelectedIndex == 4)
                {
                    paramSql = paramSql + " TX.TAXID AS ID, TX.TAXNAME AS PRODUCT, ";
                }
                else
                {
                    paramSql = paramSql + " IT.ITEMID AS ID, IT.ITEMNAME AS PRODUCT, ";
                }
                paramSql = paramSql + " CG.CATEGORYNAME,";
                if (!this.chkUOMNotReq.Checked)
                {
                    paramSql = paramSql + " COI.UOMNAME, ";
                }
                paramSql = paramSql + " SUM(TOTQTY) AS TOTQTY, 0 AS COSTVALUE, SUM(ITEMGROSSAMT) AS SALEVALUE,  SUM(ITEMDISCAMT) AS ITEMDISCAMT, SUM(ITEMTAXAMT) AS ITEMTAXAMT,  SUM(SERVICETAXAMT) AS ITEMSRVTAXAMT, 0 AS BILLTAX,   SUM(IBD.ITEMBILLDISC) AS ITEMBILLDISC, SUM(IBD.ITEMBILLTAX) AS ITEMBILLTAX,  SUM(TOTALAMT) - SUM(IBD.ITEMBILLDISC) + SUM(IBD.ITEMBILLTAX) AS NETAMOUNT  FROM RES_VW_CUSTORDERITEM COI   INNER JOIN RES_VW_CUSTORDERKOT COK ON COK.CUSTORDERKOTID = COI.CUSTORDERKOTID   INNER JOIN RES_VW_CUSTORDERMASTER COM ON COM.CUSTORDERMASTERID = COK.CUSTORDERMASTERID   INNER JOIN RES_FUNC_ITEMBILLDEDUCTIONS() IBD ON IBD.CUSTORDERITEMID = COI.CUSTORDERITEMID   INNER JOIN RES_ITEM IT ON IT.ITEMID = COI.ITEMID   INNER JOIN RES_SECTION SC ON SC.SECTIONID = IT.SECTIONID   INNER JOIN RES_CATEGORY CG ON CG.CATEGORYID = IT.CATEGORYID   INNER JOIN RES_DEPARTMENT DP ON DP.DEPARTMENTID = CG.DEPARTMENTID   INNER JOIN RES_TAX TX ON TX.TAXID = IT.TAXID " + " WHERE IT.ITEMID <> 0 ";
                if (this.dtpOrderDateFrom.Checked)
                {
                    paramSql = paramSql + " AND CONVERT(VARCHAR, COM.CUSTORDERDATE, 112) >= '" + this.dtpOrderDateFrom.Value.ToString("yyyyMMdd") + "'";
                    str = "Orderdate From : " + this.dtpOrderDateFrom.Text + "; ";
                }
                if (this.dtpOrderDateTo.Checked)
                {
                    paramSql = paramSql + " AND CONVERT(VARCHAR, COM.CUSTORDERDATE, 112) <= '" + this.dtpOrderDateTo.Value.ToString("yyyyMMdd") + "'";
                    str = str + "OrderDate UpTo : " + this.dtpOrderDateTo.Text + "; ";
                }
                if (Convert.ToInt16(this.cmbMachineNo.SelectedValue) > 0)
                {
                    paramSql = paramSql + " AND COM.MACHINENO = " + this.cmbMachineNo.SelectedValue.ToString();
                    str = str + "MNO : " + this.cmbMachineNo.Text + "; ";
                }
                if (this.chkPaymentList.CheckedItems.Count > 0)
                {
                    string str3 = "(";
                    string text = "";
                    str = str + " Payment Mode is ";
                    for (num = 0; num <= (this.chkPaymentList.Items.Count - 1); num++)
                    {
                        if (this.chkPaymentList.Items[num].Checked)
                        {
                            str = str + this.chkPaymentList.Items[num].Text + ", ";
                            if (text.Trim() != "")
                            {
                                str3 = str3 + " PAYMENTLIST LIKE '%" + text + "%' OR";
                            }
                            text = this.chkPaymentList.Items[num].Text;
                        }
                    }
                    if (text.Trim() != "")
                    {
                        paramSql = paramSql + " AND " + str3 + " PAYMENTLIST LIKE '%" + text + "%')";
                    }
                }
                if (this.txtFrCustOrderNo.Text.Trim() != "")
                {
                    paramSql = paramSql + " AND COM.CUSTORDERNO >= '" + this.txtFrCustOrderNo.Text + "'";
                    str = str + "Order From: " + this.txtFrCustOrderNo.Text + "; ";
                }
                if (this.txtToCustOrderNo.Text.Trim() != "")
                {
                    paramSql = paramSql + " AND COM.CUSTORDERNO <= '" + this.txtToCustOrderNo.Text + "'";
                    str = str + "Order UpTo : " + this.txtToCustOrderNo.Text + "; ";
                }
                if (this.txtFrCode.Text.Trim() != "")
                {
                    paramSql = paramSql + " AND COI.ITEMCODE = '" + this.txtFrCode.Text + "'";
                    str = str + "ItemCode From: " + this.txtFrCode.Text + "; ";
                }
                if (this.txtToCode.Text.Trim() != "")
                {
                    paramSql = paramSql + " AND COI.ITEMCODE = '" + this.txtToCode.Text + "'";
                    str = str + "Item Code UpTo : " + this.txtToCode.Text + "; ";
                }
                if (Convert.ToInt16(this.cmbStockPoint.SelectedValue) > 0)
                {
                    paramSql = paramSql + " AND COM.STOCKPOINTID = " + this.cmbStockPoint.SelectedValue;
                    str = str + "Stock Point : " + this.cmbStockPoint.Text + "; ";
                }
                if (Convert.ToInt16(this.cmbSteward.SelectedValue) > 0)
                {
                    paramSql = paramSql + " AND COM.STEWARDID = " + this.cmbSteward.SelectedValue;
                    str = str + "Steward : " + this.cmbSteward.Text + "; ";
                }
                if (Convert.ToInt16(this.cmbSourceName.SelectedValue) > 0)
                {
                    paramSql = paramSql + " AND COM.SOURCETYPEID = " + this.cmbSourceName.SelectedValue;
                    str = str + "Source Name : " + this.cmbSourceName.Text + "; ";
                }
                if (Convert.ToInt16(this.cmbCustomer.SelectedValue) > 0)
                {
                    paramSql = paramSql + " AND COM.CUSTOMERID = " + this.cmbCustomer.SelectedValue;
                    str = str + " Customer :" + this.cmbCustomer.Text + "; ";
                }
                if (Convert.ToInt16(this.cmbDepartment.SelectedValue) > 0)
                {
                    paramSql = paramSql + " AND CG.DEPARTMENTID = " + this.cmbDepartment.SelectedValue;
                    str = str + "Department :" + this.cmbDepartment.Text + "; ";
                }
                if (Convert.ToInt16(this.cmbCategory.SelectedValue) > 0)
                {
                    paramSql = paramSql + " AND CG.CATEGORYID = " + this.cmbCategory.SelectedValue;
                    str = str + "Category : " + this.cmbCategory.Text + "; ";
                }
                if (Convert.ToInt16(this.cmbSection.SelectedValue) > 0)
                {
                    paramSql = paramSql + " AND SC.SECTIONID = " + this.cmbSection.SelectedValue;
                    str = str + "Section : " + this.cmbSection.Text + "; ";
                }
                if (Convert.ToInt16(this.cmbTax.SelectedValue) > 0)
                {
                    paramSql = paramSql + " AND IT.TAXID = " + this.cmbTax.SelectedValue;
                    str = str + "Tax : " + this.cmbTax.Text + "; ";
                }
                if (this.cmbOrderType.Text != "")
                {
                    if (this.cmbOrderType.Text == "KOT BILLS")
                    {
                        paramSql = paramSql + " AND COM.DIRECTBILL = 0 ";
                    }
                    else if (this.cmbOrderType.Text == "DIRECT BILLS")
                    {
                        paramSql = paramSql + " AND COM.DIRECTBILL = 1 ";
                    }
                    str = str + "Order Type is " + this.cmbOrderType.Text + " ; ";
                }
                paramSql = paramSql + " GROUP BY ";
                if (this.cmbRptType.SelectedIndex == 1)
                {
                    paramSql = paramSql + " CG.CATEGORYID, CG.CATEGORYNAME ";
                }
                else if (this.cmbRptType.SelectedIndex == 2)
                {
                    paramSql = paramSql + " DP.DEPARTMENTID, DP.DEPARTMENTNAME ";
                }
                else if (this.cmbRptType.SelectedIndex == 3)
                {
                    paramSql = paramSql + " SC.SECTIONID, SC.SECTIONNAME ";
                }
                else if (this.cmbRptType.SelectedIndex == 4)
                {
                    paramSql = paramSql + " TX.TAXID, TX.TAXNAME ";
                }
                else
                {
                    paramSql = paramSql + " IT.ITEMID, IT.ITEMNAME,CG.CATEGORYNAME ";
                }
                if (!this.chkUOMNotReq.Checked)
                {
                    paramSql = paramSql + " , COI.UOMNAME ";
                }
                GlobalFill.FillDataSet(paramSql, "SALESSUMMARY", this.DS, this.SDA);
                //ParameterFields pfields = new ParameterFields();
                //ParameterField parameterField = new ParameterField
                //{
                //    Name = "CompanyName"
                //};
                //ParameterDiscreteValue value2 = new ParameterDiscreteValue
                //{
                //    Value = GlobalVariables.StrCompName
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                //parameterField = new ParameterField
                //{
                //    Name = "Address"
                //};
                //value2 = new ParameterDiscreteValue
                //{
                //    Value = Convert.ToString(GlobalFunctions.GetQueryValue("SELECT TOP 1 ADDRESS FROM GLB_COMPANY"))
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                //parameterField = new ParameterField
                //{
                //    Name = "RestaurantName"
                //};
                //value2 = new ParameterDiscreteValue
                //{
                //    Value = this.cmbStockPoint.Text
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                //parameterField = new ParameterField
                //{
                //    Name = "Parameter"
                //};
                //value2 = new ParameterDiscreteValue
                //{
                //    Value = str
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                if (this.DS.Tables["SALESSUMMARY"].Rows.Count > 0)
                {
                    if (this.CmbSumm.SelectedIndex == 1)
                    {
                        ///By Bharat we have to change Reports to itextsparp pdf
                        ///
                        RptItemProfitSummary rpt = new RptItemProfitSummary(this.DS.Tables["SALESSUMMARY"]);
                        rpt.Parameter_CompanyName=GlobalVariables.StrCompName;
                        rpt.Parameter_GrpProfit="";
                        rpt.Parameter_GrpSumProfit="";
                        rpt.Parameter_Parameter= str;
                        rpt.Parameter_Profit="";
                        rpt.Parameter_Profitpercentage="";
                        rpt.Parameter_TotProfit="";
                        rpt.Parameter_TotSumProfit="";
                        rpt.ShowDialog();
                        //rpt.SetDataSource(this.DS.Tables["SALESSUMMARY"]);
                        //FrmRptViewer viewer = new FrmRptViewer(rpt, pfields);
                        //viewer.ShowDialog();
                    }
                    else if (this.cmbSendTo.SelectedIndex == 0)
                    {
                        //parameterField = new ParameterField
                        //{
                        //    Name = "TAXINCL"
                        //};
                        //value2 = new ParameterDiscreteValue
                        //{
                        //    Value = Convert.ToBoolean(GlobalFunctions.GetQueryValue("SELECT BILLTAXINCL FROM RES_BILLSETTINGS"))
                        //};
                        //parameterField.CurrentValues.Add((ParameterValue)value2);
                        //pfields.Add(parameterField);
                        //parameterField = new ParameterField
                        //{
                        //    Name = "BILLTAXPERC"
                        //};
                        //value2 = new ParameterDiscreteValue
                        //{
                        //    Value = Convert.ToDouble(GlobalFunctions.GetQueryValue("SELECT BILLTAX FROM RES_BILLSETTINGS"))
                        //};
                        //parameterField.CurrentValues.Add((ParameterValue)value2);
                        //pfields.Add(parameterField);
                        ///By Bharat we have to change Reports to itextsparp pdf
                        ///
                        RptItemSalesSummary2 summary2 = new RptItemSalesSummary2(this.DS.Tables["SALESSUMMARY"]);
                        summary2.Parameter_Address=Convert.ToString(GlobalFunctions.GetQueryValue("SELECT TOP 1 ADDRESS FROM GLB_COMPANY"));
                        summary2.Parameter_BasicSalVal="";
                        summary2.Parameter_BillTaxPerc="";
                        summary2.Parameter_CompanyName= GlobalVariables.StrCompName;
                        summary2.Parameter_Parameter=str;
                        summary2.Parameter_VatAmt="";
                        summary2.ShowDialog();
                        //summary2.SetDataSource(this.DS.Tables["SALESSUMMARY"]);
                        //new FrmRptViewer(summary2, pfields).ShowDialog();
                    }
                    else
                    {
                        this.rptname = "SALESSUMMARY";
                        this.pfunctions.SETREPORTDATA(ref this.prnrpt, this.rptname, this.DS, this.SDA, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
                        this.pfunctions.SETCOLDATA(ref this.prnItem, this.rptname, this.DS, this.SDA, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
                        this.BillPageHeader(ref this.prnrpt, "");
                        string str5 = "";
                        for (num = 0; num <= (this.prnItem.Length - 1); num++)
                        {
                            string str6 = "";
                            if (this.prnItem[num].ISGroupHeader)
                            {
                                str6 = (this.prnItem[num].ColumnPosition + 1) + ",";
                            }
                            str5 = str5 + str6;
                        }
                        new ClsDosPrint(this.DS.Tables["SALESSUMMARY"], this.prnItem, this.prnrpt, GlobalVariables.SqlConn, GlobalVariables.PrinterId).PrintData(this.cmbSendTo.Text, "");
                    }
                }
                else
                {
                    MessageBox.Show("DATA NOT FOUND");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Printing Data");
            }
        }

        private void cmbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void cmbCategory_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (Convert.ToInt16(this.cmbSection.SelectedValue) > 0)
            {
                this.cmbDepartment.SelectedValue = new Category(Convert.ToInt16(this.cmbCategory.SelectedValue)).DepartmentID;
            }
        }

        private void cmbDepartment_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (Convert.ToInt16(this.cmbDepartment.SelectedValue) > 0)
            {
                GlobalFill.FillCombo("Select CATEGORYID, CATEGORYNAME FROM RES_CATEGORY WHERE DEPARTMENTID = " + Convert.ToString(Convert.ToInt32(this.cmbDepartment.SelectedValue)) + " ORDER BY 2", this.cmbCategory);
            }
        }

        private void cmbDepartment_TextChanged(object sender, EventArgs e)
        {
            if (this.cmbDepartment.Text.Trim() == "")
            {
                GlobalFill.FillCombo("Select CATEGORYID, CATEGORYNAME FROM RES_CATEGORY WHERE DEPARTMENTID = " + Convert.ToString(Convert.ToInt32(this.cmbDepartment.SelectedValue)) + " ORDER BY 2", this.cmbCategory);
            }
        }

       

        private void frmItemSalesSummary_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.RefreshData();
        }

     

        private void RefreshData()
        {
            GlobalFill.FillCombo("SELECT TERMINALID, TERMINALNAME FROM GLB_TERMINAL ORDER BY 2", this.cmbMachineNo);
            GlobalFill.FillCombo("SELECT STEWARDID, STEWARDNAME FROM RES_STEWARD ORDER BY 2", this.cmbSteward);
            GlobalFill.FillCombo("SELECT CUSTOMERID, CUSTOMERNAME FROM RES_CUSTOMER ORDER BY 2", this.cmbCustomer);
            GlobalFill.FillCombo("SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT WHERE ISBILLPOINT = 1 ORDER BY 2", this.cmbStockPoint);
            this.cmbStockPoint.SelectedValue = GlobalVariables.StockPointId;
            GlobalFill.FillCombo("SELECT CATEGORYID, CATEGORYNAME FROM RES_CATEGORY ORDER BY 2", this.cmbCategory);
            GlobalFill.FillCombo("SELECT SECTIONID, SECTIONNAME FROM RES_SECTION ORDER BY 2", this.cmbSection);
            GlobalFill.FillCombo("SELECT TAXID, TAXNAME FROM RES_TAX ORDER BY 2", this.cmbTax);
            GlobalFill.FillCombo("SELECT TABLEID, TABLENAME FROM RES_TABLE ORDER BY 2", this.cmbSourceName);
            GlobalFill.FillCombo("SELECT DEPARTMENTID, DEPARTMENTNAME FROM RES_DEPARTMENT ORDER BY 2", this.cmbDepartment);
            this.cmbOrderType.Items.Clear();
            this.cmbOrderType.Items.Add("KOT BILLS");
            this.cmbOrderType.Items.Add("DIRECT BILLS");
            this.cmbOrderType.SelectedIndex = -1;
            this.cmbSendTo.Items.Clear();
            this.cmbSendTo.Items.Add("SCREEN");
            this.cmbSendTo.Items.Add("PRINTER");
            this.cmbSendTo.Items.Add("DISK");
            this.cmbSendTo.SelectedIndex = 0;
            this.cmbRptType.Items.Clear();
            this.cmbRptType.Items.Add("Item Wise");
            this.cmbRptType.Items.Add("Category Wise");
            this.cmbRptType.Items.Add("Department Wise");
            this.cmbRptType.Items.Add("Section Wise");
            this.cmbRptType.Items.Add("Tax Wise");
            this.CmbSumm.Items.Clear();
            this.CmbSumm.Items.Add("Summary Report");
            this.CmbSumm.Items.Add("Profit Analysis Report");
            this.CmbSumm.SelectedIndex = 0;
            this.StrSql = "SELECT PAYMENTMODEID, PAYMENTMODE FROM GLB_PAYMENTMODE ORDER BY 2";
            GlobalFill.FillDataSet(this.StrSql, "PAYMENTMODE", this.DS, this.SDA);
            if (this.DS.Tables["PAYMENTMODE"].Rows.Count > 0)
            {
                foreach (DataRow row in this.DS.Tables["PAYMENTMODE"].Rows)
                {
                    this.chkPaymentList.Items.Add(Convert.ToString(row["PAYMENTMODE"]));
                }
            }
            GlobalFill.FillListView(this.chkPaymentList, this.DS.Tables["PAYMENTMODE"]);
            this.dtpOrderDateFrom.Value = GlobalVariables.BusinessDate;
            this.dtpOrderDateTo.Value = GlobalVariables.BusinessDate;
        }
    }
}
