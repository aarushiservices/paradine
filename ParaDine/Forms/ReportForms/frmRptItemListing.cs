﻿using ParaDine.Forms.GlobalForms;
using ParaDine.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmRptItemListing : Form
    {
        private Button BtnExit;
        private Button btnPrint;
        private ComboBox cmbCategory;
        private ComboBox cmbSection;
        private ComboBox cmbStockPoint;
        private ComboBox cmbTax;
        private ComboBox cmbUOM;
       // private IContainer components = null;
        private DataSet Ds = new DataSet();
        private GroupBox groupBox2;
        private Label label1;
        private Label label11;
        private Label label12;
        private Label label2;
        private Label label3;
        private Label label6;
        private Label label8;
        private Panel panel1;
        private SqlDataAdapter Sda = new SqlDataAdapter();
        private string StrSql = "";
        private TextBox txtItemCodeFrom;
        private TextBox txtItemCodeTo;

        public frmRptItemListing()
        {
            this.InitializeComponent();
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.cmbStockPoint.Text.Trim() != "")
                {
                    this.StrSql = "SELECT I.ITEMID, I.ITEMCODE, I.ITEMNAME, I.CATEGORYNAME, I.SECTIONNAME, I.TAXNAME, U.UOMNAME, IU.SALEPRICE FROM RES_VW_ITEM I, RES_ITEMUNITPRICE IU, RES_UOM U, RES_STOCKPOINT SP  WHERE IU.ITEMID = I.ITEMID AND IU.UOMID = U.UOMID AND IU.STOCKPOINTID = SP.STOCKPOINTID and IU.STOCKPOINTID = " + this.cmbStockPoint.SelectedValue;
                }
                else
                {
                    this.StrSql = "SELECT I.ITEMID, I.ITEMCODE, I.ITEMNAME, I.CATEGORYNAME, I.SECTIONNAME, I.TAXNAME, I.UOMNAME, I.SALEPRICE FROM RES_VW_ITEM I WHERE ITEMCODE <> '' ";
                }
                if (this.txtItemCodeFrom.Text.Trim() != "")
                {
                    this.StrSql = this.StrSql + " AND ITEMCODE >= '" + this.txtItemCodeFrom.Text.Trim() + "'";
                }
                if (this.txtItemCodeTo.Text.Trim() != "")
                {
                    this.StrSql = this.StrSql + " AND ITEMCODE <= '" + this.txtItemCodeTo.Text.Trim() + "'";
                }
                if (this.cmbCategory.Text.Trim() != "")
                {
                    this.StrSql = this.StrSql + " AND CATEGORYNAME = '" + this.cmbCategory.Text + "'";
                }
                if (this.cmbSection.Text.Trim() != "")
                {
                    this.StrSql = this.StrSql + " AND SECTIONNAME = '" + this.cmbSection.Text + "'";
                }
                if (this.cmbTax.Text.Trim() != "")
                {
                    this.StrSql = this.StrSql + " AND TAXNAME = '" + this.cmbTax.Text + "'";
                }
                if (this.cmbUOM.Text.Trim() != "")
                {
                    this.StrSql = this.StrSql + " AND U.UOMNAME = '" + this.cmbUOM.Text + "'";
                }
                GlobalFill.FillDataSet(this.StrSql, "ITEMLISTING", this.Ds, this.Sda);
                if (this.Ds.Tables["ITEMLISTING"].Rows.Count > 0)
                {
                    ///By Bharat we have to change Reports to itextsparp pdf
                    ///
                    RptItemListing rpt = new RptItemListing(this.Ds.Tables["ITEMLISTING"]);
                    
                    rpt.ShowDialog();
                    //rpt.SetDataSource(this.Ds.Tables["ITEMLISTING"]);
                    //new FrmRptViewer(rpt).ShowDialog();
                }
                else
                {
                    MessageBox.Show("NO DATA FOUND!");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

      

        private void frmRptItemListing_Load(object sender, EventArgs e)
        {
            try
            {
                new GlobalTheme().applyTheme(this);
                this.RefreshData();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

    

        private void RefreshData()
        {
            GlobalFill.FillCombo("SELECT CATEGORYID, CATEGORYNAME FROM RES_CATEGORY ORDER BY 2", this.cmbCategory);
            GlobalFill.FillCombo("SELECT SECTIONID, SECTIONNAME FROM RES_SECTION ORDER BY 2", this.cmbSection);
            GlobalFill.FillCombo("SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT ORDER BY 2", this.cmbStockPoint);
            GlobalFill.FillCombo("SELECT TAXID, TAXNAME FROM RES_TAX ORDER BY 2", this.cmbTax);
            GlobalFill.FillCombo("SELECT UOMID, UOMNAME FROM RES_UOM ORDER BY 2", this.cmbUOM);
        }
    }
}
