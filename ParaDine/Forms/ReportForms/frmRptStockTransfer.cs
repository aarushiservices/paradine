﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using ParaDine.Reports;
using ParaDinePrint;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmRptStockTransfer : Form
    {
        private Button btnExit;
        private Button btnPrint;
        private ComboBox cmbfrStockPoint;
        private ComboBox cmbPrintOn;
        private ComboBox cmbToStockPoint;
        //private IContainer components = null;
        private DataSet DS = new DataSet();
        private DateTimePicker DtpFrStockTrfDate;
        private DateTimePicker DtpToStockTrfDate;
        private Panel grpfields;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label7;
        private PrintingFunctions pfunctions = new PrintingFunctions();
        private ClsColDosPrint[] prnItem;
        private ClsRptDosPrint prnrpt = new ClsRptDosPrint();
        private string rptname = "";
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private string StrSql = "";

        public frmRptStockTransfer()
        {
            this.InitializeComponent();
        }

        private bool BillPageHeader(ref ClsRptDosPrint ParamClsRpt, string ParamWhere)
        {
            try
            {
                return true;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Bill Page Header");
                return false;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                string str = "";
                this.StrSql = "SELECT * FROM RES_VW_STOCKTRFMASTER WHERE STOCKTRFNUMBER <> ''";
                if (this.DtpFrStockTrfDate.Checked)
                {
                    this.StrSql = this.StrSql + " AND  STOCKTRFDATE >= '" + this.DtpFrStockTrfDate.Text + "'";
                    str = str + " From : " + this.DtpFrStockTrfDate.Text + "; ";
                }
                if (this.DtpToStockTrfDate.Checked)
                {
                    this.StrSql = this.StrSql + " AND  STOCKTRFDATE <= '" + this.DtpToStockTrfDate.Text + "'";
                    str = str + " To : " + this.DtpToStockTrfDate.Text + "; ";
                }
                if (this.cmbfrStockPoint.Text.Trim() != "")
                {
                    this.StrSql = this.StrSql + " AND FRSTOCKPOINTID = " + Convert.ToString(this.cmbfrStockPoint.SelectedValue);
                    str = str + " FROM : " + this.cmbfrStockPoint.Text + "; ";
                }
                if (this.cmbToStockPoint.Text.Trim() != "")
                {
                    this.StrSql = this.StrSql + " AND TOSTOCKPOINTID = " + Convert.ToString(this.cmbToStockPoint.SelectedValue);
                    str = str + " TO : " + this.cmbToStockPoint.Text + "; ";
                }
                GlobalFill.FillDataSet(this.StrSql, "STOCKTRFSUMM", this.DS, this.SDA);
                //ParameterFields pfields = new ParameterFields();
                //ParameterField parameterField = new ParameterField
                //{
                //    Name = "Parameter"
                //};
                //ParameterDiscreteValue value2 = new ParameterDiscreteValue
                //{
                //    Value = str
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                if (this.DS.Tables["STOCKTRFSUMM"].Rows.Count > 0)
                {
                    if (this.cmbPrintOn.Text == "SCREEN")
                    {
                        ///By Bharat we have to change Reports to itextsparp pdf
                        ///
                        RptStockTrfBillSumm rpt = new RptStockTrfBillSumm(this.DS.Tables["STOCKTRFSUMM"]);
                        rpt.Parameter_parameter = str;
                        rpt.ShowDialog();
                        //rpt.SetDataSource(this.DS.Tables["STOCKTRFSUMM"]);
                        //rpt.Refresh();
                        //new FrmRptViewer(rpt, pfields).ShowDialog();
                    }
                    else
                    {
                        this.rptname = "STOCK_TRANSFER_BILLS_SUMMARY";
                        this.pfunctions.SETREPORTDATA(ref this.prnrpt, this.rptname, this.DS, this.SDA, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
                        this.pfunctions.SETCOLDATA(ref this.prnItem, this.rptname, this.DS, this.SDA, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
                        this.BillPageHeader(ref this.prnrpt, "");
                        string str2 = "";
                        for (int i = 0; i <= (this.prnItem.Length - 1); i++)
                        {
                            string str3 = "";
                            if (this.prnItem[i].ISGroupHeader)
                            {
                                str3 = (this.prnItem[i].ColumnPosition + 1) + ",";
                            }
                            str2 = str2 + str3;
                        }
                        new ClsDosPrint(this.DS.Tables["STOCKTRFSUMM"], this.prnItem, this.prnrpt, GlobalVariables.SqlConn, GlobalVariables.PrinterId).PrintData(this.cmbPrintOn.Text, "");
                    }
                }
                else
                {
                    MessageBox.Show("DATA NOT FOUND!");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In Print_Click");
            }
        }

   

        private void frmRptStockTransfer_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.RefreshData();
        }

      

        private void RefreshData()
        {
            GlobalFill.FillCombo("SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT ORDER BY 2", this.cmbfrStockPoint);
            GlobalFill.FillCombo("SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT ORDER BY 2", this.cmbToStockPoint);
            this.DtpFrStockTrfDate.Value = GlobalVariables.BusinessDate;
            this.DtpToStockTrfDate.Value = GlobalVariables.BusinessDate;
            this.cmbPrintOn.Items.Clear();
            this.cmbPrintOn.Items.Add("SCREEN");
            this.cmbPrintOn.Items.Add("PRINTER");
            this.cmbPrintOn.Items.Add("DISK");
            this.cmbPrintOn.SelectedIndex = 0;
        }
    }
}
