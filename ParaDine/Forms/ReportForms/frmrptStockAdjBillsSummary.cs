﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmrptStockAdjBillsSummary : Form
    {
        private Button btnExit;
        private Button btnPrint;
        private ComboBox cmbOutletType;
        private ComboBox cmbPrintOn;
        //private IContainer components = null;
        private DateTimePicker DtpFrDate;
        private DateTimePicker DtpToDate;
        private GroupBox groupBox1;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label5;
        private Label label7;
        private Panel panel1;
        private string StrSql = "";
        private TextBox txtStockAdjMastertNo;

        public frmrptStockAdjBillsSummary()
        {
            this.InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand command = new SqlCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    Connection = GlobalVariables.SqlConn,
                    CommandText = "RES_PROC_STOCKADJMASTER"
                };
                command.Parameters.AddWithValue("@TRANSTYPE", this.cmbOutletType.Text.Substring(0, 1));
                command.Parameters.AddWithValue("@STDATE", Convert.ToDateTime(this.DtpFrDate.Text));
                command.Parameters.AddWithValue("@ENDDATE", Convert.ToDateTime(this.DtpToDate.Text));
                if (this.txtStockAdjMastertNo.Text.Trim() != "")
                {
                    command.Parameters.AddWithValue("@ADJNO", this.txtStockAdjMastertNo.Text);
                }
                else
                {
                    command.Parameters.AddWithValue("@ADJNO", DBNull.Value);
                }
                SqlDataAdapter adapter = new SqlDataAdapter
                {
                    SelectCommand = command
                };
                DataSet dataSet = new DataSet();
                adapter.Fill(dataSet, "RES_PROC_STOCKADJMASTER");
                if (dataSet.Tables["RES_PROC_STOCKADJMASTER"].Rows.Count > 0)
                {
                    //ParameterFields pfields = new ParameterFields();
                    //ParameterField parameterField = new ParameterField();
                    //ParameterDiscreteValue value2 = new ParameterDiscreteValue();
                    //parameterField.Name = "PARAMETER";
                    //value2.Value = "FROM = " + this.DtpFrDate.Text + " TO = " + this.DtpToDate.Text;
                    if (this.txtStockAdjMastertNo.Text.Trim() != "")
                    {
                       // value2.Value = value2.Value + " ADJNO = " + this.txtStockAdjMastertNo.Text;
                    }
                    //parameterField.CurrentValues.Add((ParameterValue)value2);
                    //pfields.Add(parameterField);
                    ///By Bharat we have to change Reports to itextsparp pdf
                    ///
                    //rptstockadjbill rpt = new rptstockadjbill();
                    //rpt.SetDataSource(dataSet.Tables["RES_PROC_STOCKADJMASTER"]);
                    //rpt.Refresh();
                    //new FrmRptViewer(rpt, pfields).Show();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Print_Click ");
            }
        }

   
        private void frmrptStockAdjBillsSummary_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.RefreshData();
        }

     

        private void RefreshData()
        {
            this.DtpFrDate.Value = GlobalVariables.BusinessDate;
            this.DtpToDate.Value = GlobalVariables.BusinessDate;
            this.cmbPrintOn.Items.Clear();
            this.cmbPrintOn.Items.Add("SCREEN");
            this.cmbPrintOn.Items.Add("PRINTER");
            this.cmbPrintOn.Items.Add("DISK");
            this.cmbPrintOn.SelectedIndex = 0;
            this.cmbOutletType.Items.Clear();
            this.cmbOutletType.Items.Add("WAREHOUSE");
            this.cmbOutletType.Items.Add("STORE");
            this.cmbOutletType.Items.Add("KITCHEN");
            this.cmbOutletType.Items.Add("RESTAURANT");
        }
    }
}
