﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using ParaDinePrint;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmRptSalesSummary2 : Form
    {
        private Button btnExit;
        private Button btnGrid;
        private Button btnPrint;
        private CheckBox chkCrossTab;
        private CheckBox chkDispRwMaterial;
        private CheckBox chkProfitReport;
        private CheckBox chkrawandrecipe;
        private CheckBox chkUOMNotReq;
        private ComboBox cmbSendTo;
       // private IContainer components = null;
        private DataSet DS = new DataSet();
        private DateTimePicker dtpOrderDateFrom;
        private DateTimePicker dtpOrderDateTo;
        internal GroupBox grpCategory;
        private GroupBox grpCondition;
        internal GroupBox grpDepartment;
        private GroupBox grpDisplay;
        private GroupBox grpGroup;
        internal GroupBox grpItems;
        internal GroupBox grpMachine;
        internal GroupBox grpPayments;
        internal GroupBox grpSection;
        internal GroupBox grpSteward;
        internal GroupBox grpStockPoint;
        internal GroupBox grpTax;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label15;
        private Label label3;
        internal ListView lstCategory;
        internal ListView lstDepartment;
        internal ListView lstItem;
        internal ListView lstMachine;
        internal ListView lstPayments;
        internal ListView lstSection;
        internal ListView lstSteward;
        internal ListView lstStockpoint;
        internal ListView lstTax;
        private PrintingFunctions pfunctions = new PrintingFunctions();
        private Panel pnlSaleSaummary;
        private ClsColDosPrint[] prnItem;
        private ClsRptDosPrint prnrpt = new ClsRptDosPrint();
        internal RadioButton rdAllCatg;
        internal RadioButton rdAllDept;
        internal RadioButton rdAllItem;
        internal RadioButton rdAllMachine;
        internal RadioButton rdAllPayments;
        internal RadioButton rdAllSection;
        internal RadioButton rdAllSteward;
        internal RadioButton rdAllStockpoint;
        internal RadioButton rdAllTax;
        private RadioButton rdDispBillDate;
        private RadioButton rdDispBillNo;
        private RadioButton rdDispCategory;
        private RadioButton rdDispDepartment;
        private RadioButton rdDispItem;
        private RadioButton rdDispKOT;
        private RadioButton rdDispMachine;
        private RadioButton rdDispMonth;
        private RadioButton rdDispPaymode;
        private RadioButton rdDispSaleprice;
        private RadioButton rdDispSection;
        private RadioButton rdDispSteward;
        private RadioButton rdDispStockpoint;
        private RadioButton rdDispTax;
        private RadioButton rdDispYear;
        private RadioButton rdGrpBillDate;
        private RadioButton rdGrpBillNo;
        private RadioButton rdGrpCategory;
        private RadioButton rdGrpDepartment;
        private RadioButton rdGrpItem;
        private RadioButton rdGrpKot;
        private RadioButton rdGrpMachine;
        private RadioButton rdGrpMonth;
        private RadioButton rdGrpPaymode;
        private RadioButton rdGrpSalePrice;
        private RadioButton rdGrpSection;
        private RadioButton rdGrpSteward;
        private RadioButton rdGrpStockpoint;
        private RadioButton rdGrpTax;
        private RadioButton rdGrpYear;
        internal RadioButton rdSelCatg;
        internal RadioButton rdSelDept;
        internal RadioButton rdSelItem;
        internal RadioButton rdSelMachine;
        internal RadioButton rdSelPayments;
        internal RadioButton rdSelSection;
        internal RadioButton rdSelSteward;
        internal RadioButton rdSelStockpoint;
        internal RadioButton rdSelTax;
        private string rptname = "";
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private string StrSql = "";
        private TextBox txtFrCustOrderNo;
        private TextBox txtToCustOrderNo;

        public frmRptSalesSummary2()
        {
            this.InitializeComponent();
        }

        private void AddHandlers()
        {
            this.lstDepartment.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstCategory.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstItem.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstPayments.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstTax.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstMachine.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstSection.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstSteward.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstStockpoint.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.rdAllDept.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllCatg.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllItem.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllPayments.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllTax.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllMachine.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllSection.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllSteward.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllStockpoint.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
        }

        private bool BillPageHeader(ref ClsRptDosPrint ParamClsRpt, string ParamWhere)
        {
            try
            {
                return true;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Bill Page Header");
                return false;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnGrid_Click(object sender, EventArgs e)
        {
            object obj2;
            this.DS.Tables.Clear();
            string str = "";
            this.StrSql = "SELECT ";
            this.StrSql = this.StrSql + this.GetField(this.grpGroup) + " AS CATEGORYNAME,";
            this.StrSql = this.StrSql + this.GetField(this.grpDisplay) + " AS PRODUCT,";
            if (!this.chkUOMNotReq.Checked)
            {
                if (this.chkDispRwMaterial.Checked)
                {
                    this.StrSql = this.StrSql + " RUM.UOMNAME, ";
                }
                else if (this.chkrawandrecipe.Checked)
                {
                    this.StrSql = this.StrSql + " RUM.UOMNAME, ";
                }
                else
                {
                    this.StrSql = this.StrSql + " U.UOMNAME, ";
                }
            }
            if (this.chkDispRwMaterial.Checked)
            {
                this.StrSql = this.StrSql + " SUM((TOTQTY*ISNULL(ITG.QTY, 1))) AS TOTQTY, SUM((TOTQTY*ISNULL(ITG.QTY, 1) )*DBO.RES_FUNC_GETCOSTING(ITG.RAWMATERIALITEMID, RUM.UOMID, COM.CUSTORDERDATE)) AS COSTVALUE  ";
            }
            else if (this.chkrawandrecipe.Checked)
            {
                this.StrSql = this.StrSql + "SUM(ISNULL(ITG.QTY, 1)) AS TOTQTY , DBO.RES_FUNC_GETCONSUMEDQTY(RW.ITEMID,RUM.UOMID,COM.CUSTORDERDATE) AS CONSUMEDGOODSQTY ";
            }
            else
            {
                this.StrSql = this.StrSql + " SUM(TOTQTY) AS TOTQTY, ";
                this.StrSql = this.StrSql + " SUM(TOTQTY*COI.COSTING) AS COSTVALUE, SUM(ITEMGROSSAMT) AS SALEVALUE,  SUM(ITEMDISCAMT) AS ITEMDISCAMT, SUM(ITEMTAXAMT) AS ITEMTAXAMT,  SUM(SERVICETAXAMT) AS ITEMSRVTAXAMT, 0 AS BILLTAX,   SUM(IBD.ITEMBILLDISC) AS ITEMBILLDISC, SUM(IBD.ITEMBILLTAX) AS ITEMBILLTAX,  SUM(TOTALAMT) - SUM(IBD.ITEMBILLDISC) + SUM(IBD.ITEMBILLTAX) AS NETAMOUNT ";
            }
            this.StrSql = this.StrSql + " FROM RES_VW_CUSTORDERITEM COI   INNER JOIN RES_VW_CUSTORDERKOT COK ON COK.CUSTORDERKOTID = COI.CUSTORDERKOTID   INNER JOIN RES_VW_CUSTORDERMASTER COM ON COM.CUSTORDERMASTERID = COK.CUSTORDERMASTERID   INNER JOIN RES_FUNC_ITEMBILLDEDUCTIONS() IBD ON IBD.CUSTORDERITEMID = COI.CUSTORDERITEMID ";
            if (this.chkDispRwMaterial.Checked)
            {
                this.StrSql = this.StrSql + " LEFT JOIN RES_ITEMINGREDIENT ITG ON ITG.ITEMID = COI.ITEMID \tINNER JOIN RES_ITEM ITC ON ITC.ITEMID = COI.ITEMID \tINNER JOIN RES_ITEM IT ON IT.ITEMID = CASE WHEN ITC.COMBOREQ = 1 THEN ITG.RAWMATERIALITEMID ELSE COI.ITEMID END \tINNER JOIN RES_UOM U ON U.UOMID = CASE WHEN ITC.COMBOREQ = 1 THEN ITG.RAWMATERIALUOMID ELSE COI.UOMID END ";
            }
            else if (this.chkrawandrecipe.Checked)
            {
                this.StrSql = this.StrSql + " LEFT JOIN RES_ITEMINGREDIENT ITG ON ITG.ITEMID = COI.ITEMID \tINNER JOIN RES_ITEM ITC ON ITC.ITEMID = COI.ITEMID \tINNER JOIN RES_ITEM IT ON IT.ITEMID = CASE WHEN ITC.COMBOREQ = 1 THEN ITG.RAWMATERIALITEMID ELSE COI.ITEMID END \tINNER JOIN RES_UOM U ON U.UOMID = CASE WHEN ITC.COMBOREQ = 1 THEN ITG.RAWMATERIALUOMID ELSE COI.UOMID END ";
            }
            else
            {
                this.StrSql = this.StrSql + " INNER JOIN RES_ITEM IT ON IT.ITEMID = COI.ITEMID  INNER JOIN RES_UOM U ON U.UOMID = COI.UOMID ";
            }
            this.StrSql = this.StrSql + " INNER JOIN RES_SECTION SC ON SC.SECTIONID = IT.SECTIONID  INNER JOIN RES_CATEGORY CG ON CG.CATEGORYID = IT.CATEGORYID  INNER JOIN RES_DEPARTMENT DP ON DP.DEPARTMENTID = CG.DEPARTMENTID  INNER JOIN RES_TAX TX ON TX.TAXID = IT.TAXID ";
            if (this.chkDispRwMaterial.Checked)
            {
                this.StrSql = this.StrSql + " INNER JOIN RES_ITEM RW ON RW.ITEMID = ITG.RAWMATERIALITEMID  INNER JOIN RES_UOM RUM ON RUM.UOMID = RW.UOMID ";
            }
            else if (this.chkrawandrecipe.Checked)
            {
                this.StrSql = this.StrSql + " INNER JOIN RES_ITEM RW ON RW.ITEMID = ITG.RAWMATERIALITEMID  INNER JOIN RES_UOM RUM ON RUM.UOMID = RW.UOMID ";
            }
            this.StrSql = this.StrSql + " WHERE COI.CUSTORDERITEMID <> 0 ";
            if (this.dtpOrderDateFrom.Checked)
            {
                obj2 = this.StrSql;
                this.StrSql = string.Concat(new object[] { obj2, " AND COM.CUSTORDERDATE >= '", this.dtpOrderDateFrom.Value, "'" });
                str = str + " Date:" + this.dtpOrderDateFrom.Text;
            }
            if (this.dtpOrderDateTo.Checked)
            {
                obj2 = this.StrSql;
                this.StrSql = string.Concat(new object[] { obj2, " AND COM.CUSTORDERDATE <= '", this.dtpOrderDateTo.Value, "'" });
                str = str + " To:" + this.dtpOrderDateTo.Text;
            }
            if (this.rdSelDept.Checked && (this.GetCollString(this.lstDepartment) != ""))
            {
                this.StrSql = this.StrSql + " AND DP.DEPARTMENTID IN (" + this.GetCollString(this.lstDepartment) + ")";
                str = str + " Depts: " + this.GetRetParam(this.lstDepartment);
            }
            if (this.rdSelCatg.Checked && (this.GetCollString(this.lstCategory) != ""))
            {
                this.StrSql = this.StrSql + " AND CG.CATEGORYID IN (" + this.GetCollString(this.lstCategory) + ")";
                str = str + " Catg: " + this.GetRetParam(this.lstCategory);
            }
            if (this.rdSelStockpoint.Checked && (this.GetCollString(this.lstStockpoint) != ""))
            {
                this.StrSql = this.StrSql + " AND COM.STOCKPOINTID IN (" + this.GetCollString(this.lstStockpoint) + ")";
                str = str + " StockPoints: " + this.GetRetParam(this.lstStockpoint);
            }
            if (this.rdSelPayments.Checked && (this.GetCollString(this.lstPayments) != ""))
            {
                this.StrSql = this.StrSql + " AND COM.PAYMENT IN (" + this.GetCollString(this.lstPayments) + ")";
                str = str + "   Payments. Selected Are : " + this.GetRetParam(this.lstPayments);
            }
            if (this.rdSelTax.Checked && (this.GetCollString(this.lstTax) != ""))
            {
                this.StrSql = this.StrSql + " AND TX.TAXID IN (" + this.GetCollString(this.lstTax) + ")";
                str = str + " Taxes: " + this.GetRetParam(this.lstTax);
            }
            if (this.rdSelMachine.Checked && (this.GetCollString(this.lstMachine) != ""))
            {
                this.StrSql = this.StrSql + " AND COM.MACHINENO IN (" + this.GetCollString(this.lstMachine) + ")";
                str = str + " Machines: " + this.GetRetParam(this.lstMachine);
            }
            if (this.rdSelSection.Checked && (this.GetCollString(this.lstSection) != ""))
            {
                this.StrSql = this.StrSql + " AND SC.SECTIONID IN (" + this.GetCollString(this.lstSection) + ")";
                str = str + " Sections: " + this.GetRetParam(this.lstSection);
            }
            if (this.rdSelSteward.Checked && (this.GetCollString(this.lstSteward) != ""))
            {
                this.StrSql = this.StrSql + " AND COM.STEWARDID IN (" + this.GetCollString(this.lstSteward) + ")";
                str = str + " Stewards: " + this.GetRetParam(this.lstSteward);
            }
            if (this.rdSelItem.Checked && (this.GetCollString(this.lstItem) != ""))
            {
                this.StrSql = this.StrSql + " AND IT.ITEMID IN (" + this.GetCollString(this.lstItem) + ")";
                str = str + " Items: " + this.GetRetParam(this.lstItem);
            }
            string strSql = this.StrSql;
            this.StrSql = strSql + " GROUP BY " + this.GetField(this.grpGroup) + "," + this.GetField(this.grpDisplay);
            if (this.chkrawandrecipe.Checked)
            {
            }
            if (!this.chkUOMNotReq.Checked)
            {
                if (this.chkDispRwMaterial.Checked)
                {
                    this.StrSql = this.StrSql + " ,RUM.UOMNAME ";
                }
                else if (this.chkrawandrecipe.Checked)
                {
                    this.StrSql = this.StrSql + " , RUM.UOMNAME ";
                }
                else
                {
                    this.StrSql = this.StrSql + " ,U.UOMNAME ";
                }
            }
            GlobalFill.FillDataSet(this.StrSql, "SALESSUMMARY", this.DS, this.SDA);
            DataTable dt = this.DS.Tables["SALESSUMMARY"];
            new frmGridView(dt, "Sales Summary ").ShowDialog();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                object obj2;
                this.DS.Tables.Clear();
                string str = "";
                this.StrSql = "SELECT ";
                this.StrSql = this.StrSql + this.GetField(this.grpGroup) + " AS CATEGORYNAME,";
                this.StrSql = this.StrSql + this.GetField(this.grpDisplay) + " AS PRODUCT,";
                if (!this.chkUOMNotReq.Checked)
                {
                    if (this.chkDispRwMaterial.Checked)
                    {
                        this.StrSql = this.StrSql + " RUM.UOMNAME, ";
                    }
                    else if (this.chkrawandrecipe.Checked)
                    {
                        this.StrSql = this.StrSql + " RUM.UOMNAME, ";
                    }
                    else
                    {
                        this.StrSql = this.StrSql + " U.UOMNAME, ";
                    }
                }
                if (this.chkDispRwMaterial.Checked)
                {
                    this.StrSql = this.StrSql + " SUM((ISNULL(ITG.QTY, 1))) AS TOTQTY, SUM((TOTQTY*ISNULL(ITG.QTY, 1) )*DBO.RES_FUNC_GETCOSTING(ITG.RAWMATERIALITEMID, RUM.UOMID, COM.CUSTORDERDATE)) AS COSTVALUE  ";
                }
                else if (this.chkrawandrecipe.Checked)
                {
                    this.StrSql = this.StrSql + "SUM(ISNULL(ITG.QTY, 1)) AS TOTQTY ,SUM(DBO.RES_FUNC_GETCONSUMEDQTY(RW.ITEMID,RUM.UOMID,COM.CUSTORDERDATE)) AS CONSUMEDGOODSQTY";
                }
                else
                {
                    this.StrSql = this.StrSql + " SUM(TOTQTY) AS TOTQTY, ";
                    this.StrSql = this.StrSql + " SUM(TOTQTY*COI.COSTING) AS COSTVALUE, SUM(ITEMGROSSAMT) AS SALEVALUE,  SUM(ITEMDISCAMT) AS ITEMDISCAMT, SUM(ITEMTAXAMT) AS ITEMTAXAMT,  SUM(SERVICETAXAMT) AS ITEMSRVTAXAMT, 0 AS BILLTAX,   SUM(IBD.ITEMBILLDISC) AS ITEMBILLDISC,  SUM(CASE WHEN B.BILLTAXINCL = 1 THEN ((TOTALAMT - IBD.ITEMBILLDISC) * B.BILLTAX)/(100+B.BILLTAX) ELSE IBD.ITEMBILLTAX END) AS ITEMBILLTAX, SUM(IBD.SERVTAXAMT) AS SERVTAXAMT, SUM(TOTALAMT) - SUM(IBD.ITEMBILLDISC) + SUM(IBD.ITEMBILLTAX) + SUM(IBD.SERVTAXAMT) AS NETAMOUNT ";
                }
                this.StrSql = this.StrSql + " FROM RES_VW_CUSTORDERITEM COI   INNER JOIN RES_VW_CUSTORDERKOT COK ON COK.CUSTORDERKOTID = COI.CUSTORDERKOTID   INNER JOIN RES_VW_CUSTORDERMASTER COM ON COM.CUSTORDERMASTERID = COK.CUSTORDERMASTERID   INNER JOIN RES_FUNC_ITEMBILLDEDUCTIONS() IBD ON IBD.CUSTORDERITEMID = COI.CUSTORDERITEMID  OUTER APPLY RES_BILLSETTINGS B ";
                if (this.chkDispRwMaterial.Checked)
                {
                    this.StrSql = this.StrSql + " LEFT JOIN RES_ITEMINGREDIENT ITG ON ITG.ITEMID = COI.ITEMID \tINNER JOIN RES_ITEM ITC ON ITC.ITEMID = COI.ITEMID \tINNER JOIN RES_ITEM IT ON IT.ITEMID = CASE WHEN ITC.COMBOREQ = 1 THEN ITG.RAWMATERIALITEMID ELSE COI.ITEMID END \tINNER JOIN RES_UOM U ON U.UOMID = CASE WHEN ITC.COMBOREQ = 1 THEN ITG.RAWMATERIALUOMID ELSE COI.UOMID END ";
                }
                else if (this.chkrawandrecipe.Checked)
                {
                    this.StrSql = this.StrSql + " LEFT JOIN RES_ITEMINGREDIENT ITG ON ITG.ITEMID = COI.ITEMID \tINNER JOIN RES_ITEM ITC ON ITC.ITEMID = COI.ITEMID \tINNER JOIN RES_ITEM IT ON IT.ITEMID = CASE WHEN ITC.COMBOREQ = 1 THEN ITG.RAWMATERIALITEMID ELSE COI.ITEMID END \tINNER JOIN RES_UOM U ON U.UOMID = CASE WHEN ITC.COMBOREQ = 1 THEN ITG.RAWMATERIALUOMID ELSE COI.UOMID END ";
                }
                else
                {
                    this.StrSql = this.StrSql + " INNER JOIN RES_ITEM IT ON IT.ITEMID = COI.ITEMID  INNER JOIN RES_UOM U ON U.UOMID = COI.UOMID ";
                }
                this.StrSql = this.StrSql + " INNER JOIN RES_SECTION SC ON SC.SECTIONID = IT.SECTIONID  INNER JOIN RES_CATEGORY CG ON CG.CATEGORYID = IT.CATEGORYID  INNER JOIN RES_DEPARTMENT DP ON DP.DEPARTMENTID = CG.DEPARTMENTID  INNER JOIN RES_TAX TX ON TX.TAXID = IT.TAXID ";
                if (this.chkDispRwMaterial.Checked)
                {
                    this.StrSql = this.StrSql + " INNER JOIN RES_ITEM RW ON RW.ITEMID = ITG.RAWMATERIALITEMID  INNER JOIN RES_UOM RUM ON RUM.UOMID = RW.UOMID ";
                }
                else if (this.chkrawandrecipe.Checked)
                {
                    this.StrSql = this.StrSql + " INNER JOIN RES_ITEM RW ON RW.ITEMID = ITG.RAWMATERIALITEMID  INNER JOIN RES_UOM RUM ON RUM.UOMID = RW.UOMID ";
                }
                this.StrSql = this.StrSql + " WHERE COI.CUSTORDERITEMID <> 0 ";
                if (this.dtpOrderDateFrom.Checked)
                {
                    obj2 = this.StrSql;
                    this.StrSql = string.Concat(new object[] { obj2, " AND COM.CUSTORDERDATE >= '", this.dtpOrderDateFrom.Value, "'" });
                    str = str + " Date:" + this.dtpOrderDateFrom.Text;
                }
                if (this.dtpOrderDateTo.Checked)
                {
                    obj2 = this.StrSql;
                    this.StrSql = string.Concat(new object[] { obj2, " AND COM.CUSTORDERDATE <= '", this.dtpOrderDateTo.Value, "'" });
                    str = str + " To:" + this.dtpOrderDateTo.Text;
                }
                if (this.rdSelDept.Checked && (this.GetCollString(this.lstDepartment) != ""))
                {
                    this.StrSql = this.StrSql + " AND DP.DEPARTMENTID IN (" + this.GetCollString(this.lstDepartment) + ")";
                    str = str + " Depts: " + this.GetRetParam(this.lstDepartment);
                }
                if (this.rdSelCatg.Checked && (this.GetCollString(this.lstCategory) != ""))
                {
                    this.StrSql = this.StrSql + " AND CG.CATEGORYID IN (" + this.GetCollString(this.lstCategory) + ")";
                    str = str + " Catg: " + this.GetRetParam(this.lstCategory);
                }
                if (this.rdSelStockpoint.Checked && (this.GetCollString(this.lstStockpoint) != ""))
                {
                    this.StrSql = this.StrSql + " AND COM.STOCKPOINTID IN (" + this.GetCollString(this.lstStockpoint) + ")";
                    str = str + " StockPoints: " + this.GetRetParam(this.lstStockpoint);
                }
                if (this.rdSelPayments.Checked && (this.GetCollString(this.lstPayments) != ""))
                {
                    this.StrSql = this.StrSql + " AND COM.PAYMENT IN (" + this.GetCollString(this.lstPayments) + ")";
                    str = str + "   Payments. Selected Are : " + this.GetRetParam(this.lstPayments);
                }
                if (this.rdSelTax.Checked && (this.GetCollString(this.lstTax) != ""))
                {
                    this.StrSql = this.StrSql + " AND TX.TAXID IN (" + this.GetCollString(this.lstTax) + ")";
                    str = str + " Taxes: " + this.GetRetParam(this.lstTax);
                }
                if (this.rdSelMachine.Checked && (this.GetCollString(this.lstMachine) != ""))
                {
                    this.StrSql = this.StrSql + " AND COM.MACHINENO IN (" + this.GetCollString(this.lstMachine) + ")";
                    str = str + " Machines: " + this.GetRetParam(this.lstMachine);
                }
                if (this.rdSelSection.Checked && (this.GetCollString(this.lstSection) != ""))
                {
                    this.StrSql = this.StrSql + " AND SC.SECTIONID IN (" + this.GetCollString(this.lstSection) + ")";
                    str = str + " Sections: " + this.GetRetParam(this.lstSection);
                }
                if (this.rdSelSteward.Checked && (this.GetCollString(this.lstSteward) != ""))
                {
                    this.StrSql = this.StrSql + " AND COM.STEWARDID IN (" + this.GetCollString(this.lstSteward) + ")";
                    str = str + " Stewards: " + this.GetRetParam(this.lstSteward);
                }
                if (this.rdSelItem.Checked && (this.GetCollString(this.lstItem) != ""))
                {
                    this.StrSql = this.StrSql + " AND IT.ITEMID IN (" + this.GetCollString(this.lstItem) + ")";
                    str = str + " Items: " + this.GetRetParam(this.lstItem);
                }
                string strSql = this.StrSql;
                this.StrSql = strSql + " GROUP BY " + this.GetField(this.grpGroup) + "," + this.GetField(this.grpDisplay) + "";
                if (this.chkrawandrecipe.Checked)
                {
                }
                if (!this.chkUOMNotReq.Checked)
                {
                    if (this.chkDispRwMaterial.Checked)
                    {
                        this.StrSql = this.StrSql + " ,RUM.UOMNAME ";
                    }
                    else if (this.chkrawandrecipe.Checked)
                    {
                        this.StrSql = this.StrSql + " ,RUM.UOMNAME  ";
                    }
                    else
                    {
                        this.StrSql = this.StrSql + " ,U.UOMNAME ";
                    }
                }
                GlobalFill.FillDataSet(this.StrSql, "SALESSUMMARY", this.DS, this.SDA);
                if ((this.cmbSendTo.Text.ToUpper() == "PRINTER") || (this.cmbSendTo.Text.ToUpper() == "DISK"))
                {
                    this.DS.Tables["SALESSUMMARY"].Columns.Add("HEADER");
                    foreach (DataRow row in this.DS.Tables["SALESSUMMARY"].Rows)
                    {
                        row["HEADER"] = this.dtpOrderDateFrom.Text.Substring(0, 11) + " TO " + this.dtpOrderDateTo.Text.Substring(0, 11);
                    }
                    this.rptname = "SALESSUMMARY";
                    this.pfunctions.SETREPORTDATA(ref this.prnrpt, this.rptname, this.DS, this.SDA, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
                    this.pfunctions.SETCOLDATA(ref this.prnItem, this.rptname, this.DS, this.SDA, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
                    this.BillPageHeader(ref this.prnrpt, "");
                    string str2 = "";
                    for (int i = 0; i <= (this.prnItem.Length - 1); i++)
                    {
                        string str3 = "";
                        if (this.prnItem[i].ISGroupHeader)
                        {
                            str3 = (this.prnItem[i].ColumnPosition + 1) + ",";
                        }
                        str2 = str2 + str3;
                    }
                    new ClsDosPrint(this.DS.Tables["SALESSUMMARY"], this.prnItem, this.prnrpt, GlobalVariables.SqlConn, GlobalVariables.PrinterId).PrintData(this.cmbSendTo.Text, "");
                }
                 ///By Bharat we have to change Reports to itextsparp pdf
                        ///
                //else
                //{
                //    FrmRptViewer viewer;
                //    ParameterFields pfields = new ParameterFields();
                //    ParameterField parameterField = new ParameterField
                //    {
                //        Name = "CompanyName"
                //    };
                //    ParameterDiscreteValue value2 = new ParameterDiscreteValue
                //    {
                //        Value = GlobalVariables.StrCompName
                //    };
                //    parameterField.CurrentValues.Add((ParameterValue)value2);
                //    pfields.Add(parameterField);
                //    parameterField = new ParameterField
                //    {
                //        Name = "Address"
                //    };
                //    value2 = new ParameterDiscreteValue
                //    {
                //        Value = Convert.ToString(GlobalFunctions.GetQueryValue("SELECT TOP 1 ADDRESS FROM GLB_COMPANY"))
                //    };
                //    parameterField.CurrentValues.Add((ParameterValue)value2);
                //    pfields.Add(parameterField);
                //    parameterField = new ParameterField
                //    {
                //        Name = "Parameter"
                //    };
                //    value2 = new ParameterDiscreteValue
                //    {
                //        Value = str
                //    };
                //    parameterField.CurrentValues.Add((ParameterValue)value2);
                //    pfields.Add(parameterField);
                //    if (this.chkDispRwMaterial.Checked)
                //    {
                //        parameterField = new ParameterField
                //        {
                //            Name = "ReportName"
                //        };
                //        value2 = new ParameterDiscreteValue
                //        {
                //            Value = "Rawmaterial Consumed"
                //        };
                //        parameterField.CurrentValues.Add((ParameterValue)value2);
                //        pfields.Add(parameterField);
                //        RptRawMaterialSaleSummary rpt = new RptRawMaterialSaleSummary();
                //        rpt.SetDataSource(this.DS.Tables["SALESSUMMARY"]);
                //        viewer = new FrmRptViewer(rpt, pfields);
                //        viewer.ShowDialog();
                //    }
                //    else if (this.chkrawandrecipe.Checked)
                //    {
                //        parameterField = new ParameterField
                //        {
                //            Name = "ReportName"
                //        };
                //        value2 = new ParameterDiscreteValue
                //        {
                //            Value = "CmpareRawandRecipe"
                //        };
                //        parameterField.CurrentValues.Add((ParameterValue)value2);
                //        pfields.Add(parameterField);
                //        RptCmpareRawandRecipe recipe = new RptCmpareRawandRecipe();
                //        recipe.SetDataSource(this.DS.Tables["SALESSUMMARY"]);
                //        viewer = new FrmRptViewer(recipe, pfields);
                //        viewer.ShowDialog();
                //    }
                //    else if (this.chkProfitReport.Checked)
                //    {
                //        RptItemProfitSummary summary2 = new RptItemProfitSummary();
                //        summary2.SetDataSource(this.DS.Tables["SALESSUMMARY"]);
                //        viewer = new FrmRptViewer(summary2, pfields);
                //        viewer.ShowDialog();
                //    }
                //    else
                //    {
                //        parameterField = new ParameterField
                //        {
                //            Name = "TAXINCL"
                //        };
                //        value2 = new ParameterDiscreteValue
                //        {
                //            Value = Convert.ToBoolean(GlobalFunctions.GetQueryValue("SELECT BILLTAXINCL FROM RES_BILLSETTINGS"))
                //        };
                //        parameterField.CurrentValues.Add((ParameterValue)value2);
                //        pfields.Add(parameterField);
                //        parameterField = new ParameterField
                //        {
                //            Name = "BILLTAXPERC"
                //        };
                //        value2 = new ParameterDiscreteValue
                //        {
                //            Value = Convert.ToDouble(GlobalFunctions.GetQueryValue("SELECT BILLTAX FROM RES_BILLSETTINGS"))
                //        };
                //        parameterField.CurrentValues.Add((ParameterValue)value2);
                //        pfields.Add(parameterField);
                //        if (this.chkCrossTab.Checked)
                //        {
                //            RptItemSalesSummCrossTab tab = new RptItemSalesSummCrossTab();
                //            tab.SetDataSource(this.DS.Tables["SALESSUMMARY"]);
                //            viewer = new FrmRptViewer(tab, pfields);
                //            viewer.ShowDialog();
                //        }
                //        else
                //        {
                //            RptItemSalesSummary2 summary3 = new RptItemSalesSummary2();
                //            summary3.SetDataSource(this.DS.Tables["SALESSUMMARY"]);
                //            new FrmRptViewer(summary3, pfields).ShowDialog();
                //        }
                //    }
                //}
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Printing");
            }
        }

        private void chkCrossTab_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chkCrossTab.Checked)
            {
                this.chkDispRwMaterial.Enabled = false;
                this.chkProfitReport.Enabled = false;
                this.chkUOMNotReq.Enabled = false;
            }
            else
            {
                this.chkDispRwMaterial.Enabled = true;
                this.chkProfitReport.Enabled = true;
                this.chkUOMNotReq.Enabled = true;
            }
        }

   

        private void frmRptSalesSummary2_Load(object sender, EventArgs e)
        {
            try
            {
                new GlobalTheme().applyTheme(this);
                this.AddHandlers();
                this.RefreshData();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Form Load");
            }
        }

        private string GetCollString(ListView paramLvw)
        {
            try
            {
                string str = "";
                for (int i = 0; i < paramLvw.CheckedItems.Count; i++)
                {
                    if (str != "")
                    {
                        str = str + ", ";
                    }
                    str = str + Convert.ToString(paramLvw.CheckedItems[i].Tag);
                }
                return str;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in function Collection String");
                return "";
            }
        }

        private string GetField(GroupBox paramGrp)
        {
            try
            {
                foreach (RadioButton button in paramGrp.Controls)
                {
                    if (button.Checked)
                    {
                        if (button.Text.ToUpper() == "DEPARTMENT")
                        {
                            return "DP.DEPARTMENTNAME";
                        }
                        if (button.Text.ToUpper() == "CATEGORY")
                        {
                            return "CG.CATEGORYNAME";
                        }
                        if (button.Text.ToUpper() == "STOCKPOINT")
                        {
                            return "COM.STOCKPOINTNAME";
                        }
                        if (button.Text.ToUpper() == "SECTION")
                        {
                            return "SC.SECTIONNAME";
                        }
                        if (button.Text.ToUpper() == "TAX")
                        {
                            return "TX.TAXNAME";
                        }
                        if (button.Text.ToUpper() == "ITEM")
                        {
                            if (this.chkDispRwMaterial.Checked)
                            {
                                return "RW.ITEMNAME";
                            }
                            if (this.chkrawandrecipe.Checked)
                            {
                                return " RW.ITEMNAME ";
                            }
                            return "COI.ITEMNAME";
                        }
                        if (button.Text.ToUpper() == "SALEPRICE")
                        {
                            return "COI.SALEPRICE";
                        }
                        if (button.Text.ToUpper() == "STEWARD")
                        {
                            return "COM.STEWARDNAME";
                        }
                        if (button.Text.ToUpper() == "KOT")
                        {
                            return "COK.KOTNUMBER";
                        }
                        if (button.Text.ToUpper() == "BILLNO")
                        {
                            return "COM.CUSTORDERNO";
                        }
                        if (button.Text.ToUpper() == "BILLDATE")
                        {
                            return "CONVERT(VARCHAR, COM.CUSTORDERDATE, 102)";
                        }
                        if (button.Text.ToUpper() == "MONTH")
                        {
                            return "'MONTH-' + CONVERT(VARCHAR, MONTH(COM.CUSTORDERDATE)) + '/' + CONVERT(VARCHAR, YEAR(COM.CUSTORDERDATE))";
                        }
                        if (button.Text.ToUpper() == "YEAR")
                        {
                            return "'YEAR-' + CONVERT(VARCHAR, YEAR(COM.CUSTORDERDATE))";
                        }
                        if (button.Text.ToUpper() == "MACHINE")
                        {
                            return "'MACHINE-' + CONVERT(VARCHAR, COM.MACHINENO)";
                        }
                        if (button.Text.ToUpper() == "PAYMODE")
                        {
                            return "COM.PAYMENTLIST";
                        }
                    }
                }
                return "";
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in GetFied");
                return "";
            }
        }

        private string GetParamField(GroupBox paramGrp)
        {
            try
            {
                foreach (RadioButton button in paramGrp.Controls)
                {
                    if (button.Checked)
                    {
                        if (button.Text.ToUpper() == "DEPARTMENT")
                        {
                            return "DEPARTMENT";
                        }
                        if (button.Text.ToUpper() == "CATEGORY")
                        {
                            return "CATEGORY";
                        }
                        if (button.Text.ToUpper() == "STOCKPOINT")
                        {
                            return "STOCKPOINT";
                        }
                        if (button.Text.ToUpper() == "SECTION")
                        {
                            return "SECTION";
                        }
                        if (button.Text.ToUpper() == "TAX")
                        {
                            return "TAX";
                        }
                        if (button.Text.ToUpper() == "ITEM")
                        {
                            return "ITEM";
                        }
                        if (button.Text.ToUpper() == "SALEPRICE")
                        {
                            return "SALEPRICE";
                        }
                        if (button.Text.ToUpper() == "STEWARD")
                        {
                            return "STEWARD";
                        }
                        if (button.Text.ToUpper() == "KOT")
                        {
                            return "KOTNUMBER";
                        }
                        if (button.Text.ToUpper() == "BILLNO")
                        {
                            return "ORDERNO";
                        }
                        if (button.Text.ToUpper() == "BILLDATE")
                        {
                            return "BILLDATE";
                        }
                        if (button.Text.ToUpper() == "MONTH")
                        {
                            return "MONTH";
                        }
                        if (button.Text.ToUpper() == "YEAR")
                        {
                            return "YEAR";
                        }
                        if (button.Text.ToUpper() == "MACHINE")
                        {
                            return "MACHINE";
                        }
                        if (button.Text.ToUpper() == "PAYMODE")
                        {
                            return "PAYMODE";
                        }
                    }
                }
                return "";
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in GetFied");
                return "";
            }
        }

        private string GetRetParam(ListView ParamLstVw)
        {
            try
            {
                string str = "";
                for (int i = 0; i < ParamLstVw.CheckedItems.Count; i++)
                {
                    if (str != "")
                    {
                        str = str + ", ";
                    }
                    str = str + Convert.ToString(ParamLstVw.CheckedItems[i].Text);
                }
                return str;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in function Collection String");
                return "";
            }
        }

       

        private void lst_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            try
            {
                if (e.Item.Checked)
                {
                    ListView view = (ListView)sender;
                    if (view.CheckedItems.Count > 0)
                    {
                        foreach (Control control in view.Parent.Controls)
                        {
                            if (control.GetType() == typeof(RadioButton))
                            {
                                RadioButton button = (RadioButton)control;
                                if (button.Text.ToUpper().Contains("SELECTED"))
                                {
                                    button.Checked = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in ListViewChecked");
            }
        }

        private void rdAll_checkchanged(object sender, EventArgs e)
        {
            try
            {
                RadioButton button = (RadioButton)sender;
                if (button.Checked)
                {
                    foreach (Control control in button.Parent.Controls)
                    {
                        if (control.GetType() == typeof(ListView))
                        {
                            ListView view = (ListView)control;
                            foreach (ListViewItem item in view.CheckedItems)
                            {
                                item.Checked = false;
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in AllRd");
            }
        }

        private void RefreshData()
        {
            try
            {
                this.StrSql = "SELECT DEPARTMENTID, DEPARTMENTNAME FROM RES_DEPARTMENT";
                GlobalFill.FillListView(this.lstDepartment, GlobalFill.FillDataTable(this.StrSql));
                this.StrSql = "SELECT CATEGORYID, CATEGORYNAME FROM RES_CATEGORY";
                GlobalFill.FillListView(this.lstCategory, GlobalFill.FillDataTable(this.StrSql));
                this.StrSql = "SELECT TAXID, TAXNAME FROM RES_TAX";
                GlobalFill.FillListView(this.lstTax, GlobalFill.FillDataTable(this.StrSql));
                this.StrSql = "SELECT SECTIONID, SECTIONNAME FROM RES_SECTION";
                GlobalFill.FillListView(this.lstSection, GlobalFill.FillDataTable(this.StrSql));
                this.StrSql = "SELECT ITEMID, ITEMNAME FROM RES_ITEM";
                GlobalFill.FillListView(this.lstItem, GlobalFill.FillDataTable(this.StrSql));
                this.StrSql = "SELECT PAYMENTMODEID, PAYMENTMODE FROM GLB_PAYMENTMODE";
                GlobalFill.FillListView(this.lstPayments, GlobalFill.FillDataTable(this.StrSql));
                this.StrSql = "SELECT TERMINALID, TERMINALNAME FROM GLB_TERMINAL";
                GlobalFill.FillListView(this.lstMachine, GlobalFill.FillDataTable(this.StrSql));
                this.StrSql = "SELECT STEWARDID, STEWARDNAME FROM RES_STEWARD";
                GlobalFill.FillListView(this.lstSteward, GlobalFill.FillDataTable(this.StrSql));
                this.StrSql = "SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT WHERE ISBILLPOINT = 1";
                GlobalFill.FillListView(this.lstStockpoint, GlobalFill.FillDataTable(this.StrSql));
                this.cmbSendTo.Items.Clear();
                this.cmbSendTo.Items.Add("SCREEN");
                this.cmbSendTo.Items.Add("PRINTER");
                this.cmbSendTo.Items.Add("DISK");
                this.cmbSendTo.SelectedIndex = 0;
                this.dtpOrderDateFrom.Value = new DateTime(GlobalVariables.BusinessDate.Year, GlobalVariables.BusinessDate.Month, GlobalVariables.BusinessDate.Day, 0, 0, 0);
                this.dtpOrderDateTo.Value = GlobalVariables.BusinessDate;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in RefreshData");
            }
        }
    }
}
