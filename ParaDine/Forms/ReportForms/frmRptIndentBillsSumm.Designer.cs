﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.ReportForms
{
    partial class frmRptIndentBillsSumm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPrint = new System.Windows.Forms.Button();
            this.grpfields = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbPrintOn = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.DtpToIndentDate = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.DtpFrIndentDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbStockPoint = new System.Windows.Forms.ComboBox();
            this.txtIndentNO = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.grpfields.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(85, 175);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(76, 26);
            this.btnPrint.TabIndex = 7;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // grpfields
            // 
            this.grpfields.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.grpfields.Controls.Add(this.label7);
            this.grpfields.Controls.Add(this.cmbPrintOn);
            this.grpfields.Controls.Add(this.groupBox1);
            this.grpfields.Controls.Add(this.label2);
            this.grpfields.Controls.Add(this.cmbStockPoint);
            this.grpfields.Controls.Add(this.txtIndentNO);
            this.grpfields.Controls.Add(this.label1);
            this.grpfields.Location = new System.Drawing.Point(6, 5);
            this.grpfields.Name = "grpfields";
            this.grpfields.Size = new System.Drawing.Size(330, 162);
            this.grpfields.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(67, 133);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Print On :";
            // 
            // cmbPrintOn
            // 
            this.cmbPrintOn.BackColor = System.Drawing.Color.White;
            this.cmbPrintOn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPrintOn.FormattingEnabled = true;
            this.cmbPrintOn.Location = new System.Drawing.Point(122, 130);
            this.cmbPrintOn.Name = "cmbPrintOn";
            this.cmbPrintOn.Size = new System.Drawing.Size(126, 21);
            this.cmbPrintOn.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.DtpToIndentDate);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.DtpFrIndentDate);
            this.groupBox1.Location = new System.Drawing.Point(8, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(305, 55);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Indent Date :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(162, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "To";
            // 
            // DtpToIndentDate
            // 
            this.DtpToIndentDate.CustomFormat = "dd/MMM/yyyy";
            this.DtpToIndentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpToIndentDate.Location = new System.Drawing.Point(188, 19);
            this.DtpToIndentDate.Name = "DtpToIndentDate";
            this.DtpToIndentDate.ShowCheckBox = true;
            this.DtpToIndentDate.Size = new System.Drawing.Size(101, 20);
            this.DtpToIndentDate.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "From";
            // 
            // DtpFrIndentDate
            // 
            this.DtpFrIndentDate.CustomFormat = "dd/MMM/yyyy";
            this.DtpFrIndentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpFrIndentDate.Location = new System.Drawing.Point(50, 19);
            this.DtpFrIndentDate.Name = "DtpFrIndentDate";
            this.DtpFrIndentDate.ShowCheckBox = true;
            this.DtpFrIndentDate.Size = new System.Drawing.Size(101, 20);
            this.DtpFrIndentDate.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Indent For :";
            // 
            // cmbStockPoint
            // 
            this.cmbStockPoint.BackColor = System.Drawing.Color.White;
            this.cmbStockPoint.FormattingEnabled = true;
            this.cmbStockPoint.Location = new System.Drawing.Point(87, 100);
            this.cmbStockPoint.Name = "cmbStockPoint";
            this.cmbStockPoint.Size = new System.Drawing.Size(211, 21);
            this.cmbStockPoint.TabIndex = 1;
            // 
            // txtIndentNO
            // 
            this.txtIndentNO.BackColor = System.Drawing.Color.White;
            this.txtIndentNO.Location = new System.Drawing.Point(148, 10);
            this.txtIndentNO.Name = "txtIndentNO";
            this.txtIndentNO.Size = new System.Drawing.Size(93, 20);
            this.txtIndentNO.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(88, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Indent No :";
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(179, 175);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 26);
            this.btnExit.TabIndex = 8;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmRptIndentBillsSumm
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(340, 206);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.grpfields);
            this.Controls.Add(this.btnExit);
            this.Name = "frmRptIndentBillsSumm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Indent Bills Summary";
            this.Load += new System.EventHandler(this.frmRptIndentBillsSumm_Load);
            this.grpfields.ResumeLayout(false);
            this.grpfields.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}