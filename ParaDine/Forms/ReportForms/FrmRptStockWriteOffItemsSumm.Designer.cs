﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.ReportForms
{
    partial class FrmRptStockWriteOffItemsSumm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExit = new System.Windows.Forms.Button();
            this.dtpFrDate = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grpDisplay = new System.Windows.Forms.GroupBox();
            this.rdDispStockpoint = new System.Windows.Forms.RadioButton();
            this.rdDispSection = new System.Windows.Forms.RadioButton();
            this.rdDispCostprice = new System.Windows.Forms.RadioButton();
            this.rdDispYear = new System.Windows.Forms.RadioButton();
            this.rdDispMonth = new System.Windows.Forms.RadioButton();
            this.rdDispWriteoffDate = new System.Windows.Forms.RadioButton();
            this.rdDispItem = new System.Windows.Forms.RadioButton();
            this.rdDispCategory = new System.Windows.Forms.RadioButton();
            this.rdDispDepartment = new System.Windows.Forms.RadioButton();
            this.grpGroup = new System.Windows.Forms.GroupBox();
            this.rdGrpStockpoint = new System.Windows.Forms.RadioButton();
            this.rdGrpSection = new System.Windows.Forms.RadioButton();
            this.rdGrpCostPrice = new System.Windows.Forms.RadioButton();
            this.rdGrpYear = new System.Windows.Forms.RadioButton();
            this.rdGrpMonth = new System.Windows.Forms.RadioButton();
            this.rdGrpWriteoffDate = new System.Windows.Forms.RadioButton();
            this.rdGrpItem = new System.Windows.Forms.RadioButton();
            this.rdGrpCategory = new System.Windows.Forms.RadioButton();
            this.rdGrpDepartment = new System.Windows.Forms.RadioButton();
            this.grpStockPoint = new System.Windows.Forms.GroupBox();
            this.lstStockpoint = new System.Windows.Forms.ListView();
            this.rdSelStockpoint = new System.Windows.Forms.RadioButton();
            this.rdAllStockpoint = new System.Windows.Forms.RadioButton();
            this.grpSection = new System.Windows.Forms.GroupBox();
            this.lstSection = new System.Windows.Forms.ListView();
            this.rdSelSection = new System.Windows.Forms.RadioButton();
            this.rdAllSection = new System.Windows.Forms.RadioButton();
            this.grpItems = new System.Windows.Forms.GroupBox();
            this.lstItem = new System.Windows.Forms.ListView();
            this.rdSelItem = new System.Windows.Forms.RadioButton();
            this.rdAllItem = new System.Windows.Forms.RadioButton();
            this.grpCategory = new System.Windows.Forms.GroupBox();
            this.lstCategory = new System.Windows.Forms.ListView();
            this.rdSelCatg = new System.Windows.Forms.RadioButton();
            this.rdAllCatg = new System.Windows.Forms.RadioButton();
            this.grpDepartment = new System.Windows.Forms.GroupBox();
            this.lstDepartment = new System.Windows.Forms.ListView();
            this.rdSelDept = new System.Windows.Forms.RadioButton();
            this.rdAllDept = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkCPNotReq = new System.Windows.Forms.CheckBox();
            this.chkUOMNotReq = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.grpDisplay.SuspendLayout();
            this.grpGroup.SuspendLayout();
            this.grpStockPoint.SuspendLayout();
            this.grpSection.SuspendLayout();
            this.grpItems.SuspendLayout();
            this.grpCategory.SuspendLayout();
            this.grpDepartment.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(420, 409);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 28);
            this.btnExit.TabIndex = 13;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // dtpFrDate
            // 
            this.dtpFrDate.Checked = false;
            this.dtpFrDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpFrDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFrDate.Location = new System.Drawing.Point(56, 22);
            this.dtpFrDate.Name = "dtpFrDate";
            this.dtpFrDate.ShowCheckBox = true;
            this.dtpFrDate.Size = new System.Drawing.Size(104, 20);
            this.dtpFrDate.TabIndex = 0;
            this.dtpFrDate.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(26, 63);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "To :";
            // 
            // dtpToDate
            // 
            this.dtpToDate.Checked = false;
            this.dtpToDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(56, 59);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.ShowCheckBox = true;
            this.dtpToDate.Size = new System.Drawing.Size(104, 20);
            this.dtpToDate.TabIndex = 1;
            this.dtpToDate.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "From :";
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(321, 409);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(76, 28);
            this.btnPrint.TabIndex = 12;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.grpDisplay);
            this.panel1.Controls.Add(this.grpGroup);
            this.panel1.Controls.Add(this.grpStockPoint);
            this.panel1.Controls.Add(this.grpSection);
            this.panel1.Controls.Add(this.grpItems);
            this.panel1.Controls.Add(this.grpCategory);
            this.panel1.Controls.Add(this.grpDepartment);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Location = new System.Drawing.Point(6, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(806, 382);
            this.panel1.TabIndex = 11;
            // 
            // grpDisplay
            // 
            this.grpDisplay.Controls.Add(this.rdDispStockpoint);
            this.grpDisplay.Controls.Add(this.rdDispSection);
            this.grpDisplay.Controls.Add(this.rdDispCostprice);
            this.grpDisplay.Controls.Add(this.rdDispYear);
            this.grpDisplay.Controls.Add(this.rdDispMonth);
            this.grpDisplay.Controls.Add(this.rdDispWriteoffDate);
            this.grpDisplay.Controls.Add(this.rdDispItem);
            this.grpDisplay.Controls.Add(this.rdDispCategory);
            this.grpDisplay.Controls.Add(this.rdDispDepartment);
            this.grpDisplay.Location = new System.Drawing.Point(698, 6);
            this.grpDisplay.Name = "grpDisplay";
            this.grpDisplay.Size = new System.Drawing.Size(101, 362);
            this.grpDisplay.TabIndex = 55;
            this.grpDisplay.TabStop = false;
            this.grpDisplay.Text = "Display";
            // 
            // rdDispStockpoint
            // 
            this.rdDispStockpoint.AutoSize = true;
            this.rdDispStockpoint.Location = new System.Drawing.Point(6, 27);
            this.rdDispStockpoint.Name = "rdDispStockpoint";
            this.rdDispStockpoint.Size = new System.Drawing.Size(77, 17);
            this.rdDispStockpoint.TabIndex = 29;
            this.rdDispStockpoint.Text = "StockPoint";
            this.rdDispStockpoint.UseVisualStyleBackColor = true;
            // 
            // rdDispSection
            // 
            this.rdDispSection.AutoSize = true;
            this.rdDispSection.Location = new System.Drawing.Point(6, 114);
            this.rdDispSection.Name = "rdDispSection";
            this.rdDispSection.Size = new System.Drawing.Size(61, 17);
            this.rdDispSection.TabIndex = 28;
            this.rdDispSection.Text = "Section";
            this.rdDispSection.UseVisualStyleBackColor = true;
            // 
            // rdDispCostprice
            // 
            this.rdDispCostprice.AutoSize = true;
            this.rdDispCostprice.Location = new System.Drawing.Point(6, 172);
            this.rdDispCostprice.Name = "rdDispCostprice";
            this.rdDispCostprice.Size = new System.Drawing.Size(70, 17);
            this.rdDispCostprice.TabIndex = 27;
            this.rdDispCostprice.Text = "CostPrice";
            this.rdDispCostprice.UseVisualStyleBackColor = true;
            // 
            // rdDispYear
            // 
            this.rdDispYear.AutoSize = true;
            this.rdDispYear.Location = new System.Drawing.Point(6, 259);
            this.rdDispYear.Name = "rdDispYear";
            this.rdDispYear.Size = new System.Drawing.Size(47, 17);
            this.rdDispYear.TabIndex = 25;
            this.rdDispYear.Text = "Year";
            this.rdDispYear.UseVisualStyleBackColor = true;
            // 
            // rdDispMonth
            // 
            this.rdDispMonth.AutoSize = true;
            this.rdDispMonth.Location = new System.Drawing.Point(6, 230);
            this.rdDispMonth.Name = "rdDispMonth";
            this.rdDispMonth.Size = new System.Drawing.Size(55, 17);
            this.rdDispMonth.TabIndex = 24;
            this.rdDispMonth.Text = "Month";
            this.rdDispMonth.UseVisualStyleBackColor = true;
            // 
            // rdDispWriteoffDate
            // 
            this.rdDispWriteoffDate.AutoSize = true;
            this.rdDispWriteoffDate.Location = new System.Drawing.Point(6, 201);
            this.rdDispWriteoffDate.Name = "rdDispWriteoffDate";
            this.rdDispWriteoffDate.Size = new System.Drawing.Size(85, 17);
            this.rdDispWriteoffDate.TabIndex = 22;
            this.rdDispWriteoffDate.Text = "WriteoffDate";
            this.rdDispWriteoffDate.UseVisualStyleBackColor = true;
            // 
            // rdDispItem
            // 
            this.rdDispItem.AutoSize = true;
            this.rdDispItem.Checked = true;
            this.rdDispItem.Location = new System.Drawing.Point(6, 143);
            this.rdDispItem.Name = "rdDispItem";
            this.rdDispItem.Size = new System.Drawing.Size(45, 17);
            this.rdDispItem.TabIndex = 18;
            this.rdDispItem.TabStop = true;
            this.rdDispItem.Text = "Item";
            this.rdDispItem.UseVisualStyleBackColor = true;
            // 
            // rdDispCategory
            // 
            this.rdDispCategory.AutoSize = true;
            this.rdDispCategory.Location = new System.Drawing.Point(6, 85);
            this.rdDispCategory.Name = "rdDispCategory";
            this.rdDispCategory.Size = new System.Drawing.Size(67, 17);
            this.rdDispCategory.TabIndex = 17;
            this.rdDispCategory.Text = "Category";
            this.rdDispCategory.UseVisualStyleBackColor = true;
            // 
            // rdDispDepartment
            // 
            this.rdDispDepartment.AutoSize = true;
            this.rdDispDepartment.Location = new System.Drawing.Point(6, 56);
            this.rdDispDepartment.Name = "rdDispDepartment";
            this.rdDispDepartment.Size = new System.Drawing.Size(80, 17);
            this.rdDispDepartment.TabIndex = 16;
            this.rdDispDepartment.Text = "Department";
            this.rdDispDepartment.UseVisualStyleBackColor = true;
            // 
            // grpGroup
            // 
            this.grpGroup.Controls.Add(this.rdGrpStockpoint);
            this.grpGroup.Controls.Add(this.rdGrpSection);
            this.grpGroup.Controls.Add(this.rdGrpCostPrice);
            this.grpGroup.Controls.Add(this.rdGrpYear);
            this.grpGroup.Controls.Add(this.rdGrpMonth);
            this.grpGroup.Controls.Add(this.rdGrpWriteoffDate);
            this.grpGroup.Controls.Add(this.rdGrpItem);
            this.grpGroup.Controls.Add(this.rdGrpCategory);
            this.grpGroup.Controls.Add(this.rdGrpDepartment);
            this.grpGroup.Location = new System.Drawing.Point(588, 6);
            this.grpGroup.Name = "grpGroup";
            this.grpGroup.Size = new System.Drawing.Size(101, 362);
            this.grpGroup.TabIndex = 54;
            this.grpGroup.TabStop = false;
            this.grpGroup.Text = "Group";
            // 
            // rdGrpStockpoint
            // 
            this.rdGrpStockpoint.AutoSize = true;
            this.rdGrpStockpoint.Checked = true;
            this.rdGrpStockpoint.Location = new System.Drawing.Point(8, 25);
            this.rdGrpStockpoint.Name = "rdGrpStockpoint";
            this.rdGrpStockpoint.Size = new System.Drawing.Size(77, 17);
            this.rdGrpStockpoint.TabIndex = 14;
            this.rdGrpStockpoint.TabStop = true;
            this.rdGrpStockpoint.Text = "StockPoint";
            this.rdGrpStockpoint.UseVisualStyleBackColor = true;
            // 
            // rdGrpSection
            // 
            this.rdGrpSection.AutoSize = true;
            this.rdGrpSection.Location = new System.Drawing.Point(8, 112);
            this.rdGrpSection.Name = "rdGrpSection";
            this.rdGrpSection.Size = new System.Drawing.Size(61, 17);
            this.rdGrpSection.TabIndex = 13;
            this.rdGrpSection.Text = "Section";
            this.rdGrpSection.UseVisualStyleBackColor = true;
            // 
            // rdGrpCostPrice
            // 
            this.rdGrpCostPrice.AutoSize = true;
            this.rdGrpCostPrice.Location = new System.Drawing.Point(8, 170);
            this.rdGrpCostPrice.Name = "rdGrpCostPrice";
            this.rdGrpCostPrice.Size = new System.Drawing.Size(70, 17);
            this.rdGrpCostPrice.TabIndex = 12;
            this.rdGrpCostPrice.Text = "CostPrice";
            this.rdGrpCostPrice.UseVisualStyleBackColor = true;
            // 
            // rdGrpYear
            // 
            this.rdGrpYear.AutoSize = true;
            this.rdGrpYear.Location = new System.Drawing.Point(8, 257);
            this.rdGrpYear.Name = "rdGrpYear";
            this.rdGrpYear.Size = new System.Drawing.Size(47, 17);
            this.rdGrpYear.TabIndex = 10;
            this.rdGrpYear.Text = "Year";
            this.rdGrpYear.UseVisualStyleBackColor = true;
            // 
            // rdGrpMonth
            // 
            this.rdGrpMonth.AutoSize = true;
            this.rdGrpMonth.Location = new System.Drawing.Point(8, 228);
            this.rdGrpMonth.Name = "rdGrpMonth";
            this.rdGrpMonth.Size = new System.Drawing.Size(55, 17);
            this.rdGrpMonth.TabIndex = 9;
            this.rdGrpMonth.Text = "Month";
            this.rdGrpMonth.UseVisualStyleBackColor = true;
            // 
            // rdGrpWriteoffDate
            // 
            this.rdGrpWriteoffDate.AutoSize = true;
            this.rdGrpWriteoffDate.Location = new System.Drawing.Point(8, 199);
            this.rdGrpWriteoffDate.Name = "rdGrpWriteoffDate";
            this.rdGrpWriteoffDate.Size = new System.Drawing.Size(85, 17);
            this.rdGrpWriteoffDate.TabIndex = 7;
            this.rdGrpWriteoffDate.Text = "WriteoffDate";
            this.rdGrpWriteoffDate.UseVisualStyleBackColor = true;
            // 
            // rdGrpItem
            // 
            this.rdGrpItem.AutoSize = true;
            this.rdGrpItem.Location = new System.Drawing.Point(8, 141);
            this.rdGrpItem.Name = "rdGrpItem";
            this.rdGrpItem.Size = new System.Drawing.Size(45, 17);
            this.rdGrpItem.TabIndex = 2;
            this.rdGrpItem.Text = "Item";
            this.rdGrpItem.UseVisualStyleBackColor = true;
            // 
            // rdGrpCategory
            // 
            this.rdGrpCategory.AutoSize = true;
            this.rdGrpCategory.Location = new System.Drawing.Point(8, 83);
            this.rdGrpCategory.Name = "rdGrpCategory";
            this.rdGrpCategory.Size = new System.Drawing.Size(67, 17);
            this.rdGrpCategory.TabIndex = 1;
            this.rdGrpCategory.Text = "Category";
            this.rdGrpCategory.UseVisualStyleBackColor = true;
            // 
            // rdGrpDepartment
            // 
            this.rdGrpDepartment.AutoSize = true;
            this.rdGrpDepartment.Location = new System.Drawing.Point(8, 54);
            this.rdGrpDepartment.Name = "rdGrpDepartment";
            this.rdGrpDepartment.Size = new System.Drawing.Size(80, 17);
            this.rdGrpDepartment.TabIndex = 0;
            this.rdGrpDepartment.Text = "Department";
            this.rdGrpDepartment.UseVisualStyleBackColor = true;
            // 
            // grpStockPoint
            // 
            this.grpStockPoint.Controls.Add(this.lstStockpoint);
            this.grpStockPoint.Controls.Add(this.rdSelStockpoint);
            this.grpStockPoint.Controls.Add(this.rdAllStockpoint);
            this.grpStockPoint.Location = new System.Drawing.Point(153, 193);
            this.grpStockPoint.Name = "grpStockPoint";
            this.grpStockPoint.Size = new System.Drawing.Size(148, 178);
            this.grpStockPoint.TabIndex = 53;
            this.grpStockPoint.TabStop = false;
            this.grpStockPoint.Text = "Stock Point";
            // 
            // lstStockpoint
            // 
            this.lstStockpoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstStockpoint.CheckBoxes = true;
            this.lstStockpoint.Location = new System.Drawing.Point(6, 55);
            this.lstStockpoint.Name = "lstStockpoint";
            this.lstStockpoint.Size = new System.Drawing.Size(137, 116);
            this.lstStockpoint.TabIndex = 2;
            this.lstStockpoint.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelStockpoint
            // 
            this.rdSelStockpoint.AutoSize = true;
            this.rdSelStockpoint.Location = new System.Drawing.Point(11, 36);
            this.rdSelStockpoint.Name = "rdSelStockpoint";
            this.rdSelStockpoint.Size = new System.Drawing.Size(121, 17);
            this.rdSelStockpoint.TabIndex = 1;
            this.rdSelStockpoint.Text = "Selected Stockpoint";
            this.rdSelStockpoint.UseVisualStyleBackColor = true;
            // 
            // rdAllStockpoint
            // 
            this.rdAllStockpoint.AutoSize = true;
            this.rdAllStockpoint.Checked = true;
            this.rdAllStockpoint.Location = new System.Drawing.Point(11, 18);
            this.rdAllStockpoint.Name = "rdAllStockpoint";
            this.rdAllStockpoint.Size = new System.Drawing.Size(90, 17);
            this.rdAllStockpoint.TabIndex = 0;
            this.rdAllStockpoint.TabStop = true;
            this.rdAllStockpoint.Text = "All Stockpoint";
            this.rdAllStockpoint.UseVisualStyleBackColor = true;
            // 
            // grpSection
            // 
            this.grpSection.Controls.Add(this.lstSection);
            this.grpSection.Controls.Add(this.rdSelSection);
            this.grpSection.Controls.Add(this.rdAllSection);
            this.grpSection.Location = new System.Drawing.Point(2, 193);
            this.grpSection.Name = "grpSection";
            this.grpSection.Size = new System.Drawing.Size(148, 178);
            this.grpSection.TabIndex = 52;
            this.grpSection.TabStop = false;
            this.grpSection.Text = "Section";
            // 
            // lstSection
            // 
            this.lstSection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstSection.CheckBoxes = true;
            this.lstSection.Location = new System.Drawing.Point(6, 55);
            this.lstSection.Name = "lstSection";
            this.lstSection.Size = new System.Drawing.Size(137, 116);
            this.lstSection.TabIndex = 2;
            this.lstSection.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelSection
            // 
            this.rdSelSection.AutoSize = true;
            this.rdSelSection.Location = new System.Drawing.Point(11, 36);
            this.rdSelSection.Name = "rdSelSection";
            this.rdSelSection.Size = new System.Drawing.Size(106, 17);
            this.rdSelSection.TabIndex = 1;
            this.rdSelSection.Text = "Selected Section";
            this.rdSelSection.UseVisualStyleBackColor = true;
            // 
            // rdAllSection
            // 
            this.rdAllSection.AutoSize = true;
            this.rdAllSection.Checked = true;
            this.rdAllSection.Location = new System.Drawing.Point(11, 18);
            this.rdAllSection.Name = "rdAllSection";
            this.rdAllSection.Size = new System.Drawing.Size(75, 17);
            this.rdAllSection.TabIndex = 0;
            this.rdAllSection.TabStop = true;
            this.rdAllSection.Text = "All Section";
            this.rdAllSection.UseVisualStyleBackColor = true;
            // 
            // grpItems
            // 
            this.grpItems.Controls.Add(this.lstItem);
            this.grpItems.Controls.Add(this.rdSelItem);
            this.grpItems.Controls.Add(this.rdAllItem);
            this.grpItems.Location = new System.Drawing.Point(308, 6);
            this.grpItems.Name = "grpItems";
            this.grpItems.Size = new System.Drawing.Size(274, 178);
            this.grpItems.TabIndex = 51;
            this.grpItems.TabStop = false;
            this.grpItems.Text = "Items";
            // 
            // lstItem
            // 
            this.lstItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstItem.CheckBoxes = true;
            this.lstItem.Location = new System.Drawing.Point(6, 55);
            this.lstItem.Name = "lstItem";
            this.lstItem.Size = new System.Drawing.Size(262, 116);
            this.lstItem.TabIndex = 2;
            this.lstItem.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelItem
            // 
            this.rdSelItem.AutoSize = true;
            this.rdSelItem.Location = new System.Drawing.Point(9, 36);
            this.rdSelItem.Name = "rdSelItem";
            this.rdSelItem.Size = new System.Drawing.Size(95, 17);
            this.rdSelItem.TabIndex = 1;
            this.rdSelItem.Text = "Selected Items";
            this.rdSelItem.UseVisualStyleBackColor = true;
            // 
            // rdAllItem
            // 
            this.rdAllItem.AutoSize = true;
            this.rdAllItem.Checked = true;
            this.rdAllItem.Location = new System.Drawing.Point(9, 18);
            this.rdAllItem.Name = "rdAllItem";
            this.rdAllItem.Size = new System.Drawing.Size(64, 17);
            this.rdAllItem.TabIndex = 0;
            this.rdAllItem.TabStop = true;
            this.rdAllItem.Text = "All Items";
            this.rdAllItem.UseVisualStyleBackColor = true;
            // 
            // grpCategory
            // 
            this.grpCategory.Controls.Add(this.lstCategory);
            this.grpCategory.Controls.Add(this.rdSelCatg);
            this.grpCategory.Controls.Add(this.rdAllCatg);
            this.grpCategory.Location = new System.Drawing.Point(153, 6);
            this.grpCategory.Name = "grpCategory";
            this.grpCategory.Size = new System.Drawing.Size(148, 178);
            this.grpCategory.TabIndex = 50;
            this.grpCategory.TabStop = false;
            this.grpCategory.Text = "Category";
            // 
            // lstCategory
            // 
            this.lstCategory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstCategory.CheckBoxes = true;
            this.lstCategory.Location = new System.Drawing.Point(6, 55);
            this.lstCategory.Name = "lstCategory";
            this.lstCategory.Size = new System.Drawing.Size(137, 116);
            this.lstCategory.TabIndex = 2;
            this.lstCategory.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelCatg
            // 
            this.rdSelCatg.AutoSize = true;
            this.rdSelCatg.Location = new System.Drawing.Point(6, 36);
            this.rdSelCatg.Name = "rdSelCatg";
            this.rdSelCatg.Size = new System.Drawing.Size(120, 17);
            this.rdSelCatg.TabIndex = 1;
            this.rdSelCatg.Text = "Selected Categories";
            this.rdSelCatg.UseVisualStyleBackColor = true;
            // 
            // rdAllCatg
            // 
            this.rdAllCatg.AutoSize = true;
            this.rdAllCatg.Checked = true;
            this.rdAllCatg.Location = new System.Drawing.Point(6, 18);
            this.rdAllCatg.Name = "rdAllCatg";
            this.rdAllCatg.Size = new System.Drawing.Size(89, 17);
            this.rdAllCatg.TabIndex = 0;
            this.rdAllCatg.TabStop = true;
            this.rdAllCatg.Text = "All Categories";
            this.rdAllCatg.UseVisualStyleBackColor = true;
            // 
            // grpDepartment
            // 
            this.grpDepartment.Controls.Add(this.lstDepartment);
            this.grpDepartment.Controls.Add(this.rdSelDept);
            this.grpDepartment.Controls.Add(this.rdAllDept);
            this.grpDepartment.Location = new System.Drawing.Point(2, 6);
            this.grpDepartment.Name = "grpDepartment";
            this.grpDepartment.Size = new System.Drawing.Size(148, 178);
            this.grpDepartment.TabIndex = 49;
            this.grpDepartment.TabStop = false;
            this.grpDepartment.Text = "Department";
            // 
            // lstDepartment
            // 
            this.lstDepartment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstDepartment.CheckBoxes = true;
            this.lstDepartment.Location = new System.Drawing.Point(6, 55);
            this.lstDepartment.Name = "lstDepartment";
            this.lstDepartment.Size = new System.Drawing.Size(137, 116);
            this.lstDepartment.TabIndex = 2;
            this.lstDepartment.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelDept
            // 
            this.rdSelDept.AutoSize = true;
            this.rdSelDept.Location = new System.Drawing.Point(6, 36);
            this.rdSelDept.Name = "rdSelDept";
            this.rdSelDept.Size = new System.Drawing.Size(125, 17);
            this.rdSelDept.TabIndex = 1;
            this.rdSelDept.Text = "Selected Department";
            this.rdSelDept.UseVisualStyleBackColor = true;
            // 
            // rdAllDept
            // 
            this.rdAllDept.AutoSize = true;
            this.rdAllDept.Checked = true;
            this.rdAllDept.Location = new System.Drawing.Point(6, 18);
            this.rdAllDept.Name = "rdAllDept";
            this.rdAllDept.Size = new System.Drawing.Size(99, 17);
            this.rdAllDept.TabIndex = 0;
            this.rdAllDept.TabStop = true;
            this.rdAllDept.Text = "All Departments";
            this.rdAllDept.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkCPNotReq);
            this.groupBox1.Controls.Add(this.chkUOMNotReq);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.dtpToDate);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dtpFrDate);
            this.groupBox1.Location = new System.Drawing.Point(308, 193);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(274, 175);
            this.groupBox1.TabIndex = 48;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Stock Write Off Date";
            // 
            // chkCPNotReq
            // 
            this.chkCPNotReq.AutoSize = true;
            this.chkCPNotReq.Location = new System.Drawing.Point(144, 138);
            this.chkCPNotReq.Name = "chkCPNotReq";
            this.chkCPNotReq.Size = new System.Drawing.Size(106, 17);
            this.chkCPNotReq.TabIndex = 31;
            this.chkCPNotReq.Text = "CP Not Required";
            this.chkCPNotReq.UseVisualStyleBackColor = true;
            // 
            // chkUOMNotReq
            // 
            this.chkUOMNotReq.AutoSize = true;
            this.chkUOMNotReq.Location = new System.Drawing.Point(16, 138);
            this.chkUOMNotReq.Name = "chkUOMNotReq";
            this.chkUOMNotReq.Size = new System.Drawing.Size(117, 17);
            this.chkUOMNotReq.TabIndex = 30;
            this.chkUOMNotReq.Text = "UOM Not Required";
            this.chkUOMNotReq.UseVisualStyleBackColor = true;
            // 
            // FrmRptStockWriteOffItemsSumm
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(816, 458);
            this.ControlBox = false;
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmRptStockWriteOffItemsSumm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stock Write-Off Items Summ";
            this.Load += new System.EventHandler(this.FrmRptStockWriteOffItemsSumm_Load);
            this.panel1.ResumeLayout(false);
            this.grpDisplay.ResumeLayout(false);
            this.grpDisplay.PerformLayout();
            this.grpGroup.ResumeLayout(false);
            this.grpGroup.PerformLayout();
            this.grpStockPoint.ResumeLayout(false);
            this.grpStockPoint.PerformLayout();
            this.grpSection.ResumeLayout(false);
            this.grpSection.PerformLayout();
            this.grpItems.ResumeLayout(false);
            this.grpItems.PerformLayout();
            this.grpCategory.ResumeLayout(false);
            this.grpCategory.PerformLayout();
            this.grpDepartment.ResumeLayout(false);
            this.grpDepartment.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}