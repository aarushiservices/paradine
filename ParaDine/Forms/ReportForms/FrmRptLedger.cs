﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using ParaDine.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class FrmRptLedger : Form
    {
        private Button BtnExit;
        private Button BtnPrint;
        private char ChrGlbType;
        private ComboBox CmbSource;
        //private IContainer components = null;
        private DateTimePicker DtpFrom;
        private DateTimePicker DtpTo;
        private GroupBox grpCheckIn;
        private Label label4;
        private Label label7;
        private Label lblSource;
        private Panel panel1;

        public FrmRptLedger(char chrType)
        {
            this.InitializeComponent();
            this.ChrGlbType = chrType;
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                string srcTable = "";
                SqlCommand command = new SqlCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    Connection = GlobalVariables.SqlConn
                };
                if (this.ChrGlbType == 'C')
                {
                    command.CommandText = "RES_PROC_CUSTLEDGER";
                    if (this.CmbSource.SelectedIndex != -1)
                    {
                        command.Parameters.AddWithValue("@CUSTID", Convert.ToInt32(this.CmbSource.SelectedValue));
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@CUSTID", DBNull.Value);
                    }
                    command.Parameters.AddWithValue("@STDATE", Convert.ToDateTime(this.DtpFrom.Text));
                    command.Parameters.AddWithValue("@ENDDATE", Convert.ToDateTime(this.DtpTo.Text));
                    srcTable = "RES_PROC_CUSTLEDGER";
                }
                else
                {
                    command.CommandText = "RES_PROC_SUPPLEDGER";
                    if (this.CmbSource.SelectedIndex != -1)
                    {
                        command.Parameters.AddWithValue("@SUPPID", Convert.ToInt32(this.CmbSource.SelectedValue));
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@SUPPID", DBNull.Value);
                    }
                    command.Parameters.AddWithValue("@STDATE", Convert.ToDateTime(this.DtpFrom.Text));
                    command.Parameters.AddWithValue("@ENDDATE", Convert.ToDateTime(this.DtpTo.Text));
                    srcTable = "RES_PROC_SUPPLEDGER";
                }
                SqlDataAdapter adapter = new SqlDataAdapter
                {
                    SelectCommand = command
                };
                DataSet dataSet = new DataSet();
                adapter.Fill(dataSet, srcTable);
                //if (dataSet.Tables[srcTable].Rows.Count > 0)
                //{
                //    //ParameterFields pfields = new ParameterFields();
                //ParameterField parameterField = new ParameterField();
                //ParameterDiscreteValue value2 = new ParameterDiscreteValue();
                //parameterField.Name = "COMPNAME";
                //value2.Value = GlobalVariables.StrCompName;
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                //parameterField = new ParameterField();
                //value2 = new ParameterDiscreteValue();
                //parameterField.Name = "PRAMSTR";
                //value2.Value = "FROM = " + this.DtpFrom.Text + " TO = " + this.DtpTo.Text;
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);

                ///By Bharat we have to change Reports to itextsparp pdf
                ///
                RptCustLedger rpt = new RptCustLedger(dataSet.Tables[srcTable]);

                // rpt.Parameter_bal=;
                // rpt.Parameter_ClosingBal=;
                rpt.Parameter_CompName = GlobalVariables.StrCompName;
                rpt.Parameter_Pramstr = "FROM = " + this.DtpFrom.Text + " TO = " + this.DtpTo.Text;
                rpt.Show();
                // rpt.SetDataSource(dataSet.Tables[srcTable]);
                // rpt.Refresh();
                //        new FrmRptViewer(rpt, pfields).Show();
            }
            //}
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Print_Click ");
            }
        }
        private void FrmRptCustomerLedger_Load(object sender, EventArgs e)
        {
            try
            {
                if (this.ChrGlbType == 'C')
                {
                    this.lblSource.Text = "Customer Name :";
                }
                else
                {
                    this.lblSource.Text = "Supplier Name :";
                }
                this.RefreshData();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Form Load");
            }
        }
        private void RefreshData()
        {
            if (this.ChrGlbType == 'C')
            {
                GlobalFill.FillCombo("SELECT CUSTOMERID, CUSTOMERNAME + '('+ CUSTOMERCODE+')' FROM RES_CUSTOMER", this.CmbSource);
            }
            else
            {
                GlobalFill.FillCombo("SELECT SUPPLIERID, SUPPLIERNAME + '('+ SUPPLIERCODE+')' FROM RES_SUPPLIER", this.CmbSource);
            }
            this.DtpFrom.Value = GlobalVariables.BusinessDate;
            this.DtpTo.Value = GlobalVariables.BusinessDate;
        }
    }
}
