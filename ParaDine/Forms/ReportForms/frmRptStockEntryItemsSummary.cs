﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using ParaDine.Reports;
using ParaDinePrint;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmRptStockEntryItemsSummary : Form
    {
        private Button btnExit;
        private Button btnPrint;
        private CheckBox chkCPNotReq;
        private CheckBox chkUOMNotReq;    
        private DataSet DS = new DataSet();
        private DateTimePicker dtpEntryDateFrom;
        private DateTimePicker dtpEntryDateTo;
        private DateTimePicker dtpFrInvoiceDate;
        private DateTimePicker DtpToInvoiceDate;
        private GroupBox groupBox3;
        internal GroupBox grpCategory;
        internal GroupBox grpDepartment;
        private GroupBox grpDisplay;
        private GroupBox grpGroup;
        internal GroupBox grpItems;
        internal GroupBox grpSection;
        internal GroupBox grpStockPoint;
        internal GroupBox grpSupplier;
        private Label label1;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label16;
        private Label label17;
        private Label label3;
        internal ListView lstCategory;
        internal ListView lstDepartment;
        internal ListView lstItem;
        internal ListView lstSection;
        internal ListView lstStockpoint;
        internal ListView lstSupplier;
        private PrintingFunctions pfunctions = new PrintingFunctions();
        private Panel pnlSalesSummary;
        private ClsColDosPrint[] prnItem;
        private ClsRptDosPrint prnrpt = new ClsRptDosPrint();
        internal RadioButton rdAllCatg;
        internal RadioButton rdAllDept;
        internal RadioButton rdAllItem;
        internal RadioButton rdAllSection;
        internal RadioButton rdAllStockpoint;
        internal RadioButton rdAllSupplier;
        private RadioButton rdDispCategory;
        private RadioButton rdDispCostprice;
        private RadioButton rdDispDepartment;
        private RadioButton rdDispEntryDate;
        private RadioButton rdDispEntryNo;
        private RadioButton rdDispInvoiceDate;
        private RadioButton rdDispInvoiceNo;
        private RadioButton rdDispItem;
        private RadioButton rdDispMonth;
        private RadioButton rdDispSection;
        private RadioButton rdDispStockpoint;
        private RadioButton rdDispSupplier;
        private RadioButton rdDispYear;
        private RadioButton rdGrpCategory;
        private RadioButton rdGrpCostPrice;
        private RadioButton rdGrpDepartment;
        private RadioButton rdGrpEntryDate;
        private RadioButton rdGrpEntryNo;
        private RadioButton rdGrpInvoiceDate;
        private RadioButton rdGrpInvoiceNo;
        private RadioButton rdGrpItem;
        private RadioButton rdGrpMonth;
        private RadioButton rdGrpSection;
        private RadioButton rdGrpStockpoint;
        private RadioButton rdGrpSupplier;
        private RadioButton rdGrpYear;
        internal RadioButton rdSelCatg;
        internal RadioButton rdSelDept;
        internal RadioButton rdSelItem;
        internal RadioButton rdSelSection;
        internal RadioButton rdSelStockpoint;
        internal RadioButton rdSelSupplier;
        private string rptname = "";
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private SqlCommand SqlCmd = new SqlCommand();
        private string StrSql = "";
        private TextBox txtFrCode;
        private TextBox txtInvoiceNo;
        private TextBox txtToCode;

        public frmRptStockEntryItemsSummary()
        {
            this.InitializeComponent();
        }

        private void AddHandlers()
        {
            this.lstDepartment.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstCategory.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstItem.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstSection.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstStockpoint.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstSupplier.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.rdAllDept.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllCatg.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllItem.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllSection.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllStockpoint.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllSupplier.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                object obj2;
                this.DS.Tables.Clear();
                string str = "";
                this.StrSql = "SELECT ";
                this.StrSql = this.StrSql + this.GetField(this.grpGroup) + " AS CATEGORYNAME,";
                this.StrSql = this.StrSql + this.GetField(this.grpDisplay) + " AS ITEMNAME,";
                if (!this.chkUOMNotReq.Checked)
                {
                    this.StrSql = this.StrSql + " U.UOMNAME, ";
                }
                if (!this.chkCPNotReq.Checked)
                {
                    this.StrSql = this.StrSql + " ST.COSTPRICE, ";
                }
                this.StrSql = this.StrSql + " SUM(ST.RECEIVEDQTY) AS RECEIVEDQTY,  SUM(ST.GROSSAMT) AS GROSSAMT, SUM(ST.TAXAMOUNT) AS TAXAMOUNT, SUM(ST.DISCAMOUNT) AS DISCAMOUNT,  SUM(ST.AMOUNT) AS AMOUNT ";
                this.StrSql = this.StrSql + " FROM RES_VW_STOCKENTRYTRANS ST  INNER JOIN RES_VW_STOCKENTRYMASTER SM ON SM.STOCKENTRYMASTERID = ST.STOCKENTRYMASTERID  INNER JOIN RES_ITEM IT ON IT.ITEMID = ST.ITEMID  INNER JOIN RES_UOM U ON U.UOMID = ST.UOMID  INNER JOIN RES_SECTION SC ON SC.SECTIONID = IT.SECTIONID  INNER JOIN RES_CATEGORY CG ON CG.CATEGORYID = IT.CATEGORYID  INNER JOIN RES_DEPARTMENT DP ON DP.DEPARTMENTID = CG.DEPARTMENTID ";
                this.StrSql = this.StrSql + " WHERE IT.ITEMID <> 0 ";
                if (this.dtpEntryDateFrom.Checked)
                {
                    obj2 = this.StrSql;
                    this.StrSql = string.Concat(new object[] { obj2, " AND SM.STOCKENTRYDATE >= '", this.dtpEntryDateFrom.Value, "'" });
                    str = str + " Date:" + this.dtpEntryDateFrom.Text;
                }
                if (this.dtpEntryDateTo.Checked)
                {
                    obj2 = this.StrSql;
                    this.StrSql = string.Concat(new object[] { obj2, " AND SM.STOCKENTRYDATE <= '", this.dtpEntryDateTo.Value, "'" });
                    str = str + " To:" + this.dtpEntryDateTo.Text;
                }
                if (this.dtpFrInvoiceDate.Checked)
                {
                    obj2 = this.StrSql;
                    this.StrSql = string.Concat(new object[] { obj2, " AND SM.INVOICEDATE >= '", this.dtpFrInvoiceDate.Value, "'" });
                    str = str + " InvDate:" + this.dtpFrInvoiceDate.Text;
                }
                if (this.DtpToInvoiceDate.Checked)
                {
                    obj2 = this.StrSql;
                    this.StrSql = string.Concat(new object[] { obj2, " AND SM.INVOICEDATE <= '", this.DtpToInvoiceDate.Value, "'" });
                    str = str + " To:" + this.DtpToInvoiceDate.Text;
                }
                if (this.rdSelDept.Checked && (this.GetCollString(this.lstDepartment) != ""))
                {
                    this.StrSql = this.StrSql + " AND DP.DEPARTMENTID IN (" + this.GetCollString(this.lstDepartment) + ")";
                    str = str + " Depts: " + this.GetRetParam(this.lstDepartment);
                }
                if (this.rdSelCatg.Checked && (this.GetCollString(this.lstCategory) != ""))
                {
                    this.StrSql = this.StrSql + " AND CG.CATEGORYID IN (" + this.GetCollString(this.lstCategory) + ")";
                    str = str + " Catg: " + this.GetRetParam(this.lstCategory);
                }
                if (this.rdSelStockpoint.Checked && (this.GetCollString(this.lstStockpoint) != ""))
                {
                    this.StrSql = this.StrSql + " AND SM.STOCKPOINTID IN (" + this.GetCollString(this.lstStockpoint) + ")";
                    str = str + " StockPoints: " + this.GetRetParam(this.lstStockpoint);
                }
                if (this.rdSelSection.Checked && (this.GetCollString(this.lstSection) != ""))
                {
                    this.StrSql = this.StrSql + " AND SC.SECTIONID IN (" + this.GetCollString(this.lstSection) + ")";
                    str = str + " Sections: " + this.GetRetParam(this.lstSection);
                }
                if (this.rdSelItem.Checked && (this.GetCollString(this.lstItem) != ""))
                {
                    this.StrSql = this.StrSql + " AND IT.ITEMID IN (" + this.GetCollString(this.lstItem) + ")";
                    str = str + " Items: " + this.GetRetParam(this.lstItem);
                }
                if (this.rdSelSupplier.Checked && (this.GetCollString(this.lstSupplier) != ""))
                {
                    this.StrSql = this.StrSql + " AND SM.SUPPLIERID IN (" + this.GetCollString(this.lstSupplier) + ")";
                    str = str + " Suppliers: " + this.GetRetParam(this.lstSupplier);
                }
                if (this.txtFrCode.Text != "")
                {
                    this.StrSql = this.StrSql + " AND SM.STOCKENTRYNO >= '" + this.txtFrCode.Text + "'";
                    str = str + " StockEntryNo: " + this.txtFrCode.Text;
                }
                if (this.txtToCode.Text != "")
                {
                    this.StrSql = this.StrSql + " AND SM.STOCKENTRYNO <= '" + this.txtToCode.Text + "'";
                    str = str + " To: " + this.txtToCode.Text;
                }
                if (this.txtInvoiceNo.Text != "")
                {
                    this.StrSql = this.StrSql + " AND SM.INVOICENO = '" + this.txtInvoiceNo.Text + "'";
                    str = str + " Invoice: " + this.txtInvoiceNo.Text;
                }
                string strSql = this.StrSql;
                this.StrSql = strSql + " GROUP BY " + this.GetField(this.grpGroup) + "," + this.GetField(this.grpDisplay);
                if (!this.chkUOMNotReq.Checked)
                {
                    this.StrSql = this.StrSql + " ,U.UOMNAME ";
                }
                if (!this.chkCPNotReq.Checked)
                {
                    this.StrSql = this.StrSql + ", ST.COSTPRICE ";
                }
                GlobalFill.FillDataSet(this.StrSql, "SUMMARY", this.DS, this.SDA);
                //ParameterFields pfields = new ParameterFields();
                //ParameterField parameterField = new ParameterField
                //{
                //    Name = "Parameter"
                //};
                //ParameterDiscreteValue value2 = new ParameterDiscreteValue
                //{
                //    Value = str
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                //parameterField = new ParameterField
                //{
                //    Name = "CompanyName"
                //};
                //value2 = new ParameterDiscreteValue
                //{
                //    Value = GlobalVariables.StrCompName
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                if (this.DS.Tables["SUMMARY"].Rows.Count > 0)
                {
                    ///By Bharat we have to change Reports to itextsparp pdf
                    ///
                    RptStockEntryItemsSummary rpt = new RptStockEntryItemsSummary(this.DS.Tables["SUMMARY"]);
                    rpt.Parameter_CompanyName=GlobalVariables.StrCompName;
                    rpt.Parameter_Parameter = str;
                    //rpt.SetDataSource(this.DS.Tables["SUMMARY"]);
                    //rpt.Refresh();
                    //new FrmRptViewer(rpt, pfields).ShowDialog();
                }
                else
                {
                    MessageBox.Show("DATA NOT FOUND");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Print_Click");
            }
        }

      

        private void frmRptStockEntryItemsSummary_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.RefreshData();
            this.AddHandlers();
        }

        private string GetCollString(ListView paramLvw)
        {
            try
            {
                string str = "";
                for (int i = 0; i < paramLvw.CheckedItems.Count; i++)
                {
                    if (str != "")
                    {
                        str = str + ", ";
                    }
                    str = str + Convert.ToString(paramLvw.CheckedItems[i].Tag);
                }
                return str;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in function Collection String");
                return "";
            }
        }

        private string GetField(GroupBox paramGrp)
        {
            try
            {
                foreach (RadioButton button in paramGrp.Controls)
                {
                    if (button.Checked)
                    {
                        if (button.Text.ToUpper() == "DEPARTMENT")
                        {
                            return "DP.DEPARTMENTNAME";
                        }
                        if (button.Text.ToUpper() == "CATEGORY")
                        {
                            return "CG.CATEGORYNAME";
                        }
                        if (button.Text.ToUpper() == "STOCKPOINT")
                        {
                            return "SM.STOCKPOINTNAME";
                        }
                        if (button.Text.ToUpper() == "SECTION")
                        {
                            return "SC.SECTIONNAME";
                        }
                        if (button.Text.ToUpper() == "ITEM")
                        {
                            return "ST.ITEMNAME";
                        }
                        if (button.Text.ToUpper() == "COSTPRICE")
                        {
                            return "ST.COSTPRICE";
                        }
                        if (button.Text.ToUpper() == "ENTRYNO")
                        {
                            return "SM.STOCKENTRYNO";
                        }
                        if (button.Text.ToUpper() == "ENTRYDATE")
                        {
                            return "CONVERT(VARCHAR, SM.STOCKENTRYDATE, 102)";
                        }
                        if (button.Text.ToUpper() == "MONTH")
                        {
                            return "'MONTH-' + CONVERT(VARCHAR, MONTH(SM.STOCKENTRYDATE)) + '/' + CONVERT(VARCHAR, YEAR(SM.STOCKENTRYDATE))";
                        }
                        if (button.Text.ToUpper() == "YEAR")
                        {
                            return "'YEAR-' + CONVERT(VARCHAR, YEAR(SM.STOCKENTRYDATE))";
                        }
                        if (button.Text.ToUpper() == "SUPPLIER")
                        {
                            return "SM.SUPPLIERNAME";
                        }
                        if (button.Text.ToUpper() == "INVOICENO")
                        {
                            return "SM.INVOICENO";
                        }
                        if (button.Text.ToUpper() == "INVOICEDATE")
                        {
                            return "CONVERT(VARCHAR, SM.INVOICEDATE, 102)";
                        }
                    }
                }
                return "";
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in GetFied");
                return "";
            }
        }

        private string GetParamField(GroupBox paramGrp)
        {
            try
            {
                foreach (RadioButton button in paramGrp.Controls)
                {
                    if (button.Checked)
                    {
                        if (button.Text.ToUpper() == "DEPARTMENT")
                        {
                            return "DEPARTMENT";
                        }
                        if (button.Text.ToUpper() == "CATEGORY")
                        {
                            return "CATEGORY";
                        }
                        if (button.Text.ToUpper() == "STOCKPOINT")
                        {
                            return "STOCKPOINT";
                        }
                        if (button.Text.ToUpper() == "SECTION")
                        {
                            return "SECTION";
                        }
                        if (button.Text.ToUpper() == "ITEM")
                        {
                            return "ITEM";
                        }
                        if (button.Text.ToUpper() == "COSTPRICE")
                        {
                            return "COSTPRICE";
                        }
                        if (button.Text.ToUpper() == "ENTRYNO")
                        {
                            return "ENTRYNO";
                        }
                        if (button.Text.ToUpper() == "ENTRYDATE")
                        {
                            return "ENTRYDATE";
                        }
                        if (button.Text.ToUpper() == "MONTH")
                        {
                            return "MONTH";
                        }
                        if (button.Text.ToUpper() == "YEAR")
                        {
                            return "YEAR";
                        }
                        if (button.Text.ToUpper() == "SUPPLIER")
                        {
                            return "SUPPLIER";
                        }
                        if (button.Text.ToUpper() == "INVOICENO")
                        {
                            return "SM.INVOICENO";
                        }
                        if (button.Text.ToUpper() == "INVOICEDATE")
                        {
                            return "CONVERT(VARCHAR, SM.INVOICEDATE, 102)";
                        }
                    }
                }
                return "";
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in GetFied");
                return "";
            }
        }

        private string GetRetParam(ListView ParamLstVw)
        {
            try
            {
                string str = "";
                for (int i = 0; i < ParamLstVw.CheckedItems.Count; i++)
                {
                    if (str != "")
                    {
                        str = str + ", ";
                    }
                    str = str + Convert.ToString(ParamLstVw.CheckedItems[i].Text);
                }
                return str;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in function Collection String");
                return "";
            }
        }

  

        private void lst_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            try
            {
                if (e.Item.Checked)
                {
                    ListView view = (ListView)sender;
                    if (view.CheckedItems.Count > 0)
                    {
                        foreach (Control control in view.Parent.Controls)
                        {
                            if (control.GetType() == typeof(RadioButton))
                            {
                                RadioButton button = (RadioButton)control;
                                if (button.Text.ToUpper().Contains("SELECTED"))
                                {
                                    button.Checked = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in ListViewChecked");
            }
        }

        private void rdAll_checkchanged(object sender, EventArgs e)
        {
            try
            {
                RadioButton button = (RadioButton)sender;
                if (button.Checked)
                {
                    foreach (Control control in button.Parent.Controls)
                    {
                        if (control.GetType() == typeof(ListView))
                        {
                            ListView view = (ListView)control;
                            foreach (ListViewItem item in view.CheckedItems)
                            {
                                item.Checked = false;
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in AllRd");
            }
        }

        private void RefreshData()
        {
            this.StrSql = "SELECT DEPARTMENTID, DEPARTMENTNAME FROM RES_DEPARTMENT";
            GlobalFill.FillListView(this.lstDepartment, GlobalFill.FillDataTable(this.StrSql));
            this.StrSql = "SELECT CATEGORYID, CATEGORYNAME FROM RES_CATEGORY";
            GlobalFill.FillListView(this.lstCategory, GlobalFill.FillDataTable(this.StrSql));
            this.StrSql = "SELECT SECTIONID, SECTIONNAME FROM RES_SECTION";
            GlobalFill.FillListView(this.lstSection, GlobalFill.FillDataTable(this.StrSql));
            this.StrSql = "SELECT ITEMID, ITEMNAME FROM RES_ITEM";
            GlobalFill.FillListView(this.lstItem, GlobalFill.FillDataTable(this.StrSql));
            this.StrSql = "SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT";
            GlobalFill.FillListView(this.lstStockpoint, GlobalFill.FillDataTable(this.StrSql));
            this.StrSql = "SELECT SUPPLIERID, SUPPLIERNAME FROM RES_SUPPLIER";
            GlobalFill.FillListView(this.lstSupplier, GlobalFill.FillDataTable(this.StrSql));
            this.dtpEntryDateFrom.Value = new DateTime(GlobalVariables.BusinessDate.Year, GlobalVariables.BusinessDate.Month, GlobalVariables.BusinessDate.Day, 0, 0, 0);
            this.dtpEntryDateTo.Value = GlobalVariables.BusinessDate;
            this.dtpFrInvoiceDate.Value = new DateTime(GlobalVariables.BusinessDate.Year, GlobalVariables.BusinessDate.Month, GlobalVariables.BusinessDate.Day, 0, 0, 0);
            this.DtpToInvoiceDate.Value = GlobalVariables.BusinessDate;
        }
    }
}
