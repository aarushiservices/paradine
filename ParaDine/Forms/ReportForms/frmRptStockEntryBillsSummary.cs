﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using ParaDine.Reports;
using ParaDinePrint;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmRptStockEntryBillsSummary : Form
    {
        private Button btnExit;
        private Button btnPrint;
        private ComboBox cmbPrintOn;
        private ComboBox cmbSupplier;
       // private IContainer components = null;
        private DataSet DS = new DataSet();
        private DateTimePicker DtpFrInvDate;
        private DateTimePicker DtpFrStockEntryDate;
        private DateTimePicker DtpToInvDate;
        private DateTimePicker DtpToStockEntryDate;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private PrintingFunctions pfunctions = new PrintingFunctions();
        private Panel pnlPO;
        private ClsColDosPrint[] prnItem;
        private ClsRptDosPrint prnrpt = new ClsRptDosPrint();
        private string rptname = "";
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private string StrSql = "";
        private TextBox txtStkEntryNo;

        public frmRptStockEntryBillsSummary()
        {
            this.InitializeComponent();
        }

        private bool BillPageHeader(ref ClsRptDosPrint ParamClsRpt, string ParamWhere)
        {
            try
            {
                return true;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Bill Page Header");
                return false;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                string str = "";
                this.StrSql = "SELECT * FROM RES_VW_STOCKENTRYMASTER WHERE STOCKENTRYNO <> ''";
                if (this.txtStkEntryNo.Text.Trim() != "")
                {
                    this.StrSql = this.StrSql + " AND STOCKENTRYNO = '" + this.txtStkEntryNo.Text.Trim() + "'";
                    str = str + " StockEntryNo : " + this.txtStkEntryNo.Text + "; ";
                }
                if (this.cmbSupplier.Text != "")
                {
                    this.StrSql = this.StrSql + " AND SUPPLIERNAME = '" + this.cmbSupplier.Text + "'";
                    str = str + " Supplier : " + this.cmbSupplier.Text + "; ";
                }
                if (this.DtpFrStockEntryDate.Checked)
                {
                    this.StrSql = this.StrSql + " AND STOCKENTRYDATE >= '" + this.DtpFrStockEntryDate.Text + "'";
                    str = str + " StockEntryDate From : " + this.DtpFrStockEntryDate.Text + "; ";
                }
                if (this.DtpToStockEntryDate.Checked)
                {
                    this.StrSql = this.StrSql + " AND STOCKENTRYDATE <= '" + this.DtpToStockEntryDate.Text + "'";
                    str = str + " StockEntryDate UpTo : " + this.DtpToStockEntryDate.Text + "; ";
                }
                if (this.DtpFrInvDate.Checked)
                {
                    this.StrSql = this.StrSql + " AND INVOICEDATE >= '" + this.DtpFrInvDate.Text + "'";
                    str = str + " InvoiceDate From : " + this.DtpFrInvDate.Text + "; ";
                }
                if (this.DtpToInvDate.Checked)
                {
                    this.StrSql = this.StrSql + " AND INVOICEDATE <= '" + this.DtpToInvDate.Text + "'";
                    str = str + " InvoiceDate UpTo : " + this.DtpToInvDate.Text + "; ";
                }
                GlobalFill.FillDataSet(this.StrSql, "STOCKENTRYBILLSSUMMARY", this.DS, this.SDA);
                //ParameterFields pfields = new ParameterFields();
                //ParameterField parameterField = new ParameterField
                //{
                //    Name = "CompanyName"
                //};
                //ParameterDiscreteValue value2 = new ParameterDiscreteValue
                //{
                //    Value = GlobalVariables.StrCompName
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                //parameterField = new ParameterField
                //{
                //    Name = "Parameter"
                //};
                //value2 = new ParameterDiscreteValue
                //{
                //    Value = str
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                if (this.DS.Tables["STOCKENTRYBILLSSUMMARY"].Rows.Count > 0)
                {
                    if (this.cmbPrintOn.Text == "SCREEN")
                    {
                        ///By Bharat we have to change Reports to itextsparp pdf
                        ///
                        RptStockEntryBillsSummary rpt = new RptStockEntryBillsSummary(this.DS.Tables["STOCKENTRYBILLSSUMMARY"]);
                        rpt.Parameter_CompanyName= GlobalVariables.StrCompName;
                        rpt.Parameter_Parameter = str;
                        //rpt.SetDataSource(this.DS.Tables["STOCKENTRYBILLSSUMMARY"]);
                        //rpt.Refresh();
                        //new FrmRptViewer(rpt, pfields).ShowDialog();
                    }
                    else
                    {
                        this.rptname = "STOCK_ENTRY_BILLSUMMARY";
                        this.pfunctions.SETREPORTDATA(ref this.prnrpt, this.rptname, this.DS, this.SDA, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
                        this.pfunctions.SETCOLDATA(ref this.prnItem, this.rptname, this.DS, this.SDA, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
                        this.BillPageHeader(ref this.prnrpt, "");
                        string str2 = "";
                        for (int i = 0; i <= (this.prnItem.Length - 1); i++)
                        {
                            string str3 = "";
                            if (this.prnItem[i].ISGroupHeader)
                            {
                                str3 = (this.prnItem[i].ColumnPosition + 1) + ",";
                            }
                            str2 = str2 + str3;
                        }
                        new ClsDosPrint(this.DS.Tables["STOCKENTRYBILLSSUMMARY"], this.prnItem, this.prnrpt, GlobalVariables.SqlConn, GlobalVariables.PrinterId).PrintData(this.cmbPrintOn.Text, "");
                    }
                }
                else
                {
                    MessageBox.Show("NO DATA FOUND!");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Print_Click");
            }
        }

       

        private void frmRptStockEntryBillsSummary_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.RefreshData();
        }

     

        private void RefreshData()
        {
            GlobalFill.FillCombo("SELECT SUPPLIERID, SUPPLIERNAME FROM RES_SUPPLIER ORDER BY 2", this.cmbSupplier);
            this.DtpFrStockEntryDate.Value = GlobalVariables.BusinessDate;
            this.DtpToStockEntryDate.Value = GlobalVariables.BusinessDate;
            this.cmbPrintOn.Items.Clear();
            this.cmbPrintOn.Items.Add("SCREEN");
            this.cmbPrintOn.Items.Add("PRINTER");
            this.cmbPrintOn.Items.Add("DISK");
            this.cmbPrintOn.SelectedIndex = 0;
        }
    }
}
