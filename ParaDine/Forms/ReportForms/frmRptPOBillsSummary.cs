﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using ParaDine.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmRptPOBillsSummary : Form
    {
        private Button btnExit;
        private Button btnPrint;
        internal CheckBox chkPndgPO;
        private ComboBox cmbPrintOn;
        private ComboBox cmbSupplier;
      //  private IContainer components = null;
        private DataSet DS = new DataSet();
        private DateTimePicker DtpFrDeliveryDate;
        private DateTimePicker DtpFrPODate;
        private DateTimePicker DtpToDeliveryDate;
        private DateTimePicker DtpToPODate;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Panel pnlPO;
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private string StrSql = "";
        private TextBox txtPONo;

        public frmRptPOBillsSummary()
        {
            this.InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                string str = "";
                this.StrSql = "SELECT * FROM RES_VW_POMASTER WHERE PONUMBER  <> ''";
                if (this.txtPONo.Text.Trim() != "")
                {
                    this.StrSql = this.StrSql + " AND PONUMBER = '" + this.txtPONo.Text.Trim() + "'";
                    str = str + "PONo : " + this.txtPONo.Text + "; ";
                }
                if (this.cmbSupplier.Text != "")
                {
                    this.StrSql = this.StrSql + " AND SUPPLIERNAME  = '" + this.cmbSupplier.Text + "'";
                    str = str + " Supplier : " + this.cmbSupplier.Text + "; ";
                }
                if (this.DtpFrPODate.Checked)
                {
                    this.StrSql = this.StrSql + " AND PODATE >= '" + this.DtpFrPODate.Text + "'";
                    str = str + "PODate From : " + this.DtpFrPODate.Text + "; ";
                }
                if (this.DtpToPODate.Checked)
                {
                    this.StrSql = this.StrSql + " AND PODATE <= '" + this.DtpToPODate.Text + "'";
                    str = str + " PODate UpTo : " + this.DtpToPODate.Text + "; ";
                }
                if (this.DtpFrDeliveryDate.Checked)
                {
                    this.StrSql = this.StrSql + " AND DELIVERYDATE >= '" + this.DtpFrDeliveryDate.Text + "'";
                    str = str + " Delivery Date From : " + this.DtpFrDeliveryDate.Text + "; ";
                }
                if (this.DtpToDeliveryDate.Checked)
                {
                    this.StrSql = this.StrSql + " AND DELIVERYDATE <= '" + this.DtpToDeliveryDate.Text + "'";
                    str = str + " Delivery Date UpTo : " + this.DtpToDeliveryDate.Text + "; ";
                }
                if (this.chkPndgPO.Checked)
                {
                    this.StrSql = this.StrSql + " AND PENDING = 1 ";
                    str = "Displaying Only Pending Bills\n" + str;
                }
                GlobalFill.FillDataSet(this.StrSql, "POBILLSSUMMARY", this.DS, this.SDA);
                //ParameterFields pfields = new ParameterFields();
                //ParameterField parameterField = new ParameterField
                //{
                //    Name = "Parameter"
                //};
                //ParameterDiscreteValue value2 = new ParameterDiscreteValue
                //{
                //    Value = str
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                if (this.DS.Tables["POBILLSSUMMARY"].Rows.Count > 0)
                {
                    if (this.cmbPrintOn.Text == "SCREEN")
                    {
                        ///By Bharat we have to change Reports to itextsparp pdf
                        ///
                        RptPOBillsSummary rpt = new RptPOBillsSummary(this.DS.Tables["POBILLSSUMMARY"]);
                        rpt.Parameter_Parameter = str;
                        rpt.ShowDialog();
                        //rpt.SetDataSource(this.DS.Tables["POBILLSSUMMARY"]);
                        //rpt.Refresh();
                        //new FrmRptViewer(rpt, pfields).ShowDialog();
                    }
                }
                else
                {
                    MessageBox.Show("DATA NOT FOUND!");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Print_Click");
            }
        }

    

        private void frmRptPOBillsSummary_Load(object sender, EventArgs e)
        {
            try
            {
                new GlobalTheme().applyTheme(this);
                this.RefreshData();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Loading");
            }
        }

        

        private void RefreshData()
        {
            GlobalFill.FillCombo("SELECT SUPPLIERID, SUPPLIERNAME FROM RES_SUPPLIER ORDER BY 2", this.cmbSupplier);
            this.DtpFrDeliveryDate.Value = GlobalVariables.BusinessDate;
            this.DtpToDeliveryDate.Value = GlobalVariables.BusinessDate;
            this.DtpFrPODate.Value = GlobalVariables.BusinessDate;
            this.DtpToPODate.Value = GlobalVariables.BusinessDate;
            this.cmbPrintOn.Items.Add("SCREEN");
            this.cmbPrintOn.Items.Add("PRINTER");
            this.cmbPrintOn.Items.Add("DISK");
            this.cmbPrintOn.SelectedIndex = 0;
        }
    }
}
