﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using ParaDinePrint;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmRptIndentItemSummary : Form
    {
        private Button btnExit;
        private Button btnPrint;
        private CheckBox chkDispRwMaterial;
        private CheckBox chkUOMNotReq;
        //private IContainer components = null;
        private DataSet DS = new DataSet();
        private DateTimePicker dtpOrderDateFrom;
        private DateTimePicker dtpOrderDateTo;
        internal GroupBox grpCategory;
        private GroupBox grpCondition;
        internal GroupBox grpDepartment;
        private GroupBox grpDisplay;
        private GroupBox grpGroup;
        internal GroupBox grpItems;
        internal GroupBox grpSection;
        internal GroupBox grpStockPoint;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label3;
        internal ListView lstCategory;
        internal ListView lstDepartment;
        internal ListView lstItem;
        internal ListView lstSection;
        internal ListView lstStockpoint;
        private PrintingFunctions pfunctions = new PrintingFunctions();
        private Panel pnlSaleSaummary;
        private ClsColDosPrint[] prnItem;
        private ClsRptDosPrint prnrpt = new ClsRptDosPrint();
        internal RadioButton rdAllCatg;
        internal RadioButton rdAllDept;
        internal RadioButton rdAllItem;
        internal RadioButton rdAllSection;
        internal RadioButton rdAllStockpoint;
        private RadioButton rdDispCategory;
        private RadioButton rdDispCostprice;
        private RadioButton rdDispDepartment;
        private RadioButton rdDispIndentDate;
        private RadioButton rdDispIndentNo;
        private RadioButton rdDispItem;
        private RadioButton rdDispMonth;
        private RadioButton rdDispSection;
        private RadioButton rdDispStockpoint;
        private RadioButton rdDispYear;
        private RadioButton rdGrpCategory;
        private RadioButton rdGrpCostPrice;
        private RadioButton rdGrpDepartment;
        private RadioButton rdGrpIndentDate;
        private RadioButton rdGrpIndentNo;
        private RadioButton rdGrpItem;
        private RadioButton rdGrpMonth;
        private RadioButton rdGrpSection;
        private RadioButton rdGrpStockpoint;
        private RadioButton rdGrpYear;
        internal RadioButton rdSelCatg;
        internal RadioButton rdSelDept;
        internal RadioButton rdSelItem;
        internal RadioButton rdSelSection;
        internal RadioButton rdSelStockpoint;
        private string rptname = "";
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private SqlCommand SqlCmd;
        private string StrSql = "";
        private TextBox txtFrCustOrderNo;
        private TextBox txtToCustOrderNo;

        public frmRptIndentItemSummary()
        {
            this.InitializeComponent();
        }

        private void AddHandlers()
        {
            this.lstDepartment.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstCategory.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstItem.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstSection.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstStockpoint.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.rdAllDept.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllCatg.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllItem.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllSection.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllStockpoint.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                object obj2;
                this.DS.Tables.Clear();
                string str = "";
                this.StrSql = "SELECT ";
                this.StrSql = this.StrSql + this.GetField(this.grpGroup) + " AS CATEGORYNAME,";
                this.StrSql = this.StrSql + this.GetField(this.grpDisplay) + " AS PRODUCT,";
                if (!this.chkUOMNotReq.Checked)
                {
                    if (this.chkDispRwMaterial.Checked)
                    {
                        this.StrSql = this.StrSql + " RUM.UOMNAME, ";
                    }
                    else
                    {
                        this.StrSql = this.StrSql + " U.UOMNAME, ";
                    }
                }
                if (this.chkDispRwMaterial.Checked)
                {
                    this.StrSql = this.StrSql + " SUM(DBO.RES_FUNC_CONVERTQTY(ITG.RAWMATERIALITEMID, ORDERQTY*ISNULL(ITG.QTY, 1), ITG.RAWMATERIALUOMID, RW.UOMID)) AS TOTQTY,   SUM(DBO.RES_FUNC_CONVERTQTY(ITG.RAWMATERIALITEMID, ORDERQTY*ISNULL(ITG.QTY, 1), ITG.RAWMATERIALUOMID, RW.UOMID)*DBO.RES_FUNC_GETCOSTING(ITG.RAWMATERIALITEMID, RUM.UOMID, INM.INDENTDATE)) AS COSTVALUE  ";
                }
                else
                {
                    this.StrSql = this.StrSql + " SUM(ORDERQTY) AS ORDERQTY, ";
                    this.StrSql = this.StrSql + " SUM(ORDERQTY*INT.COSTPRICE) AS COSTVALUE ";
                }
                this.StrSql = this.StrSql + " FROM RES_VW_INDENTTRANS INT   INNER JOIN RES_VW_INDENTMASTER INM ON INM.INDENTMASTERID = INT.INDENTMASTERID ";
                if (this.chkDispRwMaterial.Checked)
                {
                    this.StrSql = this.StrSql + " LEFT JOIN RES_ITEMINGREDIENT ITG ON ITG.ITEMID = INT.ITEMID \tINNER JOIN RES_ITEM ITC ON ITC.ITEMID = INT.ITEMID \tINNER JOIN RES_ITEM IT ON IT.ITEMID = INT.ITEMID \tINNER JOIN RES_UOM U ON U.UOMID = INT.UOMID ";
                }
                else
                {
                    this.StrSql = this.StrSql + " INNER JOIN RES_ITEM IT ON IT.ITEMID = INT.ITEMID  INNER JOIN RES_UOM U ON U.UOMID = INT.UOMID ";
                }
                this.StrSql = this.StrSql + " INNER JOIN RES_SECTION SC ON SC.SECTIONID = IT.SECTIONID  INNER JOIN RES_CATEGORY CG ON CG.CATEGORYID = IT.CATEGORYID  INNER JOIN RES_DEPARTMENT DP ON DP.DEPARTMENTID = CG.DEPARTMENTID ";
                if (this.chkDispRwMaterial.Checked)
                {
                    this.StrSql = this.StrSql + " INNER JOIN RES_ITEM RW ON RW.ITEMID = ITG.RAWMATERIALITEMID  INNER JOIN RES_UOM RUM ON RUM.UOMID = RW.UOMID ";
                }
                this.StrSql = this.StrSql + " WHERE INT.ITEMID <> 0 ";
                if (this.dtpOrderDateFrom.Checked)
                {
                    obj2 = this.StrSql;
                    this.StrSql = string.Concat(new object[] { obj2, " AND INM.INDENTDATE >= '", this.dtpOrderDateFrom.Value, "'" });
                    str = str + " Date:" + this.dtpOrderDateFrom.Text;
                }
                if (this.dtpOrderDateTo.Checked)
                {
                    obj2 = this.StrSql;
                    this.StrSql = string.Concat(new object[] { obj2, " AND INM.INDENTDATE <= '", this.dtpOrderDateTo.Value, "'" });
                    str = str + " To:" + this.dtpOrderDateTo.Text;
                }
                if (this.rdSelDept.Checked && (this.GetCollString(this.lstDepartment) != ""))
                {
                    this.StrSql = this.StrSql + " AND DP.DEPARTMENTID IN (" + this.GetCollString(this.lstDepartment) + ")";
                    str = str + " Depts: " + this.GetRetParam(this.lstDepartment);
                }
                if (this.rdSelCatg.Checked && (this.GetCollString(this.lstCategory) != ""))
                {
                    this.StrSql = this.StrSql + " AND CG.CATEGORYID IN (" + this.GetCollString(this.lstCategory) + ")";
                    str = str + " Catg: " + this.GetRetParam(this.lstCategory);
                }
                if (this.rdSelStockpoint.Checked && (this.GetCollString(this.lstStockpoint) != ""))
                {
                    this.StrSql = this.StrSql + " AND INM.STOCKPOINTID IN (" + this.GetCollString(this.lstStockpoint) + ")";
                    str = str + " StockPoints: " + this.GetRetParam(this.lstStockpoint);
                }
                if (this.rdSelSection.Checked && (this.GetCollString(this.lstSection) != ""))
                {
                    this.StrSql = this.StrSql + " AND SC.SECTIONID IN (" + this.GetCollString(this.lstSection) + ")";
                    str = str + " Sections: " + this.GetRetParam(this.lstSection);
                }
                if (this.rdSelItem.Checked && (this.GetCollString(this.lstItem) != ""))
                {
                    this.StrSql = this.StrSql + " AND IT.ITEMID IN (" + this.GetCollString(this.lstItem) + ")";
                    str = str + " Items: " + this.GetRetParam(this.lstItem);
                }
                if (this.txtFrCustOrderNo.Text != "")
                {
                    this.StrSql = this.StrSql + " AND INM.INDENTMASTERNO >= '" + this.txtFrCustOrderNo.Text + "'";
                    str = str + " IndentNo: " + this.txtFrCustOrderNo.Text;
                }
                if (this.txtToCustOrderNo.Text != "")
                {
                    this.StrSql = this.StrSql + " AND INM.INDENTMASTERNO <= '" + this.txtToCustOrderNo.Text + "'";
                    str = str + " To: " + this.txtToCustOrderNo.Text;
                }
                string strSql = this.StrSql;
                this.StrSql = strSql + " GROUP BY " + this.GetField(this.grpGroup) + "," + this.GetField(this.grpDisplay);
                if (!this.chkUOMNotReq.Checked)
                {
                    if (this.chkDispRwMaterial.Checked)
                    {
                        this.StrSql = this.StrSql + " ,RUM.UOMNAME ";
                    }
                    else
                    {
                        this.StrSql = this.StrSql + " ,U.UOMNAME ";
                    }
                }
                GlobalFill.FillDataSet(this.StrSql, "SUMMARY", this.DS, this.SDA);
                //ParameterFields pfields = new ParameterFields();
                //ParameterField parameterField = new ParameterField
                //{
                //    Name = "CompanyName"
                //};
                //ParameterDiscreteValue value2 = new ParameterDiscreteValue
                //{
                //    Value = GlobalVariables.StrCompName
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                //parameterField = new ParameterField
                //{
                //    Name = "Parameter"
                //};
                //value2 = new ParameterDiscreteValue
                //{
                //    Value = str
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                ///By Bharat we have to change Reports to itextsparp pdf
                ///
                //if (this.chkDispRwMaterial.Checked)
                //{
                //    parameterField = new ParameterField
                //    {
                //        Name = "Address"
                //    };
                //    value2 = new ParameterDiscreteValue
                //    {
                //        Value = ""
                //    };
                //    parameterField.CurrentValues.Add((ParameterValue)value2);
                //    pfields.Add(parameterField);
                //    parameterField = new ParameterField
                //    {
                //        Name = "ReportName"
                //    };
                //    value2 = new ParameterDiscreteValue
                //    {
                //        Value = "Indent Rawmaterial"
                //    };
                //    parameterField.CurrentValues.Add((ParameterValue)value2);
                //    pfields.Add(parameterField);
                //    RptRawMaterialSaleSummary rpt = new RptRawMaterialSaleSummary();
                //    rpt.SetDataSource(this.DS.Tables["SUMMARY"]);
                //    FrmRptViewer viewer = new FrmRptViewer(rpt, pfields);
                //    viewer.ShowDialog();
                //}
                //else
                //{
                //    RptIndentItemSummary summary2 = new RptIndentItemSummary();
                //    summary2.SetDataSource(this.DS.Tables["SUMMARY"]);
                //    new FrmRptViewer(summary2, pfields).ShowDialog();
                //}
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Printing");
            }
        }

      

        private void frmRptIndentItemSummary_Load(object sender, EventArgs e)
        {
            try
            {
                new GlobalTheme().applyTheme(this);
                this.AddHandlers();
                this.RefreshData();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Form Load");
            }
        }

        private string GetCollString(ListView paramLvw)
        {
            try
            {
                string str = "";
                for (int i = 0; i < paramLvw.CheckedItems.Count; i++)
                {
                    if (str != "")
                    {
                        str = str + ", ";
                    }
                    str = str + Convert.ToString(paramLvw.CheckedItems[i].Tag);
                }
                return str;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in function Collection String");
                return "";
            }
        }

        private string GetField(GroupBox paramGrp)
        {
            try
            {
                foreach (RadioButton button in paramGrp.Controls)
                {
                    if (button.Checked)
                    {
                        if (button.Text.ToUpper() == "DEPARTMENT")
                        {
                            return "DP.DEPARTMENTNAME";
                        }
                        if (button.Text.ToUpper() == "CATEGORY")
                        {
                            return "CG.CATEGORYNAME";
                        }
                        if (button.Text.ToUpper() == "STOCKPOINT")
                        {
                            return "INM.STOCKPOINTNAME";
                        }
                        if (button.Text.ToUpper() == "SECTION")
                        {
                            return "SC.SECTIONNAME";
                        }
                        if (button.Text.ToUpper() == "ITEM")
                        {
                            if (this.chkDispRwMaterial.Checked)
                            {
                                return "RW.ITEMNAME";
                            }
                            return "INT.PRODUCT";
                        }
                        if (button.Text.ToUpper() == "COSTPRICE")
                        {
                            return "INT.COSTPRICE";
                        }
                        if (button.Text.ToUpper() == "INDENTNO")
                        {
                            return "INM.INDENTMASTERNO";
                        }
                        if (button.Text.ToUpper() == "INDENTDATE")
                        {
                            return "CONVERT(VARCHAR, INM.INDENTDATE, 102)";
                        }
                        if (button.Text.ToUpper() == "MONTH")
                        {
                            return "'MONTH-' + CONVERT(VARCHAR, MONTH(INM.INDENTDATE)) + '/' + CONVERT(VARCHAR, YEAR(INM.INDENTDATE))";
                        }
                        if (button.Text.ToUpper() == "YEAR")
                        {
                            return "'YEAR-' + CONVERT(VARCHAR, YEAR(INM.INDENTDATE))";
                        }
                    }
                }
                return "";
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in GetFied");
                return "";
            }
        }

        private string GetParamField(GroupBox paramGrp)
        {
            try
            {
                foreach (RadioButton button in paramGrp.Controls)
                {
                    if (button.Checked)
                    {
                        if (button.Text.ToUpper() == "DEPARTMENT")
                        {
                            return "DEPARTMENT";
                        }
                        if (button.Text.ToUpper() == "CATEGORY")
                        {
                            return "CATEGORY";
                        }
                        if (button.Text.ToUpper() == "STOCKPOINT")
                        {
                            return "STOCKPOINT";
                        }
                        if (button.Text.ToUpper() == "SECTION")
                        {
                            return "SECTION";
                        }
                        if (button.Text.ToUpper() == "ITEM")
                        {
                            return "ITEM";
                        }
                        if (button.Text.ToUpper() == "COSTPRICE")
                        {
                            return "COSTPRICE";
                        }
                        if (button.Text.ToUpper() == "INDENTNO")
                        {
                            return "INDENTNO";
                        }
                        if (button.Text.ToUpper() == "INDENTDATE")
                        {
                            return "INDENTDATE";
                        }
                        if (button.Text.ToUpper() == "MONTH")
                        {
                            return "MONTH";
                        }
                        if (button.Text.ToUpper() == "YEAR")
                        {
                            return "YEAR";
                        }
                    }
                }
                return "";
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in GetFied");
                return "";
            }
        }

        private string GetRetParam(ListView ParamLstVw)
        {
            try
            {
                string str = "";
                for (int i = 0; i < ParamLstVw.CheckedItems.Count; i++)
                {
                    if (str != "")
                    {
                        str = str + ", ";
                    }
                    str = str + Convert.ToString(ParamLstVw.CheckedItems[i].Text);
                }
                return str;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in function Collection String");
                return "";
            }
        }

   

        private void lst_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            try
            {
                if (e.Item.Checked)
                {
                    ListView view = (ListView)sender;
                    if (view.CheckedItems.Count > 0)
                    {
                        foreach (Control control in view.Parent.Controls)
                        {
                            if (control.GetType() == typeof(RadioButton))
                            {
                                RadioButton button = (RadioButton)control;
                                if (button.Text.ToUpper().Contains("SELECTED"))
                                {
                                    button.Checked = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in ListViewChecked");
            }
        }

        private void rdAll_checkchanged(object sender, EventArgs e)
        {
            try
            {
                RadioButton button = (RadioButton)sender;
                if (button.Checked)
                {
                    foreach (Control control in button.Parent.Controls)
                    {
                        if (control.GetType() == typeof(ListView))
                        {
                            ListView view = (ListView)control;
                            foreach (ListViewItem item in view.CheckedItems)
                            {
                                item.Checked = false;
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in AllRd");
            }
        }

        private void RefreshData()
        {
            try
            {
                this.StrSql = "SELECT DEPARTMENTID, DEPARTMENTNAME FROM RES_DEPARTMENT";
                GlobalFill.FillListView(this.lstDepartment, GlobalFill.FillDataTable(this.StrSql));
                this.StrSql = "SELECT CATEGORYID, CATEGORYNAME FROM RES_CATEGORY";
                GlobalFill.FillListView(this.lstCategory, GlobalFill.FillDataTable(this.StrSql));
                this.StrSql = "SELECT SECTIONID, SECTIONNAME FROM RES_SECTION";
                GlobalFill.FillListView(this.lstSection, GlobalFill.FillDataTable(this.StrSql));
                this.StrSql = "SELECT ITEMID, ITEMNAME FROM RES_ITEM";
                GlobalFill.FillListView(this.lstItem, GlobalFill.FillDataTable(this.StrSql));
                this.StrSql = "SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT";
                GlobalFill.FillListView(this.lstStockpoint, GlobalFill.FillDataTable(this.StrSql));
                this.dtpOrderDateFrom.Value = new DateTime(GlobalVariables.BusinessDate.Year, GlobalVariables.BusinessDate.Month, GlobalVariables.BusinessDate.Day, 0, 0, 0);
                this.dtpOrderDateTo.Value = GlobalVariables.BusinessDate;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in RefreshData");
            }
        }
    }
}
