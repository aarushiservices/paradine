﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using ParaDine.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmRptIndentBillsSumm : Form
    {
        private Button btnExit;
        private Button btnPrint;
        private ComboBox cmbPrintOn;
        private ComboBox cmbStockPoint;
       // private IContainer components = null;
        private DataSet DS = new DataSet();
        private DateTimePicker DtpFrIndentDate;
        private DateTimePicker DtpToIndentDate;
        private GroupBox groupBox1;
        private Panel grpfields;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label5;
        private Label label7;
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private string StrSql = "";
        private TextBox txtIndentNO;

        public frmRptIndentBillsSumm()
        {
            this.InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                string str = "";
                this.StrSql = " SELECT * FROM RES_VW_INDENTMASTER WHERE INDENTMASTERNO <> ''";
                if (this.txtIndentNO.Text.Trim() != "")
                {
                    this.StrSql = this.StrSql + " AND INDENTMASTERNO = '" + this.txtIndentNO.Text.Trim() + "'";
                    str = str + "IndentNo : " + this.txtIndentNO.Text.Trim() + "; ";
                }
                if (this.DtpFrIndentDate.Checked)
                {
                    this.StrSql = this.StrSql + " AND  INDENTDATE >= '" + this.DtpFrIndentDate.Text + "'";
                    str = str + " IndentDate:" + this.DtpFrIndentDate.Text + "; ";
                }
                if (this.DtpToIndentDate.Checked)
                {
                    this.StrSql = this.StrSql + " AND INDENTDATE <= '" + this.DtpToIndentDate.Text + "'";
                    str = str + " To:" + this.DtpToIndentDate.Text + "; ";
                }
                if (this.cmbStockPoint.Text != "")
                {
                    this.StrSql = this.StrSql + " AND STOCKPOINTID = '" + Convert.ToString(this.cmbStockPoint.SelectedValue) + "'";
                    str = str + " Indent For: " + this.cmbStockPoint.Text + "; ";
                }
                GlobalFill.FillDataSet(this.StrSql, "INDENTBILLSSUMM", this.DS, this.SDA);
                //ParameterFields pfields = new ParameterFields();
                //ParameterField parameterField = new ParameterField
                //{
                //    Name = "CompanyName"
                //};
                //ParameterDiscreteValue value2 = new ParameterDiscreteValue
                //{
                //    Value = GlobalVariables.StrCompName
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                //parameterField = new ParameterField
                //{
                //    Name = "Parameter"
                //};
                //value2 = new ParameterDiscreteValue
                //{
                //    Value = str
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                if (this.DS.Tables["INDENTBILLSSUMM"].Rows.Count > 0)
                {
                    if (this.cmbPrintOn.Text == "SCREEN")
                    {
                        ///By Bharat we have to change Reports to itextsparp pdf
                        ///
                        RptIndentBillsSumm rpt = new RptIndentBillsSumm(this.DS.Tables["INDENTBILLSSUMM"]);
                        rpt.Parameter_CompanyName=GlobalVariables.StrCompName;
                        rpt.Parameter_Parameter = str;
                        rpt.ShowDialog();
                        //rpt.SetDataSource(this.DS.Tables["INDENTBILLSSUMM"]);
                        //rpt.Refresh();
                        //new FrmRptViewer(rpt, pfields).ShowDialog();
                    }
                }
                else
                {
                    MessageBox.Show("DATA NOT FOUND!");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Print_Click");
            }
        }

      

        private void frmRptIndentBillsSumm_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.RefreshData();
        }

    

        private void RefreshData()
        {
            GlobalFill.FillCombo("SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT ORDER BY 2", this.cmbStockPoint);
            this.cmbPrintOn.Items.Clear();
            this.cmbPrintOn.Items.Add("SCREEN");
            this.cmbPrintOn.Items.Add("PRINTER");
            this.cmbPrintOn.Items.Add("DISK");
            this.cmbPrintOn.SelectedIndex = 0;
            this.DtpFrIndentDate.Value = GlobalVariables.BusinessDate;
            this.DtpToIndentDate.Value = GlobalVariables.BusinessDate;
        }
    }
}
