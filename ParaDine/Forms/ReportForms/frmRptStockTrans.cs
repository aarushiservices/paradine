﻿using ParaDine.Forms.GlobalForms;
using ParaDine.Reports;
using ParaDinePrint;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmRptStockTrans : Form
    {
        private Button btnExit;
        private Button btnPrint;
        private ComboBox CMBOUTLET;
        private ComboBox cmbSendTo;
        //private IContainer components;
        private DataSet DS;
        private Panel grpfields;
        private Label label15;
        private Label LBLOUTLET;
        private Label lblType;
        private PrintingFunctions pfunctions;
        private ClsColDosPrint[] prnItem;
        private ClsRptDosPrint prnrpt;
        private string rptname;
        private SqlDataAdapter SDA;
        private SqlCommand SqlCmd;
        private string StrSql;
        public string TransType;
        private ToolTip TTip;
        private TextBox txtType;

        public frmRptStockTrans(string Type)
        {
            this.StrSql = "";
            this.SDA = new SqlDataAdapter();
            this.DS = new DataSet();
            this.SqlCmd = new SqlCommand();
            this.pfunctions = new PrintingFunctions();
            this.prnrpt = new ClsRptDosPrint();
            this.TransType = "";
            this.rptname = "";
            this.components = null;
            this.InitializeComponent();
            this.TransType = Type;
            this.Text = this.TransType;
            this.lblType.Text = this.TransType + " No. :";
        }

        public frmRptStockTrans(long ID, string PONo)
        {
            this.StrSql = "";
            this.SDA = new SqlDataAdapter();
            this.DS = new DataSet();
            this.SqlCmd = new SqlCommand();
            this.pfunctions = new PrintingFunctions();
            this.prnrpt = new ClsRptDosPrint();
            this.TransType = "";
            this.rptname = "";
            this.components = null;
            this.InitializeComponent();
            this.txtType.Tag = ID;
            this.txtType.Text = PONo;
            this.TransType = "PO";
            this.Text = this.TransType;
            this.lblType.Text = this.TransType + " No. :";
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (GlobalValidations.ValidateFields(this.grpfields, this.TTip))
                {
                    FrmRptViewer viewer;
                    if (this.TransType == "PO")
                    {
                        this.StrSql = "SELECT * FROM RES_VW_PURCHASEORDER WHERE PONUMBER = '" + this.txtType.Text + "'";
                        GlobalFill.FillDataSet(this.StrSql, "PURCHASE_ORDER", this.DS, this.SDA);
                        if (this.DS.Tables["PURCHASE_ORDER"].Rows.Count > 0)
                        {
                            if (this.cmbSendTo.Text == "SCREEN")
                            {
                                ///By Bharat we have to change Reports to itextsparp pdf
                                ///
                                RptPurchaseOrder rpt = new RptPurchaseOrder(this.DS.Tables["PURCHASE_ORDER"]);
                                rpt.ShowDialog();
                                //rpt.SetDataSource(this.DS.Tables["PURCHASE_ORDER"]);
                                //rpt.Refresh();
                                //viewer = new FrmRptViewer(rpt);
                                //viewer.Show();
                            }
                            else
                            {
                                this.rptname = "PO";
                                this.RptPrint("PURCHASE_ORDER", this.rptname);
                            }
                        }
                    }
                    else if (this.TransType == "STOCKENTRY")
                    {
                        this.StrSql = "SELECT * FROM RES_VW_STOCKENTRY WHERE STOCKENTRYNO = '" + this.txtType.Text.Trim() + "'";
                        GlobalFill.FillDataSet(this.StrSql, "STOCK_ENTRY", this.DS, this.SDA);
                        if (this.DS.Tables["STOCK_ENTRY"].Rows.Count > 0)
                        {
                            if (this.cmbSendTo.Text == "SCREEN")
                            {
                                ///By Bharat we have to change Reports to itextsparp pdf
                                ///
                                RptStockEntry entry = new RptStockEntry(this.DS.Tables["STOCK_ENTRY"]);
                                entry.Parameter_GrossTotal="";
                                entry.Show();
                                //entry.SetDataSource(this.DS.Tables["STOCK_ENTRY"]);
                                //entry.Refresh();
                                //viewer = new FrmRptViewer(entry);
                                //viewer.Show();
                            }
                            else
                            {
                                this.rptname = "STOCK_ENTRY";
                                this.RptPrint("STOCK_ENTRY", this.rptname);
                            }
                        }
                    }
                    else if (this.TransType == "STOCKRETURN")
                    {
                        this.StrSql = "SELECT * FROM RES_VW_STOCKRETURN WHERE STOCKRETURNNO = '" + this.txtType.Text.Trim().ToUpper() + "'";
                        GlobalFill.FillDataSet(this.StrSql, "STOCK_RETURN", this.DS, this.SDA);
                        if (this.DS.Tables["STOCK_RETURN"].Rows.Count > 0)
                        {
                            if (this.cmbSendTo.Text == "SCREEN")
                            {
                                ///By Bharat we have to change Reports to itextsparp pdf
                                ///
                                //RptStockReturn return2 = new RptStockReturn();
                                //return2.SetDataSource(this.DS.Tables["STOCK_RETURN"]);
                                //return2.Refresh();
                                //new FrmRptViewer(return2).Show();
                            }
                            else
                            {
                                this.rptname = "STOCK_RETURN";
                                this.RptPrint("STOCK_RETURN", this.rptname);
                            }
                        }
                    }
                    else
                    {
                        FrmRptViewer viewer3;
                        if (this.TransType == "INDENT")
                        {
                            this.StrSql = "SELECT * FROM RES_VW_INDENT WHERE INDENTMASTERNO = '" + this.txtType.Text.Trim().ToUpper() + "'";
                            GlobalFill.FillDataSet(this.StrSql, "INDENT", this.DS, this.SDA);
                            if (this.DS.Tables["INDENT"].Rows.Count > 0)
                            {
                                this.StrSql = "SELECT RW.ITEMNAME, RUM.UOMNAME,  SUM(DBO.RES_FUNC_CONVERTQTY(ITG.RAWMATERIALITEMID, ORDERQTY*ISNULL(ITG.QTY, 1), ITG.RAWMATERIALUOMID, RW.UOMID)) AS QTY  FROM RES_VW_INDENTTRANS INT   \tINNER JOIN RES_VW_INDENTMASTER INM ON INM.INDENTMASTERID = INT.INDENTMASTERID  \tINNER JOIN RES_ITEMINGREDIENT ITG ON ITG.ITEMID = INT.ITEMID \t\tINNER JOIN RES_ITEM IT ON IT.ITEMID = INT.ITEMID \t\tINNER JOIN RES_ITEM RW ON RW.ITEMID = ITG.RAWMATERIALITEMID  \tINNER JOIN RES_UOM RUM ON RUM.UOMID = RW.UOMID   WHERE INT.ITEMID <> 0  AND INM.INDENTMASTERNO = '" + this.txtType.Text.Trim().ToUpper() + "' GROUP BY RW.ITEMNAME ,RUM.UOMNAME ";
                                GlobalFill.FillDataSet(this.StrSql, "INDENTRW", this.DS, this.SDA);
                                ///By Bharat we have to change Reports to itextsparp pdf
                                ///
                                RptIndent indent = new RptIndent(this.DS.Tables["INDENT"]);
                                indent.Show();
                                //indent.SetDataSource(this.DS.Tables["INDENT"]);
                                //indent.Subreports[0].SetDataSource(this.DS.Tables["INDENTRW"]);
                                //viewer3 = new FrmRptViewer(indent);
                                //viewer3.Show();
                            }
                        }
                        else if (this.TransType == "STOCKADJUSTMENT")
                        {
                            this.StrSql = "EXEC DBO.RES_PROC_STOCKADJUSTMENT '" + this.CMBOUTLET.Text.ToString().Substring(0, 1) + "','" + this.txtType.Text.Trim().ToUpper() + "'";
                            GlobalFill.FillDataSet(this.StrSql, "STOCKADJUSTMENT", this.DS, this.SDA);
                            if (this.DS.Tables["STOCKADJUSTMENT"].Rows.Count > 0)
                            {
                                if (this.cmbSendTo.Text == "SCREEN")
                                {
                                    ///By Bharat we have to change Reports to itextsparp pdf
                                    ///
                                    //RPTSTOCKADJ rptstockadj = new RPTSTOCKADJ();
                                    //rptstockadj.SetDataSource(this.DS.Tables["STOCKADJUSTMENT"]);
                                    //viewer3 = new FrmRptViewer(rptstockadj);
                                    //viewer3.Show();
                                }
                                else
                                {
                                    this.rptname = "STOCKADJUSTMENT";
                                    this.RptPrint("STOCKADJUSTMENT", this.rptname);
                                }
                            }
                        }
                        else if (this.TransType == "PRODUCTION")
                        {
                            this.StrSql = "SELECT * FROM RES_VW_PRODUCTIONSUMM WHERE PRODUCTIONID=" + Convert.ToInt64(this.txtType.Text.Trim());
                            GlobalFill.FillDataSet(this.StrSql, "PRODUCTIONSUMM", this.DS, this.SDA);
                            if (this.DS.Tables["PRODUCTIONSUMM"].Rows.Count > 0)
                            {
                                if (this.cmbSendTo.Text == "SCREEN")
                                {
                                    ///By Bharat we have to change Reports to itextsparp pdf
                                    ///
                                    RPTPRODUCTIONSUMM rptproductionsumm = new RPTPRODUCTIONSUMM();
                                    //rptproductionsumm.SetDataSource(this.DS.Tables["PRODUCTIONSUMM"]);
                                    //viewer3 = new FrmRptViewer(rptproductionsumm);
                                    //viewer3.Show();
                                }
                                else
                                {
                                    this.rptname = "PRODUCTIONSUMM";
                                    this.RptPrint("PRODUCTIONSUMM", this.rptname);
                                }
                            }
                        }
                        else if (this.TransType == "DELIVERY_ORDER")
                        {
                            this.StrSql = "SELECT * FROM RES_VW_DELIVERYORDERSUMM WHERE ORDERID=" + Convert.ToInt64(this.txtType.Text.Trim());
                            GlobalFill.FillDataSet(this.StrSql, "DELIVERYORDER", this.DS, this.SDA);
                            if (this.DS.Tables["DELIVERYORDER"].Rows.Count > 0)
                            {
                                if (this.cmbSendTo.Text == "SCREEN")
                                {
                                    ///By Bharat we have to change Reports to itextsparp pdf
                                    ///
                                    RptDeliveryOrderSummary summary = new RptDeliveryOrderSummary(this.DS.Tables["DELIVERYORDER"]);
                                    summary.Parameter_netAmt = "";
                                    summary.Show();
                                   // summary.Parameter_netAmt = ;
                                    //summary.SetDataSource(this.DS.Tables["DELIVERYORDER"]);
                                    //new FrmRptViewer(summary).Show();
                                }
                                else
                                {
                                    this.rptname = "DELIVERYORDER";
                                    this.RptPrint("DELIVERYORDER", this.rptname);
                                }
                            }
                        }
                        else if (this.TransType == "WRITEOFF")
                        {
                            this.StrSql = "EXEC DBO.RES_PROC_STOCKWRITEOFFREPORT '" + this.CMBOUTLET.Text.ToString().Substring(0, 1) + "','" + this.txtType.Text.Trim().ToUpper() + "'";
                            GlobalFill.FillDataSet(this.StrSql, "RES_PROC_STOCKWRITEOFFREPORT", this.DS, this.SDA);
                            if (this.DS.Tables["RES_PROC_STOCKWRITEOFFREPORT"].Rows.Count > 0)
                            {
                                if (this.cmbSendTo.Text == "SCREEN")
                                {
                                    ///By Bharat we have to change Reports to itextsparp pdf
                                    ///
                                    RptStockWriteOf of = new RptStockWriteOf(this.DS.Tables["RES_PROC_STOCKWRITEOFFREPORT"]);
                                    of.ShowDialog();
                                    //of.SetDataSource(this.DS.Tables["RES_PROC_STOCKWRITEOFFREPORT"]);
                                    //new FrmRptViewer(of).ShowDialog();
                                }
                                else
                                {
                                    this.rptname = "RES_PROC_STOCKWRITEOFFREPORT";
                                    this.RptPrint("RES_PROC_STOCKWRITEOFFREPORT", this.rptname);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Data Not Found!", "Write Off", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Printing Document");
            }
        }

     

        private void frmRptPurchaseOrder_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            if ((this.Text == "STOCKADJUSTMENT") || (this.Text == "WRITEOFF"))
            {
                this.LBLOUTLET.Visible = true;
                this.CMBOUTLET.Visible = true;
            }
            GlobalFunctions.AddCompHandler(this.grpfields);
            this.RefreshData();
        }

     

        private bool PageHeader(ref ClsRptDosPrint ParamClsRpt, string str)
        {
            try
            {
                ParamClsRpt.set_PageHeader(0, Convert.ToString(new SqlCommand("SELECT BILLHEADER1 FROM RES_BILLSETTINGS", GlobalVariables.SqlConn).ExecuteScalar()));
                ParamClsRpt.set_PageHeader(1, str.Replace("_", " "));
                return true;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Page Header");
                return false;
            }
        }

        private void RefreshData()
        {
            this.cmbSendTo.Items.Clear();
            this.cmbSendTo.Items.Add("SCREEN");
            this.cmbSendTo.Items.Add("PRINTER");
            this.cmbSendTo.Items.Add("DISK");
            this.cmbSendTo.SelectedIndex = 0;
            this.CMBOUTLET.Items.Clear();
            this.CMBOUTLET.Items.Add("WAREHOUSE");
            this.CMBOUTLET.Items.Add("STORES");
            this.CMBOUTLET.Items.Add("KITCHEN");
            this.CMBOUTLET.Items.Add("RESTAURANT");
            this.CMBOUTLET.SelectedIndex = 0;
        }

        public void RptPrint(string dsname, string rptname)
        {
            this.pfunctions.SETREPORTDATA(ref this.prnrpt, rptname, this.DS, this.SDA, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
            this.pfunctions.SETCOLDATA(ref this.prnItem, rptname, this.DS, this.SDA, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
            this.PageHeader(ref this.prnrpt, rptname);
            string str = "";
            for (int i = 0; i <= (this.prnItem.Length - 1); i++)
            {
                string str2 = "";
                if (this.prnItem[i].ISGroupHeader)
                {
                    str2 = (this.prnItem[i].ColumnPosition + 1) + ",";
                }
                str = str + str2;
            }
            if (this.DS.Tables[dsname].Rows.Count > 0)
            {
                new ClsDosPrint(this.DS.Tables[dsname], this.prnItem, this.prnrpt, GlobalVariables.SqlConn, GlobalVariables.PrinterId).PrintData(this.cmbSendTo.Text, "");
            }
        }
    }
}

