﻿//using CrystalDecisions.CrystalReports.Engine;
//using CrystalDecisions.ReportSource;
//using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    //[ToolboxBitmap(typeof(ExportOptions), "report.bmp")]

    public partial class CachedIndentBill : Component
    //public partial class CachedIndentBill : Component, ICachedReport
    {
        //public virtual ReportDocument CreateReport()
        //{
        //    return new IndentBill { Site = this.Site };
        //}

        //public virtual string GetCustomizedCacheKey(RequestContext request)
        //{
        //    return null;
        //}

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        //public virtual TimeSpan CacheTimeOut
        //{
        //    get
        //    {
        //        return CachedReportConstants.DEFAULT_TIMEOUT;
        //    }
        //    set
        //    {
        //    }
        //}

        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public virtual bool IsCacheable
        {
            get
            {
                return true;
            }
            set
            {
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual bool ShareDBLogonInfo
        {
            get
            {
                return false;
            }
            set
            {
            }
        }
    }
}
