﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.ReportForms
{
    partial class frmRptCounterSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkRCardReport = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbPrintOn = new System.Windows.Forms.ComboBox();
            this.lvwPaymentList = new System.Windows.Forms.ListView();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbStockPoint = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpCounterDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbMachineNo = new System.Windows.Forms.ComboBox();
            this.BtnExit = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnGrid = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.chkRCardReport);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.cmbPrintOn);
            this.panel1.Controls.Add(this.lvwPaymentList);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cmbStockPoint);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.dtpCounterDate);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cmbMachineNo);
            this.panel1.Location = new System.Drawing.Point(1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(355, 208);
            this.panel1.TabIndex = 0;
            // 
            // chkRCardReport
            // 
            this.chkRCardReport.AutoSize = true;
            this.chkRCardReport.Location = new System.Drawing.Point(217, 43);
            this.chkRCardReport.Name = "chkRCardReport";
            this.chkRCardReport.Size = new System.Drawing.Size(133, 17);
            this.chkRCardReport.TabIndex = 13;
            this.chkRCardReport.Text = "Recharge Card Report";
            this.chkRCardReport.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(219, 154);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Print On :";
            // 
            // cmbPrintOn
            // 
            this.cmbPrintOn.BackColor = System.Drawing.Color.White;
            this.cmbPrintOn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPrintOn.FormattingEnabled = true;
            this.cmbPrintOn.Location = new System.Drawing.Point(219, 170);
            this.cmbPrintOn.Name = "cmbPrintOn";
            this.cmbPrintOn.Size = new System.Drawing.Size(119, 21);
            this.cmbPrintOn.TabIndex = 11;
            // 
            // lvwPaymentList
            // 
            this.lvwPaymentList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lvwPaymentList.CheckBoxes = true;
            this.lvwPaymentList.Location = new System.Drawing.Point(14, 58);
            this.lvwPaymentList.Name = "lvwPaymentList";
            this.lvwPaymentList.Size = new System.Drawing.Size(190, 137);
            this.lvwPaymentList.TabIndex = 1;
            this.lvwPaymentList.UseCompatibleStateImageBehavior = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Payment Mode :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(43, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "StockPoint";
            // 
            // cmbStockPoint
            // 
            this.cmbStockPoint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStockPoint.FormattingEnabled = true;
            this.cmbStockPoint.Location = new System.Drawing.Point(142, 11);
            this.cmbStockPoint.Name = "cmbStockPoint";
            this.cmbStockPoint.Size = new System.Drawing.Size(167, 21);
            this.cmbStockPoint.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(219, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Counter Date :";
            // 
            // dtpCounterDate
            // 
            this.dtpCounterDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpCounterDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCounterDate.Location = new System.Drawing.Point(219, 81);
            this.dtpCounterDate.Name = "dtpCounterDate";
            this.dtpCounterDate.Size = new System.Drawing.Size(104, 20);
            this.dtpCounterDate.TabIndex = 0;
            this.dtpCounterDate.Value = new System.DateTime(2008, 6, 14, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(219, 109);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Machine No :";
            // 
            // cmbMachineNo
            // 
            this.cmbMachineNo.FormattingEnabled = true;
            this.cmbMachineNo.Location = new System.Drawing.Point(219, 125);
            this.cmbMachineNo.Name = "cmbMachineNo";
            this.cmbMachineNo.Size = new System.Drawing.Size(104, 21);
            this.cmbMachineNo.TabIndex = 3;
            // 
            // BtnExit
            // 
            this.BtnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExit.Location = new System.Drawing.Point(258, 214);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(79, 24);
            this.BtnExit.TabIndex = 2;
            this.BtnExit.Text = "E&xit";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(173, 214);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(79, 24);
            this.btnPrint.TabIndex = 1;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnGrid
            // 
            this.btnGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrid.Location = new System.Drawing.Point(80, 214);
            this.btnGrid.Name = "btnGrid";
            this.btnGrid.Size = new System.Drawing.Size(79, 24);
            this.btnGrid.TabIndex = 3;
            this.btnGrid.Text = "&Grid";
            this.btnGrid.UseVisualStyleBackColor = true;
            this.btnGrid.Click += new System.EventHandler(this.btnGrid_Click);
            // 
            // frmRptCounterSummary
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(357, 246);
            this.Controls.Add(this.btnGrid);
            this.Controls.Add(this.BtnExit);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnPrint);
            this.Name = "frmRptCounterSummary";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Counter Summary";
            this.Load += new System.EventHandler(this.frmRptCounterSummary_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
    }
}