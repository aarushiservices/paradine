﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.ReportForms
{
    partial class frmRptStockTransfer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbfrStockPoint = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.DtpToStockTrfDate = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.DtpFrStockTrfDate = new System.Windows.Forms.DateTimePicker();
            this.cmbToStockPoint = new System.Windows.Forms.ComboBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.grpfields = new System.Windows.Forms.Panel();
            this.cmbPrintOn = new System.Windows.Forms.ComboBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.grpfields.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "To StockPoint";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "From StockPoint";
            // 
            // cmbfrStockPoint
            // 
            this.cmbfrStockPoint.BackColor = System.Drawing.Color.White;
            this.cmbfrStockPoint.FormattingEnabled = true;
            this.cmbfrStockPoint.Location = new System.Drawing.Point(103, 59);
            this.cmbfrStockPoint.Name = "cmbfrStockPoint";
            this.cmbfrStockPoint.Size = new System.Drawing.Size(211, 21);
            this.cmbfrStockPoint.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(179, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "To";
            // 
            // DtpToStockTrfDate
            // 
            this.DtpToStockTrfDate.Checked = false;
            this.DtpToStockTrfDate.CustomFormat = "dd/MMM/yyyy";
            this.DtpToStockTrfDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpToStockTrfDate.Location = new System.Drawing.Point(205, 17);
            this.DtpToStockTrfDate.Name = "DtpToStockTrfDate";
            this.DtpToStockTrfDate.ShowCheckBox = true;
            this.DtpToStockTrfDate.Size = new System.Drawing.Size(106, 20);
            this.DtpToStockTrfDate.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(33, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "From";
            // 
            // DtpFrStockTrfDate
            // 
            this.DtpFrStockTrfDate.Checked = false;
            this.DtpFrStockTrfDate.CustomFormat = "dd/MMM/yyyy";
            this.DtpFrStockTrfDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpFrStockTrfDate.Location = new System.Drawing.Point(67, 17);
            this.DtpFrStockTrfDate.Name = "DtpFrStockTrfDate";
            this.DtpFrStockTrfDate.ShowCheckBox = true;
            this.DtpFrStockTrfDate.Size = new System.Drawing.Size(106, 20);
            this.DtpFrStockTrfDate.TabIndex = 0;
            // 
            // cmbToStockPoint
            // 
            this.cmbToStockPoint.BackColor = System.Drawing.Color.White;
            this.cmbToStockPoint.FormattingEnabled = true;
            this.cmbToStockPoint.Location = new System.Drawing.Point(103, 89);
            this.cmbToStockPoint.Name = "cmbToStockPoint";
            this.cmbToStockPoint.Size = new System.Drawing.Size(211, 21);
            this.cmbToStockPoint.TabIndex = 11;
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(97, 204);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(76, 26);
            this.btnPrint.TabIndex = 10;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(68, 139);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Print On :";
            // 
            // grpfields
            // 
            this.grpfields.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.grpfields.Controls.Add(this.label3);
            this.grpfields.Controls.Add(this.label4);
            this.grpfields.Controls.Add(this.cmbToStockPoint);
            this.grpfields.Controls.Add(this.DtpToStockTrfDate);
            this.grpfields.Controls.Add(this.label5);
            this.grpfields.Controls.Add(this.label7);
            this.grpfields.Controls.Add(this.cmbPrintOn);
            this.grpfields.Controls.Add(this.DtpFrStockTrfDate);
            this.grpfields.Controls.Add(this.label2);
            this.grpfields.Controls.Add(this.cmbfrStockPoint);
            this.grpfields.Location = new System.Drawing.Point(12, 12);
            this.grpfields.Name = "grpfields";
            this.grpfields.Size = new System.Drawing.Size(330, 173);
            this.grpfields.TabIndex = 9;
            // 
            // cmbPrintOn
            // 
            this.cmbPrintOn.BackColor = System.Drawing.Color.White;
            this.cmbPrintOn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPrintOn.FormattingEnabled = true;
            this.cmbPrintOn.Location = new System.Drawing.Point(122, 135);
            this.cmbPrintOn.Name = "cmbPrintOn";
            this.cmbPrintOn.Size = new System.Drawing.Size(126, 21);
            this.cmbPrintOn.TabIndex = 4;
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(185, 204);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 26);
            this.btnExit.TabIndex = 11;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmRptStockTransfer
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(353, 247);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.grpfields);
            this.Controls.Add(this.btnExit);
            this.Name = "frmRptStockTransfer";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stock Transfer";
            this.Load += new System.EventHandler(this.frmRptStockTransfer_Load);
            this.grpfields.ResumeLayout(false);
            this.grpfields.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}