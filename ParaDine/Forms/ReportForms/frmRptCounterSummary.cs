﻿using ParaDine.Forms.GlobalForms;
using ParaDine.Reports;
using ParaDinePrint;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmRptCounterSummary : Form
    {
        private Button BtnExit;
        private Button btnGrid;
        private Button btnPrint;
        private CheckBox chkRCardReport;
        private ComboBox cmbMachineNo;
        private ComboBox cmbPrintOn;
        private ComboBox cmbStockPoint;
       // private IContainer components = null;
        private DataSet DS = new DataSet();
        private DateTimePicker dtpCounterDate;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label7;
        private ListView lvwPaymentList;
        private Panel panel1;
        private PrintingFunctions pfunctions = new PrintingFunctions();
        private ClsColDosPrint[] prnItem;
        private ClsRptDosPrint prnrpt = new ClsRptDosPrint();
        private string rptname = "";
        private SqlDataAdapter Sda = new SqlDataAdapter();
        private string StrSql = "";

        public frmRptCounterSummary()
        {
            this.InitializeComponent();
        }

        private bool BillPageHeader(ref ClsRptDosPrint ParamClsRpt, string ParamWhere)
        {
            try
            {
                return true;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Bill Page Header");
                return false;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnGrid_Click(object sender, EventArgs e)
        {
            string str = "";
            SqlCommand command = new SqlCommand
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = "DBO.RES_PROC_COUNTERSUMMARY",
                Connection = GlobalVariables.SqlConn
            };
            command.Parameters.AddWithValue("@STOCKPOINTID", Convert.ToInt32(this.cmbStockPoint.SelectedValue));
            command.Parameters.AddWithValue("@COUNTERDATE", Convert.ToDateTime(this.dtpCounterDate.Text));
            if (this.cmbMachineNo.Text.Trim() != "")
            {
                command.Parameters.AddWithValue("@MACHINENO", Convert.ToInt32(this.cmbMachineNo.SelectedValue));
            }
            if (this.lvwPaymentList.CheckedItems.Count > 0)
            {
                for (int i = 0; i <= (this.lvwPaymentList.Items.Count - 1); i++)
                {
                    if (this.lvwPaymentList.Items[i].Checked)
                    {
                        if (str.Trim() != "")
                        {
                            str = str + ", ";
                        }
                        str = str + "'" + this.lvwPaymentList.Items[i].Text + "'";
                    }
                }
            }
            this.Sda.SelectCommand = command;
            if (!object.ReferenceEquals(this.DS.Tables["COUNTERSUMMARY"], null))
            {
                this.DS.Tables["COUNTERSUMMARY"].Clear();
            }
            this.Sda.Fill(this.DS, "COUNTERSUMMARY");
            DataTable dt = this.DS.Tables["COUNTERSUMMARY"];
            new frmGridView(dt, "Counter Summary ").ShowDialog();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                int num;
                string str = "";
                SqlCommand command = new SqlCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "DBO.RES_PROC_COUNTERSUMMARY",
                    Connection = GlobalVariables.SqlConn
                };
                command.Parameters.AddWithValue("@STOCKPOINTID", Convert.ToInt32(this.cmbStockPoint.SelectedValue));
                command.Parameters.AddWithValue("@COUNTERDATE", Convert.ToDateTime(this.dtpCounterDate.Text));
                if (this.cmbMachineNo.Text.Trim() != "")
                {
                    command.Parameters.AddWithValue("@MACHINENO", Convert.ToInt32(this.cmbMachineNo.SelectedValue));
                }
                if (this.lvwPaymentList.CheckedItems.Count > 0)
                {
                    for (num = 0; num <= (this.lvwPaymentList.Items.Count - 1); num++)
                    {
                        if (this.lvwPaymentList.Items[num].Checked)
                        {
                            if (str.Trim() != "")
                            {
                                str = str + ", ";
                            }
                            str = str + "'" + this.lvwPaymentList.Items[num].Text + "'";
                        }
                    }
                }
                this.Sda.SelectCommand = command;
                if (!object.ReferenceEquals(this.DS.Tables["COUNTERSUMMARY"], null))
                {
                    this.DS.Tables["COUNTERSUMMARY"].Clear();
                }
                this.Sda.Fill(this.DS, "COUNTERSUMMARY");
                if (this.DS.Tables["COUNTERSUMMARY"].Rows.Count > 0)
                {
                    if (this.chkRCardReport.Checked)
                    {
                        DataTable table = new DataTable("RCARDCOUNTERSUMMARY");
                        table.Columns.Add("MACHINENO", typeof(int));
                        table.Columns.Add("MACHINENAME", typeof(string));
                        table.Columns.Add("COUNTERDATE", typeof(DateTime));
                        table.Columns.Add("PAYMENTMODE", typeof(string));
                        table.Columns.Add("DISCAMT", typeof(double));
                        table.Columns.Add("TAXAMT", typeof(double));
                        table.Columns.Add("AMOUNT", typeof(double));
                        table.Columns.Add("STOCKPOINTNAME", typeof(string));
                        table.Columns.Add("RCARDISSUES", typeof(double));
                        table.Columns.Add("RCARDRECHARGES", typeof(double));
                        table.Columns.Add("RCARDREFUNDS", typeof(double));
                        table.Columns.Add("RCARDTOTAL", typeof(double));
                        foreach (DataRow row in this.DS.Tables["COUNTERSUMMARY"].Rows)
                        {
                            table.Rows.Add(new object[] { Convert.ToString(row["MACHINENO"]), Convert.ToString(row["MACHINENAME"]), Convert.ToDateTime(row["COUNTERDATE"]), Convert.ToString(row["PAYMENTMODE"]), Convert.ToDouble(row["DISCAMT"]), Convert.ToDouble(row["TAXAMT"]), Convert.ToDouble(row["AMOUNT"]), Convert.ToString(row["STOCKPOINTNAME"]), 0, 0, 0, 0 });
                        }
                        this.StrSql = "SELECT CT.TERMINALNO AS MACHINENO, T.TERMINALNAME AS MACHINENAME, CONVERT(DATETIME, CONVERT(VARCHAR, CT.CARDTRANSDATE, 106)) AS COUNTERDATE, SUM(CASE WHEN REFTYPE = 'CI' THEN FACEVALUE+CREDITAMOUNT ELSE 0 END) AS CARDISSUES, \tSUM(CASE WHEN REFTYPE = 'RC' THEN FACEVALUE+CREDITAMOUNT ELSE 0 END) AS CARDRECHARGES,\tSUM(CASE WHEN REFTYPE = 'CR' THEN FACEVALUE+CREDITAMOUNT ELSE 0 END) AS CARDRETURNS, P.PAYMENTMODE  FROM RES_CARDTRANS CT  INNER JOIN GLB_TERMINAL T ON T.TERMINALID = CT.TERMINALNO  OUTER APPLY GLB_PAYMENTMODE P  WHERE P.PAYMENTMODEID = 1  GROUP BY CT.TERMINALNO, CONVERT(DATETIME, CONVERT(VARCHAR, CT.CARDTRANSDATE, 106)), T.TERMINALNAME, P.PAYMENTMODE ";
                        GlobalFill.FillDataSet(this.StrSql, "RCARD", this.DS, this.Sda);
                        foreach (DataRow row in this.DS.Tables["RCARD"].Rows)
                        {
                            if (table.Select("MACHINENO=" + Convert.ToString(row["MACHINENO"])).Length > 0)
                            {
                                foreach (DataRow row2 in table.Select("MACHINENO=" + Convert.ToString(row["MACHINENO"]) + " AND PAYMENTMODE = '" + Convert.ToString(row["PAYMENTMODE"]) + "'"))
                                {
                                    row2["RCARDISSUES"] = row["CARDISSUES"];
                                    row2["RCARDRECHARGES"] = row["CARDRECHARGES"];
                                    row2["RCARDREFUNDS"] = row["CARDRETURNS"];
                                    row2["RCARDTOTAL"] = (Convert.ToDouble(row["CARDISSUES"]) + Convert.ToDouble(row["CARDRECHARGES"])) + Convert.ToDouble(row["CARDRETURNS"]);
                                }
                            }
                            else
                            {
                                table.Rows.Add(new object[] { Convert.ToString(row["MACHINENO"]), Convert.ToString(row["MACHINENAME"]), Convert.ToDateTime(row["COUNTERDATE"]), Convert.ToString(row["PAYMENTMODE"]), 0, 0, 0, "", Convert.ToDouble(row["CARDISSUES"]), Convert.ToDouble(row["CARDRECHARGES"]), Convert.ToDouble(row["CARDRETURNS"]), (Convert.ToDouble(row["CARDISSUES"]) + Convert.ToDouble(row["CARDRECHARGES"])) + Convert.ToDouble(row["CARDRETURNS"]) });
                            }
                        }
                        if (this.DS.Tables.Contains("RCARDCOUNTERSUMMARY"))
                        {
                            this.DS.Tables.Remove("RCARDCOUNTERSUMMARY");
                        }
                        this.DS.Tables.Add(table);
                    }
                    if (this.cmbPrintOn.Text.Trim() == "SCREEN")
                    {
                        ///By Bharat we have to change Reports to itextsparp pdf
                        ///
                        if (this.chkRCardReport.Checked)
                        {
                            RptCounterSummaryRCard rpt = new RptCounterSummaryRCard(this.DS.Tables["RCARDCOUNTERSUMMARY"]);
                            rpt.ShowDialog();
                           // rpt.SetDataSource(this.DS.Tables["RCARDCOUNTERSUMMARY"]);
                           
                           // FrmRptViewer viewer = new FrmRptViewer(rpt);
                           // viewer.ShowDialog();
                        }
                        else
                        {
                            RptCounterSummary summary = new RptCounterSummary(this.DS.Tables["COUNTERSUMMARY"]);
                            //summary.SetDataSource(this.DS.Tables["COUNTERSUMMARY"]);
                            //  new FrmRptViewer(summary).ShowDialog();
                            summary.ShowDialog();

                        }
                    }
                    else
                    {
                        this.rptname = "COUNTER_SUMMARY";
                        this.pfunctions.SETREPORTDATA(ref this.prnrpt, this.rptname, this.DS, this.Sda, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
                        this.pfunctions.SETCOLDATA(ref this.prnItem, this.rptname, this.DS, this.Sda, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
                        this.BillPageHeader(ref this.prnrpt, "");
                        string str2 = "";
                        for (num = 0; num <= (this.prnItem.Length - 1); num++)
                        {
                            string str3 = "";
                            if (this.prnItem[num].ISGroupHeader)
                            {
                                str3 = (this.prnItem[num].ColumnPosition + 1) + ",";
                            }
                            str2 = str2 + str3;
                        }
                        new ClsDosPrint(this.DS.Tables["COUNTERSUMMARY"], this.prnItem, this.prnrpt, GlobalVariables.SqlConn, GlobalVariables.PrinterId).PrintData(this.cmbPrintOn.Text, "");
                    }
                }
                else
                {
                    MessageBox.Show("DATA NOT FOUND");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

       

        private void frmRptCounterSummary_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.RefreshData();
        }

    

        private void RefreshData()
        {
            GlobalFill.FillCombo("SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT WHERE ISBILLPOINT = 1 ORDER BY 2", this.cmbStockPoint);
            GlobalFill.FillCombo("SELECT TERMINALID, TERMINALNAME FROM GLB_TERMINAL ORDER BY 2 ", this.cmbMachineNo);
            this.cmbStockPoint.SelectedValue = GlobalVariables.StockPointId;
            this.StrSql = "SELECT PAYMENTMODEID, PAYMENTMODE FROM GLB_PAYMENTMODE ORDER BY 2";
            GlobalFill.FillDataSet(this.StrSql, "PAYMENTMODE", this.DS, this.Sda);
            GlobalFill.FillListView(this.lvwPaymentList, this.DS.Tables["PAYMENTMODE"]);
            this.dtpCounterDate.Value = GlobalVariables.BusinessDate;
            this.cmbPrintOn.Items.Clear();
            this.cmbPrintOn.Items.Add("SCREEN");
            this.cmbPrintOn.Items.Add("PRINTER");
            this.cmbPrintOn.Items.Add("DISK");
            this.cmbPrintOn.SelectedIndex = 0;
        }
    }
}
