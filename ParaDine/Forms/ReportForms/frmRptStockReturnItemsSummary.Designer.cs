﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.ReportForms
{
    partial class frmRptStockReturnItemsSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExit = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.pnlSalesSummary = new System.Windows.Forms.Panel();
            this.grpDisplay = new System.Windows.Forms.GroupBox();
            this.rdDispStockpoint = new System.Windows.Forms.RadioButton();
            this.rdDispSection = new System.Windows.Forms.RadioButton();
            this.rdDispCostprice = new System.Windows.Forms.RadioButton();
            this.rdDispYear = new System.Windows.Forms.RadioButton();
            this.rdDispMonth = new System.Windows.Forms.RadioButton();
            this.rdDispEntryDate = new System.Windows.Forms.RadioButton();
            this.rdDispEntryNo = new System.Windows.Forms.RadioButton();
            this.rdDispItem = new System.Windows.Forms.RadioButton();
            this.rdDispCategory = new System.Windows.Forms.RadioButton();
            this.rdDispDepartment = new System.Windows.Forms.RadioButton();
            this.grpGroup = new System.Windows.Forms.GroupBox();
            this.rdGrpStockpoint = new System.Windows.Forms.RadioButton();
            this.rdGrpSection = new System.Windows.Forms.RadioButton();
            this.rdGrpCostPrice = new System.Windows.Forms.RadioButton();
            this.rdGrpYear = new System.Windows.Forms.RadioButton();
            this.rdGrpMonth = new System.Windows.Forms.RadioButton();
            this.rdGrpEntryDate = new System.Windows.Forms.RadioButton();
            this.rdGrpreturnNo = new System.Windows.Forms.RadioButton();
            this.rdGrpItem = new System.Windows.Forms.RadioButton();
            this.rdGrpCategory = new System.Windows.Forms.RadioButton();
            this.rdGrpDepartment = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkCPNotReq = new System.Windows.Forms.CheckBox();
            this.chkUOMNotReq = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dtpreturnDateTo = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpRuurnDateFrom = new System.Windows.Forms.DateTimePicker();
            this.txtstockreturntoNO = new System.Windows.Forms.TextBox();
            this.lblto = new System.Windows.Forms.Label();
            this.lblstockentryno = new System.Windows.Forms.Label();
            this.txtstockfromNo = new System.Windows.Forms.TextBox();
            this.gpItem = new System.Windows.Forms.GroupBox();
            this.lstitems = new System.Windows.Forms.ListView();
            this.rdselectedItems = new System.Windows.Forms.RadioButton();
            this.rdAllItems = new System.Windows.Forms.RadioButton();
            this.gpsection = new System.Windows.Forms.GroupBox();
            this.lstsection = new System.Windows.Forms.ListView();
            this.rdselectedsection = new System.Windows.Forms.RadioButton();
            this.rdAllSection = new System.Windows.Forms.RadioButton();
            this.gpsuppilers = new System.Windows.Forms.GroupBox();
            this.lstsuppliers = new System.Windows.Forms.ListView();
            this.rdselectedsupplier = new System.Windows.Forms.RadioButton();
            this.rdallsuppliers = new System.Windows.Forms.RadioButton();
            this.gpstockpoint = new System.Windows.Forms.GroupBox();
            this.lststockpoint = new System.Windows.Forms.ListView();
            this.rdSelectedStockpoint = new System.Windows.Forms.RadioButton();
            this.rdallstockpoint = new System.Windows.Forms.RadioButton();
            this.gpcategory = new System.Windows.Forms.GroupBox();
            this.lstcategory = new System.Windows.Forms.ListView();
            this.rdselectedcategory = new System.Windows.Forms.RadioButton();
            this.rdAllCategory = new System.Windows.Forms.RadioButton();
            this.gpdepartment = new System.Windows.Forms.GroupBox();
            this.lstdepartment = new System.Windows.Forms.ListView();
            this.rdselectedDepartment = new System.Windows.Forms.RadioButton();
            this.rdalldepatment = new System.Windows.Forms.RadioButton();
            this.pnlSalesSummary.SuspendLayout();
            this.grpDisplay.SuspendLayout();
            this.grpGroup.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gpItem.SuspendLayout();
            this.gpsection.SuspendLayout();
            this.gpsuppilers.SuspendLayout();
            this.gpstockpoint.SuspendLayout();
            this.gpcategory.SuspendLayout();
            this.gpdepartment.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(441, 398);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 11;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(344, 398);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 10;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // pnlSalesSummary
            // 
            this.pnlSalesSummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSalesSummary.Controls.Add(this.grpDisplay);
            this.pnlSalesSummary.Controls.Add(this.grpGroup);
            this.pnlSalesSummary.Controls.Add(this.groupBox1);
            this.pnlSalesSummary.Controls.Add(this.gpItem);
            this.pnlSalesSummary.Controls.Add(this.gpsection);
            this.pnlSalesSummary.Controls.Add(this.gpsuppilers);
            this.pnlSalesSummary.Controls.Add(this.gpstockpoint);
            this.pnlSalesSummary.Controls.Add(this.gpcategory);
            this.pnlSalesSummary.Controls.Add(this.gpdepartment);
            this.pnlSalesSummary.Location = new System.Drawing.Point(2, 3);
            this.pnlSalesSummary.Name = "pnlSalesSummary";
            this.pnlSalesSummary.Size = new System.Drawing.Size(873, 387);
            this.pnlSalesSummary.TabIndex = 9;
            // 
            // grpDisplay
            // 
            this.grpDisplay.Controls.Add(this.rdDispStockpoint);
            this.grpDisplay.Controls.Add(this.rdDispSection);
            this.grpDisplay.Controls.Add(this.rdDispCostprice);
            this.grpDisplay.Controls.Add(this.rdDispYear);
            this.grpDisplay.Controls.Add(this.rdDispMonth);
            this.grpDisplay.Controls.Add(this.rdDispEntryDate);
            this.grpDisplay.Controls.Add(this.rdDispEntryNo);
            this.grpDisplay.Controls.Add(this.rdDispItem);
            this.grpDisplay.Controls.Add(this.rdDispCategory);
            this.grpDisplay.Controls.Add(this.rdDispDepartment);
            this.grpDisplay.Location = new System.Drawing.Point(766, 17);
            this.grpDisplay.Name = "grpDisplay";
            this.grpDisplay.Size = new System.Drawing.Size(91, 362);
            this.grpDisplay.TabIndex = 56;
            this.grpDisplay.TabStop = false;
            this.grpDisplay.Text = "Display";
            // 
            // rdDispStockpoint
            // 
            this.rdDispStockpoint.AutoSize = true;
            this.rdDispStockpoint.Location = new System.Drawing.Point(8, 27);
            this.rdDispStockpoint.Name = "rdDispStockpoint";
            this.rdDispStockpoint.Size = new System.Drawing.Size(77, 17);
            this.rdDispStockpoint.TabIndex = 29;
            this.rdDispStockpoint.Text = "StockPoint";
            this.rdDispStockpoint.UseVisualStyleBackColor = true;
            // 
            // rdDispSection
            // 
            this.rdDispSection.AutoSize = true;
            this.rdDispSection.Location = new System.Drawing.Point(8, 102);
            this.rdDispSection.Name = "rdDispSection";
            this.rdDispSection.Size = new System.Drawing.Size(61, 17);
            this.rdDispSection.TabIndex = 28;
            this.rdDispSection.Text = "Section";
            this.rdDispSection.UseVisualStyleBackColor = true;
            // 
            // rdDispCostprice
            // 
            this.rdDispCostprice.AutoSize = true;
            this.rdDispCostprice.Location = new System.Drawing.Point(8, 152);
            this.rdDispCostprice.Name = "rdDispCostprice";
            this.rdDispCostprice.Size = new System.Drawing.Size(70, 17);
            this.rdDispCostprice.TabIndex = 27;
            this.rdDispCostprice.Text = "CostPrice";
            this.rdDispCostprice.UseVisualStyleBackColor = true;
            // 
            // rdDispYear
            // 
            this.rdDispYear.AutoSize = true;
            this.rdDispYear.Location = new System.Drawing.Point(12, 255);
            this.rdDispYear.Name = "rdDispYear";
            this.rdDispYear.Size = new System.Drawing.Size(47, 17);
            this.rdDispYear.TabIndex = 25;
            this.rdDispYear.Text = "Year";
            this.rdDispYear.UseVisualStyleBackColor = true;
            // 
            // rdDispMonth
            // 
            this.rdDispMonth.AutoSize = true;
            this.rdDispMonth.Location = new System.Drawing.Point(12, 230);
            this.rdDispMonth.Name = "rdDispMonth";
            this.rdDispMonth.Size = new System.Drawing.Size(55, 17);
            this.rdDispMonth.TabIndex = 24;
            this.rdDispMonth.Text = "Month";
            this.rdDispMonth.UseVisualStyleBackColor = true;
            // 
            // rdDispEntryDate
            // 
            this.rdDispEntryDate.AutoSize = true;
            this.rdDispEntryDate.Location = new System.Drawing.Point(9, 203);
            this.rdDispEntryDate.Name = "rdDispEntryDate";
            this.rdDispEntryDate.Size = new System.Drawing.Size(80, 17);
            this.rdDispEntryDate.TabIndex = 22;
            this.rdDispEntryDate.Text = "ReturnDate";
            this.rdDispEntryDate.UseVisualStyleBackColor = true;
            // 
            // rdDispEntryNo
            // 
            this.rdDispEntryNo.AutoSize = true;
            this.rdDispEntryNo.Location = new System.Drawing.Point(9, 178);
            this.rdDispEntryNo.Name = "rdDispEntryNo";
            this.rdDispEntryNo.Size = new System.Drawing.Size(71, 17);
            this.rdDispEntryNo.TabIndex = 21;
            this.rdDispEntryNo.Text = "ReturnNo";
            this.rdDispEntryNo.UseVisualStyleBackColor = true;
            // 
            // rdDispItem
            // 
            this.rdDispItem.AutoSize = true;
            this.rdDispItem.Checked = true;
            this.rdDispItem.Location = new System.Drawing.Point(8, 127);
            this.rdDispItem.Name = "rdDispItem";
            this.rdDispItem.Size = new System.Drawing.Size(45, 17);
            this.rdDispItem.TabIndex = 18;
            this.rdDispItem.TabStop = true;
            this.rdDispItem.Text = "Item";
            this.rdDispItem.UseVisualStyleBackColor = true;
            // 
            // rdDispCategory
            // 
            this.rdDispCategory.AutoSize = true;
            this.rdDispCategory.Location = new System.Drawing.Point(8, 77);
            this.rdDispCategory.Name = "rdDispCategory";
            this.rdDispCategory.Size = new System.Drawing.Size(67, 17);
            this.rdDispCategory.TabIndex = 17;
            this.rdDispCategory.Text = "Category";
            this.rdDispCategory.UseVisualStyleBackColor = true;
            // 
            // rdDispDepartment
            // 
            this.rdDispDepartment.AutoSize = true;
            this.rdDispDepartment.Location = new System.Drawing.Point(8, 52);
            this.rdDispDepartment.Name = "rdDispDepartment";
            this.rdDispDepartment.Size = new System.Drawing.Size(80, 17);
            this.rdDispDepartment.TabIndex = 16;
            this.rdDispDepartment.Text = "Department";
            this.rdDispDepartment.UseVisualStyleBackColor = true;
            // 
            // grpGroup
            // 
            this.grpGroup.Controls.Add(this.rdGrpStockpoint);
            this.grpGroup.Controls.Add(this.rdGrpSection);
            this.grpGroup.Controls.Add(this.rdGrpCostPrice);
            this.grpGroup.Controls.Add(this.rdGrpYear);
            this.grpGroup.Controls.Add(this.rdGrpMonth);
            this.grpGroup.Controls.Add(this.rdGrpEntryDate);
            this.grpGroup.Controls.Add(this.rdGrpreturnNo);
            this.grpGroup.Controls.Add(this.rdGrpItem);
            this.grpGroup.Controls.Add(this.rdGrpCategory);
            this.grpGroup.Controls.Add(this.rdGrpDepartment);
            this.grpGroup.Location = new System.Drawing.Point(664, 15);
            this.grpGroup.Name = "grpGroup";
            this.grpGroup.Size = new System.Drawing.Size(91, 362);
            this.grpGroup.TabIndex = 55;
            this.grpGroup.TabStop = false;
            this.grpGroup.Text = "Group";
            // 
            // rdGrpStockpoint
            // 
            this.rdGrpStockpoint.AutoSize = true;
            this.rdGrpStockpoint.Checked = true;
            this.rdGrpStockpoint.Location = new System.Drawing.Point(8, 25);
            this.rdGrpStockpoint.Name = "rdGrpStockpoint";
            this.rdGrpStockpoint.Size = new System.Drawing.Size(77, 17);
            this.rdGrpStockpoint.TabIndex = 14;
            this.rdGrpStockpoint.TabStop = true;
            this.rdGrpStockpoint.Text = "StockPoint";
            this.rdGrpStockpoint.UseVisualStyleBackColor = true;
            // 
            // rdGrpSection
            // 
            this.rdGrpSection.AutoSize = true;
            this.rdGrpSection.Location = new System.Drawing.Point(8, 100);
            this.rdGrpSection.Name = "rdGrpSection";
            this.rdGrpSection.Size = new System.Drawing.Size(61, 17);
            this.rdGrpSection.TabIndex = 13;
            this.rdGrpSection.Text = "Section";
            this.rdGrpSection.UseVisualStyleBackColor = true;
            // 
            // rdGrpCostPrice
            // 
            this.rdGrpCostPrice.AutoSize = true;
            this.rdGrpCostPrice.Location = new System.Drawing.Point(8, 150);
            this.rdGrpCostPrice.Name = "rdGrpCostPrice";
            this.rdGrpCostPrice.Size = new System.Drawing.Size(70, 17);
            this.rdGrpCostPrice.TabIndex = 12;
            this.rdGrpCostPrice.Text = "CostPrice";
            this.rdGrpCostPrice.UseVisualStyleBackColor = true;
            // 
            // rdGrpYear
            // 
            this.rdGrpYear.AutoSize = true;
            this.rdGrpYear.Location = new System.Drawing.Point(11, 257);
            this.rdGrpYear.Name = "rdGrpYear";
            this.rdGrpYear.Size = new System.Drawing.Size(47, 17);
            this.rdGrpYear.TabIndex = 10;
            this.rdGrpYear.Text = "Year";
            this.rdGrpYear.UseVisualStyleBackColor = true;
            // 
            // rdGrpMonth
            // 
            this.rdGrpMonth.AutoSize = true;
            this.rdGrpMonth.Location = new System.Drawing.Point(11, 232);
            this.rdGrpMonth.Name = "rdGrpMonth";
            this.rdGrpMonth.Size = new System.Drawing.Size(55, 17);
            this.rdGrpMonth.TabIndex = 9;
            this.rdGrpMonth.Text = "Month";
            this.rdGrpMonth.UseVisualStyleBackColor = true;
            // 
            // rdGrpEntryDate
            // 
            this.rdGrpEntryDate.AutoSize = true;
            this.rdGrpEntryDate.Location = new System.Drawing.Point(8, 203);
            this.rdGrpEntryDate.Name = "rdGrpEntryDate";
            this.rdGrpEntryDate.Size = new System.Drawing.Size(80, 17);
            this.rdGrpEntryDate.TabIndex = 7;
            this.rdGrpEntryDate.Text = "ReturnDate";
            this.rdGrpEntryDate.UseVisualStyleBackColor = true;
            // 
            // rdGrpreturnNo
            // 
            this.rdGrpreturnNo.AutoSize = true;
            this.rdGrpreturnNo.Location = new System.Drawing.Point(8, 178);
            this.rdGrpreturnNo.Name = "rdGrpreturnNo";
            this.rdGrpreturnNo.Size = new System.Drawing.Size(71, 17);
            this.rdGrpreturnNo.TabIndex = 6;
            this.rdGrpreturnNo.Text = "ReturnNo";
            this.rdGrpreturnNo.UseVisualStyleBackColor = true;
            // 
            // rdGrpItem
            // 
            this.rdGrpItem.AutoSize = true;
            this.rdGrpItem.Location = new System.Drawing.Point(8, 125);
            this.rdGrpItem.Name = "rdGrpItem";
            this.rdGrpItem.Size = new System.Drawing.Size(45, 17);
            this.rdGrpItem.TabIndex = 2;
            this.rdGrpItem.Text = "Item";
            this.rdGrpItem.UseVisualStyleBackColor = true;
            // 
            // rdGrpCategory
            // 
            this.rdGrpCategory.AutoSize = true;
            this.rdGrpCategory.Location = new System.Drawing.Point(8, 75);
            this.rdGrpCategory.Name = "rdGrpCategory";
            this.rdGrpCategory.Size = new System.Drawing.Size(67, 17);
            this.rdGrpCategory.TabIndex = 1;
            this.rdGrpCategory.Text = "Category";
            this.rdGrpCategory.UseVisualStyleBackColor = true;
            // 
            // rdGrpDepartment
            // 
            this.rdGrpDepartment.AutoSize = true;
            this.rdGrpDepartment.Location = new System.Drawing.Point(8, 50);
            this.rdGrpDepartment.Name = "rdGrpDepartment";
            this.rdGrpDepartment.Size = new System.Drawing.Size(80, 17);
            this.rdGrpDepartment.TabIndex = 0;
            this.rdGrpDepartment.Text = "Department";
            this.rdGrpDepartment.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkCPNotReq);
            this.groupBox1.Controls.Add(this.chkUOMNotReq);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.dtpreturnDateTo);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dtpRuurnDateFrom);
            this.groupBox1.Controls.Add(this.txtstockreturntoNO);
            this.groupBox1.Controls.Add(this.lblto);
            this.groupBox1.Controls.Add(this.lblstockentryno);
            this.groupBox1.Controls.Add(this.txtstockfromNo);
            this.groupBox1.Location = new System.Drawing.Point(340, 196);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(308, 177);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // chkCPNotReq
            // 
            this.chkCPNotReq.AutoSize = true;
            this.chkCPNotReq.Location = new System.Drawing.Point(89, 139);
            this.chkCPNotReq.Name = "chkCPNotReq";
            this.chkCPNotReq.Size = new System.Drawing.Size(106, 17);
            this.chkCPNotReq.TabIndex = 41;
            this.chkCPNotReq.Text = "CP Not Required";
            this.chkCPNotReq.UseVisualStyleBackColor = true;
            // 
            // chkUOMNotReq
            // 
            this.chkUOMNotReq.AutoSize = true;
            this.chkUOMNotReq.Location = new System.Drawing.Point(88, 110);
            this.chkUOMNotReq.Name = "chkUOMNotReq";
            this.chkUOMNotReq.Size = new System.Drawing.Size(117, 17);
            this.chkUOMNotReq.TabIndex = 40;
            this.chkUOMNotReq.Text = "UOM Not Required";
            this.chkUOMNotReq.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(174, 69);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(20, 13);
            this.label10.TabIndex = 37;
            this.label10.Text = "To";
            // 
            // dtpreturnDateTo
            // 
            this.dtpreturnDateTo.Checked = false;
            this.dtpreturnDateTo.CustomFormat = "dd/MMM/yyyy";
            this.dtpreturnDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpreturnDateTo.Location = new System.Drawing.Point(197, 65);
            this.dtpreturnDateTo.Name = "dtpreturnDateTo";
            this.dtpreturnDateTo.ShowCheckBox = true;
            this.dtpreturnDateTo.Size = new System.Drawing.Size(104, 20);
            this.dtpreturnDateTo.TabIndex = 33;
            this.dtpreturnDateTo.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 34;
            this.label3.Text = "Stock Return";
            // 
            // dtpRuurnDateFrom
            // 
            this.dtpRuurnDateFrom.Checked = false;
            this.dtpRuurnDateFrom.CustomFormat = "dd/MMM/yyyy";
            this.dtpRuurnDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpRuurnDateFrom.Location = new System.Drawing.Point(67, 65);
            this.dtpRuurnDateFrom.Name = "dtpRuurnDateFrom";
            this.dtpRuurnDateFrom.ShowCheckBox = true;
            this.dtpRuurnDateFrom.Size = new System.Drawing.Size(104, 20);
            this.dtpRuurnDateFrom.TabIndex = 31;
            this.dtpRuurnDateFrom.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // txtstockreturntoNO
            // 
            this.txtstockreturntoNO.Location = new System.Drawing.Point(214, 22);
            this.txtstockreturntoNO.Name = "txtstockreturntoNO";
            this.txtstockreturntoNO.Size = new System.Drawing.Size(76, 20);
            this.txtstockreturntoNO.TabIndex = 3;
            // 
            // lblto
            // 
            this.lblto.AutoSize = true;
            this.lblto.Location = new System.Drawing.Point(188, 27);
            this.lblto.Name = "lblto";
            this.lblto.Size = new System.Drawing.Size(20, 13);
            this.lblto.TabIndex = 2;
            this.lblto.Text = "To";
            // 
            // lblstockentryno
            // 
            this.lblstockentryno.AutoSize = true;
            this.lblstockentryno.Location = new System.Drawing.Point(11, 25);
            this.lblstockentryno.Name = "lblstockentryno";
            this.lblstockentryno.Size = new System.Drawing.Size(87, 13);
            this.lblstockentryno.TabIndex = 1;
            this.lblstockentryno.Text = "Stock Return No";
            // 
            // txtstockfromNo
            // 
            this.txtstockfromNo.Location = new System.Drawing.Point(101, 22);
            this.txtstockfromNo.Name = "txtstockfromNo";
            this.txtstockfromNo.Size = new System.Drawing.Size(76, 20);
            this.txtstockfromNo.TabIndex = 0;
            // 
            // gpItem
            // 
            this.gpItem.Controls.Add(this.lstitems);
            this.gpItem.Controls.Add(this.rdselectedItems);
            this.gpItem.Controls.Add(this.rdAllItems);
            this.gpItem.Location = new System.Drawing.Point(176, 196);
            this.gpItem.Name = "gpItem";
            this.gpItem.Size = new System.Drawing.Size(151, 177);
            this.gpItem.TabIndex = 11;
            this.gpItem.TabStop = false;
            this.gpItem.Text = "Item";
            // 
            // lstitems
            // 
            this.lstitems.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstitems.CheckBoxes = true;
            this.lstitems.Location = new System.Drawing.Point(8, 60);
            this.lstitems.Name = "lstitems";
            this.lstitems.Size = new System.Drawing.Size(137, 107);
            this.lstitems.TabIndex = 8;
            this.lstitems.UseCompatibleStateImageBehavior = false;
            // 
            // rdselectedItems
            // 
            this.rdselectedItems.AutoSize = true;
            this.rdselectedItems.Location = new System.Drawing.Point(8, 41);
            this.rdselectedItems.Name = "rdselectedItems";
            this.rdselectedItems.Size = new System.Drawing.Size(106, 17);
            this.rdselectedItems.TabIndex = 2;
            this.rdselectedItems.Text = "Selected Section";
            this.rdselectedItems.UseVisualStyleBackColor = true;
            // 
            // rdAllItems
            // 
            this.rdAllItems.AutoSize = true;
            this.rdAllItems.Checked = true;
            this.rdAllItems.Location = new System.Drawing.Point(8, 18);
            this.rdAllItems.Name = "rdAllItems";
            this.rdAllItems.Size = new System.Drawing.Size(64, 17);
            this.rdAllItems.TabIndex = 1;
            this.rdAllItems.TabStop = true;
            this.rdAllItems.Text = "All Items";
            this.rdAllItems.UseVisualStyleBackColor = true;
            // 
            // gpsection
            // 
            this.gpsection.Controls.Add(this.lstsection);
            this.gpsection.Controls.Add(this.rdselectedsection);
            this.gpsection.Controls.Add(this.rdAllSection);
            this.gpsection.Location = new System.Drawing.Point(9, 196);
            this.gpsection.Name = "gpsection";
            this.gpsection.Size = new System.Drawing.Size(151, 177);
            this.gpsection.TabIndex = 10;
            this.gpsection.TabStop = false;
            this.gpsection.Text = "Section";
            // 
            // lstsection
            // 
            this.lstsection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstsection.CheckBoxes = true;
            this.lstsection.Location = new System.Drawing.Point(8, 60);
            this.lstsection.Name = "lstsection";
            this.lstsection.Size = new System.Drawing.Size(137, 107);
            this.lstsection.TabIndex = 8;
            this.lstsection.UseCompatibleStateImageBehavior = false;
            // 
            // rdselectedsection
            // 
            this.rdselectedsection.AutoSize = true;
            this.rdselectedsection.Location = new System.Drawing.Point(8, 41);
            this.rdselectedsection.Name = "rdselectedsection";
            this.rdselectedsection.Size = new System.Drawing.Size(106, 17);
            this.rdselectedsection.TabIndex = 2;
            this.rdselectedsection.Text = "Selected Section";
            this.rdselectedsection.UseVisualStyleBackColor = true;
            // 
            // rdAllSection
            // 
            this.rdAllSection.AutoSize = true;
            this.rdAllSection.Checked = true;
            this.rdAllSection.Location = new System.Drawing.Point(8, 18);
            this.rdAllSection.Name = "rdAllSection";
            this.rdAllSection.Size = new System.Drawing.Size(83, 17);
            this.rdAllSection.TabIndex = 1;
            this.rdAllSection.TabStop = true;
            this.rdAllSection.Text = "All Selection";
            this.rdAllSection.UseVisualStyleBackColor = true;
            // 
            // gpsuppilers
            // 
            this.gpsuppilers.Controls.Add(this.lstsuppliers);
            this.gpsuppilers.Controls.Add(this.rdselectedsupplier);
            this.gpsuppilers.Controls.Add(this.rdallsuppliers);
            this.gpsuppilers.Location = new System.Drawing.Point(497, 12);
            this.gpsuppilers.Name = "gpsuppilers";
            this.gpsuppilers.Size = new System.Drawing.Size(151, 177);
            this.gpsuppilers.TabIndex = 9;
            this.gpsuppilers.TabStop = false;
            this.gpsuppilers.Text = "Suppliers";
            // 
            // lstsuppliers
            // 
            this.lstsuppliers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstsuppliers.CheckBoxes = true;
            this.lstsuppliers.Location = new System.Drawing.Point(8, 60);
            this.lstsuppliers.Name = "lstsuppliers";
            this.lstsuppliers.Size = new System.Drawing.Size(137, 107);
            this.lstsuppliers.TabIndex = 8;
            this.lstsuppliers.UseCompatibleStateImageBehavior = false;
            // 
            // rdselectedsupplier
            // 
            this.rdselectedsupplier.AutoSize = true;
            this.rdselectedsupplier.Location = new System.Drawing.Point(8, 41);
            this.rdselectedsupplier.Name = "rdselectedsupplier";
            this.rdselectedsupplier.Size = new System.Drawing.Size(106, 17);
            this.rdselectedsupplier.TabIndex = 2;
            this.rdselectedsupplier.Text = "Selected supplier";
            this.rdselectedsupplier.UseVisualStyleBackColor = true;
            // 
            // rdallsuppliers
            // 
            this.rdallsuppliers.AutoSize = true;
            this.rdallsuppliers.Checked = true;
            this.rdallsuppliers.Location = new System.Drawing.Point(8, 18);
            this.rdallsuppliers.Name = "rdallsuppliers";
            this.rdallsuppliers.Size = new System.Drawing.Size(82, 17);
            this.rdallsuppliers.TabIndex = 1;
            this.rdallsuppliers.TabStop = true;
            this.rdallsuppliers.Text = "All Suppliers";
            this.rdallsuppliers.UseVisualStyleBackColor = true;
            // 
            // gpstockpoint
            // 
            this.gpstockpoint.Controls.Add(this.lststockpoint);
            this.gpstockpoint.Controls.Add(this.rdSelectedStockpoint);
            this.gpstockpoint.Controls.Add(this.rdallstockpoint);
            this.gpstockpoint.Location = new System.Drawing.Point(335, 11);
            this.gpstockpoint.Name = "gpstockpoint";
            this.gpstockpoint.Size = new System.Drawing.Size(151, 177);
            this.gpstockpoint.TabIndex = 4;
            this.gpstockpoint.TabStop = false;
            this.gpstockpoint.Text = "Stock Point";
            // 
            // lststockpoint
            // 
            this.lststockpoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lststockpoint.CheckBoxes = true;
            this.lststockpoint.Location = new System.Drawing.Point(8, 60);
            this.lststockpoint.Name = "lststockpoint";
            this.lststockpoint.Size = new System.Drawing.Size(137, 107);
            this.lststockpoint.TabIndex = 8;
            this.lststockpoint.UseCompatibleStateImageBehavior = false;
            // 
            // rdSelectedStockpoint
            // 
            this.rdSelectedStockpoint.AutoSize = true;
            this.rdSelectedStockpoint.Location = new System.Drawing.Point(8, 41);
            this.rdSelectedStockpoint.Name = "rdSelectedStockpoint";
            this.rdSelectedStockpoint.Size = new System.Drawing.Size(120, 17);
            this.rdSelectedStockpoint.TabIndex = 2;
            this.rdSelectedStockpoint.Text = "Selected stockPoint";
            this.rdSelectedStockpoint.UseVisualStyleBackColor = true;
            // 
            // rdallstockpoint
            // 
            this.rdallstockpoint.AutoSize = true;
            this.rdallstockpoint.Checked = true;
            this.rdallstockpoint.Location = new System.Drawing.Point(8, 18);
            this.rdallstockpoint.Name = "rdallstockpoint";
            this.rdallstockpoint.Size = new System.Drawing.Size(91, 17);
            this.rdallstockpoint.TabIndex = 1;
            this.rdallstockpoint.TabStop = true;
            this.rdallstockpoint.Text = "All StockPoint";
            this.rdallstockpoint.UseVisualStyleBackColor = true;
            // 
            // gpcategory
            // 
            this.gpcategory.Controls.Add(this.lstcategory);
            this.gpcategory.Controls.Add(this.rdselectedcategory);
            this.gpcategory.Controls.Add(this.rdAllCategory);
            this.gpcategory.Location = new System.Drawing.Point(174, 10);
            this.gpcategory.Name = "gpcategory";
            this.gpcategory.Size = new System.Drawing.Size(151, 177);
            this.gpcategory.TabIndex = 3;
            this.gpcategory.TabStop = false;
            this.gpcategory.Text = "Category";
            // 
            // lstcategory
            // 
            this.lstcategory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstcategory.CheckBoxes = true;
            this.lstcategory.Location = new System.Drawing.Point(9, 61);
            this.lstcategory.Name = "lstcategory";
            this.lstcategory.Size = new System.Drawing.Size(137, 107);
            this.lstcategory.TabIndex = 7;
            this.lstcategory.UseCompatibleStateImageBehavior = false;
            // 
            // rdselectedcategory
            // 
            this.rdselectedcategory.AutoSize = true;
            this.rdselectedcategory.Location = new System.Drawing.Point(8, 41);
            this.rdselectedcategory.Name = "rdselectedcategory";
            this.rdselectedcategory.Size = new System.Drawing.Size(112, 17);
            this.rdselectedcategory.TabIndex = 2;
            this.rdselectedcategory.Text = "Selected Category";
            this.rdselectedcategory.UseVisualStyleBackColor = true;
            // 
            // rdAllCategory
            // 
            this.rdAllCategory.AutoSize = true;
            this.rdAllCategory.Checked = true;
            this.rdAllCategory.Location = new System.Drawing.Point(8, 18);
            this.rdAllCategory.Name = "rdAllCategory";
            this.rdAllCategory.Size = new System.Drawing.Size(81, 17);
            this.rdAllCategory.TabIndex = 1;
            this.rdAllCategory.TabStop = true;
            this.rdAllCategory.Text = "All Category";
            this.rdAllCategory.UseVisualStyleBackColor = true;
            // 
            // gpdepartment
            // 
            this.gpdepartment.Controls.Add(this.lstdepartment);
            this.gpdepartment.Controls.Add(this.rdselectedDepartment);
            this.gpdepartment.Controls.Add(this.rdalldepatment);
            this.gpdepartment.Location = new System.Drawing.Point(7, 9);
            this.gpdepartment.Name = "gpdepartment";
            this.gpdepartment.Size = new System.Drawing.Size(160, 177);
            this.gpdepartment.TabIndex = 0;
            this.gpdepartment.TabStop = false;
            this.gpdepartment.Text = "Department";
            // 
            // lstdepartment
            // 
            this.lstdepartment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstdepartment.CheckBoxes = true;
            this.lstdepartment.Location = new System.Drawing.Point(11, 58);
            this.lstdepartment.Name = "lstdepartment";
            this.lstdepartment.Size = new System.Drawing.Size(137, 110);
            this.lstdepartment.TabIndex = 6;
            this.lstdepartment.UseCompatibleStateImageBehavior = false;
            // 
            // rdselectedDepartment
            // 
            this.rdselectedDepartment.AutoSize = true;
            this.rdselectedDepartment.Location = new System.Drawing.Point(8, 38);
            this.rdselectedDepartment.Name = "rdselectedDepartment";
            this.rdselectedDepartment.Size = new System.Drawing.Size(125, 17);
            this.rdselectedDepartment.TabIndex = 2;
            this.rdselectedDepartment.Text = "Selected Department";
            this.rdselectedDepartment.UseVisualStyleBackColor = true;
            // 
            // rdalldepatment
            // 
            this.rdalldepatment.AutoSize = true;
            this.rdalldepatment.Checked = true;
            this.rdalldepatment.Location = new System.Drawing.Point(8, 18);
            this.rdalldepatment.Name = "rdalldepatment";
            this.rdalldepatment.Size = new System.Drawing.Size(94, 17);
            this.rdalldepatment.TabIndex = 1;
            this.rdalldepatment.TabStop = true;
            this.rdalldepatment.Text = "All Department";
            this.rdalldepatment.UseVisualStyleBackColor = true;
            // 
            // frmRptStockReturnItemsSummary
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(891, 432);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.pnlSalesSummary);
            this.Name = "frmRptStockReturnItemsSummary";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stock Return Items Summary";
            this.Load += new System.EventHandler(this.frmRptStockReturnItemsSummary_Load);
            this.pnlSalesSummary.ResumeLayout(false);
            this.grpDisplay.ResumeLayout(false);
            this.grpDisplay.PerformLayout();
            this.grpGroup.ResumeLayout(false);
            this.grpGroup.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gpItem.ResumeLayout(false);
            this.gpItem.PerformLayout();
            this.gpsection.ResumeLayout(false);
            this.gpsection.PerformLayout();
            this.gpsuppilers.ResumeLayout(false);
            this.gpsuppilers.PerformLayout();
            this.gpstockpoint.ResumeLayout(false);
            this.gpstockpoint.PerformLayout();
            this.gpcategory.ResumeLayout(false);
            this.gpcategory.PerformLayout();
            this.gpdepartment.ResumeLayout(false);
            this.gpdepartment.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}