﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.ReportForms
{
    partial class FrmRptDeliveryOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PnlMain = new System.Windows.Forms.Panel();
            this.chkorderdelivered = new System.Windows.Forms.CheckBox();
            this.ChkCancelled = new System.Windows.Forms.CheckBox();
            this.CHKPendingOrd = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbSendTo = new System.Windows.Forms.ComboBox();
            this.dtpDeliveryDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpOrderDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbCustomer = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.PnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // PnlMain
            // 
            this.PnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlMain.Controls.Add(this.chkorderdelivered);
            this.PnlMain.Controls.Add(this.ChkCancelled);
            this.PnlMain.Controls.Add(this.CHKPendingOrd);
            this.PnlMain.Controls.Add(this.label15);
            this.PnlMain.Controls.Add(this.cmbSendTo);
            this.PnlMain.Controls.Add(this.dtpDeliveryDate);
            this.PnlMain.Controls.Add(this.label3);
            this.PnlMain.Controls.Add(this.dtpOrderDate);
            this.PnlMain.Controls.Add(this.label2);
            this.PnlMain.Controls.Add(this.cmbCustomer);
            this.PnlMain.Controls.Add(this.label1);
            this.PnlMain.Location = new System.Drawing.Point(8, 6);
            this.PnlMain.Name = "PnlMain";
            this.PnlMain.Size = new System.Drawing.Size(319, 172);
            this.PnlMain.TabIndex = 0;
            // 
            // chkorderdelivered
            // 
            this.chkorderdelivered.AutoSize = true;
            this.chkorderdelivered.Location = new System.Drawing.Point(101, 143);
            this.chkorderdelivered.Name = "chkorderdelivered";
            this.chkorderdelivered.Size = new System.Drawing.Size(100, 17);
            this.chkorderdelivered.TabIndex = 66;
            this.chkorderdelivered.Text = "Order Delivered";
            this.chkorderdelivered.UseVisualStyleBackColor = true;
            // 
            // ChkCancelled
            // 
            this.ChkCancelled.AutoSize = true;
            this.ChkCancelled.Location = new System.Drawing.Point(185, 122);
            this.ChkCancelled.Name = "ChkCancelled";
            this.ChkCancelled.Size = new System.Drawing.Size(107, 17);
            this.ChkCancelled.TabIndex = 65;
            this.ChkCancelled.Text = "Cancelled Orders";
            this.ChkCancelled.UseVisualStyleBackColor = true;
            // 
            // CHKPendingOrd
            // 
            this.CHKPendingOrd.AutoSize = true;
            this.CHKPendingOrd.Checked = true;
            this.CHKPendingOrd.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CHKPendingOrd.Location = new System.Drawing.Point(52, 122);
            this.CHKPendingOrd.Name = "CHKPendingOrd";
            this.CHKPendingOrd.Size = new System.Drawing.Size(99, 17);
            this.CHKPendingOrd.TabIndex = 64;
            this.CHKPendingOrd.Text = "Pending Orders";
            this.CHKPendingOrd.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(42, 94);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 13);
            this.label15.TabIndex = 63;
            this.label15.Text = "Send To :";
            // 
            // cmbSendTo
            // 
            this.cmbSendTo.FormattingEnabled = true;
            this.cmbSendTo.Location = new System.Drawing.Point(99, 90);
            this.cmbSendTo.Name = "cmbSendTo";
            this.cmbSendTo.Size = new System.Drawing.Size(159, 21);
            this.cmbSendTo.TabIndex = 62;
            // 
            // dtpDeliveryDate
            // 
            this.dtpDeliveryDate.Checked = false;
            this.dtpDeliveryDate.CustomFormat = "dd/MMM/yy";
            this.dtpDeliveryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDeliveryDate.Location = new System.Drawing.Point(99, 63);
            this.dtpDeliveryDate.Name = "dtpDeliveryDate";
            this.dtpDeliveryDate.ShowCheckBox = true;
            this.dtpDeliveryDate.Size = new System.Drawing.Size(107, 20);
            this.dtpDeliveryDate.TabIndex = 59;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 61;
            this.label3.Text = "Delivery Date :";
            // 
            // dtpOrderDate
            // 
            this.dtpOrderDate.Checked = false;
            this.dtpOrderDate.CustomFormat = "dd/MMM/yy";
            this.dtpOrderDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOrderDate.Location = new System.Drawing.Point(99, 36);
            this.dtpOrderDate.Name = "dtpOrderDate";
            this.dtpOrderDate.ShowCheckBox = true;
            this.dtpOrderDate.Size = new System.Drawing.Size(107, 20);
            this.dtpOrderDate.TabIndex = 58;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 60;
            this.label2.Text = "Order Date :";
            // 
            // cmbCustomer
            // 
            this.cmbCustomer.FormattingEnabled = true;
            this.cmbCustomer.Location = new System.Drawing.Point(99, 8);
            this.cmbCustomer.Name = "cmbCustomer";
            this.cmbCustomer.Size = new System.Drawing.Size(209, 21);
            this.cmbCustomer.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Customer Name :";
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(74, 187);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(78, 28);
            this.btnPrint.TabIndex = 1;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(183, 187);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(78, 28);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // FrmRptDeliveryOrder
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(334, 222);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.PnlMain);
            this.Name = "FrmRptDeliveryOrder";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Delivery Order";
            this.Load += new System.EventHandler(this.FrmRptPendingDeliveryOrder_Load);
            this.PnlMain.ResumeLayout(false);
            this.PnlMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}