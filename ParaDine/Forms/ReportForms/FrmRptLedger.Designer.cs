﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.ReportForms
{
    partial class FrmRptLedger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnExit = new System.Windows.Forms.Button();
            this.BtnPrint = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblSource = new System.Windows.Forms.Label();
            this.grpCheckIn = new System.Windows.Forms.GroupBox();
            this.DtpTo = new System.Windows.Forms.DateTimePicker();
            this.DtpFrom = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.CmbSource = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.grpCheckIn.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnExit
            // 
            this.BtnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExit.Location = new System.Drawing.Point(175, 153);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(77, 28);
            this.BtnExit.TabIndex = 6;
            this.BtnExit.Text = "E&xit";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Location = new System.Drawing.Point(70, 153);
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(77, 28);
            this.BtnPrint.TabIndex = 5;
            this.BtnPrint.Text = "&Print";
            this.BtnPrint.UseVisualStyleBackColor = true;
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblSource);
            this.panel1.Controls.Add(this.grpCheckIn);
            this.panel1.Controls.Add(this.CmbSource);
            this.panel1.Location = new System.Drawing.Point(10, 9);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(302, 133);
            this.panel1.TabIndex = 4;
            // 
            // lblSource
            // 
            this.lblSource.AutoSize = true;
            this.lblSource.Location = new System.Drawing.Point(25, 19);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(78, 13);
            this.lblSource.TabIndex = 10;
            this.lblSource.Text = "Source Name :";
            // 
            // grpCheckIn
            // 
            this.grpCheckIn.Controls.Add(this.DtpTo);
            this.grpCheckIn.Controls.Add(this.DtpFrom);
            this.grpCheckIn.Controls.Add(this.label4);
            this.grpCheckIn.Controls.Add(this.label7);
            this.grpCheckIn.Location = new System.Drawing.Point(9, 49);
            this.grpCheckIn.Name = "grpCheckIn";
            this.grpCheckIn.Size = new System.Drawing.Size(282, 69);
            this.grpCheckIn.TabIndex = 21;
            this.grpCheckIn.TabStop = false;
            // 
            // DtpTo
            // 
            this.DtpTo.Checked = false;
            this.DtpTo.CustomFormat = "dd/MMM/yyyy";
            this.DtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpTo.Location = new System.Drawing.Point(166, 33);
            this.DtpTo.Name = "DtpTo";
            this.DtpTo.Size = new System.Drawing.Size(101, 20);
            this.DtpTo.TabIndex = 1;
            // 
            // DtpFrom
            // 
            this.DtpFrom.Checked = false;
            this.DtpFrom.CustomFormat = "dd/MMM/yyyy";
            this.DtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpFrom.Location = new System.Drawing.Point(13, 32);
            this.DtpFrom.Name = "DtpFrom";
            this.DtpFrom.Size = new System.Drawing.Size(101, 20);
            this.DtpFrom.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "From :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(163, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "To :";
            // 
            // CmbSource
            // 
            this.CmbSource.FormattingEnabled = true;
            this.CmbSource.Location = new System.Drawing.Point(117, 16);
            this.CmbSource.Name = "CmbSource";
            this.CmbSource.Size = new System.Drawing.Size(158, 21);
            this.CmbSource.TabIndex = 0;
            // 
            // FrmRptLedger
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(323, 190);
            this.Controls.Add(this.BtnExit);
            this.Controls.Add(this.BtnPrint);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmRptLedger";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ledger";
            this.Load += new System.EventHandler(this.FrmRptCustomerLedger_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.grpCheckIn.ResumeLayout(false);
            this.grpCheckIn.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
    }
}