﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using ParaDine.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmRptStockWriteOffBillsSumm : Form
    {
        private Button btnExit;
        private Button btnPrint;
        private ComboBox cmbOutletType;
      //private IContainer components = null;
        private DateTimePicker DtpFrDate;
        private DateTimePicker DtpToDate;
        private Label label2;
        private Label label3;
        private Label label5;
        private Panel panel1;
        private string StrSql = "";

        public frmRptStockWriteOffBillsSumm()
        {
            this.InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                string str = "";
                this.StrSql = "SELECT * FROM RES_VW_STOCKWRITEOFF WHERE WRITEOFFID <> 0 ";
                if (Convert.ToInt32(this.cmbOutletType.SelectedValue) > 0)
                {
                    this.StrSql = this.StrSql + " AND STOCKPOINTID = " + this.cmbOutletType.SelectedValue.ToString();
                    str = str + " STOCKPOINT : " + this.cmbOutletType.Text + ",";
                }
                if (this.DtpFrDate.Checked)
                {
                    this.StrSql = this.StrSql + " AND CONVERT(DATETIME, CONVERT(VARCHAR, WRITEOFFDATE, 112)) >= '" + this.DtpFrDate.Text + "'";
                    str = str + " FROM:" + this.DtpFrDate.Text + ",";
                }
                if (this.DtpToDate.Checked)
                {
                    this.StrSql = this.StrSql + " AND CONVERT(DATETIME, CONVERT(VARCHAR, WRITEOFFDATE, 112)) <= '" + this.DtpToDate.Text + "'";
                    str = str + " TO:" + this.DtpToDate.Text + ",";
                }
                SqlDataAdapter adapter = new SqlDataAdapter(this.StrSql, GlobalVariables.SqlConn);
                DataSet dataSet = new DataSet();
                if (dataSet.Tables.Contains("STOCKWRITEOFFBILLSSUMM"))
                {
                    dataSet.Tables.Remove("STOCKWRITEOFFBILLSSUMM");
                }
                adapter.Fill(dataSet, "STOCKWRITEOFFBILLSSUMM");
                if (dataSet.Tables["STOCKWRITEOFFBILLSSUMM"].Rows.Count > 0)
                {
                    //ParameterFields pfields = new ParameterFields();
                    //ParameterField parameterField = new ParameterField();
                    //ParameterDiscreteValue value2 = new ParameterDiscreteValue();
                    //parameterField.Name = "PARAMETER";
                    //value2.Value = str;
                    //parameterField.CurrentValues.Add((ParameterValue)value2);
                    //pfields.Add(parameterField);
                    ///By Bharat we have to change Reports to itextsparp pdf
                    ///
                    RptStockWriteOffBillsSumm rpt = new RptStockWriteOffBillsSumm(dataSet.Tables["STOCKWRITEOFFBILLSSUMM"]);
                    rpt.Parameter_parameter = str;
                    rpt.ShowDialog();
                    //rpt.SetDataSource(dataSet.Tables["STOCKWRITEOFFBILLSSUMM"]);
                    //rpt.Refresh();
                    //new FrmRptViewer(rpt, pfields).ShowDialog();
                }
                else
                {
                    MessageBox.Show("DATA NOT FOUND!", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Print Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

   

        private void frmRptStockWriteOffBillsSumm_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.RefreshData();
            this.DtpFrDate.Checked = false;
            this.DtpToDate.Checked = false;
        }

     

        private void RefreshData()
        {
            this.DtpFrDate.Value = GlobalVariables.BusinessDate;
            this.DtpToDate.Value = GlobalVariables.BusinessDate;
            this.StrSql = "SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT";
            GlobalFill.FillCombo(this.StrSql, this.cmbOutletType);
            this.cmbOutletType.SelectedIndex = 0;
        }
    }
}
