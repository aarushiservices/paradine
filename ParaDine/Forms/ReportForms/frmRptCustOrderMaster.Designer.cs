﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.ReportForms
{
    partial class frmRptCustOrderMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpfields = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbPrintOn = new System.Windows.Forms.ComboBox();
            this.chkRefund = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbMachineNo = new System.Windows.Forms.ComboBox();
            this.cmbCustomer = new System.Windows.Forms.ComboBox();
            this.cmbSteward = new System.Windows.Forms.ComboBox();
            this.lvwPaymentList = new System.Windows.Forms.ListView();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCustOrderNoTo = new System.Windows.Forms.TextBox();
            this.txtCustOrderNoFrom = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dtpOrderDateTo = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpOrderDateFrom = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbOrderType = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbSourceName = new System.Windows.Forms.ComboBox();
            this.chkCancelled = new System.Windows.Forms.CheckBox();
            this.chkPendingBills = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbStockPoint = new System.Windows.Forms.ComboBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.BtnExit = new System.Windows.Forms.Button();
            this.btnGrid = new System.Windows.Forms.Button();
            this.grpfields.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpfields
            // 
            this.grpfields.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.grpfields.Controls.Add(this.label2);
            this.grpfields.Controls.Add(this.cmbPrintOn);
            this.grpfields.Controls.Add(this.chkRefund);
            this.grpfields.Controls.Add(this.label7);
            this.grpfields.Controls.Add(this.cmbMachineNo);
            this.grpfields.Controls.Add(this.cmbCustomer);
            this.grpfields.Controls.Add(this.cmbSteward);
            this.grpfields.Controls.Add(this.lvwPaymentList);
            this.grpfields.Controls.Add(this.label4);
            this.grpfields.Controls.Add(this.label1);
            this.grpfields.Controls.Add(this.label5);
            this.grpfields.Controls.Add(this.groupBox2);
            this.grpfields.Controls.Add(this.groupBox1);
            this.grpfields.Controls.Add(this.label9);
            this.grpfields.Controls.Add(this.cmbOrderType);
            this.grpfields.Controls.Add(this.label8);
            this.grpfields.Controls.Add(this.cmbSourceName);
            this.grpfields.Controls.Add(this.chkCancelled);
            this.grpfields.Controls.Add(this.chkPendingBills);
            this.grpfields.Controls.Add(this.label6);
            this.grpfields.Controls.Add(this.cmbStockPoint);
            this.grpfields.Location = new System.Drawing.Point(0, 0);
            this.grpfields.Name = "grpfields";
            this.grpfields.Size = new System.Drawing.Size(342, 395);
            this.grpfields.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 366);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Print On :";
            // 
            // cmbPrintOn
            // 
            this.cmbPrintOn.BackColor = System.Drawing.Color.White;
            this.cmbPrintOn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPrintOn.FormattingEnabled = true;
            this.cmbPrintOn.Location = new System.Drawing.Point(134, 363);
            this.cmbPrintOn.Name = "cmbPrintOn";
            this.cmbPrintOn.Size = new System.Drawing.Size(119, 21);
            this.cmbPrintOn.TabIndex = 24;
            // 
            // chkRefund
            // 
            this.chkRefund.AutoSize = true;
            this.chkRefund.Location = new System.Drawing.Point(211, 335);
            this.chkRefund.Name = "chkRefund";
            this.chkRefund.Size = new System.Drawing.Size(82, 17);
            this.chkRefund.TabIndex = 23;
            this.chkRefund.Text = "Refund Bills";
            this.chkRefund.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(173, 128);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Machine No :";
            // 
            // cmbMachineNo
            // 
            this.cmbMachineNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMachineNo.FormattingEnabled = true;
            this.cmbMachineNo.Location = new System.Drawing.Point(173, 144);
            this.cmbMachineNo.Name = "cmbMachineNo";
            this.cmbMachineNo.Size = new System.Drawing.Size(140, 21);
            this.cmbMachineNo.TabIndex = 21;
            // 
            // cmbCustomer
            // 
            this.cmbCustomer.FormattingEnabled = true;
            this.cmbCustomer.Location = new System.Drawing.Point(173, 304);
            this.cmbCustomer.Name = "cmbCustomer";
            this.cmbCustomer.Size = new System.Drawing.Size(140, 21);
            this.cmbCustomer.TabIndex = 8;
            // 
            // cmbSteward
            // 
            this.cmbSteward.FormattingEnabled = true;
            this.cmbSteward.Location = new System.Drawing.Point(19, 304);
            this.cmbSteward.Name = "cmbSteward";
            this.cmbSteward.Size = new System.Drawing.Size(140, 21);
            this.cmbSteward.TabIndex = 7;
            // 
            // lvwPaymentList
            // 
            this.lvwPaymentList.CheckBoxes = true;
            this.lvwPaymentList.Location = new System.Drawing.Point(19, 144);
            this.lvwPaymentList.Name = "lvwPaymentList";
            this.lvwPaymentList.Size = new System.Drawing.Size(144, 141);
            this.lvwPaymentList.TabIndex = 3;
            this.lvwPaymentList.UseCompatibleStateImageBehavior = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 288);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Steward :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Payment Mode :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(173, 290);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Customer :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.txtCustOrderNoTo);
            this.groupBox2.Controls.Add(this.txtCustOrderNoFrom);
            this.groupBox2.Location = new System.Drawing.Point(6, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(328, 43);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Customer Order No :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(157, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "To :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 21);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "from :";
            // 
            // txtCustOrderNoTo
            // 
            this.txtCustOrderNoTo.Location = new System.Drawing.Point(185, 17);
            this.txtCustOrderNoTo.Name = "txtCustOrderNoTo";
            this.txtCustOrderNoTo.Size = new System.Drawing.Size(104, 20);
            this.txtCustOrderNoTo.TabIndex = 1;
            // 
            // txtCustOrderNoFrom
            // 
            this.txtCustOrderNoFrom.Location = new System.Drawing.Point(51, 17);
            this.txtCustOrderNoFrom.Name = "txtCustOrderNoFrom";
            this.txtCustOrderNoFrom.Size = new System.Drawing.Size(104, 20);
            this.txtCustOrderNoFrom.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.dtpOrderDateTo);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dtpOrderDateFrom);
            this.groupBox1.Location = new System.Drawing.Point(6, 50);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(328, 73);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Order Date :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(158, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "To :";
            // 
            // dtpOrderDateTo
            // 
            this.dtpOrderDateTo.Checked = false;
            this.dtpOrderDateTo.CustomFormat = "dd/MMM/yyyy hh:mm tt";
            this.dtpOrderDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOrderDateTo.Location = new System.Drawing.Point(167, 37);
            this.dtpOrderDateTo.Name = "dtpOrderDateTo";
            this.dtpOrderDateTo.ShowCheckBox = true;
            this.dtpOrderDateTo.Size = new System.Drawing.Size(155, 20);
            this.dtpOrderDateTo.TabIndex = 1;
            this.dtpOrderDateTo.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "from :";
            // 
            // dtpOrderDateFrom
            // 
            this.dtpOrderDateFrom.Checked = false;
            this.dtpOrderDateFrom.CustomFormat = "dd/MMM/yyyy hh:mm tt";
            this.dtpOrderDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOrderDateFrom.Location = new System.Drawing.Point(6, 37);
            this.dtpOrderDateFrom.Name = "dtpOrderDateFrom";
            this.dtpOrderDateFrom.ShowCheckBox = true;
            this.dtpOrderDateFrom.Size = new System.Drawing.Size(155, 20);
            this.dtpOrderDateFrom.TabIndex = 0;
            this.dtpOrderDateFrom.Value = new System.DateTime(2008, 6, 12, 0, 0, 0, 0);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(173, 249);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Order Type :";
            // 
            // cmbOrderType
            // 
            this.cmbOrderType.FormattingEnabled = true;
            this.cmbOrderType.Location = new System.Drawing.Point(173, 263);
            this.cmbOrderType.Name = "cmbOrderType";
            this.cmbOrderType.Size = new System.Drawing.Size(140, 21);
            this.cmbOrderType.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(173, 210);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Source Name :";
            // 
            // cmbSourceName
            // 
            this.cmbSourceName.FormattingEnabled = true;
            this.cmbSourceName.Location = new System.Drawing.Point(173, 225);
            this.cmbSourceName.Name = "cmbSourceName";
            this.cmbSourceName.Size = new System.Drawing.Size(140, 21);
            this.cmbSourceName.TabIndex = 5;
            // 
            // chkCancelled
            // 
            this.chkCancelled.AutoSize = true;
            this.chkCancelled.Location = new System.Drawing.Point(134, 335);
            this.chkCancelled.Name = "chkCancelled";
            this.chkCancelled.Size = new System.Drawing.Size(73, 17);
            this.chkCancelled.TabIndex = 10;
            this.chkCancelled.Text = "Cancelled";
            this.chkCancelled.UseVisualStyleBackColor = true;
            // 
            // chkPendingBills
            // 
            this.chkPendingBills.AutoSize = true;
            this.chkPendingBills.Location = new System.Drawing.Point(44, 335);
            this.chkPendingBills.Name = "chkPendingBills";
            this.chkPendingBills.Size = new System.Drawing.Size(86, 17);
            this.chkPendingBills.TabIndex = 9;
            this.chkPendingBills.Text = "Pending Bills";
            this.chkPendingBills.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(173, 171);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "StockPoint";
            // 
            // cmbStockPoint
            // 
            this.cmbStockPoint.FormattingEnabled = true;
            this.cmbStockPoint.Location = new System.Drawing.Point(173, 187);
            this.cmbStockPoint.Name = "cmbStockPoint";
            this.cmbStockPoint.Size = new System.Drawing.Size(140, 21);
            this.cmbStockPoint.TabIndex = 4;
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(132, 401);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(79, 24);
            this.btnPrint.TabIndex = 1;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // BtnExit
            // 
            this.BtnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExit.Location = new System.Drawing.Point(235, 401);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(79, 24);
            this.BtnExit.TabIndex = 2;
            this.BtnExit.Text = "E&xit";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // btnGrid
            // 
            this.btnGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrid.Location = new System.Drawing.Point(29, 401);
            this.btnGrid.Name = "btnGrid";
            this.btnGrid.Size = new System.Drawing.Size(79, 24);
            this.btnGrid.TabIndex = 3;
            this.btnGrid.Text = "&Grid";
            this.btnGrid.UseVisualStyleBackColor = true;
            this.btnGrid.Click += new System.EventHandler(this.btnGrid_Click);
            // 
            // frmRptCustOrderMaster
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(343, 435);
            this.Controls.Add(this.btnGrid);
            this.Controls.Add(this.BtnExit);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.grpfields);
            this.Name = "frmRptCustOrderMaster";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customer Order Master";
            this.Load += new System.EventHandler(this.frmRptCustOrderMaster_Load);
            this.grpfields.ResumeLayout(false);
            this.grpfields.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}