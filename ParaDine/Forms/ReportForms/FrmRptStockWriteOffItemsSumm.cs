﻿
using ParaDine.Forms.GlobalForms;
using ParaDine.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class FrmRptStockWriteOffItemsSumm : Form
    {
        private Button btnExit;
        private Button btnPrint;
        private CheckBox chkCPNotReq;
        private CheckBox chkUOMNotReq;
       // private IContainer components = null;
        private DataSet DS = new DataSet();
        private DateTimePicker dtpFrDate;
        private DateTimePicker dtpToDate;
        private GroupBox groupBox1;
        internal GroupBox grpCategory;
        internal GroupBox grpDepartment;
        private GroupBox grpDisplay;
        private GroupBox grpGroup;
        internal GroupBox grpItems;
        internal GroupBox grpSection;
        internal GroupBox grpStockPoint;
        private Label label10;
        private Label label3;
        internal ListView lstCategory;
        internal ListView lstDepartment;
        internal ListView lstItem;
        internal ListView lstSection;
        internal ListView lstStockpoint;
        private Panel panel1;
        internal RadioButton rdAllCatg;
        internal RadioButton rdAllDept;
        internal RadioButton rdAllItem;
        internal RadioButton rdAllSection;
        internal RadioButton rdAllStockpoint;
        private RadioButton rdDispCategory;
        private RadioButton rdDispCostprice;
        private RadioButton rdDispDepartment;
        private RadioButton rdDispItem;
        private RadioButton rdDispMonth;
        private RadioButton rdDispSection;
        private RadioButton rdDispStockpoint;
        private RadioButton rdDispWriteoffDate;
        private RadioButton rdDispYear;
        private RadioButton rdGrpCategory;
        private RadioButton rdGrpCostPrice;
        private RadioButton rdGrpDepartment;
        private RadioButton rdGrpItem;
        private RadioButton rdGrpMonth;
        private RadioButton rdGrpSection;
        private RadioButton rdGrpStockpoint;
        private RadioButton rdGrpWriteoffDate;
        private RadioButton rdGrpYear;
        internal RadioButton rdSelCatg;
        internal RadioButton rdSelDept;
        internal RadioButton rdSelItem;
        internal RadioButton rdSelSection;
        internal RadioButton rdSelStockpoint;
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private SqlCommand SqlCmd;
        private string StrSql = "";

        public FrmRptStockWriteOffItemsSumm()
        {
            this.InitializeComponent();
        }

        private void AddHandlers()
        {
            this.lstDepartment.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstCategory.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstItem.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstSection.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.lstStockpoint.ItemChecked += new ItemCheckedEventHandler(this.lst_ItemChecked);
            this.rdAllDept.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllCatg.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllItem.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllSection.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
            this.rdAllStockpoint.CheckedChanged += new EventHandler(this.rdAll_checkchanged);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                object obj2;
                this.DS.Tables.Clear();
                string str = "";
                this.StrSql = "SELECT ";
                this.StrSql = this.StrSql + this.GetField(this.grpGroup) + " AS CATEGORYNAME,";
                this.StrSql = this.StrSql + this.GetField(this.grpDisplay) + " AS ITEMNAME,";
                if (!this.chkUOMNotReq.Checked)
                {
                    this.StrSql = this.StrSql + " U.UOMNAME, ";
                }
                if (!this.chkCPNotReq.Checked)
                {
                    this.StrSql = this.StrSql + " WT.PRICE, ";
                }
                this.StrSql = this.StrSql + " SUM(QTY) AS QTY, ";
                this.StrSql = this.StrSql + " SUM(QTY*WT.PRICE) AS AMOUNT ";
                this.StrSql = this.StrSql + " FROM RES_VW_STOCKWRITEOFFTRANS WT   INNER JOIN RES_VW_STOCKWRITEOFF WM ON WM.WRITEOFFID = WT.WRITEOFFID  INNER JOIN RES_STOCKPOINT SP ON SP.STOCKPOINTID = WM.STOCKPOINTID ";
                this.StrSql = this.StrSql + " INNER JOIN RES_ITEM IT ON IT.ITEMID = WT.ITEMID  INNER JOIN RES_UOM U ON U.UOMID = WT.UOMID ";
                this.StrSql = this.StrSql + " INNER JOIN RES_SECTION SC ON SC.SECTIONID = IT.SECTIONID  INNER JOIN RES_CATEGORY CG ON CG.CATEGORYID = IT.CATEGORYID  INNER JOIN RES_DEPARTMENT DP ON DP.DEPARTMENTID = CG.DEPARTMENTID ";
                this.StrSql = this.StrSql + " WHERE WT.ITEMID <> 0 ";
                if (this.dtpFrDate.Checked)
                {
                    obj2 = this.StrSql;
                    this.StrSql = string.Concat(new object[] { obj2, " AND WM.WRITEOFFDATE >= '", this.dtpFrDate.Value, "'" });
                    str = str + " Date:" + this.dtpFrDate.Text;
                }
                if (this.dtpToDate.Checked)
                {
                    obj2 = this.StrSql;
                    this.StrSql = string.Concat(new object[] { obj2, " AND WM.WRITEOFFDATE <= '", this.dtpToDate.Value, "'" });
                    str = str + " To:" + this.dtpToDate.Text;
                }
                if (this.rdSelDept.Checked && (this.GetCollString(this.lstDepartment) != ""))
                {
                    this.StrSql = this.StrSql + " AND DP.DEPARTMENTID IN (" + this.GetCollString(this.lstDepartment) + ")";
                    str = str + " Depts: " + this.GetRetParam(this.lstDepartment);
                }
                if (this.rdSelCatg.Checked && (this.GetCollString(this.lstCategory) != ""))
                {
                    this.StrSql = this.StrSql + " AND CG.CATEGORYID IN (" + this.GetCollString(this.lstCategory) + ")";
                    str = str + " Catg: " + this.GetRetParam(this.lstCategory);
                }
                if (this.rdSelStockpoint.Checked && (this.GetCollString(this.lstStockpoint) != ""))
                {
                    this.StrSql = this.StrSql + " AND WM.STOCKPOINTID IN (" + this.GetCollString(this.lstStockpoint) + ")";
                    str = str + " StockPoints: " + this.GetRetParam(this.lstStockpoint);
                }
                if (this.rdSelSection.Checked && (this.GetCollString(this.lstSection) != ""))
                {
                    this.StrSql = this.StrSql + " AND SC.SECTIONID IN (" + this.GetCollString(this.lstSection) + ")";
                    str = str + " Sections: " + this.GetRetParam(this.lstSection);
                }
                if (this.rdSelItem.Checked && (this.GetCollString(this.lstItem) != ""))
                {
                    this.StrSql = this.StrSql + " AND IT.ITEMID IN (" + this.GetCollString(this.lstItem) + ")";
                    str = str + " Items: " + this.GetRetParam(this.lstItem);
                }
                string strSql = this.StrSql;
                this.StrSql = strSql + " GROUP BY " + this.GetField(this.grpGroup) + "," + this.GetField(this.grpDisplay);
                if (!this.chkUOMNotReq.Checked)
                {
                    this.StrSql = this.StrSql + " ,U.UOMNAME ";
                }
                if (!this.chkCPNotReq.Checked)
                {
                    this.StrSql = this.StrSql + " ,WT.PRICE ";
                }
                GlobalFill.FillDataSet(this.StrSql, "STOCKWRITEOFFITEMSSUMM", this.DS, this.SDA);
                //ParameterFields pfields = new ParameterFields();
                //ParameterField parameterField = new ParameterField
                //{
                //    Name = "Parameter"
                //};
                //ParameterDiscreteValue value2 = new ParameterDiscreteValue
                //{
                //    Value = str
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                if (this.DS.Tables["STOCKWRITEOFFITEMSSUMM"].Rows.Count > 0)
                {
                    ///By Bharat we have to change Reports to itextsparp pdf
                    ///
                    RptStockWriteOffItemsSumm rpt = new RptStockWriteOffItemsSumm(this.DS.Tables["STOCKWRITEOFFITEMSSUMM"]);
                    rpt.Parameter_parameter = str;
                    rpt.ShowDialog();
                    //rpt.SetDataSource(this.DS.Tables["STOCKWRITEOFFITEMSSUMM"]);
                    //rpt.Refresh();
                    //new FrmRptViewer(rpt, pfields).ShowDialog();
                }
                else
                {
                    MessageBox.Show("DATA NOT FOUND!");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

       

        private void FrmRptStockWriteOffItemsSumm_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.AddHandlers();
            this.RefreshData();
        }

        private string GetCollString(ListView paramLvw)
        {
            try
            {
                string str = "";
                for (int i = 0; i < paramLvw.CheckedItems.Count; i++)
                {
                    if (str != "")
                    {
                        str = str + ", ";
                    }
                    str = str + Convert.ToString(paramLvw.CheckedItems[i].Tag);
                }
                return str;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in function Collection String");
                return "";
            }
        }

        private string GetField(GroupBox paramGrp)
        {
            try
            {
                foreach (RadioButton button in paramGrp.Controls)
                {
                    if (button.Checked)
                    {
                        if (button.Text.ToUpper() == "DEPARTMENT")
                        {
                            return "DP.DEPARTMENTNAME";
                        }
                        if (button.Text.ToUpper() == "CATEGORY")
                        {
                            return "CG.CATEGORYNAME";
                        }
                        if (button.Text.ToUpper() == "STOCKPOINT")
                        {
                            return "WM.STOCKPOINTNAME";
                        }
                        if (button.Text.ToUpper() == "SECTION")
                        {
                            return "SC.SECTIONNAME";
                        }
                        if (button.Text.ToUpper() == "ITEM")
                        {
                            return "WT.ITEMNAME";
                        }
                        if (button.Text.ToUpper() == "COSTPRICE")
                        {
                            return "WT.PRICE";
                        }
                        if (button.Text.ToUpper() == "WRITEOFFDATE")
                        {
                            return "CONVERT(VARCHAR, WM.WRITEOFFDATE, 102)";
                        }
                        if (button.Text.ToUpper() == "MONTH")
                        {
                            return "'MONTH-' + CONVERT(VARCHAR, MONTH(WM.WRITEOFFDATE)) + '/' + CONVERT(VARCHAR, YEAR(WM.WRITEOFFDATE))";
                        }
                        if (button.Text.ToUpper() == "YEAR")
                        {
                            return "'YEAR-' + CONVERT(VARCHAR, YEAR(WM.WRITEOFFDATE))";
                        }
                    }
                }
                return "";
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in GetFied");
                return "";
            }
        }

        private string GetParamField(GroupBox paramGrp)
        {
            try
            {
                foreach (RadioButton button in paramGrp.Controls)
                {
                    if (button.Checked)
                    {
                        if (button.Text.ToUpper() == "DEPARTMENT")
                        {
                            return "DEPARTMENT";
                        }
                        if (button.Text.ToUpper() == "CATEGORY")
                        {
                            return "CATEGORY";
                        }
                        if (button.Text.ToUpper() == "STOCKPOINT")
                        {
                            return "STOCKPOINT";
                        }
                        if (button.Text.ToUpper() == "SECTION")
                        {
                            return "SECTION";
                        }
                        if (button.Text.ToUpper() == "ITEM")
                        {
                            return "ITEM";
                        }
                        if (button.Text.ToUpper() == "COSTPRICE")
                        {
                            return "COSTPRICE";
                        }
                        if (button.Text.ToUpper() == "WRITEOFFDATE")
                        {
                            return "WRITEOFFDATE";
                        }
                        if (button.Text.ToUpper() == "MONTH")
                        {
                            return "MONTH";
                        }
                        if (button.Text.ToUpper() == "YEAR")
                        {
                            return "YEAR";
                        }
                    }
                }
                return "";
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in GetFied");
                return "";
            }
        }

        private string GetRetParam(ListView ParamLstVw)
        {
            try
            {
                string str = "";
                for (int i = 0; i < ParamLstVw.CheckedItems.Count; i++)
                {
                    if (str != "")
                    {
                        str = str + ", ";
                    }
                    str = str + Convert.ToString(ParamLstVw.CheckedItems[i].Text);
                }
                return str;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in function Collection String");
                return "";
            }
        }

    

        private void lst_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            try
            {
                if (e.Item.Checked)
                {
                    ListView view = (ListView)sender;
                    if (view.CheckedItems.Count > 0)
                    {
                        foreach (Control control in view.Parent.Controls)
                        {
                            if (control.GetType() == typeof(RadioButton))
                            {
                                RadioButton button = (RadioButton)control;
                                if (button.Text.ToUpper().Contains("SELECTED"))
                                {
                                    button.Checked = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in ListViewChecked");
            }
        }

        private void rdAll_checkchanged(object sender, EventArgs e)
        {
            try
            {
                RadioButton button = (RadioButton)sender;
                if (button.Checked)
                {
                    foreach (Control control in button.Parent.Controls)
                    {
                        if (control.GetType() == typeof(ListView))
                        {
                            ListView view = (ListView)control;
                            foreach (ListViewItem item in view.CheckedItems)
                            {
                                item.Checked = false;
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in AllRd");
            }
        }

        private void RefreshData()
        {
            try
            {
                this.StrSql = "SELECT DEPARTMENTID, DEPARTMENTNAME FROM RES_DEPARTMENT";
                GlobalFill.FillListView(this.lstDepartment, GlobalFill.FillDataTable(this.StrSql));
                this.StrSql = "SELECT CATEGORYID, CATEGORYNAME FROM RES_CATEGORY";
                GlobalFill.FillListView(this.lstCategory, GlobalFill.FillDataTable(this.StrSql));
                this.StrSql = "SELECT SECTIONID, SECTIONNAME FROM RES_SECTION";
                GlobalFill.FillListView(this.lstSection, GlobalFill.FillDataTable(this.StrSql));
                this.StrSql = "SELECT ITEMID, ITEMNAME FROM RES_ITEM";
                GlobalFill.FillListView(this.lstItem, GlobalFill.FillDataTable(this.StrSql));
                this.StrSql = "SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT";
                GlobalFill.FillListView(this.lstStockpoint, GlobalFill.FillDataTable(this.StrSql));
                this.dtpFrDate.Value = new DateTime(GlobalVariables.BusinessDate.Year, GlobalVariables.BusinessDate.Month, GlobalVariables.BusinessDate.Day, 0, 0, 0);
                this.dtpToDate.Value = GlobalVariables.BusinessDate;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in RefreshData");
            }
        }
    }
}
