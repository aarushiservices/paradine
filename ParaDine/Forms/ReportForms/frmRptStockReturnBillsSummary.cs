﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using ParaDine.Reports;
using ParaDinePrint;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmRptStockReturnBillsSummary : Form
    {
        private Button btnExit;
        private Button btnPrint;
        private ComboBox cmbPrintOn;
        private ComboBox cmbSupplier;
        //private IContainer components = null;
        private DataSet DS = new DataSet();
        private DateTimePicker DtpFrReturnBillDate;
        private DateTimePicker DtpFrStkreturnDate;
        private DateTimePicker DtpToReturnBillDate;
        private DateTimePicker DtpToStkreturnDate;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private PrintingFunctions pfunctions = new PrintingFunctions();
        private Panel pnlPO;
        private ClsColDosPrint[] prnItem;
        private ClsRptDosPrint prnrpt = new ClsRptDosPrint();
        private string rptname = "";
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private string StrSql = "";
        private TextBox txtStkReturnNo;

        public frmRptStockReturnBillsSummary()
        {
            this.InitializeComponent();
        }

        private bool BillPageHeader(ref ClsRptDosPrint ParamClsRpt, string ParamWhere)
        {
            try
            {
                return true;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Bill Page Header");
                return false;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                string str = "";
                this.StrSql = "SELECT * FROM RES_VW_STOCKRETURNMASTER WHERE STOCKRETURNNO <> ''";
                if (this.txtStkReturnNo.Text.Trim() != "")
                {
                    this.StrSql = this.StrSql + " AND STOCKRETURNNO = '" + this.txtStkReturnNo.Text.Trim() + "'";
                    str = str + " StockReturn No : " + this.txtStkReturnNo.Text + "; ";
                }
                if (this.cmbSupplier.Text != "")
                {
                    this.StrSql = this.StrSql + " AND SUPPLIERNAME  = '" + this.cmbSupplier.Text + "'";
                    str = str + " Supplier :" + this.cmbSupplier.Text + "; ";
                }
                if (this.DtpFrStkreturnDate.Checked)
                {
                    this.StrSql = this.StrSql + " AND STOCKRETURNDATE >= '" + this.DtpFrStkreturnDate.Text + "'";
                    str = str + " StockReturn Date From : " + this.DtpFrStkreturnDate.Text + "; ";
                }
                if (this.DtpToStkreturnDate.Checked)
                {
                    this.StrSql = this.StrSql + " AND STOCKRETURNDATE <= '" + this.DtpToStkreturnDate.Text + "'";
                    str = str + " StockReturnDate UpTo :" + this.DtpToStkreturnDate.Text + "; ";
                }
                if (this.DtpFrReturnBillDate.Checked)
                {
                    this.StrSql = this.StrSql + " AND RETURNBILLDATE >= '" + this.DtpFrReturnBillDate.Text + "'";
                    str = str + " ReturnBillDate From : " + this.DtpFrReturnBillDate.Text + "; ";
                }
                if (this.DtpToReturnBillDate.Checked)
                {
                    this.StrSql = this.StrSql + " AND RETURNBILLDATE <= '" + this.DtpToReturnBillDate.Text + "'";
                    str = str + " ReturnBillDate UpTo : " + this.DtpToReturnBillDate.Text + "; ";
                }
                GlobalFill.FillDataSet(this.StrSql, "STOCKRETURNBILLSSUMMARY", this.DS, this.SDA);
                //ParameterFields pfields = new ParameterFields();
                //ParameterField parameterField = new ParameterField
                //{
                //    Name = "Parameter"
                //};
                //ParameterDiscreteValue value2 = new ParameterDiscreteValue
                //{
                //    Value = str
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                if (this.DS.Tables["STOCKRETURNBILLSSUMMARY"].Rows.Count > 0)
                {
                    if (this.cmbPrintOn.Text == "SCREEN")
                    {
                        ///By Bharat we have to change Reports to itextsparp pdf
                        ///
                        RptStkReturnBillsSummary rpt = new RptStkReturnBillsSummary(this.DS.Tables["STOCKRETURNBILLSSUMMARY"]);
                        rpt.ShowDialog();
                        //rpt.SetDataSource(this.DS.Tables["STOCKRETURNBILLSSUMMARY"]);
                        //rpt.Refresh();
                        //new FrmRptViewer(rpt, pfields).ShowDialog();
                    }
                    else
                    {
                        this.rptname = "STOCK_RETURN_BILLS_SUMMARY";
                        this.pfunctions.SETREPORTDATA(ref this.prnrpt, this.rptname, this.DS, this.SDA, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
                        this.pfunctions.SETCOLDATA(ref this.prnItem, this.rptname, this.DS, this.SDA, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
                        this.BillPageHeader(ref this.prnrpt, "");
                        string str2 = "";
                        for (int i = 0; i <= (this.prnItem.Length - 1); i++)
                        {
                            string str3 = "";
                            if (this.prnItem[i].ISGroupHeader)
                            {
                                str3 = (this.prnItem[i].ColumnPosition + 1) + ",";
                            }
                            str2 = str2 + str3;
                        }
                        new ClsDosPrint(this.DS.Tables["STOCKRETURNBILLSSUMMARY"], this.prnItem, this.prnrpt, GlobalVariables.SqlConn, GlobalVariables.PrinterId).PrintData(this.cmbPrintOn.Text, "");
                    }
                }
                else
                {
                    MessageBox.Show("NO DATA FOUND!");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Print_Click");
            }
        }

       

        private void frmRptStockReturnBillsSummary_Load(object sender, EventArgs e)
        {
            try
            {
                new GlobalTheme().applyTheme(this);
                this.RefreshData();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Loading");
            }
        }

   

        private void label1_Click(object sender, EventArgs e)
        {
        }

        private void RefreshData()
        {
            GlobalFill.FillCombo("SELECT SUPPLIERID, SUPPLIERNAME FROM RES_SUPPLIER ORDER BY 2", this.cmbSupplier);
            this.DtpFrReturnBillDate.Value = GlobalVariables.BusinessDate;
            this.DtpToReturnBillDate.Value = GlobalVariables.BusinessDate;
            this.cmbPrintOn.Items.Add("SCREEN");
            this.cmbPrintOn.Items.Add("PRINTER");
            this.cmbPrintOn.Items.Add("DISK");
            this.cmbPrintOn.SelectedIndex = 0;
        }
    }
}
