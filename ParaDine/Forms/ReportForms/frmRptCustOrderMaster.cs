﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using ParaDine.Reports;
using ParaDinePrint;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmRptCustOrderMaster : Form
    {
        private Button BtnExit;
        private Button btnGrid;
        private Button btnPrint;
        private CheckBox chkCancelled;
        private CheckBox chkPendingBills;
        private CheckBox chkRefund;
        private ComboBox cmbCustomer;
        private ComboBox cmbMachineNo;
        private ComboBox cmbOrderType;
        private ComboBox cmbPrintOn;
        private ComboBox cmbSourceName;
        private ComboBox cmbSteward;
        private ComboBox cmbStockPoint;
       // private IContainer components = null;
        private DataSet Ds = new DataSet();
        private DateTimePicker dtpOrderDateFrom;
        private DateTimePicker dtpOrderDateTo;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private Panel grpfields;
        private Label label1;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private ListView lvwPaymentList;
        private PrintingFunctions pfunctions = new PrintingFunctions();
        private ClsColDosPrint[] prnItem;
        private ClsRptDosPrint prnrpt = new ClsRptDosPrint();
        private string rptname = "";
        private SqlDataAdapter Sda = new SqlDataAdapter();
        private string StrSql = "";
        private TextBox txtCustOrderNoFrom;
        private TextBox txtCustOrderNoTo;

        public frmRptCustOrderMaster(SecurityClass paramSecurity)
        {
            this.InitializeComponent();
        }

        private bool BillPageHeader(ref ClsRptDosPrint ParamClsRpt, string ParamWhere)
        {
            try
            {
                return true;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Bill Page Header");
                return false;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnGrid_Click(object sender, EventArgs e)
        {
            string str = "";
            string text = "";
            string str3 = "";
            this.StrSql = "SELECT * FROM RES_VW_CUSTORDERMASTER WHERE CUSTORDERNO <> ''";
            if (this.txtCustOrderNoFrom.Text != "")
            {
                str3 = str3 + " AND CONVERT(INT, CUSTORDERNO) >= " + this.txtCustOrderNoFrom.Text;
                str = str + "CustomerOrderNo From " + this.txtCustOrderNoFrom.Text + "; ";
            }
            if (this.txtCustOrderNoTo.Text != "")
            {
                str3 = str3 + " AND CONVERT(INT, CUSTORDERNO) <= " + this.txtCustOrderNoTo.Text;
                str = str + "CustomerOrderNo Upto " + this.txtCustOrderNoTo.Text + "; ";
            }
            if (this.cmbMachineNo.Text != "")
            {
                str3 = str3 + " AND MACHINENO = " + this.cmbMachineNo.SelectedValue;
                str = str + "Machine = " + this.cmbMachineNo.Text + " ; ";
            }
            if (this.dtpOrderDateFrom.Checked)
            {
                str3 = string.Concat(new object[] { str3, " AND CUSTORDERDATE  >= '", this.dtpOrderDateFrom.Value, "'" });
                str = str + "OrderDate From " + this.dtpOrderDateFrom.Text + "; ";
            }
            if (this.dtpOrderDateTo.Checked)
            {
                str3 = string.Concat(new object[] { str3, " AND CUSTORDERDATE <= '", this.dtpOrderDateTo.Value, "'" });
                str = str + "OrderDate UpTo " + this.dtpOrderDateTo.Text + "; ";
            }
            if (this.cmbSteward.Text != "")
            {
                str3 = str3 + " AND STEWARDNAME = '" + this.cmbSteward.Text + "'";
                str = str + "Steward is " + this.cmbSteward.Text + " ; ";
            }
            if (this.cmbCustomer.Text != "")
            {
                str3 = str3 + " AND CUSTOMERNAME = '" + this.cmbCustomer.Text + "'";
                str = str + "Customer is " + this.cmbCustomer.Text + " ; ";
            }
            if (this.cmbStockPoint.Text != "")
            {
                str3 = str3 + " AND STOCKPOINTNAME = '" + this.cmbStockPoint.Text + "'";
                text = this.cmbStockPoint.Text;
            }
            if (this.cmbSourceName.Text != "")
            {
                str3 = str3 + " AND SOURCENAME = '" + this.cmbSourceName.Text + "'";
                str = str + " Source - " + this.cmbSourceName.Text + " ; ";
            }
            if (this.cmbOrderType.Text != "")
            {
                if (this.cmbOrderType.Text == "KOT BILLS")
                {
                    str3 = str3 + " AND DIRECTBILL = 0 ";
                }
                else if (this.cmbOrderType.Text == "DIRECT BILLS")
                {
                    str3 = str3 + " AND DIRECTBILL = 1 ";
                }
                str = str + "Order Type is " + this.cmbOrderType.Text + " ; ";
            }
            if (this.chkPendingBills.Checked)
            {
                str3 = str3 + " AND SETTLED = 0 ";
                str = str + " Bill is Pending; ";
            }
            if (this.chkCancelled.Checked)
            {
                str3 = str3 + " AND CANCELLEDSTATUS = " + Convert.ToInt32(this.chkCancelled.Checked);
                str = str + "Cancelled; ";
            }
            if (this.lvwPaymentList.CheckedItems.Count > 0)
            {
                string str4 = "(";
                string str5 = "";
                str = str + " Payment Mode is ";
                for (int i = 0; i <= (this.lvwPaymentList.Items.Count - 1); i++)
                {
                    if (this.lvwPaymentList.Items[i].Checked)
                    {
                        str = str + this.lvwPaymentList.Items[i].Text + ", ";
                        if (str5.Trim() != "")
                        {
                            str4 = str4 + " PAYMENTLIST LIKE '%" + str5 + "%' OR";
                        }
                        str5 = this.lvwPaymentList.Items[i].Text;
                    }
                }
                if (str5.Trim() != "")
                {
                    str3 = str3 + " AND " + str4 + " PAYMENTLIST LIKE '%" + str5 + "%')";
                }
            }
            if (this.chkRefund.Checked)
            {
                this.StrSql = this.StrSql + " AND CUSTORDERMASTERID IN  (SELECT DISTINCT COM.CUSTORDERMASTERID FROM RES_CUSTORDERMASTER COM INNER JOIN RES_CUSTORDERKOT COK ON COK.CUSTORDERMASTERID = COM.CUSTORDERMASTERID  INNER JOIN RES_CUSTORDERITEM COI ON COI.CUSTORDERKOTID = COK.CUSTORDERKOTID  WHERE SALEQTY < 0) " + str3;
                str = str + " Refund Bills";
            }
            else
            {
                this.StrSql = this.StrSql + str3;
            }
            this.StrSql = this.StrSql + " ORDER BY CUSTORDERMASTERID";
            GlobalFill.FillDataSet(this.StrSql, "CUSTORDERMASTER", this.Ds, this.Sda);
            DataTable dt = this.Ds.Tables["CUSTORDERMASTER"];
            new frmGridView(dt, "Order Summary ").ShowDialog();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                int num;
                string str = "";
                string text = "";
                string str3 = "";
                this.StrSql = "SELECT * FROM RES_VW_CUSTORDERMASTER WHERE CUSTORDERNO <> ''";
                if (this.txtCustOrderNoFrom.Text != "")
                {
                    str3 = str3 + " AND CONVERT(INT, CUSTORDERNO) >= " + this.txtCustOrderNoFrom.Text;
                    str = str + "CustomerOrderNo From " + this.txtCustOrderNoFrom.Text + "; ";
                }
                if (this.txtCustOrderNoTo.Text != "")
                {
                    str3 = str3 + " AND CONVERT(INT, CUSTORDERNO) <= " + this.txtCustOrderNoTo.Text;
                    str = str + "CustomerOrderNo Upto " + this.txtCustOrderNoTo.Text + "; ";
                }
                if (this.cmbMachineNo.Text != "")
                {
                    str3 = str3 + " AND MACHINENO = " + this.cmbMachineNo.SelectedValue;
                    str = str + "Machine = " + this.cmbMachineNo.Text + " ; ";
                }
                if (this.dtpOrderDateFrom.Checked)
                {
                    str3 = string.Concat(new object[] { str3, " AND CUSTORDERDATE  >= '", this.dtpOrderDateFrom.Value, "'" });
                    str = str + "OrderDate From " + this.dtpOrderDateFrom.Text + "; ";
                }
                if (this.dtpOrderDateTo.Checked)
                {
                    str3 = string.Concat(new object[] { str3, " AND CUSTORDERDATE <= '", this.dtpOrderDateTo.Value, "'" });
                    str = str + "OrderDate UpTo " + this.dtpOrderDateTo.Text + "; ";
                }
                if (this.cmbSteward.Text != "")
                {
                    str3 = str3 + " AND STEWARDNAME = '" + this.cmbSteward.Text + "'";
                    str = str + "Steward is " + this.cmbSteward.Text + " ; ";
                }
                if (this.cmbCustomer.Text != "")
                {
                    str3 = str3 + " AND CUSTOMERNAME = '" + this.cmbCustomer.Text + "'";
                    str = str + "Customer is " + this.cmbCustomer.Text + " ; ";
                }
                if (this.cmbStockPoint.Text != "")
                {
                    str3 = str3 + " AND STOCKPOINTNAME = '" + this.cmbStockPoint.Text + "'";
                    text = this.cmbStockPoint.Text;
                }
                if (this.cmbSourceName.Text != "")
                {
                    str3 = str3 + " AND SOURCENAME = '" + this.cmbSourceName.Text + "'";
                    str = str + " Source - " + this.cmbSourceName.Text + " ; ";
                }
                if (this.cmbOrderType.Text != "")
                {
                    if (this.cmbOrderType.Text == "KOT BILLS")
                    {
                        str3 = str3 + " AND DIRECTBILL = 0 ";
                    }
                    else if (this.cmbOrderType.Text == "DIRECT BILLS")
                    {
                        str3 = str3 + " AND DIRECTBILL = 1 ";
                    }
                    str = str + "Order Type is " + this.cmbOrderType.Text + " ; ";
                }
                if (this.chkPendingBills.Checked)
                {
                    str3 = str3 + " AND SETTLED = 0 ";
                    str = str + " Bill is Pending; ";
                }
                if (this.chkCancelled.Checked)
                {
                    str3 = str3 + " AND CANCELLEDSTATUS = " + Convert.ToInt32(this.chkCancelled.Checked);
                    str = str + "Cancelled; ";
                }
                if (this.lvwPaymentList.CheckedItems.Count > 0)
                {
                    string str4 = "(";
                    string str5 = "";
                    str = str + " Payment Mode is ";
                    for (num = 0; num <= (this.lvwPaymentList.Items.Count - 1); num++)
                    {
                        if (this.lvwPaymentList.Items[num].Checked)
                        {
                            str = str + this.lvwPaymentList.Items[num].Text + ", ";
                            if (str5.Trim() != "")
                            {
                                str4 = str4 + " PAYMENTLIST LIKE '%" + str5 + "%' OR";
                            }
                            str5 = this.lvwPaymentList.Items[num].Text;
                        }
                    }
                    if (str5.Trim() != "")
                    {
                        str3 = str3 + " AND " + str4 + " PAYMENTLIST LIKE '%" + str5 + "%')";
                    }
                }
                if (this.chkRefund.Checked)
                {
                    this.StrSql = this.StrSql + " AND CUSTORDERMASTERID IN  (SELECT DISTINCT COM.CUSTORDERMASTERID FROM RES_CUSTORDERMASTER COM INNER JOIN RES_CUSTORDERKOT COK ON COK.CUSTORDERMASTERID = COM.CUSTORDERMASTERID  INNER JOIN RES_CUSTORDERITEM COI ON COI.CUSTORDERKOTID = COK.CUSTORDERKOTID  WHERE SALEQTY < 0) " + str3;
                    str = str + " Refund Bills";
                }
                else
                {
                    this.StrSql = this.StrSql + str3;
                }
                this.StrSql = this.StrSql + " ORDER BY CUSTORDERMASTERID";
                //ParameterFields pfields = new ParameterFields();
                //ParameterField parameterField = new ParameterField
                //{
                //    Name = "Parameter"
                //};
                //ParameterDiscreteValue value2 = new ParameterDiscreteValue
                //{
                //    Value = str
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                //parameterField = new ParameterField
                //{
                //    Name = "RestaurantName"
                //};
                //value2 = new ParameterDiscreteValue
                //{
                //    Value = text
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                //parameterField = new ParameterField
                //{
                //    Name = "CompanyName"
                //};
                //value2 = new ParameterDiscreteValue
                //{
                //    Value = GlobalVariables.StrCompName
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                //parameterField = new ParameterField
                //{
                //    Name = "Address"
                //};
                //value2 = new ParameterDiscreteValue
                //{
                //    Value = Convert.ToString(GlobalFunctions.GetQueryValue("SELECT TOP 1 ADDRESS FROM GLB_COMPANY"))
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                GlobalFill.FillDataSet(this.StrSql, "CUSTORDERMASTER", this.Ds, this.Sda);
                if (this.Ds.Tables["CUSTORDERMASTER"].Rows.Count > 0)
                {
                    if (this.cmbPrintOn.Text.Trim() == "SCREEN")
                    {
                        ///By Bharat we have to change Reports to itextsparp pdf
                        ///
                        RptCustOrderMaster rpt = new RptCustOrderMaster(this.Ds.Tables["CUSTORDERMASTER"]);
                        rpt.Parameter_Address= Convert.ToString(GlobalFunctions.GetQueryValue("SELECT TOP 1 ADDRESS FROM GLB_COMPANY"));
                        rpt.Parameter_CompanyName=GlobalVariables.StrCompName;
                        rpt.Parameter_parameter=str;
                        rpt.Parameter_RestaurantName=text;
                       // rpt.Parameter_Status=;
                        rpt.Show();
                        //rpt.SetDataSource(this.Ds.Tables["CUSTORDERMASTER"]);
                        //new FrmRptViewer(rpt, pfields).Show();
                    }
                    else
                    {
                        this.rptname = "ORDERSUMMARY";
                        this.pfunctions.SETREPORTDATA(ref this.prnrpt, this.rptname, this.Ds, this.Sda, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
                        this.pfunctions.SETCOLDATA(ref this.prnItem, this.rptname, this.Ds, this.Sda, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
                        this.BillPageHeader(ref this.prnrpt, "");
                        string str6 = "";
                        for (num = 0; num <= (this.prnItem.Length - 1); num++)
                        {
                            string str7 = "";
                            if (this.prnItem[num].ISGroupHeader)
                            {
                                str7 = (this.prnItem[num].ColumnPosition + 1) + ",";
                            }
                            str6 = str6 + str7;
                        }
                        new ClsDosPrint(this.Ds.Tables["CUSTORDERMASTER"], this.prnItem, this.prnrpt, GlobalVariables.SqlConn, GlobalVariables.PrinterId).PrintData(this.cmbPrintOn.Text, "");
                    }
                }
                else
                {
                    MessageBox.Show("Data Not Found!");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

       
        private void frmRptCustOrderMaster_Load(object sender, EventArgs e)
        {
            try
            {
                new GlobalTheme().applyTheme(this);
                this.RefreshData();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Form Load");
            }
        }

   

        private void RefreshData()
        {
            GlobalFill.FillCombo("SELECT TERMINALID, TERMINALNAME FROM GLB_TERMINAL ORDER BY 2", this.cmbMachineNo);
            GlobalFill.FillCombo("SELECT STEWARDID, STEWARDNAME FROM RES_STEWARD ORDER BY 2", this.cmbSteward);
            GlobalFill.FillCombo("SELECT CUSTOMERID, CUSTOMERNAME FROM RES_CUSTOMER ORDER BY 2", this.cmbCustomer);
            GlobalFill.FillCombo("SELECT TABLEID, TABLENAME FROM RES_TABLE ORDER BY 2", this.cmbSourceName);
            GlobalFill.FillCombo("SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT WHERE ISBILLPOINT = 1 ORDER BY 2", this.cmbStockPoint);
            this.cmbOrderType.Items.Add("KOT BILLS");
            this.cmbOrderType.Items.Add("DIRECT BILLS");
            this.cmbOrderType.SelectedIndex = -1;
            this.cmbStockPoint.SelectedValue = GlobalVariables.StockPointId;
            this.cmbPrintOn.Items.Clear();
            this.cmbPrintOn.Items.Add("SCREEN");
            this.cmbPrintOn.Items.Add("PRINTER");
            this.cmbPrintOn.Items.Add("DISK");
            this.cmbPrintOn.SelectedIndex = 0;
            this.StrSql = "SELECT PAYMENTMODEID, PAYMENTMODE FROM GLB_PAYMENTMODE ORDER BY 2";
            GlobalFill.FillDataSet(this.StrSql, "PAYMENTMODE", this.Ds, this.Sda);
            GlobalFill.FillListView(this.lvwPaymentList, this.Ds.Tables["PAYMENTMODE"]);
            this.dtpOrderDateFrom.Value = new DateTime(GlobalVariables.BusinessDate.Year, GlobalVariables.BusinessDate.Month, GlobalVariables.BusinessDate.Day, 0, 0, 0);
            this.dtpOrderDateTo.Value = new DateTime(GlobalVariables.BusinessDate.Year, GlobalVariables.BusinessDate.Month, GlobalVariables.BusinessDate.Day, 0x17, 0x3b, 0x3b);
        }
    }
}
