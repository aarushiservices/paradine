﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmRptRestaurantStock : Form
    {
        private Button btnExit;
        private Button btnPrint;
        private CheckBox ChkShowAllItemStock;
        private ComboBox cmbCategory;
        private ComboBox cmbDepartment;
        private ComboBox cmbRestaurant;
        private ComboBox cmbRptType;
        private ComboBox cmbSection;
        private ComboBox cmbSendTo;
        private ComboBox cmbTax;
       // private IContainer components = null;
        private DataSet DS = new DataSet();
        private DateTimePicker dtpEndDate;
        private DateTimePicker dtpStartDate;
        private GroupBox groupBox1;
        private Label label1;
        private Label label10;
        private Label label13;
        private Label label14;
        private Label label15;
        private Label label17;
        private Label label18;
        private Label label19;
        private Label label2;
        private Label label3;
        private Panel pnlSalesSummary;
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private SqlCommand SqlCmd = new SqlCommand();
        public bool StockToBeCarryForward;
        private string StrSql = "";
        private TextBox txtItemCode;

        public frmRptRestaurantStock()
        {
            this.InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                string str = "";
                this.SqlCmd = new SqlCommand();
                this.SqlCmd.Connection = GlobalVariables.SqlConn;
                if (!this.StockToBeCarryForward)
                {
                    this.SqlCmd.CommandText = "RES_PROC_RESTAURANTSTOCK";
                }
                else
                {
                    this.SqlCmd.CommandText = "RES_PROC_RESTAURANTSTOCKTOBEFORWARD";
                }
                this.SqlCmd.CommandType = CommandType.StoredProcedure;
                if (this.cmbRptType.SelectedIndex == 1)
                {
                    this.SqlCmd.Parameters.AddWithValue("@TRANSTYPE", "CATGWISE");
                    str = str + "  'Category Wise Report' ;";
                }
                else if (this.cmbRptType.SelectedIndex == 2)
                {
                    this.SqlCmd.Parameters.AddWithValue("@TRANSTYPE", "DEPTWISE");
                    str = str + "  'Department Wise Report' ;";
                }
                else if (this.cmbRptType.SelectedIndex == 3)
                {
                    this.SqlCmd.Parameters.AddWithValue("@TRANSTYPE", "SECTIONWISE");
                    str = str + "  'Section Wise Report' ;";
                }
                else if (this.cmbRptType.SelectedIndex == 4)
                {
                    this.SqlCmd.Parameters.AddWithValue("@TRANSTYPE", "TAXWISE");
                    str = str + "  'Tax Wise Report' ;";
                }
                else
                {
                    this.SqlCmd.Parameters.AddWithValue("@TRANSTYPE", "ITEMWISE");
                    str = str + "  'Item Wise Report' ;";
                }
                if (this.txtItemCode.Text.Trim() != "")
                {
                    this.SqlCmd.Parameters.AddWithValue("@ITEMID", this.txtItemCode.Tag);
                    str = str + " ItemCode = " + this.txtItemCode.Text.Trim() + "; ";
                }
                this.SqlCmd.Parameters.AddWithValue("@STDATE", this.dtpStartDate.Text);
                str = str + " Date Range From : " + this.dtpStartDate.Text + "  ";
                this.SqlCmd.Parameters.AddWithValue("@ENDDATE", this.dtpEndDate.Text);
                str = str + " UpTo : " + this.dtpEndDate.Text + " ;  ";
                if (Convert.ToInt16(this.cmbDepartment.SelectedValue) > 0)
                {
                    this.SqlCmd.Parameters.AddWithValue("@DEPARTMENTID", this.cmbDepartment.SelectedValue);
                    str = str + "Department :" + this.cmbDepartment.Text + "; ";
                }
                if (Convert.ToInt16(this.cmbCategory.SelectedValue) > 0)
                {
                    this.SqlCmd.Parameters.AddWithValue("@CATEGORYID", this.cmbCategory.SelectedValue);
                    str = str + "Category : " + this.cmbCategory.Text + "; ";
                }
                if (Convert.ToInt16(this.cmbSection.SelectedValue) > 0)
                {
                    this.SqlCmd.Parameters.AddWithValue("@SECTIONID", this.cmbSection.SelectedValue);
                    str = str + "Section : " + this.cmbSection.Text + "; ";
                }
                if (Convert.ToInt16(this.cmbTax.SelectedValue) > 0)
                {
                    this.SqlCmd.Parameters.AddWithValue("@TAXID", this.cmbTax.SelectedValue);
                    str = str + "Tax : " + this.cmbTax.Text + "; ";
                }
                if (Convert.ToInt16(this.cmbRestaurant.SelectedValue) > 0)
                {
                    this.SqlCmd.Parameters.AddWithValue("@RESTAURANTID", this.cmbRestaurant.SelectedValue);
                    str = str + "\tRestaurant : " + this.cmbRestaurant.Text + "; ";
                }
                this.SqlCmd.Parameters.AddWithValue("@SHOWALL", Convert.ToBoolean(this.ChkShowAllItemStock.Checked));
                this.SDA.SelectCommand = this.SqlCmd;
                if (!object.ReferenceEquals(this.DS.Tables["RESTAURNTSTOCK"], null))
                {
                    this.DS.Tables["RESTAURNTSTOCK"].Clear();
                }
                this.SDA.Fill(this.DS, "RESTAURNTSTOCK");
                //ParameterFields pfields = new ParameterFields();
                //ParameterField parameterField = new ParameterField
                //{
                //    Name = "Parameter"
                //};
                //ParameterDiscreteValue value2 = new ParameterDiscreteValue
                {
                    //Value = str
                };
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                //parameterField = new ParameterField
                {
                    //Name = "STOCKTOBEFORWARD"
                };
                //value2 = new ParameterDiscreteValue
                //{
                //    Value = this.StockToBeCarryForward
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //pfields.Add(parameterField);
                if (this.DS.Tables["RESTAURNTSTOCK"].Rows.Count > 0)
                {
                    if (this.cmbSendTo.Text == "SCREEN")
                    {
                        ///By Bharat we have to change Reports to itextsparp pdf
                        ///
                        //RptRestaurantStock1New rpt = new RptRestaurantStock1New();
                        //rpt.SetDataSource(this.DS.Tables["RESTAURNTSTOCK"]);
                        //rpt.Refresh();
                        //new FrmRptViewer(rpt, pfields).ShowDialog();
                    }
                }
                else
                {
                    MessageBox.Show("DATA NOT FOUND!");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Print_Click");
            }
        }

    

        private void frmRptRestaurantStock_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.RefreshData();
        }



        private void RefreshData()
        {
            GlobalFill.FillCombo("SELECT DEPARTMENTID, DEPARTMENTNAME FROM RES_DEPARTMENT ORDER BY 2", this.cmbDepartment);
            GlobalFill.FillCombo("SELECT CATEGORYID, CATEGORYNAME FROM RES_CATEGORY ORDER BY 2", this.cmbCategory);
            GlobalFill.FillCombo("SELECT SECTIONID, SECTIONNAME FROM RES_SECTION ORDER BY 2", this.cmbSection);
            GlobalFill.FillCombo("SELECT TAXID, TAXNAME FROM RES_TAX ORDER BY 2", this.cmbTax);
            GlobalFill.FillCombo("SELECT RESTAURANTID, RESTAURANTNAME FROM RES_RESTAURANT ORDER BY 2", this.cmbRestaurant);
            this.cmbRptType.Items.Clear();
            this.cmbRptType.Items.Add("Item Wise");
            this.cmbRptType.Items.Add("Category Wise");
            this.cmbRptType.Items.Add("Department Wise");
            this.cmbRptType.Items.Add("Section Wise");
            this.cmbRptType.Items.Add("Tax Wise");
            this.cmbRestaurant.SelectedIndex = 0;
            this.cmbRptType.SelectedIndex = 0;
            this.cmbSendTo.Items.Clear();
            this.cmbSendTo.Items.Add("SCREEN");
            this.cmbSendTo.Items.Add("PRINTER");
            this.cmbSendTo.Items.Add("DISK");
            this.cmbSendTo.SelectedIndex = 0;
            this.dtpEndDate.Value = GlobalVariables.BusinessDate;
            this.dtpStartDate.Value = GlobalVariables.BusinessDate;
            this.SqlCmd = new SqlCommand("SELECT ISNULL(StockToBeForward,0) FROM RES_STOCKREPORTSETTINGS", GlobalVariables.SqlConn);
            this.StockToBeCarryForward = Convert.ToBoolean(this.SqlCmd.ExecuteScalar());
        }
    }
}
