﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.GlobalForms;
using ParaDine.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.ReportForms
{
    public partial class frmItemHistory : Form
    {
        private Button btnExit;
        private Button btnPrint;
        //private IContainer components = null;
        private DateTimePicker dtpOrderDateFrom;
        private GroupBox groupBox1;
        private Panel pnlSalesSummary;

        public frmItemHistory()
        {
            this.InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            string paramSql = "SELECT *  FROM RES_VW_CUSTORDERREPORT ";
            if (this.dtpOrderDateFrom.Checked)
            {
                paramSql = paramSql + " WHERE CUSTORDERDATE  = CONVERT(VARCHAR, CONVERT(DATETIME,'" + this.dtpOrderDateFrom.Text + "'),105)";
            }
            DataTable dataTable = GlobalFill.FillDataTable(paramSql);
            //ParameterFields pfields = new ParameterFields();
            if (dataTable.Rows.Count > 0)
            {
                ///By Bharat we have to change Reports to itextsparp pdf
                ///
                RptItemBillWise rpt = new RptItemBillWise(dataTable);
                rpt.ShowDialog();

                //rpt.SetDataSource(dataTable);
                //new FrmRptViewer(rpt, pfields).ShowDialog();
            }
            else
            {
                MessageBox.Show("DATA NOT FOUND");
            }
        }

       

        private void frmItemHistory_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
        }

      
    }
}
