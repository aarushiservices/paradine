﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.RestaurantItm
{
    partial class FrmDeliveryOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PnlMain = new System.Windows.Forms.Panel();
            this.chkorder = new System.Windows.Forms.CheckBox();
            this.dtpDeliveredDate = new System.Windows.Forms.DateTimePicker();
            this.label18 = new System.Windows.Forms.Label();
            this.btnBrowseCustomer = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.gvNetAmount = new ParaSysCom.NumControl();
            this.label17 = new System.Windows.Forms.Label();
            this.gvAdvanceAmt = new ParaSysCom.NumControl();
            this.label16 = new System.Windows.Forms.Label();
            this.chkCancel = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.btnCustAdd = new System.Windows.Forms.Button();
            this.dtpDeliveryDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpOrderDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDeliveryAdd = new System.Windows.Forms.TextBox();
            this.txtContactNo = new System.Windows.Forms.TextBox();
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.gvTotalAmount = new ParaSysCom.NumControl();
            this.label10 = new System.Windows.Forms.Label();
            this.gvTotalQty = new ParaSysCom.NumControl();
            this.label11 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnTrans = new System.Windows.Forms.Button();
            this.pnlItem = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.gvItemAmount = new ParaSysCom.NumControl();
            this.label4 = new System.Windows.Forms.Label();
            this.gvItemSalePrice = new ParaSysCom.NumControl();
            this.label8 = new System.Windows.Forms.Label();
            this.gvQty = new ParaSysCom.NumControl();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbItemUOM = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtItemCode = new System.Windows.Forms.TextBox();
            this.dgDeliveryOrder = new System.Windows.Forms.DataGridView();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.PnlMain.SuspendLayout();
            this.pnlItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDeliveryOrder)).BeginInit();
            this.SuspendLayout();
            // 
            // PnlMain
            // 
            this.PnlMain.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.PnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlMain.Controls.Add(this.chkorder);
            this.PnlMain.Controls.Add(this.dtpDeliveredDate);
            this.PnlMain.Controls.Add(this.label18);
            this.PnlMain.Controls.Add(this.btnBrowseCustomer);
            this.PnlMain.Controls.Add(this.gvNetAmount);
            this.PnlMain.Controls.Add(this.label17);
            this.PnlMain.Controls.Add(this.gvAdvanceAmt);
            this.PnlMain.Controls.Add(this.label16);
            this.PnlMain.Controls.Add(this.chkCancel);
            this.PnlMain.Controls.Add(this.label15);
            this.PnlMain.Controls.Add(this.txtCustomerName);
            this.PnlMain.Controls.Add(this.btnCustAdd);
            this.PnlMain.Controls.Add(this.dtpDeliveryDate);
            this.PnlMain.Controls.Add(this.label3);
            this.PnlMain.Controls.Add(this.dtpOrderDate);
            this.PnlMain.Controls.Add(this.label2);
            this.PnlMain.Controls.Add(this.label14);
            this.PnlMain.Controls.Add(this.label13);
            this.PnlMain.Controls.Add(this.label1);
            this.PnlMain.Controls.Add(this.txtDeliveryAdd);
            this.PnlMain.Controls.Add(this.txtContactNo);
            this.PnlMain.Controls.Add(this.txtOrderNo);
            this.PnlMain.Controls.Add(this.gvTotalAmount);
            this.PnlMain.Controls.Add(this.label10);
            this.PnlMain.Controls.Add(this.gvTotalQty);
            this.PnlMain.Controls.Add(this.label11);
            this.PnlMain.Controls.Add(this.btnExit);
            this.PnlMain.Controls.Add(this.btnTrans);
            this.PnlMain.Controls.Add(this.pnlItem);
            this.PnlMain.Controls.Add(this.dgDeliveryOrder);
            this.PnlMain.Location = new System.Drawing.Point(3, 4);
            this.PnlMain.Name = "PnlMain";
            this.PnlMain.Size = new System.Drawing.Size(600, 412);
            this.PnlMain.TabIndex = 0;
            // 
            // chkorder
            // 
            this.chkorder.AutoSize = true;
            this.chkorder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkorder.Location = new System.Drawing.Point(8, 355);
            this.chkorder.Name = "chkorder";
            this.chkorder.Size = new System.Drawing.Size(111, 17);
            this.chkorder.TabIndex = 72;
            this.chkorder.Text = "OrderDelivered";
            this.chkorder.UseVisualStyleBackColor = true;
            // 
            // dtpDeliveredDate
            // 
            this.dtpDeliveredDate.Checked = false;
            this.dtpDeliveredDate.CustomFormat = "dd/MMM/yy hh:mm tt";
            this.dtpDeliveredDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDeliveredDate.Location = new System.Drawing.Point(213, 379);
            this.dtpDeliveredDate.Name = "dtpDeliveredDate";
            this.dtpDeliveredDate.ShowCheckBox = true;
            this.dtpDeliveredDate.Size = new System.Drawing.Size(141, 20);
            this.dtpDeliveredDate.TabIndex = 71;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(210, 363);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(84, 13);
            this.label18.TabIndex = 70;
            this.label18.Text = "Delivered Date :";
            // 
            // btnBrowseCustomer
            // 
            this.btnBrowseCustomer.ImageList = this.imageList1;
            this.btnBrowseCustomer.Location = new System.Drawing.Point(170, 332);
            this.btnBrowseCustomer.Name = "btnBrowseCustomer";
            this.btnBrowseCustomer.Size = new System.Drawing.Size(30, 24);
            this.btnBrowseCustomer.TabIndex = 68;
            this.btnBrowseCustomer.TabStop = false;
            this.TTip.SetToolTip(this.btnBrowseCustomer, "Browse existing customers");
            this.btnBrowseCustomer.UseVisualStyleBackColor = true;
            this.btnBrowseCustomer.Visible = false;
            this.btnBrowseCustomer.Click += new System.EventHandler(this.btnBrowseCustomer_Click);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // gvNetAmount
            // 
            this.gvNetAmount.DecimalRequired = true;
            this.gvNetAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvNetAmount.Location = new System.Drawing.Point(475, 382);
            this.gvNetAmount.Name = "gvNetAmount";
            this.gvNetAmount.Size = new System.Drawing.Size(118, 20);
            this.gvNetAmount.SymbolRequired = true;
            this.gvNetAmount.TabIndex = 67;
            this.gvNetAmount.TabStop = false;
            this.gvNetAmount.Text = "`0.00";
            this.gvNetAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvNetAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(392, 388);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(81, 13);
            this.label17.TabIndex = 66;
            this.label17.Text = "Net Amount :";
            // 
            // gvAdvanceAmt
            // 
            this.gvAdvanceAmt.DecimalRequired = true;
            this.gvAdvanceAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvAdvanceAmt.Location = new System.Drawing.Point(475, 355);
            this.gvAdvanceAmt.Name = "gvAdvanceAmt";
            this.gvAdvanceAmt.Size = new System.Drawing.Size(118, 20);
            this.gvAdvanceAmt.SymbolRequired = true;
            this.gvAdvanceAmt.TabIndex = 65;
            this.gvAdvanceAmt.Text = "`0.00";
            this.gvAdvanceAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvAdvanceAmt.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvAdvanceAmt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gvAdvanceAmt_KeyPressEvent);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(362, 361);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(111, 13);
            this.label16.TabIndex = 64;
            this.label16.Text = "Advance Amount :";
            // 
            // chkCancel
            // 
            this.chkCancel.AutoSize = true;
            this.chkCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCancel.ForeColor = System.Drawing.Color.Red;
            this.chkCancel.Location = new System.Drawing.Point(8, 332);
            this.chkCancel.Name = "chkCancel";
            this.chkCancel.Size = new System.Drawing.Size(100, 17);
            this.chkCancel.TabIndex = 63;
            this.chkCancel.Text = "Cancel Order";
            this.chkCancel.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(99, 47);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(88, 13);
            this.label15.TabIndex = 62;
            this.label15.Text = "Customer Name :";
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.BackColor = System.Drawing.Color.White;
            this.txtCustomerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCustomerName.Location = new System.Drawing.Point(102, 63);
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Size = new System.Drawing.Size(284, 20);
            this.txtCustomerName.TabIndex = 4;
            // 
            // btnCustAdd
            // 
            this.btnCustAdd.ImageList = this.imageList1;
            this.btnCustAdd.Location = new System.Drawing.Point(141, 332);
            this.btnCustAdd.Name = "btnCustAdd";
            this.btnCustAdd.Size = new System.Drawing.Size(30, 24);
            this.btnCustAdd.TabIndex = 59;
            this.btnCustAdd.TabStop = false;
            this.TTip.SetToolTip(this.btnCustAdd, "Add new customer");
            this.btnCustAdd.UseVisualStyleBackColor = true;
            this.btnCustAdd.Visible = false;
            this.btnCustAdd.Click += new System.EventHandler(this.btnCustAdd_Click);
            // 
            // dtpDeliveryDate
            // 
            this.dtpDeliveryDate.CustomFormat = "dd/MMM/yy";
            this.dtpDeliveryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDeliveryDate.Location = new System.Drawing.Point(253, 20);
            this.dtpDeliveryDate.Name = "dtpDeliveryDate";
            this.dtpDeliveryDate.Size = new System.Drawing.Size(110, 20);
            this.dtpDeliveryDate.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(256, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 57;
            this.label3.Text = "Exp Delivery Date :";
            // 
            // dtpOrderDate
            // 
            this.dtpOrderDate.CustomFormat = "dd/MMM/yy";
            this.dtpOrderDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOrderDate.Location = new System.Drawing.Point(113, 20);
            this.dtpOrderDate.Name = "dtpOrderDate";
            this.dtpOrderDate.Size = new System.Drawing.Size(110, 20);
            this.dtpOrderDate.TabIndex = 1;
            this.dtpOrderDate.ValueChanged += new System.EventHandler(this.dtpOrderDate_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(116, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 57;
            this.label2.Text = "Order Date :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(388, 5);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 13);
            this.label14.TabIndex = 56;
            this.label14.Text = "Delivery Add :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 47);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 13);
            this.label13.TabIndex = 56;
            this.label13.Text = "Contact No. :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 56;
            this.label1.Text = "Order No. :";
            // 
            // txtDeliveryAdd
            // 
            this.txtDeliveryAdd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDeliveryAdd.Location = new System.Drawing.Point(392, 20);
            this.txtDeliveryAdd.MaxLength = 300;
            this.txtDeliveryAdd.Multiline = true;
            this.txtDeliveryAdd.Name = "txtDeliveryAdd";
            this.txtDeliveryAdd.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDeliveryAdd.Size = new System.Drawing.Size(201, 63);
            this.txtDeliveryAdd.TabIndex = 4;
            this.TTip.SetToolTip(this.txtDeliveryAdd, "Enter Delivery Address");
            // 
            // txtContactNo
            // 
            this.txtContactNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtContactNo.Location = new System.Drawing.Point(8, 63);
            this.txtContactNo.MaxLength = 12;
            this.txtContactNo.Name = "txtContactNo";
            this.txtContactNo.Size = new System.Drawing.Size(88, 20);
            this.txtContactNo.TabIndex = 3;
            this.TTip.SetToolTip(this.txtContactNo, "Enter Contact No");
            this.txtContactNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtContactNo_Validating);
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOrderNo.Location = new System.Drawing.Point(11, 20);
            this.txtOrderNo.MaxLength = 10;
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.Size = new System.Drawing.Size(72, 20);
            this.txtOrderNo.TabIndex = 0;
            // 
            // gvTotalAmount
            // 
            this.gvTotalAmount.DecimalRequired = true;
            this.gvTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvTotalAmount.Location = new System.Drawing.Point(475, 328);
            this.gvTotalAmount.Name = "gvTotalAmount";
            this.gvTotalAmount.Size = new System.Drawing.Size(118, 20);
            this.gvTotalAmount.SymbolRequired = true;
            this.gvTotalAmount.TabIndex = 54;
            this.gvTotalAmount.TabStop = false;
            this.gvTotalAmount.Text = "`0.00";
            this.gvTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvTotalAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(383, 334);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 13);
            this.label10.TabIndex = 53;
            this.label10.Text = "Total Amount :";
            // 
            // gvTotalQty
            // 
            this.gvTotalQty.DecimalRequired = true;
            this.gvTotalQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvTotalQty.Location = new System.Drawing.Point(290, 328);
            this.gvTotalQty.Name = "gvTotalQty";
            this.gvTotalQty.Size = new System.Drawing.Size(63, 20);
            this.gvTotalQty.SymbolRequired = false;
            this.gvTotalQty.TabIndex = 52;
            this.gvTotalQty.TabStop = false;
            this.gvTotalQty.Text = "0.00";
            this.gvTotalQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvTotalQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(221, 334);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 51;
            this.label11.Text = "Total Qty :";
            // 
            // btnExit
            // 
            this.btnExit.CausesValidation = false;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(90, 379);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 26);
            this.btnExit.TabIndex = 9;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(6, 379);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 26);
            this.btnTrans.TabIndex = 8;
            this.btnTrans.Text = "Trans";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // pnlItem
            // 
            this.pnlItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlItem.Controls.Add(this.label12);
            this.pnlItem.Controls.Add(this.txtRemarks);
            this.pnlItem.Controls.Add(this.label9);
            this.pnlItem.Controls.Add(this.gvItemAmount);
            this.pnlItem.Controls.Add(this.label4);
            this.pnlItem.Controls.Add(this.gvItemSalePrice);
            this.pnlItem.Controls.Add(this.label8);
            this.pnlItem.Controls.Add(this.gvQty);
            this.pnlItem.Controls.Add(this.label7);
            this.pnlItem.Controls.Add(this.cmbItemUOM);
            this.pnlItem.Controls.Add(this.label6);
            this.pnlItem.Controls.Add(this.txtItemName);
            this.pnlItem.Controls.Add(this.label5);
            this.pnlItem.Controls.Add(this.txtItemCode);
            this.pnlItem.Location = new System.Drawing.Point(6, 242);
            this.pnlItem.Name = "pnlItem";
            this.pnlItem.Size = new System.Drawing.Size(587, 82);
            this.pnlItem.TabIndex = 5;
            this.pnlItem.Tag = "-1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(247, 41);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 52;
            this.label12.Text = "Remarks :";
            // 
            // txtRemarks
            // 
            this.txtRemarks.BackColor = System.Drawing.Color.White;
            this.txtRemarks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRemarks.Location = new System.Drawing.Point(244, 57);
            this.txtRemarks.MaxLength = 350;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(337, 20);
            this.txtRemarks.TabIndex = 4;
            this.txtRemarks.Tag = "NoHandler";
            this.txtRemarks.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRemarks_KeyDown);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(511, 2);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 50;
            this.label9.Text = "Amount :";
            // 
            // gvItemAmount
            // 
            this.gvItemAmount.DecimalRequired = true;
            this.gvItemAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemAmount.Location = new System.Drawing.Point(507, 16);
            this.gvItemAmount.Name = "gvItemAmount";
            this.gvItemAmount.Size = new System.Drawing.Size(74, 20);
            this.gvItemAmount.SymbolRequired = true;
            this.gvItemAmount.TabIndex = 4;
            this.gvItemAmount.TabStop = false;
            this.gvItemAmount.Tag = "";
            this.gvItemAmount.Text = "`0.00";
            this.gvItemAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvItemAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(420, 2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Sale Price";
            // 
            // gvItemSalePrice
            // 
            this.gvItemSalePrice.DecimalRequired = true;
            this.gvItemSalePrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemSalePrice.Location = new System.Drawing.Point(417, 16);
            this.gvItemSalePrice.Name = "gvItemSalePrice";
            this.gvItemSalePrice.Size = new System.Drawing.Size(64, 20);
            this.gvItemSalePrice.SymbolRequired = true;
            this.gvItemSalePrice.TabIndex = 3;
            this.gvItemSalePrice.Tag = "";
            this.gvItemSalePrice.Text = "`0.00";
            this.gvItemSalePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvItemSalePrice, "Enter Sale Price");
            this.gvItemSalePrice.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvItemSalePrice.TextChanged += new System.EventHandler(this.gvItemCostPrice_Change);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(330, 1);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Qty :";
            // 
            // gvQty
            // 
            this.gvQty.DecimalRequired = true;
            this.gvQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvQty.Location = new System.Drawing.Point(327, 16);
            this.gvQty.Name = "gvQty";
            this.gvQty.Size = new System.Drawing.Size(64, 20);
            this.gvQty.SymbolRequired = false;
            this.gvQty.TabIndex = 2;
            this.gvQty.Tag = "";
            this.gvQty.Text = "0.00";
            this.gvQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvQty, "Enter Qty");
            this.gvQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvQty.TextChanged += new System.EventHandler(this.gvQty_Change);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(131, 2);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "UOM :";
            // 
            // cmbItemUOM
            // 
            this.cmbItemUOM.BackColor = System.Drawing.Color.Ivory;
            this.cmbItemUOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbItemUOM.FormattingEnabled = true;
            this.cmbItemUOM.Location = new System.Drawing.Point(127, 16);
            this.cmbItemUOM.Name = "cmbItemUOM";
            this.cmbItemUOM.Size = new System.Drawing.Size(174, 21);
            this.cmbItemUOM.TabIndex = 1;
            this.cmbItemUOM.SelectionChangeCommitted += new System.EventHandler(this.cmbItemUOM_SelectionChangeCommitted);
            this.cmbItemUOM.SelectedValueChanged += new System.EventHandler(this.cmbItemUOM_SelectedValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Item Name :";
            // 
            // txtItemName
            // 
            this.txtItemName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemName.Location = new System.Drawing.Point(6, 57);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(233, 20);
            this.txtItemName.TabIndex = 5;
            this.txtItemName.TabStop = false;
            this.txtItemName.Tag = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 2);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Item Code :";
            // 
            // txtItemCode
            // 
            this.txtItemCode.BackColor = System.Drawing.Color.Ivory;
            this.txtItemCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemCode.Location = new System.Drawing.Point(6, 16);
            this.txtItemCode.Name = "txtItemCode";
            this.txtItemCode.Size = new System.Drawing.Size(95, 20);
            this.txtItemCode.TabIndex = 0;
            this.txtItemCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItemCode_KeyDown);
            this.txtItemCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtItemCode_Validating);
            // 
            // dgDeliveryOrder
            // 
            this.dgDeliveryOrder.BackgroundColor = System.Drawing.Color.White;
            this.dgDeliveryOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDeliveryOrder.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgDeliveryOrder.Location = new System.Drawing.Point(6, 89);
            this.dgDeliveryOrder.Name = "dgDeliveryOrder";
            this.dgDeliveryOrder.RowHeadersVisible = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.SeaShell;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgDeliveryOrder.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgDeliveryOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDeliveryOrder.Size = new System.Drawing.Size(587, 150);
            this.dgDeliveryOrder.TabIndex = 48;
            this.dgDeliveryOrder.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgDeliveryOrder_CellDoubleClick);
            this.dgDeliveryOrder.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgDeliveryOrder_KeyDown);
            // 
            // FrmDeliveryOrder
            // 
            this.ClientSize = new System.Drawing.Size(607, 419);
            this.Controls.Add(this.PnlMain);
            this.Name = "FrmDeliveryOrder";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Delivery Order";
            this.Load += new System.EventHandler(this.FrmDeliveryOrder_Load);
            this.PnlMain.ResumeLayout(false);
            this.PnlMain.PerformLayout();
            this.pnlItem.ResumeLayout(false);
            this.pnlItem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDeliveryOrder)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
    }
}