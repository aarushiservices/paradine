﻿using ParaDine.Forms.GlobalForms;
using ParaDine.Forms.Masters;
using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.RestaurantItm
{
    public partial class FrmDeliveryOrder : Form
    {
        private Button btnBrowseCustomer;
        private Button btnCustAdd;
        private Button btnExit;
        private Button btnTrans;
        private CheckBox chkCancel;
        private CheckBox chkorder;
        private ComboBox cmbItemUOM;
       //private IContainer components;
        private DataGridView dgDeliveryOrder;
        private DateTimePicker dtpDeliveredDate;
        private DateTimePicker dtpDeliveryDate;
        private DateTimePicker dtpOrderDate;
        private DeliveryOrder EntId;
        private NumControl gvAdvanceAmt;
        private NumControl gvItemAmount;
        private NumControl gvItemSalePrice;
        private NumControl gvNetAmount;
        private NumControl gvQty;
        private NumControl gvTotalAmount;
        private NumControl gvTotalQty;
        private ImageList imageList1;
        private Label label1;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label13;
        private Label label14;
        private Label label15;
        private Label label16;
        private Label label17;
        private Label label18;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private Panel pnlItem;
        private Panel PnlMain;
        private ToolTip TTip;
        private TextBox txtContactNo;
        private TextBox txtCustomerName;
        private TextBox txtDeliveryAdd;
        private TextBox txtItemCode;
        private TextBox txtItemName;
        private TextBox txtOrderNo;
        private TextBox txtRemarks;

        public FrmDeliveryOrder()
        {
            this.EntId = new DeliveryOrder(0L);
            this.components = null;
            this.InitializeComponent();
        }

        public FrmDeliveryOrder(string Mode, long ID, SecurityClass paramSecurity)
        {
            this.EntId = new DeliveryOrder(0L);
            this.components = null;
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes(ID);
            this.EntId.Security = paramSecurity;
        }

        private void btnBrowseCustomer_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void btnCustAdd_Click(object sender, EventArgs e)
        {
            new frmCustomer("&Add", 0, new SecurityClass(new User(ParaDine.GlobalVariables.UserID).LevelID, new Permission("customerToolStripMenuItem").PermissionID)).ShowDialog();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (ParaDine.GlobalValidations.ValidateFields(this.PnlMain, this.TTip))
            {
                this.LoadEntities();
                if (this.btnTrans.Text.ToUpper() != "&ADD")
                {
                    DeliveryOrder order = new DeliveryOrder(Convert.ToInt64(this.txtOrderNo.Tag));
                    if (order.BillRaised)
                    {
                        MessageBox.Show("This Order is already delivered. Changes aborted...");
                        return;
                    }
                    order.Dispose();
                }
                SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_00E0;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_00F0;
                            }
                        }
                        else
                        {
                            this.EntId.Add(sqlTrans, true);
                        }
                    }
                    goto Label_0102;
                Label_00E0:
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_0102;
                Label_00F0:
                    this.EntId.Delete(sqlTrans, true);
                Label_0102:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                    sqlTrans.Rollback();
                }
            }
        }
        private void CalculateItemTotal()
        {
            this.gvItemAmount.Value = Convert.ToDecimal((double)(Convert.ToDouble(this.gvItemSalePrice.Value) * Convert.ToDouble(this.gvQty.Value)));
        }

        private void CalculateTotal()
        {
            this.gvTotalQty.Value = Convert.ToDecimal(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgDeliveryOrder.DataSource, "Qty"));
            this.gvTotalAmount.Value = Convert.ToDecimal(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgDeliveryOrder.DataSource, "Amount"));
            this.gvNetAmount.Value = Convert.ToDecimal((double)(Convert.ToDouble(this.gvTotalAmount.Value) - Convert.ToDouble(this.gvAdvanceAmt.Value)));
        }

        private void cmbItemUOM_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if ((this.cmbItemUOM.SelectedIndex != -1) && (this.cmbItemUOM.ValueMember != ""))
                {
                    Item item = new Item(Convert.ToInt64(this.txtItemCode.Tag));
                    this.gvItemSalePrice.Value = Convert.ToDecimal(item.GetSalePrice(Convert.ToInt32(this.cmbItemUOM.SelectedValue), ParaDine.GlobalVariables.StockPointId));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "ERROR IN CMBITEMUOM_SELECTEDINDEXCHANGED");
            }
        }

        private void cmbItemUOM_SelectionChangeCommitted(object sender, EventArgs e)
        {

        }

        private void dgDeliveryOrder_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgDeliveryOrder.SelectedRows.Count > 0)
            {
                this.pnlItem.Tag = Convert.ToInt16(this.dgDeliveryOrder.SelectedRows[0].Cells["TableId"].Value) - 1;
                this.LoadTransFields(this.EntId.DeliveryOrdtransCollection[Convert.ToInt32(this.pnlItem.Tag)]);
                if (this.txtItemCode.Enabled)
                {
                    this.txtItemCode.Focus();
                }
            }
        }

        private void dgDeliveryOrder_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    this.EntId.DeliveryOrdtransCollection[Convert.ToInt16(this.dgDeliveryOrder.SelectedRows[0].Cells["TableId"].Value) - 1].Deleted = true;
                    this.PopulateView();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    this.dgDeliveryOrder_CellDoubleClick(sender, new DataGridViewCellEventArgs(0, this.dgDeliveryOrder.SelectedRows[0].Index));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in dgDeliveryOrder_KeyDown");
            }
        }
        private void dtpOrderDate_ValueChanged(object sender, EventArgs e)
        {
            this.dtpDeliveryDate.Value = this.dtpOrderDate.Value;
            this.dtpDeliveryDate.MinDate = this.dtpOrderDate.Value;
        }

        private void FrmDeliveryOrder_Load(object sender, EventArgs e)
        {
            if (!this.EntId.Security.AllowRead)
            {
                throw new Exception("Permission Denied");
            }
            new GlobalTheme().applyTheme(this);
            ParaDine.GlobalFunctions.AddCompHandler(this.PnlMain);
            this.LoadFields();
            this.pnlItem.Tag = -1;
        }

        private void gvAdvanceAmt_KeyDownEvent(object sender, KeyEventArgs e)
        {
        }

        private void gvAdvanceAmt_KeyPressEvent(object sender, KeyPressEventArgs e)
        {
            this.CalculateTotal();
        }

        private void gvItemCostPrice_Change(object sender, EventArgs e)
        {
            this.CalculateItemTotal();
        }

        private void gvQty_Change(object sender, EventArgs e)
        {
            this.CalculateItemTotal();
        }

   

        private void LoadEntities()
        {
            this.EntId.OrderID = Convert.ToInt64(this.txtOrderNo.Tag);
            this.EntId.OrderNo = Convert.ToString(this.txtOrderNo.Text);
            this.EntId.OrderDate = Convert.ToDateTime(this.dtpOrderDate.Value);
            this.EntId.DeliveryDate = Convert.ToDateTime(this.dtpDeliveryDate.Value);
            this.EntId.CustID = Convert.ToInt16(this.txtContactNo.Tag);
            this.EntId.DeliveryAdd = Convert.ToString(this.txtDeliveryAdd.Text);
            this.EntId.AdvanceAmount = Convert.ToDouble(this.gvAdvanceAmt.Value);
            this.EntId.Cancelled = this.chkCancel.Checked;
            this.EntId.ContactNo = Convert.ToString(this.txtContactNo.Text);
            this.EntId.ContactName = Convert.ToString(this.txtCustomerName.Text);
            this.EntId.OrderDelivered = Convert.ToBoolean(this.chkorder.Checked);
            if (this.dtpDeliveredDate.Checked)
            {
                this.EntId.HomeDeliveredDate = Convert.ToDateTime(this.dtpDeliveredDate.Value);
            }
        }

        private void LoadFields()
        {
            if ((this.EntId.OrderID == 0L) || this.EntId.BillRaised)
            {
                this.chkCancel.Enabled = false;
            }
            this.txtOrderNo.Tag = Convert.ToString(this.EntId.OrderID);
            if (this.EntId.OrderID <= 0L)
            {
                this.txtOrderNo.Text = this.EntId.GetMaxCode;
                this.txtOrderNo.ReadOnly = true;
            }
            else
            {
                this.txtOrderNo.Text = Convert.ToString(this.EntId.OrderNo);
            }
            this.dtpOrderDate.Value = Convert.ToDateTime(this.EntId.OrderDate);
            this.dtpDeliveryDate.Value = Convert.ToDateTime(this.EntId.DeliveryDate);
            this.txtContactNo.Tag = Convert.ToString(this.EntId.CustID);
            this.txtContactNo.Text = Convert.ToString(this.EntId.ContactNo);
            this.txtCustomerName.Text = Convert.ToString(this.EntId.ContactName);
            this.txtDeliveryAdd.Text = Convert.ToString(this.EntId.DeliveryAdd);
            this.gvAdvanceAmt.Value = Convert.ToDecimal(this.EntId.AdvanceAmount);
            this.chkCancel.Checked = this.EntId.Cancelled;
            this.chkorder.Checked = this.EntId.OrderDelivered;
            if (this.EntId.IsHomeDelivered)
            {
                this.dtpDeliveredDate.Value = this.EntId.HomeDeliveredDate;
            }
            this.PopulateView();
        }

        private void LoadTransEntities(DeliveryOrderTrans varDeliveryTrans)
        {
            varDeliveryTrans.OrderID = this.EntId.OrderID;
            varDeliveryTrans.ItemID = Convert.ToInt64(this.txtItemCode.Tag);
            varDeliveryTrans.UOMID = Convert.ToInt16(this.cmbItemUOM.SelectedValue);
            varDeliveryTrans.Qty = Convert.ToDouble(this.gvQty.Value);
            varDeliveryTrans.Price = Convert.ToDouble(this.gvItemSalePrice.Value);
            varDeliveryTrans.Remarks = Convert.ToString(this.txtRemarks.Text.Trim());
            this.CalculateTotal();
        }

        private void LoadTransFields(DeliveryOrderTrans varDeliveryOrder)
        {
            Item item = new Item(varDeliveryOrder.ItemID);
            this.txtItemCode.Text = Convert.ToString(item.ItemCode);
            this.txtItemName.Text = Convert.ToString(item.ItemName);
            this.txtItemCode_Validating(new object(), new CancelEventArgs());
            this.cmbItemUOM.SelectedValue = Convert.ToInt16(varDeliveryOrder.UOMID);
            this.gvQty.Value = Convert.ToDecimal(varDeliveryOrder.Qty);
            this.gvItemSalePrice.Value = Convert.ToDecimal(varDeliveryOrder.Price);
            this.gvItemAmount.Value = Convert.ToDecimal(varDeliveryOrder.GetAmount());
            this.txtRemarks.Text = varDeliveryOrder.Remarks;
        }

        private void PopulateView()
        {
            ParaDine.GlobalFill.FillGridView(this.dgDeliveryOrder, this.EntId.DeliveryOrdtransCollection.GetCollectiontable());
            this.dgDeliveryOrder.Refresh();
            this.dgDeliveryOrder.AllowUserToAddRows = false;
            this.dgDeliveryOrder.AllowUserToDeleteRows = false;
            this.dgDeliveryOrder.AllowUserToOrderColumns = false;
            this.dgDeliveryOrder.AllowUserToResizeColumns = false;
            this.dgDeliveryOrder.AllowUserToResizeRows = false;
            this.dgDeliveryOrder.EditMode = DataGridViewEditMode.EditProgrammatically;
            this.dgDeliveryOrder.ReadOnly = true;
            this.dgDeliveryOrder.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgDeliveryOrder.RowTemplate.ReadOnly = true;
            this.dgDeliveryOrder.StandardTab = false;
            this.dgDeliveryOrder.RowsDefaultCellStyle.SelectionBackColor = Color.SeaShell;
            this.SetVwColWidth();
            this.CalculateTotal();
        }

        private void SetVwColWidth()
        {
            try
            {
                this.dgDeliveryOrder.Columns[0].Visible = false;
                this.dgDeliveryOrder.Columns[1].Visible = false;
                this.dgDeliveryOrder.Columns[2].Width = 140;
                this.dgDeliveryOrder.Columns[3].Width = 90;
                this.dgDeliveryOrder.Columns[4].Width = 60;
                this.dgDeliveryOrder.Columns[5].Width = 70;
                this.dgDeliveryOrder.Columns[6].Width = 80;
                this.dgDeliveryOrder.Columns[7].Width = 120;
                for (int i = 8; i < this.dgDeliveryOrder.Columns.Count; i++)
                {
                    this.dgDeliveryOrder.Columns[i].Visible = false;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Setting Column Width");
            }
        }

        private void txtContactNo_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.txtContactNo.Text.Trim() != "")
                {
                    long iD = Convert.ToInt64(ParaDine.GlobalFunctions.GetQueryValue("SELECT ISNULL(ORDERID, 0) FROM RES_DELIVERYORDER WHERE CONTACTNO = '" + this.txtContactNo.Text + "'"));
                    if (iD > 0L)
                    {
                        DeliveryOrder order = new DeliveryOrder(iD);
                        this.txtCustomerName.Text = order.ContactName;
                        this.txtDeliveryAdd.Text = order.DeliveryAdd;
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "txtcontactno_validating");
                this.txtContactNo.Focus();
            }
        }

        private void txtcopyDeliveryAdd_Click(object sender, EventArgs e)
        {
            if (this.txtContactNo.Text.Trim() != "")
            {
                Customer customer = new Customer(this.txtContactNo.Text);
                this.txtDeliveryAdd.Text = customer.Address;
            }
        }

        private void txtItemCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F1)
                {
                    string paramSql = "SELECT * FROM RES_VW_ITEM WHERE NONINVENTORY = 0 OR RAWMATERIAL = 1";
                    new frmBrowseData(ParaDine.GlobalFill.FillDataTable(paramSql), this.txtItemCode, "ITEMS", 0).ShowDialog();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in ItemCode_KeyDown");
            }
        }

        private void txtItemCode_Validating(object sender, CancelEventArgs e)
        {
            if (this.txtItemCode.Text.Trim() != "")
            {
                Item item = new Item(this.txtItemCode.Text);
                if (!item.NonInventory || item.RawMaterial)
                {
                    this.txtItemCode.Tag = item.ItemID;
                    this.txtItemCode.Text = item.ItemCode;
                    this.txtItemName.Text = item.ItemName;
                    DataTable uOM = item.GetUOM();
                    if (uOM.Rows.Count > 0)
                    {
                        ParaDine.GlobalFill.FillCombo(uOM, this.cmbItemUOM);
                        if (uOM.Rows.Count == 1)
                        {
                            this.cmbItemUOM.Enabled = false;
                            this.cmbItemUOM.TabStop = false;
                            if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                            {
                                this.cmbItemUOM.SelectedIndex = 0;
                                this.cmbItemUOM_SelectionChangeCommitted(sender, e);
                            }
                        }
                        else
                        {
                            if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                            {
                                this.cmbItemUOM.Enabled = true;
                                this.cmbItemUOM.TabStop = true;
                                this.cmbItemUOM.SelectedIndex = 0;
                            }
                            else
                            {
                                this.cmbItemUOM.Enabled = false;
                                this.cmbItemUOM.Text = Convert.ToString(this.dgDeliveryOrder.SelectedRows[0].Cells["UOM"].Value);
                            }
                            this.cmbItemUOM.TabStop = true;
                        }
                    }
                    else
                    {
                        this.cmbItemUOM.Enabled = true;
                        this.cmbItemUOM.TabStop = true;
                        ParaDine.GlobalFill.FillCombo("SELECT UOMID, UOMNAME FROM RES_UOM", this.cmbItemUOM);
                        this.cmbItemUOM.SelectedValue = item.UOMID;
                        if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                        {
                            this.gvItemSalePrice.Value = Convert.ToDecimal(item.GetSalePrice(Convert.ToInt32(this.cmbItemUOM.SelectedValue), ParaDine.GlobalVariables.StockPointId));
                        }
                    }
                    if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                    {
                        this.gvQty.Value = 1M;
                        this.cmbItemUOM.SelectedIndex = 0;
                    }
                    else
                    {
                        this.cmbItemUOM.Text = Convert.ToString(this.dgDeliveryOrder.SelectedRows[0].Cells["UOM"].Value);
                    }
                    this.CalculateItemTotal();
                }
                else
                {
                    MessageBox.Show("Please Check the Item Code");
                    this.txtItemCode.SelectAll();
                    this.txtItemCode.Focus();
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void txtRemarks_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) && ParaDine.GlobalValidations.ValidateFields(this.pnlItem, this.TTip))
                {
                    DeliveryOrderTrans trans;
                    if (Convert.ToInt32(this.pnlItem.Tag) < 0)
                    {
                        trans = new DeliveryOrderTrans
                        {
                            OrderID = 0L,
                            Added = true
                        };
                        this.LoadTransEntities(trans);
                        this.EntId.DeliveryOrdtransCollection.Add(trans);
                    }
                    else
                    {
                        trans = this.EntId.DeliveryOrdtransCollection[Convert.ToInt32(this.pnlItem.Tag)];
                        this.LoadTransEntities(trans);
                        trans.Modified = true;
                    }
                    this.pnlItem.Tag = -1;
                    this.PopulateView();
                    if (this.txtItemCode.Enabled)
                    {
                        this.txtItemCode.Focus();
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in txtRemark_KeyDownEvent");
            }
        }
    }
}
