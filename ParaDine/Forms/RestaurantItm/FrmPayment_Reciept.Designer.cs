﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.RestaurantItm
{
    partial class FrmPayment_Reciept
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PnlMain = new Panel();
            this.btnPayment = new Button();
            this.lblamt = new Label();
            this.gvRecieptAmt = new NumControl();
            this.label2 = new Label();
            this.gvAmtToPay = new NumControl();
            this.cmbsource = new ComboBox();
            this.lblSource = new Label();
            this.btnExit = new Button();
            this.PnlMain.SuspendLayout();
            base.SuspendLayout();
            this.PnlMain.BorderStyle = BorderStyle.FixedSingle;
            this.PnlMain.Controls.Add(this.gvAmtToPay);
            this.PnlMain.Controls.Add(this.btnPayment);
            this.PnlMain.Controls.Add(this.lblamt);
            this.PnlMain.Controls.Add(this.gvRecieptAmt);
            this.PnlMain.Controls.Add(this.label2);
            this.PnlMain.Controls.Add(this.cmbsource);
            this.PnlMain.Controls.Add(this.lblSource);
            this.PnlMain.Location = new Point(6, 6);
            this.PnlMain.Name = "PnlMain";
            this.PnlMain.Size = new Size(0x155, 0x66);
            this.PnlMain.TabIndex = 1;
            this.btnPayment.Location = new Point(0x6c, 0x3f);
            this.btnPayment.Name = "btnPayment";
            this.btnPayment.Size = new Size(0x7b, 0x1b);
            this.btnPayment.TabIndex = 5;
            this.btnPayment.Text = "Make Payment";
            this.btnPayment.UseVisualStyleBackColor = true;
            this.btnPayment.Click += new EventHandler(this.btnReciept_Click);
            this.lblamt.AutoSize = true;
            this.lblamt.Location = new Point(170, 0x24);
            this.lblamt.Name = "lblamt";
            this.lblamt.Size = new Size(0x4a, 13);
            this.lblamt.TabIndex = 5;
            this.lblamt.Text = "Payment amt :";
            this.gvRecieptAmt.DecimalRequired = true;
            this.gvRecieptAmt.Font = new Font("Rupee Foradian", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.gvRecieptAmt.Location = new Point(0xf8, 0x20);
            this.gvRecieptAmt.Name = "gvRecieptAmt";
            this.gvRecieptAmt.Size = new Size(0x54, 20);
            this.gvRecieptAmt.SymbolRequired = true;
            this.gvRecieptAmt.TabIndex = 4;
            this.gvRecieptAmt.Text = "`0.00";
            this.gvRecieptAmt.TextAlign = HorizontalAlignment.Right;
            int[] bits = new int[4];
            this.gvRecieptAmt.Value = new decimal(bits);
            this.gvRecieptAmt.Validating += new CancelEventHandler(this.gvRecieptAmt_Validating);
            this.label2.AutoSize = true;
            this.label2.Location = new Point(6, 0x24);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0x3f, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Amt to pay :";
            this.gvAmtToPay.DecimalRequired = true;
            this.gvAmtToPay.Font = new Font("Rupee Foradian", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.gvAmtToPay.Location = new Point(0x49, 0x20);
            this.gvAmtToPay.Name = "gvAmtToPay";
            this.gvAmtToPay.Size = new Size(0x54, 20);
            this.gvAmtToPay.SymbolRequired = true;
            this.gvAmtToPay.TabIndex = 2;
            this.gvAmtToPay.Text = "`0.00";
            this.gvAmtToPay.TextAlign = HorizontalAlignment.Right;
           // bits = new int[4];
            this.gvAmtToPay.Value = new decimal(bits);
            this.cmbsource.FormattingEnabled = true;
            this.cmbsource.Location = new Point(0x49, 6);
            this.cmbsource.Name = "cmbsource";
            this.cmbsource.Size = new Size(0x103, 0x15);
            this.cmbsource.TabIndex = 1;
            this.cmbsource.SelectionChangeCommitted += new EventHandler(this.cmbsource_SelectionChangeCommitted);
            this.lblSource.AutoSize = true;
            this.lblSource.Location = new Point(6, 10);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new Size(0x39, 13);
            this.lblSource.TabIndex = 0;
            this.lblSource.Text = "Customer :";
            this.btnExit.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnExit.Location = new Point(0x88, 0x73);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new Size(80, 0x1d);
            this.btnExit.TabIndex = 6;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new EventHandler(this.btnExit_Click);
            base.AutoScaleDimensions = new SizeF(6f, 13f);
//            base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x160, 0x97);
            base.Controls.Add(this.btnExit);
            base.Controls.Add(this.PnlMain);
//            base.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            base.Name = "FrmPayment_Reciept";
            base.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "Customer Payment";
            base.Load += new EventHandler(this.FrmCustomerPayment_Load);
            this.PnlMain.ResumeLayout(false);
            this.PnlMain.PerformLayout();
            base.ResumeLayout(false);
        }
        #endregion
    }
}