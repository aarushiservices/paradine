﻿using ParaDine.Forms.GlobalForms;
using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.RestaurantItm
{
    public partial class FrmPayment_Reciept : Form
    {
        private Button btnExit;
        private Button btnPayment;
        private char ChrGlbSource;
        private ComboBox cmbsource;
       //private IContainer components = null;
        private GlobalTheme GT = new GlobalTheme();
        private NumControl gvAmtToPay;
        private NumControl gvRecieptAmt;
        private Label label2;
        private Label lblamt;
        private Label lblSource;
        private Panel PnlMain;

        public FrmPayment_Reciept(char ChrSource)
        {
            this.InitializeComponent();
            this.ChrGlbSource = ChrSource;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnReciept_Click(object sender, EventArgs e)
        {
            try
            {
                frmMoneyTrans trans = new frmMoneyTrans("&Add", 0L, Convert.ToDouble(this.gvRecieptAmt.Value), false);
                trans.ShowDialog();
                if (trans.DialogResult == DialogResult.Yes)
                {
                    MoneyMaster getMoneyMaster = new MoneyMaster();
                    getMoneyMaster = trans.GetMoneyMaster;
                    if (this.ChrGlbSource == 'C')
                    {
                        getMoneyMaster.TransactionSource = "CUST";
                    }
                    else
                    {
                        getMoneyMaster.TransactionSource = "SUPP";
                    }
                    getMoneyMaster.TransactionSourceID = Convert.ToInt32(this.cmbsource.SelectedValue);
                    SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
                    try
                    {
                        getMoneyMaster.Add(sqlTrans, true);
                        sqlTrans.Commit();
                        this.cmbsource_SelectionChangeCommitted(sender, e);
                        this.gvRecieptAmt.Value = Convert.ToDecimal((double)0.0);
                        this.gvRecieptAmt.Focus();
                    }
                    catch (Exception exception)
                    {
                        sqlTrans.Rollback();
                        MessageBox.Show(exception.Message, "Error Occured while payment");
                    }
                }
            }
            catch (Exception exception2)
            {
                MessageBox.Show(exception2.Message, "Error in payment");
            }
        }

        private void cmbsource_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (this.ChrGlbSource == 'C')
            {
                Customer customer = new Customer(Convert.ToInt16(this.cmbsource.SelectedValue));
                this.gvAmtToPay.Value = Convert.ToDecimal(customer.GetCustomerBalance());
                customer.Dispose();
            }
            else
            {
                Supplier supplier = new Supplier((long)Convert.ToInt16(this.cmbsource.SelectedValue));
                this.gvAmtToPay.Value = Convert.ToDecimal(supplier.GetSupplierBalance());
                supplier.Dispose();
            }
        }

       
        private void FrmCustomerPayment_Load(object sender, EventArgs e)
        {
            ParaDine.GlobalFunctions.GetConnection();
            this.GT.applyTheme(this);
            if (this.ChrGlbSource == 'C')
            {
                this.lblSource.Text = "Customer :";
                this.Text = "Customer Reciept";
                this.btnPayment.Text = "Make Reciept";
                this.lblamt.Text = "Reciept Amt:";
            }
            else
            {
                this.lblSource.Text = "Supplier :";
                this.Text = "Supplier Payment";
                this.btnPayment.Text = "Make Payment";
                this.lblamt.Text = "Payment Amt:";
            }
            this.RefreshData();
        }

        private void gvRecieptAmt_Validating(object sender, CancelEventArgs e)
        {
            if (Convert.ToDouble(this.gvRecieptAmt.Value) > Convert.ToDouble(this.gvAmtToPay.Value))
            {
                MessageBox.Show("Invalid Amount. Please re-enter..");
                e.Cancel = true;
            }
        }
        private void RefreshData()
        {
            if (this.ChrGlbSource == 'C')
            {
                ParaDine.GlobalFill.FillCombo("SELECT CUSTOMERID, CUSTOMERNAME +'('+ CUSTOMERCODE +')' FROM RES_CUSTOMER", this.cmbsource);
            }
            else
            {
                ParaDine.GlobalFill.FillCombo("SELECT SUPPLIERID, SUPPLIERNAME +'('+ SUPPLIERCODE +')' FROM RES_SUPPLIER", this.cmbsource);
            }
        }
    }
}
