﻿using ParaDine.Forms.GlobalForms;
using ParaDine.Forms.ReportForms;
using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Stock
{
    public partial class frmPurchaseOrder : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private ComboBox cmbItemUOM;
        private ComboBox cmbSupplier;
       // private IContainer components;
        private DataGridView dgPO;
        private DateTimePicker dtpDelvDate;
        private DateTimePicker dtpPODate;
        private POMaster EntId;
        private NumControl gvItemAmount;
        private NumControl gvItemCostPrice;
        private NumControl gvItemQty;
        private NumControl gvQtyinHand;
        private NumControl gvTotAmount;
        private NumControl gvTotQty;
        private Label label1;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label2;
        private Label label25;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label lblitem;
        private Panel pnlItem;
        private Panel pnlPo;
        private ToolTip TTip;
        private TextBox txtItemCode;
        private TextBox txtItemName;
        private TextBox txtPoNumber;

        public frmPurchaseOrder()
        {
            this.components = null;
            this.EntId = new POMaster(0L);
            this.InitializeComponent();
        }

        public frmPurchaseOrder(string Mode, long ID, SecurityClass paramSecurity)
        {
            this.components = null;
            this.EntId = new POMaster(0L);
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes(ID);
            this.EntId.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            //if ((this.dgPO.Rows.Count > 0) && ParaDine.GlobalValidations.ValidateFields(this.pnlPo, this.TTip))
            //{
            this.LoadEntities();
            SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
            try
            {
                string str = this.btnTrans.Text.ToUpper();
                if (str != null)
                {
                    if (!(str == "&ADD"))
                    {
                        if (str == "&EDIT")
                        {
                            goto Label_00A7;
                        }
                        if (str == "&DELETE")
                        {
                            goto Label_00B7;
                        }
                    }
                    else
                    {
                        ParaDine.GlobalFunctions.ClearFields(this.pnlPo);
                        this.EntId.Add(sqlTrans, true);
                    }
                }
                goto Label_00C9;
            Label_00A7:
                this.EntId.Modify(sqlTrans, true);
                goto Label_00C9;
            Label_00B7:
                this.EntId.Delete(sqlTrans, true);
            Label_00C9:
                sqlTrans.Commit();
                if ((this.btnTrans.Text.ToUpper() != "&DELETE") && (MessageBox.Show("Do u want to Print This PO??", "Print PO", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes))
                {
                    this.Print();
                }
                base.Close();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                sqlTrans.Rollback();
            }
        }
        // }

        private void CalculateItemTotal()
        {
            this.gvItemAmount.Value = Convert.ToDecimal((double)(Convert.ToDouble(this.gvItemCostPrice.Value) * Convert.ToDouble(this.gvItemQty.Value)));
        }

        private void CalculateTotal()
        {
            this.gvTotQty.Value = Convert.ToDecimal(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgPO.DataSource, "Qty"));
            this.gvTotAmount.Value = Convert.ToDecimal(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgPO.DataSource, "Amount"));
        }

        private void cmbItemUOM_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if ((this.cmbItemUOM.SelectedIndex != -1) && (this.cmbItemUOM.ValueMember != ""))
                {
                    Item item = new Item(Convert.ToInt64(this.txtItemCode.Tag));
                    this.gvItemCostPrice.Value = Convert.ToDecimal(item.GetCostPrice(Convert.ToInt32(this.cmbItemUOM.SelectedValue)));
                    this.gvQtyinHand.Value = Convert.ToDecimal(item.GetWHStock(Convert.ToInt32(this.cmbItemUOM.SelectedValue)));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "ERROR IN CMBITEMUOM_SELECTEDINDEXCHANGED");
            }
        }

        private void cmbItemUOM_SelectionChangeCommitted(object sender, EventArgs e)
        {
        }

        private void dgPO_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgPO.SelectedRows.Count > 0)
            {
                this.pnlItem.Tag = Convert.ToInt16(this.dgPO.SelectedRows[0].Cells["TableId"].Value) - 1;
                this.LoadTransFields(this.EntId.POTransCollection[Convert.ToInt32(this.pnlItem.Tag)]);
                if (this.txtItemCode.Enabled)
                {
                    this.txtItemCode.Focus();
                }
            }
        }

        private void dgPO_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    this.EntId.POTransCollection[Convert.ToInt16(this.dgPO.SelectedRows[0].Cells["TableId"].Value) - 1].Deleted = true;
                    this.PopulateView();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    this.dgPO_CellDoubleClick(sender, new DataGridViewCellEventArgs(0, this.dgPO.SelectedRows[0].Index));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in dgPO_KeyDown");
            }
        }

        private void frmPurchaseOrder_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                new GlobalTheme().applyTheme(this);
                ParaDine.GlobalFunctions.AddCompHandler(this.pnlPo);
                this.RefreshData();
                this.LoadFields();
                this.pnlItem.Tag = -1;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Form Load");
                base.Close();
            }
        }

        private void gvItemCostPrice_Change(object sender, EventArgs e)
        {
            this.CalculateItemTotal();
        }

        private void gvItemCostPrice_KeyDownEvent(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) && ParaDine.GlobalValidations.ValidateFields(this.pnlItem, this.TTip))
                {
                    POTrans trans;
                    if (Convert.ToInt32(this.pnlItem.Tag) < 0)
                    {
                        trans = new POTrans
                        {
                            POTransID = 0L,
                            Added = true
                        };
                        this.LoadTransEntities(trans);
                        trans.POTransID = 0L;
                        this.EntId.POTransCollection.Add(trans);
                    }
                    else
                    {
                        trans = this.EntId.POTransCollection[Convert.ToInt32(this.pnlItem.Tag)];
                        this.LoadTransEntities(trans);
                        trans.Modified = true;
                    }
                    this.pnlItem.Tag = -1;
                    this.PopulateView();
                    if (this.txtItemCode.Enabled)
                    {
                        this.txtItemCode.Focus();
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in gvItemCostPrice_KeyDownEvent");
            }
        }

        private void gvItemCostPrice_KeyPressEvent(object sender, KeyPressEventArgs e)
        {
        }

        private void gvItemQty_Change(object sender, EventArgs e)
        {
            this.CalculateItemTotal();
        }

  

        private void LoadEntities()
        {
            this.EntId.POMasterID = Convert.ToInt64(this.txtPoNumber.Tag);
            this.EntId.PONumber = Convert.ToString(this.txtPoNumber.Text);
            this.EntId.PODate = Convert.ToDateTime(this.dtpPODate.Value);
            this.EntId.DeliveryDate = Convert.ToDateTime(this.dtpDelvDate.Value);
            this.EntId.SupplierID = Convert.ToInt32(this.cmbSupplier.SelectedValue);
            this.EntId.CompanyID = 1;
        }

        private void LoadFields()
        {
            this.txtPoNumber.Tag = Convert.ToString(this.EntId.POMasterID);
            if (this.EntId.POMasterID <= 0L)
            {
                this.txtPoNumber.Text = this.EntId.GetMaxCode;
                this.txtPoNumber.ReadOnly = true;
            }
            else
            {
                this.txtPoNumber.Text = Convert.ToString(this.EntId.PONumber);
            }
            this.dtpPODate.Value = Convert.ToDateTime(this.EntId.PODate);
            this.dtpDelvDate.Value = Convert.ToDateTime(this.EntId.DeliveryDate);
            this.cmbSupplier.SelectedValue = Convert.ToInt32(this.EntId.SupplierID);
            this.PopulateView();
        }

        private void LoadTransEntities(POTrans varPoTrans)
        {
            varPoTrans.POMasterID = this.EntId.POMasterID;
            varPoTrans.ItemID = Convert.ToInt64(this.txtItemCode.Tag);
            varPoTrans.UOMID = Convert.ToInt32(this.cmbItemUOM.SelectedValue);
            varPoTrans.OrderQty = Convert.ToDouble(this.gvItemQty.Value);
            varPoTrans.CostPrice = Convert.ToDouble(this.gvItemCostPrice.Value);
        }

        private void LoadTransFields(POTrans varPoTrans)
        {
            Item item = new Item(varPoTrans.ItemID);
            this.txtItemCode.Text = Convert.ToString(item.ItemCode);
            this.txtItemName.Text = Convert.ToString(item.ItemName);
            this.txtItemCode_Validating(new object(), new CancelEventArgs());
            this.cmbItemUOM.SelectedValue = Convert.ToInt16(varPoTrans.UOMID);
            this.gvItemQty.Value = Convert.ToDecimal(varPoTrans.OrderQty);
            this.gvItemCostPrice.Value = Convert.ToDecimal(varPoTrans.CostPrice);
            this.gvItemAmount.Value = Convert.ToDecimal(varPoTrans.GetItemAmount);
        }

        private void PopulateView()
        {
            ParaDine.GlobalFill.FillGridView(this.dgPO, this.EntId.POTransCollection.GetCollectionTable());
            this.SetVwColWidth();
            this.CalculateTotal();
        }

        private void Print()
        {
            new frmRptStockTrans(this.EntId.POMasterID, this.EntId.PONumber).ShowDialog();
        }

        private void RefreshData()
        {
            try
            {
                ParaDine.GlobalFill.FillCombo("SELECT SUPPLIERID, SUPPLIERNAME FROM RES_SUPPLIER", this.cmbSupplier);
                this.dtpPODate.Value = ParaDine.GlobalVariables.BusinessDate;
                this.dtpDelvDate.Value = ParaDine.GlobalVariables.BusinessDate;
                this.dgPO.AllowUserToAddRows = false;
                this.dgPO.AllowUserToDeleteRows = false;
                this.dgPO.AllowUserToOrderColumns = false;
                this.dgPO.AllowUserToResizeColumns = false;
                this.dgPO.AllowUserToResizeRows = false;
                this.dgPO.EditMode = DataGridViewEditMode.EditProgrammatically;
                this.dgPO.ReadOnly = true;
                this.dgPO.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                this.dgPO.RowTemplate.ReadOnly = true;
                this.dgPO.StandardTab = false;
                this.dgPO.RowsDefaultCellStyle.SelectionBackColor = Color.SeaShell;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In RefreshData");
            }
        }

        private void SetVwColWidth()
        {
            try
            {
                this.dgPO.Columns[0].Visible = false;
                this.dgPO.Columns[1].Width = 180;
                this.dgPO.Columns[2].Width = 100;
                this.dgPO.Columns[3].Width = 0x37;
                this.dgPO.Columns[4].Width = 0x4b;
                this.dgPO.Columns[5].Width = 90;
                for (int i = 6; i < this.dgPO.Columns.Count; i++)
                {
                    this.dgPO.Columns[i].Visible = false;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Setting Column Width");
            }
        }

        private void txtItemCode_Enter(object sender, EventArgs e)
        {
            this.lblitem.Visible = true;
        }

        private void txtItemCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F1)
                {
                    string paramSql = "SELECT * FROM RES_VW_ITEM WHERE NONINVENTORY = 0 OR RAWMATERIAL = 1";
                    new frmBrowseData(ParaDine.GlobalFill.FillDataTable(paramSql), this.txtItemCode, "ITEMS", 0).ShowDialog();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in ItemCode_KeyDown");
            }
        }

        private void txtItemCode_Leave(object sender, EventArgs e)
        {
            this.lblitem.Visible = false;
        }

        private void txtItemCode_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.txtItemCode.Text.Trim() != "")
                {
                    Item item = new Item(this.txtItemCode.Text);
                    if (!item.NonInventory || item.RawMaterial)
                    {
                        this.txtItemCode.Tag = item.ItemID;
                        this.txtItemCode.Text = item.ItemCode;
                        this.txtItemName.Text = item.ItemName;
                        DataTable uOM = item.GetUOM();
                        if (uOM.Rows.Count > 0)
                        {
                            ParaDine.GlobalFill.FillCombo(uOM, this.cmbItemUOM);
                            if (uOM.Rows.Count == 1)
                            {
                                this.cmbItemUOM.Enabled = false;
                                this.cmbItemUOM.TabStop = false;
                                if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                                {
                                    this.cmbItemUOM.SelectedIndex = 0;
                                    this.cmbItemUOM_SelectionChangeCommitted(sender, e);
                                }
                            }
                            else
                            {
                                if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                                {
                                    this.cmbItemUOM.Enabled = true;
                                    this.cmbItemUOM.TabStop = true;
                                    this.cmbItemUOM.SelectedIndex = 0;
                                }
                                else
                                {
                                    this.cmbItemUOM.Enabled = false;
                                    this.cmbItemUOM.Text = Convert.ToString(this.dgPO.SelectedRows[0].Cells["UOM"].Value);
                                }
                                this.cmbItemUOM.TabStop = true;
                            }
                        }
                        else
                        {
                            this.cmbItemUOM.Enabled = true;
                            this.cmbItemUOM.TabStop = true;
                            ParaDine.GlobalFill.FillCombo("SELECT UOMID, UOMNAME FROM RES_UOM", this.cmbItemUOM);
                            this.cmbItemUOM.SelectedValue = item.UOMID;
                            if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                            {
                                this.gvItemCostPrice.Value = Convert.ToDecimal(item.CostPrice);
                            }
                        }
                        if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                        {
                            this.gvItemQty.Value = 1M;
                            this.cmbItemUOM.SelectedIndex = 0;
                        }
                        else
                        {
                            this.cmbItemUOM.Text = Convert.ToString(this.dgPO.SelectedRows[0].Cells["UOM"].Value);
                        }
                        this.CalculateItemTotal();
                    }
                    else
                    {
                        MessageBox.Show("Please Check the Item Code");
                        this.txtItemCode.SelectAll();
                        e.Cancel = true;
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Item Code Validation");
            }
        }

        private void dgPO_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
