﻿using ParaDine.Forms.GlobalForms;
using ParaDine.Forms.ReportForms;
using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Stock
{
    public partial class frmIndent : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private ComboBox cmbItemUOM;
        private ComboBox cmbStockPoint;
       // private IContainer components;
        private DataGridView dgIndent;
        private DateTimePicker dtpIndentDate;
        private IndentMaster EntId;
        private NumControl gvItemAmount;
        private NumControl gvItemCostPrice;
        private NumControl gvItemOrdQty;
        private NumControl gvQtyinHand;
        private NumControl gvTotalQty;
        private NumControl gvTotAmount;
        private Label label1;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label2;
        private Label label25;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label lblitem;
        private Panel pnlItem;
        private Panel PnlMain;
        private string strsql;
        private ToolTip TTip;
        private TextBox txtIndentNo;
        private TextBox txtItemCode;
        private TextBox txtItemName;
        private TextBox txtRemark;

        public frmIndent()
        {
            this.components = null;
            this.strsql = "";
            this.EntId = new IndentMaster(0L);
            this.InitializeComponent();
        }

        public frmIndent(string Mode, long ID, SecurityClass paramSecurity)
        {
            this.components = null;
            this.strsql = "";
            this.EntId = new IndentMaster(0L);
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes(ID);
            this.EntId.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (ParaDine.GlobalValidations.ValidateFields(this.PnlMain, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_0084;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_0094;
                            }
                        }
                        else
                        {
                            this.EntId.Add(sqlTrans, true);
                        }
                    }
                    goto Label_00A6;
                Label_0084:
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_00A6;
                Label_0094:
                    this.EntId.Delete(sqlTrans, true);
                Label_00A6:
                    sqlTrans.Commit();
                    if ((this.btnTrans.Text != "&Delete") && (MessageBox.Show("Do U Want Print Indent Bill ??", "Bill", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes))
                    {
                        DataTable dataTable = new DataTable();
                        this.strsql = "SELECT * FROM RES_VW_INDENT WHERE  indentmasterid= " + this.EntId.IndentMasterID;
                        dataTable = ParaDine.GlobalFill.FillDataTable(this.strsql);
                        IndentBill rpt = new IndentBill();
                        //rpt.SetDataSource(dataTable);
                        //new FrmRptViewer(rpt).Show();
                    }
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                    sqlTrans.Rollback();
                }
            }
        }

        private void CalculateItemTotal()
        {
            this.gvItemAmount.Value = Convert.ToDecimal((double)(Convert.ToDouble(this.gvItemCostPrice.Value) * Convert.ToDouble(this.gvItemOrdQty.Text)));
        }

        private void CalculateTotal()
        {
            this.gvTotalQty.Value = Convert.ToDecimal(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgIndent.DataSource, "Qty"));
            this.gvTotAmount.Value = Convert.ToDecimal(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgIndent.DataSource, "Amount"));
        }

        private void cmbItemUOM_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if ((this.cmbItemUOM.SelectedIndex != -1) && (this.cmbItemUOM.ValueMember != ""))
                {
                    Item item = new Item(Convert.ToInt64(this.txtItemCode.Tag));
                    this.gvItemCostPrice.Value = Convert.ToDecimal(item.GetCostPrice(Convert.ToInt32(this.cmbItemUOM.SelectedValue)));
                    this.gvQtyinHand.Value = Convert.ToDecimal(item.GetStockPointStock(Convert.ToInt32(this.cmbItemUOM.SelectedValue), Convert.ToInt32(this.cmbStockPoint.SelectedValue)));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "ERROR IN CMBITEMUOM_SELECTEDINDEXCHANGED");
            }
        }

        private void cmbItemUOM_SelectionChangeCommitted(object sender, EventArgs e)
        {
        }

        private void dgIndent_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgIndent.SelectedRows.Count > 0)
            {
                this.pnlItem.Tag = Convert.ToInt16(this.dgIndent.SelectedRows[0].Cells["TableId"].Value) - 1;
                this.LoadTransFields(this.EntId.IndentTransCollection[Convert.ToInt32(this.pnlItem.Tag)]);
                if (this.txtItemCode.Enabled)
                {
                    this.txtItemCode.Focus();
                }
            }
        }

        private void dgIndent_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    this.EntId.IndentTransCollection[Convert.ToInt16(this.dgIndent.SelectedRows[0].Cells["TableId"].Value) - 1].Deleted = true;
                    this.PopulateView();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    this.dgIndent_CellDoubleClick(sender, new DataGridViewCellEventArgs(0, this.dgIndent.SelectedRows[0].Index));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in dgIndent_KeyDown");
            }
        }

      

        private void frmIndent_Load(object sender, EventArgs e)
        {
            if (!this.EntId.Security.AllowRead)
            {
                throw new Exception("Permission Denied");
            }
            new GlobalTheme().applyTheme(this);
            ParaDine.GlobalFunctions.AddCompHandler(this.PnlMain);
            this.RefreshData();
            this.LoadFields();
            this.pnlItem.Tag = -1;
        }

        private void gvItemCostPrice_Change(object sender, EventArgs e)
        {
            this.CalculateItemTotal();
        }

        private void gvItemOrdQty_Change(object sender, EventArgs e)
        {
            this.CalculateItemTotal();
        }

       

        private void LoadEntities()
        {
            this.EntId.IndentMasterID = Convert.ToInt64(this.txtIndentNo.Tag);
            this.EntId.IndentMasterNo = Convert.ToString(this.txtIndentNo.Text);
            this.EntId.IndentDate = Convert.ToDateTime(this.dtpIndentDate.Value);
            this.EntId.StockPointID = Convert.ToInt32(this.cmbStockPoint.SelectedValue);
        }

        private void LoadFields()
        {
            this.txtIndentNo.Tag = Convert.ToString(this.EntId.IndentMasterID);
            if (this.EntId.IndentMasterID <= 0L)
            {
                this.txtIndentNo.Text = this.EntId.GetMaxCode;
                this.txtIndentNo.ReadOnly = true;
            }
            else
            {
                this.txtIndentNo.Text = Convert.ToString(this.EntId.IndentMasterNo);
            }
            this.dtpIndentDate.Value = Convert.ToDateTime(this.EntId.IndentDate);
            if (this.EntId.StockPointID > 0)
            {
                this.cmbStockPoint.SelectedValue = Convert.ToInt32(this.EntId.StockPointID);
            }
            this.PopulateView();
        }

        private void LoadTransEntities(IndentTrans varIndTrans)
        {
            varIndTrans.IndentMasterID = this.EntId.IndentMasterID;
            varIndTrans.ItemID = Convert.ToInt64(this.txtItemCode.Tag);
            varIndTrans.UOMID = Convert.ToInt32(this.cmbItemUOM.SelectedValue);
            varIndTrans.OrderQty = Convert.ToDouble(this.gvItemOrdQty.Value);
            varIndTrans.CostPrice = Convert.ToDouble(this.gvItemCostPrice.Value);
            varIndTrans.Remarks = Convert.ToString(this.txtRemark.Text.Trim());
            this.CalculateTotal();
        }

        private void LoadTransFields(IndentTrans varIndTrans)
        {
            Item item = new Item(varIndTrans.ItemID);
            this.txtItemCode.Text = Convert.ToString(item.ItemCode);
            this.txtItemName.Text = Convert.ToString(item.ItemName);
            this.txtItemCode_Validating(new object(), new CancelEventArgs());
            this.cmbItemUOM.SelectedValue = Convert.ToInt16(varIndTrans.UOMID);
            this.gvItemOrdQty.Value = Convert.ToDecimal(varIndTrans.OrderQty);
            this.gvItemCostPrice.Text = Convert.ToString(varIndTrans.CostPrice);
            this.gvItemAmount.Value = Convert.ToDecimal(varIndTrans.GetAmount());
        }

        private void PopulateView()
        {
            ParaDine.GlobalFill.FillGridView(this.dgIndent, this.EntId.IndentTransCollection.GetCollectionTable());
            this.dgIndent.Refresh();
            this.dgIndent.AllowUserToAddRows = false;
            this.dgIndent.AllowUserToDeleteRows = false;
            this.dgIndent.AllowUserToOrderColumns = false;
            this.dgIndent.AllowUserToResizeColumns = false;
            this.dgIndent.AllowUserToResizeRows = false;
            this.dgIndent.EditMode = DataGridViewEditMode.EditProgrammatically;
            this.dgIndent.ReadOnly = true;
            this.dgIndent.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgIndent.RowTemplate.ReadOnly = true;
            this.dgIndent.StandardTab = false;
            this.dgIndent.RowsDefaultCellStyle.SelectionBackColor = Color.SeaShell;
            this.SetVwColWidth();
            this.CalculateTotal();
        }

        private void RefreshData()
        {
            try
            {
                ParaDine.GlobalFill.FillCombo("SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT", this.cmbStockPoint);
                this.dtpIndentDate.Value = ParaDine.GlobalVariables.BusinessDate;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In RefreshData");
            }
        }

        private void SetVwColWidth()
        {
            try
            {
                this.dgIndent.Columns[0].Visible = false;
                this.dgIndent.Columns[1].Visible = false;
                this.dgIndent.Columns[2].Width = 140;
                this.dgIndent.Columns[3].Width = 90;
                this.dgIndent.Columns[4].Width = 60;
                this.dgIndent.Columns[5].Width = 70;
                this.dgIndent.Columns[7].Width = 80;
                for (int i = 7; i < this.dgIndent.Columns.Count; i++)
                {
                    this.dgIndent.Columns[i].Visible = false;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Setting Column Width");
            }
        }

        private void txtItemCode_Enter(object sender, EventArgs e)
        {
            this.lblitem.Visible = true;
        }

        private void txtItemCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F1)
                {
                    string paramSql = "SELECT * FROM RES_VW_ITEM WHERE NONINVENTORY = 0";
                    new frmBrowseData(ParaDine.GlobalFill.FillDataTable(paramSql), this.txtItemCode, "ITEMS", 0).ShowDialog();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in ItemCode_KeyDown");
            }
        }

        private void txtItemCode_Leave(object sender, EventArgs e)
        {
            this.lblitem.Visible = false;
        }

        private void txtItemCode_Validating(object sender, CancelEventArgs e)
        {
            if (this.txtItemCode.Text.Trim() != "")
            {
                Item item = new Item(this.txtItemCode.Text);
                if (item.NonInventory)
                {
                    this.txtItemCode.Tag = item.ItemID;
                    this.txtItemCode.Text = item.ItemCode;
                    this.txtItemName.Text = item.ItemName;
                    DataTable uOM = item.GetUOM();
                    if (uOM.Rows.Count > 0)
                    {
                        ParaDine.GlobalFill.FillCombo(uOM, this.cmbItemUOM);
                        if (uOM.Rows.Count == 1)
                        {
                            this.cmbItemUOM.Enabled = false;
                            this.cmbItemUOM.TabStop = false;
                            if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                            {
                                this.cmbItemUOM.SelectedIndex = 0;
                                this.cmbItemUOM_SelectionChangeCommitted(sender, e);
                            }
                        }
                        else
                        {
                            if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                            {
                                this.cmbItemUOM.Enabled = true;
                                this.cmbItemUOM.TabStop = true;
                                this.cmbItemUOM.SelectedIndex = 0;
                            }
                            else
                            {
                                this.cmbItemUOM.Enabled = false;
                                this.cmbItemUOM.Text = Convert.ToString(this.dgIndent.SelectedRows[0].Cells["UOM"].Value);
                            }
                            this.cmbItemUOM.TabStop = true;
                        }
                    }
                    else
                    {
                        this.cmbItemUOM.Enabled = true;
                        this.cmbItemUOM.TabStop = true;
                        ParaDine.GlobalFill.FillCombo("SELECT UOMID, UOMNAME FROM RES_UOM", this.cmbItemUOM);
                        this.cmbItemUOM.SelectedValue = item.UOMID;
                        if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                        {
                            this.gvItemCostPrice.Value = Convert.ToDecimal(item.CostPrice);
                        }
                    }
                    if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                    {
                        this.gvItemOrdQty.Value = 1M;
                        this.cmbItemUOM.SelectedIndex = 0;
                    }
                    else
                    {
                        this.cmbItemUOM.Text = Convert.ToString(this.dgIndent.SelectedRows[0].Cells["UOM"].Value);
                    }
                    this.CalculateItemTotal();
                }
                else
                {
                    MessageBox.Show("Please Check the Item Code");
                    this.txtItemCode.SelectAll();
                    this.txtItemCode.Focus();
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void txtRemark_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) && ParaDine.GlobalValidations.ValidateFields(this.pnlItem, this.TTip))
                {
                    IndentTrans trans;
                    if (Convert.ToInt32(this.pnlItem.Tag) < 0)
                    {
                        trans = new IndentTrans
                        {
                            IndentTransID = 0L,
                            Added = true
                        };
                        this.LoadTransEntities(trans);
                        this.EntId.IndentTransCollection.Add(trans);
                    }
                    else
                    {
                        trans = this.EntId.IndentTransCollection[Convert.ToInt32(this.pnlItem.Tag)];
                        this.LoadTransEntities(trans);
                        trans.Modified = true;
                    }
                    this.pnlItem.Tag = -1;
                    this.PopulateView();
                    if (this.txtItemCode.Enabled)
                    {
                        this.txtItemCode.Focus();
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in txtRemark_KeyDownEvent");
            }
        }
    }
}
