﻿namespace ParaDine.Forms.Stock
{
    partial class frmPurchaseOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlPo = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.lblitem = new System.Windows.Forms.Label();
            this.gvTotQty = new ParaSysCom.NumControl();
            this.dgPO = new System.Windows.Forms.DataGridView();
            this.label11 = new System.Windows.Forms.Label();
            this.gvTotAmount = new ParaSysCom.NumControl();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnTrans = new System.Windows.Forms.Button();
            this.pnlItem = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.gvQtyinHand = new ParaSysCom.NumControl();
            this.label10 = new System.Windows.Forms.Label();
            this.gvItemAmount = new ParaSysCom.NumControl();
            this.label9 = new System.Windows.Forms.Label();
            this.gvItemCostPrice = new ParaSysCom.NumControl();
            this.label8 = new System.Windows.Forms.Label();
            this.gvItemQty = new ParaSysCom.NumControl();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbItemUOM = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtItemCode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpDelvDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbSupplier = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpPODate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPoNumber = new System.Windows.Forms.TextBox();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.pnlPo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPO)).BeginInit();
            this.pnlItem.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlPo
            // 
            this.pnlPo.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlPo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPo.Controls.Add(this.label12);
            this.pnlPo.Controls.Add(this.lblitem);
            this.pnlPo.Controls.Add(this.gvTotQty);
            this.pnlPo.Controls.Add(this.dgPO);
            this.pnlPo.Controls.Add(this.label11);
            this.pnlPo.Controls.Add(this.gvTotAmount);
            this.pnlPo.Controls.Add(this.btnExit);
            this.pnlPo.Controls.Add(this.btnTrans);
            this.pnlPo.Controls.Add(this.pnlItem);
            this.pnlPo.Controls.Add(this.label4);
            this.pnlPo.Controls.Add(this.dtpDelvDate);
            this.pnlPo.Controls.Add(this.label3);
            this.pnlPo.Controls.Add(this.cmbSupplier);
            this.pnlPo.Controls.Add(this.label2);
            this.pnlPo.Controls.Add(this.dtpPODate);
            this.pnlPo.Controls.Add(this.label1);
            this.pnlPo.Controls.Add(this.txtPoNumber);
            this.pnlPo.Location = new System.Drawing.Point(6, 8);
            this.pnlPo.Name = "pnlPo";
            this.pnlPo.Size = new System.Drawing.Size(539, 361);
            this.pnlPo.TabIndex = 0;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(331, 311);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Total Qty :";
            // 
            // lblitem
            // 
            this.lblitem.AutoSize = true;
            this.lblitem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblitem.ForeColor = System.Drawing.Color.Red;
            this.lblitem.Location = new System.Drawing.Point(105, 308);
            this.lblitem.Name = "lblitem";
            this.lblitem.Size = new System.Drawing.Size(173, 13);
            this.lblitem.TabIndex = 12;
            this.lblitem.Text = "Press F1 For Listing all items.";
            this.lblitem.Visible = false;
            // 
            // gvTotQty
            // 
            this.gvTotQty.Cursor = System.Windows.Forms.Cursors.Default;
            this.gvTotQty.DecimalRequired = true;
            this.gvTotQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvTotQty.Location = new System.Drawing.Point(402, 306);
            this.gvTotQty.Name = "gvTotQty";
            this.gvTotQty.Size = new System.Drawing.Size(123, 20);
            this.gvTotQty.SymbolRequired = false;
            this.gvTotQty.TabIndex = 22;
            this.gvTotQty.TabStop = false;
            this.gvTotQty.Text = "0.00";
            this.gvTotQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvTotQty, "Displays Total Qty");
            this.gvTotQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // dgPO
            // 
            this.dgPO.BackgroundColor = System.Drawing.Color.White;
            this.dgPO.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPO.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgPO.Location = new System.Drawing.Point(7, 65);
            this.dgPO.Name = "dgPO";
            this.dgPO.ReadOnly = true;
            this.dgPO.RowHeadersVisible = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.SeaShell;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgPO.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgPO.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPO.Size = new System.Drawing.Size(523, 150);
            this.dgPO.TabIndex = 8;
            this.dgPO.VirtualMode = true;
            this.dgPO.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPO_CellContentClick);
            this.dgPO.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPO_CellDoubleClick);
            this.dgPO.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgPO_KeyDown);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(308, 337);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Total Amount :";
            // 
            // gvTotAmount
            // 
            this.gvTotAmount.DecimalRequired = true;
            this.gvTotAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvTotAmount.Location = new System.Drawing.Point(401, 332);
            this.gvTotAmount.Name = "gvTotAmount";
            this.gvTotAmount.Size = new System.Drawing.Size(123, 20);
            this.gvTotAmount.SymbolRequired = true;
            this.gvTotAmount.TabIndex = 12;
            this.gvTotAmount.TabStop = false;
            this.gvTotAmount.Text = "`0.00";
            this.gvTotAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvTotAmount, "Displays Total Amount");
            this.gvTotAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // btnExit
            // 
            this.btnExit.CausesValidation = false;
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(93, 329);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 11;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(12, 329);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 10;
            this.btnTrans.Text = "Trans";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // pnlItem
            // 
            this.pnlItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlItem.Controls.Add(this.label25);
            this.pnlItem.Controls.Add(this.gvQtyinHand);
            this.pnlItem.Controls.Add(this.label10);
            this.pnlItem.Controls.Add(this.gvItemAmount);
            this.pnlItem.Controls.Add(this.label9);
            this.pnlItem.Controls.Add(this.gvItemCostPrice);
            this.pnlItem.Controls.Add(this.label8);
            this.pnlItem.Controls.Add(this.gvItemQty);
            this.pnlItem.Controls.Add(this.label7);
            this.pnlItem.Controls.Add(this.cmbItemUOM);
            this.pnlItem.Controls.Add(this.label6);
            this.pnlItem.Controls.Add(this.txtItemName);
            this.pnlItem.Controls.Add(this.label5);
            this.pnlItem.Controls.Add(this.txtItemCode);
            this.pnlItem.Location = new System.Drawing.Point(7, 221);
            this.pnlItem.Name = "pnlItem";
            this.pnlItem.Size = new System.Drawing.Size(523, 80);
            this.pnlItem.TabIndex = 4;
            this.pnlItem.Tag = "";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(186, 8);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(63, 13);
            this.label25.TabIndex = 32;
            this.label25.Text = "Qty in Hand";
            // 
            // gvQtyinHand
            // 
            this.gvQtyinHand.DecimalRequired = true;
            this.gvQtyinHand.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvQtyinHand.Location = new System.Drawing.Point(189, 24);
            this.gvQtyinHand.Name = "gvQtyinHand";
            this.gvQtyinHand.Size = new System.Drawing.Size(78, 20);
            this.gvQtyinHand.SymbolRequired = false;
            this.gvQtyinHand.TabIndex = 31;
            this.gvQtyinHand.Text = "0.00";
            this.gvQtyinHand.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvQtyinHand, "Enter Free Qty ");
            this.gvQtyinHand.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(430, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "Amount";
            // 
            // gvItemAmount
            // 
            this.gvItemAmount.DecimalRequired = true;
            this.gvItemAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemAmount.Location = new System.Drawing.Point(424, 24);
            this.gvItemAmount.Name = "gvItemAmount";
            this.gvItemAmount.Size = new System.Drawing.Size(92, 20);
            this.gvItemAmount.SymbolRequired = true;
            this.gvItemAmount.TabIndex = 4;
            this.gvItemAmount.TabStop = false;
            this.gvItemAmount.Text = "`0.00";
            this.gvItemAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvItemAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(347, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Cost Price";
            // 
            // gvItemCostPrice
            // 
            this.gvItemCostPrice.DecimalRequired = true;
            this.gvItemCostPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemCostPrice.Location = new System.Drawing.Point(343, 24);
            this.gvItemCostPrice.Name = "gvItemCostPrice";
            this.gvItemCostPrice.Size = new System.Drawing.Size(75, 20);
            this.gvItemCostPrice.SymbolRequired = true;
            this.gvItemCostPrice.TabIndex = 3;
            this.gvItemCostPrice.Tag = "NoHandler";
            this.gvItemCostPrice.Text = "`0.00";
            this.gvItemCostPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvItemCostPrice, "Enter Cost Price");
            this.gvItemCostPrice.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvItemCostPrice.TextChanged += new System.EventHandler(this.gvItemCostPrice_Change);
            this.gvItemCostPrice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gvItemCostPrice_KeyDownEvent);
            this.gvItemCostPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gvItemCostPrice_KeyPressEvent);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(284, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Qty";
            // 
            // gvItemQty
            // 
            this.gvItemQty.DecimalRequired = true;
            this.gvItemQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemQty.Location = new System.Drawing.Point(274, 24);
            this.gvItemQty.Name = "gvItemQty";
            this.gvItemQty.Size = new System.Drawing.Size(63, 20);
            this.gvItemQty.SymbolRequired = false;
            this.gvItemQty.TabIndex = 2;
            this.gvItemQty.Text = "0.00";
            this.gvItemQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvItemQty, "Enter Qty");
            this.gvItemQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvItemQty.TextChanged += new System.EventHandler(this.gvItemQty_Change);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(79, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "UOM";
            // 
            // cmbItemUOM
            // 
            this.cmbItemUOM.BackColor = System.Drawing.Color.Ivory;
            this.cmbItemUOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbItemUOM.FormattingEnabled = true;
            this.cmbItemUOM.Location = new System.Drawing.Point(74, 24);
            this.cmbItemUOM.Name = "cmbItemUOM";
            this.cmbItemUOM.Size = new System.Drawing.Size(113, 21);
            this.cmbItemUOM.TabIndex = 1;
            this.TTip.SetToolTip(this.cmbItemUOM, "select UOM");
            this.cmbItemUOM.SelectionChangeCommitted += new System.EventHandler(this.cmbItemUOM_SelectionChangeCommitted);
            this.cmbItemUOM.SelectedValueChanged += new System.EventHandler(this.cmbItemUOM_SelectedValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(30, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Item Name :";
            // 
            // txtItemName
            // 
            this.txtItemName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemName.Location = new System.Drawing.Point(100, 51);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(395, 20);
            this.txtItemName.TabIndex = 5;
            this.txtItemName.TabStop = false;
            this.txtItemName.Tag = "";
            this.TTip.SetToolTip(this.txtItemName, "Displays ItemName");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Item Code";
            // 
            // txtItemCode
            // 
            this.txtItemCode.BackColor = System.Drawing.Color.Ivory;
            this.txtItemCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemCode.Location = new System.Drawing.Point(7, 24);
            this.txtItemCode.Name = "txtItemCode";
            this.txtItemCode.Size = new System.Drawing.Size(59, 20);
            this.txtItemCode.TabIndex = 0;
            this.TTip.SetToolTip(this.txtItemCode, "Enter Item Code");
            this.txtItemCode.Enter += new System.EventHandler(this.txtItemCode_Enter);
            this.txtItemCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItemCode_KeyDown);
            this.txtItemCode.Leave += new System.EventHandler(this.txtItemCode_Leave);
            this.txtItemCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtItemCode_Validating);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(326, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Delv Date :";
            // 
            // dtpDelvDate
            // 
            this.dtpDelvDate.CustomFormat = "dd/MMM/yy";
            this.dtpDelvDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDelvDate.Location = new System.Drawing.Point(393, 39);
            this.dtpDelvDate.Name = "dtpDelvDate";
            this.dtpDelvDate.Size = new System.Drawing.Size(108, 20);
            this.dtpDelvDate.TabIndex = 3;
            this.TTip.SetToolTip(this.dtpDelvDate, "Displays PO Date");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Supplier :";
            // 
            // cmbSupplier
            // 
            this.cmbSupplier.BackColor = System.Drawing.Color.Ivory;
            this.cmbSupplier.FormattingEnabled = true;
            this.cmbSupplier.Location = new System.Drawing.Point(94, 35);
            this.cmbSupplier.Name = "cmbSupplier";
            this.cmbSupplier.Size = new System.Drawing.Size(179, 21);
            this.cmbSupplier.TabIndex = 1;
            this.TTip.SetToolTip(this.cmbSupplier, "Select Supplier");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(333, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "PO Date :";
            // 
            // dtpPODate
            // 
            this.dtpPODate.CustomFormat = "dd/MMM/yy";
            this.dtpPODate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPODate.Location = new System.Drawing.Point(393, 8);
            this.dtpPODate.Name = "dtpPODate";
            this.dtpPODate.Size = new System.Drawing.Size(108, 20);
            this.dtpPODate.TabIndex = 2;
            this.TTip.SetToolTip(this.dtpPODate, "Displays PO Date");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "PO Number :";
            // 
            // txtPoNumber
            // 
            this.txtPoNumber.BackColor = System.Drawing.Color.Ivory;
            this.txtPoNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPoNumber.Location = new System.Drawing.Point(94, 9);
            this.txtPoNumber.Name = "txtPoNumber";
            this.txtPoNumber.Size = new System.Drawing.Size(100, 20);
            this.txtPoNumber.TabIndex = 0;
            this.TTip.SetToolTip(this.txtPoNumber, "Enter PO Number");
            // 
            // frmPurchaseOrder
            // 
            this.CancelButton = this.btnExit;
            this.ClientSize = new System.Drawing.Size(550, 375);
            this.Controls.Add(this.pnlPo);
            this.Name = "frmPurchaseOrder";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Purchase Order";
            this.Load += new System.EventHandler(this.frmPurchaseOrder_Load);
            this.pnlPo.ResumeLayout(false);
            this.pnlPo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPO)).EndInit();
            this.pnlItem.ResumeLayout(false);
            this.pnlItem.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
    }
}