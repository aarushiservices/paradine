﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Stock
{
    partial class frmStockAdjustment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label25 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.gvVarianceQty = new ParaSysCom.NumControl();
            this.dgStockTake = new System.Windows.Forms.DataGridView();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.dtpStockAdjDate = new System.Windows.Forms.DateTimePicker();
            this.txtStockAdjustNo = new System.Windows.Forms.TextBox();
            this.cmbStockPoint = new System.Windows.Forms.ComboBox();
            this.gvPhysicalQty = new ParaSysCom.NumControl();
            this.cmbItemUOM = new System.Windows.Forms.ComboBox();
            this.txtItemCode = new System.Windows.Forms.TextBox();
            this.gvItemCostPrice = new ParaSysCom.NumControl();
            this.gvItemAmount = new ParaSysCom.NumControl();
            this.gvQtyinHand = new ParaSysCom.NumControl();
            this.gvTotalAmount = new ParaSysCom.NumControl();
            this.lblitem = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gvTotalQty = new ParaSysCom.NumControl();
            this.label11 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnTrans = new System.Windows.Forms.Button();
            this.lblSource = new System.Windows.Forms.Label();
            this.PnlMain = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.pnlItem = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgStockTake)).BeginInit();
            this.PnlMain.SuspendLayout();
            this.pnlItem.SuspendLayout();
            this.SuspendLayout();
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(326, 47);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(63, 13);
            this.label25.TabIndex = 46;
            this.label25.Text = "Qty in Hand";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(316, 2);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 13);
            this.label12.TabIndex = 29;
            this.label12.Text = "Variance Qty :";
            // 
            // gvVarianceQty
            // 
            this.gvVarianceQty.DecimalRequired = true;
            this.gvVarianceQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvVarianceQty.Location = new System.Drawing.Point(312, 18);
            this.gvVarianceQty.Name = "gvVarianceQty";
            this.gvVarianceQty.Size = new System.Drawing.Size(82, 20);
            this.gvVarianceQty.SymbolRequired = false;
            this.gvVarianceQty.TabIndex = 3;
            this.gvVarianceQty.Tag = "";
            this.gvVarianceQty.Text = "0.00";
            this.gvVarianceQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvVarianceQty, "Displays CostPrice");
            this.gvVarianceQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvVarianceQty.TextChanged += new System.EventHandler(this.gvVarianceQty_Change);
            // 
            // dgStockTake
            // 
            this.dgStockTake.BackgroundColor = System.Drawing.Color.White;
            this.dgStockTake.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgStockTake.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgStockTake.Location = new System.Drawing.Point(7, 66);
            this.dgStockTake.Name = "dgStockTake";
            this.dgStockTake.RowHeadersVisible = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.SeaShell;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgStockTake.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgStockTake.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgStockTake.Size = new System.Drawing.Size(495, 150);
            this.dgStockTake.TabIndex = 4;
            this.dgStockTake.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgStockTake_CellDoubleClick);
            this.dgStockTake.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgStockTake_KeyDown);
            // 
            // dtpStockAdjDate
            // 
            this.dtpStockAdjDate.CustomFormat = "dd/MMM/yy";
            this.dtpStockAdjDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStockAdjDate.Location = new System.Drawing.Point(356, 10);
            this.dtpStockAdjDate.Name = "dtpStockAdjDate";
            this.dtpStockAdjDate.Size = new System.Drawing.Size(100, 20);
            this.dtpStockAdjDate.TabIndex = 1;
            this.TTip.SetToolTip(this.dtpStockAdjDate, "Displays KitchenToStoreDate");
            // 
            // txtStockAdjustNo
            // 
            this.txtStockAdjustNo.Location = new System.Drawing.Point(100, 10);
            this.txtStockAdjustNo.Name = "txtStockAdjustNo";
            this.txtStockAdjustNo.Size = new System.Drawing.Size(84, 20);
            this.txtStockAdjustNo.TabIndex = 0;
            this.TTip.SetToolTip(this.txtStockAdjustNo, "enter KitchenToStore No");
            // 
            // cmbStockPoint
            // 
            this.cmbStockPoint.BackColor = System.Drawing.Color.White;
            this.cmbStockPoint.FormattingEnabled = true;
            this.cmbStockPoint.Location = new System.Drawing.Point(100, 36);
            this.cmbStockPoint.Name = "cmbStockPoint";
            this.cmbStockPoint.Size = new System.Drawing.Size(158, 21);
            this.cmbStockPoint.TabIndex = 3;
            this.TTip.SetToolTip(this.cmbStockPoint, "Select Kitchen");
            // 
            // gvPhysicalQty
            // 
            this.gvPhysicalQty.DecimalRequired = true;
            this.gvPhysicalQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvPhysicalQty.Location = new System.Drawing.Point(233, 22);
            this.gvPhysicalQty.Name = "gvPhysicalQty";
            this.gvPhysicalQty.Size = new System.Drawing.Size(73, 20);
            this.gvPhysicalQty.SymbolRequired = false;
            this.gvPhysicalQty.TabIndex = 2;
            this.gvPhysicalQty.Tag = "";
            this.gvPhysicalQty.Text = "0.00";
            this.gvPhysicalQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvPhysicalQty, "Enter Trans Qty");
            this.gvPhysicalQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvPhysicalQty.TextChanged += new System.EventHandler(this.gvPhysicalQty_Change);
            this.gvPhysicalQty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gvPhysicalQty_KeyDownEvent);
            // 
            // cmbItemUOM
            // 
            this.cmbItemUOM.BackColor = System.Drawing.Color.Ivory;
            this.cmbItemUOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbItemUOM.FormattingEnabled = true;
            this.cmbItemUOM.Location = new System.Drawing.Point(95, 23);
            this.cmbItemUOM.Name = "cmbItemUOM";
            this.cmbItemUOM.Size = new System.Drawing.Size(132, 21);
            this.cmbItemUOM.TabIndex = 1;
            this.TTip.SetToolTip(this.cmbItemUOM, "Select an Unit");
            this.cmbItemUOM.SelectedValueChanged += new System.EventHandler(this.cmbItemUOM_SelectedValueChanged);
            // 
            // txtItemCode
            // 
            this.txtItemCode.BackColor = System.Drawing.Color.Ivory;
            this.txtItemCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemCode.Location = new System.Drawing.Point(10, 23);
            this.txtItemCode.Name = "txtItemCode";
            this.txtItemCode.Size = new System.Drawing.Size(76, 20);
            this.txtItemCode.TabIndex = 0;
            this.TTip.SetToolTip(this.txtItemCode, "Give an Item Code");
            this.txtItemCode.Enter += new System.EventHandler(this.txtItemCode_Enter);
            this.txtItemCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItemCode_KeyDown);
            this.txtItemCode.Leave += new System.EventHandler(this.txtItemCode_Leave);
            this.txtItemCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtItemCode_Validating);
            // 
            // gvItemCostPrice
            // 
            this.gvItemCostPrice.DecimalRequired = true;
            this.gvItemCostPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemCostPrice.Location = new System.Drawing.Point(404, 18);
            this.gvItemCostPrice.Name = "gvItemCostPrice";
            this.gvItemCostPrice.Size = new System.Drawing.Size(83, 20);
            this.gvItemCostPrice.SymbolRequired = true;
            this.gvItemCostPrice.TabIndex = 47;
            this.gvItemCostPrice.Tag = "NoHandler";
            this.gvItemCostPrice.Text = "`0.00";
            this.gvItemCostPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvItemCostPrice, "Enter Trans Qty");
            this.gvItemCostPrice.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvItemCostPrice.TextChanged += new System.EventHandler(this.gvItemCostPrice_Change);
            this.gvItemCostPrice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gvItemCostPrice_KeyDownEvent);
            // 
            // gvItemAmount
            // 
            this.gvItemAmount.DecimalRequired = true;
            this.gvItemAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemAmount.Location = new System.Drawing.Point(404, 64);
            this.gvItemAmount.Name = "gvItemAmount";
            this.gvItemAmount.Size = new System.Drawing.Size(83, 20);
            this.gvItemAmount.SymbolRequired = true;
            this.gvItemAmount.TabIndex = 49;
            this.gvItemAmount.Tag = "";
            this.gvItemAmount.Text = "`0.00";
            this.gvItemAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvItemAmount, "Displays CostPrice");
            this.gvItemAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // gvQtyinHand
            // 
            this.gvQtyinHand.DecimalRequired = true;
            this.gvQtyinHand.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvQtyinHand.Location = new System.Drawing.Point(326, 63);
            this.gvQtyinHand.Name = "gvQtyinHand";
            this.gvQtyinHand.Size = new System.Drawing.Size(68, 20);
            this.gvQtyinHand.SymbolRequired = false;
            this.gvQtyinHand.TabIndex = 45;
            this.gvQtyinHand.Text = "0.00";
            this.gvQtyinHand.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvQtyinHand, "Enter Free Qty ");
            this.gvQtyinHand.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvQtyinHand.TextChanged += new System.EventHandler(this.gvQtyinHand_Change);
            // 
            // gvTotalAmount
            // 
            this.gvTotalAmount.DecimalRequired = true;
            this.gvTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvTotalAmount.Location = new System.Drawing.Point(384, 362);
            this.gvTotalAmount.Name = "gvTotalAmount";
            this.gvTotalAmount.Size = new System.Drawing.Size(118, 20);
            this.gvTotalAmount.SymbolRequired = true;
            this.gvTotalAmount.TabIndex = 46;
            this.gvTotalAmount.Text = "`0.00";
            this.gvTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvTotalAmount, "Enter Free Qty ");
            this.gvTotalAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // lblitem
            // 
            this.lblitem.AutoSize = true;
            this.lblitem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblitem.ForeColor = System.Drawing.Color.Red;
            this.lblitem.Location = new System.Drawing.Point(122, 326);
            this.lblitem.Name = "lblitem";
            this.lblitem.Size = new System.Drawing.Size(173, 13);
            this.lblitem.TabIndex = 41;
            this.lblitem.Text = "Press F1 For Listing all items.";
            this.lblitem.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(252, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Stock Adjust Date :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Stock Adjust No.";
            // 
            // gvTotalQty
            // 
            this.gvTotalQty.DecimalRequired = true;
            this.gvTotalQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvTotalQty.Location = new System.Drawing.Point(384, 334);
            this.gvTotalQty.Name = "gvTotalQty";
            this.gvTotalQty.Size = new System.Drawing.Size(118, 20);
            this.gvTotalQty.SymbolRequired = false;
            this.gvTotalQty.TabIndex = 26;
            this.gvTotalQty.Text = "0.00";
            this.gvTotalQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvTotalQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(314, 339);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Total Qty :";
            // 
            // btnExit
            // 
            this.btnExit.CausesValidation = false;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(90, 358);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 7;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(7, 358);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 6;
            this.btnTrans.Text = "Trans";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // lblSource
            // 
            this.lblSource.AutoSize = true;
            this.lblSource.Location = new System.Drawing.Point(29, 39);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(65, 13);
            this.lblSource.TabIndex = 27;
            this.lblSource.Text = "StockPoint :";
            // 
            // PnlMain
            // 
            this.PnlMain.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.PnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlMain.Controls.Add(this.gvTotalAmount);
            this.PnlMain.Controls.Add(this.label10);
            this.PnlMain.Controls.Add(this.lblitem);
            this.PnlMain.Controls.Add(this.cmbStockPoint);
            this.PnlMain.Controls.Add(this.lblSource);
            this.PnlMain.Controls.Add(this.gvTotalQty);
            this.PnlMain.Controls.Add(this.label11);
            this.PnlMain.Controls.Add(this.btnExit);
            this.PnlMain.Controls.Add(this.btnTrans);
            this.PnlMain.Controls.Add(this.pnlItem);
            this.PnlMain.Controls.Add(this.dgStockTake);
            this.PnlMain.Controls.Add(this.dtpStockAdjDate);
            this.PnlMain.Controls.Add(this.label2);
            this.PnlMain.Controls.Add(this.label1);
            this.PnlMain.Controls.Add(this.txtStockAdjustNo);
            this.PnlMain.Location = new System.Drawing.Point(5, 6);
            this.PnlMain.Name = "PnlMain";
            this.PnlMain.Size = new System.Drawing.Size(511, 393);
            this.PnlMain.TabIndex = 40;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(291, 368);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 13);
            this.label10.TabIndex = 42;
            this.label10.Text = "Total Amount :";
            // 
            // pnlItem
            // 
            this.pnlItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlItem.Controls.Add(this.label9);
            this.pnlItem.Controls.Add(this.gvItemAmount);
            this.pnlItem.Controls.Add(this.label4);
            this.pnlItem.Controls.Add(this.gvItemCostPrice);
            this.pnlItem.Controls.Add(this.label25);
            this.pnlItem.Controls.Add(this.gvQtyinHand);
            this.pnlItem.Controls.Add(this.label12);
            this.pnlItem.Controls.Add(this.gvVarianceQty);
            this.pnlItem.Controls.Add(this.label8);
            this.pnlItem.Controls.Add(this.gvPhysicalQty);
            this.pnlItem.Controls.Add(this.label7);
            this.pnlItem.Controls.Add(this.cmbItemUOM);
            this.pnlItem.Controls.Add(this.label6);
            this.pnlItem.Controls.Add(this.txtItemName);
            this.pnlItem.Controls.Add(this.label5);
            this.pnlItem.Controls.Add(this.txtItemCode);
            this.pnlItem.Location = new System.Drawing.Point(7, 223);
            this.pnlItem.Name = "pnlItem";
            this.pnlItem.Size = new System.Drawing.Size(495, 95);
            this.pnlItem.TabIndex = 4;
            this.pnlItem.Tag = "-1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(404, 48);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 50;
            this.label9.Text = "Amount :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(404, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Cost Price";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(237, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Qty :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(98, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "UOM :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Item Name :";
            // 
            // txtItemName
            // 
            this.txtItemName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemName.Location = new System.Drawing.Point(9, 65);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(311, 20);
            this.txtItemName.TabIndex = 5;
            this.txtItemName.TabStop = false;
            this.txtItemName.Tag = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Item Code :";
            // 
            // frmStockAdjustment
            // 
            this.ClientSize = new System.Drawing.Size(520, 405);
            this.Controls.Add(this.PnlMain);
            this.Name = "frmStockAdjustment";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stock Adjustment";
            this.Load += new System.EventHandler(this.frmStockAdjustment_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgStockTake)).EndInit();
            this.PnlMain.ResumeLayout(false);
            this.PnlMain.PerformLayout();
            this.pnlItem.ResumeLayout(false);
            this.pnlItem.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
    }
}