﻿using ParaDine.Forms.GlobalForms;
using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Stock
{
    public partial class frmStockReturn : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private ComboBox cmbItemUOM;
        private ComboBox cmbStockPoint;
        private ComboBox cmbSupplier;
       // private IContainer components;
        private DataGridView dgStkReturn;
        private DateTimePicker DtpStockReturnBillDate;
        private DateTimePicker DtpStockReturnDate;
        private StockReturnMaster EntID;
        private StockReturnTrans EntStockReturnTrans;
        private NumControl gvFreightChars;
        private NumControl gvGrossAmt;
        private NumControl gvItemAmount;
        private NumControl gvItemCostPrice;
        private NumControl gvQtyinHand;
        private NumControl gvReturnQty;
        private NumControl gvTotalAmt;
        private NumControl gvTotQty;
        private Label label1;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label13;
        private Label label14;
        private Label label15;
        private Label label16;
        private Label label17;
        private Label label19;
        private Label label2;
        private Label label25;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label lblitem;
        private Panel pnlItem;
        private Panel pnlStockReturn;
        private ToolTip TTip;
        private TextBox txtItemCode;
        private TextBox txtItemName;
        private TextBox txtLrNo;
        private TextBox txtRemarks;
        private TextBox txtReturnBillNo;
        private TextBox txtStockReturnNo;

        public frmStockReturn()
        {
            this.EntID = new StockReturnMaster(0L);
            this.components = null;
            this.InitializeComponent();
        }
        public frmStockReturn(string Mode, long ID, SecurityClass paramSecurity)
        {
            this.EntID = new StockReturnMaster(0L);
            this.components = null;
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntID.LoadAttributes(ID);
            this.EntID.Security = paramSecurity;
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }
        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (ParaDine.GlobalValidations.ValidateFields(this.pnlStockReturn, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_007D;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_008D;
                            }
                        }
                        else
                        {
                            this.EntID.Add(sqlTrans, true);
                        }
                    }
                    goto Label_009F;
                Label_007D:
                    this.EntID.Modify(sqlTrans, true);
                    goto Label_009F;
                Label_008D:
                    this.EntID.Delete(sqlTrans, true);
                Label_009F:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                    sqlTrans.Rollback();
                }
            }
        }
        private void CalculateItemTotal()
        {
            this.gvItemAmount.Value = Convert.ToDecimal((double)(Convert.ToDouble(this.gvItemCostPrice.Value) * Convert.ToDouble(this.gvReturnQty.Value)));
        }

        private void CalculateTotal()
        {
            this.gvGrossAmt.Value = Convert.ToDecimal(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgStkReturn.DataSource, "Amount"));
            this.gvTotQty.Value = Convert.ToDecimal(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgStkReturn.DataSource, "ReturnQty"));
            this.gvTotalAmt.Value = Convert.ToDecimal((double)(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgStkReturn.DataSource, "Amount") + Convert.ToDouble(this.gvFreightChars.Value)));
        }

        private void cmbItemUOM_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if ((this.cmbItemUOM.SelectedIndex != -1) && (this.cmbItemUOM.ValueMember != ""))
                {
                    Item item = new Item(Convert.ToString(this.txtItemCode.Text));
                    this.gvItemCostPrice.Value = Convert.ToDecimal(item.GetCostPrice(Convert.ToInt32(this.cmbItemUOM.SelectedValue)));
                    this.gvQtyinHand.Value = Convert.ToDecimal((double)(item.GetStockPointStock(Convert.ToInt32(this.cmbItemUOM.SelectedValue), Convert.ToInt32(this.cmbStockPoint.SelectedValue)) - this.EntID.StockReturntransCollection.GetPreviousQty(item.ItemID, Convert.ToInt32(this.cmbItemUOM.SelectedValue))));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "ERROR IN CMBITEMUOM_SELECTEDINDEXCHANGED");
            }
        }

        private void cmbItemUOM_SelectionChangeCommitted(object sender, EventArgs e)
        {

        }

        private void dgStkReturn_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgStkReturn.SelectedRows.Count > 0)
            {
                this.pnlItem.Tag = Convert.ToInt16(this.dgStkReturn.SelectedRows[0].Cells["TableId"].Value) - 1;
                this.LoadTransFields(this.EntID.StockReturntransCollection[Convert.ToInt32(this.pnlItem.Tag)]);
                if (this.txtItemCode.Enabled)
                {
                    this.txtItemCode.Focus();
                }
            }
        }
        private void dgStkReturn_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    this.EntID.StockReturntransCollection[Convert.ToInt32(this.dgStkReturn.SelectedRows[0].Cells["TABLEID"].Value) - 1].Deleted = true;
                    this.PopulateView();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    this.dgStkReturn_CellDoubleClick(sender, new DataGridViewCellEventArgs(0, this.dgStkReturn.SelectedRows[0].Index));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }    
        private void frmStockReturn_Load(object sender, EventArgs e)
        {
            try
            {
                new GlobalTheme().applyTheme(this);
                ParaDine.GlobalFunctions.AddCompHandler(this.pnlStockReturn);
                this.RefreshData();
                this.LoadFields();
                this.pnlItem.Tag = -1;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Form Load");
                base.Close();
            }
        }
        private void gvFreightChars_Change(object sender, EventArgs e)
        {
            this.CalculateTotal();
        }

        private void gvItemCostPrice_Change(object sender, EventArgs e)
        {
            this.CalculateItemTotal();
        }

        private void gvReturnQty_Change(object sender, EventArgs e)
        {
            this.CalculateItemTotal();
        }

        private void gvTot_KeyPressEvent(object sender, KeyPressEventArgs e)
        {
        }
        private void LoadEntities()
        {
            this.EntID.StockReturnMasterID = Convert.ToInt64(this.txtStockReturnNo.Tag);
            this.EntID.StockReturnNo = Convert.ToString(this.txtStockReturnNo.Text);
            this.EntID.StockreturnDate = Convert.ToDateTime(this.DtpStockReturnDate.Value);
            this.EntID.SupplierID = Convert.ToInt64(this.cmbSupplier.SelectedValue);
            this.EntID.StockPointId = Convert.ToInt64(this.cmbStockPoint.SelectedValue);
            this.EntID.ReturnBillNo = Convert.ToString(this.txtReturnBillNo.Text);
            this.EntID.ReturnBillDate = Convert.ToDateTime(this.DtpStockReturnBillDate.Value);
            this.EntID.LRNo = Convert.ToString(this.txtLrNo.Text);
            this.EntID.FreightCharges = Convert.ToDouble(this.gvFreightChars.Value);
            this.EntID.CompanyID = 1;
        }
        private void LoadFields()
        {
            this.txtStockReturnNo.Tag = Convert.ToInt64(this.EntID.StockReturnMasterID);
            if (this.EntID.StockReturnMasterID <= 0L)
            {
                this.txtStockReturnNo.Text = this.EntID.GetMaxCode;
                this.txtStockReturnNo.ReadOnly = true;
            }
            else
            {
                this.txtStockReturnNo.Text = Convert.ToString(this.EntID.StockReturnNo);
            }
            this.DtpStockReturnBillDate.Value = Convert.ToDateTime(this.EntID.StockreturnDate);
            this.cmbSupplier.SelectedValue = Convert.ToInt64(this.EntID.SupplierID);
            this.cmbStockPoint.SelectedValue = Convert.ToInt64(this.EntID.StockPointId);
            this.txtReturnBillNo.Text = Convert.ToString(this.EntID.ReturnBillNo);
            this.DtpStockReturnBillDate.Value = Convert.ToDateTime(this.EntID.ReturnBillDate);
            this.txtLrNo.Text = Convert.ToString(this.EntID.LRNo);
            this.gvFreightChars.Value = Convert.ToDecimal(this.EntID.FreightCharges);
            this.PopulateView();
        }

        private void LoadTransEntities(StockReturnTrans VarStockReturnTrans)
        {
            VarStockReturnTrans.StockReturnMasterID = this.EntID.StockReturnMasterID;
            VarStockReturnTrans.ItemID = Convert.ToInt64(this.txtItemCode.Tag);
            VarStockReturnTrans.UOMID = Convert.ToInt32(this.cmbItemUOM.SelectedValue);
            VarStockReturnTrans.ReturnQty = Convert.ToDouble(this.gvReturnQty.Value);
            VarStockReturnTrans.CostPrice = Convert.ToDouble(this.gvItemCostPrice.Value);
            VarStockReturnTrans.Remarks = Convert.ToString(this.txtRemarks.Text);
            VarStockReturnTrans.StockPointId = Convert.ToInt32(this.cmbStockPoint.SelectedValue);
        }

        private void LoadTransFields(StockReturnTrans VarStockReturnTrans)
        {
            Item item = new Item(VarStockReturnTrans.ItemID);
            this.txtItemCode.Text = Convert.ToString(item.ItemCode);
            this.txtItemName.Text = Convert.ToString(item.ItemName);
            this.txtItemCode_Validating(new object(), new CancelEventArgs());
            this.cmbItemUOM.SelectedValue = Convert.ToInt16(VarStockReturnTrans.UOMID);
            this.gvReturnQty.Value = Convert.ToDecimal(VarStockReturnTrans.ReturnQty);
            this.gvItemCostPrice.Value = Convert.ToDecimal(VarStockReturnTrans.CostPrice);
            this.gvItemAmount.Value = Convert.ToDecimal(VarStockReturnTrans.GetItemAmount);
            this.txtRemarks.Text = Convert.ToString(VarStockReturnTrans.Remarks);
        }

        private void PopulateView()
        {
            ParaDine.GlobalFill.FillGridView(this.dgStkReturn, this.EntID.StockReturntransCollection.GetCollectionTable());
            this.dgStkReturn.Refresh();
            this.dgStkReturn.AllowUserToAddRows = false;
            this.dgStkReturn.AllowUserToDeleteRows = false;
            this.dgStkReturn.AllowUserToOrderColumns = false;
            this.dgStkReturn.AllowUserToResizeColumns = false;
            this.dgStkReturn.AllowUserToResizeRows = false;
            this.dgStkReturn.EditMode = DataGridViewEditMode.EditProgrammatically;
            this.dgStkReturn.ReadOnly = true;
            this.dgStkReturn.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgStkReturn.RowTemplate.ReadOnly = true;
            this.dgStkReturn.StandardTab = false;
            this.dgStkReturn.RowsDefaultCellStyle.SelectionBackColor = Color.SeaShell;
            this.SetVwColWidth();
            this.CalculateTotal();
        }

        private void RefreshData()
        {
            try
            {
                ParaDine.GlobalFill.FillCombo("SELECT SUPPLIERID, SUPPLIERNAME FROM RES_SUPPLIER ORDER BY 2", this.cmbSupplier);
                ParaDine.GlobalFill.FillCombo("SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT", this.cmbStockPoint);
                this.DtpStockReturnBillDate.Value = ParaDine.GlobalVariables.BusinessDate;
                this.DtpStockReturnDate.Value = ParaDine.GlobalVariables.BusinessDate;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In RefreshData");
            }
        }
        private void SetVwColWidth()
        {
            try
            {
                this.dgStkReturn.Columns[0].Visible = false;
                this.dgStkReturn.Columns[1].Width = 180;
                this.dgStkReturn.Columns[2].Width = 100;
                this.dgStkReturn.Columns[3].Width = 0x55;
                this.dgStkReturn.Columns[4].Width = 80;
                this.dgStkReturn.Columns[5].Width = 0x55;
                this.dgStkReturn.Columns[6].Width = 150;
                for (int i = 7; i < this.dgStkReturn.Columns.Count; i++)
                {
                    this.dgStkReturn.Columns[i].Visible = false;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Setting Column Width");
            }
        }

        private void txtItemCode_Enter(object sender, EventArgs e)
        {
            this.lblitem.Visible = true;
        }

        private void txtItemCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                string paramSql = "SELECT * FROM RES_VW_ITEM WHERE NONINVENTORY = 0 ";
                new frmBrowseData(ParaDine.GlobalFill.FillDataTable(paramSql), this.txtItemCode, "ITEMS", 2).ShowDialog();
            }
        }

        private void txtItemCode_Leave(object sender, EventArgs e)
        {
            this.lblitem.Visible = false;
        }

        private void txtItemCode_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if ((this.txtItemCode.Text.Trim() != "") && (Convert.ToInt32(this.cmbStockPoint.SelectedValue) > 0))
                {
                    Item item = new Item(this.txtItemCode.Text);
                    if (!item.NonInventory)
                    {
                        throw new Exception("U Entered A Non Inventory Item.\nPlease Check the Item Code....");
                    }
                    this.txtItemCode.Tag = item.ItemID;
                    this.txtItemCode.Text = item.ItemCode;
                    this.txtItemName.Text = item.ItemName;
                    DataTable uOM = item.GetUOM();
                    if (uOM.Rows.Count > 0)
                    {
                        ParaDine.GlobalFill.FillCombo(uOM, this.cmbItemUOM);
                        if (uOM.Rows.Count == 1)
                        {
                            this.cmbItemUOM.Enabled = false;
                            this.cmbItemUOM.TabStop = false;
                            if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                            {
                                this.cmbItemUOM.SelectedIndex = 0;
                                this.cmbItemUOM_SelectionChangeCommitted(sender, e);
                            }
                        }
                        else
                        {
                            if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                            {
                                this.cmbItemUOM.Enabled = true;
                                this.cmbItemUOM.TabStop = true;
                                this.cmbItemUOM.SelectedIndex = 0;
                            }
                            else
                            {
                                this.cmbItemUOM.Enabled = false;
                                this.cmbItemUOM.Text = Convert.ToString(this.dgStkReturn.SelectedRows[0].Cells["UOM"].Value);
                            }
                            this.cmbItemUOM.TabStop = true;
                        }
                    }
                    else
                    {
                        this.cmbItemUOM.Enabled = true;
                        this.cmbItemUOM.TabStop = true;
                        ParaDine.GlobalFill.FillCombo("SELECT UOMID, UOMNAME FROM RES_UOM ORDER BY 2", this.cmbItemUOM);
                        this.cmbItemUOM.SelectedValue = item.UOMID;
                        if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                        {
                            this.gvItemCostPrice.Value = Convert.ToDecimal(item.CostPrice);
                        }
                    }
                    if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                    {
                        this.gvReturnQty.Value = 1M;
                        this.cmbItemUOM.SelectedIndex = 0;
                    }
                    else
                    {
                        this.cmbItemUOM.Text = Convert.ToString(this.dgStkReturn.SelectedRows[0].Cells["UOM"].Value);
                    }
                    this.CalculateItemTotal();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Item Code", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                e.Cancel = true;
            }
        }
        private void txtRemarks_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) && ParaDine.GlobalValidations.ValidateFields(this.pnlStockReturn, this.TTip))
                {
                    if (Convert.ToInt32(this.pnlItem.Tag) < 0)
                    {
                        StockReturnTrans varStockReturnTrans = new StockReturnTrans
                        {
                            StockReturnMasterID = this.EntID.StockReturnMasterID,
                            StockReturnTransID = 0L
                        };
                        this.LoadTransEntities(varStockReturnTrans);
                        varStockReturnTrans.Added = true;
                        this.EntID.StockReturntransCollection.Add(varStockReturnTrans);
                    }
                    else
                    {
                        StockReturnTrans trans2 = this.EntID.StockReturntransCollection[Convert.ToInt32(this.pnlItem.Tag)].Copy();
                        this.LoadTransEntities(trans2);
                        this.EntID.StockReturntransCollection.InsertAt(trans2, Convert.ToInt16(this.pnlItem.Tag));
                    }
                    this.pnlItem.Tag = -1;
                    this.PopulateView();
                    if (this.txtItemCode.Enabled)
                    {
                        this.txtItemCode.Focus();
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Remarks_KeyDown", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
    }
}
