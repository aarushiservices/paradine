﻿using ParaDine.Forms.GlobalForms;
using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Stock
{
    public partial class FrmStockWriteOff : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private ComboBox cmbItemUOM;
        private ComboBox cmbStockPoint;
        //private IContainer components;
        private DataGridView dgStockWriteOff;
        private DateTimePicker dtpStockWriteOffDate;
        private StockWriteOff EntId;
        private NumControl gvItemAmount;
        private NumControl gvItemCostPrice;
        private NumControl gvPhysicalQty;
        private NumControl gvQtyinHand;
        private NumControl gvTotalAmount;
        private NumControl gvTotalQty;
        private Label label1;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label2;
        private Label label25;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label lblitem;
        private Panel pnlItem;
        private Panel PnlMain;
        private ToolTip TTip;
        private TextBox txtItemCode;
        private TextBox txtItemName;
        private TextBox txtRemarks;
        private TextBox txtStockWriteOffNo;

        public FrmStockWriteOff()
        {
            this.components = null;
            this.EntId = new StockWriteOff(0L);
            this.InitializeComponent();
        }

        public FrmStockWriteOff(string Mode, long ID, SecurityClass ParamSecurity)
        {
            this.components = null;
            this.EntId = new StockWriteOff(0L);
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes(ID);
            this.EntId.Security = ParamSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (ParaDine.GlobalValidations.ValidateFields(this.PnlMain, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_007D;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_008D;
                            }
                        }
                        else
                        {
                            this.EntId.Add(sqlTrans, true);
                        }
                    }
                    goto Label_009F;
                Label_007D:
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_009F;
                Label_008D:
                    this.EntId.Delete(sqlTrans, true);
                Label_009F:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                    sqlTrans.Rollback();
                }
            }
        }

        private void CalculateItemTotal()
        {
            this.gvItemAmount.Value = Convert.ToDecimal((double)(Convert.ToDouble(this.gvItemCostPrice.Value) * Convert.ToDouble(this.gvPhysicalQty.Value)));
        }

        private void CalculateTotal()
        {
            this.gvTotalQty.Value = Convert.ToDecimal(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgStockWriteOff.DataSource, "Qty"));
            this.gvTotalAmount.Value = Convert.ToDecimal(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgStockWriteOff.DataSource, "Amount"));
        }

        private void cmbItemUOM_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if ((this.cmbItemUOM.SelectedIndex != -1) && (this.cmbItemUOM.ValueMember != ""))
                {
                    Item item = new Item(Convert.ToString(this.txtItemCode.Text));
                    this.gvItemCostPrice.Value = Convert.ToDecimal(item.GetCostPrice(Convert.ToInt32(this.cmbItemUOM.SelectedValue)));
                    this.gvQtyinHand.Value = Convert.ToDecimal(item.GetStockPointStock(Convert.ToInt32(this.cmbItemUOM.SelectedValue), Convert.ToInt32(this.cmbStockPoint.SelectedValue)));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "ERROR IN CMBITEMUOM_SELECTEDINDEXCHANGED");
            }
        }

        private void dgStockWriteOff_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgStockWriteOff.SelectedRows.Count > 0)
            {
                this.pnlItem.Tag = Convert.ToInt16(this.dgStockWriteOff.SelectedRows[0].Cells["TableId"].Value) - 1;
                this.LoadTransFields(this.EntId.SWOTransCollection[Convert.ToInt32(this.pnlItem.Tag)]);
                if (this.txtItemCode.Enabled)
                {
                    this.txtItemCode.Focus();
                }
            }
        }

        private void dgStockWriteOff_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    this.EntId.SWOTransCollection[Convert.ToInt32(this.dgStockWriteOff.SelectedRows[0].Cells["TABLEID"].Value) - 1].Deleted = true;
                    this.PopulateView();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    this.dgStockWriteOff_CellDoubleClick(sender, new DataGridViewCellEventArgs(0, this.dgStockWriteOff.SelectedRows[0].Index));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

    

        private void FrmStockWriteOff_Load(object sender, EventArgs e)
        {
            try
            {
                new GlobalTheme().applyTheme(this);
                ParaDine.GlobalFunctions.AddCompHandler(this.PnlMain);
                this.RefreshData();
                this.LoadFields();
                this.pnlItem.Tag = -1;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Form Load");
                base.Close();
            }
        }

        private void gvItemCostPrice_Change(object sender, EventArgs e)
        {
            this.CalculateItemTotal();
        }

        private void gvItemCostPrice_KeyDownEvent(object sender, KeyEventArgs e)
        {
        }

        private void gvPhysicalQty_Change(object sender, EventArgs e)
        {
            this.CalculateItemTotal();
        }

  

        private void LoadEntities()
        {
            this.EntId.WriteOffID = Convert.ToInt64(this.txtStockWriteOffNo.Tag);
            this.EntId.WriteOffNo = Convert.ToString(this.txtStockWriteOffNo.Text);
            this.EntId.WriteOffDate = Convert.ToDateTime(this.dtpStockWriteOffDate.Value);
            this.EntId.StockPointId = Convert.ToInt16(this.cmbStockPoint.SelectedValue);
        }

        public void LoadFields()
        {
            this.txtStockWriteOffNo.Tag = Convert.ToString(this.EntId.WriteOffID);
            if (Convert.ToInt64(this.EntId.WriteOffID) == 0L)
            {
                this.txtStockWriteOffNo.Text = this.EntId.GetMaxCode;
            }
            else
            {
                this.txtStockWriteOffNo.Text = this.EntId.WriteOffNo;
            }
            this.dtpStockWriteOffDate.Value = Convert.ToDateTime(this.EntId.WriteOffDate);
            this.cmbStockPoint.SelectedValue = Convert.ToInt16(this.EntId.StockPointId);
            this.PopulateView();
        }

        private void LoadTransEntities(StockWriteOffTrans VarStockWOTrans)
        {
            VarStockWOTrans.WriteOffID = this.EntId.WriteOffID;
            VarStockWOTrans.ItemID = Convert.ToInt64(this.txtItemCode.Tag);
            VarStockWOTrans.UOMID = Convert.ToInt32(this.cmbItemUOM.SelectedValue);
            VarStockWOTrans.Qty = Convert.ToDouble(this.gvPhysicalQty.Value);
            VarStockWOTrans.Price = Convert.ToDouble(this.gvItemCostPrice.Value);
            VarStockWOTrans.Remarks = Convert.ToString(this.txtRemarks.Text);
            VarStockWOTrans.StockPointId = Convert.ToInt32(this.cmbStockPoint.SelectedValue);
        }

        private void LoadTransFields(StockWriteOffTrans VarStockWriteOffTrans)
        {
            Item item = new Item(VarStockWriteOffTrans.ItemID);
            this.txtItemCode.Tag = Convert.ToInt64(item.ItemID);
            this.txtItemCode.Text = Convert.ToString(item.ItemCode);
            this.txtItemCode_Validating(new object(), new CancelEventArgs());
            this.txtItemName.Text = Convert.ToString(item.ItemName);
            this.cmbItemUOM.SelectedValue = Convert.ToInt32(VarStockWriteOffTrans.UOMID);
            this.gvPhysicalQty.Value = Convert.ToDecimal(VarStockWriteOffTrans.Qty);
            this.gvItemCostPrice.Value = Convert.ToDecimal(VarStockWriteOffTrans.Price);
            this.txtRemarks.Text = Convert.ToString(VarStockWriteOffTrans.Remarks);
        }

        private void PopulateView()
        {
            ParaDine.GlobalFill.FillGridView(this.dgStockWriteOff, this.EntId.SWOTransCollection.GetCollectionTable());
            this.dgStockWriteOff.Refresh();
            this.dgStockWriteOff.AllowUserToAddRows = false;
            this.dgStockWriteOff.AllowUserToDeleteRows = false;
            this.dgStockWriteOff.AllowUserToOrderColumns = false;
            this.dgStockWriteOff.AllowUserToResizeColumns = false;
            this.dgStockWriteOff.AllowUserToResizeRows = false;
            this.dgStockWriteOff.EditMode = DataGridViewEditMode.EditProgrammatically;
            this.dgStockWriteOff.ReadOnly = true;
            this.dgStockWriteOff.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgStockWriteOff.RowTemplate.ReadOnly = true;
            this.dgStockWriteOff.StandardTab = false;
            this.dgStockWriteOff.RowsDefaultCellStyle.SelectionBackColor = Color.SeaShell;
            this.SetVwColWidth();
            this.CalculateTotal();
        }

        private void RefreshData()
        {
            ParaDine.GlobalFill.FillCombo("SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT", this.cmbStockPoint);
        }

        private void SetVwColWidth()
        {
            try
            {
                this.dgStockWriteOff.Columns[0].Visible = false;
                this.dgStockWriteOff.Columns[1].Visible = false;
                this.dgStockWriteOff.Columns[2].Width = 150;
                this.dgStockWriteOff.Columns[3].Width = 100;
                this.dgStockWriteOff.Columns[4].Width = 60;
                this.dgStockWriteOff.Columns[5].Width = 80;
                this.dgStockWriteOff.Columns[6].Width = 80;
                for (int i = 8; i < this.dgStockWriteOff.Columns.Count; i++)
                {
                    this.dgStockWriteOff.Columns[i].Visible = false;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Setting Column Width");
            }
        }

        private void txtItemCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                string paramSql = "SELECT * FROM RES_VW_ITEM WHERE NONINVENTORY = 0 ";
                new frmBrowseData(ParaDine.GlobalFill.FillDataTable(paramSql), this.txtItemCode, "ITEMS", 2).ShowDialog();
            }
        }

        private void txtItemCode_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.txtItemCode.Text.Trim() != "")
                {
                    Item item = new Item(this.txtItemCode.Text);
                    if (!item.NonInventory)
                    {
                        throw new Exception("U Entered A Non Inventory Item.\nPlease Check the Item Code....");
                    }
                    this.txtItemCode.Tag = item.ItemID;
                    this.txtItemCode.Text = item.ItemCode;
                    this.txtItemName.Text = item.ItemName;
                    DataTable uOM = item.GetUOM();
                    if (uOM.Rows.Count > 0)
                    {
                        ParaDine.GlobalFill.FillCombo(uOM, this.cmbItemUOM);
                        if (uOM.Rows.Count == 1)
                        {
                            this.cmbItemUOM.Enabled = false;
                            this.cmbItemUOM.TabStop = false;
                            if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                            {
                                this.cmbItemUOM.SelectedIndex = 0;
                                this.cmbItemUOM_SelectedValueChanged(sender, e);
                            }
                        }
                        else
                        {
                            if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                            {
                                this.cmbItemUOM.Enabled = true;
                                this.cmbItemUOM.TabStop = true;
                                this.cmbItemUOM.SelectedIndex = 0;
                            }
                            else
                            {
                                this.cmbItemUOM.Enabled = false;
                                this.cmbItemUOM.Text = Convert.ToString(this.dgStockWriteOff.SelectedRows[0].Cells["UNIT"].Value);
                            }
                            this.cmbItemUOM.TabStop = true;
                        }
                    }
                    else
                    {
                        this.cmbItemUOM.Enabled = true;
                        this.cmbItemUOM.TabStop = true;
                        ParaDine.GlobalFill.FillCombo("SELECT UOMID, UOMNAME FROM RES_UOM ORDER BY 2", this.cmbItemUOM);
                        this.cmbItemUOM.SelectedValue = item.UOMID;
                    }
                    if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                    {
                        this.cmbItemUOM.SelectedIndex = 0;
                    }
                    else
                    {
                        this.cmbItemUOM.Text = Convert.ToString(this.dgStockWriteOff.SelectedRows[0].Cells["UNIT"].Value);
                    }
                    if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                    {
                        this.gvItemCostPrice.Value = Convert.ToDecimal(item.CostPrice);
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Item Code", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                e.Cancel = true;
            }
        }

        private void txtRemarks_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) && ParaDine.GlobalValidations.ValidateFields(this.pnlItem, this.TTip))
                {
                    StockWriteOffTrans trans;
                    if (Convert.ToInt32(this.pnlItem.Tag) < 0)
                    {
                        trans = new StockWriteOffTrans
                        {
                            WriteOffID = this.EntId.WriteOffID,
                            WriteOffTransID = 0L
                        };
                        this.LoadTransEntities(trans);
                        trans.Added = true;
                        this.EntId.SWOTransCollection.Add(trans);
                    }
                    else
                    {
                        trans = this.EntId.SWOTransCollection[Convert.ToInt32(this.pnlItem.Tag)];
                        this.LoadTransEntities(trans);
                        trans.Modified = true;
                    }
                    this.pnlItem.Tag = -1;
                    this.PopulateView();
                    if (this.txtItemCode.Enabled)
                    {
                        this.txtItemCode.Focus();
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in ItmCostPrice_KayDown", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
    }
}
