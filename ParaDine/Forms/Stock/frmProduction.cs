﻿using ParaDine.Forms.GlobalForms;
using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Stock
{
    public partial class frmProduction : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private ComboBox cmbItemUOM;
        private ComboBox cmbStockPoint;
        private ComboBox cmbUOM;
       // private IContainer components;
        private DataGridView dgProduction;
        private DateTimePicker dtpProductionDate;
        private Production EntId;
        private NumControl gvConsumedQty;
        private NumControl gvproductionQTy;
        private NumControl gvQtyinHand;
        private NumControl gvTotQty;
        private Label label1;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label LBLID;
        private Panel pnlItem;
        private Panel pnlMain;
        private ToolTip TTip;
        private TextBox txtItemCode;
        private TextBox txtItemName;
        private TextBox TxtItmCode;
        private TextBox txtItmName;

        public frmProduction()
        {
            this.components = null;
            this.EntId = new Production(0L);
            this.InitializeComponent();
        }

        public frmProduction(string Mode, long ID, SecurityClass paramSecurity)
        {
            this.components = null;
            this.EntId = new Production(0L);
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes(ID);
            this.EntId.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (ParaDine.GlobalValidations.ValidateFields(this.pnlMain, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_0089;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_00A5;
                            }
                        }
                        else
                        {
                            ParaDine.GlobalFunctions.ClearFields(this.pnlItem);
                            this.EntId.Add(sqlTrans, true);
                        }
                    }
                    goto Label_00B7;
                Label_0089:
                    ParaDine.GlobalFunctions.ClearFields(this.pnlItem);
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_00B7;
                Label_00A5:
                    this.EntId.Delete(sqlTrans, true);
                Label_00B7:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                    sqlTrans.Rollback();
                }
            }
        }

        private void CalculateTotal()
        {
            this.gvTotQty.Value = Convert.ToDecimal(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgProduction.DataSource, "CONSUMEDQty"));
        }

        private void cmbItemUOM_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if ((this.cmbItemUOM.SelectedIndex != -1) && (this.cmbItemUOM.ValueMember != ""))
                {
                    Item item = new Item(Convert.ToInt64(this.txtItemCode.Tag));
                    this.gvQtyinHand.Value = Convert.ToDecimal((double)(item.GetStockPointStock(Convert.ToInt32(this.cmbItemUOM.SelectedValue), Convert.ToInt32(this.cmbStockPoint.SelectedValue)) - this.EntId.ProdTransCollection.GetPreviousQty(item.ItemID, Convert.ToInt32(this.cmbItemUOM.SelectedValue))));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "ERROR IN CMBITEMUOM_SELECTEDVALUECHANGED");
            }
        }

        private void dgProduction_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                this.pnlItem.Tag = Convert.ToInt16(this.dgProduction.SelectedRows[0].Cells["TableId"].Value) - 1;
                this.LoadTransFields(this.EntId.ProdTransCollection[Convert.ToInt32(this.pnlItem.Tag)]);
                if (this.txtItemCode.Enabled)
                {
                    this.txtItemCode.Focus();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void dgProduction_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    this.EntId.ProdTransCollection[Convert.ToInt16(this.dgProduction.SelectedRows[0].Cells["TableId"].Value) - 1].Deleted = true;
                    this.PopulateView();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    this.dgProduction_CellDoubleClick(sender, new DataGridViewCellEventArgs(0, this.dgProduction.SelectedRows[0].Index));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in dgProduction_KeyDown");
            }
        }



        private void frmProduction_Load(object sender, EventArgs e)
        {
            if (!this.EntId.Security.AllowRead)
            {
                throw new Exception("Permission Denied");
            }
            new GlobalTheme().applyTheme(this);
            ParaDine.GlobalFunctions.AddCompHandler(this.pnlMain);
            this.RefreshData();
            this.LoadFields();
            this.pnlItem.Tag = -1;
        }

        private void gvConsumedQty_KeyDownEvent(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) && ParaDine.GlobalValidations.ValidateFields(this.pnlItem, this.TTip))
                {
                    ProductionTrans trans;
                    this.EntId.ProdTransCollection.StockPointId = Convert.ToInt16(this.cmbStockPoint.SelectedValue);
                    if (Convert.ToInt32(this.pnlItem.Tag) < 0)
                    {
                        trans = new ProductionTrans
                        {
                            ProductionTransID = 0L,
                            Added = true
                        };
                        this.LoadTransEntities(trans);
                        trans.Added = true;
                        this.EntId.ProdTransCollection.Add(trans);
                    }
                    else
                    {
                        trans = this.EntId.ProdTransCollection[Convert.ToInt32(this.pnlItem.Tag)];
                        this.LoadTransEntities(trans);
                        trans.Modified = true;
                    }
                    this.pnlItem.Tag = -1;
                    this.PopulateView();
                    this.txtItemCode.Focus();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in txtRemark_KeyDownEvent");
            }
        }

       

        public void LoadEntities()
        {
            try
            {
                this.EntId.ProductionID = Convert.ToInt64(this.LBLID.Text);
                this.EntId.StockPointID = Convert.ToInt32(this.cmbStockPoint.SelectedValue);
                this.EntId.ProductionDate = Convert.ToDateTime(this.dtpProductionDate.Value);
                this.EntId.ItemID = Convert.ToInt32(this.TxtItmCode.Tag);
                this.EntId.UOMID = Convert.ToInt16(this.cmbUOM.SelectedValue);
                this.EntId.ProductionQty = Convert.ToDouble(this.gvproductionQTy.Value);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        public void LoadFields()
        {
            this.LBLID.Text = Convert.ToString(this.EntId.ProductionID);
            this.cmbStockPoint.SelectedValue = Convert.ToInt64(this.EntId.StockPointID);
            this.dtpProductionDate.Value = Convert.ToDateTime(this.EntId.ProductionDate);
            Item item = new Item((long)this.EntId.ItemID);
            this.TxtItmCode.Text = Convert.ToString(item.ItemCode);
            this.txtItmName.Text = Convert.ToString(item.ItemName);
            this.TxtItmCode_Validating(new object(), new CancelEventArgs());
            this.cmbUOM.SelectedValue = Convert.ToInt32(this.EntId.UOMID);
            this.gvproductionQTy.Value = Convert.ToDecimal(this.EntId.ProductionQty);
            this.PopulateView();
        }

        public void LoadTransEntities(ProductionTrans VarProductionTrans)
        {
            VarProductionTrans.ProductionID = this.EntId.ProductionID;
            VarProductionTrans.ItemID = Convert.ToInt32(this.txtItemCode.Tag);
            VarProductionTrans.UOMID = Convert.ToInt32(this.cmbItemUOM.SelectedValue);
            VarProductionTrans.ConsumedQty = Convert.ToDouble(this.gvConsumedQty.Value);
            this.CalculateTotal();
        }

        public void LoadTransFields(ProductionTrans VarProductionTrans)
        {
            Item item = new Item((long)VarProductionTrans.ItemID);
            this.txtItemCode.Text = Convert.ToString(item.ItemCode);
            this.txtItemName.Text = Convert.ToString(item.ItemName);
            this.txtItemCode_Validating(new object(), new CancelEventArgs());
            this.cmbItemUOM.SelectedValue = Convert.ToInt32(VarProductionTrans.UOMID);
            this.gvConsumedQty.Value = Convert.ToDecimal(VarProductionTrans.ConsumedQty);
        }

        private void PopulateView()
        {
            ParaDine.GlobalFill.FillGridView(this.dgProduction, this.EntId.ProdTransCollection.GetCollectiontable());
            this.dgProduction.Refresh();
            this.dgProduction.AllowUserToAddRows = false;
            this.dgProduction.AllowUserToDeleteRows = false;
            this.dgProduction.AllowUserToOrderColumns = false;
            this.dgProduction.AllowUserToResizeColumns = false;
            this.dgProduction.AllowUserToResizeRows = false;
            this.dgProduction.EditMode = DataGridViewEditMode.EditProgrammatically;
            this.dgProduction.ReadOnly = true;
            this.dgProduction.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgProduction.RowTemplate.ReadOnly = true;
            this.dgProduction.StandardTab = false;
            this.dgProduction.RowsDefaultCellStyle.SelectionBackColor = Color.SeaShell;
            this.SetVwColWidth();
            this.CalculateTotal();
        }

        private void RefreshData()
        {
            ParaDine.GlobalFill.FillCombo("SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT ORDER BY 2", this.cmbStockPoint);
            this.dtpProductionDate.Value = ParaDine.GlobalVariables.BusinessDate;
            ParaDine.GlobalFill.FillCombo("SELECT UOMNAME, UOMID FROM RES_UOM ORDER BY 2", this.cmbUOM);
        }

        private void SetVwColWidth()
        {
            try
            {
                this.dgProduction.Columns[0].Visible = false;
                this.dgProduction.Columns[1].Visible = false;
                this.dgProduction.Columns[2].Width = 80;
                this.dgProduction.Columns[3].Width = 190;
                this.dgProduction.Columns[4].Width = 110;
                for (int i = 5; i < this.dgProduction.Columns.Count; i++)
                {
                    this.dgProduction.Columns[i].Visible = false;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Setting Column Width");
            }
        }

        private void txtItemCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F1)
                {
                    string paramSql = "SELECT * FROM RES_VW_ITEM WHERE NONINVENTORY = 0 OR RAWMATERIAL = 1";
                    new frmBrowseData(ParaDine.GlobalFill.FillDataTable(paramSql), this.txtItemCode, "ITEMS", 0).ShowDialog();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in ItemCode_KeyDown");
            }
        }

        private void txtItemCode_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.txtItemCode.Text.Trim() != "")
                {
                    Item item = new Item(this.txtItemCode.Text);
                    if (!item.NonInventory || item.RawMaterial)
                    {
                        this.txtItemCode.Tag = item.ItemID;
                        this.txtItemCode.Text = item.ItemCode;
                        this.txtItemName.Text = item.ItemName;
                        DataTable uOM = item.GetUOM();
                        if (uOM.Rows.Count > 0)
                        {
                            ParaDine.GlobalFill.FillCombo(uOM, this.cmbItemUOM);
                            if (uOM.Rows.Count == 1)
                            {
                                this.cmbItemUOM.Enabled = false;
                                this.cmbItemUOM.TabStop = false;
                            }
                            else
                            {
                                this.cmbItemUOM.Enabled = true;
                                this.cmbItemUOM.TabStop = true;
                            }
                            this.cmbItemUOM.SelectedIndex = 0;
                        }
                        else
                        {
                            this.cmbItemUOM.Enabled = true;
                            this.cmbItemUOM.TabStop = true;
                            ParaDine.GlobalFill.FillCombo("SELECT UOMID, UOMNAME FROM RES_UOM", this.cmbItemUOM);
                            this.cmbItemUOM.SelectedValue = item.UOMID;
                        }
                        if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                        {
                            this.gvConsumedQty.Value = 1M;
                        }
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "ItemCode Validation");
                e.Cancel = true;
            }
        }

        private void TxtItmCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F1)
                {
                    string paramSql = "SELECT * FROM RES_VW_ITEM ";
                    new frmBrowseData(ParaDine.GlobalFill.FillDataTable(paramSql), this.TxtItmCode, "ITEMS", 0).ShowDialog();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in ItmCode_KeyDown");
            }
        }

        private void TxtItmCode_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.TxtItmCode.Text.Trim() != "")
                {
                    Item item = new Item(this.TxtItmCode.Text);
                    if (!item.NonInventory || item.RawMaterial)
                    {
                        this.TxtItmCode.Tag = item.ItemID;
                        this.TxtItmCode.Text = item.ItemCode;
                        this.txtItmName.Text = item.ItemName;
                        DataTable uOM = item.GetUOM();
                        if (uOM.Rows.Count > 0)
                        {
                            ParaDine.GlobalFill.FillCombo(uOM, this.cmbUOM);
                            if (uOM.Rows.Count == 1)
                            {
                                this.cmbUOM.Enabled = false;
                                this.cmbUOM.TabStop = false;
                            }
                            else
                            {
                                this.cmbUOM.Enabled = true;
                                this.cmbUOM.TabStop = true;
                            }
                            this.cmbUOM.SelectedIndex = 0;
                        }
                        else
                        {
                            this.cmbUOM.Enabled = true;
                            this.cmbUOM.TabStop = true;
                            ParaDine.GlobalFill.FillCombo("SELECT UOMID, UOMNAME FROM RES_UOM", this.cmbUOM);
                            this.cmbUOM.SelectedValue = item.UOMID;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please Check the Item Code");
                        this.TxtItmCode.SelectAll();
                        this.TxtItmCode.Focus();
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in ItmCode_Validating");
            }
        }
    }
}
