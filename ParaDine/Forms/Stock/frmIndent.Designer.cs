﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Stock
{
    partial class frmIndent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PnlMain = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.gvTotAmount = new ParaSysCom.NumControl();
            this.gvTotalQty = new ParaSysCom.NumControl();
            this.label11 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnTrans = new System.Windows.Forms.Button();
            this.pnlItem = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.gvQtyinHand = new ParaSysCom.NumControl();
            this.label10 = new System.Windows.Forms.Label();
            this.gvItemAmount = new ParaSysCom.NumControl();
            this.label12 = new System.Windows.Forms.Label();
            this.gvItemCostPrice = new ParaSysCom.NumControl();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRemark = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.gvItemOrdQty = new ParaSysCom.NumControl();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbItemUOM = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtItemCode = new System.Windows.Forms.TextBox();
            this.dgIndent = new System.Windows.Forms.DataGridView();
            this.cmbStockPoint = new System.Windows.Forms.ComboBox();
            this.dtpIndentDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtIndentNo = new System.Windows.Forms.TextBox();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.lblitem = new System.Windows.Forms.Label();
            this.PnlMain.SuspendLayout();
            this.pnlItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgIndent)).BeginInit();
            this.SuspendLayout();
            // 
            // PnlMain
            // 
            this.PnlMain.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.PnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlMain.Controls.Add(this.label9);
            this.PnlMain.Controls.Add(this.gvTotAmount);
            this.PnlMain.Controls.Add(this.gvTotalQty);
            this.PnlMain.Controls.Add(this.label11);
            this.PnlMain.Controls.Add(this.btnExit);
            this.PnlMain.Controls.Add(this.btnTrans);
            this.PnlMain.Controls.Add(this.pnlItem);
            this.PnlMain.Controls.Add(this.dgIndent);
            this.PnlMain.Controls.Add(this.cmbStockPoint);
            this.PnlMain.Controls.Add(this.dtpIndentDate);
            this.PnlMain.Controls.Add(this.label3);
            this.PnlMain.Controls.Add(this.label2);
            this.PnlMain.Controls.Add(this.label1);
            this.PnlMain.Controls.Add(this.txtIndentNo);
            this.PnlMain.Location = new System.Drawing.Point(6, 6);
            this.PnlMain.Name = "PnlMain";
            this.PnlMain.Size = new System.Drawing.Size(518, 397);
            this.PnlMain.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(292, 372);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 13);
            this.label9.TabIndex = 34;
            this.label9.Text = "Total Amount :";
            // 
            // gvTotAmount
            // 
            this.gvTotAmount.DecimalRequired = true;
            this.gvTotAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvTotAmount.Location = new System.Drawing.Point(388, 366);
            this.gvTotAmount.Name = "gvTotAmount";
            this.gvTotAmount.Size = new System.Drawing.Size(115, 20);
            this.gvTotAmount.SymbolRequired = true;
            this.gvTotAmount.TabIndex = 33;
            this.gvTotAmount.TabStop = false;
            this.gvTotAmount.Text = "`0.00";
            this.gvTotAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvTotAmount, "Displays Amount");
            this.gvTotAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // gvTotalQty
            // 
            this.gvTotalQty.DecimalRequired = true;
            this.gvTotalQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvTotalQty.Location = new System.Drawing.Point(388, 335);
            this.gvTotalQty.Name = "gvTotalQty";
            this.gvTotalQty.Size = new System.Drawing.Size(115, 20);
            this.gvTotalQty.SymbolRequired = false;
            this.gvTotalQty.TabIndex = 26;
            this.gvTotalQty.Text = "0.00";
            this.gvTotalQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvTotalQty, "Displays Total Quantity");
            this.gvTotalQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(315, 342);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Total Qty :";
            // 
            // btnExit
            // 
            this.btnExit.CausesValidation = false;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(88, 365);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 23;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(7, 365);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 22;
            this.btnTrans.Text = "Trans";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // pnlItem
            // 
            this.pnlItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlItem.Controls.Add(this.label25);
            this.pnlItem.Controls.Add(this.gvQtyinHand);
            this.pnlItem.Controls.Add(this.label10);
            this.pnlItem.Controls.Add(this.gvItemAmount);
            this.pnlItem.Controls.Add(this.label12);
            this.pnlItem.Controls.Add(this.gvItemCostPrice);
            this.pnlItem.Controls.Add(this.label4);
            this.pnlItem.Controls.Add(this.txtRemark);
            this.pnlItem.Controls.Add(this.label8);
            this.pnlItem.Controls.Add(this.gvItemOrdQty);
            this.pnlItem.Controls.Add(this.label7);
            this.pnlItem.Controls.Add(this.cmbItemUOM);
            this.pnlItem.Controls.Add(this.label6);
            this.pnlItem.Controls.Add(this.txtItemName);
            this.pnlItem.Controls.Add(this.label5);
            this.pnlItem.Controls.Add(this.txtItemCode);
            this.pnlItem.Location = new System.Drawing.Point(7, 222);
            this.pnlItem.Name = "pnlItem";
            this.pnlItem.Size = new System.Drawing.Size(504, 111);
            this.pnlItem.TabIndex = 3;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(190, 10);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(63, 13);
            this.label25.TabIndex = 40;
            this.label25.Text = "Qty in Hand";
            // 
            // gvQtyinHand
            // 
            this.gvQtyinHand.DecimalRequired = true;
            this.gvQtyinHand.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvQtyinHand.Location = new System.Drawing.Point(191, 26);
            this.gvQtyinHand.Name = "gvQtyinHand";
            this.gvQtyinHand.Size = new System.Drawing.Size(68, 20);
            this.gvQtyinHand.SymbolRequired = false;
            this.gvQtyinHand.TabIndex = 39;
            this.gvQtyinHand.Text = "0.00";
            this.gvQtyinHand.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvQtyinHand, "Enter Free Qty ");
            this.gvQtyinHand.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(417, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 34;
            this.label10.Text = "Amount";
            // 
            // gvItemAmount
            // 
            this.gvItemAmount.DecimalRequired = true;
            this.gvItemAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemAmount.Location = new System.Drawing.Point(411, 26);
            this.gvItemAmount.Name = "gvItemAmount";
            this.gvItemAmount.Size = new System.Drawing.Size(85, 20);
            this.gvItemAmount.SymbolRequired = true;
            this.gvItemAmount.TabIndex = 32;
            this.gvItemAmount.TabStop = false;
            this.gvItemAmount.Text = "`0.00";
            this.gvItemAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvItemAmount, "Displays Amount");
            this.gvItemAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(327, 10);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 33;
            this.label12.Text = "Cost Price";
            // 
            // gvItemCostPrice
            // 
            this.gvItemCostPrice.DecimalRequired = true;
            this.gvItemCostPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemCostPrice.Location = new System.Drawing.Point(327, 26);
            this.gvItemCostPrice.Name = "gvItemCostPrice";
            this.gvItemCostPrice.Size = new System.Drawing.Size(81, 20);
            this.gvItemCostPrice.SymbolRequired = true;
            this.gvItemCostPrice.TabIndex = 3;
            this.gvItemCostPrice.Tag = "";
            this.gvItemCostPrice.Text = "`0.00";
            this.gvItemCostPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvItemCostPrice, "Displays CostPrice");
            this.gvItemCostPrice.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvItemCostPrice.TextChanged += new System.EventHandler(this.gvItemCostPrice_Change);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(247, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Remark :";
            // 
            // txtRemark
            // 
            this.txtRemark.BackColor = System.Drawing.Color.White;
            this.txtRemark.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRemark.Location = new System.Drawing.Point(254, 75);
            this.txtRemark.Multiline = true;
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.Size = new System.Drawing.Size(244, 20);
            this.txtRemark.TabIndex = 4;
            this.txtRemark.Tag = "NoHandler";
            this.TTip.SetToolTip(this.txtRemark, "Enter Remarks");
            this.txtRemark.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRemark_KeyDown);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(261, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Ord Qty :";
            // 
            // gvItemOrdQty
            // 
            this.gvItemOrdQty.DecimalRequired = true;
            this.gvItemOrdQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemOrdQty.Location = new System.Drawing.Point(263, 26);
            this.gvItemOrdQty.Name = "gvItemOrdQty";
            this.gvItemOrdQty.Size = new System.Drawing.Size(59, 20);
            this.gvItemOrdQty.SymbolRequired = false;
            this.gvItemOrdQty.TabIndex = 2;
            this.gvItemOrdQty.Text = "0.00";
            this.gvItemOrdQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvItemOrdQty, "Enter Order Qty");
            this.gvItemOrdQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvItemOrdQty.TextChanged += new System.EventHandler(this.gvItemOrdQty_Change);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(83, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "UOM :";
            // 
            // cmbItemUOM
            // 
            this.cmbItemUOM.BackColor = System.Drawing.Color.Ivory;
            this.cmbItemUOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbItemUOM.FormattingEnabled = true;
            this.cmbItemUOM.Location = new System.Drawing.Point(80, 26);
            this.cmbItemUOM.Name = "cmbItemUOM";
            this.cmbItemUOM.Size = new System.Drawing.Size(107, 21);
            this.cmbItemUOM.TabIndex = 1;
            this.TTip.SetToolTip(this.cmbItemUOM, "Select UOM");
            this.cmbItemUOM.SelectionChangeCommitted += new System.EventHandler(this.cmbItemUOM_SelectionChangeCommitted);
            this.cmbItemUOM.SelectedValueChanged += new System.EventHandler(this.cmbItemUOM_SelectedValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Item Name :";
            // 
            // txtItemName
            // 
            this.txtItemName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemName.Location = new System.Drawing.Point(6, 75);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(237, 20);
            this.txtItemName.TabIndex = 20;
            this.txtItemName.TabStop = false;
            this.txtItemName.Tag = "";
            this.TTip.SetToolTip(this.txtItemName, "Displays Item Name");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Item Code :";
            // 
            // txtItemCode
            // 
            this.txtItemCode.BackColor = System.Drawing.Color.Ivory;
            this.txtItemCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemCode.Location = new System.Drawing.Point(5, 26);
            this.txtItemCode.Name = "txtItemCode";
            this.txtItemCode.Size = new System.Drawing.Size(68, 20);
            this.txtItemCode.TabIndex = 0;
            this.TTip.SetToolTip(this.txtItemCode, "Enter Item Code");
            this.txtItemCode.Enter += new System.EventHandler(this.txtItemCode_Enter);
            this.txtItemCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItemCode_KeyDown);
            this.txtItemCode.Leave += new System.EventHandler(this.txtItemCode_Leave);
            this.txtItemCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtItemCode_Validating);
            // 
            // dgIndent
            // 
            this.dgIndent.BackgroundColor = System.Drawing.Color.White;
            this.dgIndent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgIndent.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgIndent.Location = new System.Drawing.Point(7, 66);
            this.dgIndent.Name = "dgIndent";
            this.dgIndent.RowHeadersVisible = false;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.SeaShell;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.dgIndent.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgIndent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgIndent.Size = new System.Drawing.Size(495, 150);
            this.dgIndent.TabIndex = 9;
            this.dgIndent.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgIndent_CellDoubleClick);
            this.dgIndent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgIndent_KeyDown);
            // 
            // cmbStockPoint
            // 
            this.cmbStockPoint.FormattingEnabled = true;
            this.cmbStockPoint.Location = new System.Drawing.Point(95, 37);
            this.cmbStockPoint.Name = "cmbStockPoint";
            this.cmbStockPoint.Size = new System.Drawing.Size(127, 21);
            this.cmbStockPoint.TabIndex = 2;
            this.TTip.SetToolTip(this.cmbStockPoint, "Select Stock Point");
            // 
            // dtpIndentDate
            // 
            this.dtpIndentDate.CustomFormat = "dd/MMM/yy";
            this.dtpIndentDate.Enabled = false;
            this.dtpIndentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpIndentDate.Location = new System.Drawing.Point(388, 11);
            this.dtpIndentDate.Name = "dtpIndentDate";
            this.dtpIndentDate.Size = new System.Drawing.Size(100, 20);
            this.dtpIndentDate.TabIndex = 1;
            this.dtpIndentDate.TabStop = false;
            this.TTip.SetToolTip(this.dtpIndentDate, "Displays Indent Date");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Indent For";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(313, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Indent Date :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Indent No. :";
            // 
            // txtIndentNo
            // 
            this.txtIndentNo.Location = new System.Drawing.Point(95, 11);
            this.txtIndentNo.Name = "txtIndentNo";
            this.txtIndentNo.Size = new System.Drawing.Size(127, 20);
            this.txtIndentNo.TabIndex = 0;
            this.TTip.SetToolTip(this.txtIndentNo, "Enter Indent Number");
            // 
            // lblitem
            // 
            this.lblitem.AutoSize = true;
            this.lblitem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblitem.ForeColor = System.Drawing.Color.Red;
            this.lblitem.Location = new System.Drawing.Point(185, 408);
            this.lblitem.Name = "lblitem";
            this.lblitem.Size = new System.Drawing.Size(173, 13);
            this.lblitem.TabIndex = 38;
            this.lblitem.Text = "Press F1 For Listing all items.";
            this.lblitem.Visible = false;
            // 
            // frmIndent
            // 
            this.ClientSize = new System.Drawing.Size(526, 423);
            this.Controls.Add(this.lblitem);
            this.Controls.Add(this.PnlMain);
            this.Name = "frmIndent";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Indent";
            this.Load += new System.EventHandler(this.frmIndent_Load);
            this.PnlMain.ResumeLayout(false);
            this.PnlMain.PerformLayout();
            this.pnlItem.ResumeLayout(false);
            this.pnlItem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgIndent)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}