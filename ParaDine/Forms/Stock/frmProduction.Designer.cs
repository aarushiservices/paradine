﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Stock
{
    partial class frmProduction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.txtItmName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtItmCode = new System.Windows.Forms.TextBox();
            this.gvTotQty = new ParaSysCom.NumControl();
            this.label11 = new System.Windows.Forms.Label();
            this.pnlItem = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.gvQtyinHand = new ParaSysCom.NumControl();
            this.label8 = new System.Windows.Forms.Label();
            this.gvConsumedQty = new ParaSysCom.NumControl();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbItemUOM = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtItemCode = new System.Windows.Forms.TextBox();
            this.gvproductionQTy = new ParaSysCom.NumControl();
            this.LBLID = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbUOM = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbStockPoint = new System.Windows.Forms.ComboBox();
            this.dtpProductionDate = new System.Windows.Forms.DateTimePicker();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnTrans = new System.Windows.Forms.Button();
            this.dgProduction = new System.Windows.Forms.DataGridView();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.pnlMain.SuspendLayout();
            this.pnlItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProduction)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMain.Controls.Add(this.label9);
            this.pnlMain.Controls.Add(this.txtItmName);
            this.pnlMain.Controls.Add(this.label10);
            this.pnlMain.Controls.Add(this.TxtItmCode);
            this.pnlMain.Controls.Add(this.gvTotQty);
            this.pnlMain.Controls.Add(this.label11);
            this.pnlMain.Controls.Add(this.pnlItem);
            this.pnlMain.Controls.Add(this.gvproductionQTy);
            this.pnlMain.Controls.Add(this.LBLID);
            this.pnlMain.Controls.Add(this.label4);
            this.pnlMain.Controls.Add(this.cmbUOM);
            this.pnlMain.Controls.Add(this.label1);
            this.pnlMain.Controls.Add(this.label3);
            this.pnlMain.Controls.Add(this.label2);
            this.pnlMain.Controls.Add(this.cmbStockPoint);
            this.pnlMain.Controls.Add(this.dtpProductionDate);
            this.pnlMain.Controls.Add(this.btnExit);
            this.pnlMain.Controls.Add(this.btnTrans);
            this.pnlMain.Controls.Add(this.dgProduction);
            this.pnlMain.Location = new System.Drawing.Point(3, 3);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(520, 349);
            this.pnlMain.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(186, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 36;
            this.label9.Text = "Item Name :";
            // 
            // txtItmName
            // 
            this.txtItmName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItmName.Location = new System.Drawing.Point(189, 63);
            this.txtItmName.Name = "txtItmName";
            this.txtItmName.Size = new System.Drawing.Size(281, 20);
            this.txtItmName.TabIndex = 5;
            this.txtItmName.TabStop = false;
            this.txtItmName.Tag = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(158, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 35;
            this.label10.Text = "Item Code";
            // 
            // TxtItmCode
            // 
            this.TxtItmCode.BackColor = System.Drawing.Color.Ivory;
            this.TxtItmCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtItmCode.Location = new System.Drawing.Point(156, 23);
            this.TxtItmCode.Name = "TxtItmCode";
            this.TxtItmCode.Size = new System.Drawing.Size(82, 20);
            this.TxtItmCode.TabIndex = 2;
            this.TxtItmCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtItmCode_KeyDown);
            this.TxtItmCode.Validating += new System.ComponentModel.CancelEventHandler(this.TxtItmCode_Validating);
            // 
            // gvTotQty
            // 
            this.gvTotQty.DecimalRequired = true;
            this.gvTotQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvTotQty.Location = new System.Drawing.Point(371, 315);
            this.gvTotQty.Name = "gvTotQty";
            this.gvTotQty.Size = new System.Drawing.Size(134, 20);
            this.gvTotQty.SymbolRequired = true;
            this.gvTotQty.TabIndex = 32;
            this.gvTotQty.Text = "`0.00";
            this.gvTotQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvTotQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(298, 321);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 31;
            this.label11.Text = "Total Qty :";
            // 
            // pnlItem
            // 
            this.pnlItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlItem.Controls.Add(this.label12);
            this.pnlItem.Controls.Add(this.gvQtyinHand);
            this.pnlItem.Controls.Add(this.label8);
            this.pnlItem.Controls.Add(this.gvConsumedQty);
            this.pnlItem.Controls.Add(this.label7);
            this.pnlItem.Controls.Add(this.cmbItemUOM);
            this.pnlItem.Controls.Add(this.label6);
            this.pnlItem.Controls.Add(this.txtItemName);
            this.pnlItem.Controls.Add(this.label5);
            this.pnlItem.Controls.Add(this.txtItemCode);
            this.pnlItem.Location = new System.Drawing.Point(5, 235);
            this.pnlItem.Name = "pnlItem";
            this.pnlItem.Size = new System.Drawing.Size(508, 72);
            this.pnlItem.TabIndex = 7;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(270, 4);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(31, 13);
            this.label12.TabIndex = 30;
            this.label12.Text = "QOH";
            // 
            // gvQtyinHand
            // 
            this.gvQtyinHand.DecimalRequired = true;
            this.gvQtyinHand.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvQtyinHand.Location = new System.Drawing.Point(267, 19);
            this.gvQtyinHand.Name = "gvQtyinHand";
            this.gvQtyinHand.Size = new System.Drawing.Size(76, 20);
            this.gvQtyinHand.SymbolRequired = false;
            this.gvQtyinHand.TabIndex = 29;
            this.gvQtyinHand.Tag = "NoHandler";
            this.gvQtyinHand.Text = "0.00";
            this.gvQtyinHand.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvQtyinHand.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(375, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Consumed Qty";
            // 
            // gvConsumedQty
            // 
            this.gvConsumedQty.DecimalRequired = true;
            this.gvConsumedQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvConsumedQty.Location = new System.Drawing.Point(372, 20);
            this.gvConsumedQty.Name = "gvConsumedQty";
            this.gvConsumedQty.Size = new System.Drawing.Size(108, 20);
            this.gvConsumedQty.SymbolRequired = false;
            this.gvConsumedQty.TabIndex = 2;
            this.gvConsumedQty.Tag = "NoHandler";
            this.gvConsumedQty.Text = "0.00";
            this.gvConsumedQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvConsumedQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvConsumedQty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gvConsumedQty_KeyDownEvent);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(138, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "UOM";
            // 
            // cmbItemUOM
            // 
            this.cmbItemUOM.BackColor = System.Drawing.Color.Ivory;
            this.cmbItemUOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbItemUOM.FormattingEnabled = true;
            this.cmbItemUOM.Location = new System.Drawing.Point(135, 20);
            this.cmbItemUOM.Name = "cmbItemUOM";
            this.cmbItemUOM.Size = new System.Drawing.Size(103, 21);
            this.cmbItemUOM.TabIndex = 1;
            this.cmbItemUOM.SelectedValueChanged += new System.EventHandler(this.cmbItemUOM_SelectedValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Item Name :";
            // 
            // txtItemName
            // 
            this.txtItemName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemName.Location = new System.Drawing.Point(88, 47);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(401, 20);
            this.txtItemName.TabIndex = 3;
            this.txtItemName.TabStop = false;
            this.txtItemName.Tag = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Item Code";
            // 
            // txtItemCode
            // 
            this.txtItemCode.BackColor = System.Drawing.Color.Ivory;
            this.txtItemCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemCode.Location = new System.Drawing.Point(24, 20);
            this.txtItemCode.Name = "txtItemCode";
            this.txtItemCode.Size = new System.Drawing.Size(82, 20);
            this.txtItemCode.TabIndex = 0;
            this.txtItemCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItemCode_KeyDown);
            this.txtItemCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtItemCode_Validating);
            // 
            // gvproductionQTy
            // 
            this.gvproductionQTy.DecimalRequired = true;
            this.gvproductionQTy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvproductionQTy.Location = new System.Drawing.Point(424, 23);
            this.gvproductionQTy.Name = "gvproductionQTy";
            this.gvproductionQTy.Size = new System.Drawing.Size(75, 20);
            this.gvproductionQTy.SymbolRequired = false;
            this.gvproductionQTy.TabIndex = 6;
            this.gvproductionQTy.Text = "0.00";
            this.gvproductionQTy.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvproductionQTy.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // LBLID
            // 
            this.LBLID.AutoSize = true;
            this.LBLID.Location = new System.Drawing.Point(478, 66);
            this.LBLID.Name = "LBLID";
            this.LBLID.Size = new System.Drawing.Size(35, 13);
            this.LBLID.TabIndex = 28;
            this.LBLID.Text = "label1";
            this.LBLID.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(273, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "UOM";
            // 
            // cmbUOM
            // 
            this.cmbUOM.BackColor = System.Drawing.Color.Ivory;
            this.cmbUOM.FormattingEnabled = true;
            this.cmbUOM.Location = new System.Drawing.Point(267, 23);
            this.cmbUOM.Name = "cmbUOM";
            this.cmbUOM.Size = new System.Drawing.Size(128, 21);
            this.cmbUOM.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(426, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Prod Qty";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Stock Point";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Production Date";
            // 
            // cmbStockPoint
            // 
            this.cmbStockPoint.BackColor = System.Drawing.Color.Ivory;
            this.cmbStockPoint.FormattingEnabled = true;
            this.cmbStockPoint.Location = new System.Drawing.Point(19, 63);
            this.cmbStockPoint.Name = "cmbStockPoint";
            this.cmbStockPoint.Size = new System.Drawing.Size(164, 21);
            this.cmbStockPoint.TabIndex = 1;
            // 
            // dtpProductionDate
            // 
            this.dtpProductionDate.CustomFormat = "dd/MMM/yy";
            this.dtpProductionDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpProductionDate.Location = new System.Drawing.Point(19, 23);
            this.dtpProductionDate.Name = "dtpProductionDate";
            this.dtpProductionDate.Size = new System.Drawing.Size(108, 20);
            this.dtpProductionDate.TabIndex = 0;
            // 
            // btnExit
            // 
            this.btnExit.CausesValidation = false;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(95, 316);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 9;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(14, 316);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 8;
            this.btnTrans.Text = "Trans";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // dgProduction
            // 
            this.dgProduction.BackgroundColor = System.Drawing.Color.White;
            this.dgProduction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProduction.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgProduction.Location = new System.Drawing.Point(5, 90);
            this.dgProduction.Name = "dgProduction";
            this.dgProduction.RowHeadersVisible = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.SeaShell;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgProduction.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgProduction.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgProduction.Size = new System.Drawing.Size(508, 142);
            this.dgProduction.TabIndex = 12;
            this.dgProduction.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgProduction_CellDoubleClick);
            this.dgProduction.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgProduction_KeyDown);
            // 
            // frmProduction
            // 
            this.ClientSize = new System.Drawing.Size(526, 354);
            this.Controls.Add(this.pnlMain);
            this.Name = "frmProduction";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Production";
            this.Load += new System.EventHandler(this.frmProduction_Load);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.pnlItem.ResumeLayout(false);
            this.pnlItem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProduction)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
    }
}