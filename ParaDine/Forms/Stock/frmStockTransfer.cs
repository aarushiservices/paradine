﻿using ParaDine.Forms.GlobalForms;
using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Stock
{
    public partial class frmStockTransfer : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private ComboBox cmbfrStockPoint;
        private ComboBox cmbItemUOM;
        private ComboBox cmbToStockPoint;
       // private IContainer components;
        private DataGridView dgStkTransfer;
        private DateTimePicker dtpTrfDate;
        private StockTrfMaster EntId;
        private NumControl gvItemAmount;
        private NumControl gvItemCostPrice;
        private NumControl gvItemTransQty;
        private NumControl gvQtyinHand;
        private NumControl gvTotalQty;
        private NumControl gvTotAmount;
        private Label label1;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label13;
        private Label label14;
        private Label label2;
        private Label label25;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label lblitem;
        private Panel pnlItem;
        private Panel PnlStockTrf;
        private ToolTip TTip;
        private TextBox txtIndentNo;
        private TextBox txtItemCode;
        private TextBox txtItemName;
        private TextBox txtRemark;
        private TextBox txtTrfNo;
        private Level uLevel;

        public frmStockTransfer()
        {
            this.components = null;
            this.EntId = new StockTrfMaster(0L);
            this.InitializeComponent();
        }

        public frmStockTransfer(string Mode, long ID, SecurityClass paramSecurity)
        {
            this.components = null;
            this.EntId = new StockTrfMaster(0L);
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes(ID);
            this.EntId.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (ParaDine.GlobalValidations.ValidateFields(this.PnlStockTrf, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_0089;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_0099;
                            }
                        }
                        else
                        {
                            ParaDine.GlobalFunctions.ClearFields(this.PnlStockTrf);
                            this.EntId.Add(sqlTrans, true);
                        }
                    }
                    goto Label_00AB;
                Label_0089:
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_00AB;
                Label_0099:
                    this.EntId.Delete(sqlTrans, true);
                Label_00AB:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                    sqlTrans.Rollback();
                }
            }
        }

        private void CalculateItemTotal()
        {
            this.gvItemAmount.Value = Convert.ToDecimal((double)(Convert.ToDouble(this.gvItemCostPrice.Value) * Convert.ToDouble(this.gvItemTransQty.Value)));
        }

        private void CalculateTotal()
        {
            this.gvTotalQty.Value = Convert.ToDecimal(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgStkTransfer.DataSource, "Qty"));
            this.gvTotAmount.Value = Convert.ToDecimal(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgStkTransfer.DataSource, "Amount"));
        }

        private void cmbfrStockPoint_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ParaDine.GlobalFill.FillCombo("SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT WHERE STOCKPOINTID <> " + Convert.ToInt32(this.cmbfrStockPoint.SelectedValue).ToString(), this.cmbToStockPoint);
        }

        private void cmbItemUOM_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if ((this.cmbItemUOM.SelectedIndex != -1) && (this.cmbItemUOM.ValueMember != ""))
                {
                    Item item = new Item(Convert.ToInt64(this.txtItemCode.Tag));
                    this.gvItemCostPrice.Value = Convert.ToDecimal(item.GetCostPrice(Convert.ToInt32(this.cmbItemUOM.SelectedValue)));
                    this.gvQtyinHand.Value = Convert.ToDecimal((double)(item.GetStockPointStock(Convert.ToInt32(this.cmbItemUOM.SelectedValue), Convert.ToInt32(this.cmbfrStockPoint.SelectedValue)) - this.EntId.StkTrfTransCollection.GetPreviousQty(item.ItemID, Convert.ToInt32(this.cmbItemUOM.SelectedValue))));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "ERROR IN CMBITEMUOM_SELECTEDVALUECHANGED");
            }
        }

        private void dgStkTransfer_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgStkTransfer.SelectedRows.Count > 0)
            {
                this.pnlItem.Tag = Convert.ToInt16(this.dgStkTransfer.SelectedRows[0].Cells["TableId"].Value) - 1;
                this.LoadTransFields(this.EntId.StkTrfTransCollection[Convert.ToInt32(this.pnlItem.Tag)]);
                if (this.txtItemCode.Enabled)
                {
                    this.txtItemCode.Focus();
                }
            }
        }

        private void dgStkTransfer_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    this.EntId.StkTrfTransCollection[Convert.ToInt16(this.dgStkTransfer.SelectedRows[0].Cells["TableId"].Value) - 1].Deleted = true;
                    this.PopulateView();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    this.dgStkTransfer_CellDoubleClick(sender, new DataGridViewCellEventArgs(0, this.dgStkTransfer.SelectedRows[0].Index));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in dgKitToStore_KeyDown");
            }
        }

        

        private void FrmStockTransfer_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                this.uLevel = new Level(new User(ParaDine.GlobalVariables.UserID).LevelID);
                new GlobalTheme().applyTheme(this);
                ParaDine.GlobalFunctions.AddCompHandler(this.PnlStockTrf);
                this.RefreshData();
                this.LoadFields();
                this.PnlStockTrf.Tag = -1;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Form Load");
                base.Close();
            }
        }

        private void gvItemCostPrice_Change(object sender, EventArgs e)
        {
            this.CalculateItemTotal();
        }

        private void gvItemTransQty_Change(object sender, EventArgs e)
        {
            this.CalculateItemTotal();
        }

        private void gvTot_KeyPressEvent(object sender, KeyPressEventArgs e)
        {
        }

       

        private void LoadEntities()
        {
            this.EntId.StockTrfMasterId = Convert.ToInt64(this.txtTrfNo.Tag);
            this.EntId.StockTrfNumber = Convert.ToString(this.txtTrfNo.Text);
            this.EntId.StockTrfDate = Convert.ToDateTime(this.dtpTrfDate.Value);
            this.EntId.FrStockPointId = Convert.ToInt32(this.cmbfrStockPoint.SelectedValue);
            this.EntId.ToStockPointId = Convert.ToInt32(this.cmbToStockPoint.SelectedValue);
            this.EntId.IndentMasterId = Convert.ToInt64(this.txtIndentNo.Tag);
        }

        private void LoadFields()
        {
            if (this.EntId.StockTrfMasterId <= 0L)
            {
                this.txtTrfNo.Text = this.EntId.GetMaxCode;
                this.txtTrfNo.ReadOnly = true;
            }
            else
            {
                this.txtTrfNo.Text = Convert.ToString(this.EntId.StockTrfNumber);
                this.txtTrfNo.Tag = this.EntId.StockTrfMasterId;
            }
            this.dtpTrfDate.Value = Convert.ToDateTime(this.EntId.StockTrfDate);
            this.cmbfrStockPoint.SelectedValue = Convert.ToInt32(this.EntId.FrStockPointId);
            this.cmbfrStockPoint_SelectionChangeCommitted(new object(), new EventArgs());
            this.cmbToStockPoint.SelectedValue = Convert.ToInt32(this.EntId.ToStockPointId);
            if (this.EntId.IndentMasterId > 0L)
            {
                this.txtIndentNo.Text = new IndentMaster(Convert.ToInt64(this.EntId.IndentMasterId)).IndentMasterNo;
            }
            this.PopulateView();
        }

        private void LoadTransEntities(StockTrfTrans varStkTrfTrans)
        {
            varStkTrfTrans.StockTrfMasterId = this.EntId.StockTrfMasterId;
            varStkTrfTrans.ItemId = Convert.ToInt64(this.txtItemCode.Tag);
            varStkTrfTrans.UOMId = Convert.ToInt32(this.cmbItemUOM.SelectedValue);
            varStkTrfTrans.TransferQty = Convert.ToDouble(this.gvItemTransQty.Value);
            varStkTrfTrans.CostPrice = Convert.ToDouble(this.gvItemCostPrice.Value);
            varStkTrfTrans.StockPointId = Convert.ToInt32(this.cmbfrStockPoint.SelectedValue);
        }

        private void LoadTransFields(StockTrfTrans VarStkTrfTrans)
        {
            Item item = new Item(VarStkTrfTrans.ItemId);
            this.txtItemCode.Text = Convert.ToString(item.ItemCode);
            this.txtItemName.Text = Convert.ToString(item.ItemName);
            this.txtItemCode_Validating(new object(), new CancelEventArgs());
            this.cmbItemUOM.SelectedValue = Convert.ToInt16(VarStkTrfTrans.UOMId);
            this.gvItemTransQty.Value = Convert.ToDecimal(VarStkTrfTrans.TransferQty);
            this.gvItemCostPrice.Value = Convert.ToDecimal(VarStkTrfTrans.CostPrice);
            this.gvItemAmount.Value = Convert.ToDecimal(VarStkTrfTrans.GetAmount());
        }

        private void PopulateView()
        {
            ParaDine.GlobalFill.FillGridView(this.dgStkTransfer, this.EntId.StkTrfTransCollection.GetCollectiontable());
            this.dgStkTransfer.Refresh();
            this.dgStkTransfer.AllowUserToAddRows = false;
            this.dgStkTransfer.AllowUserToDeleteRows = false;
            this.dgStkTransfer.AllowUserToOrderColumns = false;
            this.dgStkTransfer.AllowUserToResizeColumns = false;
            this.dgStkTransfer.AllowUserToResizeRows = false;
            this.dgStkTransfer.EditMode = DataGridViewEditMode.EditProgrammatically;
            this.dgStkTransfer.ReadOnly = true;
            this.dgStkTransfer.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgStkTransfer.RowTemplate.ReadOnly = true;
            this.dgStkTransfer.StandardTab = false;
            this.dgStkTransfer.RowsDefaultCellStyle.SelectionBackColor = Color.SeaShell;
            this.SetVwColWidth();
            this.CalculateTotal();
        }

        private void RefreshData()
        {
            try
            {
                if (this.uLevel.IsAdmin)
                {
                    this.dtpTrfDate.Enabled = true;
                }
                ParaDine.GlobalFill.FillCombo("SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT", this.cmbfrStockPoint);
                this.dtpTrfDate.Value = ParaDine.GlobalVariables.BusinessDate;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In RefreshData");
            }
        }

        private void SetVwColWidth()
        {
            try
            {
                this.dgStkTransfer.Columns[0].Visible = false;
                this.dgStkTransfer.Columns[1].Visible = false;
                this.dgStkTransfer.Columns[2].Width = 150;
                this.dgStkTransfer.Columns[3].Width = 100;
                this.dgStkTransfer.Columns[4].Width = 60;
                this.dgStkTransfer.Columns[5].Width = 80;
                this.dgStkTransfer.Columns[6].Width = 80;
                this.dgStkTransfer.Columns[7].Width = 100;
                this.dgStkTransfer.Columns["TABLEID"].Visible = false;
                for (int i = 8; i < this.dgStkTransfer.Columns.Count; i++)
                {
                    this.dgStkTransfer.Columns[i].Visible = false;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Setting Column Width");
            }
        }

        private void txtIndentNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                string paramSql = "SELECT * FROM RES_INDENTMASTER WHERE INDENTMASTERID NOT IN (SELECT ISNULL(INDENTMASTERID, 0) FROM RES_STOCKTRFMASTER)";
                new frmBrowseData(ParaDine.GlobalFill.FillDataTable(paramSql), this.txtIndentNo, "INDENTS", 2).ShowDialog();
            }
        }

        private void txtIndentNo_Validating(object sender, CancelEventArgs e)
        {
            if (this.txtIndentNo.Text != "")
            {
                IndentMaster master = new IndentMaster(this.txtIndentNo.Text);
                if (master.IndentMasterID > 0L)
                {
                    this.txtIndentNo.Tag = master.IndentMasterID;
                }
                else
                {
                    e.Cancel = true;
                    MessageBox.Show("Indent Not Found");
                }
            }
        }

        private void txtItemCode_Enter(object sender, EventArgs e)
        {
            this.lblitem.Visible = true;
        }

        private void txtItemCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F1)
                {
                    string paramSql = "SELECT * FROM RES_VW_ITEM WHERE NONINVENTORY = 0 ";
                    new frmBrowseData(ParaDine.GlobalFill.FillDataTable(paramSql), this.txtItemCode, "ITEMS", 2).ShowDialog();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in ItemCode_KeyDown");
            }
        }

        private void txtItemCode_Leave(object sender, EventArgs e)
        {
            this.lblitem.Visible = false;
        }

        private void txtItemCode_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.txtItemCode.Text.Trim() != "")
                {
                    Item item = new Item(this.txtItemCode.Text);
                    if (item.NonInventory && item.RawMaterial)
                    {
                        this.txtItemCode.Tag = item.ItemID;
                        this.txtItemCode.Text = item.ItemCode;
                        this.txtItemName.Text = item.ItemName;
                        DataTable uOM = item.GetUOM();
                        if (uOM.Rows.Count > 0)
                        {
                            ParaDine.GlobalFill.FillCombo(uOM, this.cmbItemUOM);
                            if (uOM.Rows.Count == 1)
                            {
                                this.cmbItemUOM.Enabled = false;
                                this.cmbItemUOM.TabStop = false;
                                if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                                {
                                    this.cmbItemUOM.SelectedIndex = 0;
                                }
                            }
                            else
                            {
                                if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                                {
                                    this.cmbItemUOM.Enabled = true;
                                    this.cmbItemUOM.TabStop = true;
                                    this.cmbItemUOM.SelectedIndex = 0;
                                }
                                else
                                {
                                    this.cmbItemUOM.Enabled = false;
                                    this.cmbItemUOM.Text = Convert.ToString(this.dgStkTransfer.SelectedRows[0].Cells["UOM"].Value);
                                }
                                this.cmbItemUOM.TabStop = true;
                            }
                        }
                        else
                        {
                            this.cmbItemUOM.Enabled = true;
                            this.cmbItemUOM.TabStop = true;
                            ParaDine.GlobalFill.FillCombo("SELECT UOMID, UOMNAME FROM RES_UOM", this.cmbItemUOM);
                            this.cmbItemUOM.SelectedValue = item.UOMID;
                            if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                            {
                                this.gvItemCostPrice.Value = Convert.ToDecimal(item.CostPrice);
                            }
                        }
                        if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                        {
                            this.gvItemTransQty.Value = 1M;
                            this.cmbItemUOM.SelectedIndex = 0;
                        }
                        else
                        {
                            this.cmbItemUOM.Text = Convert.ToString(this.dgStkTransfer.SelectedRows[0].Cells["UOM"].Value);
                        }
                        this.CalculateItemTotal();
                    }
                    else
                    {
                        MessageBox.Show("Please Check the Item Code");
                        this.txtItemCode.SelectAll();
                        this.txtItemCode.Focus();
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Item Validataion", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void txtRemark_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) && ParaDine.GlobalValidations.ValidateFields(this.pnlItem, this.TTip))
                {
                    if (Convert.ToInt32(this.pnlItem.Tag) < 0)
                    {
                        StockTrfTrans varStkTrfTrans = new StockTrfTrans
                        {
                            StockTrfMasterId = 0L
                        };
                        this.LoadTransEntities(varStkTrfTrans);
                        varStkTrfTrans.StockTrfMasterId = 0L;
                        varStkTrfTrans.Added = true;
                        this.EntId.StkTrfTransCollection.Add(varStkTrfTrans);
                    }
                    else
                    {
                        StockTrfTrans trans2 = this.EntId.StkTrfTransCollection[Convert.ToInt32(this.pnlItem.Tag)].Copy();
                        this.LoadTransEntities(trans2);
                        this.EntId.StkTrfTransCollection.InsertAt(trans2, Convert.ToInt16(this.pnlItem.Tag));
                    }
                    this.pnlItem.Tag = -1;
                    this.PopulateView();
                    if (this.txtItemCode.Enabled)
                    {
                        this.txtItemCode.Focus();
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in gvRemark_KeyDown");
            }
        }
    }
}
