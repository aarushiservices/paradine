﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Stock
{
    partial class frmStockReturn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlStockReturn = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.cmbStockPoint = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.gvTotQty = new ParaSysCom.NumControl();
            this.lblitem = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnTrans = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.gvTotalAmt = new ParaSysCom.NumControl();
            this.label14 = new System.Windows.Forms.Label();
            this.gvFreightChars = new ParaSysCom.NumControl();
            this.label19 = new System.Windows.Forms.Label();
            this.gvGrossAmt = new ParaSysCom.NumControl();
            this.pnlItem = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.gvQtyinHand = new ParaSysCom.NumControl();
            this.label7 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.gvItemAmount = new ParaSysCom.NumControl();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.gvReturnQty = new ParaSysCom.NumControl();
            this.gvItemCostPrice = new ParaSysCom.NumControl();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbItemUOM = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtItemCode = new System.Windows.Forms.TextBox();
            this.dgStkReturn = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbSupplier = new System.Windows.Forms.ComboBox();
            this.DtpStockReturnDate = new System.Windows.Forms.DateTimePicker();
            this.DtpStockReturnBillDate = new System.Windows.Forms.DateTimePicker();
            this.txtStockReturnNo = new System.Windows.Forms.TextBox();
            this.txtReturnBillNo = new System.Windows.Forms.TextBox();
            this.txtLrNo = new System.Windows.Forms.TextBox();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.pnlStockReturn.SuspendLayout();
            this.pnlItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgStkReturn)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlStockReturn
            // 
            this.pnlStockReturn.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlStockReturn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlStockReturn.Controls.Add(this.label17);
            this.pnlStockReturn.Controls.Add(this.cmbStockPoint);
            this.pnlStockReturn.Controls.Add(this.label16);
            this.pnlStockReturn.Controls.Add(this.gvTotQty);
            this.pnlStockReturn.Controls.Add(this.lblitem);
            this.pnlStockReturn.Controls.Add(this.btnExit);
            this.pnlStockReturn.Controls.Add(this.btnTrans);
            this.pnlStockReturn.Controls.Add(this.label15);
            this.pnlStockReturn.Controls.Add(this.gvTotalAmt);
            this.pnlStockReturn.Controls.Add(this.label14);
            this.pnlStockReturn.Controls.Add(this.gvFreightChars);
            this.pnlStockReturn.Controls.Add(this.label19);
            this.pnlStockReturn.Controls.Add(this.gvGrossAmt);
            this.pnlStockReturn.Controls.Add(this.pnlItem);
            this.pnlStockReturn.Controls.Add(this.dgStkReturn);
            this.pnlStockReturn.Controls.Add(this.label6);
            this.pnlStockReturn.Controls.Add(this.label5);
            this.pnlStockReturn.Controls.Add(this.label4);
            this.pnlStockReturn.Controls.Add(this.label3);
            this.pnlStockReturn.Controls.Add(this.label2);
            this.pnlStockReturn.Controls.Add(this.label1);
            this.pnlStockReturn.Controls.Add(this.cmbSupplier);
            this.pnlStockReturn.Controls.Add(this.DtpStockReturnDate);
            this.pnlStockReturn.Controls.Add(this.DtpStockReturnBillDate);
            this.pnlStockReturn.Controls.Add(this.txtStockReturnNo);
            this.pnlStockReturn.Controls.Add(this.txtReturnBillNo);
            this.pnlStockReturn.Controls.Add(this.txtLrNo);
            this.pnlStockReturn.Location = new System.Drawing.Point(3, 4);
            this.pnlStockReturn.Name = "pnlStockReturn";
            this.pnlStockReturn.Size = new System.Drawing.Size(700, 451);
            this.pnlStockReturn.TabIndex = 0;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(432, 52);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(62, 13);
            this.label17.TabIndex = 47;
            this.label17.Text = "StockPoint:";
            // 
            // cmbStockPoint
            // 
            this.cmbStockPoint.BackColor = System.Drawing.Color.Ivory;
            this.cmbStockPoint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStockPoint.FormattingEnabled = true;
            this.cmbStockPoint.Location = new System.Drawing.Point(498, 48);
            this.cmbStockPoint.Name = "cmbStockPoint";
            this.cmbStockPoint.Size = new System.Drawing.Size(176, 21);
            this.cmbStockPoint.TabIndex = 5;
            this.TTip.SetToolTip(this.cmbStockPoint, "Select StockPoint");
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(505, 341);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 13);
            this.label16.TabIndex = 45;
            this.label16.Text = "Total Qty :";
            // 
            // gvTotQty
            // 
            this.gvTotQty.DecimalRequired = true;
            this.gvTotQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvTotQty.Location = new System.Drawing.Point(567, 337);
            this.gvTotQty.Name = "gvTotQty";
            this.gvTotQty.Size = new System.Drawing.Size(123, 20);
            this.gvTotQty.SymbolRequired = false;
            this.gvTotQty.TabIndex = 44;
            this.gvTotQty.Text = "0.00";
            this.gvTotQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvTotQty, "Displays Total Quanity");
            this.gvTotQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvTotQty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gvTot_KeyPressEvent);
            // 
            // lblitem
            // 
            this.lblitem.AutoSize = true;
            this.lblitem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblitem.ForeColor = System.Drawing.Color.Red;
            this.lblitem.Location = new System.Drawing.Point(21, 347);
            this.lblitem.Name = "lblitem";
            this.lblitem.Size = new System.Drawing.Size(173, 13);
            this.lblitem.TabIndex = 43;
            this.lblitem.Text = "Press F1 For Listing all items.";
            this.lblitem.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.CausesValidation = false;
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(107, 415);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 9;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(26, 415);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 8;
            this.btnTrans.Text = "Trans";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(427, 419);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(90, 13);
            this.label15.TabIndex = 40;
            this.label15.Text = "Total Amount :";
            // 
            // gvTotalAmt
            // 
            this.gvTotalAmt.DecimalRequired = true;
            this.gvTotalAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvTotalAmt.Location = new System.Drawing.Point(523, 412);
            this.gvTotalAmt.Name = "gvTotalAmt";
            this.gvTotalAmt.Size = new System.Drawing.Size(167, 20);
            this.gvTotalAmt.SymbolRequired = true;
            this.gvTotalAmt.TabIndex = 39;
            this.gvTotalAmt.Text = "`0.00";
            this.gvTotalAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvTotalAmt, "Displays total Amount");
            this.gvTotalAmt.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvTotalAmt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gvTot_KeyPressEvent);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(476, 391);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 13);
            this.label14.TabIndex = 38;
            this.label14.Text = "Freight Charges :";
            // 
            // gvFreightChars
            // 
            this.gvFreightChars.DecimalRequired = true;
            this.gvFreightChars.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvFreightChars.Location = new System.Drawing.Point(567, 387);
            this.gvFreightChars.Name = "gvFreightChars";
            this.gvFreightChars.Size = new System.Drawing.Size(123, 20);
            this.gvFreightChars.SymbolRequired = true;
            this.gvFreightChars.TabIndex = 7;
            this.gvFreightChars.Text = "`0.00";
            this.gvFreightChars.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvFreightChars, "Enter Freight Charges");
            this.gvFreightChars.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvFreightChars.TextChanged += new System.EventHandler(this.gvFreightChars_Change);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(484, 366);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(79, 13);
            this.label19.TabIndex = 36;
            this.label19.Text = "Gross Amount :";
            // 
            // gvGrossAmt
            // 
            this.gvGrossAmt.DecimalRequired = true;
            this.gvGrossAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvGrossAmt.Location = new System.Drawing.Point(567, 362);
            this.gvGrossAmt.Name = "gvGrossAmt";
            this.gvGrossAmt.Size = new System.Drawing.Size(123, 20);
            this.gvGrossAmt.SymbolRequired = true;
            this.gvGrossAmt.TabIndex = 35;
            this.gvGrossAmt.Text = "`0.00";
            this.gvGrossAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvGrossAmt, "Displays Gross Amount");
            this.gvGrossAmt.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvGrossAmt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gvTot_KeyPressEvent);
            // 
            // pnlItem
            // 
            this.pnlItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlItem.Controls.Add(this.label25);
            this.pnlItem.Controls.Add(this.gvQtyinHand);
            this.pnlItem.Controls.Add(this.label7);
            this.pnlItem.Controls.Add(this.txtRemarks);
            this.pnlItem.Controls.Add(this.label10);
            this.pnlItem.Controls.Add(this.gvItemAmount);
            this.pnlItem.Controls.Add(this.label9);
            this.pnlItem.Controls.Add(this.label8);
            this.pnlItem.Controls.Add(this.gvReturnQty);
            this.pnlItem.Controls.Add(this.gvItemCostPrice);
            this.pnlItem.Controls.Add(this.label11);
            this.pnlItem.Controls.Add(this.cmbItemUOM);
            this.pnlItem.Controls.Add(this.label12);
            this.pnlItem.Controls.Add(this.txtItemName);
            this.pnlItem.Controls.Add(this.label13);
            this.pnlItem.Controls.Add(this.txtItemCode);
            this.pnlItem.Location = new System.Drawing.Point(6, 242);
            this.pnlItem.Name = "pnlItem";
            this.pnlItem.Size = new System.Drawing.Size(684, 88);
            this.pnlItem.TabIndex = 6;
            this.pnlItem.Tag = "";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(389, 4);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(31, 13);
            this.label25.TabIndex = 42;
            this.label25.Text = "QOH";
            // 
            // gvQtyinHand
            // 
            this.gvQtyinHand.BackColor = System.Drawing.Color.White;
            this.gvQtyinHand.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvQtyinHand.DecimalRequired = true;
            this.gvQtyinHand.Enabled = false;
            this.gvQtyinHand.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvQtyinHand.Location = new System.Drawing.Point(386, 19);
            this.gvQtyinHand.Name = "gvQtyinHand";
            this.gvQtyinHand.Size = new System.Drawing.Size(78, 20);
            this.gvQtyinHand.SymbolRequired = false;
            this.gvQtyinHand.TabIndex = 41;
            this.gvQtyinHand.Text = "0.00";
            this.gvQtyinHand.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvQtyinHand, "Enter Free Qty ");
            this.gvQtyinHand.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvQtyinHand.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gvTot_KeyPressEvent);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(389, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Remarks :";
            // 
            // txtRemarks
            // 
            this.txtRemarks.BackColor = System.Drawing.Color.White;
            this.txtRemarks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRemarks.Location = new System.Drawing.Point(386, 59);
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(288, 20);
            this.txtRemarks.TabIndex = 4;
            this.txtRemarks.Tag = "NoHandler";
            this.TTip.SetToolTip(this.txtRemarks, "Enter Remarks");
            this.txtRemarks.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRemarks_KeyDown);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(583, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "Amount";
            // 
            // gvItemAmount
            // 
            this.gvItemAmount.BackColor = System.Drawing.Color.White;
            this.gvItemAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvItemAmount.DecimalRequired = true;
            this.gvItemAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemAmount.Location = new System.Drawing.Point(580, 19);
            this.gvItemAmount.Name = "gvItemAmount";
            this.gvItemAmount.Size = new System.Drawing.Size(94, 20);
            this.gvItemAmount.SymbolRequired = true;
            this.gvItemAmount.TabIndex = 12;
            this.gvItemAmount.TabStop = false;
            this.gvItemAmount.Text = "`0.00";
            this.gvItemAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvItemAmount, "Displays ItemAmount");
            this.gvItemAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(286, 4);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Cost Price";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(488, 4);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Rtrn Qty :";
            // 
            // gvReturnQty
            // 
            this.gvReturnQty.BackColor = System.Drawing.Color.Ivory;
            this.gvReturnQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvReturnQty.DecimalRequired = true;
            this.gvReturnQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvReturnQty.Location = new System.Drawing.Point(485, 19);
            this.gvReturnQty.Name = "gvReturnQty";
            this.gvReturnQty.Size = new System.Drawing.Size(74, 20);
            this.gvReturnQty.SymbolRequired = false;
            this.gvReturnQty.TabIndex = 3;
            this.gvReturnQty.Text = "0.00";
            this.gvReturnQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvReturnQty, "Enter Return Qty");
            this.gvReturnQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvReturnQty.TextChanged += new System.EventHandler(this.gvReturnQty_Change);
            // 
            // gvItemCostPrice
            // 
            this.gvItemCostPrice.BackColor = System.Drawing.Color.White;
            this.gvItemCostPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvItemCostPrice.DecimalRequired = true;
            this.gvItemCostPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemCostPrice.Location = new System.Drawing.Point(283, 19);
            this.gvItemCostPrice.Name = "gvItemCostPrice";
            this.gvItemCostPrice.Size = new System.Drawing.Size(82, 20);
            this.gvItemCostPrice.SymbolRequired = true;
            this.gvItemCostPrice.TabIndex = 2;
            this.gvItemCostPrice.Tag = "";
            this.gvItemCostPrice.Text = "`0.00";
            this.gvItemCostPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvItemCostPrice, "Enter Cost Price");
            this.gvItemCostPrice.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvItemCostPrice.TextChanged += new System.EventHandler(this.gvItemCostPrice_Change);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(126, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(32, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "UOM";
            // 
            // cmbItemUOM
            // 
            this.cmbItemUOM.BackColor = System.Drawing.Color.Ivory;
            this.cmbItemUOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbItemUOM.FormattingEnabled = true;
            this.cmbItemUOM.Location = new System.Drawing.Point(123, 18);
            this.cmbItemUOM.Name = "cmbItemUOM";
            this.cmbItemUOM.Size = new System.Drawing.Size(139, 21);
            this.cmbItemUOM.TabIndex = 1;
            this.TTip.SetToolTip(this.cmbItemUOM, "Select Unit");
            this.cmbItemUOM.SelectionChangeCommitted += new System.EventHandler(this.cmbItemUOM_SelectionChangeCommitted);
            this.cmbItemUOM.SelectedValueChanged += new System.EventHandler(this.cmbItemUOM_SelectedValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 44);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "Desc :";
            // 
            // txtItemName
            // 
            this.txtItemName.BackColor = System.Drawing.Color.White;
            this.txtItemName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemName.Location = new System.Drawing.Point(8, 59);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(355, 20);
            this.txtItemName.TabIndex = 13;
            this.txtItemName.TabStop = false;
            this.txtItemName.Tag = "";
            this.TTip.SetToolTip(this.txtItemName, "Displays Item Name");
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 3);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 10;
            this.label13.Text = "Item Code";
            // 
            // txtItemCode
            // 
            this.txtItemCode.BackColor = System.Drawing.Color.Ivory;
            this.txtItemCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemCode.Location = new System.Drawing.Point(8, 19);
            this.txtItemCode.Name = "txtItemCode";
            this.txtItemCode.Size = new System.Drawing.Size(94, 20);
            this.txtItemCode.TabIndex = 0;
            this.TTip.SetToolTip(this.txtItemCode, "Enter Item Code");
            this.txtItemCode.Enter += new System.EventHandler(this.txtItemCode_Enter);
            this.txtItemCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItemCode_KeyDown);
            this.txtItemCode.Leave += new System.EventHandler(this.txtItemCode_Leave);
            this.txtItemCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtItemCode_Validating);
            // 
            // dgStkReturn
            // 
            this.dgStkReturn.BackgroundColor = System.Drawing.Color.White;
            this.dgStkReturn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgStkReturn.Location = new System.Drawing.Point(6, 75);
            this.dgStkReturn.Name = "dgStkReturn";
            this.dgStkReturn.RowHeadersVisible = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.SeaShell;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgStkReturn.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgStkReturn.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgStkReturn.Size = new System.Drawing.Size(684, 161);
            this.dgStkReturn.TabIndex = 12;
            this.dgStkReturn.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgStkReturn_CellDoubleClick);
            this.dgStkReturn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgStkReturn_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(218, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Lr Number :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Rtrn Date :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(372, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Return Bill No :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(511, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Return Bill Date :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Rtrn#";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(187, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Supplier :";
            // 
            // cmbSupplier
            // 
            this.cmbSupplier.BackColor = System.Drawing.Color.Ivory;
            this.cmbSupplier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSupplier.FormattingEnabled = true;
            this.cmbSupplier.Location = new System.Drawing.Point(241, 48);
            this.cmbSupplier.Name = "cmbSupplier";
            this.cmbSupplier.Size = new System.Drawing.Size(176, 21);
            this.cmbSupplier.TabIndex = 4;
            this.TTip.SetToolTip(this.cmbSupplier, "Select Supplier");
            // 
            // DtpStockReturnDate
            // 
            this.DtpStockReturnDate.CustomFormat = "dd/MMM/yyyy";
            this.DtpStockReturnDate.Enabled = false;
            this.DtpStockReturnDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpStockReturnDate.Location = new System.Drawing.Point(77, 48);
            this.DtpStockReturnDate.Name = "DtpStockReturnDate";
            this.DtpStockReturnDate.Size = new System.Drawing.Size(101, 20);
            this.DtpStockReturnDate.TabIndex = 0;
            this.DtpStockReturnDate.TabStop = false;
            this.TTip.SetToolTip(this.DtpStockReturnDate, "Displays Stock Return Date");
            // 
            // DtpStockReturnBillDate
            // 
            this.DtpStockReturnBillDate.CustomFormat = "dd/MMM/yyyy";
            this.DtpStockReturnBillDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpStockReturnBillDate.Location = new System.Drawing.Point(508, 19);
            this.DtpStockReturnBillDate.Name = "DtpStockReturnBillDate";
            this.DtpStockReturnBillDate.Size = new System.Drawing.Size(101, 20);
            this.DtpStockReturnBillDate.TabIndex = 3;
            this.TTip.SetToolTip(this.DtpStockReturnBillDate, "Displays Return Bill Date");
            // 
            // txtStockReturnNo
            // 
            this.txtStockReturnNo.BackColor = System.Drawing.Color.Ivory;
            this.txtStockReturnNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStockReturnNo.Enabled = false;
            this.txtStockReturnNo.Location = new System.Drawing.Point(77, 19);
            this.txtStockReturnNo.Name = "txtStockReturnNo";
            this.txtStockReturnNo.Size = new System.Drawing.Size(101, 20);
            this.txtStockReturnNo.TabIndex = 0;
            this.txtStockReturnNo.TabStop = false;
            this.TTip.SetToolTip(this.txtStockReturnNo, "Enter Stock Return No");
            // 
            // txtReturnBillNo
            // 
            this.txtReturnBillNo.BackColor = System.Drawing.Color.White;
            this.txtReturnBillNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtReturnBillNo.Location = new System.Drawing.Point(370, 19);
            this.txtReturnBillNo.Name = "txtReturnBillNo";
            this.txtReturnBillNo.Size = new System.Drawing.Size(101, 20);
            this.txtReturnBillNo.TabIndex = 2;
            this.TTip.SetToolTip(this.txtReturnBillNo, "Enter Return Bill No");
            // 
            // txtLrNo
            // 
            this.txtLrNo.BackColor = System.Drawing.Color.White;
            this.txtLrNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLrNo.Location = new System.Drawing.Point(215, 19);
            this.txtLrNo.Name = "txtLrNo";
            this.txtLrNo.Size = new System.Drawing.Size(118, 20);
            this.txtLrNo.TabIndex = 1;
            this.TTip.SetToolTip(this.txtLrNo, "Enter LrNumber");
            // 
            // frmStockReturn
            // 
            this.CancelButton = this.btnExit;
            this.ClientSize = new System.Drawing.Size(706, 461);
            this.Controls.Add(this.pnlStockReturn);
            this.Name = "frmStockReturn";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stock Return";
            this.Load += new System.EventHandler(this.frmStockReturn_Load);
            this.pnlStockReturn.ResumeLayout(false);
            this.pnlStockReturn.PerformLayout();
            this.pnlItem.ResumeLayout(false);
            this.pnlItem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgStkReturn)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
    }
}