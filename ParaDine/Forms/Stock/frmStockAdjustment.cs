﻿using ParaDine.Forms.GlobalForms;
using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Stock
{
    public partial class frmStockAdjustment : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private ComboBox cmbItemUOM;
        private ComboBox cmbStockPoint;
       // private IContainer components;
        private DataGridView dgStockTake;
        private DateTimePicker dtpStockAdjDate;
        private StockAdjust EntId;
        private NumControl gvItemAmount;
        private NumControl gvItemCostPrice;
        private NumControl gvPhysicalQty;
        private NumControl gvQtyinHand;
        private NumControl gvTotalAmount;
        private NumControl gvTotalQty;
        private NumControl gvVarianceQty;
        private Label label1;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label2;
        private Label label25;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label lblitem;
        private Label lblSource;
        private Panel pnlItem;
        private Panel PnlMain;
        private ToolTip TTip;
        private TextBox txtItemCode;
        private TextBox txtItemName;
        private TextBox txtStockAdjustNo;

        public frmStockAdjustment()
        {
            this.EntId = new StockAdjust(0L);
            this.components = null;
            this.InitializeComponent();
        }

        public frmStockAdjustment(string Mode, long ID, SecurityClass ParamSecurity)
        {
            this.EntId = new StockAdjust(0L);
            this.components = null;
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes(ID);
            this.EntId.Security = ParamSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (ParaDine.GlobalValidations.ValidateFields(this.PnlMain, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_007D;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_008D;
                            }
                        }
                        else
                        {
                            this.EntId.Add(sqlTrans, true);
                        }
                    }
                    goto Label_009F;
                Label_007D:
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_009F;
                Label_008D:
                    this.EntId.Delete(sqlTrans, true);
                Label_009F:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                    sqlTrans.Rollback();
                }
            }
        }

        private void CalculateItemTotal()
        {
            this.gvItemAmount.Value = Convert.ToDecimal((double)(Convert.ToDouble(this.gvItemCostPrice.Value) * Convert.ToDouble(this.gvVarianceQty.Value)));
        }

        private void CalculateTotal()
        {
            this.gvTotalQty.Value = Convert.ToDecimal(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgStockTake.DataSource, "VarianceQty"));
            this.gvTotalAmount.Value = Convert.ToDecimal(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgStockTake.DataSource, "Amount"));
        }

        private void CalculateVariance()
        {
            this.gvVarianceQty.Value = Convert.ToDecimal((double)(Convert.ToDouble(this.gvPhysicalQty.Value) - Convert.ToDouble(this.gvQtyinHand.Value)));
        }

        private void cmbItemUOM_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if ((this.cmbItemUOM.SelectedIndex != -1) && (this.cmbItemUOM.ValueMember != ""))
                {
                    Item item = new Item(Convert.ToString(this.txtItemCode.Text));
                    this.gvItemCostPrice.Value = Convert.ToDecimal(item.GetCostPrice(Convert.ToInt32(this.cmbItemUOM.SelectedValue)));
                    this.gvQtyinHand.Value = Convert.ToDecimal(item.GetStoreStock(Convert.ToInt32(this.cmbItemUOM.SelectedValue), Convert.ToInt16(this.cmbStockPoint.SelectedValue)));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "ERROR IN CMBITEMUOM_SELECTEDINDEXCHANGED");
            }
        }

        private void dgStockTake_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgStockTake.SelectedRows.Count > 0)
            {
                this.pnlItem.Tag = Convert.ToInt16(this.dgStockTake.SelectedRows[0].Cells["TableId"].Value) - 1;
                this.LoadTransFields(this.EntId.StockAdjustTransCollection[Convert.ToInt32(this.pnlItem.Tag)]);
                if (this.txtItemCode.Enabled)
                {
                    this.txtItemCode.Focus();
                }
            }
        }

        private void dgStockTake_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    this.EntId.StockAdjustTransCollection[Convert.ToInt32(this.dgStockTake.SelectedRows[0].Cells["TABLEID"].Value) - 1].Deleted = true;
                    this.PopulateView();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    this.dgStockTake_CellDoubleClick(sender, new DataGridViewCellEventArgs(0, this.dgStockTake.SelectedRows[0].Index));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
        private void frmStockAdjustment_Load(object sender, EventArgs e)
        {
            try
            {
                new GlobalTheme().applyTheme(this);
                ParaDine.GlobalFunctions.AddCompHandler(this.PnlMain);
                this.RefreshData();
                this.LoadFields();
                this.pnlItem.Tag = -1;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Form Load");
                base.Close();
            }
        }

        private void gvItemCostPrice_Change(object sender, EventArgs e)
        {
            this.CalculateItemTotal();
        }

        private void gvItemCostPrice_KeyDownEvent(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) && ParaDine.GlobalValidations.ValidateFields(this.pnlItem, this.TTip))
                {
                    StockAdjustTrans trans;
                    if (Convert.ToInt32(this.pnlItem.Tag) < 0)
                    {
                        trans = new StockAdjustTrans
                        {
                            StockAdjustID = this.EntId.StockAdjustID,
                            StockAdjustTransID = 0L
                        };
                        this.LoadTransEntities(trans);
                        trans.Added = true;
                        this.EntId.StockAdjustTransCollection.Add(trans);
                    }
                    else
                    {
                        trans = this.EntId.StockAdjustTransCollection[Convert.ToInt32(this.pnlItem.Tag)];
                        this.LoadTransEntities(trans);
                        trans.Modified = true;
                    }
                    this.pnlItem.Tag = -1;
                    this.PopulateView();
                    if (this.txtItemCode.Enabled)
                    {
                        this.txtItemCode.Focus();
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in PhysicalQty_KayDown", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void gvPhysicalQty_Change(object sender, EventArgs e)
        {
            this.CalculateVariance();
        }

        private void gvPhysicalQty_KeyDownEvent(object sender, KeyEventArgs e)
        {
        }

        private void gvQtyinHand_Change(object sender, EventArgs e)
        {
            this.CalculateVariance();
        }

        private void gvVarianceQty_Change(object sender, EventArgs e)
        {
            this.CalculateItemTotal();
        }

    

        private void LoadEntities()
        {
            this.EntId.StockAdjustID = Convert.ToInt64(this.txtStockAdjustNo.Tag);
            this.EntId.StockAdjustNo = Convert.ToString(this.txtStockAdjustNo.Text);
            this.EntId.StockAdjustDate = Convert.ToDateTime(this.dtpStockAdjDate.Value);
            this.EntId.StockPointID = Convert.ToInt16(this.cmbStockPoint.SelectedValue);
        }

        public void LoadFields()
        {
            this.txtStockAdjustNo.Tag = Convert.ToInt64(this.EntId.StockAdjustID);
            if (this.EntId.StockAdjustID <= 0L)
            {
                this.txtStockAdjustNo.Text = this.EntId.GetMaxCode;
                this.txtStockAdjustNo.ReadOnly = true;
            }
            else
            {
                this.txtStockAdjustNo.Text = Convert.ToString(this.EntId.StockAdjustNo);
            }
            this.dtpStockAdjDate.Value = Convert.ToDateTime(this.EntId.StockAdjustDate);
            this.cmbStockPoint.SelectedValue = Convert.ToInt16(this.EntId.StockPointID);
            this.PopulateView();
        }

        private void LoadTransEntities(StockAdjustTrans VarStockAdjustTrans)
        {
            VarStockAdjustTrans.StockAdjustID = this.EntId.StockAdjustID;
            VarStockAdjustTrans.ItemID = Convert.ToInt64(this.txtItemCode.Tag);
            VarStockAdjustTrans.UOMID = Convert.ToInt32(this.cmbItemUOM.SelectedValue);
            VarStockAdjustTrans.VarianceQty = Convert.ToDouble(this.gvVarianceQty.Value);
            VarStockAdjustTrans.CostPrice = Convert.ToDouble(this.gvItemCostPrice.Value);
        }

        private void LoadTransFields(StockAdjustTrans VarStockAdjustTrans)
        {
            Item item = new Item(VarStockAdjustTrans.ItemID);
            this.txtItemCode.Tag = Convert.ToInt64(item.ItemID);
            this.txtItemCode.Text = Convert.ToString(item.ItemCode);
            this.txtItemCode_Validating(new object(), new CancelEventArgs());
            this.txtItemName.Text = Convert.ToString(item.ItemName);
            this.cmbItemUOM.SelectedValue = Convert.ToInt32(VarStockAdjustTrans.UOMID);
            this.gvVarianceQty.Value = Convert.ToDecimal(VarStockAdjustTrans.VarianceQty);
            this.gvItemCostPrice.Value = Convert.ToDecimal(VarStockAdjustTrans.CostPrice);
        }

        private void PopulateView()
        {
            ParaDine.GlobalFill.FillGridView(this.dgStockTake, this.EntId.StockAdjustTransCollection.GetCollectionTable());
            this.dgStockTake.Refresh();
            this.dgStockTake.AllowUserToAddRows = false;
            this.dgStockTake.AllowUserToDeleteRows = false;
            this.dgStockTake.AllowUserToOrderColumns = false;
            this.dgStockTake.AllowUserToResizeColumns = false;
            this.dgStockTake.AllowUserToResizeRows = false;
            this.dgStockTake.EditMode = DataGridViewEditMode.EditProgrammatically;
            this.dgStockTake.ReadOnly = true;
            this.dgStockTake.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgStockTake.RowTemplate.ReadOnly = true;
            this.dgStockTake.StandardTab = false;
            this.dgStockTake.RowsDefaultCellStyle.SelectionBackColor = Color.SeaShell;
            this.SetVwColWidth();
            this.CalculateTotal();
        }

        private void RefreshData()
        {
            ParaDine.GlobalFill.FillCombo("SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT", this.cmbStockPoint);
        }

        private void SetVwColWidth()
        {
            try
            {
                this.dgStockTake.Columns[0].Visible = false;
                this.dgStockTake.Columns[1].Visible = false;
                this.dgStockTake.Columns[2].Width = 150;
                this.dgStockTake.Columns[3].Width = 100;
                this.dgStockTake.Columns[4].Width = 60;
                this.dgStockTake.Columns[5].Width = 80;
                for (int i = 7; i < this.dgStockTake.Columns.Count; i++)
                {
                    this.dgStockTake.Columns[i].Visible = false;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Setting Column Width");
            }
        }

        private void txtItemCode_Enter(object sender, EventArgs e)
        {
            this.lblitem.Visible = true;
        }

        private void txtItemCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                string paramSql = "SELECT * FROM RES_VW_ITEM WHERE NONINVENTORY = 0 OR RAWMATERIAL = 1";
                new frmBrowseData(ParaDine.GlobalFill.FillDataTable(paramSql), this.txtItemCode, "ITEMS", 2).ShowDialog();
            }
        }

        private void txtItemCode_Leave(object sender, EventArgs e)
        {
            this.lblitem.Visible = false;
        }

        private void txtItemCode_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.txtItemCode.Text.Trim() != "")
                {
                    Item item = new Item(this.txtItemCode.Text);
                    if (item.NonInventory && !item.RawMaterial)
                    {
                        throw new Exception("U Entered A Non Inventory Item.\nPlease Check the Item Code....");
                    }
                    this.txtItemCode.Tag = item.ItemID;
                    this.txtItemCode.Text = item.ItemCode;
                    this.txtItemName.Text = item.ItemName;
                    DataTable uOM = item.GetUOM();
                    if (uOM.Rows.Count > 0)
                    {
                        ParaDine.GlobalFill.FillCombo(uOM, this.cmbItemUOM);
                        if (uOM.Rows.Count == 1)
                        {
                            this.cmbItemUOM.Enabled = false;
                            this.cmbItemUOM.TabStop = false;
                            if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                            {
                                this.cmbItemUOM.SelectedIndex = 0;
                                this.cmbItemUOM_SelectedValueChanged(sender, e);
                            }
                        }
                        else
                        {
                            if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                            {
                                this.cmbItemUOM.Enabled = true;
                                this.cmbItemUOM.TabStop = true;
                                this.cmbItemUOM.SelectedIndex = 0;
                            }
                            else
                            {
                                this.cmbItemUOM.Enabled = false;
                                this.cmbItemUOM.Text = Convert.ToString(this.dgStockTake.SelectedRows[0].Cells["UNIT"].Value);
                                this.gvPhysicalQty.Value = Convert.ToDecimal((double)(Convert.ToDouble(this.gvQtyinHand.Value) + Convert.ToDouble(this.dgStockTake.SelectedRows[0].Cells["VARIANCEQTY"].Value)));
                            }
                            this.cmbItemUOM.TabStop = true;
                        }
                    }
                    else
                    {
                        this.cmbItemUOM.Enabled = true;
                        this.cmbItemUOM.TabStop = true;
                        ParaDine.GlobalFill.FillCombo("SELECT UOMID, UOMNAME FROM RES_UOM ORDER BY 2", this.cmbItemUOM);
                        this.cmbItemUOM.SelectedValue = item.UOMID;
                    }
                    if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                    {
                        this.cmbItemUOM.SelectedIndex = 0;
                    }
                    else
                    {
                        this.cmbItemUOM.Text = Convert.ToString(this.dgStockTake.SelectedRows[0].Cells["UNIT"].Value);
                    }
                    if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                    {
                        this.gvItemCostPrice.Value = Convert.ToDecimal(item.CostPrice);
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Item Code", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                e.Cancel = true;
            }
        }
    }
}
