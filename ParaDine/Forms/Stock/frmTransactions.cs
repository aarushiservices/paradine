﻿using Microsoft.VisualBasic;
using ParaDine.Forms.RestaurantItm;
using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Stock
{
    public partial class frmTransactions : Form
    {
        private Button btnAdd;
        private Button btnBack;
        private Button btnDelete;
        private Button btnDisplay;
        private Button btnEdit;
        private Button btnExit;
        private Button btnReset;
        private CheckBox chkSelect;
        private ComboBox cmbBoolean;
        private ComboBox cmbFilter;
        private ComboBox cmbPnlRealComparision;
      //private IContainer components;
        private DataGridView dgViewMaster;
        private DateTimePicker dtFrom;
        private DateTimePicker dtTo;
        private string EntName;
        private SecurityClass EntSecurity;
        private NumControl gvFilterAmount;
        private ImageList imageList1;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label lblFilter;
        private DataTable mstDT;
        private Panel panel1;
        private Panel pnlBoolean;
        private Panel pnlDate;
        private Panel pnlFilter;
        private Panel pnlReal;
        private Panel pnlString;
        private Panel pnlTrans;
        public DataTable SelDt;
        private string StrCond;
        private TreeView trView;
        private ToolTip TTip;
        private TextBox txtFilter;
        private TransactionBase varLocalEntity;

        public frmTransactions(string paramEntName)
        {
            this.components = null;
            this.EntName = "";
            this.SelDt = new DataTable();
            this.StrCond = "";
            this.InitializeComponent();
            ParaDine.GlobalFunctions.GetConnection();
            this.EntName = paramEntName;
        }

        public frmTransactions(string paramEntName, SecurityClass paramSecurity)
        {
            this.components = null;
            this.EntName = "";
            this.SelDt = new DataTable();
            this.StrCond = "";
            this.InitializeComponent();
            ParaDine.GlobalFunctions.GetConnection();
            this.EntName = paramEntName;
            this.EntSecurity = paramSecurity;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.ProcessEntity(this.EntName, this.btnAdd.Text, 0);
                this.PopulateDateTree();
                //if (this.trView.Nodes[0].Nodes.Count > 0)
                //{
                //    this.PopulateMasterTable(this.trView.Nodes[0].Nodes[0]);
                //}
                //this.PopulateListView();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Add Click");
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (this.StrCond.Contains("AND"))
            {
                string strCond = this.StrCond;
                int startIndex = 0;
                while (strCond.IndexOf("AND") > 0)
                {
                    startIndex = this.StrCond.IndexOf("AND", startIndex) + 3;
                    if (startIndex < strCond.Length)
                    {
                        strCond = strCond.Substring(startIndex);
                    }
                }
                this.StrCond = this.StrCond.Substring(0, startIndex - 3);
            }
            else
            {
                this.StrCond = "";
            }
            this.lblFilter.Text = this.StrCond.Replace("%", "");
            this.PopulateDateTree();
            this.PopulateListView();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgViewMaster.SelectedRows.Count > 0)
                {
                    this.ProcessEntity(this.EntName, this.btnDelete.Text, Convert.ToInt16(this.dgViewMaster.SelectedRows[0].Cells[0].Value));
                    if (this.trView.Nodes[0].Nodes.Count > 0)
                    {
                        this.PopulateMasterTable(this.trView.Nodes[0].Nodes[0]);
                    }
                    this.PopulateDateTree();
                    this.PopulateListView();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Delete Click");
            }
        }

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            this.GetFilter();
            this.PopulateDateTree();
            this.PopulateListView();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgViewMaster.SelectedRows.Count > 0)
                {
                    this.ProcessEntity(this.EntName, this.btnEdit.Text, Convert.ToInt16(this.dgViewMaster.SelectedRows[0].Cells[0].Value));
                    if (this.trView.Nodes[0].Nodes.Count > 0)
                    {
                        if (this.trView.SelectedNode != null)
                        {
                            this.PopulateMasterTable(this.trView.SelectedNode);
                        }
                        else
                        {
                            this.PopulateMasterTable(this.trView.Nodes[0].Nodes[0]);
                        }
                    }
                    this.PopulateListView();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Edit Click");
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }
        private void btnReset_Click(object sender, EventArgs e)
        {
            this.RefreshData();
            this.PopulateDateTree();
            if (this.trView.Nodes[0].Nodes.Count > 0)
            {
                this.PopulateMasterTable(this.trView.Nodes[0].Nodes[0]);
            }
            this.PopulateListView();
        }
        private void chkSelect_CheckedChanged(object sender, EventArgs e)
        {
            this.PopulateListView();
        }

        private void cmbFilter_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (this.dgViewMaster.Columns[this.cmbFilter.SelectedItem.ToString()].ValueType == typeof(DateTime))
            {
                this.pnlDate.Visible = true;
                this.pnlString.Visible = false;
                this.pnlReal.Visible = false;
                this.pnlBoolean.Visible = false;
            }
            else if (this.dgViewMaster.Columns[this.cmbFilter.SelectedItem.ToString()].ValueType == typeof(string))
            {
                this.pnlDate.Visible = false;
                this.pnlString.Visible = true;
                this.pnlReal.Visible = false;
                this.pnlBoolean.Visible = false;
            }
            else if ((this.dgViewMaster.Columns[this.cmbFilter.SelectedItem.ToString()].ValueType == typeof(double)) || (this.dgViewMaster.Columns[this.cmbFilter.SelectedItem.ToString()].ValueType == typeof(decimal)))
            {
                this.pnlDate.Visible = false;
                this.pnlString.Visible = false;
                this.pnlReal.Visible = true;
                this.pnlBoolean.Visible = false;
            }
            else if (this.dgViewMaster.Columns[this.cmbFilter.SelectedItem.ToString()].ValueType == typeof(bool))
            {
                this.pnlBoolean.Visible = true;
                this.pnlDate.Visible = false;
                this.pnlString.Visible = false;
                this.pnlReal.Visible = false;
            }
        }

        private void dgViewMaster_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                this.btnEdit_Click(sender, new EventArgs());
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void dgViewMaster_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.dgViewMaster_DoubleClick(sender, new EventArgs());
            }
        }
        private void frmTransactions_Load(object sender, EventArgs e)
        {
            try
            {
                this.Text = this.EntName;
                if (this.EntName != "")
                {
                    this.ProcessEntity(this.EntName, "LOAD", 0);
                    this.RefreshData();
                    this.SetGridView();
                    this.PopulateDateTree();
                    if (this.trView.Nodes.Count > 0)
                    {
                        if (this.trView.Nodes[0].Nodes.Count > 0)
                        {
                            this.PopulateMasterTable(this.trView.Nodes[0].Nodes[0]);
                        }
                        this.PopulateListView();
                        this.PopulateFilters();
                    }
                }
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Master Control Load");
            }
        }

        private void GetFilter()
        {
            string strCond;
            if (this.pnlString.Visible && (this.txtFilter.Text.Trim() != ""))
            {
                if (this.StrCond.Trim() != "")
                {
                    this.StrCond = this.StrCond + " AND ";
                }
                strCond = this.StrCond;
                this.StrCond = strCond + this.cmbFilter.Text + " LIKE '%" + this.txtFilter.Text.Trim() + "%'";
            }
            else
            {
                object obj2;
                if (this.pnlReal.Visible && (this.cmbPnlRealComparision.Text != ""))
                {
                    if (this.StrCond.Trim() != "")
                    {
                        this.StrCond = this.StrCond + " AND ";
                    }
                    obj2 = this.StrCond;
                    this.StrCond = string.Concat(new object[] { obj2, this.cmbFilter.Text, this.cmbPnlRealComparision.Text, this.gvFilterAmount.Value });
                }
                else if (this.pnlDate.Visible && (this.dtFrom.Checked || this.dtTo.Checked))
                {
                    if (this.StrCond.Trim() != "")
                    {
                        this.StrCond = this.StrCond + " AND ";
                    }
                    string str = "";
                    if (this.dtFrom.Checked)
                    {
                        strCond = str;
                        str = strCond + this.cmbFilter.Text + " >= '" + this.dtFrom.Text + "'";
                    }
                    if (this.dtTo.Checked)
                    {
                        if (str.Trim() != "")
                        {
                            str = str + " AND ";
                        }
                        strCond = str;
                        str = strCond + this.cmbFilter.Text + " <= '" + this.dtTo.Text + "'";
                    }
                    this.StrCond = this.StrCond + str;
                }
                else if (this.pnlBoolean.Visible && (this.cmbBoolean.SelectedIndex >= 0))
                {
                    if (this.StrCond.Trim() != "")
                    {
                        this.StrCond = this.StrCond + " AND ";
                    }
                    obj2 = this.StrCond;
                    this.StrCond = string.Concat(new object[] { obj2, this.cmbFilter.Text, "=", this.cmbBoolean.SelectedIndex });
                }
            }
        }

    

        private void PopulateDateTree()
        {
            try
            {
                this.trView.Nodes.Clear();
                string text = "";
                int num = 0;
                TreeNode node = new TreeNode();
                foreach (DataRow row in this.varLocalEntity.GetDateList(this.StrCond).Rows)
                {
                    if (text != row[0].ToString().Substring(0, 4))
                    {
                        text = row[0].ToString().Substring(0, 4);
                        node.Text = node.Text + " (" + Convert.ToString(num) + ")";
                        num = 0;
                        node = new TreeNode(text);
                        this.trView.Nodes.Add(node);
                    }
                    TreeNode node2 = new TreeNode(DateAndTime.MonthName(Convert.ToInt16(row[0].ToString().Substring(4)), false) + " (" + row[1].ToString() + ")")
                    {
                        Name = row[0].ToString()
                    };
                    node.Nodes.Add(node2);
                    num += Convert.ToInt32(row[1]);
                    if (row.Equals(row.Table.Rows[row.Table.Rows.Count - 1]))
                    {
                        node.Text = node.Text + " (" + Convert.ToString(num) + ")";
                        num = 0;
                    }
                }
                if (this.trView.Nodes.Count > 0)
                {
                    this.trView.Nodes[0].Expand();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "PopulateMonthTree");
            }
        }

        private void PopulateFilters()
        {
            try
            {
                if (!object.Equals(this.varLocalEntity, null))
                {
                    foreach (DataColumn column in this.mstDT.Columns)
                    {
                        if (column.Ordinal > 0)
                        {
                            this.cmbFilter.Items.Add(column.ColumnName);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "PopulateFilters");
            }
        }

        private void PopulateListView()
        {
            try
            {
                if (!object.Equals(this.varLocalEntity, null))
                {
                    this.mstDT.DefaultView.RowFilter = this.StrCond;
                    this.lblFilter.Text = this.StrCond.Replace("%", "");
                    ParaDine.GlobalFill.FillGridView(this.dgViewMaster, this.mstDT.DefaultView.ToTable());
                    if (this.dgViewMaster.Columns.Count > 0)
                    {
                        this.dgViewMaster.Columns[0].Visible = false;
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "PopulateListView");
            }
        }

        private void PopulateMasterTable(TreeNode pTrNode)
        {
            try
            {
                if (!object.Equals(this.varLocalEntity, null))
                {
                    this.mstDT = new DataTable();
                    this.mstDT.Rows.Clear();
                    this.mstDT.Columns.Clear();
                    string condSql = "";
                    condSql = " month(" + this.varLocalEntity.DateColName + ") = " + pTrNode.Name.Substring(4) + " AND year(" + this.varLocalEntity.DateColName + ") = " + pTrNode.Name.Substring(0, 4);
                    if (this.StrCond != "")
                    {
                        condSql = condSql + " AND " + this.StrCond;
                    }
                    this.mstDT = this.varLocalEntity.GetDataList(condSql);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "PopulateMasterTable");
            }
        }

        private void ProcessEntity(string paramEntName, string strProcess, int ProcessID)
        {
            switch (paramEntName)
            {
                case "StockEntry":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmStockEntry(strProcess, (long)ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new StockEntryMaster(0L);
                    break;

                case "ConsumedGoods":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmCosumedGoods(strProcess, (long)ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new ConsumedGoods(0L);
                    break;

                case "StockReturn":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmStockReturn(strProcess, (long)ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new StockReturnMaster(0L);
                    break;

                case "StockWriteOff":
                    if (!(strProcess == "LOAD"))
                    {
                        new FrmStockWriteOff(strProcess, (long)ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new StockWriteOff(0L);
                    break;

                case "StockTransfer":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmStockTransfer(strProcess, (long)ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new StockTrfMaster(0L);
                    break;

                case "Indent":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmIndent(strProcess, (long)ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new IndentMaster(0L);
                    break;

                case "Production":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmProduction(strProcess, (long)ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new Production(0L);
                    break;

                case "StockTake":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmStockAdjustment(strProcess, (long)ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new StockAdjust(0L);
                    break;

                case "DeliveryOrder":
                    if (!(strProcess == "LOAD"))
                    {
                        new FrmDeliveryOrder(strProcess, (long)ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new DeliveryOrder(0L);
                    break;

                case "CardIssue":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmCardIssue(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new CardIssue(0L);
                    this.btnEdit.Enabled = false;
                    this.btnDelete.Enabled = false;
                    break;

                case "CardRecharge":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmCardRecharge(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new CardTrans(0L);
                    this.btnEdit.Enabled = false;
                    this.btnDelete.Enabled = false;
                    break;

                case "CardReturn":
                    if (!(strProcess == "LOAD"))
                    {
                        new frmCardReturn(strProcess, ProcessID, this.EntSecurity).ShowDialog();
                        break;
                    }
                    this.varLocalEntity = new CardReturn(0L);
                    this.btnEdit.Enabled = false;
                    this.btnDelete.Enabled = false;
                    break;
            }
        }

        private void RefreshData()
        {
            this.StrCond = "";
            this.txtFilter.Text = "";
            this.cmbPnlRealComparision.Text = "";
            this.gvFilterAmount.Value = 0M;
            this.dtFrom.Value = ParaDine.GlobalVariables.BusinessDate;
            this.dtTo.Value = ParaDine.GlobalVariables.BusinessDate;
            this.dtFrom.Checked = false;
            this.dtTo.Checked = false;
            this.cmbBoolean.Text = "";
        }

        private void SetGridView()
        {
            this.dgViewMaster.AllowUserToAddRows = false;
            this.dgViewMaster.AllowUserToDeleteRows = false;
            this.dgViewMaster.AllowUserToResizeRows = false;
            this.dgViewMaster.EditMode = DataGridViewEditMode.EditProgrammatically;
            this.dgViewMaster.ReadOnly = true;
            this.dgViewMaster.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgViewMaster.RowTemplate.ReadOnly = true;
            this.dgViewMaster.StandardTab = false;
            this.dgViewMaster.RowHeadersVisible = false;
        }

        private void trView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Name.Length >= 4)
            {
                this.PopulateMasterTable(e.Node);
                this.PopulateListView();
            }
        }
    }
}
