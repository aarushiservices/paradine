﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Stock
{
    partial class frmStockTransfer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label25 = new System.Windows.Forms.Label();
            this.gvQtyinHand = new ParaSysCom.NumControl();
            this.dgStkTransfer = new System.Windows.Forms.DataGridView();
            this.label10 = new System.Windows.Forms.Label();
            this.gvItemAmount = new ParaSysCom.NumControl();
            this.label12 = new System.Windows.Forms.Label();
            this.gvItemCostPrice = new ParaSysCom.NumControl();
            this.btnExit = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRemark = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.gvTotalQty = new ParaSysCom.NumControl();
            this.btnTrans = new System.Windows.Forms.Button();
            this.gvItemTransQty = new ParaSysCom.NumControl();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbItemUOM = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.PnlStockTrf = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.txtIndentNo = new System.Windows.Forms.TextBox();
            this.lblitem = new System.Windows.Forms.Label();
            this.gvTotAmount = new ParaSysCom.NumControl();
            this.cmbfrStockPoint = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.pnlItem = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.txtItemCode = new System.Windows.Forms.TextBox();
            this.cmbToStockPoint = new System.Windows.Forms.ComboBox();
            this.dtpTrfDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTrfNo = new System.Windows.Forms.TextBox();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgStkTransfer)).BeginInit();
            this.PnlStockTrf.SuspendLayout();
            this.pnlItem.SuspendLayout();
            this.SuspendLayout();
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(233, 5);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(31, 13);
            this.label25.TabIndex = 46;
            this.label25.Text = "QOH";
            // 
            // gvQtyinHand
            // 
            this.gvQtyinHand.BackColor = System.Drawing.Color.White;
            this.gvQtyinHand.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvQtyinHand.DecimalRequired = true;
            this.gvQtyinHand.Enabled = false;
            this.gvQtyinHand.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvQtyinHand.Location = new System.Drawing.Point(235, 19);
            this.gvQtyinHand.Name = "gvQtyinHand";
            this.gvQtyinHand.ReadOnly = true;
            this.gvQtyinHand.Size = new System.Drawing.Size(82, 20);
            this.gvQtyinHand.SymbolRequired = false;
            this.gvQtyinHand.TabIndex = 2;
            this.gvQtyinHand.TabStop = false;
            this.gvQtyinHand.Text = "0.00";
            this.gvQtyinHand.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvQtyinHand, "Displays Qty On Hand ");
            this.gvQtyinHand.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // dgStkTransfer
            // 
            this.dgStkTransfer.BackgroundColor = System.Drawing.Color.White;
            this.dgStkTransfer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgStkTransfer.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgStkTransfer.Location = new System.Drawing.Point(8, 60);
            this.dgStkTransfer.Name = "dgStkTransfer";
            this.dgStkTransfer.RowHeadersVisible = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.SeaShell;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgStkTransfer.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgStkTransfer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgStkTransfer.Size = new System.Drawing.Size(645, 237);
            this.dgStkTransfer.TabIndex = 4;
            this.dgStkTransfer.TabStop = false;
            this.dgStkTransfer.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgStkTransfer_CellDoubleClick);
            this.dgStkTransfer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgStkTransfer_KeyDown);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(527, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 30;
            this.label10.Text = "Amount";
            // 
            // gvItemAmount
            // 
            this.gvItemAmount.BackColor = System.Drawing.Color.White;
            this.gvItemAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvItemAmount.DecimalRequired = true;
            this.gvItemAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemAmount.Location = new System.Drawing.Point(529, 19);
            this.gvItemAmount.Name = "gvItemAmount";
            this.gvItemAmount.Size = new System.Drawing.Size(101, 20);
            this.gvItemAmount.SymbolRequired = true;
            this.gvItemAmount.TabIndex = 5;
            this.gvItemAmount.TabStop = false;
            this.gvItemAmount.Text = "`0.00";
            this.gvItemAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvItemAmount, "Displays Amount");
            this.gvItemAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(429, 5);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 29;
            this.label12.Text = "Cost Price";
            // 
            // gvItemCostPrice
            // 
            this.gvItemCostPrice.BackColor = System.Drawing.Color.White;
            this.gvItemCostPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvItemCostPrice.DecimalRequired = true;
            this.gvItemCostPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemCostPrice.Location = new System.Drawing.Point(431, 19);
            this.gvItemCostPrice.Name = "gvItemCostPrice";
            this.gvItemCostPrice.Size = new System.Drawing.Size(82, 20);
            this.gvItemCostPrice.SymbolRequired = true;
            this.gvItemCostPrice.TabIndex = 4;
            this.gvItemCostPrice.TabStop = false;
            this.gvItemCostPrice.Tag = "";
            this.gvItemCostPrice.Text = "`0.00";
            this.gvItemCostPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvItemCostPrice, "Displays CostPrice");
            this.gvItemCostPrice.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvItemCostPrice.TextChanged += new System.EventHandler(this.gvItemCostPrice_Change);
            // 
            // btnExit
            // 
            this.btnExit.CausesValidation = false;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(100, 412);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 6;
            this.btnExit.TabStop = false;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(333, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Remark :";
            // 
            // txtRemark
            // 
            this.txtRemark.BackColor = System.Drawing.Color.White;
            this.txtRemark.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRemark.Location = new System.Drawing.Point(334, 56);
            this.txtRemark.Multiline = true;
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.Size = new System.Drawing.Size(297, 20);
            this.txtRemark.TabIndex = 7;
            this.txtRemark.Tag = "NoHandler";
            this.TTip.SetToolTip(this.txtRemark, "Enter Remarks");
            this.txtRemark.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRemark_KeyDown);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(332, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Trf Qty :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(412, 417);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(90, 13);
            this.label13.TabIndex = 34;
            this.label13.Text = "Total Amount :";
            // 
            // gvTotalQty
            // 
            this.gvTotalQty.DecimalRequired = true;
            this.gvTotalQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvTotalQty.Location = new System.Drawing.Point(548, 383);
            this.gvTotalQty.Name = "gvTotalQty";
            this.gvTotalQty.Size = new System.Drawing.Size(105, 20);
            this.gvTotalQty.SymbolRequired = false;
            this.gvTotalQty.TabIndex = 35;
            this.gvTotalQty.TabStop = false;
            this.gvTotalQty.Text = "0.00";
            this.gvTotalQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvTotalQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvTotalQty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gvTot_KeyPressEvent);
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(19, 412);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 5;
            this.btnTrans.TabStop = false;
            this.btnTrans.Text = "Trans";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // gvItemTransQty
            // 
            this.gvItemTransQty.BackColor = System.Drawing.Color.Ivory;
            this.gvItemTransQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvItemTransQty.DecimalRequired = true;
            this.gvItemTransQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemTransQty.Location = new System.Drawing.Point(333, 19);
            this.gvItemTransQty.Name = "gvItemTransQty";
            this.gvItemTransQty.Size = new System.Drawing.Size(82, 20);
            this.gvItemTransQty.SymbolRequired = false;
            this.gvItemTransQty.TabIndex = 3;
            this.gvItemTransQty.Text = "0.00";
            this.gvItemTransQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvItemTransQty, "Enter Transfer Qty");
            this.gvItemTransQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvItemTransQty.TextChanged += new System.EventHandler(this.gvItemTransQty_Change);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(96, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "UOM :";
            // 
            // cmbItemUOM
            // 
            this.cmbItemUOM.BackColor = System.Drawing.Color.Ivory;
            this.cmbItemUOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbItemUOM.FormattingEnabled = true;
            this.cmbItemUOM.Location = new System.Drawing.Point(98, 19);
            this.cmbItemUOM.Name = "cmbItemUOM";
            this.cmbItemUOM.Size = new System.Drawing.Size(121, 21);
            this.cmbItemUOM.TabIndex = 1;
            this.TTip.SetToolTip(this.cmbItemUOM, "Select an Unit");
            this.cmbItemUOM.SelectedValueChanged += new System.EventHandler(this.cmbItemUOM_SelectedValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(478, 388);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Total Qty :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Item Name :";
            // 
            // txtItemName
            // 
            this.txtItemName.BackColor = System.Drawing.Color.White;
            this.txtItemName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemName.Location = new System.Drawing.Point(15, 56);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(307, 20);
            this.txtItemName.TabIndex = 6;
            this.txtItemName.TabStop = false;
            this.txtItemName.Tag = "";
            this.TTip.SetToolTip(this.txtItemName, "Displays Item Name");
            // 
            // PnlStockTrf
            // 
            this.PnlStockTrf.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.PnlStockTrf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlStockTrf.Controls.Add(this.label14);
            this.PnlStockTrf.Controls.Add(this.txtIndentNo);
            this.PnlStockTrf.Controls.Add(this.lblitem);
            this.PnlStockTrf.Controls.Add(this.gvTotalQty);
            this.PnlStockTrf.Controls.Add(this.label13);
            this.PnlStockTrf.Controls.Add(this.gvTotAmount);
            this.PnlStockTrf.Controls.Add(this.cmbfrStockPoint);
            this.PnlStockTrf.Controls.Add(this.label9);
            this.PnlStockTrf.Controls.Add(this.label11);
            this.PnlStockTrf.Controls.Add(this.btnExit);
            this.PnlStockTrf.Controls.Add(this.btnTrans);
            this.PnlStockTrf.Controls.Add(this.pnlItem);
            this.PnlStockTrf.Controls.Add(this.dgStkTransfer);
            this.PnlStockTrf.Controls.Add(this.cmbToStockPoint);
            this.PnlStockTrf.Controls.Add(this.dtpTrfDate);
            this.PnlStockTrf.Controls.Add(this.label3);
            this.PnlStockTrf.Controls.Add(this.label2);
            this.PnlStockTrf.Controls.Add(this.label1);
            this.PnlStockTrf.Controls.Add(this.txtTrfNo);
            this.PnlStockTrf.Location = new System.Drawing.Point(5, 4);
            this.PnlStockTrf.Name = "PnlStockTrf";
            this.PnlStockTrf.Size = new System.Drawing.Size(663, 448);
            this.PnlStockTrf.TabIndex = 41;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(223, 11);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 13);
            this.label14.TabIndex = 43;
            this.label14.Text = "Indent No.";
            // 
            // txtIndentNo
            // 
            this.txtIndentNo.BackColor = System.Drawing.Color.White;
            this.txtIndentNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIndentNo.Location = new System.Drawing.Point(286, 7);
            this.txtIndentNo.Name = "txtIndentNo";
            this.txtIndentNo.ReadOnly = true;
            this.txtIndentNo.Size = new System.Drawing.Size(100, 20);
            this.txtIndentNo.TabIndex = 2;
            this.TTip.SetToolTip(this.txtIndentNo, "Displays KitchenToStore No");
            this.txtIndentNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtIndentNo_KeyDown);
            this.txtIndentNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtIndentNo_Validating);
            // 
            // lblitem
            // 
            this.lblitem.AutoSize = true;
            this.lblitem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblitem.ForeColor = System.Drawing.Color.Red;
            this.lblitem.Location = new System.Drawing.Point(218, 418);
            this.lblitem.Name = "lblitem";
            this.lblitem.Size = new System.Drawing.Size(173, 13);
            this.lblitem.TabIndex = 41;
            this.lblitem.Text = "Press F1 For Listing all items.";
            this.lblitem.Visible = false;
            // 
            // gvTotAmount
            // 
            this.gvTotAmount.DecimalRequired = true;
            this.gvTotAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvTotAmount.Location = new System.Drawing.Point(505, 411);
            this.gvTotAmount.Name = "gvTotAmount";
            this.gvTotAmount.Size = new System.Drawing.Size(148, 20);
            this.gvTotAmount.SymbolRequired = true;
            this.gvTotAmount.TabIndex = 33;
            this.gvTotAmount.TabStop = false;
            this.gvTotAmount.Text = "`0.00";
            this.gvTotAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvTotAmount, "Displays Amount");
            this.gvTotAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvTotAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gvTot_KeyPressEvent);
            // 
            // cmbfrStockPoint
            // 
            this.cmbfrStockPoint.FormattingEnabled = true;
            this.cmbfrStockPoint.Location = new System.Drawing.Point(469, 7);
            this.cmbfrStockPoint.Name = "cmbfrStockPoint";
            this.cmbfrStockPoint.Size = new System.Drawing.Size(184, 21);
            this.cmbfrStockPoint.TabIndex = 3;
            this.TTip.SetToolTip(this.cmbfrStockPoint, "Select Kitchen");
            this.cmbfrStockPoint.SelectionChangeCommitted += new System.EventHandler(this.cmbfrStockPoint_SelectionChangeCommitted);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(430, 11);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 13);
            this.label9.TabIndex = 27;
            this.label9.Text = "From :";
            // 
            // pnlItem
            // 
            this.pnlItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlItem.Controls.Add(this.label25);
            this.pnlItem.Controls.Add(this.gvQtyinHand);
            this.pnlItem.Controls.Add(this.label10);
            this.pnlItem.Controls.Add(this.gvItemAmount);
            this.pnlItem.Controls.Add(this.label12);
            this.pnlItem.Controls.Add(this.gvItemCostPrice);
            this.pnlItem.Controls.Add(this.label4);
            this.pnlItem.Controls.Add(this.txtRemark);
            this.pnlItem.Controls.Add(this.label8);
            this.pnlItem.Controls.Add(this.gvItemTransQty);
            this.pnlItem.Controls.Add(this.label7);
            this.pnlItem.Controls.Add(this.cmbItemUOM);
            this.pnlItem.Controls.Add(this.label6);
            this.pnlItem.Controls.Add(this.txtItemName);
            this.pnlItem.Controls.Add(this.label5);
            this.pnlItem.Controls.Add(this.txtItemCode);
            this.pnlItem.Location = new System.Drawing.Point(8, 296);
            this.pnlItem.Name = "pnlItem";
            this.pnlItem.Size = new System.Drawing.Size(645, 82);
            this.pnlItem.TabIndex = 5;
            this.pnlItem.Tag = "-1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Item Code :";
            // 
            // txtItemCode
            // 
            this.txtItemCode.BackColor = System.Drawing.Color.Ivory;
            this.txtItemCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemCode.Location = new System.Drawing.Point(14, 19);
            this.txtItemCode.Name = "txtItemCode";
            this.txtItemCode.Size = new System.Drawing.Size(68, 20);
            this.txtItemCode.TabIndex = 0;
            this.TTip.SetToolTip(this.txtItemCode, "Give an Item Code");
            this.txtItemCode.Enter += new System.EventHandler(this.txtItemCode_Enter);
            this.txtItemCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItemCode_KeyDown);
            this.txtItemCode.Leave += new System.EventHandler(this.txtItemCode_Leave);
            this.txtItemCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtItemCode_Validating);
            // 
            // cmbToStockPoint
            // 
            this.cmbToStockPoint.FormattingEnabled = true;
            this.cmbToStockPoint.Location = new System.Drawing.Point(469, 33);
            this.cmbToStockPoint.Name = "cmbToStockPoint";
            this.cmbToStockPoint.Size = new System.Drawing.Size(184, 21);
            this.cmbToStockPoint.TabIndex = 4;
            this.TTip.SetToolTip(this.cmbToStockPoint, "Select Store");
            // 
            // dtpTrfDate
            // 
            this.dtpTrfDate.CustomFormat = "dd/MMM/yy";
            this.dtpTrfDate.Enabled = false;
            this.dtpTrfDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTrfDate.Location = new System.Drawing.Point(75, 33);
            this.dtpTrfDate.Name = "dtpTrfDate";
            this.dtpTrfDate.Size = new System.Drawing.Size(100, 20);
            this.dtpTrfDate.TabIndex = 1;
            this.dtpTrfDate.TabStop = false;
            this.TTip.SetToolTip(this.dtpTrfDate, "Displays KitchenToStoreDate");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(437, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "To :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Trf Date.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Trf No.";
            // 
            // txtTrfNo
            // 
            this.txtTrfNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTrfNo.Enabled = false;
            this.txtTrfNo.Location = new System.Drawing.Point(75, 7);
            this.txtTrfNo.Name = "txtTrfNo";
            this.txtTrfNo.Size = new System.Drawing.Size(100, 20);
            this.txtTrfNo.TabIndex = 0;
            this.txtTrfNo.TabStop = false;
            this.TTip.SetToolTip(this.txtTrfNo, "Displays KitchenToStore No");
            // 
            // frmStockTransfer
            // 
            this.ClientSize = new System.Drawing.Size(672, 464);
            this.Controls.Add(this.PnlStockTrf);
            this.Name = "frmStockTransfer";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stock Transfer";
            this.Load += new System.EventHandler(this.FrmStockTransfer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgStkTransfer)).EndInit();
            this.PnlStockTrf.ResumeLayout(false);
            this.PnlStockTrf.PerformLayout();
            this.pnlItem.ResumeLayout(false);
            this.pnlItem.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}