﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Stock
{
    partial class frmTransactions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label4 = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.txtFilter = new System.Windows.Forms.TextBox();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.cmbPnlRealComparision = new System.Windows.Forms.ComboBox();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.pnlReal = new System.Windows.Forms.Panel();
            this.gvFilterAmount = new ParaSysCom.NumControl();
            this.pnlDate = new System.Windows.Forms.Panel();
            this.chkSelect = new System.Windows.Forms.CheckBox();
            this.dgViewMaster = new System.Windows.Forms.DataGridView();
            this.pnlTrans = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlFilter = new System.Windows.Forms.Panel();
            this.btnBack = new System.Windows.Forms.Button();
            this.lblFilter = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnDisplay = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbFilter = new System.Windows.Forms.ComboBox();
            this.pnlBoolean = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbBoolean = new System.Windows.Forms.ComboBox();
            this.pnlString = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.trView = new System.Windows.Forms.TreeView();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.pnlReal.SuspendLayout();
            this.pnlDate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgViewMaster)).BeginInit();
            this.pnlTrans.SuspendLayout();
            this.pnlFilter.SuspendLayout();
            this.pnlBoolean.SuspendLayout();
            this.pnlString.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(214, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "To :";
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "From :";
            // 
            // txtFilter
            // 
            this.txtFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFilter.Location = new System.Drawing.Point(57, 8);
            this.txtFilter.Name = "txtFilter";
            this.txtFilter.Size = new System.Drawing.Size(164, 20);
            this.txtFilter.TabIndex = 6;
            // 
            // dtTo
            // 
            this.dtTo.CustomFormat = "dd/MMM/yy";
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTo.Location = new System.Drawing.Point(246, 8);
            this.dtTo.Name = "dtTo";
            this.dtTo.ShowCheckBox = true;
            this.dtTo.Size = new System.Drawing.Size(104, 20);
            this.dtTo.TabIndex = 1;
            // 
            // cmbPnlRealComparision
            // 
            this.cmbPnlRealComparision.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPnlRealComparision.FormattingEnabled = true;
            this.cmbPnlRealComparision.Items.AddRange(new object[] {
            "=",
            ">",
            ">=",
            "<",
            "<="});
            this.cmbPnlRealComparision.Location = new System.Drawing.Point(18, 8);
            this.cmbPnlRealComparision.Name = "cmbPnlRealComparision";
            this.cmbPnlRealComparision.Size = new System.Drawing.Size(48, 21);
            this.cmbPnlRealComparision.TabIndex = 1;
            // 
            // dtFrom
            // 
            this.dtFrom.CustomFormat = "dd/MMM/yy";
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFrom.Location = new System.Drawing.Point(57, 8);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.ShowCheckBox = true;
            this.dtFrom.Size = new System.Drawing.Size(104, 20);
            this.dtFrom.TabIndex = 0;
            // 
            // pnlReal
            // 
            this.pnlReal.Controls.Add(this.cmbPnlRealComparision);
            this.pnlReal.Controls.Add(this.gvFilterAmount);
            this.pnlReal.Location = new System.Drawing.Point(206, 14);
            this.pnlReal.Name = "pnlReal";
            this.pnlReal.Size = new System.Drawing.Size(445, 36);
            this.pnlReal.TabIndex = 11;
            this.pnlReal.Visible = false;
            // 
            // gvFilterAmount
            // 
            this.gvFilterAmount.DecimalRequired = false;
            this.gvFilterAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvFilterAmount.Location = new System.Drawing.Point(100, 7);
            this.gvFilterAmount.Name = "gvFilterAmount";
            this.gvFilterAmount.Size = new System.Drawing.Size(121, 20);
            this.gvFilterAmount.SymbolRequired = false;
            this.gvFilterAmount.TabIndex = 0;
            this.gvFilterAmount.Text = "0";
            this.gvFilterAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvFilterAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // pnlDate
            // 
            this.pnlDate.Controls.Add(this.label4);
            this.pnlDate.Controls.Add(this.label3);
            this.pnlDate.Controls.Add(this.dtTo);
            this.pnlDate.Controls.Add(this.dtFrom);
            this.pnlDate.Location = new System.Drawing.Point(206, 14);
            this.pnlDate.Name = "pnlDate";
            this.pnlDate.Size = new System.Drawing.Size(445, 36);
            this.pnlDate.TabIndex = 9;
            this.pnlDate.Visible = false;
            // 
            // chkSelect
            // 
            this.chkSelect.AutoSize = true;
            this.chkSelect.Location = new System.Drawing.Point(6, 5);
            this.chkSelect.Name = "chkSelect";
            this.chkSelect.Size = new System.Drawing.Size(133, 17);
            this.chkSelect.TabIndex = 8;
            this.chkSelect.Text = "Display Selected Items";
            this.chkSelect.UseVisualStyleBackColor = true;
            this.chkSelect.CheckedChanged += new System.EventHandler(this.chkSelect_CheckedChanged);
            // 
            // dgViewMaster
            // 
            this.dgViewMaster.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgViewMaster.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgViewMaster.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgViewMaster.Dock = System.Windows.Forms.DockStyle.Right;
            this.dgViewMaster.Location = new System.Drawing.Point(126, 75);
            this.dgViewMaster.Name = "dgViewMaster";
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgViewMaster.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgViewMaster.Size = new System.Drawing.Size(815, 368);
            this.dgViewMaster.TabIndex = 9;
            this.dgViewMaster.DoubleClick += new System.EventHandler(this.dgViewMaster_DoubleClick);
            this.dgViewMaster.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgViewMaster_KeyDown);
            // 
            // pnlTrans
            // 
            this.pnlTrans.Controls.Add(this.chkSelect);
            this.pnlTrans.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlTrans.Location = new System.Drawing.Point(0, 443);
            this.pnlTrans.Name = "pnlTrans";
            this.pnlTrans.Size = new System.Drawing.Size(941, 27);
            this.pnlTrans.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Like";
            // 
            // pnlFilter
            // 
            this.pnlFilter.Controls.Add(this.btnBack);
            this.pnlFilter.Controls.Add(this.lblFilter);
            this.pnlFilter.Controls.Add(this.btnReset);
            this.pnlFilter.Controls.Add(this.btnDisplay);
            this.pnlFilter.Controls.Add(this.label2);
            this.pnlFilter.Controls.Add(this.cmbFilter);
            this.pnlFilter.Controls.Add(this.pnlBoolean);
            this.pnlFilter.Controls.Add(this.pnlString);
            this.pnlFilter.Controls.Add(this.pnlReal);
            this.pnlFilter.Controls.Add(this.pnlDate);
            this.pnlFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFilter.Location = new System.Drawing.Point(0, 0);
            this.pnlFilter.Name = "pnlFilter";
            this.pnlFilter.Size = new System.Drawing.Size(941, 75);
            this.pnlFilter.TabIndex = 6;
            // 
            // btnBack
            // 
            this.btnBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBack.ImageKey = "exit.gif";
            this.btnBack.ImageList = this.imageList1;
            this.btnBack.Location = new System.Drawing.Point(668, 23);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(79, 25);
            this.btnBack.TabIndex = 15;
            this.btnBack.Text = "Back";
            this.btnBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // lblFilter
            // 
            this.lblFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFilter.Location = new System.Drawing.Point(23, 55);
            this.lblFilter.Name = "lblFilter";
            this.lblFilter.Size = new System.Drawing.Size(901, 14);
            this.lblFilter.TabIndex = 13;
            this.lblFilter.Text = "Filter";
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReset.ImageKey = "refresh_16x16.gif";
            this.btnReset.ImageList = this.imageList1;
            this.btnReset.Location = new System.Drawing.Point(753, 23);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(78, 25);
            this.btnReset.TabIndex = 12;
            this.btnReset.Text = "Reset";
            this.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnDisplay
            // 
            this.btnDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDisplay.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDisplay.ImageKey = "zoom_16x16.gif";
            this.btnDisplay.ImageList = this.imageList1;
            this.btnDisplay.Location = new System.Drawing.Point(837, 23);
            this.btnDisplay.Name = "btnDisplay";
            this.btnDisplay.Size = new System.Drawing.Size(75, 25);
            this.btnDisplay.TabIndex = 9;
            this.btnDisplay.Text = "Display";
            this.btnDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDisplay.UseVisualStyleBackColor = true;
            this.btnDisplay.Click += new System.EventHandler(this.btnDisplay_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Filter by";
            // 
            // cmbFilter
            // 
            this.cmbFilter.FormattingEnabled = true;
            this.cmbFilter.Location = new System.Drawing.Point(26, 22);
            this.cmbFilter.Name = "cmbFilter";
            this.cmbFilter.Size = new System.Drawing.Size(160, 21);
            this.cmbFilter.TabIndex = 7;
            this.cmbFilter.SelectionChangeCommitted += new System.EventHandler(this.cmbFilter_SelectionChangeCommitted);
            // 
            // pnlBoolean
            // 
            this.pnlBoolean.BackColor = System.Drawing.SystemColors.Control;
            this.pnlBoolean.Controls.Add(this.label5);
            this.pnlBoolean.Controls.Add(this.cmbBoolean);
            this.pnlBoolean.Location = new System.Drawing.Point(206, 14);
            this.pnlBoolean.Name = "pnlBoolean";
            this.pnlBoolean.Size = new System.Drawing.Size(432, 36);
            this.pnlBoolean.TabIndex = 14;
            this.pnlBoolean.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(17, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 17);
            this.label5.TabIndex = 1;
            this.label5.Text = "=";
            // 
            // cmbBoolean
            // 
            this.cmbBoolean.FormattingEnabled = true;
            this.cmbBoolean.Items.AddRange(new object[] {
            "FALSE",
            "TRUE"});
            this.cmbBoolean.Location = new System.Drawing.Point(46, 8);
            this.cmbBoolean.Name = "cmbBoolean";
            this.cmbBoolean.Size = new System.Drawing.Size(81, 21);
            this.cmbBoolean.TabIndex = 0;
            // 
            // pnlString
            // 
            this.pnlString.Controls.Add(this.txtFilter);
            this.pnlString.Controls.Add(this.label1);
            this.pnlString.Location = new System.Drawing.Point(206, 14);
            this.pnlString.Name = "pnlString";
            this.pnlString.Size = new System.Drawing.Size(445, 36);
            this.pnlString.TabIndex = 10;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.trView);
            this.panel1.Controls.Add(this.dgViewMaster);
            this.panel1.Controls.Add(this.pnlTrans);
            this.panel1.Controls.Add(this.pnlFilter);
            this.panel1.Location = new System.Drawing.Point(12, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(943, 472);
            this.panel1.TabIndex = 14;
            // 
            // trView
            // 
            this.trView.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.trView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.trView.Dock = System.Windows.Forms.DockStyle.Left;
            this.trView.Location = new System.Drawing.Point(0, 75);
            this.trView.Name = "trView";
            this.trView.Size = new System.Drawing.Size(127, 368);
            this.trView.TabIndex = 10;
            this.trView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.trView_AfterSelect);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.ImageList = this.imageList1;
            this.btnExit.Location = new System.Drawing.Point(839, 486);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(98, 23);
            this.btnExit.TabIndex = 13;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.ImageList = this.imageList1;
            this.btnAdd.Location = new System.Drawing.Point(552, 485);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(85, 24);
            this.btnAdd.TabIndex = 10;
            this.btnAdd.Text = "&Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.ImageKey = "fromat2_16x16.gif";
            this.btnEdit.ImageList = this.imageList1;
            this.btnEdit.Location = new System.Drawing.Point(650, 485);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(85, 24);
            this.btnEdit.TabIndex = 11;
            this.btnEdit.Text = "&Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.ImageList = this.imageList1;
            this.btnDelete.Location = new System.Drawing.Point(748, 485);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(85, 24);
            this.btnDelete.TabIndex = 12;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // frmTransactions
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(954, 522);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnDelete);
            this.Name = "frmTransactions";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Transactions";
            this.Load += new System.EventHandler(this.frmTransactions_Load);
            this.pnlReal.ResumeLayout(false);
            this.pnlReal.PerformLayout();
            this.pnlDate.ResumeLayout(false);
            this.pnlDate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgViewMaster)).EndInit();
            this.pnlTrans.ResumeLayout(false);
            this.pnlTrans.PerformLayout();
            this.pnlFilter.ResumeLayout(false);
            this.pnlFilter.PerformLayout();
            this.pnlBoolean.ResumeLayout(false);
            this.pnlBoolean.PerformLayout();
            this.pnlString.ResumeLayout(false);
            this.pnlString.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
    }
}