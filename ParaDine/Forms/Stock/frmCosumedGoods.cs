﻿using ParaDine.Forms.GlobalForms;
using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Stock
{
    public partial class frmCosumedGoods : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private ComboBox cmbItemUOM;
        private ComboBox cmbStockPoint;
       // private IContainer components;
        private DataGridView dgConsumedItems;
        private DateTimePicker dtpConsumedDate;
        private ConsumedGoods EntId;
        private NumControl gvConsumedQty;
        private NumControl gvTotQty;
        private Label label11;
        private Label label2;
        private Label label3;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label LBLID;
        private Panel pnlItem;
        private Panel PnlMain;
        private ToolTip TTip;
        private TextBox txtItemCode;
        private TextBox txtItemName;

        public frmCosumedGoods()
        {
            this.components = null;
            this.EntId = new ConsumedGoods(0L);
            this.InitializeComponent();
        }

        public frmCosumedGoods(string Mode, long ID, SecurityClass paramSecurity)
        {
            this.components = null;
            this.EntId = new ConsumedGoods(0L);
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes(ID);
            this.EntId.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (ParaDine.GlobalValidations.ValidateFields(this.PnlMain, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_0089;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_00A5;
                            }
                        }
                        else
                        {
                            ParaDine.GlobalFunctions.ClearFields(this.pnlItem);
                            this.EntId.Add(sqlTrans, true);
                        }
                    }
                    goto Label_00B7;
                Label_0089:
                    ParaDine.GlobalFunctions.ClearFields(this.pnlItem);
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_00B7;
                Label_00A5:
                    this.EntId.Delete(sqlTrans, true);
                Label_00B7:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                    sqlTrans.Rollback();
                }
            }
        }

        private void CalculateTotal()
        {
            this.gvTotQty.Value = Convert.ToDecimal(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgConsumedItems.DataSource, "CONSUMEDQty"));
        }

        private void cmbSource_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (this.cmbStockPoint.SelectedIndex >= -1)
            {
                this.EntId = new ConsumedGoods(Convert.ToInt64(this.cmbStockPoint.SelectedValue), Convert.ToDateTime(this.dtpConsumedDate.Text));
                if (this.EntId.ConsumedDate == this.dtpConsumedDate.Value)
                {
                    if (this.EntId.StockPointID == Convert.ToInt64(this.cmbStockPoint.SelectedValue))
                    {
                        this.LoadFields();
                        this.btnTrans.Text = "&Edit";
                    }
                    else
                    {
                        this.dgConsumedItems.DataSource = null;
                        this.btnTrans.Text = "&Add";
                    }
                }
            }
        }

        private void cmbSource_Validating(object sender, CancelEventArgs e)
        {
        }

        private void dgConsumedItems_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                this.pnlItem.Tag = Convert.ToInt16(this.dgConsumedItems.SelectedRows[0].Cells["TableId"].Value) - 1;
                this.LoadTransFields(this.EntId.ConsumedGoodsTransCollection[Convert.ToInt32(this.pnlItem.Tag)]);
                if (this.txtItemCode.Enabled)
                {
                    this.txtItemCode.Focus();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void dgConsumedItems_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    this.EntId.ConsumedGoodsTransCollection[Convert.ToInt16(this.dgConsumedItems.SelectedRows[0].Cells["TableId"].Value) - 1].Deleted = true;
                    this.PopulateView();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    this.dgConsumedItems_CellDoubleClick(sender, new DataGridViewCellEventArgs(0, this.dgConsumedItems.SelectedRows[0].Index));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in dgConsumedGoods_KeyDown");
            }
        }     
        private void dtpConsumedDate_Validated(object sender, EventArgs e)
        {
        }

        private void frmCosumedGoods_Load(object sender, EventArgs e)
        {
            if (!this.EntId.Security.AllowRead)
            {
                throw new Exception("Permission Denied");
            }
            new GlobalTheme().applyTheme(this);
            ParaDine.GlobalFunctions.AddCompHandler(this.PnlMain);
            this.RefreshData();
            this.LoadFields();
            this.pnlItem.Tag = -1;
        }

        private void gvConsumedQty_KeyDownEvent(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) && ParaDine.GlobalValidations.ValidateFields(this.pnlItem, this.TTip))
                {
                    ConsumedGoodsTrans trans;
                    if (Convert.ToInt32(this.pnlItem.Tag) < 0)
                    {
                        trans = new ConsumedGoodsTrans
                        {
                            ConsumedGoodsTransID = 0L,
                            Added = true
                        };
                        this.LoadTransEntities(trans);
                        this.EntId.ConsumedGoodsTransCollection.Add(trans);
                    }
                    else
                    {
                        trans = this.EntId.ConsumedGoodsTransCollection[Convert.ToInt32(this.pnlItem.Tag)];
                        this.LoadTransEntities(trans);
                        trans.Modified = true;
                    }
                    this.pnlItem.Tag = -1;
                    this.PopulateView();
                    this.CalculateTotal();
                    this.txtItemCode.Focus();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in txtRemark_KeyDownEvent");
            }
        }

        public void LoadEntities()
        {
            try
            {
                this.EntId.ConsumedGoodsID = Convert.ToInt64(this.LBLID.Text);
                this.EntId.StockPointID = Convert.ToInt64(this.cmbStockPoint.SelectedValue);
                this.EntId.ConsumedDate = Convert.ToDateTime(this.dtpConsumedDate.Value);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        public void LoadFields()
        {
            this.LBLID.Text = Convert.ToString(this.EntId.ConsumedGoodsID);
            this.cmbStockPoint.SelectedValue = Convert.ToInt64(this.EntId.StockPointID);
            this.dtpConsumedDate.Value = Convert.ToDateTime(this.EntId.ConsumedDate);
            this.PopulateView();
        }

        public void LoadTransEntities(ConsumedGoodsTrans VarConsumedGoodsTrans)
        {
            VarConsumedGoodsTrans.ConsumedGoodsID = this.EntId.ConsumedGoodsID;
            VarConsumedGoodsTrans.ItemID = Convert.ToInt64(this.txtItemCode.Tag);
            VarConsumedGoodsTrans.UOMID = Convert.ToInt32(this.cmbItemUOM.SelectedValue);
            VarConsumedGoodsTrans.ConsumedQty = Convert.ToDouble(this.gvConsumedQty.Value);
            VarConsumedGoodsTrans.CostPrice = Convert.ToDouble(new Item(Convert.ToInt64(this.txtItemCode.Tag)).CostPrice);
        }

        public void LoadTransFields(ConsumedGoodsTrans VarConsumedGoodsTrans)
        {
            Item item = new Item(VarConsumedGoodsTrans.ItemID);
            this.txtItemCode.Text = Convert.ToString(item.ItemCode);
            this.txtItemName.Text = Convert.ToString(item.ItemName);
            this.txtItemCode_Validating(new object(), new CancelEventArgs());
            this.cmbItemUOM.SelectedValue = Convert.ToInt32(VarConsumedGoodsTrans.UOMID);
            this.gvConsumedQty.Value = Convert.ToDecimal(VarConsumedGoodsTrans.ConsumedQty);
        }

        private void PopulateView()
        {
            ParaDine.GlobalFill.FillGridView(this.dgConsumedItems, this.EntId.ConsumedGoodsTransCollection.GetCollectionTable());
            this.dgConsumedItems.Refresh();
            this.dgConsumedItems.AllowUserToAddRows = false;
            this.dgConsumedItems.AllowUserToDeleteRows = false;
            this.dgConsumedItems.AllowUserToOrderColumns = false;
            this.dgConsumedItems.AllowUserToResizeColumns = false;
            this.dgConsumedItems.AllowUserToResizeRows = false;
            this.dgConsumedItems.EditMode = DataGridViewEditMode.EditProgrammatically;
            this.dgConsumedItems.ReadOnly = true;
            this.dgConsumedItems.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgConsumedItems.RowTemplate.ReadOnly = true;
            this.dgConsumedItems.StandardTab = false;
            this.dgConsumedItems.RowsDefaultCellStyle.SelectionBackColor = Color.SeaShell;
            this.SetVwColWidth();
            this.CalculateTotal();
        }

        private void RefreshData()
        {
            ParaDine.GlobalFill.FillCombo("SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT WHERE ISBILLPOINT = 0", this.cmbStockPoint);
            this.dtpConsumedDate.Value = ParaDine.GlobalVariables.BusinessDate;
        }

        private void SetVwColWidth()
        {
            try
            {
                this.dgConsumedItems.Columns[0].Visible = false;
                this.dgConsumedItems.Columns[1].Visible = false;
                this.dgConsumedItems.Columns[2].Width = 80;
                this.dgConsumedItems.Columns[3].Width = 190;
                this.dgConsumedItems.Columns[4].Width = 110;
                this.dgConsumedItems.Columns[5].Width = 100;
                for (int i = 6; i < this.dgConsumedItems.Columns.Count; i++)
                {
                    this.dgConsumedItems.Columns[i].Visible = false;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Setting Column Width");
            }
        }

        private void txtItemCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F1)
                {
                    string paramSql = "SELECT * FROM RES_VW_ITEM WHERE NONINVENTORY = 0 OR RAWMATERIAL = 1";
                    new frmBrowseData(ParaDine.GlobalFill.FillDataTable(paramSql), this.txtItemCode, "ITEMS", 0).ShowDialog();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in ItemCode_KeyDown");
            }
        }

        private void txtItemCode_Validating(object sender, CancelEventArgs e)
        {
            if (this.txtItemCode.Text.Trim() != "")
            {
                Item item = new Item(this.txtItemCode.Text);
                if (!item.NonInventory || item.RawMaterial)
                {
                    this.txtItemCode.Tag = item.ItemID;
                    this.txtItemCode.Text = item.ItemCode;
                    this.txtItemName.Text = item.ItemName;
                    DataTable uOM = item.GetUOM();
                    if (uOM.Rows.Count > 0)
                    {
                        ParaDine.GlobalFill.FillCombo(uOM, this.cmbItemUOM);
                        if (uOM.Rows.Count == 1)
                        {
                            this.cmbItemUOM.Enabled = false;
                            this.cmbItemUOM.TabStop = false;
                        }
                        else
                        {
                            this.cmbItemUOM.Enabled = true;
                            this.cmbItemUOM.TabStop = true;
                        }
                        this.cmbItemUOM.SelectedIndex = 0;
                    }
                    else
                    {
                        this.cmbItemUOM.Enabled = true;
                        this.cmbItemUOM.TabStop = true;
                        ParaDine.GlobalFill.FillCombo("SELECT UOMID, UOMNAME FROM RES_UOM", this.cmbItemUOM);
                        this.cmbItemUOM.SelectedValue = item.UOMID;
                    }
                    if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                    {
                        this.gvConsumedQty.Value = 1M;
                    }
                }
                else
                {
                    MessageBox.Show("Please Check the Item Code");
                    this.txtItemCode.SelectAll();
                    this.txtItemCode.Focus();
                }
            }
            else
            {
                e.Cancel = true;
            }
        }
        private void cmbStockPoint_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
