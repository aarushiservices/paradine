﻿namespace ParaDine.Forms.Stock
{
    partial class frmCosumedGoods
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PnlMain = new System.Windows.Forms.Panel();
            this.gvTotQty = new ParaSysCom.NumControl();
            this.label11 = new System.Windows.Forms.Label();
            this.LBLID = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnTrans = new System.Windows.Forms.Button();
            this.pnlItem = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.gvConsumedQty = new ParaSysCom.NumControl();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbItemUOM = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtItemCode = new System.Windows.Forms.TextBox();
            this.dgConsumedItems = new System.Windows.Forms.DataGridView();
            this.cmbStockPoint = new System.Windows.Forms.ComboBox();
            this.dtpConsumedDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.PnlMain.SuspendLayout();
            this.pnlItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgConsumedItems)).BeginInit();
            this.SuspendLayout();
            // 
            // PnlMain
            // 
            this.PnlMain.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.PnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlMain.Controls.Add(this.gvTotQty);
            this.PnlMain.Controls.Add(this.label11);
            this.PnlMain.Controls.Add(this.LBLID);
            this.PnlMain.Controls.Add(this.btnExit);
            this.PnlMain.Controls.Add(this.btnTrans);
            this.PnlMain.Controls.Add(this.pnlItem);
            this.PnlMain.Controls.Add(this.dgConsumedItems);
            this.PnlMain.Controls.Add(this.cmbStockPoint);
            this.PnlMain.Controls.Add(this.dtpConsumedDate);
            this.PnlMain.Controls.Add(this.label3);
            this.PnlMain.Controls.Add(this.label2);
            this.PnlMain.Location = new System.Drawing.Point(3, 5);
            this.PnlMain.Name = "PnlMain";
            this.PnlMain.Size = new System.Drawing.Size(524, 324);
            this.PnlMain.TabIndex = 0;
            // 
            // gvTotQty
            // 
            this.gvTotQty.DecimalRequired = true;
            this.gvTotQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvTotQty.Location = new System.Drawing.Point(405, 290);
            this.gvTotQty.Name = "gvTotQty";
            this.gvTotQty.Size = new System.Drawing.Size(84, 20);
            this.gvTotQty.SymbolRequired = false;
            this.gvTotQty.TabIndex = 30;
            this.gvTotQty.Text = "0.00";
            this.gvTotQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvTotQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(337, 296);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 28;
            this.label11.Text = "Total Qty :";
            // 
            // LBLID
            // 
            this.LBLID.AutoSize = true;
            this.LBLID.Location = new System.Drawing.Point(365, 23);
            this.LBLID.Name = "LBLID";
            this.LBLID.Size = new System.Drawing.Size(35, 13);
            this.LBLID.TabIndex = 27;
            this.LBLID.Text = "label1";
            this.LBLID.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.CausesValidation = false;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(90, 290);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(9, 290);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 3;
            this.btnTrans.Text = "Trans";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // pnlItem
            // 
            this.pnlItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlItem.Controls.Add(this.label8);
            this.pnlItem.Controls.Add(this.gvConsumedQty);
            this.pnlItem.Controls.Add(this.label7);
            this.pnlItem.Controls.Add(this.cmbItemUOM);
            this.pnlItem.Controls.Add(this.label6);
            this.pnlItem.Controls.Add(this.txtItemName);
            this.pnlItem.Controls.Add(this.label5);
            this.pnlItem.Controls.Add(this.txtItemCode);
            this.pnlItem.Location = new System.Drawing.Point(7, 200);
            this.pnlItem.Name = "pnlItem";
            this.pnlItem.Size = new System.Drawing.Size(508, 83);
            this.pnlItem.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(331, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Consumed Qty :";
            // 
            // gvConsumedQty
            // 
            this.gvConsumedQty.DecimalRequired = true;
            this.gvConsumedQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvConsumedQty.Location = new System.Drawing.Point(333, 22);
            this.gvConsumedQty.Name = "gvConsumedQty";
            this.gvConsumedQty.Size = new System.Drawing.Size(108, 20);
            this.gvConsumedQty.SymbolRequired = false;
            this.gvConsumedQty.TabIndex = 26;
            this.gvConsumedQty.Tag = "NoHandler";
            this.gvConsumedQty.Text = "0.00";
            this.gvConsumedQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvConsumedQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvConsumedQty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gvConsumedQty_KeyDownEvent);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(144, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "UOM :";
            // 
            // cmbItemUOM
            // 
            this.cmbItemUOM.BackColor = System.Drawing.Color.Ivory;
            this.cmbItemUOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbItemUOM.FormattingEnabled = true;
            this.cmbItemUOM.Location = new System.Drawing.Point(141, 23);
            this.cmbItemUOM.Name = "cmbItemUOM";
            this.cmbItemUOM.Size = new System.Drawing.Size(176, 21);
            this.cmbItemUOM.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Item Name :";
            // 
            // txtItemName
            // 
            this.txtItemName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemName.Location = new System.Drawing.Point(89, 51);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(391, 20);
            this.txtItemName.TabIndex = 20;
            this.txtItemName.TabStop = false;
            this.txtItemName.Tag = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(55, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Item Code :";
            // 
            // txtItemCode
            // 
            this.txtItemCode.BackColor = System.Drawing.Color.Ivory;
            this.txtItemCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemCode.Location = new System.Drawing.Point(53, 23);
            this.txtItemCode.Name = "txtItemCode";
            this.txtItemCode.Size = new System.Drawing.Size(82, 20);
            this.txtItemCode.TabIndex = 0;
            this.txtItemCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItemCode_KeyDown);
            this.txtItemCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtItemCode_Validating);
            // 
            // dgConsumedItems
            // 
            this.dgConsumedItems.BackgroundColor = System.Drawing.Color.White;
            this.dgConsumedItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgConsumedItems.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgConsumedItems.Location = new System.Drawing.Point(7, 50);
            this.dgConsumedItems.Name = "dgConsumedItems";
            this.dgConsumedItems.RowHeadersVisible = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.SeaShell;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgConsumedItems.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgConsumedItems.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgConsumedItems.Size = new System.Drawing.Size(508, 142);
            this.dgConsumedItems.TabIndex = 9;
            this.dgConsumedItems.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgConsumedItems_CellDoubleClick);
            this.dgConsumedItems.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgConsumedItems_KeyDown);
            // 
            // cmbStockPoint
            // 
            this.cmbStockPoint.FormattingEnabled = true;
            this.cmbStockPoint.Location = new System.Drawing.Point(171, 23);
            this.cmbStockPoint.Name = "cmbStockPoint";
            this.cmbStockPoint.Size = new System.Drawing.Size(176, 21);
            this.cmbStockPoint.TabIndex = 1;
            this.cmbStockPoint.SelectedIndexChanged += new System.EventHandler(this.cmbStockPoint_SelectedIndexChanged);
            this.cmbStockPoint.SelectionChangeCommitted += new System.EventHandler(this.cmbSource_SelectionChangeCommitted);
            this.cmbStockPoint.Validating += new System.ComponentModel.CancelEventHandler(this.cmbSource_Validating);
            // 
            // dtpConsumedDate
            // 
            this.dtpConsumedDate.CustomFormat = "dd/MMM/yy";
            this.dtpConsumedDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpConsumedDate.Location = new System.Drawing.Point(18, 22);
            this.dtpConsumedDate.Name = "dtpConsumedDate";
            this.dtpConsumedDate.Size = new System.Drawing.Size(108, 20);
            this.dtpConsumedDate.TabIndex = 0;
            this.dtpConsumedDate.Validated += new System.EventHandler(this.dtpConsumedDate_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(169, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Stock Point :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Consumed Date :";
            // 
            // frmCosumedGoods
            // 
            this.ClientSize = new System.Drawing.Size(531, 334);
            this.Controls.Add(this.PnlMain);
            this.Name = "frmCosumedGoods";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consumed Goods";
            this.Load += new System.EventHandler(this.frmCosumedGoods_Load);
            this.PnlMain.ResumeLayout(false);
            this.PnlMain.PerformLayout();
            this.pnlItem.ResumeLayout(false);
            this.pnlItem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgConsumedItems)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
    }
}