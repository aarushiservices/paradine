﻿using ParaDine.Forms.GlobalForms;
using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Stock
{
    public partial class frmStockEntry : Form
    {
        private ParButton btnExit;
        private ParButton btnTrans;
        private ComboBox cmbItemUOM;
        private ComboBox cmbStockPoint;
        private ComboBox cmbSupplier;
       // private IContainer components;
        private DataGridView dgvwStock;
        private DateTimePicker dtpInvoiceDate;
        private DateTimePicker dtpStockEntryDate;
        private StockEntryMaster EntID;
        private NumControl gvFreeQty;
        private NumControl gvFreightChrgs;
        private NumControl gvGrossAmt;
        private NumControl gvItemAmount;
        private NumControl gvItemCostPrice;
        private NumControl gvItemDiscPerc;
        private NumControl gvItemOrdQty;
        private NumControl gvItemTaxPerc;
        private NumControl gvQtyinHand;
        private NumControl gvReceivedQty;
        private NumControl gvTotAmount;
        private NumControl gvTotDiscAmount;
        private NumControl gvTotDiscPerc;
        private NumControl gvTotItemDisc;
        private NumControl gvTotItemTax;
        private NumControl gvTotPaidAmt;
        private NumControl gvTotQty;
        private NumControl gvTotTaxAmount;
        private NumControl gvTotTaxPerc;
        private Label label1;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label13;
        private Label label14;
        private Label label15;
        private Label label16;
        private Label label17;
        private Label label18;
        private Label label19;
        private Label label2;
        private Label label20;
        private Label label21;
        private Label label22;
        private Label label23;
        private Label label24;
        private Label label25;
        private Label label26;
        private Label label27;
        private Label label28;
        private Label label29;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label lblitem;
        private Label lblPayment;
        private Panel pnlItem;
        private Panel pnlStock;
        private ToolTip TTip;
        private TextBox txtInvoiceNo;
        private TextBox txtItemCode;
        private TextBox txtItemName;
        private TextBox txtPoNumber;
        private TextBox txtStockEntryNo;
        private Level uLevel;
        private Item varItem;
        private POMaster VarPOMaster;

        public frmStockEntry()
        {
            this.EntID = new StockEntryMaster(0L);
            this.VarPOMaster = new POMaster(0L);
            this.components = null;
            this.InitializeComponent();
        }

        public frmStockEntry(string Mode, long ID, SecurityClass paramSecurity)
        {
            this.EntID = new StockEntryMaster(0L);
            this.VarPOMaster = new POMaster(0L);
            this.components = null;
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntID.LoadAttributes(ID);
            this.EntID.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if ((this.dgvwStock.Rows.Count > 0) && ParaDine.GlobalValidations.ValidateFields(this.pnlStock, this.TTip))
            {
                try
                {
                    if (Convert.ToDouble(this.gvTotAmount.Value) > 0.0)
                    {
                        this.SaveDetails();
                    }
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error In Saving");
                }
            }
        }

        private void CalculateItemTotal()
        {
            this.gvItemAmount.Value = ((Convert.ToDecimal(this.gvItemCostPrice.Value) * Convert.ToDecimal(this.gvReceivedQty.Value)) + Convert.ToDecimal(this.gvItemTaxPerc.Value)) - Convert.ToDecimal(this.gvItemDiscPerc.Value);
        }

        private void CalculateTotal()
        {
            if (this.dgvwStock.DataSource != null)
            {
                this.gvGrossAmt.Value = Convert.ToDecimal(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgvwStock.DataSource, "NetAmount"));
                this.gvTotQty.Value = Convert.ToDecimal((double)(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgvwStock.DataSource, "RcvdQty") + ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgvwStock.DataSource, "FreeQty")));
                this.gvTotItemTax.Value = Convert.ToDecimal(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgvwStock.DataSource, "TaxAmt"));
                this.gvTotItemDisc.Value = Convert.ToDecimal(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgvwStock.DataSource, "DiscAmt"));
                this.gvTotAmount.Value = Convert.ToDecimal((decimal)(((Convert.ToDecimal(ParaDine.GlobalFunctions.GetColumnTotal((DataTable)this.dgvwStock.DataSource, "NetAmount")) + Convert.ToDecimal(this.gvTotTaxAmount.Value)) - Convert.ToDecimal(this.gvTotDiscAmount.Value)) + Convert.ToDecimal(this.gvFreightChrgs.Value)));
            }
        }

        private void cmbItemUOM_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if ((((this.cmbItemUOM.SelectedIndex != -1) && (Convert.ToInt16(this.pnlItem.Tag) < 0)) && (this.cmbItemUOM.ValueMember != "")) && (this.varItem != null))
                {
                    this.gvItemCostPrice.Value = Convert.ToDecimal(this.varItem.GetCostPrice(Convert.ToInt32(this.cmbItemUOM.SelectedValue)));
                    this.gvQtyinHand.Value = Convert.ToDecimal(this.varItem.GetStockPointStock(Convert.ToInt32(this.cmbItemUOM.SelectedValue), Convert.ToInt32(this.cmbStockPoint.SelectedValue)));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "ERROR IN CMBITEMUOM_SELECTEDINDEXCHANGED");
            }
        }

        private void cmbItemUOM_SelectionChangeCommitted(object sender, EventArgs e)
        {
        }

        private void dgvwStock_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgvwStock.SelectedRows.Count > 0)
            {
                this.pnlItem.Tag = Convert.ToInt16(this.dgvwStock.SelectedRows[0].Cells["TableId"].Value) - 1;
                this.varItem = new Item(this.EntID.StockEntryTransCollection[Convert.ToInt32(this.pnlItem.Tag)].ItemID);
                this.LoadTransFields(this.EntID.StockEntryTransCollection[Convert.ToInt32(this.pnlItem.Tag)]);
                if (this.txtItemCode.Enabled)
                {
                    this.txtItemCode.Focus();
                }
            }
        }

        private void dgvwStock_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    this.EntID.StockEntryTransCollection[Convert.ToInt32(this.dgvwStock.SelectedRows[0].Cells["TABLEID"].Value) - 1].Deleted = true;
                    this.PopulateView();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    this.dgvwStock_CellDoubleClick(sender, new DataGridViewCellEventArgs(0, this.dgvwStock.SelectedRows[0].Index));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

       

        private void frmStockEntry_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntID.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                this.uLevel = new Level(new User(ParaDine.GlobalVariables.UserID).LevelID);
                new GlobalTheme().applyTheme(this);
                ParaDine.GlobalFunctions.AddCompHandler(this.pnlStock);
                this.RefreshData();
                this.LoadFields();
                this.pnlItem.Tag = -1;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Form Load");
                base.Close();
            }
        }

        private void gvDiscAmount_Change(object sender, EventArgs e)
        {
            this.CalculateItemTotal();
        }

        private void gvDiscAmount_KeyDownEvent(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) && ParaDine.GlobalValidations.ValidateFields(this.pnlItem, this.TTip))
                {
                    StockEntryTrans trans;
                    if (Convert.ToInt32(this.pnlItem.Tag) < 0)
                    {
                        trans = new StockEntryTrans
                        {
                            StockEntryTransID = 0L,
                            Added = true
                        };
                        this.LoadTransEntities(trans);
                        trans.StockEntryTransID = 0L;
                        this.EntID.StockEntryTransCollection.Add(trans);
                    }
                    else
                    {
                        trans = this.EntID.StockEntryTransCollection[Convert.ToInt32(this.pnlItem.Tag)];
                        this.LoadTransEntities(trans);
                        trans.Modified = true;
                    }
                    if ((trans.CostPrice != this.varItem.GetCostPrice(trans.UOMID)) && (MessageBox.Show("CostPrice Is Different for this UOM.\nDo U Want To Change This ????", "Change CP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes))
                    {
                        this.varItem.SetCostPrice(trans.UOMID, trans.CostPrice);
                    }
                    trans.Dispose();
                    this.varItem.Dispose();
                    this.pnlItem.Tag = -1;
                    this.PopulateView();
                    if (this.txtItemCode.Enabled)
                    {
                        this.txtItemCode.Focus();
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in gvItemCostPrice_KeyDownEvent");
            }
        }

        private void gvFreightChrgs_Change(object sender, EventArgs e)
        {
            this.CalculateTotal();
        }

        private void gvItemCostPrice_Change(object sender, EventArgs e)
        {
            this.CalculateItemTotal();
        }

        private void gvTaxAmount_Change(object sender, EventArgs e)
        {
            this.CalculateItemTotal();
        }

        private void gvTot_KeyPressEvent(object sender, KeyPressEventArgs e)
        {
        }

        private void gvTotDiscAmount_Change(object sender, EventArgs e)
        {
            this.CalculateTotal();
        }

        private void gvTotDiscPerc_KeyDownEvent(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.pnlItem.Focus();
            }
        }

        private void gvTotDiscPerc_Validated(object sender, EventArgs e)
        {
            double num = 0.0;
            if (Convert.ToDouble(this.gvTotTaxAmount.Value) == 0.0)
            {
                num = Convert.ToDouble(this.gvGrossAmt.Value);
                this.gvTotDiscAmount.Value = Convert.ToDecimal((decimal)((Convert.ToDecimal(num) * Convert.ToDecimal(this.gvTotDiscPerc.Value)) / 100M));
            }
            else
            {
                num = Convert.ToDouble(this.gvGrossAmt.Value) + Convert.ToDouble(this.gvTotTaxAmount.Value);
                this.gvTotDiscAmount.Value = Convert.ToDecimal((decimal)((Convert.ToDecimal(num) * Convert.ToDecimal(this.gvTotDiscPerc.Value)) / 100M));
            }
        }

        private void gvTotTaxAmount_Change(object sender, EventArgs e)
        {
            this.CalculateTotal();
        }

        private void gvTotTaxPerc_KeyDownEvent(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.pnlItem.Focus();
            }
        }

        private void gvTotTaxPerc_Validated(object sender, EventArgs e)
        {
            double num = 0.0;
            if (Convert.ToDouble(this.gvTotDiscAmount.Value) == 0.0)
            {
                num = Convert.ToDouble(this.gvGrossAmt.Value);
                this.gvTotTaxAmount.Value = Convert.ToDecimal((decimal)((Convert.ToDecimal(num) * Convert.ToDecimal(this.gvTotTaxPerc.Value)) / 100M));
            }
            else
            {
                num = Convert.ToDouble(this.gvGrossAmt.Value) - Convert.ToDouble(this.gvTotDiscAmount.Value);
                this.gvTotTaxAmount.Value = Convert.ToDecimal((decimal)((Convert.ToDecimal(num) * Convert.ToDecimal(this.gvTotTaxPerc.Value)) / 100M));
            }
        }

     

        private void lblPayment_Click(object sender, EventArgs e)
        {
            if (ParaDine.GlobalValidations.ValidateFields(this.pnlStock, this.TTip))
            {
                try
                {
                    if (Convert.ToDouble(this.gvTotAmount.Value) > 0.0)
                    {
                        frmMoneyTrans trans = new frmMoneyTrans("&Add", 0L, Convert.ToDouble(this.gvTotAmount.Value), true);
                        if (this.EntID.EntMoneyMaster.MoneyTransCollection.Count > 0)
                        {
                            trans = new frmMoneyTrans("&Edit", this.EntID.EntMoneyMaster.MoneyMasterID, Convert.ToDouble(this.gvTotAmount.Value), true);
                        }
                        trans.ShowDialog();
                        if (trans.DialogResult == DialogResult.Yes)
                        {
                            this.EntID.EntMoneyMaster = trans.GetMoneyMaster;
                            if (this.EntID.EntMoneyMaster.GetTransAmount != Math.Round(Convert.ToDouble(this.gvTotAmount.Value), 2))
                            {
                                throw new Exception("PaidAmount is Not Sufficient");
                            }
                            this.EntID.EntMoneyMaster.TransactionSource = "GRN";
                            this.gvTotPaidAmt.Value = Convert.ToDecimal(this.EntID.EntMoneyMaster.GetTransAmount);
                        }
                    }
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error In Saving");
                }
            }
        }

        private void LoadEntities()
        {
            this.EntID.StockEntryMasterID = Convert.ToInt64(this.txtStockEntryNo.Tag);
            this.EntID.StockEntryNo = Convert.ToString(this.txtStockEntryNo.Text);
            this.EntID.StockEntryDate = Convert.ToDateTime(this.dtpStockEntryDate.Text);
            this.EntID.POMasterID = Convert.ToInt64(this.txtPoNumber.Tag);
            this.EntID.SupplierID = Convert.ToInt64(this.cmbSupplier.SelectedValue);
            this.EntID.InvoiceNo = Convert.ToString(this.txtInvoiceNo.Text);
            this.EntID.InvoiceDate = Convert.ToDateTime(this.dtpInvoiceDate.Text);
            this.EntID.TaxAmount = Convert.ToDouble(this.gvTotTaxAmount.Value);
            this.EntID.DiscAmount = Convert.ToDouble(this.gvTotDiscAmount.Value);
            this.EntID.FreightCharges = Convert.ToDouble(this.gvFreightChrgs.Value);
            this.EntID.CompanyID = 1;
            this.EntID.StockPointId = Convert.ToInt64(this.cmbStockPoint.SelectedValue);
        }

        private void LoadFields()
        {
            POMaster master = new POMaster(this.EntID.POMasterID);
            this.txtStockEntryNo.Tag = Convert.ToInt64(this.EntID.StockEntryMasterID);
            if (this.EntID.StockEntryMasterID <= 0L)
            {
                this.txtStockEntryNo.Text = this.EntID.GetMaxCode;
                this.txtStockEntryNo.ReadOnly = true;
            }
            else
            {
                this.txtStockEntryNo.Text = Convert.ToString(this.EntID.StockEntryNo);
            }
            if (this.EntID.POMasterID > 0L)
            {
                this.txtPoNumber.Tag = this.EntID.POMasterID;
                this.txtPoNumber.Text = master.PONumber;
            }
            this.dtpStockEntryDate.Value = Convert.ToDateTime(this.EntID.StockEntryDate);
            this.txtInvoiceNo.Text = Convert.ToString(this.EntID.InvoiceNo);
            this.dtpInvoiceDate.Value = Convert.ToDateTime(this.EntID.InvoiceDate);
            this.cmbSupplier.SelectedValue = Convert.ToInt64(this.EntID.SupplierID);
            this.cmbStockPoint.SelectedValue = Convert.ToInt64(this.EntID.StockPointId);
            this.gvTotTaxAmount.Value = Convert.ToDecimal(this.EntID.TaxAmount);
            this.gvTotDiscAmount.Value = Convert.ToDecimal(this.EntID.DiscAmount);
            this.gvFreightChrgs.Value = Convert.ToDecimal(this.EntID.FreightCharges);
            this.gvTotPaidAmt.Value = Convert.ToDecimal(this.EntID.EntMoneyMaster.GetTransAmount);
            this.PopulateView();
        }

        private void LoadTransEntities(StockEntryTrans VarStockEntryTrans)
        {
            VarStockEntryTrans.StockEntryMasterID = this.EntID.StockEntryMasterID;
            VarStockEntryTrans.ItemID = Convert.ToInt64(this.txtItemCode.Tag);
            VarStockEntryTrans.UOMID = Convert.ToInt32(this.cmbItemUOM.SelectedValue);
            VarStockEntryTrans.ReceivedQty = Convert.ToDouble(this.gvReceivedQty.Value);
            VarStockEntryTrans.FreeQty = Convert.ToDouble(this.gvFreeQty.Value);
            VarStockEntryTrans.TaxPerc = Convert.ToDouble(this.gvItemTaxPerc.Value);
            VarStockEntryTrans.DiscPerc = Convert.ToDouble(this.gvItemDiscPerc.Value);
            VarStockEntryTrans.CostPrice = Convert.ToDouble(this.gvItemCostPrice.Value);
        }

        private void LoadTransFields(StockEntryTrans VarStockEntryTrans)
        {
            if (this.varItem != null)
            {
                this.txtItemCode.Text = Convert.ToString(this.varItem.ItemCode);
                this.txtItemName.Text = Convert.ToString(this.varItem.ItemName);
                this.txtItemCode_Validating(new object(), new CancelEventArgs());
                this.cmbItemUOM.SelectedValue = Convert.ToInt32(VarStockEntryTrans.UOMID);
                this.gvReceivedQty.Value = Convert.ToDecimal(VarStockEntryTrans.ReceivedQty);
                this.gvItemOrdQty.Value = Convert.ToDecimal(VarStockEntryTrans.OrderedQty);
                this.gvFreeQty.Value = Convert.ToDecimal(VarStockEntryTrans.FreeQty);
                this.gvItemCostPrice.Value = Convert.ToDecimal(VarStockEntryTrans.CostPrice);
                this.gvItemTaxPerc.Value = Convert.ToDecimal(VarStockEntryTrans.TaxPerc);
                this.gvItemDiscPerc.Value = Convert.ToDecimal(VarStockEntryTrans.DiscPerc);
                this.gvItemAmount.Value = Convert.ToDecimal(VarStockEntryTrans.GetItemAmount);
            }
        }

        private void PopulateView()
        {
            ParaDine.GlobalFill.FillGridView(this.dgvwStock, this.EntID.StockEntryTransCollection.GetColectionTable());
            this.dgvwStock.Refresh();
            this.dgvwStock.AllowUserToAddRows = false;
            this.dgvwStock.AllowUserToDeleteRows = false;
            this.dgvwStock.AllowUserToOrderColumns = false;
            this.dgvwStock.AllowUserToResizeColumns = false;
            this.dgvwStock.AllowUserToResizeRows = false;
            this.dgvwStock.EditMode = DataGridViewEditMode.EditProgrammatically;
            this.dgvwStock.ReadOnly = true;
            this.dgvwStock.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgvwStock.RowTemplate.ReadOnly = true;
            this.dgvwStock.StandardTab = false;
            this.dgvwStock.RowsDefaultCellStyle.SelectionBackColor = Color.SeaShell;
            this.SetVwColWidth();
            this.CalculateTotal();
        }

        private void RefreshData()
        {
            try
            {
                ParaDine.GlobalFill.FillCombo("SELECT SUPPLIERID, SUPPLIERNAME FROM RES_SUPPLIER", this.cmbSupplier);
                ParaDine.GlobalFill.FillCombo("SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT", this.cmbStockPoint);
                if (this.uLevel.IsAdmin)
                {
                    this.dtpStockEntryDate.Enabled = true;
                }
                this.dtpStockEntryDate.Value = ParaDine.GlobalVariables.BusinessDate;
                this.dtpInvoiceDate.Value = ParaDine.GlobalVariables.BusinessDate;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In RefreshData");
            }
        }

        private void SaveDetails()
        {
            this.LoadEntities();
            SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
            try
            {
                string str = this.btnTrans.Text.ToUpper();
                if (str != null)
                {
                    if (!(str == "&ADD"))
                    {
                        if (str == "&EDIT")
                        {
                            goto Label_0061;
                        }
                        if (str == "&DELETE")
                        {
                            goto Label_0071;
                        }
                    }
                    else
                    {
                        this.EntID.Add(sqlTrans, true);
                    }
                }
                goto Label_0083;
            Label_0061:
                this.EntID.Modify(sqlTrans, true);
                goto Label_0083;
            Label_0071:
                this.EntID.Delete(sqlTrans, true);
            Label_0083:
                sqlTrans.Commit();
                base.Close();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                sqlTrans.Rollback();
            }
        }

        private void SetVwColWidth()
        {
            try
            {
                this.dgvwStock.Columns[0].Visible = false;
                this.dgvwStock.Columns[1].Width = 140;
                this.dgvwStock.Columns[2].Width = 0x55;
                this.dgvwStock.Columns[3].Width = 0x41;
                this.dgvwStock.Columns[4].Width = 0x41;
                this.dgvwStock.Columns[5].Width = 0x41;
                this.dgvwStock.Columns[6].Width = 80;
                this.dgvwStock.Columns[7].Width = 50;
                this.dgvwStock.Columns[8].Width = 50;
                this.dgvwStock.Columns[9].Width = 80;
                for (int i = 10; i < this.dgvwStock.Columns.Count; i++)
                {
                    this.dgvwStock.Columns[i].Visible = false;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Setting Column Width");
            }
        }

        private void txtItemCode_Enter(object sender, EventArgs e)
        {
            this.lblitem.Visible = true;
        }

        private void txtItemCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F1)
                {
                    string paramSql = "SELECT * FROM RES_VW_ITEM WHERE NONINVENTORY = 0 ";
                    new frmBrowseData(ParaDine.GlobalFill.FillDataTable(paramSql), this.txtItemCode, "ITEMS", 2).ShowDialog();
                }
                else if (e.KeyCode == Keys.F2)
                {
                    if (this.gvTotTaxPerc.Enabled)
                    {
                        this.gvTotTaxPerc.Focus();
                    }
                }
                else if (e.KeyCode == Keys.F3)
                {
                    if (this.gvTotDiscPerc.Enabled)
                    {
                        this.gvTotDiscPerc.Focus();
                    }
                }
                else if (e.KeyCode == Keys.F4)
                {
                    this.lblPayment_Click(sender, new EventArgs());
                }
                else if (e.KeyCode == Keys.Up)
                {
                    this.dgvwStock.Focus();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in ItemCode_KeyDown");
            }
        }

        private void txtItemCode_Leave(object sender, EventArgs e)
        {
            this.lblitem.Visible = false;
        }

        private void txtItemCode_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if ((this.txtItemCode.Text.Trim() != "") && (Convert.ToInt32(this.cmbStockPoint.SelectedValue) > 0))
                {
                    this.varItem = new Item(this.txtItemCode.Text);
                    if (!this.varItem.NonInventory)
                    {
                        this.txtItemCode.Tag = this.varItem.ItemID;
                        this.txtItemCode.Text = this.varItem.ItemCode;
                        this.txtItemName.Text = this.varItem.ItemName;
                        DataTable uOM = this.varItem.GetUOM();
                        if (uOM.Rows.Count > 0)
                        {
                            ParaDine.GlobalFill.FillCombo(uOM, this.cmbItemUOM);
                            if (uOM.Rows.Count == 1)
                            {
                                this.cmbItemUOM.Enabled = false;
                                this.cmbItemUOM.TabStop = false;
                                this.cmbItemUOM.SelectedIndex = 0;
                            }
                            else
                            {
                                if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                                {
                                    this.cmbItemUOM.Enabled = true;
                                    this.cmbItemUOM.TabStop = true;
                                    this.cmbItemUOM.SelectedIndex = 0;
                                }
                                else
                                {
                                    this.cmbItemUOM.Enabled = false;
                                    this.cmbItemUOM.Text = Convert.ToString(this.dgvwStock.SelectedRows[0].Cells["UOM"].Value);
                                }
                                this.cmbItemUOM.TabStop = true;
                            }
                        }
                        else
                        {
                            this.cmbItemUOM.Enabled = true;
                            this.cmbItemUOM.TabStop = true;
                            ParaDine.GlobalFill.FillCombo("SELECT UOMID, UOMNAME FROM RES_UOM", this.cmbItemUOM);
                            this.cmbItemUOM.SelectedValue = this.varItem.UOMID;
                            if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                            {
                                this.gvItemCostPrice.Value = Convert.ToDecimal(this.varItem.CostPrice);
                            }
                        }
                        if (Convert.ToInt16(this.pnlItem.Tag) == -1)
                        {
                            this.gvReceivedQty.Value = 1M;
                            this.gvFreeQty.Value = 0M;
                            this.cmbItemUOM.SelectedIndex = 0;
                            this.gvItemDiscPerc.Value = 0M;
                            this.gvItemTaxPerc.Value = 0M;
                        }
                        else
                        {
                            this.cmbItemUOM.Text = Convert.ToString(this.dgvwStock.SelectedRows[0].Cells["UOM"].Value);
                        }
                        this.CalculateItemTotal();
                    }
                    else
                    {
                        MessageBox.Show("Please Check the Item Code");
                        this.txtItemCode.SelectAll();
                        this.txtItemCode.Focus();
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in ItemCode Validation");
                e.Cancel = true;
            }
        }

        private void txtPoNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.F1) && (this.btnTrans.Text.ToUpper() == "&ADD"))
            {
                string paramSql = "SELECT * FROM RES_VW_POMASTER WHERE PENDING = 'TRUE'";
                new frmBrowseData(ParaDine.GlobalFill.FillDataTable(paramSql), this.txtPoNumber, "PONUMBER", 0).ShowDialog();
            }
        }

        private void txtPoNumber_Leave(object sender, EventArgs e)
        {
            if (this.btnTrans.Text.ToUpper() == "&ADD")
            {
                if (this.txtPoNumber.Text.Trim() != "")
                {
                    POMaster master = new POMaster(this.txtPoNumber.Text.Trim());
                    if (master.POMasterID > 0L)
                    {
                        if (this.btnTrans.Text.ToUpper() == "&ADD")
                        {
                            this.EntID.StockEntryTransCollection.Clear();
                            this.EntID.POMasterID = master.POMasterID;
                            this.EntID.SupplierID = master.SupplierID;
                            this.EntID.LoadAttributes(this.EntID.StockEntryMasterID);
                            this.LoadFields();
                            this.cmbSupplier.Enabled = false;
                            this.cmbSupplier.TabStop = false;
                        }
                    }
                    else
                    {
                        this.cmbSupplier.Text = "";
                        this.cmbSupplier.Enabled = true;
                        this.cmbSupplier.TabStop = true;
                        MessageBox.Show("PONumber Does Not Exists!");
                        this.txtPoNumber.SelectAll();
                        this.txtPoNumber.Focus();
                    }
                }
                else
                {
                    this.cmbSupplier.Text = "";
                    this.cmbSupplier.Enabled = true;
                    this.cmbSupplier.TabStop = true;
                }
            }
        }

        private void txtPoNumber_Validated(object sender, EventArgs e)
        {
            if (this.txtPoNumber.Text.Trim() != "")
            {
                POMaster master = new POMaster(this.txtPoNumber.Text.Trim());
                if (master.POMasterID > 0L)
                {
                    if (this.btnTrans.Text.ToUpper() == "&ADD")
                    {
                        this.EntID.StockEntryTransCollection.Clear();
                        this.EntID.POMasterID = master.POMasterID;
                        this.EntID.SupplierID = master.SupplierID;
                        this.EntID.LoadAttributes(this.EntID.StockEntryMasterID);
                        this.LoadFields();
                        this.cmbSupplier.Enabled = false;
                        this.cmbSupplier.TabStop = false;
                    }
                }
                else
                {
                    this.cmbSupplier.Text = "";
                    this.cmbSupplier.Enabled = true;
                    this.cmbSupplier.TabStop = true;
                    MessageBox.Show("PONumber Does Not Exists!");
                    this.txtPoNumber.SelectAll();
                    this.txtPoNumber.Focus();
                }
            }
            else
            {
                this.cmbSupplier.Text = "";
                this.cmbSupplier.Enabled = true;
                this.cmbSupplier.TabStop = true;
            }
        }

        private void txtPoNumber_Validating(object sender, CancelEventArgs e)
        {
            if (this.txtPoNumber.Text.Trim() != "")
            {
                POMaster master = new POMaster(this.txtPoNumber.Text.Trim());
                if (master.POMasterID > 0L)
                {
                    if (this.btnTrans.Text.ToUpper() == "&ADD")
                    {
                        this.EntID.StockEntryTransCollection.Clear();
                        this.EntID.POMasterID = master.POMasterID;
                        this.EntID.SupplierID = master.SupplierID;
                        this.EntID.LoadAttributes(this.EntID.StockEntryMasterID);
                        this.LoadFields();
                        this.cmbSupplier.Enabled = false;
                        this.cmbSupplier.TabStop = false;
                    }
                }
                else
                {
                    this.cmbSupplier.Text = "";
                    this.cmbSupplier.Enabled = true;
                    this.cmbSupplier.TabStop = true;
                    MessageBox.Show("PONumber Does Not Exists!");
                    this.txtPoNumber.SelectAll();
                    this.txtPoNumber.Focus();
                }
            }
            else
            {
                this.cmbSupplier.Text = "";
                this.cmbSupplier.Enabled = true;
                this.cmbSupplier.TabStop = true;
            }
        }
    }
}
