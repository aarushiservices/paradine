﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Stock
{
    partial class FrmStockWriteOff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PnlMain = new System.Windows.Forms.Panel();
            this.gvTotalAmount = new ParaSysCom.NumControl();
            this.label10 = new System.Windows.Forms.Label();
            this.lblitem = new System.Windows.Forms.Label();
            this.gvTotalQty = new ParaSysCom.NumControl();
            this.label11 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnTrans = new System.Windows.Forms.Button();
            this.pnlItem = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.gvItemAmount = new ParaSysCom.NumControl();
            this.label4 = new System.Windows.Forms.Label();
            this.gvItemCostPrice = new ParaSysCom.NumControl();
            this.label25 = new System.Windows.Forms.Label();
            this.gvQtyinHand = new ParaSysCom.NumControl();
            this.label8 = new System.Windows.Forms.Label();
            this.gvPhysicalQty = new ParaSysCom.NumControl();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbItemUOM = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtItemCode = new System.Windows.Forms.TextBox();
            this.dgStockWriteOff = new System.Windows.Forms.DataGridView();
            this.cmbStockPoint = new System.Windows.Forms.ComboBox();
            this.dtpStockWriteOffDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStockWriteOffNo = new System.Windows.Forms.TextBox();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.PnlMain.SuspendLayout();
            this.pnlItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgStockWriteOff)).BeginInit();
            this.SuspendLayout();
            // 
            // PnlMain
            // 
            this.PnlMain.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.PnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlMain.Controls.Add(this.gvTotalAmount);
            this.PnlMain.Controls.Add(this.label10);
            this.PnlMain.Controls.Add(this.lblitem);
            this.PnlMain.Controls.Add(this.gvTotalQty);
            this.PnlMain.Controls.Add(this.label11);
            this.PnlMain.Controls.Add(this.btnExit);
            this.PnlMain.Controls.Add(this.btnTrans);
            this.PnlMain.Controls.Add(this.pnlItem);
            this.PnlMain.Controls.Add(this.dgStockWriteOff);
            this.PnlMain.Controls.Add(this.cmbStockPoint);
            this.PnlMain.Controls.Add(this.dtpStockWriteOffDate);
            this.PnlMain.Controls.Add(this.label3);
            this.PnlMain.Controls.Add(this.label2);
            this.PnlMain.Controls.Add(this.label1);
            this.PnlMain.Controls.Add(this.txtStockWriteOffNo);
            this.PnlMain.Location = new System.Drawing.Point(5, 5);
            this.PnlMain.Name = "PnlMain";
            this.PnlMain.Size = new System.Drawing.Size(511, 362);
            this.PnlMain.TabIndex = 0;
            // 
            // gvTotalAmount
            // 
            this.gvTotalAmount.BackColor = System.Drawing.Color.White;
            this.gvTotalAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvTotalAmount.DecimalRequired = true;
            this.gvTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvTotalAmount.Location = new System.Drawing.Point(386, 329);
            this.gvTotalAmount.Name = "gvTotalAmount";
            this.gvTotalAmount.Size = new System.Drawing.Size(117, 20);
            this.gvTotalAmount.SymbolRequired = true;
            this.gvTotalAmount.TabIndex = 46;
            this.gvTotalAmount.TabStop = false;
            this.gvTotalAmount.Text = "`0.00";
            this.gvTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvTotalAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(294, 336);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 13);
            this.label10.TabIndex = 42;
            this.label10.Text = "Total Amount :";
            // 
            // lblitem
            // 
            this.lblitem.AutoSize = true;
            this.lblitem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblitem.ForeColor = System.Drawing.Color.Red;
            this.lblitem.Location = new System.Drawing.Point(5, 300);
            this.lblitem.Name = "lblitem";
            this.lblitem.Size = new System.Drawing.Size(173, 13);
            this.lblitem.TabIndex = 41;
            this.lblitem.Text = "Press F1 For Listing all items.";
            this.lblitem.Visible = false;
            // 
            // gvTotalQty
            // 
            this.gvTotalQty.BackColor = System.Drawing.Color.White;
            this.gvTotalQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvTotalQty.DecimalRequired = true;
            this.gvTotalQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvTotalQty.Location = new System.Drawing.Point(386, 300);
            this.gvTotalQty.Name = "gvTotalQty";
            this.gvTotalQty.Size = new System.Drawing.Size(117, 20);
            this.gvTotalQty.SymbolRequired = false;
            this.gvTotalQty.TabIndex = 26;
            this.gvTotalQty.TabStop = false;
            this.gvTotalQty.Text = "0.00";
            this.gvTotalQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvTotalQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(317, 307);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Total Qty :";
            // 
            // btnExit
            // 
            this.btnExit.CausesValidation = false;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(87, 331);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 26);
            this.btnExit.TabIndex = 6;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(4, 331);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(76, 26);
            this.btnTrans.TabIndex = 5;
            this.btnTrans.Text = "Trans";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // pnlItem
            // 
            this.pnlItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlItem.Controls.Add(this.label12);
            this.pnlItem.Controls.Add(this.txtRemarks);
            this.pnlItem.Controls.Add(this.label9);
            this.pnlItem.Controls.Add(this.gvItemAmount);
            this.pnlItem.Controls.Add(this.label4);
            this.pnlItem.Controls.Add(this.gvItemCostPrice);
            this.pnlItem.Controls.Add(this.label25);
            this.pnlItem.Controls.Add(this.gvQtyinHand);
            this.pnlItem.Controls.Add(this.label8);
            this.pnlItem.Controls.Add(this.gvPhysicalQty);
            this.pnlItem.Controls.Add(this.label7);
            this.pnlItem.Controls.Add(this.cmbItemUOM);
            this.pnlItem.Controls.Add(this.label6);
            this.pnlItem.Controls.Add(this.txtItemName);
            this.pnlItem.Controls.Add(this.label5);
            this.pnlItem.Controls.Add(this.txtItemCode);
            this.pnlItem.Location = new System.Drawing.Point(7, 208);
            this.pnlItem.Name = "pnlItem";
            this.pnlItem.Size = new System.Drawing.Size(495, 87);
            this.pnlItem.TabIndex = 4;
            this.pnlItem.Tag = "-1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(228, 45);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 52;
            this.label12.Text = "Remarks :";
            // 
            // txtRemarks
            // 
            this.txtRemarks.BackColor = System.Drawing.Color.White;
            this.txtRemarks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRemarks.Location = new System.Drawing.Point(230, 61);
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(258, 20);
            this.txtRemarks.TabIndex = 7;
            this.txtRemarks.Tag = "";
            this.txtRemarks.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRemarks_KeyDown);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(414, 5);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 50;
            this.label9.Text = "Amount :";
            // 
            // gvItemAmount
            // 
            this.gvItemAmount.BackColor = System.Drawing.Color.White;
            this.gvItemAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvItemAmount.DecimalRequired = true;
            this.gvItemAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemAmount.Location = new System.Drawing.Point(414, 20);
            this.gvItemAmount.Name = "gvItemAmount";
            this.gvItemAmount.Size = new System.Drawing.Size(70, 20);
            this.gvItemAmount.SymbolRequired = true;
            this.gvItemAmount.TabIndex = 5;
            this.gvItemAmount.TabStop = false;
            this.gvItemAmount.Tag = "";
            this.gvItemAmount.Text = "`0.00";
            this.gvItemAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvItemAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(352, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Cost Price";
            // 
            // gvItemCostPrice
            // 
            this.gvItemCostPrice.BackColor = System.Drawing.Color.White;
            this.gvItemCostPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvItemCostPrice.DecimalRequired = true;
            this.gvItemCostPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemCostPrice.Location = new System.Drawing.Point(350, 20);
            this.gvItemCostPrice.Name = "gvItemCostPrice";
            this.gvItemCostPrice.Size = new System.Drawing.Size(57, 20);
            this.gvItemCostPrice.SymbolRequired = true;
            this.gvItemCostPrice.TabIndex = 4;
            this.gvItemCostPrice.Tag = "";
            this.gvItemCostPrice.Text = "`0.00";
            this.gvItemCostPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvItemCostPrice.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvItemCostPrice.TextChanged += new System.EventHandler(this.gvItemCostPrice_Change);
            this.gvItemCostPrice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gvItemCostPrice_KeyDownEvent);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(288, 5);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(37, 13);
            this.label25.TabIndex = 46;
            this.label25.Text = "QOH :";
            // 
            // gvQtyinHand
            // 
            this.gvQtyinHand.BackColor = System.Drawing.Color.White;
            this.gvQtyinHand.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvQtyinHand.DecimalRequired = true;
            this.gvQtyinHand.Enabled = false;
            this.gvQtyinHand.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvQtyinHand.Location = new System.Drawing.Point(286, 20);
            this.gvQtyinHand.Name = "gvQtyinHand";
            this.gvQtyinHand.Size = new System.Drawing.Size(57, 20);
            this.gvQtyinHand.SymbolRequired = false;
            this.gvQtyinHand.TabIndex = 3;
            this.gvQtyinHand.TabStop = false;
            this.gvQtyinHand.Text = "0.00";
            this.gvQtyinHand.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvQtyinHand.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(233, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Qty :";
            // 
            // gvPhysicalQty
            // 
            this.gvPhysicalQty.BackColor = System.Drawing.Color.Ivory;
            this.gvPhysicalQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvPhysicalQty.DecimalRequired = true;
            this.gvPhysicalQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvPhysicalQty.Location = new System.Drawing.Point(233, 20);
            this.gvPhysicalQty.Name = "gvPhysicalQty";
            this.gvPhysicalQty.Size = new System.Drawing.Size(46, 20);
            this.gvPhysicalQty.SymbolRequired = false;
            this.gvPhysicalQty.TabIndex = 2;
            this.gvPhysicalQty.Tag = "";
            this.gvPhysicalQty.Text = "0.00";
            this.gvPhysicalQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvPhysicalQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvPhysicalQty.TextChanged += new System.EventHandler(this.gvPhysicalQty_Change);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(94, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "UOM :";
            // 
            // cmbItemUOM
            // 
            this.cmbItemUOM.BackColor = System.Drawing.Color.Ivory;
            this.cmbItemUOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbItemUOM.FormattingEnabled = true;
            this.cmbItemUOM.Location = new System.Drawing.Point(92, 20);
            this.cmbItemUOM.Name = "cmbItemUOM";
            this.cmbItemUOM.Size = new System.Drawing.Size(132, 21);
            this.cmbItemUOM.TabIndex = 1;
            this.cmbItemUOM.SelectedValueChanged += new System.EventHandler(this.cmbItemUOM_SelectedValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 45);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Item Name :";
            // 
            // txtItemName
            // 
            this.txtItemName.BackColor = System.Drawing.Color.White;
            this.txtItemName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemName.Location = new System.Drawing.Point(6, 61);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.ReadOnly = true;
            this.txtItemName.Size = new System.Drawing.Size(218, 20);
            this.txtItemName.TabIndex = 6;
            this.txtItemName.TabStop = false;
            this.txtItemName.Tag = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Item Code :";
            // 
            // txtItemCode
            // 
            this.txtItemCode.BackColor = System.Drawing.Color.Ivory;
            this.txtItemCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemCode.Location = new System.Drawing.Point(6, 20);
            this.txtItemCode.Name = "txtItemCode";
            this.txtItemCode.Size = new System.Drawing.Size(76, 20);
            this.txtItemCode.TabIndex = 0;
            this.txtItemCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItemCode_KeyDown);
            this.txtItemCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtItemCode_Validating);
            // 
            // dgStockWriteOff
            // 
            this.dgStockWriteOff.BackgroundColor = System.Drawing.Color.White;
            this.dgStockWriteOff.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgStockWriteOff.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgStockWriteOff.Location = new System.Drawing.Point(7, 54);
            this.dgStockWriteOff.Name = "dgStockWriteOff";
            this.dgStockWriteOff.RowHeadersVisible = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.SeaShell;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgStockWriteOff.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgStockWriteOff.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgStockWriteOff.Size = new System.Drawing.Size(495, 150);
            this.dgStockWriteOff.TabIndex = 4;
            this.dgStockWriteOff.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgStockWriteOff_CellDoubleClick);
            this.dgStockWriteOff.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgStockWriteOff_KeyDown);
            // 
            // cmbStockPoint
            // 
            this.cmbStockPoint.FormattingEnabled = true;
            this.cmbStockPoint.Location = new System.Drawing.Point(116, 28);
            this.cmbStockPoint.Name = "cmbStockPoint";
            this.cmbStockPoint.Size = new System.Drawing.Size(173, 21);
            this.cmbStockPoint.TabIndex = 2;
            // 
            // dtpStockWriteOffDate
            // 
            this.dtpStockWriteOffDate.CustomFormat = "dd/MMM/yy";
            this.dtpStockWriteOffDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStockWriteOffDate.Location = new System.Drawing.Point(358, 4);
            this.dtpStockWriteOffDate.Name = "dtpStockWriteOffDate";
            this.dtpStockWriteOffDate.Size = new System.Drawing.Size(100, 20);
            this.dtpStockWriteOffDate.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "StockPoint :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(244, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Stock Write-Off Date :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Stock Write-Off No. :";
            // 
            // txtStockWriteOffNo
            // 
            this.txtStockWriteOffNo.Location = new System.Drawing.Point(116, 4);
            this.txtStockWriteOffNo.Name = "txtStockWriteOffNo";
            this.txtStockWriteOffNo.Size = new System.Drawing.Size(84, 20);
            this.txtStockWriteOffNo.TabIndex = 0;
            // 
            // FrmStockWriteOff
            // 
            this.ClientSize = new System.Drawing.Size(521, 372);
            this.Controls.Add(this.PnlMain);
            this.Name = "FrmStockWriteOff";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stock Write-Off";
            this.Load += new System.EventHandler(this.FrmStockWriteOff_Load);
            this.PnlMain.ResumeLayout(false);
            this.PnlMain.PerformLayout();
            this.pnlItem.ResumeLayout(false);
            this.pnlItem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgStockWriteOff)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
    }
}