﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Printing
{
    partial class FrmPrinting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlPrintOptions = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMno = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpBillDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.txtKOTNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBillNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbPrintType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbTarget = new System.Windows.Forms.ComboBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.pnlPrintOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlPrintOptions
            // 
            this.pnlPrintOptions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPrintOptions.Controls.Add(this.label6);
            this.pnlPrintOptions.Controls.Add(this.txtMno);
            this.pnlPrintOptions.Controls.Add(this.label5);
            this.pnlPrintOptions.Controls.Add(this.dtpBillDate);
            this.pnlPrintOptions.Controls.Add(this.label4);
            this.pnlPrintOptions.Controls.Add(this.txtKOTNo);
            this.pnlPrintOptions.Controls.Add(this.label3);
            this.pnlPrintOptions.Controls.Add(this.txtBillNo);
            this.pnlPrintOptions.Controls.Add(this.label2);
            this.pnlPrintOptions.Controls.Add(this.cmbPrintType);
            this.pnlPrintOptions.Controls.Add(this.label1);
            this.pnlPrintOptions.Controls.Add(this.cmbTarget);
            this.pnlPrintOptions.Location = new System.Drawing.Point(11, 8);
            this.pnlPrintOptions.Name = "pnlPrintOptions";
            this.pnlPrintOptions.Size = new System.Drawing.Size(226, 175);
            this.pnlPrintOptions.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(35, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "MNo :";
            // 
            // txtMno
            // 
            this.txtMno.BackColor = System.Drawing.Color.Ivory;
            this.txtMno.Location = new System.Drawing.Point(84, 10);
            this.txtMno.Name = "txtMno";
            this.txtMno.Size = new System.Drawing.Size(121, 20);
            this.txtMno.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Bill Date :";
            // 
            // dtpBillDate
            // 
            this.dtpBillDate.CalendarMonthBackground = System.Drawing.Color.Ivory;
            this.dtpBillDate.CalendarTitleForeColor = System.Drawing.Color.Ivory;
            this.dtpBillDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpBillDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBillDate.Location = new System.Drawing.Point(84, 88);
            this.dtpBillDate.Name = "dtpBillDate";
            this.dtpBillDate.Size = new System.Drawing.Size(121, 20);
            this.dtpBillDate.TabIndex = 8;
            this.dtpBillDate.Value = new System.DateTime(2008, 5, 24, 0, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "KOT No. :";
            // 
            // txtKOTNo
            // 
            this.txtKOTNo.Location = new System.Drawing.Point(84, 62);
            this.txtKOTNo.Name = "txtKOTNo";
            this.txtKOTNo.Size = new System.Drawing.Size(121, 20);
            this.txtKOTNo.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Bill No. :";
            // 
            // txtBillNo
            // 
            this.txtBillNo.BackColor = System.Drawing.Color.Ivory;
            this.txtBillNo.Location = new System.Drawing.Point(84, 36);
            this.txtBillNo.Name = "txtBillNo";
            this.txtBillNo.Size = new System.Drawing.Size(121, 20);
            this.txtBillNo.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Print Type :";
            // 
            // cmbPrintType
            // 
            this.cmbPrintType.BackColor = System.Drawing.Color.Ivory;
            this.cmbPrintType.FormattingEnabled = true;
            this.cmbPrintType.Items.AddRange(new object[] {
            "KOT",
            "Bill"});
            this.cmbPrintType.Location = new System.Drawing.Point(84, 114);
            this.cmbPrintType.Name = "cmbPrintType";
            this.cmbPrintType.Size = new System.Drawing.Size(121, 21);
            this.cmbPrintType.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 145);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Send To :";
            // 
            // cmbTarget
            // 
            this.cmbTarget.BackColor = System.Drawing.Color.Ivory;
            this.cmbTarget.FormattingEnabled = true;
            this.cmbTarget.Items.AddRange(new object[] {
            "Printer",
            "Screen",
            "Disk"});
            this.cmbTarget.Location = new System.Drawing.Point(84, 141);
            this.cmbTarget.Name = "cmbTarget";
            this.cmbTarget.Size = new System.Drawing.Size(121, 21);
            this.cmbTarget.TabIndex = 0;
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(35, 197);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(86, 26);
            this.btnPrint.TabIndex = 1;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(127, 197);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(86, 26);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // FrmPrinting
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(248, 230);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.pnlPrintOptions);
            this.Name = "FrmPrinting";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Print Dialog";
            this.Load += new System.EventHandler(this.FrmPrinting_Load);
            this.pnlPrintOptions.ResumeLayout(false);
            this.pnlPrintOptions.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}