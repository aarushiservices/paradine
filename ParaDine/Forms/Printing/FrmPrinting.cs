﻿//using CrystalDecisions.Shared;
using ParaDine.Reports;
using ParaDinePrint;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Printing
{
    public partial class FrmPrinting : Form
    {
        private Button btnClose;
        private Button btnPrint;
        private ComboBox cmbPrintType;
        private ComboBox cmbTarget;
        //private IContainer components;
        private DataSet ds;
        private DataSet DSX;
        private DateTimePicker dtpBillDate;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private PrintingFunctions pfunctions;
        private Panel pnlPrintOptions;
        private ClsColDosPrint[] prnItem;
        private ClsRptDosPrint prnrpt;
        private string PrntTo;
        private string rptname;
        private SqlDataAdapter sda;
        private SqlCommand SQLCMD;
        private TextBox txtBillNo;
        private TextBox txtKOTNo;
        private TextBox txtMno;
        private BillSettings varBillSettings;

        public FrmPrinting()
        {
            this.rptname = "";
            this.pfunctions = new PrintingFunctions();
            this.prnrpt = new ClsRptDosPrint();
            this.ds = new DataSet();
            this.sda = new SqlDataAdapter();
            this.PrntTo = "";
            this.SQLCMD = new SqlCommand();
            this.DSX = new DataSet();
            this.varBillSettings = new BillSettings(1);
            this.components = null;
            this.InitializeComponent();
            this.PrntTo = "Printer";
        }

        public FrmPrinting(long ID, string Type)
        {
            this.rptname = "";
            this.pfunctions = new PrintingFunctions();
            this.prnrpt = new ClsRptDosPrint();
            this.ds = new DataSet();
            this.sda = new SqlDataAdapter();
            this.PrntTo = "";
            this.SQLCMD = new SqlCommand();
            this.DSX = new DataSet();
            this.varBillSettings = new BillSettings(1);
            this.components = null;
            this.InitializeComponent();
            CustOrderMaster master = new CustOrderMaster(ID);
            this.txtMno.Text = Convert.ToString(master.MachineNo);
            this.txtBillNo.Text = Convert.ToString(master.CustOrderNo);
            if (master.CustOrderKOTCollection.Count > 0)
            {
                this.txtKOTNo.Text = Convert.ToString(master.CustOrderKOTCollection[0].KOTNumber);
            }
            this.dtpBillDate.Value = Convert.ToDateTime(master.CustOrderDate);
            this.cmbPrintType.Text = Type;
            this.PrntTo = "Printer";
        }

        public FrmPrinting(int Mno, string CustOrdNo, string KOTNo, DateTime Billdt, string type)
        {
            this.rptname = "";
            this.pfunctions = new PrintingFunctions();
            this.prnrpt = new ClsRptDosPrint();
            this.ds = new DataSet();
            this.sda = new SqlDataAdapter();
            this.PrntTo = "";
            this.SQLCMD = new SqlCommand();
            this.DSX = new DataSet();
            this.varBillSettings = new BillSettings(1);
            this.components = null;
            this.InitializeComponent();
            this.txtMno.Text = Convert.ToString(Mno);
            this.txtBillNo.Text = CustOrdNo;
            this.txtKOTNo.Text = KOTNo;
            this.dtpBillDate.Text = Convert.ToString(Billdt);
            this.cmbPrintType.Text = type;
            this.PrntTo = this.cmbTarget.Text;
        }

        private bool AreEqual(object obj1, object obj2)
        {
            if (object.ReferenceEquals(obj1, DBNull.Value) & object.ReferenceEquals(obj2, DBNull.Value))
            {
                return true;
            }
            if (object.ReferenceEquals(obj1, DBNull.Value) | object.ReferenceEquals(obj2, DBNull.Value))
            {
                return false;
            }
            return (obj1 == obj2);
        }

        private bool BillPageHeader(ref ClsRptDosPrint ParamClsRpt, string ParamWhere)
        {
            try
            {
                string str = "";
                ParamClsRpt.set_PageHeader(0, Convert.ToString(GlobalFunctions.GetQueryValue("SELECT STOCKPOINTNAME FROM RES_VW_CUSTORDERREPORT WHERE " + ParamWhere)));
                if (Convert.ToBoolean(GlobalFunctions.GetQueryValue("SELECT SETTLED FROM RES_VW_CUSTORDERREPORT WHERE " + ParamWhere)))
                {
                    if (Convert.ToInt16(GlobalFunctions.GetQueryValue("SELECT DUPLICATENO FROM RES_VW_CUSTORDERREPORT WHERE " + ParamWhere)) > 0)
                    {
                        str = "DUPLICATE BILL";
                    }
                }
                else
                {
                    str = "TEMPORARY BILL";
                }
                ParamClsRpt.set_PageHeader(1, str);
                if (Convert.ToBoolean(GlobalFunctions.GetQueryValue("SELECT DIRECTBILL FROM RES_CUSTORDERMASTER WHERE " + ParamWhere)))
                {
                    ParamClsRpt.set_PageHeader(2, this.varBillSettings.DirectBillHeader);
                }
                else
                {
                    ParamClsRpt.set_PageHeader(2, this.varBillSettings.KOTBillHeader);
                }
                return true;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Bill Page Header");
                return false;
            }
        }

        public void BillPrint(long intCustOrderId)
        {
            int num;
            string str4;
            ClsDosPrint print;
            string paramSql = "";
            this.rptname = "BILL";
            if (this.cmbPrintType.Text != "")
            {
            }
            string paramWhere = " CUSTORDERMASTERID = " + Convert.ToString(intCustOrderId);
            this.pfunctions.SETREPORTDATA(ref this.prnrpt, this.rptname, this.ds, this.sda, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
            this.pfunctions.SETCOLDATA(ref this.prnItem, this.rptname, this.ds, this.sda, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
            paramSql = "SELECT * FROM RES_VW_CUSTORDERREPORT WHERE " + paramWhere;
            if (GlobalVariables.blnNegQtyReq)
            {
                paramSql = paramSql + " AND SALEQTY <> 0 ";
            }
            this.BillPageHeader(ref this.prnrpt, paramWhere);
            string str3 = "";
            for (num = 0; num <= (this.prnItem.Length - 1); num++)
            {
                str4 = "";
                if (this.prnItem[num].ISGroupHeader)
                {
                    str4 = (this.prnItem[num].ColumnPosition + 1) + ",";
                }
                str3 = str3 + str4;
            }
            if (str3.Trim() != "")
            {
                paramSql = paramSql + " ORDER BY " + str3.Trim().Substring(1, str3.Length - 1);
            }
            GlobalFill.FillDataSet(paramSql, "RPTTRANS", this.ds, this.sda);
            if (this.ds.Tables["RPTTRANS"].Rows.Count > 0)
            {
                //    if ((new StockPoint((long)GlobalVariables.StockPointId).BillFormat == 1) && (this.cmbTarget.Text.ToUpper() != "DISK"))
                //    {
                //        num = 0;
                //        while (num < 1)
                //        {
                ///By Bharat we have to change Reports to itextsparp pdf
                ///
                RptBill1 bill = new RptBill1(this.ds.Tables["RPTTRANS"]);
                bill.Parameter_BillHeader2 = this.varBillSettings.BILLHEADER2;
                bill.Parameter_BillHeader3 = this.varBillSettings.BILLHEADER3;
                bill.Parameter_BillHeader4 = this.varBillSettings.BILLHEADER4;
                bill.ShowDialog();
                //RptBill1 bill = new RptBill1();
                //bill.SetDataSource(this.ds.Tables["RPTTRANS"]);
                //ParameterDiscreteValue value2 = new ParameterDiscreteValue
                //{
                //    Value = this.varBillSettings.BILLHEADER1
                //};
                //bill.ParameterFields["BillHeader1"].CurrentValues.Add((ParameterValue)value2);
                //value2 = new ParameterDiscreteValue
                //{
                //    Value = this.varBillSettings.BILLHEADER2
                //};
                //bill.ParameterFields["BillHeader2"].CurrentValues.Add((ParameterValue)value2);
                //value2 = new ParameterDiscreteValue
                //{
                //    Value = this.varBillSettings.BILLHEADER3
                //};
                //bill.ParameterFields["BillHeader3"].CurrentValues.Add((ParameterValue)value2);
                //value2 = new ParameterDiscreteValue
                //{
                //    Value = this.varBillSettings.BILLHEADER4
                //};
                //bill.ParameterFields["BillHeader4"].CurrentValues.Add((ParameterValue)value2);
                //bill.PrintToPrinter(1, false, 1, 1);
                //num++;
                //    }
                //}
                //else
                //{
                //    print = new ClsDosPrint(this.ds.Tables["RPTTRANS"], this.prnItem, this.prnrpt, GlobalVariables.SqlConn, GlobalVariables.PrinterId);
                //    if (this.PrntTo == "")
                //    {
                //        print.PrintData(this.cmbTarget.Text, Convert.ToString(GlobalVariables.BILLPRINTINGDEVICE));
                //    }
                //    else
                //    {
                //        print.PrintData(this.PrntTo, Convert.ToString(GlobalVariables.BILLPRINTINGDEVICE));
                //    }
                //}
            }
            if (GlobalVariables.blnStubReq && (new CustOrderMaster(intCustOrderId).SourceTypeId == 0))
            {
                this.rptname = "STUB";
                GlobalFill.FillDataSet("SELECT DISTINCT SECTIONID FROM RES_VW_CUSTORDERREPORT WHERE " + paramWhere, "ALLDEPT", this.ds, this.sda);
                for (int i = 0; i <= (this.ds.Tables["ALLDEPT"].Rows.Count - 1); i++)
                {
                    this.pfunctions.SETREPORTDATA(ref this.prnrpt, this.rptname, this.ds, this.sda, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
                    this.pfunctions.SETCOLDATA(ref this.prnItem, this.rptname, this.ds, this.sda, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
                    paramSql = "SELECT * FROM RES_VW_CUSTORDERREPORT";
                    if ((paramWhere != "") && (paramWhere != ""))
                    {
                        paramSql = paramSql + " WHERE SECTIONID = " + this.ds.Tables["ALLDEPT"].Rows[i][0].ToString() + " AND " + paramWhere;
                    }
                    this.STUBPageHeader(ref this.prnrpt, Convert.ToInt16(this.ds.Tables["ALLDEPT"].Rows[i][0].ToString()));
                    for (num = 0; num <= (this.prnItem.Length - 1); num++)
                    {
                        str4 = "";
                        if (this.prnItem[num].ISGroupHeader)
                        {
                            str4 = (this.prnItem[num].ColumnPosition + 1) + ",";
                        }
                        str3 = str3 + str4;
                    }
                    if (str3.Trim() != "")
                    {
                        paramSql = paramSql + " ORDER BY " + str3.Trim().Substring(1, str3.Length - 1);
                    }
                    GlobalFill.FillDataSet(paramSql, "RPTTRANS", this.ds, this.sda);
                    if ((this.ds.Tables["RPTTRANS"].Rows.Count > 0) && Convert.ToBoolean(this.ds.Tables["RPTTRANS"].Rows[0]["DIRECTBILL"]))
                    {
                        print = new ClsDosPrint(this.ds.Tables["RPTTRANS"], this.prnItem, this.prnrpt, GlobalVariables.SqlConn, GlobalVariables.PrinterId);
                        if (this.PrntTo == "")
                        {
                            print.PrintData(this.cmbTarget.Text, Convert.ToString(GlobalFunctions.GetQueryValue("SELECT PRINTERNAME FROM RES_SECTION WHERE SECTIONID = " + this.ds.Tables["ALLDEPT"].Rows[i][0].ToString())));
                        }
                        else
                        {
                            print.PrintData(this.PrntTo, Convert.ToString(GlobalFunctions.GetQueryValue("SELECT PRINTERNAME FROM RES_SECTION WHERE SECTIONID = " + this.ds.Tables["ALLDEPT"].Rows[i][0].ToString())));
                        }
                    }
                }
            }
            string cmdText = "";
            cmdText = "UPDATE RES_CUSTORDERMASTER SET DUPLICATENO = DUPLICATENO + 1 WHERE " + paramWhere + " AND CUSTORDERMASTERID IN (SELECT TRANSACTIONSOURCEID FROM GLB_MONEYMASTER WHERE TRANSACTIONSOURCE = 'BILL') ";
            this.SQLCMD = new SqlCommand(cmdText, GlobalVariables.SqlConn);
            this.SQLCMD.ExecuteNonQuery();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (this.cmbPrintType.Text.ToUpper() == "BILL")
            {
                this.BillPrint(this.GETCUSTORDERMASTERID());
            }
            else
            {
                this.KOTPrint(this.GETCUSTORDERKOTID());
            }
            this.PrntTo = this.cmbTarget.Text;
        }



        private void FrmPrinting_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
        }

        private long GETCUSTORDERKOTID()
        {
            return Convert.ToInt64(GlobalFunctions.GetQueryValue("SELECT CUSTORDERKOTID FROM RES_CUSTORDERMASTER COM, RES_CUSTORDERKOT CKT  WHERE COM.CUSTORDERMASTERID = CKT.CUSTORDERMASTERID  AND CUSTORDERNO =  '" + this.txtBillNo.Text.Trim() + "' AND MACHINENO = " + GlobalVariables.TerminalNo.ToString() + " AND CONVERT(DATETIME, CONVERT(VARCHAR, CUSTORDERDATE, 112)) = '" + this.dtpBillDate.Text + "' AND KOTNUMBER = '" + this.txtKOTNo.Text.Trim() + "'"));
        }

        private long GETCUSTORDERMASTERID()
        {
            return Convert.ToInt64(GlobalFunctions.GetQueryValue("SELECT CUSTORDERMASTERID FROM RES_CUSTORDERMASTER  WHERE  CUSTORDERNO =  '" + this.txtBillNo.Text.Trim() + "' AND MACHINENO = " + GlobalVariables.TerminalNo.ToString() + " AND CONVERT(DATETIME, CONVERT(VARCHAR, CUSTORDERDATE, 112)) = '" + this.dtpBillDate.Text + "'"));
        }

        public DataTable GetDistinctTable(string table, DataTable source, string field)
        {
            DataTable table2 = new DataTable(table);
            DataColumn column = new DataColumn
            {
                ColumnName = field,
                DataType = source.Columns[field].DataType,
                Unique = true
            };
            table2.Columns.Add(column);
            DataRow[] rowArray = source.Select("", field);
            object obj2 = null;
            foreach (DataRow row in rowArray)
            {
                if ((obj2 == null) || !this.AreEqual(obj2, row[field]))
                {
                    obj2 = row[field];
                    try
                    {
                        table2.Rows.Add(new object[] { obj2 });
                    }
                    catch
                    {
                    }
                }
            }
            return table2;
        }



        private bool KOTPageHeader(ref ClsRptDosPrint prnrpt)
        {
            try
            {
                int num = 0;
                return (num > 1);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "KOT Page Header");
                return false;
            }
        }

        public void KOTPrint(long intCustOrderKotId)
        {
            int num2;
            string str5;
            ClsDosPrint print;
            BillSettings settings = new BillSettings(1);
            string str = "";
            string paramSql = "";
            this.rptname = "KOT";
            string str3 = " CUSTORDERKOTID = " + Convert.ToString(intCustOrderKotId);
            this.pfunctions.SETREPORTDATA(ref this.prnrpt, this.rptname, this.ds, this.sda, GlobalVariables.KotPrinterId, GlobalVariables.SqlConn);
            this.pfunctions.SETCOLDATA(ref this.prnItem, this.rptname, this.ds, this.sda, GlobalVariables.KotPrinterId, GlobalVariables.SqlConn);
            if ((settings.BILLSTUBREQ.ToString().ToUpper() != "NRA") && GlobalVariables.blnStubReq)
            {
                this.rptname = "STUB";
                GlobalFill.FillDataSet("SELECT DISTINCT SECTIONID FROM RES_VW_CUSTORDERKOTREPORT WHERE " + str3, "ALLDEPT", this.ds, this.sda);
                for (int i = 0; i <= (this.ds.Tables["ALLDEPT"].Rows.Count - 1); i++)
                {
                    this.pfunctions.SETREPORTDATA(ref this.prnrpt, this.rptname, this.ds, this.sda, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
                    this.pfunctions.SETCOLDATA(ref this.prnItem, this.rptname, this.ds, this.sda, GlobalVariables.PrinterId, GlobalVariables.SqlConn);
                    paramSql = "SELECT * FROM RES_VW_CUSTORDERKOTREPORT";
                    if ((str3 != "") && (str3 != ""))
                    {
                        paramSql = paramSql + " WHERE SECTIONID = " + this.ds.Tables["ALLDEPT"].Rows[i][0].ToString() + " AND " + str3;
                    }
                    this.STUBPageHeader(ref this.prnrpt, Convert.ToInt16(this.ds.Tables["ALLDEPT"].Rows[i][0].ToString()));
                    num2 = 0;
                    while (num2 <= (this.prnItem.Length - 1))
                    {
                        str5 = "";
                        if (this.prnItem[num2].ISGroupHeader)
                        {
                            str5 = (this.prnItem[num2].ColumnPosition + 1) + ",";
                        }
                        str = str + str5;
                        num2++;
                    }
                    if (str.Trim() != "")
                    {
                        paramSql = paramSql + " ORDER BY " + str.Trim().Substring(1, str.Length - 1);
                    }
                    GlobalFill.FillDataSet(paramSql, "KOTTRANS", this.ds, this.sda);
                    string str6 = "NRA";
                    DataTable dtPrn = new DataTable();
                    if (settings.BILLSTUBREQ.ToString().ToUpper() == "RA")
                    {
                        str6 = "";
                    }
                    else if (settings.BILLSTUBREQ.ToString().ToUpper() == "RP")
                    {
                        str6 = "PARCELSTATUS = TRUE ";
                    }
                    else if (settings.BILLSTUBREQ.ToString().ToUpper() == "NRP")
                    {
                        str6 = "PARCELSTATUS = FALSE ";
                    }
                    if (str6 != "")
                    {
                        this.ds.Tables["KOTTRANS"].DefaultView.RowFilter = str6;
                        dtPrn = this.ds.Tables["KOTTRANS"].DefaultView.ToTable();
                    }
                    else
                    {
                        dtPrn = this.ds.Tables["KOTTRANS"];
                    }
                    if (dtPrn.Rows.Count > 0)
                    {
                        print = new ClsDosPrint(dtPrn, this.prnItem, this.prnrpt, GlobalVariables.SqlConn, GlobalVariables.PrinterId);
                        if (this.PrntTo == "")
                        {
                            print.PrintData(this.cmbTarget.Text, Convert.ToString(dtPrn.Rows[0]["STUBPRINTERNAME"].ToString()));
                        }
                        else
                        {
                            print.PrintData(this.PrntTo, Convert.ToString(dtPrn.Rows[0]["STUBPRINTERNAME"].ToString()));
                        }
                    }
                }
            }
            if (settings.KOTPRINTREQ)
            {
                paramSql = "SELECT * FROM RES_VW_CUSTORDERKOTREPORT WHERE " + str3;
                for (num2 = 0; num2 <= (this.prnItem.Length - 1); num2++)
                {
                    str5 = "";
                    if (this.prnItem[num2].ISGroupHeader)
                    {
                        str5 = (this.prnItem[num2].ColumnPosition + 1) + ",";
                    }
                    str = str + str5;
                }
                if (str.Trim() != "")
                {
                    paramSql = paramSql + " ORDER BY " + str.Trim().Substring(1, str.Length - 1);
                }
                GlobalFill.FillDataSet(paramSql, "KOTTRANS", this.ds, this.sda);
                this.KOTPageHeader(ref this.prnrpt);
                DataTable table2 = this.GetDistinctTable("dtKotPrinters", this.ds.Tables["KOTTRANS"], "KOTPRINTERNAME");
                foreach (DataRow row in table2.Rows)
                {
                    this.ds.Tables["KOTTRANS"].DefaultView.RowFilter = "KOTPRINTERNAME = '" + row["KOTPRINTERNAME"] + "'";
                    if (this.ds.Tables["KOTTRANS"].DefaultView.ToTable().Rows.Count > 0)
                    {
                        print = new ClsDosPrint(this.ds.Tables["KOTTRANS"].DefaultView.ToTable(), this.prnItem, this.prnrpt, GlobalVariables.SqlConn, GlobalVariables.KotPrinterId);
                        if (this.PrntTo == "")
                        {
                            print.PrintData(this.cmbTarget.Text, row["KOTPRINTERNAME"].ToString());
                        }
                        else
                        {
                            print.PrintData(this.PrntTo, row["KOTPRINTERNAME"].ToString());
                        }
                    }
                }
            }
        }

        private bool STUBPageHeader(ref ClsRptDosPrint ParamPrnRpt, int ParamSecID)
        {
            try
            {
                ParamPrnRpt.set_PageHeader(0, Convert.ToString(GlobalFunctions.GetQueryValue("SELECT SECTIONNAME FROM RES_SECTION WHERE SECTIONID = " + ParamSecID)));
                return true;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Bill Page Header");
                return false;
            }
        }
    }
}
