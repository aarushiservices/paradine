﻿using Microsoft.VisualBasic;
using ParaDine.Forms.Billing;
using ParaDine.Forms.GlobalForms;
using ParaDine.Forms.Printing;
using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Main
{
    public partial class FrmBilling : Form
    {
        private Card bCard;
        private CardIssue bCardIssue;
        private CardTrans bCardTrans;
        private bool blnChgTable;
        private bool blnDirectBill;
        public bool blnKeyBoardMenu;
        private Button btnBillParking;
        private Button btnbillrecall;
        private Button btnBrowseCustomer;
        private Button btnBrowseDeliveryOrder;
        private Button btnCalculator;
        private Button btnCash;
        private Button btnChageTable;
        private Button btnClear;
        private Button btnCopyAddress;
        private Button btnCustomer;
        private Button btnFinalAmts;
        private Button btnGenerateKOT;
        private Button btnImagePnl;
        private Button btnKOT;
        private Button btnNewKOT;
        private Button btnNewSource;
        private Button btnNewSteward;
        private Button btnOptions;
        private Button btnOrdDiscount;
        private Button btnOrderDetails;
        private Button btnordermerge;
        private Button btnOrderSearch;
        private Button btnReprint;
        private Button btnSplitPayment;
        private Button btnTax;
        private Button btnTempBill;
        public Color CatdeSelectedColor;
        public Color CatSelectedColor;
        private CheckBox chkBillPrntNotReq;
        private CheckBox chkItemParcelStatus;
        private CheckBox chkKotPrntNotReq;
        private ComboBox cmbCustOrderNo;
        private ComboBox cmbItemUOM;
        private ComboBox cmbSource;
        private ComboBox cmbSteward;
        private ContextMenuStrip cntxMenuGrdItems;
        private ContextMenuStrip cntxMenuLvwItems;
        // private IContainer components;
        private long CustOrderMasterID;
        private ToolStripMenuItem decreaseQtyToolStripMenuItem;
        private ToolStripMenuItem deleteItemToolStripMenuItem;
        private DataGridView dgrdVwCategory;
        private DataGridView dgrdVwItems;
        private DataSet DS;
        private DataTable dtCategory;
        private DataTable dtDepartment;
        private DataTable dtItems;
        private DataTable dtOrder;
        private DataTable dtParking;
        private DateTimePicker dtpEndTime;
        private DateTimePicker dtpStartTime;
        private ToolStripMenuItem editToolStripMenuItem;
        private CustOrderMaster EntId;
        private CustOrderMasterCollection EntIdCollection;
        private NumControl gvAmtTend;
        private NumControl gvChange;
        private NumControl gvItemAmount;
        private NumControl gvItemDiscPerc;
        private NumControl gvItemFreeQty;
        private NumControl gvItemSalePrice;
        private NumControl gvItemSaleQty;
        private NumControl gvItemServTax;
        private NumControl gvItemTaxPerc;
        private NumControl gvOrderDiscAmt;
        private NumControl gvOrderDiscPerc;
        private NumControl gvOrderDiscPercAmt;
        private NumControl gvOrderGrossAmt;
        private NumControl gvOrderItemDiscAmt;
        private NumControl gvOrderItemServTax;
        private NumControl gvOrderItemTaxAmt;
        private NumControl gvOrderNetAmt;
        private NumControl gvOrderTaxAmt;
        private NumControl gvOrderTaxPerc;
        private NumControl gvOrderTaxPercAmt;
        private NumControl gvOrderTotalItems;
        private NumControl gvOrderTotalKOTs;
        private NumControl gvRefund;
        private ImageList imageList1;
        private ToolStripMenuItem increaseQtyToolStripMenuItem;
        private Label label1;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label13;
        private Label label14;
        private Label label15;
        private Label label16;
        private Label label17;
        private Label label18;
        private Label label19;
        private Label label2;
        private Label label20;
        private Label label21;
        private Label label22;
        private Label label23;
        private Label label24;
        private Label label25;
        private Label label26;
        private Label label27;
        private Label label28;
        private Label label29;
        private Label label3;
        private Label label30;
        private Label label31;
        private Label label32;
        private Label label34;
        private Label label35;
        private Label label36;
        private Label label37;
        private Label label38;
        private Label label39;
        private Label label4;
        private Label label40;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label lblBalance;
        private Label lblBalHeader;
        private Label lblBillTotal;
        private Label lblBillTotHeader;
        private Label lblCardBalance;
        private Label lblCardBalHeader;
        private Label lblCardCustomer;
        private Label LblPrice;
        private Label LblQty;
        internal ListView lstVwOrder;
        private BillSettings ObjBillSettings;
        private Panel panel1;
        private ToolStripMenuItem parcelToolStripMenuItem;
        private PictureBox pictureBox1;
        private Panel pnlControls;
        private Panel pnlCustomer;
        private Panel pnlDispCategory;
        private Panel pnlFinalAmts;
        private Panel pnlItem;
        private Panel pnlKOT;
        private Panel pnlMaster;
        private int SourceID;
        private SplitContainer splitContainer1;
        private Splitter splitter1;
        private Timer tmrChange;
        private TreeView trVwOrder;
        private ToolTip TTip;
        private TextBox txtAddress;
        private TextBox txtCover;
        private TextBox txtCustCode;
        private TextBox txtCustName;
        private TextBox txtCustPhone;
        private TextBox txtDeliveryOrder;
        private TextBox txtDelvAddress;
        private TextBox txtItemCode;
        private TextBox txtItemName;
        private TextBox txtItemNote;
        private TextBox txtKOTNo;
        private TextBox txtMno;
        private TextBox txtOrderNo;
        private TextBox txtRefNo;

        public FrmBilling(int TableID)
        {
            this.ObjBillSettings = new BillSettings(1);
            this.EntIdCollection = new CustOrderMasterCollection();
            this.EntId = new CustOrderMaster();
            this.SourceID = 0;
            this.CustOrderMasterID = 0L;
            this.CatSelectedColor = Color.Brown;
            this.CatdeSelectedColor = Color.Gray;
            this.DS = new DataSet();
            this.blnKeyBoardMenu = true;
            this.dtParking = new DataTable();
            this.LblQty = new Label();
            this.LblPrice = new Label();
            this.dtDepartment = new DataTable();
            this.dtCategory = new DataTable();
            this.dtItems = new DataTable();
            this.dtOrder = new DataTable();
            this.bCard = new Card();
            this.bCardTrans = new CardTrans();
            this.bCardIssue = new CardIssue();
            this.components = null;
            this.InitializeComponent();
            ParaDine.GlobalFunctions.AddCompHandler(this.panel1);
            this.SourceID = TableID;
        }

        public FrmBilling(int TableID, long mCustOrderMasterID)
        {
            this.ObjBillSettings = new BillSettings(1);
            this.EntIdCollection = new CustOrderMasterCollection();
            this.EntId = new CustOrderMaster();
            this.SourceID = 0;
            this.CustOrderMasterID = 0L;
            this.CatSelectedColor = Color.Brown;
            this.CatdeSelectedColor = Color.Gray;
            this.DS = new DataSet();
            this.blnKeyBoardMenu = true;
            this.dtParking = new DataTable();
            this.LblQty = new Label();
            this.LblPrice = new Label();
            this.dtDepartment = new DataTable();
            this.dtCategory = new DataTable();
            this.dtItems = new DataTable();
            this.dtOrder = new DataTable();
            this.bCard = new Card();
            this.bCardTrans = new CardTrans();
            this.bCardIssue = new CardIssue();
            this.components = null;
            this.InitializeComponent();
            ParaDine.GlobalFunctions.AddCompHandler(this.panel1);
            this.SourceID = TableID;
            this.CustOrderMasterID = mCustOrderMasterID;
        }

        public FrmBilling(string Mode, long ID, bool paramDirectBill)
        {
            this.ObjBillSettings = new BillSettings(1);
            this.EntIdCollection = new CustOrderMasterCollection();
            this.EntId = new CustOrderMaster();
            this.SourceID = 0;
            this.CustOrderMasterID = 0L;
            this.CatSelectedColor = Color.Brown;
            this.CatdeSelectedColor = Color.Gray;
            this.DS = new DataSet();
            this.blnKeyBoardMenu = true;
            this.dtParking = new DataTable();
            this.LblQty = new Label();
            this.LblPrice = new Label();
            this.dtDepartment = new DataTable();
            this.dtCategory = new DataTable();
            this.dtItems = new DataTable();
            this.dtOrder = new DataTable();
            this.bCard = new Card();
            this.bCardTrans = new CardTrans();
            this.bCardIssue = new CardIssue();
            this.components = null;
            this.InitializeComponent();
            this.blnDirectBill = paramDirectBill;
            if (ID <= 0L)
            {
                this.EntId.Added = true;
                this.EntId.EntMoneyMaster.Added = true;
            }
            this.EntId.LoadAttributes(ID);
            ParaDine.GlobalFunctions.AddCompHandler(this.panel1);
        }

        private void btnBillParking_Click(object sender, EventArgs e)
        {
            try
            {
                Label lParking = new Label();
                new frmBillParking(lParking).ShowDialog();
                if ((lParking.Text != "") && ((this.dtParking.Select("ParkingNo = '" + lParking.Text + "'").GetUpperBound(0) + 1) > 0))
                {
                    MessageBox.Show("Entered Parking No Exist In Parking List", "ParaDine");
                }
                else
                {
                    foreach (CustOrderItem item in this.EntId.CustOrderKOTCollection[Convert.ToInt16(this.pnlKOT.Tag)].CustOrderItemCollection)
                    {
                        if (!item.Deleted)
                        {
                            DataRow row = this.dtParking.NewRow();
                            row["ItemId"] = item.ItemID;
                            row["SALEQTY"] = item.SaleQty;
                            row["FREEQTY"] = item.FreeQty;
                            row["ITEMDISCPERC"] = item.ItemDiscPerc;
                            row["ITEMTAXPERC"] = item.ItemTaxPerc;
                            row["SALEPRICE"] = item.SalePrice;
                            row["UOMID"] = item.UOMId;
                            row["PARKINGNO"] = lParking.Text;
                            this.dtParking.Rows.Add(row);
                        }
                    }
                    this.EntId.CustOrderKOTCollection[Convert.ToInt16(this.pnlKOT.Tag)].CustOrderItemCollection.Clear();
                    this.PopulateItems();
                    this.PopulateTotals();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void btnbillrecall_Click(object sender, EventArgs e)
        {
            DataTable dtp = ParaDine.GlobalFunctions.GetDistinctTable("DTPARK", this.dtParking, "PARKINGNO");
            Label lParking = new Label();
            new frmbillReCall(lParking, dtp).ShowDialog();
            if (lParking.Text != "")
            {
                this.EntId.CustOrderKOTCollection[Convert.ToInt16(this.pnlKOT.Tag)].CustOrderItemCollection.Clear();
                foreach (DataRow row in this.dtParking.Select("PARKINGNO = '" + lParking.Text + "'"))
                {
                    CustOrderItem varItem = new CustOrderItem
                    {
                        ItemID = Convert.ToInt64(row["ITEMID"]),
                        SaleQty = Convert.ToDouble(row["SALEQTY"]),
                        FreeQty = Convert.ToDouble(row["FREEQTY"]),
                        ItemDiscPerc = Convert.ToDouble(row["ITEMDISCPERC"]),
                        ItemTaxPerc = Convert.ToDouble(row["ITEMTAXPERC"]),
                        SalePrice = Convert.ToDouble(row["SALEPRICE"]),
                        UOMId = Convert.ToInt16(row["UOMID"]),
                        Added = true
                    };
                    this.EntId.CustOrderKOTCollection[Convert.ToInt16(this.pnlKOT.Tag)].CustOrderItemCollection.Add(varItem);
                    this.dtParking.Rows.Remove(row);
                }
                this.PopulateItems();
                this.PopulateTotals();
            }
        }

        private void btnBrowseCustomer_Click(object sender, EventArgs e)
        {
            try
            {
                string paramSql = "SELECT * FROM RES_CUSTOMER WHERE INACTIVE = 0";
                new frmBrowseData(ParaDine.GlobalFill.FillDataTable(paramSql), this.txtCustCode, "Customer Details", 2).ShowDialog();
                this.LoadCustomer();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void btnBrowseDeliveryOrder_Click(object sender, EventArgs e)
        {
            try
            {
                string paramSql = "SELECT * FROM RES_VW_PENDINGDELIVERYORDER";
                new frmBrowseData(ParaDine.GlobalFill.FillDataTable(paramSql), this.txtDeliveryOrder, "Pending Delivery Order", 2).ShowDialog();
                DeliveryOrder entDelivery = new DeliveryOrder(Convert.ToInt64(this.txtDeliveryOrder.Tag));
                this.txtCustCode.Tag = entDelivery.CustID;
                this.txtDelvAddress.Text = entDelivery.DeliveryAdd;
                this.gvRefund.Value = Convert.ToDecimal(entDelivery.AdvanceAmount);
                this.LoadCustomer();
                this.LoadDeliveryItems(entDelivery);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void btnCalculator_Click(object sender, EventArgs e)
        {
            Interaction.Shell(@"C:\Windows\system32\calc.exe", AppWinStyle.NormalFocus, false, -1);
        }

        private void btnCash_Click(object sender, EventArgs e)
        {
            try
            {
                this.EntId.EntMoneyMaster.MoneyTransCollection.Clear();
                this.EntId.EntMoneyMaster.TransactionDate = ParaDine.GlobalVariables.BusinessDateTime;
                this.EntId.EntMoneyMaster.TransactionSource = "BILL";
                this.EntId.EntMoneyMaster.TransactionSourceID = this.EntId.CustOrderMasterID;
                MoneyTrans varEntityMoneyTrans = new MoneyTrans
                {
                    Added = true,
                    MoneyMasterID = this.EntId.EntMoneyMaster.MoneyMasterID,
                    MoneyTransID = 0L,
                    PaidAmount = this.EntId.GetOrderNetAmount,
                    PaySourceDate = this.EntId.CustOrderDate,
                    PaymentModeID = 1
                };
                this.EntId.EntMoneyMaster.MoneyTransCollection.Add(varEntityMoneyTrans);
                if ((this.ObjBillSettings.CardSystemReq && this.blnDirectBill) && (Convert.ToDouble(this.gvOrderNetAmt.Value) > Convert.ToDouble(this.lblCardBalance.Text)))
                {
                    MessageBox.Show("Insufficiant Balance", "Billing", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                else
                {
                    this.SaveOrder();
                    if (!this.chkBillPrntNotReq.Checked)
                    {
                        FrmPrinting printing = new FrmPrinting(this.EntId.CustOrderMasterID, "BILL");
                        printing.BillPrint(this.EntId.CustOrderMasterID);
                        printing.Dispose();
                    }
                    this.CustOrderMasterID = 0L;
                    this.LoadNewOrder();
                    this.cmbSource_SelectionChangeCommitted(sender, e);
                    if (this.blnDirectBill)
                    {
                        this.cmbSteward.Focus();
                    }
                    else
                    {
                        this.cmbSource.Focus();
                    }
                    if (this.ObjBillSettings.CardSystemReq && this.blnDirectBill)
                    {
                        frmShowCard card = new frmShowCard();
                        card.ShowDialog();
                        this.bCard = card.EntCard;
                        this.bCardIssue = card.EntIssue;
                        this.FrmBilling_Shown(sender, e);
                        this.lblCardCustomer.Text = this.bCardIssue.CustomerName;
                        this.lblCardBalance.Text = this.bCard.CardBalance().ToString("####0.00");
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In btnCash_Click");
            }
        }

        private void btnChageTable_Click(object sender, EventArgs e)
        {
            this.blnChgTable = true;
            this.cmbSource.Focus();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (this.EntId.CustOrderKOTCollection.GetItemCollectionTable().Rows.Count > 0)
            {
                if (MessageBox.Show("Order Containing Some Information.\n Do U Really Want To Quit????", "Close", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    base.Close();
                }
            }
            else
            {
                base.Close();
            }
        }

        private void btnCopyAddress_Click(object sender, EventArgs e)
        {
            this.txtDelvAddress.Text = this.txtAddress.Text;
        }

        private void btnCustomer_Click(object sender, EventArgs e)
        {
            this.pnlCustomer.BringToFront();
            this.txtCustCode.Focus();
        }

        private void btnFinalAmts_ChangeUICues(object sender, UICuesEventArgs e)
        {
        }

        private void btnFinalAmts_Click(object sender, EventArgs e)
        {
            this.pnlFinalAmts.BringToFront();
        }

        private void btnGenerateKOT_Click(object sender, EventArgs e)
        {
            if ((this.txtCover.BackColor == Color.Ivory) && (Convert.ToInt16(this.txtCover.Text) <= 0))
            {
                MessageBox.Show("Please Enter Covered before saving a KOT", "ParaDine");
            }
            else if ((this.txtRefNo.BackColor == Color.Ivory) && (this.txtRefNo.Text == ""))
            {
                MessageBox.Show("Please Enter Reference Kot No before saving a KOT", "ParaDine");
            }
            else if (this.ObjBillSettings.RefKotNoIdentity && (Convert.ToInt16(ParaDine.GlobalFunctions.GetQueryValue("SELECT COUNT(*) FROM RES_CUSTORDERKOT WHERE REFNO = '" + this.txtRefNo.Text + "'")) > 0))
            {
                MessageBox.Show("Entered Reference Kot No Exist in Data", "ParaDine");
            }
            else if (Convert.ToInt32(this.cmbSource.SelectedValue) > 0)
            {
                if (this.SaveOrder())
                {
                    FrmPrinting printing = new FrmPrinting(this.EntId.CustOrderKOTCollection[Convert.ToInt16(this.pnlKOT.Tag)].CustOrderKOTID, "KOT");
                    printing.KOTPrint(this.EntId.CustOrderKOTCollection[Convert.ToInt16(this.pnlKOT.Tag)].CustOrderKOTID);
                    printing.Dispose();
                    this.LoadNewOrder();
                    this.cmbSource.SelectedIndex = -1;
                    this.cmbCustOrderNo_SelectionChangeCommitted(sender, e);
                    this.cmbSource.Focus();
                }
            }
            else
            {
                MessageBox.Show("Please select the table before saving a KOT", "Select Table");
                this.btnChageTable_Click(sender, e);
            }
        }

        private void btnImagePnl_Click(object sender, EventArgs e)
        {
            this.splitContainer1.Panel2Collapsed = !this.splitContainer1.Panel2Collapsed;
            this.splitContainer1.Update();
            if (this.splitContainer1.Panel2Collapsed)
            {
                this.panel1.Left += 140;
            }
            else
            {
                this.panel1.Left -= 140;
            }
            SendKeys.Send("{TAB}");
        }

        private void btnNewKOT_Click(object sender, EventArgs e)
        {
            try
            {
                CustOrderKOT rkot;
                for (int i = 0; i < this.EntId.CustOrderKOTCollection.Count; i++)
                {
                    if (this.EntId.CustOrderKOTCollection[i].CustOrderKOTID <= 0L)
                    {
                        this.pnlKOT.Tag = i;
                        this.LoadKOTFields(this.EntId.CustOrderKOTCollection[i]);
                        return;
                    }
                }

                rkot = new CustOrderKOT();

                rkot.Added = true;
                rkot.CustOrderMasterID = this.EntId.CustOrderMasterID;
                rkot.CustOrderNo = this.EntId.CustOrderNo;
                rkot.KOTNumber = rkot.MaxCode();

                //rkot = new CustOrderKOT {
                //    Added = true,
                //    CustOrderMasterID = this.EntId.CustOrderMasterID,
                //    CustOrderNo = this.EntId.CustOrderNo,
                //    KOTNumber = rkot.MaxCode()
                //};
                this.pnlKOT.Tag = this.EntId.CustOrderKOTCollection.Count;
                rkot.UserID = ParaDine.GlobalVariables.UserID;
                this.EntId.CustOrderKOTCollection.Add(rkot);
                this.LoadKOTFields(rkot);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In Generating New KOT");
            }
        }

        private void btnOptions_Click(object sender, EventArgs e)
        {
            this.pnlControls.BringToFront();
        }

        private void btnOrdDiscount_Click(object sender, EventArgs e)
        {
            this.pnlFinalAmts.BringToFront();
            this.gvOrderDiscPerc.Focus();
        }

        private void btnOrderDetails_Click(object sender, EventArgs e)
        {
            this.pnlFinalAmts.BringToFront();
            this.txtItemCode.Focus();
        }

        private void btnordermerge_Click(object sender, EventArgs e)
        {
            this.EntId.CustOrderKOTCollection[Convert.ToInt16(this.pnlKOT.Tag)].CustOrderItemCollection.Clear();
            Label lOrderMerge = new Label();
            new frmorderMerge(lOrderMerge).ShowDialog();
            if (lOrderMerge.Text != "")
            {
                try
                {
                    this.EntId.CustOrderKOTCollection[Convert.ToInt16(this.pnlKOT.Tag)].CustOrderItemCollection.Clear();
                    this.btnClear_Click(sender, e);
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }
            }
            else
            {
                this.btnClear_Click(sender, e);
                base.Close();
            }
        }

        private void btnOrderSearch_Click(object sender, EventArgs e)
        {
            string paramSql = "SELECT * FROM RES_VW_CUSTORDERMASTER WHERE SETTLED = 0";
            new frmBrowseData(ParaDine.GlobalFill.FillDataTable(paramSql), this.txtOrderNo, "Pending Orders", 2).ShowDialog();
            this.EntId = new CustOrderMaster(Convert.ToInt64(this.txtOrderNo.Tag));
            this.LoadFields(this.EntId);
        }

        private void btnReprint_Click(object sender, EventArgs e)
        {
            new FrmPrinting(ParaDine.GlobalVariables.TerminalNo, Convert.ToString((int)(Convert.ToInt16(this.txtOrderNo.Text) - 1)).PadLeft(4, Convert.ToChar("0")), "0001", ParaDine.GlobalVariables.BusinessDate, "Bill").ShowDialog();
        }

        private void btnSplitPayment_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.EntId.GetOrderNetAmount > 0.0)
                {
                    bool blnCredit = true;
                    if (Convert.ToInt16(this.txtCustCode.Tag) == 0)
                    {
                        blnCredit = false;
                    }
                    else
                    {
                        blnCredit = true;
                    }
                    frmMoneyTrans trans = new frmMoneyTrans("&Add", 0L, this.EntId.GetOrderNetAmount, blnCredit);
                    if (this.EntId.EntMoneyMaster.MoneyTransCollection.Count > 0)
                    {
                        trans = new frmMoneyTrans("&Edit", this.EntId.EntMoneyMaster.MoneyMasterID, this.EntId.GetOrderNetAmount, blnCredit);
                    }
                    trans.ShowDialog();
                    if (trans.DialogResult == DialogResult.Yes)
                    {
                        this.EntId.EntMoneyMaster = trans.GetMoneyMaster;
                        if (Math.Round(this.EntId.EntMoneyMaster.GetTransAmount) != Math.Round(this.EntId.GetOrderNetAmount))
                        {
                            throw new Exception("PaidAmount is Not Sufficient");
                        }
                        this.EntId.EntMoneyMaster.TransactionSource = "BILL";
                        this.SaveOrder();
                        FrmPrinting printing = new FrmPrinting(this.EntId.CustOrderMasterID, "BILL");
                        printing.BillPrint(this.EntId.CustOrderMasterID);
                        printing.Dispose();
                        this.LoadNewOrder();
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In btnSplitPayment");
            }
        }

        private void btnTax_Click(object sender, EventArgs e)
        {
            this.pnlFinalAmts.BringToFront();
            this.gvOrderTaxPerc.Focus();
        }

        private void btnTempBill_Click(object sender, EventArgs e)
        {
            this.SaveOrder();
            FrmPrinting printing = new FrmPrinting(this.EntId.CustOrderMasterID, "BILL");
            printing.BillPrint(this.EntId.CustOrderMasterID);
            printing.Dispose();
            this.LoadNewOrder();
        }

        private void CalculateItemAmount()
        {
            this.gvItemAmount.Value = Convert.ToDecimal((double)((((Convert.ToDouble(this.gvItemSalePrice.Value) * Convert.ToDouble(this.gvItemSaleQty.Value)) - (((Convert.ToDouble(this.gvItemSalePrice.Value) * Convert.ToDouble(this.gvItemSaleQty.Value)) * Convert.ToDouble(this.gvItemDiscPerc.Value)) / 100.0)) + ((((Convert.ToDouble(this.gvItemSalePrice.Value) * Convert.ToDouble(this.gvItemSaleQty.Value)) - (((Convert.ToDouble(this.gvItemSalePrice.Value) * Convert.ToDouble(this.gvItemSaleQty.Value)) * Convert.ToDouble(this.gvItemDiscPerc.Value)) / 100.0)) * Convert.ToDouble(this.gvItemTaxPerc.Value)) / 100.0)) + ((((Convert.ToDouble(this.gvItemSalePrice.Value) * Convert.ToDouble(this.gvItemSaleQty.Value)) - (((Convert.ToDouble(this.gvItemSalePrice.Value) * Convert.ToDouble(this.gvItemSaleQty.Value)) * Convert.ToDouble(this.gvItemDiscPerc.Value)) / 100.0)) * Convert.ToDouble(this.gvItemServTax.Value)) / 100.0)));
        }

        private void cmbCustOrderNo_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Table table;
            if (!this.blnChgTable)
            {
                if (this.EntId.SourceType == Convert.ToChar("T"))
                {
                    table = new Table(Convert.ToInt32(this.cmbSource.SelectedValue));
                    if (table.PendingOrders(Convert.ToInt64(this.cmbCustOrderNo.SelectedValue)).Rows.Count > 0)
                    {
                        foreach (DataRow row in table.PendingOrders(Convert.ToInt64(this.cmbCustOrderNo.SelectedValue)).Rows)
                        {
                            this.EntId = new CustOrderMaster(Convert.ToInt64(row[0]));
                            this.LoadOrderTree(this.EntId);
                        }
                        this.LoadFields(this.EntId);
                    }
                    else
                    {
                        this.LoadNewOrder();
                        if (this.SourceID > 0)
                        {
                            this.cmbSource.SelectedValue = this.SourceID;
                            this.SourceID = 0;
                        }
                    }
                }
            }
            else
            {
                table = new Table(Convert.ToInt16(this.cmbSource.SelectedValue));
                this.EntId.SourceTypeId = Convert.ToInt16(this.cmbSource.SelectedValue);
                this.SaveOrder();
                this.blnChgTable = false;
            }
        }

        private void cmbItemUOM_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if ((this.cmbItemUOM.SelectedIndex != -1) && (this.cmbItemUOM.ValueMember != ""))
                {
                    ItemUnitPrice price = new ItemUnitPrice(Convert.ToInt64(this.txtItemCode.Tag), Convert.ToInt32(this.cmbItemUOM.SelectedValue), ParaDine.GlobalVariables.StockPointId);
                    if (price.ItemID > 0L)
                    {
                        this.cmbItemUOM.Tag = Convert.ToString(price.UOMID);
                        this.gvItemSalePrice.Value = Convert.ToDecimal(price.SalePrice);
                    }
                    else
                    {
                        this.gvItemSalePrice.Value = Convert.ToDecimal(new Item(Convert.ToInt64(this.txtItemCode.Tag)).SalePrice);
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "ERROR IN CMBITEMUOM_SELECTEDINDEXCHANGED");
            }
        }

        private void cmbSource_DropDownClosed(object sender, EventArgs e)
        {
            this.cmbSteward.Focus();
        }

        private void cmbSource_Enter(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception)
            {
                MessageBox.Show("error in table no");
            }
        }

        private void cmbSource_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Table table = new Table(Convert.ToInt32(this.cmbSource.SelectedValue));
            this.cmbCustOrderNo.DataSource = null;
            DataTable existingOrdersList = table.ExistingOrdersList;
            DataRow row = existingOrdersList.NewRow();
            row[0] = 0;
            row[1] = "New Order";
            row[2] = ParaDine.GlobalVariables.TerminalNo.ToString();
            existingOrdersList.Rows.Add(row);
            existingOrdersList.DefaultView.RowFilter = "MACHINENO = " + ParaDine.GlobalVariables.TerminalNo;
            ParaDine.GlobalFill.FillCombo(existingOrdersList.DefaultView.ToTable(), this.cmbCustOrderNo);
            if (this.CustOrderMasterID > 0L)
            {
                this.cmbCustOrderNo.SelectedValue = this.CustOrderMasterID;
            }
            else if (this.cmbCustOrderNo.Items.Count > 0)
            {
                this.cmbCustOrderNo.SelectedIndex = 0;
            }
        }

        private void cmbSource_Validated(object sender, EventArgs e)
        {
        }

        private void cmbSource_Validating(object sender, CancelEventArgs e)
        {
            if (Convert.ToInt16(this.cmbSource.SelectedValue) <= 0)
            {
                long num = Convert.ToInt32(ParaDine.GlobalFunctions.GetQueryValue("SELECT TABLEID FROM RES_TABLE WHERE LTRIM(RTRIM(UPPER(TABLENAME))) = '" + this.cmbSource.Text.ToUpper().Trim() + "'"));
                this.cmbSource.SelectedValue = num;
                this.cmbSource_SelectionChangeCommitted(sender, e);
            }
            this.cmbCustOrderNo_SelectionChangeCommitted(sender, e);
            this.cmbSteward.Focus();
        }

        private void cntxMenuItem_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            this.LoadCustOrderItemFromGrid(Convert.ToInt64(this.cntxMenuGrdItems.Tag), Convert.ToInt32(e.ClickedItem.Tag));
        }

        private void decreaseQtyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.lstVwOrder_KeyDown(sender, new KeyEventArgs(Keys.Subtract));
        }

        private void deleteItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.lstVwOrder_KeyDown(sender, new KeyEventArgs(Keys.Delete));
        }

        private void dgrdVwCategory_Enter(object sender, EventArgs e)
        {
            this.dgrdVwCategory.RowsDefaultCellStyle.SelectionBackColor = this.CatSelectedColor;
        }

        private void dgrdVwCategory_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right)
            {
                this.dgrdVwItems.Focus();
            }
            else if (e.KeyCode == Keys.Left)
            {
                this.txtItemCode.Focus();
            }
            else
            {
                DataTable dataSource = (DataTable)this.dgrdVwCategory.DataSource;
                DataRow[] rowArray = dataSource.Select("CATEGORYNAME LIKE '" + e.KeyData.ToString() + "%'");
                int index = 0;
                while (index < rowArray.Length)
                {
                    DataRow row = rowArray[index];
                    this.dgrdVwCategory.Rows[dataSource.Rows.IndexOf(row)].Selected = true;
                    this.dgrdVwCategory.CurrentCell = this.dgrdVwCategory.SelectedRows[0].Cells[0];
                    break;
                }
            }
        }

        private void dgrdVwCategory_Leave(object sender, EventArgs e)
        {
            this.dgrdVwCategory.RowsDefaultCellStyle.SelectionBackColor = this.CatdeSelectedColor;
        }

        private void dgrdVwCategory_SelectionChanged(object sender, EventArgs e)
        {
            if (this.dgrdVwCategory.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow row in this.dgrdVwCategory.SelectedRows)
                {
                    if ((Convert.ToString(row.Cells["CATEGORYID"].Value).Trim() != "") && this.DS.Tables.Contains("CATGITEMS"))
                    {
                        this.DS.Tables["CATGITEMS"].DefaultView.RowFilter = "CATEGORYID = " + Convert.ToString(row.Cells["CATEGORYID"].Value);
                        DataTable table = this.DS.Tables["CATGITEMS"];
                        this.dgrdVwItems.DataSource = table;
                        this.dgrdVwItems.Refresh();
                        if (this.dgrdVwItems.Columns.Count > 0)
                        {
                            this.dgrdVwItems.Columns[0].Width = 0xaf;
                            this.dgrdVwItems.Columns[1].Width = 0x41;
                            this.dgrdVwItems.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                            this.dgrdVwItems.Columns[1].DefaultCellStyle.Format = "F2";
                            this.dgrdVwItems.Columns[2].Visible = false;
                        }
                    }
                }
            }
        }

        private void dgrdVwItems_Enter(object sender, EventArgs e)
        {
            this.dgrdVwItems.RowsDefaultCellStyle.SelectionBackColor = this.CatSelectedColor;
        }

        private void dgrdVwItems_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                this.dgrdVwCategory.Focus();
            }
            else if (e.KeyCode == Keys.Enter)
            {
                if ((this.dgrdVwItems.SelectedRows.Count > 0) && !(this.dgrdVwItems.SelectedRows[0].Cells["ITEMID"].Value is DBNull))
                {
                    if (this.ObjBillSettings.MouseModeReq)
                    {
                        if (Convert.ToInt64(this.txtKOTNo.Tag) <= 0L)
                        {
                            this.LoadCustOrderItemFromGrid(Convert.ToInt64(this.dgrdVwItems.SelectedRows[0].Cells["ITEMID"].Value), Convert.ToInt32(this.dgrdVwItems.SelectedRows[0].Cells["UOMID"].Value));
                        }
                        else if (MessageBox.Show("Can't Add or Edit Items for this KOT, Do U Want to Add New KOT ???", "New KOT", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            this.btnNewKOT_Click(sender, new EventArgs());
                        }
                    }
                    else
                    {
                        this.cntxMenuGrdItems.Tag = Convert.ToString(this.dgrdVwItems.SelectedRows[0].Cells["ITEMID"].Value);
                        Item item = new Item(Convert.ToInt64(this.cntxMenuGrdItems.Tag));
                        this.txtItemCode.Text = item.ItemCode;
                        this.txtItemCode_KeyDown(sender, new KeyEventArgs(Keys.Enter));
                        this.cmbItemUOM.SelectedValue = Convert.ToInt32(this.dgrdVwItems.SelectedRows[0].Cells["UOMID"].Value);
                        this.gvItemSaleQty.Focus();
                    }
                }
            }
            else
            {
                DataTable dataSource = (DataTable)this.dgrdVwItems.DataSource;
                DataRow[] rowArray = dataSource.Select("ITEMNAME LIKE '" + e.KeyData.ToString() + "%'");
                int index = 0;
                while (index < rowArray.Length)
                {
                    DataRow row = rowArray[index];
                    this.dgrdVwItems.Rows[dataSource.Rows.IndexOf(row)].Selected = true;
                    this.dgrdVwItems.CurrentCell = this.dgrdVwItems.SelectedRows[0].Cells[0];
                    break;
                }
            }
        }

        private void dgrdVwItems_Leave(object sender, EventArgs e)
        {
            this.dgrdVwItems.RowsDefaultCellStyle.SelectionBackColor = this.CatdeSelectedColor;
        }

        private void dgrdVwItems_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.dgrdVwItems_KeyDown(sender, new KeyEventArgs(Keys.Enter));
        }



        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.lstVwOrder_KeyDown(sender, new KeyEventArgs(Keys.Enter));
        }

        private void FrmBilling_Activated(object sender, EventArgs e)
        {
        }

        private void FrmBilling_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                ((frmMDIRest)base.MdiParent).btn_Tables_Click(sender, new EventArgs());
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Billing Closing", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void FrmBilling_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Home)
                {
                    this.btnFinalAmts_Click(sender, e);
                }
                else if (e.KeyCode == Keys.F12)
                {
                    this.btnChageTable_Click(sender, e);
                }
                else if (e.KeyCode == Keys.F3)
                {
                    this.btnTempBill_Click(sender, e);
                }
                else if (e.KeyCode == Keys.F5)
                {
                    if (this.ObjBillSettings.AMTTENDREQ)
                    {
                        this.gvAmtTend.Focus();
                    }
                    else
                    {
                        this.btnCash_Click(sender, e);
                    }
                }
                else if (e.KeyCode == Keys.F6)
                {
                    this.btnCustomer_Click(sender, e);
                }
                else if (e.KeyCode == Keys.F7)
                {
                    this.btnOrdDiscount_Click(sender, e);
                }
                else if (e.KeyCode == Keys.F8)
                {
                    this.btnGenerateKOT_Click(sender, e);
                }
                else if (e.KeyCode == Keys.F9)
                {
                    this.btnSplitPayment_Click(sender, e);
                }
                else if (e.KeyCode == Keys.F11)
                {
                    this.btnTax_Click(sender, e);
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    this.btnClear_Click(sender, e);
                }
                else if (e.KeyCode == Keys.F10)
                {
                    this.btnCalculator_Click(sender, e);
                }
                else if (e.KeyCode == Keys.F4)
                {
                    this.btnReprint_Click(sender, e);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "frmBilling_KeyDown");
            }
        }

        private void FrmBilling_Load(object sender, EventArgs e)
        {
            try
            {
                this.EntId = new CustOrderMaster(this.CustOrderMasterID);
                this.LblQty.Text = "0.00";
                this.LblPrice.Text = "0.00";
                this.dtParking.Columns.Add("ITEMID");
                this.dtParking.Columns.Add("SALEQTY");
                this.dtParking.Columns.Add("FREEQTY");
                this.dtParking.Columns.Add("ITEMDISCPERC");
                this.dtParking.Columns.Add("ITEMTAXPERC");
                this.dtParking.Columns.Add("SALEPRICE");
                this.dtParking.Columns.Add("UOMID");
                this.dtParking.Columns.Add("PARKINGNO");
                new GlobalTheme().applyTheme(this);
                this.RefreshData();
                if (this.blnDirectBill)
                {
                    if (this.ObjBillSettings.HIDING)
                    {
                        this.pnlControls.Visible = false;
                        this.pnlCustomer.Visible = false;
                        this.pnlFinalAmts.Visible = false;
                        this.label2.Visible = false;
                        this.label3.Visible = false;
                        this.label4.Visible = false;
                        this.cmbSource.Visible = false;
                        this.cmbSteward.Visible = false;
                        this.txtCover.Visible = false;
                        this.btnNewSource.Visible = false;
                        this.btnNewSteward.Visible = false;
                        this.splitContainer1.Panel2Collapsed = !this.splitContainer1.Panel2Collapsed;
                        this.splitContainer1.Update();
                        if (this.splitContainer1.Panel2Collapsed)
                        {
                            this.panel1.Left += 140;
                        }
                        else
                        {
                            this.panel1.Left -= 140;
                        }
                        SendKeys.Send("{TAB}");
                        this.lstVwOrder.Font = new Font("Microsoft Sans Serif", 15f, FontStyle.Bold, GraphicsUnit.Point, 0);
                        this.gvItemFreeQty.Enabled = false;
                        this.pnlItem.Location = new Point(7, 6);
                        this.pnlMaster.Location = new Point(7, 480);
                        this.pnlMaster.Controls.Add(this.label36);
                        this.label36.Location = new Point(200, 0x16);
                        this.pnlMaster.Controls.Add(this.gvAmtTend);
                        this.gvAmtTend.Location = new Point(300, 0x16);
                        this.pnlMaster.Controls.Add(this.btnImagePnl);
                        this.btnImagePnl.Location = new Point(0x7b, 40);
                        this.pnlMaster.Controls.Add(this.label37);
                        this.label37.Location = new Point(230, 50);
                        this.pnlMaster.Controls.Add(this.gvChange);
                        this.gvChange.Location = new Point(300, 50);
                        this.pnlMaster.Controls.Add(this.label21);
                        this.label21.Location = new Point(450, 40);
                        this.label21.Font = new Font("Microsoft Sans Serif", 10f, FontStyle.Bold, GraphicsUnit.Point, 0);
                        this.pnlMaster.Controls.Add(this.gvOrderNetAmt);
                        this.gvOrderNetAmt.Location = new Point(560, 20);
                        this.gvOrderNetAmt.Font = new Font("Times New Roman", 25f);
                        this.gvOrderNetAmt.Size = new Size(130, 40);
                        this.lstVwOrder.Top = this.trVwOrder.Top;
                        this.lstVwOrder.Height += this.trVwOrder.Height + 0x69;
                    }
                    else
                    {
                        this.lstVwOrder.Top = this.trVwOrder.Top;
                        this.lstVwOrder.Height += this.trVwOrder.Height;
                    }
                    this.trVwOrder.Visible = false;
                    this.pnlKOT.Visible = false;
                    this.cmbSource.Enabled = false;
                    this.cmbSource.TabStop = false;
                    this.cmbCustOrderNo.Enabled = false;
                    this.cmbSource.BackColor = Color.White;
                    if (ParaDine.GlobalVariables.StrFinyear == "0809")
                    {
                        this.cmbSteward.Enabled = false;
                        this.cmbSteward.TabStop = false;
                    }
                    this.cmbSteward.BackColor = Color.White;
                    this.txtCover.Enabled = false;
                    this.txtCover.TabStop = false;
                    if (this.ObjBillSettings.CardSystemReq)
                    {
                        frmShowCard card = new frmShowCard();
                        card.ShowDialog();
                        this.bCard = card.EntCard;
                        this.bCardIssue = card.EntIssue;
                        this.lblCardCustomer.Text = this.bCardIssue.CustomerName;
                        this.lblCardBalance.Text = this.bCard.CardBalance().ToString("####0.00");
                    }
                }
                string paramSql = "SELECT CATEGORYNAME, CATEGORYID FROM RES_CATEGORY ORDER BY CATEGORYNAME";
                SqlDataAdapter sDA = new SqlDataAdapter();
                ParaDine.GlobalFill.FillDataSet(paramSql, "CATEGORY", this.DS, sDA);
                this.dgrdVwCategory.DataSource = this.DS.Tables["CATEGORY"];
                if (this.dgrdVwCategory.Columns.Count > 0)
                {
                    this.dgrdVwCategory.Columns[0].Width = 550;
                }
                this.dgrdVwCategory.Columns[1].Visible = false;
                paramSql = "SELECT ITEMNAME + ' (' + UOMNAME + ')' AS ITEMNAME, IU.SALEPRICE, IT.ITEMID, U.UOMID, IT.CATEGORYID  FROM RES_ITEM IT  INNER JOIN RES_ITEMUNITPRICE IU ON IU.ITEMID = IT.ITEMID  INNER JOIN RES_UOM U ON U.UOMID = IU.UOMID  WHERE IT.INACTIVE = 0 AND IU.STOCKPOINTID = " + ParaDine.GlobalVariables.StockPointId.ToString() + " ORDER BY ITEMNAME ";
                sDA = new SqlDataAdapter();
                ParaDine.GlobalFill.FillDataSet(paramSql, "CATGITEMS", this.DS, sDA);
                this.LoadNewOrder();
                if (this.SourceID > 0)
                {
                    this.cmbSource.SelectedValue = this.SourceID;
                    this.cmbSource_SelectionChangeCommitted(sender, e);
                    if (this.CustOrderMasterID == 0L)
                    {
                        this.cmbCustOrderNo.SelectedIndex = this.cmbCustOrderNo.Items.Count - 1;
                    }
                    this.cmbSource.TabStop = false;
                    this.cmbSteward.Focus();
                }
                this.dgrdVwCategory.Height = this.ObjBillSettings.CategoryViewHeight;
                Level level = new Level(Convert.ToInt16(ParaDine.GlobalFunctions.GetQueryValue("SELECT LEVELID FROM GLB_USER WHERE USERID =  " + ParaDine.GlobalVariables.UserID)));
                if (level.IsAdmin)
                {
                    this.btnTempBill.Enabled = true;
                    this.btnReprint.Enabled = true;
                }
                else
                {
                    this.btnTempBill.Enabled = false;
                    this.btnReprint.Enabled = false;
                }
                this.cmbCustOrderNo_SelectionChangeCommitted(sender, e);
                this.lblCardCustomer.Visible = this.ObjBillSettings.CardSystemReq && this.blnDirectBill;
                this.lblCardBalHeader.Visible = this.ObjBillSettings.CardSystemReq && this.blnDirectBill;
                this.lblCardBalance.Visible = this.ObjBillSettings.CardSystemReq && this.blnDirectBill;
                this.lblBalance.Visible = this.ObjBillSettings.CardSystemReq && this.blnDirectBill;
                this.lblBalHeader.Visible = this.ObjBillSettings.CardSystemReq && this.blnDirectBill;
                this.lblBillTotal.Visible = this.ObjBillSettings.CardSystemReq && this.blnDirectBill;
                this.lblBillTotHeader.Visible = this.ObjBillSettings.CardSystemReq && this.blnDirectBill;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In LoadBill");
            }
        }

        private void FrmBilling_Shown(object sender, EventArgs e)
        {
            if ((this.ObjBillSettings.CardSystemReq && this.blnDirectBill) && (this.bCard.CardId <= 0L))
            {
                base.Close();
            }
        }

        private object GetItemTableValue(string StrCol)
        {
            if (this.lstVwOrder.SelectedItems.Count > 0)
            {
                for (int i = 0; i < this.lstVwOrder.Columns.Count; i++)
                {
                    if (this.lstVwOrder.Columns[i].Text == StrCol)
                    {
                        return this.lstVwOrder.SelectedItems[0].SubItems[i].Text;
                    }
                }
            }
            return 0;
        }

        private void gvAmtTend_KeyDownEvent(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                double num = Convert.ToDouble(this.gvAmtTend.Value) - Convert.ToDouble(this.gvOrderNetAmt.Value);
                this.btnCash_Click(sender, new EventArgs());
                this.gvChange.Value = Convert.ToDecimal(num);
                this.tmrChange.Start();
            }
        }

        private void gvItemAmount_Change(object sender, EventArgs e)
        {
        }

        private void gvItemAmount_KeyDownEvent(object sender, KeyEventArgs e)
        {
        }

        private void gvItemAmount_KeyUpEvent(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.gvItemSaleQty.Focus();
            }
            else if (Convert.ToDouble(this.gvItemAmount.Value) > 0.0)
            {
                this.gvItemSaleQty.Value = Convert.ToDecimal((double)(Convert.ToDouble(this.gvItemAmount.Value) / Convert.ToDouble(this.gvItemSalePrice.Value)));
            }
        }

        private void gvItemDiscPerc_Leave(object sender, EventArgs e)
        {
            this.CalculateItemAmount();
        }

        private void gvItemSalePrice_Leave(object sender, EventArgs e)
        {
            this.CalculateItemAmount();
        }

        private void gvItemSalePrice_Validating(object sender, CancelEventArgs e)
        {
            if (this.ObjBillSettings.HIDING)
            {
                SendKeys.Send("{TAB}");
                SendKeys.Send("{Enter}");
            }
        }

        private void gvItemSaleQty_Enter(object sender, EventArgs e)
        {
            this.gvItemSaleQty.Select();
        }

        private void gvItemSaleQty_KeyDownEvent(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.F1)
            {
                this.gvItemAmount.Focus();
            }
        }

        private void gvItemSaleQty_Leave(object sender, EventArgs e)
        {
            this.CalculateItemAmount();
        }

        private void gvItemSaleQty_Validating(object sender, CancelEventArgs e)
        {
            if (this.ObjBillSettings.HIDING && !this.gvItemSalePrice.Enabled)
            {
                SendKeys.Send("{TAB}");
                SendKeys.Send("{Enter}");
            }
        }

        private void gvItemServTax_Leave(object sender, EventArgs e)
        {
            this.CalculateItemAmount();
        }

        private void gvItemTaxPerc_Leave(object sender, EventArgs e)
        {
            this.CalculateItemAmount();
        }

        private void gvOrderDiscPerc_Validating(object sender, CancelEventArgs e)
        {
            if (Convert.ToDouble(this.gvOrderDiscPerc.Value) > 0.0)
            {
                this.PopulateTotals();
            }
        }

        private void gvOrderDiscPercAmt_Validating(object sender, CancelEventArgs e)
        {
            this.PopulateTotals();
        }

        private void gvOrderTaxPerc_Validating(object sender, CancelEventArgs e)
        {
            if (Convert.ToDouble(this.gvOrderTaxPerc.Value) > 0.0)
            {
                this.PopulateTotals();
            }
        }

        private void gvOrderTaxPercAmt_Validating(object sender, CancelEventArgs e)
        {
            this.PopulateTotals();
        }

        private void increaseQtyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.lstVwOrder_KeyDown(sender, new KeyEventArgs(Keys.Add));
        }

        private void LoadCustomer()
        {
            Customer customer = new Customer(Convert.ToInt32(this.txtCustCode.Tag));
            this.txtCustCode.Text = customer.CustomerCode;
            this.txtCustName.Text = customer.CustomerName;
            this.txtCustPhone.Text = Convert.ToString(customer.ContactNo);
            this.txtAddress.Text = customer.Address;
        }

        private void LoadCustOrderItemFromGrid(long paramItemId, int paramUOMId)
        {
            Item item = new Item(paramItemId);
            CustOrderItem varItem = new CustOrderItem
            {
                Added = true,
                CusOrderKOTID = this.EntId.CustOrderKOTCollection[Convert.ToInt16(this.pnlKOT.Tag)].CustOrderKOTID,
                CustOrderItemID = 0L
            };
            if (item.Taxable)
            {
                varItem.ServiceTaxPerc = new BillSettings(1).SERVICECHARGE;
            }
            varItem.ItemID = item.ItemID;
            varItem.SaleQty = 1.0;
            varItem.FreeQty = 0.0;
            varItem.ItemDiscPerc = 0.0;
            varItem.Note = "";
            varItem.ItemTaxPerc = new Tax(item.TaxID).TaxPerc;
            varItem.SalePrice = new ItemUnitPrice(Convert.ToInt64(item.ItemID), Convert.ToInt32(paramUOMId), ParaDine.GlobalVariables.StockPointId).SalePrice;
            varItem.UOMId = Convert.ToInt32(paramUOMId);
            this.EntId.CustOrderKOTCollection[Convert.ToInt16(this.pnlKOT.Tag)].CustOrderItemCollection.Add(varItem);
            this.PopulateItems();
        }

        private void LoadDeliveryItems(DeliveryOrder EntDelivery)
        {
            this.EntId.CustOrderKOTCollection[Convert.ToInt32(this.pnlKOT.Tag)].CustOrderItemCollection.Clear();
            foreach (DeliveryOrderTrans trans in EntDelivery.DeliveryOrdtransCollection)
            {
                CustOrderItem varItem = new CustOrderItem
                {
                    ItemID = trans.ItemID,
                    UOMId = trans.UOMID,
                    SaleQty = trans.Qty,
                    SalePrice = trans.Price,
                    Note = trans.Remarks,
                    Added = true
                };
                this.EntId.CustOrderKOTCollection[Convert.ToInt32(this.pnlKOT.Tag)].CustOrderItemCollection.Add(varItem);
            }
            this.PopulateItems();
        }

        private void LoadEntities()
        {
            this.EntId.CustOrderMasterID = Convert.ToInt64(this.txtOrderNo.Tag);
            this.EntId.CustOrderNo = Convert.ToString(this.txtOrderNo.Text);
            this.EntId.SourceTypeId = Convert.ToInt32(this.cmbSource.SelectedValue);
            this.EntId.StewardID = Convert.ToInt32(this.cmbSteward.SelectedValue);
            this.EntId.MachineNo = Convert.ToInt16(this.txtMno.Text);
            this.EntId.CustomerID = Convert.ToInt32(this.txtCustCode.Tag);
            this.EntId.Cover = Convert.ToInt32(this.txtCover.Text);
            this.EntId.DeliveryAddress = Convert.ToString(this.txtDelvAddress.Text);
            this.EntId.DiscAmount = Convert.ToDouble(this.gvOrderDiscAmt.Value);
            this.EntId.TaxAmount = Convert.ToDouble(this.gvOrderTaxAmt.Value);
            this.EntId.OrderID = Convert.ToInt64(this.txtDeliveryOrder.Tag);
            this.EntId.Refund = Convert.ToDouble(this.gvRefund.Value);
        }

        private void LoadFields(CustOrderMaster paramMaster)
        {
            this.txtOrderNo.Tag = Convert.ToString(paramMaster.CustOrderMasterID);
            if (Convert.ToInt32(this.EntId.CustOrderMasterID) <= 0)
            {
                this.EntId.CustOrderNo = this.EntId.MaxCode();
            }
            this.txtOrderNo.Text = this.EntId.CustOrderNo;
            this.cmbSteward.SelectedValue = Convert.ToInt32(paramMaster.StewardID);
            this.txtCover.Text = Convert.ToString(paramMaster.Cover);
            this.txtMno.Text = Convert.ToString(paramMaster.MachineNo);
            if (!this.ObjBillSettings.BillTaxIncl)
            {
                this.gvOrderTaxPerc.Value = Convert.ToDecimal(this.ObjBillSettings.BILLTAX);
            }
            this.gvOrderDiscPercAmt.Value = Convert.ToDecimal(paramMaster.DiscAmount);
            if (paramMaster.OrderID == 0L)
            {
                this.txtCustCode.Tag = Convert.ToInt32(paramMaster.CustomerID);
                this.txtCustCode.Text = new Customer(Convert.ToInt32(paramMaster.CustomerID)).CustomerCode;
                this.txtCustCode_Validating(new object(), new CancelEventArgs());
                this.txtDelvAddress.Text = Convert.ToString(paramMaster.DeliveryAddress);
            }
            else
            {
                this.txtDeliveryOrder.Tag = paramMaster.OrderID;
                DeliveryOrder order = new DeliveryOrder(Convert.ToInt64(this.txtDeliveryOrder.Tag));
                this.txtDeliveryOrder.Text = order.OrderNo;
                this.txtCustCode.Tag = order.CustID;
                this.gvRefund.Value = Convert.ToDecimal(order.AdvanceAmount);
                this.LoadCustomer();
            }
            if (this.EntId.CustOrderKOTCollection.Count <= 0)
            {
                CustOrderKOT varKot = new CustOrderKOT();
                this.pnlKOT.Tag = "0";
                varKot.Added = true;
                varKot.CustOrderMasterID = this.EntId.CustOrderMasterID;
                varKot.CustOrderNo = this.EntId.CustOrderNo;
                varKot.KOTNumber = varKot.MaxCode();
                this.EntId.CustOrderKOTCollection.Add(varKot);
            }
            for (int i = 0; i < this.EntId.CustOrderKOTCollection.Count; i++)
            {
                this.pnlKOT.Tag = i;
                this.LoadKOTFields(this.EntId.CustOrderKOTCollection[i]);
                ParaDine.GlobalFill.FillListView(this.lstVwOrder, this.EntId.CustOrderKOTCollection.GetItemCollectionTable());
                this.SetLvwColWidth();
            }
            this.LoadOrderTree(paramMaster);
        }

        private void LoadItemEntities(CustOrderItem paramItem)
        {
            paramItem.ItemID = Convert.ToInt64(this.txtItemCode.Tag);
            paramItem.UOMId = Convert.ToInt32(this.cmbItemUOM.SelectedValue);
            paramItem.SalePrice = Convert.ToDouble(this.gvItemSalePrice.Value);
            paramItem.SaleQty = Convert.ToDouble(this.gvItemSaleQty.Value);
            paramItem.FreeQty = Convert.ToDouble(this.gvItemFreeQty.Value);
            paramItem.ItemDiscPerc = Convert.ToDouble(this.gvItemDiscPerc.Value);
            paramItem.ItemTaxPerc = Convert.ToDouble(this.gvItemTaxPerc.Value);
            paramItem.ServiceTaxPerc = Convert.ToDouble(this.gvItemServTax.Value);
            paramItem.ParcelStatus = Convert.ToBoolean(this.chkItemParcelStatus.Checked);
            paramItem.Note = Convert.ToString(this.txtItemNote.Text);
            this.LoadOrderTree(this.EntId);
        }

        private void LoadItemFields(CustOrderItem paramItem)
        {
            Item item = new Item(paramItem.ItemID);
            this.txtItemCode.Tag = Convert.ToInt64(item.ItemID);
            this.txtItemCode.Text = Convert.ToString(item.ItemCode);
            this.txtItemName.Text = Convert.ToString(item.ItemName);
            this.txtItemCode_KeyDown(this.txtItemCode, new KeyEventArgs(Keys.Enter));
            this.cmbItemUOM.SelectedValue = Convert.ToInt16(paramItem.UOMId);
            this.gvItemSalePrice.Value = Convert.ToDecimal(paramItem.SalePrice);
            this.gvItemSaleQty.Value = Convert.ToDecimal(paramItem.SaleQty);
            this.gvItemFreeQty.Value = Convert.ToDecimal(paramItem.FreeQty);
            this.gvItemDiscPerc.Value = Convert.ToDecimal(paramItem.ItemDiscPerc);
            this.gvItemServTax.Value = Convert.ToDecimal(paramItem.ServiceTaxPerc);
            this.txtItemNote.Text = Convert.ToString(paramItem.Note);
            this.chkItemParcelStatus.Checked = Convert.ToBoolean(paramItem.ParcelStatus);
        }

        private void LoadKOTEntities(CustOrderKOT paramKOT)
        {
            paramKOT.CustOrderKOTID = Convert.ToInt64(this.txtKOTNo.Tag);
            paramKOT.KOTNumber = Convert.ToString(this.txtKOTNo.Text);
            paramKOT.StartTime = Convert.ToDateTime(this.dtpStartTime.Value);
            paramKOT.EndTime = Convert.ToDateTime(this.dtpEndTime.Value);
            paramKOT.RefNo = Convert.ToString(this.txtRefNo.Text);
            paramKOT.UserID = ParaDine.GlobalVariables.UserID;
            this.LoadOrderTree(this.EntId);
        }

        private void LoadKOTFields(CustOrderKOT paramKOT)
        {
            this.txtKOTNo.Tag = Convert.ToInt64(paramKOT.CustOrderKOTID);
            this.txtKOTNo.Text = Convert.ToString(paramKOT.KOTNumber);
            this.txtRefNo.Text = Convert.ToString(paramKOT.RefNo);
            if ((paramKOT.StartTime < this.dtpStartTime.MaxDate) && (paramKOT.StartTime > this.dtpStartTime.MinDate))
            {
                this.dtpStartTime.Value = Convert.ToDateTime(paramKOT.StartTime);
            }
            else
            {
                this.dtpStartTime.Checked = false;
            }
            if ((paramKOT.EndTime < this.dtpEndTime.MaxDate) && (paramKOT.EndTime > this.dtpEndTime.MinDate))
            {
                this.dtpEndTime.Value = Convert.ToDateTime(paramKOT.EndTime);
            }
            else
            {
                this.dtpEndTime.Checked = false;
            }
            this.PopulateItems();
        }

        private void LoadNewOrder()
        {
            ParaDine.GlobalFunctions.ClearFields(this.splitContainer1.Panel1);
            this.EntId = new CustOrderMaster();
            this.EntId.DirectBill = this.blnDirectBill;
            this.EntId.Added = true;
            this.EntId.EntMoneyMaster.Added = true;
            this.LoadFields(this.EntId);
        }

        private void LoadOrderTree(CustOrderMaster paramMaster)
        {
            try
            {
                this.trVwOrder.Nodes.Clear();
                this.trVwOrder.Nodes.Add("Orders");
                this.trVwOrder.Nodes[0].Nodes.Add("Order - " + paramMaster.CustOrderNo);
                for (int i = 0; i < paramMaster.CustOrderKOTCollection.Count; i++)
                {
                    string text = "";
                    text = "KOT (" + paramMaster.CustOrderNo + "/" + Convert.ToString(paramMaster.CustOrderKOTCollection[i].KOTNumber) + "/" + Convert.ToString(paramMaster.CustOrderKOTCollection[i].RefNo) + ") -- " + paramMaster.CustOrderKOTCollection[i].GetKOTNetAmount.ToString("##0.00");
                    this.trVwOrder.Nodes[0].Nodes[this.trVwOrder.Nodes[0].Nodes.Count - 1].Nodes.Add(text);
                    this.trVwOrder.Nodes[0].Nodes[this.trVwOrder.Nodes[0].Nodes.Count - 1].Nodes[i].Tag = i;
                }
                this.trVwOrder.ExpandAll();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In LoadOrderTree");
            }
        }

        private void lstVwOrder_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                CustOrderItem item;
                if (e.KeyCode == Keys.Delete)
                {
                    if (Convert.ToInt64(this.txtKOTNo.Tag) <= 0L)
                    {
                        item = this.EntId.CustOrderKOTCollection[Convert.ToInt16(this.pnlKOT.Tag)].CustOrderItemCollection[this.GetItemTableId];
                        if (item.CustOrderItemID <= 0L)
                        {
                            item.Deleted = true;
                        }
                        this.PopulateItems();
                        this.LoadOrderTree(this.EntId);
                    }
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    if (Convert.ToInt64(this.txtKOTNo.Tag) <= 0L)
                    {
                        item = this.EntId.CustOrderKOTCollection[Convert.ToInt16(this.pnlKOT.Tag)].CustOrderItemCollection[this.GetItemTableId];
                        if (item.CustOrderItemID <= 0L)
                        {
                            this.pnlItem.Tag = this.GetItemTableId;
                            this.LoadItemFields(this.EntId.CustOrderKOTCollection[Convert.ToInt16(this.pnlKOT.Tag)].CustOrderItemCollection[this.GetItemTableId]);
                            this.txtItemCode.Focus();
                        }
                    }
                }
                else if (e.KeyCode == Keys.Add)
                {
                    if (Convert.ToInt64(this.txtKOTNo.Tag) <= 0L)
                    {
                        CustOrderItem item1 = this.EntId.CustOrderKOTCollection[Convert.ToInt16(this.pnlKOT.Tag)].CustOrderItemCollection[this.GetItemTableId];
                        item1.SaleQty++;
                        this.PopulateItems();
                    }
                }
                else if ((e.KeyCode == Keys.Subtract) && (Convert.ToInt64(this.txtKOTNo.Tag) <= 0L))
                {
                    CustOrderItem item2 = this.EntId.CustOrderKOTCollection[Convert.ToInt16(this.pnlKOT.Tag)].CustOrderItemCollection[this.GetItemTableId];
                    item2.SaleQty--;
                    this.PopulateItems();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In LstVwOrder");
            }
        }

        private void lstVwOrder_MouseClick(object sender, MouseEventArgs e)
        {
            if (((this.lstVwOrder.SelectedItems.Count > 0) && (e.Button == MouseButtons.Right)) && (Convert.ToInt64(this.txtKOTNo.Tag) <= 0L))
            {
                this.cntxMenuLvwItems.Show(this.lstVwOrder, new Point(e.X, e.Y));
            }
        }

        private void lstVwOrder_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.lstVwOrder_KeyDown(sender, new KeyEventArgs(Keys.Enter));
        }

        private void numericPad1_Enter(object sender, EventArgs e)
        {
        }

        private void parcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.EntId.CustOrderKOTCollection[Convert.ToInt16(this.pnlKOT.Tag)].CustOrderItemCollection[this.GetItemTableId].ParcelStatus = !this.EntId.CustOrderKOTCollection[Convert.ToInt16(this.pnlKOT.Tag)].CustOrderItemCollection[this.GetItemTableId].ParcelStatus;
            this.PopulateItems();
        }

        private void pnlCustomer_Leave(object sender, EventArgs e)
        {
            this.LoadEntities();
        }

        private void pnlKOT_Leave(object sender, EventArgs e)
        {
            try
            {
                this.LoadKOTEntities(this.EntId.CustOrderKOTCollection[Convert.ToInt16(this.pnlKOT.Tag)]);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in pnlKOT_Leave");
            }
        }

        private void pnlMaster_Leave(object sender, EventArgs e)
        {
            try
            {
                this.LoadEntities();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in pnlMaster_Leave");
            }
        }

        private void PopulateItems()
        {
            ParaDine.GlobalFill.FillListView(this.lstVwOrder, this.EntId.CustOrderKOTCollection[Convert.ToInt16(this.pnlKOT.Tag)].CustOrderItemCollection.GetCollectionTable());
            this.SetLvwColWidth();
            if (this.txtItemCode.Enabled)
            {
                this.txtItemCode.Focus();
            }
            this.PopulateTotals();
        }

        private void PopulateTotals()
        {
            if (Convert.ToDouble(this.gvOrderDiscPerc.Value) > 0.0)
            {
                this.gvOrderDiscPercAmt.Value = Convert.ToDecimal((double)(((this.EntId.GetOrderGrossAmt - this.EntId.GetOrderItemTaxAmt) * Convert.ToDouble(this.gvOrderDiscPerc.Value)) / 100.0));
            }
            if (Convert.ToDouble(this.gvOrderTaxPerc.Value) > 0.0)
            {
                this.gvOrderTaxPercAmt.Value = Convert.ToDecimal((double)((this.EntId.GetOrderGrossAmt * Convert.ToDouble(this.gvOrderTaxPerc.Value)) / 100.0));
            }
            this.EntId.DiscAmount = Convert.ToDouble(this.gvOrderDiscPercAmt.Value);
            this.EntId.TaxAmount = Convert.ToDouble(this.gvOrderTaxPercAmt.Value);
            this.gvOrderGrossAmt.Value = Convert.ToDecimal(this.EntId.GetOrderGrossAmt);
            this.gvOrderDiscAmt.Value = Convert.ToDecimal(this.EntId.DiscAmount);
            this.gvOrderTaxAmt.Value = Convert.ToDecimal(this.EntId.TaxAmount);
            this.gvOrderNetAmt.Value = Convert.ToDecimal((double)(this.EntId.GetOrderNetAmount - Convert.ToDouble(this.gvRefund.Value)));
            this.gvOrderItemDiscAmt.Value = Convert.ToDecimal(this.EntId.GetOrderItemDiscAmt);
            this.gvOrderItemServTax.Value = Convert.ToDecimal(this.EntId.GetOrderItemServTaxAmt);
            this.gvOrderItemTaxAmt.Value = Convert.ToDecimal(this.EntId.GetOrderItemTaxAmt);
            this.gvOrderTotalItems.Value = Convert.ToDecimal(this.EntId.GetOrderTotalQty);
            this.gvOrderTotalKOTs.Value = Convert.ToDecimal(this.EntId.CustOrderKOTCollection.Count);
            if (this.txtItemCode.Enabled)
            {
                this.txtItemCode.Focus();
            }
            this.lblCardBalance.Text = this.bCard.CardBalance().ToString("####0.00");
            this.lblBillTotal.Text = this.gvOrderNetAmt.Value.ToString("####0.00");
            this.lblBalance.Text = Convert.ToString((double)(Convert.ToDouble(this.lblCardBalance.Text) - Convert.ToDouble(this.lblBillTotal.Text)));
        }

        private void RefreshData()
        {
            try
            {
                ParaDine.GlobalFill.FillCombo("SELECT STEWARDID, STEWARDNAME FROM RES_STEWARD", this.cmbSteward);
                ParaDine.GlobalFill.FillCombo("SELECT TABLEID, TABLENAME FROM RES_TABLE", this.cmbSource);
                this.pnlItem.Tag = "-1";
                if (this.ObjBillSettings.CoveredNoOptional)
                {
                    this.txtCover.BackColor = Color.Ivory;
                }
                else
                {
                    this.txtCover.BackColor = Color.White;
                }
                if (this.ObjBillSettings.RefKotNoOptional)
                {
                    this.txtRefNo.BackColor = Color.Ivory;
                }
                else
                {
                    this.txtRefNo.BackColor = Color.White;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in frmBilling_RefreshData");
            }
        }

        private bool SaveOrder()
        {
            if (ParaDine.GlobalValidations.ValidateFields(this.splitContainer1.Panel1, this.TTip))
            {
                if (this.EntId.CustOrderKOTCollection.GetItemCollectionTable().Rows.Count > 0)
                {
                    if (Convert.ToInt32(this.EntId.CustOrderMasterID) <= 0)
                    {
                        this.EntId.CustOrderNo = this.EntId.MaxCode();
                        foreach (CustOrderKOT rkot in this.EntId.CustOrderKOTCollection)
                        {
                            rkot.CustOrderNo = this.EntId.CustOrderNo;
                        }
                    }
                    if (this.EntId.SourceType == Convert.ToChar("R"))
                    {
                        double num = 0.0;
                        foreach (MoneyTrans trans in this.EntId.EntMoneyMaster.MoneyTransCollection)
                        {
                            if (!trans.Deleted && new PaymentMode(trans.PaymentModeID).Credit)
                            {
                                num += trans.PaidAmount;
                            }
                        }
                        if (num > 0.0)
                        {
                        }
                    }
                    SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
                    try
                    {
                        if (this.EntId.Deleted)
                        {
                            this.EntId.Delete(sqlTrans, false);
                        }
                        else if (this.EntId.Added)
                        {
                            this.EntId.Add(sqlTrans, false);
                        }
                        else
                        {
                            this.EntId.Modify(sqlTrans, false);
                        }
                        if (this.ObjBillSettings.CardSystemReq)
                        {
                            this.bCardTrans = new CardTrans();
                            this.bCardTrans.CardIssueId = this.bCard.CardIssueId;
                            this.bCardTrans.CardTransDate = ParaDine.GlobalVariables.BusinessDateTime;
                            this.bCardTrans.CreditAmount = 0.0;
                            this.bCardTrans.DebitAmount = Convert.ToDouble(this.gvOrderNetAmt.Value);
                            this.bCardTrans.RefId = this.EntId.CustOrderMasterID;
                            this.bCardTrans.RefType = "BILL";
                            this.bCardTrans.TerminalNo = ParaDine.GlobalVariables.TerminalNo;
                            this.bCardTrans.UserId = ParaDine.GlobalVariables.UserID;
                            this.bCardTrans.Add(sqlTrans, false);
                        }
                        sqlTrans.Commit();
                        return true;
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(exception.Message, "Error in Generating KOT");
                        sqlTrans.Rollback();
                        return false;
                    }
                }
                return false;
            }
            MessageBox.Show("Order Cannot Be Saved", "Error in SaveOrder()");
            return false;
        }

        private void SetLvwColWidth()
        {
            try
            {
                if (this.ObjBillSettings.HIDING)
                {
                    this.lstVwOrder.Columns[0].Width = 280;
                    this.lstVwOrder.Columns[1].Width = 0;
                    this.lstVwOrder.Columns[2].Width = 120;
                    this.lstVwOrder.Columns[3].Width = 120;
                    this.lstVwOrder.Columns[4].Width = 0;
                    this.lstVwOrder.Columns[5].Width = 0;
                    this.lstVwOrder.Columns[6].Width = 0;
                    this.lstVwOrder.Columns[7].Width = 0;
                    this.lstVwOrder.Columns[8].Width = 180;
                    this.lstVwOrder.Columns[9].Width = 0;
                    this.lstVwOrder.Columns[10].Width = 0;
                    this.lstVwOrder.Columns[11].Width = 0;
                    this.lstVwOrder.Columns[12].Width = 0;
                    this.lstVwOrder.Columns[13].Width = 0;
                    this.lstVwOrder.Columns[14].Width = 0;
                }
                else
                {
                    this.lstVwOrder.Columns[0].Width = 170;
                    this.lstVwOrder.Columns[1].Width = 100;
                    this.lstVwOrder.Columns[2].Width = 50;
                    this.lstVwOrder.Columns[3].Width = 80;
                    this.lstVwOrder.Columns[4].Width = 50;
                    this.lstVwOrder.Columns[5].Width = 50;
                    this.lstVwOrder.Columns[6].Width = 50;
                    this.lstVwOrder.Columns[7].Width = 50;
                    this.lstVwOrder.Columns[8].Width = 100;
                    this.lstVwOrder.Columns[9].Width = 0;
                    this.lstVwOrder.Columns[10].Width = 0;
                    this.lstVwOrder.Columns[11].Width = 0;
                    this.lstVwOrder.Columns[12].Width = 0;
                    this.lstVwOrder.Columns[13].Width = 0;
                    this.lstVwOrder.Columns[14].Width = 60;
                    this.lstVwOrder.Columns[15].Width = 60;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In Setting Col Width");
            }
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {
        }

        private void splitContainer1_Panel1_Resize(object sender, EventArgs e)
        {
        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {
        }

        private void splitter1_SplitterMoved(object sender, SplitterEventArgs e)
        {
        }

        private void tmrChange_Tick(object sender, EventArgs e)
        {
            this.gvChange.Visible = !this.gvChange.Visible;
        }

        private void trVwOrder_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                if (e.Node.Parent == this.trVwOrder.Nodes[0].Nodes[0])
                {
                    this.pnlKOT.Tag = e.Node.Tag;
                    this.LoadKOTFields(this.EntId.CustOrderKOTCollection[Convert.ToInt16(e.Node.Tag)]);
                }
                else if (e.Node.Parent == this.trVwOrder.Nodes[0])
                {
                    ParaDine.GlobalFill.FillListView(this.lstVwOrder, this.EntId.CustOrderKOTCollection.GetItemCollectionTable());
                    this.SetLvwColWidth();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in trVwOrder_NodeMouseClick");
            }
        }

        private void txtCover_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.pnlKOT.Focus();
            }
        }

        private void txtCustCode_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                string paramSql = "SELECT * FROM RES_CUSTOMER WHERE INACTIVE = 0";
                new frmBrowseData(ParaDine.GlobalFill.FillDataTable(paramSql), this.txtCustCode, "Customer Details", 2).ShowDialog();
                this.LoadCustomer();
            }
        }

        private void txtCustCode_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.txtCustCode.Text.Trim() != "")
                {
                    Customer customer = new Customer(this.txtCustCode.Text.Trim());
                    this.txtCustCode.Tag = Convert.ToString(customer.Customerid);
                    this.txtCustName.Text = Convert.ToString(customer.CustomerName);
                    this.txtAddress.Text = Convert.ToString(customer.Address);
                    this.txtCustPhone.Text = Convert.ToString(customer.ContactNo);
                }
                else
                {
                    this.txtCustCode.Tag = 0;
                    this.txtCustName.Text = "";
                    this.txtCustPhone.Text = "";
                    this.txtAddress.Text = "";
                    this.txtDelvAddress.Text = "";
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in txtCustcode_Validating");
                e.Cancel = true;
            }
        }

        private void txtDeliveryOrder_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.txtDeliveryOrder.Text.Trim() != "")
                {
                    SqlCommand command = new SqlCommand("SELECT COUNT(*) FROM RES_VW_PENDINGDELIVERYORDER WHERE ORDERNO = '" + this.txtDeliveryOrder.Text.Trim() + "'", ParaDine.GlobalVariables.SqlConn);
                    if (Convert.ToInt16(command.ExecuteScalar()) == 0)
                    {
                        MessageBox.Show("Delivery Order doesn't exist or bill already exist against this delivery order.", "Delivery Order");
                        e.Cancel = true;
                    }
                    else
                    {
                        DeliveryOrder entDelivery = new DeliveryOrder(this.txtDeliveryOrder.Text.Trim());
                        this.txtDeliveryOrder.Tag = entDelivery.OrderID;
                        this.txtCustCode.Tag = entDelivery.CustID;
                        this.txtDelvAddress.Text = entDelivery.DeliveryAdd;
                        this.gvRefund.Value = Convert.ToDecimal(entDelivery.AdvanceAmount);
                        this.LoadCustomer();
                        this.LoadDeliveryItems(entDelivery);
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void txtItemCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Item item = new Item(this.txtItemCode.Text);
                    if (item.Inactive)
                    {
                        MessageBox.Show("Item in Inactive Mode", "ParaDine");
                    }
                    else
                    {
                        this.txtItemCode.Tag = Convert.ToString(item.ItemID);
                        this.txtItemName.Text = Convert.ToString(item.ItemName);
                        this.gvItemSalePrice.Enabled = Convert.ToBoolean(item.OpenRate);
                        this.gvItemSalePrice.TabStop = Convert.ToBoolean(item.OpenRate);
                        this.gvItemDiscPerc.Enabled = Convert.ToBoolean(item.Discountable);
                        this.gvItemDiscPerc.TabStop = Convert.ToBoolean(item.Discountable);
                        this.gvItemDiscPerc.Value = Convert.ToDecimal(item.ItemDiscPerc);
                        if (this.ObjBillSettings.AllTaxesInclusive)
                        {
                            this.gvItemTaxPerc.Value = 0M;
                        }
                        else
                        {
                            this.gvItemTaxPerc.Value = Convert.ToDecimal(new Tax(item.TaxID).TaxPerc);
                        }
                        if (Convert.ToInt16(this.pnlItem.Tag) < 0)
                        {
                            this.gvItemSaleQty.Value = 1M;
                        }
                        if (item.Taxable)
                        {
                            if (this.ObjBillSettings.AllTaxesInclusive)
                            {
                                this.gvItemServTax.Value = 0M;
                            }
                            else if (new StockPoint((long)ParaDine.GlobalVariables.StockPointId).ServiceTaxApplicable)
                            {
                                this.gvItemServTax.Value = Convert.ToDecimal(this.ObjBillSettings.SERVICECHARGE);
                            }
                            else
                            {
                                this.gvItemServTax.Value = 0M;
                            }
                        }
                        else
                        {
                            this.gvItemServTax.Value = 0M;
                        }
                        DataTable uOM = item.GetUOM(ParaDine.GlobalVariables.StockPointId);
                        if (uOM.Rows.Count > 0)
                        {
                            ParaDine.GlobalFill.FillCombo(uOM, this.cmbItemUOM);
                            if (uOM.Rows.Count == 1)
                            {
                                this.cmbItemUOM.Enabled = false;
                                this.cmbItemUOM.TabStop = false;
                            }
                            else
                            {
                                this.cmbItemUOM.Enabled = true;
                                this.cmbItemUOM.TabStop = true;
                            }
                            this.cmbItemUOM.SelectedIndex = 0;
                        }
                        else
                        {
                            this.cmbItemUOM.Enabled = true;
                            this.cmbItemUOM.TabStop = true;
                            ParaDine.GlobalFill.FillCombo("SELECT IT.UOMID, U.UOMNAME FROM RES_ITEM IT  INNER JOIN RES_UOM U ON U.UOMID = IT.UOMID WHERE IT.ITEMID = " + item.ItemID.ToString(), this.cmbItemUOM);
                            this.cmbItemUOM.SelectedValue = item.UOMID;
                            this.gvItemSalePrice.Value = Convert.ToDecimal(item.SalePrice);
                        }
                        this.chkItemParcelStatus.Checked = ParaDine.GlobalVariables.blnParcel;
                    }
                }
                else if (e.KeyCode == Keys.F1)
                {
                    new frmBrowseData(ParaDine.GlobalFill.FillDataTable("SELECT ITEMID, ITEMCODE, ITEMNAME FROM RES_ITEM WHERE ITEMID IN (SELECT ITEMID FROM RES_ITEMUNITPRICE WHERE STOCKPOINTID = " + ParaDine.GlobalVariables.StockPointId + ")"), this.txtItemCode, "ITEMS", 2).ShowDialog();
                }
                else if (e.KeyCode == Keys.Up)
                {
                    this.dgrdVwCategory.Focus();
                }
                else if (e.KeyCode == Keys.Down)
                {
                    this.dgrdVwCategory.Focus();
                }
                else if (e.KeyCode == Keys.Right)
                {
                    this.dgrdVwCategory.Focus();
                }
            }
            catch (Exception exception)
            {
                if (this.blnKeyBoardMenu)
                {
                    this.dgrdVwCategory.Focus();
                }
                else
                {
                    MessageBox.Show(exception.Message, "ERROR IN txtItemCode_KeyDown");
                    this.pnlItem.Focus();
                }
            }
        }

        private void txtItemCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ' ')
            {
                e.Handled = true;
                this.btnOptions_Click(sender, e);
            }
        }

        private void txtItemCode_TextChanged(object sender, EventArgs e)
        {
            this.tmrChange.Stop();
            this.gvChange.Visible = true;
        }

        private void txtItemNote_KeyDown(object sender, KeyEventArgs e)
        {
            if (Convert.ToInt64(this.txtKOTNo.Tag) <= 0L)
            {
                if ((e.KeyCode == Keys.Enter) && ParaDine.GlobalValidations.ValidateFields(this.pnlItem, this.TTip))
                {
                    if (Convert.ToDouble(this.gvItemSaleQty.Value) < 0.0)
                    {
                        double num = 0.0;
                        foreach (CustOrderKOT rkot in this.EntId.CustOrderKOTCollection)
                        {
                            foreach (CustOrderItem item in rkot.CustOrderItemCollection)
                            {
                                if (((Convert.ToInt32(this.pnlItem.Tag) < 0) || (item != this.EntId.CustOrderKOTCollection[Convert.ToInt16(this.pnlKOT.Tag)].CustOrderItemCollection[Convert.ToInt16(this.pnlItem.Tag)])) && ((item.ItemID == Convert.ToInt64(this.txtItemCode.Tag)) && (item.UOMId == Convert.ToInt32(this.cmbItemUOM.SelectedValue))))
                                {
                                    num += item.SaleQty;
                                }
                            }
                        }
                        if (num < -Convert.ToDouble(this.gvItemSaleQty.Value))
                        {
                            MessageBox.Show("Return Not Acceptable", "KOT Return", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                            return;
                        }
                    }
                    if ((this.ObjBillSettings.CardSystemReq && this.blnDirectBill) && (Convert.ToDouble(this.gvItemAmount.Value) > Convert.ToDouble(this.lblBalance.Text)))
                    {
                        MessageBox.Show("Insufficiant Balance", "Billing", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                    else
                    {
                        CustOrderItem item2;
                        if (this.EntId.CustOrderKOTCollection[0].CustOrderItemCollection.Count == 0)
                        {
                            this.EntId.CustOrderDate = ParaDine.GlobalVariables.BusinessDateTime;
                        }
                        if (Convert.ToInt32(this.pnlItem.Tag) < 0)
                        {
                            item2 = new CustOrderItem
                            {
                                CustOrderItemID = 0L,
                                Added = true
                            };
                            this.LoadItemEntities(item2);
                            this.LoadKOTEntities(this.EntId.CustOrderKOTCollection[Convert.ToInt32(this.pnlKOT.Tag)]);
                            item2.CusOrderKOTID = this.EntId.CustOrderKOTCollection[Convert.ToInt32(this.pnlKOT.Tag)].CustOrderKOTID;
                            this.EntId.CustOrderKOTCollection[Convert.ToInt32(this.pnlKOT.Tag)].CustOrderItemCollection.Add(item2);
                        }
                        else
                        {
                            item2 = this.EntId.CustOrderKOTCollection[Convert.ToInt16(this.pnlKOT.Tag)].CustOrderItemCollection[Convert.ToInt16(this.pnlItem.Tag)];
                            this.LoadItemEntities(item2);
                            item2.Modified = true;
                        }
                        this.PopulateItems();
                        this.LoadOrderTree(this.EntId);
                        ParaDine.GlobalFunctions.ClearFields(this.pnlItem);
                        this.pnlItem.Tag = "-1";
                        if (this.txtItemCode.Enabled)
                        {
                            this.txtItemCode.Focus();
                        }
                    }
                }
            }
            else if (MessageBox.Show("Can't Add or Edit Items for this KOT, Do U Want to Add New KOT ???", "New KOT", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                this.btnNewKOT_Click(sender, new EventArgs());
                this.txtItemNote_KeyDown(sender, e);
            }
        }

        private int GetItemTableId
        {
            get
            {
                if (this.lstVwOrder.SelectedItems.Count > 0)
                {
                    return (Convert.ToInt16(this.lstVwOrder.SelectedItems[0].SubItems[this.lstVwOrder.SelectedItems[0].SubItems.Count - 1].Text) - 1);
                }
                return 0;
            }
        }
    }
}
