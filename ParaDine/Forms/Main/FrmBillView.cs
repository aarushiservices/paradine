﻿//using CrystalDecisions.Windows.Forms;
using ParaDine.Forms.Printing;
using ParaDine.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Main
{
    public partial class FrmBillView : Form
    {
        private Button BtnBillCancel;
        private Button BtnDisplay;
        private Button BtnExit;
        private Button btnReprint;
        //private CrystalReportViewer CerRptMain;
        internal CheckBox chkReturnBills;
        private ComboBox cmbMachineNo;
        private ComboBox cmbSteward;
        private ComboBox cmbStockPoint;
        private ComboBox cmbTable;
      //private IContainer components = null;
        private DateTimePicker dateTimePicker1;
        private DataGridView DgvBillView;
        private DataSet DS = new DataSet();
        private DateTimePicker dtpFrOrderDate;
        private DateTimePicker dtpToOrderDate;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private int LevelId;
        private Panel pnlMain;
        private Panel pnlTools;
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private string StrSql;
        private TextBox txtOrderNoFr;
        private TextBox txtOrderNoTo;

        public FrmBillView()
        {
            this.InitializeComponent();
        }

        private void BtnBillCancel_Click(object sender, EventArgs e)
        {
            if (this.DgvBillView.SelectedRows.Count > 0)
            {
                new CustOrderMaster(Convert.ToInt64(this.DgvBillView.SelectedRows[0].Cells["Custordermasterid"].Value)).CancelBill();
                this.BtnDisplay_Click(sender, e);
            }
        }

        private void BtnDisplay_Click(object sender, EventArgs e)
        {
            try
            {
                this.DgvBillView.DataSource = null;
                this.DgvBillView.Rows.Clear();
                this.StrSql = "SELECT COM.CUSTORDERMASTERID, CUSTORDERNO,  CONVERT(VARCHAR, CUSTORDERDATE, 106) AS CUSTORDERDATE, MACHINENO as MNO, NETAMOUNT, PAYMENTLIST,STEWARDNAME, SOURCENAME AS TABLENAME, CancelledStatus AS CANCELLED, SETTLED  FROM RES_VW_CUSTORDERMASTER COM, RES_CUSTORDERKOT COK, RES_CUSTORDERITEM COI WHERE CUSTORDERNO <> '' AND  COI.CUSTORDERKOTID = COK.CUSTORDERKOTID AND  COK.CUSTORDERMASTERID = COM.CUSTORDERMASTERID ";
                if (this.txtOrderNoFr.Text.Trim() != "")
                {
                    this.StrSql = this.StrSql + " AND CUSTORDERNO >= '" + this.txtOrderNoFr.Text.Trim() + "'";
                }
                if (this.txtOrderNoTo.Text.Trim() != "")
                {
                    this.StrSql = this.StrSql + " AND CUSTORDERNO <= '" + this.txtOrderNoTo.Text.Trim() + "'";
                }
                if (this.dtpFrOrderDate.Checked)
                {
                    this.StrSql = string.Concat(new object[] { this.StrSql, " AND CONVERT(DATETIME, CONVERT(VARCHAR, CUSTORDERDATE, 112))  >= '", Convert.ToDateTime(this.dtpFrOrderDate.Text), "'" });
                }
                if (this.dtpToOrderDate.Checked)
                {
                    this.StrSql = string.Concat(new object[] { this.StrSql, " AND CONVERT(DATETIME, CONVERT(VARCHAR, CUSTORDERDATE, 112))  <= '", Convert.ToDateTime(this.dtpToOrderDate.Text), "'" });
                }
                if (this.cmbMachineNo.Text.Trim() != "")
                {
                    this.StrSql = this.StrSql + " AND MACHINENO = " + Convert.ToInt32(this.cmbMachineNo.SelectedValue);
                }
                if (Convert.ToInt32(this.cmbStockPoint.SelectedValue) > 0)
                {
                    this.StrSql = this.StrSql + " AND STOCKPOINTID = " + this.cmbStockPoint.SelectedValue;
                }
                if (this.cmbTable.Text.Trim() != "")
                {
                    this.StrSql = this.StrSql + " AND SOURCENAME = '" + this.cmbTable.Text + "'";
                }
                if (this.cmbSteward.Text.Trim() != "")
                {
                    this.StrSql = this.StrSql + " AND STEWARDNAME = '" + this.cmbSteward.Text + "'";
                }
                if (this.chkReturnBills.Checked)
                {
                    this.StrSql = this.StrSql + " AND COI.SALEQTY < 0";
                }
                this.StrSql = this.StrSql + " GROUP BY COM.CUSTORDERMASTERID, CUSTORDERNO,CUSTORDERDATE, MACHINENO, NETAMOUNT,  PAYMENTLIST,STEWARDNAME, SOURCENAME,CancelledStatus, SETTLED ORDER BY COM.CUSTORDERMASTERID ";
                GlobalFill.FillDataSet(this.StrSql, "BILLVIEW", this.DS, this.SDA);
                if (this.DS.Tables["BILLVIEW"].Rows.Count > 0)
                {
                    this.DgvBillView.DataSource = this.DS.Tables["BILLVIEW"].DefaultView;
                    this.DgvBillView.Columns[0].Visible = false;
                    this.DgvBillView.Columns[1].Width = 0x5d;
                    this.DgvBillView.Columns[2].Width = 0x6a;
                    this.DgvBillView.Columns[3].Width = 0x23;
                    this.DgvBillView.Columns[4].DefaultCellStyle.Format = "N2";
                    this.DgvBillView.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    this.DgvBillView.Columns[4].Width = 0x4e;
                    this.DgvBillView.Columns[5].Width = 0x48;
                    this.DgvBillView.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    this.DgvBillView.Columns[6].Width = 0x3a;
                }
                else
                {
                    MessageBox.Show("No Records To Display");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Display Report");
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            if ((GlobalVariables.MobileNoStr != "") && Sms.IsConnectionAvailable())
            {
                Sms.SendSMSQueue();
            }
            base.Close();
        }

        private void btnReprint_Click(object sender, EventArgs e)
        {
            if (this.DgvBillView.SelectedRows.Count > 0)
            {
                new FrmPrinting(Convert.ToInt64(this.DgvBillView.SelectedRows[0].Cells["CUSTORDERMASTERID"].Value), "BILL").ShowDialog();
            }
        }

        private void cmbRestaurant_SelectedValueChanged(object sender, EventArgs e)
        {
            this.LoadSource();
        }

        private void cmbRestaurant_SelectionChangeCommitted(object sender, EventArgs e)
        {
            this.StrSql = "SELECT TABLEID, TABLENAME FROM RES_TABLE ORDER BY 2";
            GlobalFill.FillCombo(this.StrSql, this.cmbTable);
        }

        private void DgvBillView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            this.DisplayReport();
        }

        private void DgvBillView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.DisplayReport();
                this.DgvBillView.Focus();
            }
        }

        private void DisplayReport()
        {
            if (this.DgvBillView.SelectedRows.Count > 0)
            {
                this.StrSql = "SELECT * FROM RES_VW_CUSTORDERREPORT WHERE CUSTORDERMASTERID = " + this.DgvBillView.SelectedRows[0].Cells["CUSTORDERMASTERID"].Value;
                GlobalFill.FillDataSet(this.StrSql, "BILL", this.DS, this.SDA);
                if (this.DS.Tables["BILL"].Rows.Count > 0)
                {
                    RptBillView view = new RptBillView(this.DS.Tables["BILL"]);
                    //view.SetDataSource(this.DS.Tables["BILL"]);
                   // this.CerRptMain.ReportSource = view;
                   //
                    //this.CerRptMain.Refresh();
                    view.Show();
                }
                else
                {
                    MessageBox.Show("Cannot Displayed Cancelled Bills.", "Display Bill", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
        }

       

        private void FrmBillView_Load(object sender, EventArgs e)
        {
            this.LevelId = new User(GlobalVariables.UserID).LevelID;
            this.StrSql = " SELECT ALLOWREAD FROM GLB_SECURITY WHERE PERMISSIONID = (SELECT PERMISSIONID FROM GLB_PERMISSION WHERE PERMISSIONNAME = 'Bill &View')  AND LEVELID = " + this.LevelId;
            if (!Convert.ToBoolean(GlobalFunctions.GetQueryValue(this.StrSql)))
            {
                this.BtnBillCancel.Enabled = false;
            }
            Level level = new Level(Convert.ToInt16(GlobalFunctions.GetQueryValue("SELECT LEVELID FROM GLB_USER WHERE USERID =  " + GlobalVariables.UserID)));
            if (level.IsAdmin)
            {
                this.btnReprint.Enabled = true;
                this.BtnBillCancel.Enabled = true;
            }
            else
            {
                this.btnReprint.Enabled = false;
                this.BtnBillCancel.Enabled = false;
            }
            new GlobalTheme().applyTheme(this);
            this.RefreshData();
        }

     

        private void LoadSource()
        {
            try
            {
                if (this.cmbStockPoint.Text.Trim() != "")
                {
                    if (true)
                    {
                        this.StrSql = "SELECT TABLEID, TABLENAME FROM RES_TABLE ORDER BY 2";
                    }
                    else
                    {
                        this.StrSql = "SELECT ROOMID, ROOMNUMBER FROM RRS_ROOM ORDER BY 2";
                    }
                    GlobalFill.FillCombo(this.StrSql, this.cmbTable);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in RefershData");
            }
        }

        private void pnlMain_Paint(object sender, PaintEventArgs e)
        {
        }

        private void RefreshData()
        {
            GlobalFill.FillCombo("SELECT TERMINALID, TERMINALNAME FROM GLB_TERMINAL ORDER BY 2", this.cmbMachineNo);
            GlobalFill.FillCombo("SELECT STOCKPOINTID, STOCKPOINTNAME FROM RES_STOCKPOINT ORDER BY 2", this.cmbStockPoint);
            this.cmbStockPoint.SelectedValue = GlobalVariables.StockPointId;
            GlobalFill.FillCombo("SELECT STEWARDID, STEWARDNAME FROM RES_STEWARD ORDER BY 2", this.cmbSteward);
            this.dtpFrOrderDate.Value = GlobalVariables.BusinessDate;
            this.dtpToOrderDate.Value = GlobalVariables.BusinessDate;
            this.cmbMachineNo.SelectedValue = GlobalVariables.TerminalNo;
        }
    }
}
