﻿using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Main
{
    public partial class frmBillModify : Form
    {
        internal Button btnClose;
        internal Button btnModify;
        internal Button btnRetreive;
        internal ComboBox cmbMachineNo;
        internal ComboBox cmbPaymode;
       // private IContainer components = null;
        private DataSet DS = new DataSet();
        internal DateTimePicker dtpBillDate;
        internal GroupBox GroupBox1;
        internal Panel grpfields;
        internal NumControl gvDiscountAmt;
        internal NumControl gvDiscPerc;
        internal NumControl gvGrossAmt;
        internal NumControl gvNetAmount;
        internal NumControl gvTaxAmt;
        internal Label Label1;
        internal Label Label2;
        internal Label Label3;
        internal Label Label4;
        internal Label Label5;
        internal Label label6;
        internal Label Label7;
        internal Label Label9;
        internal Label lblMachineNo;
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private SqlCommand SqlCmd = new SqlCommand();
        private string StrSql;
        private ToolTip TTip;
        internal TextBox txtBillNo;

        public frmBillModify()
        {
            this.InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.btnModify.Text == "&Modify")
                {
                    this.grpfields.Enabled = true;
                    this.btnModify.Text = "&Save";
                    this.cmbMachineNo.Focus();
                }
                else if (ParaDine.GlobalValidations.ValidateFields(this.grpfields, this.TTip) && (Convert.ToInt64(this.txtBillNo.Tag) > 0L))
                {
                    this.StrSql = string.Concat(new object[] { "UPDATE RES_CUSTORDERMASTER SET DISCAMOUNT = ", ((Convert.ToDouble(this.gvGrossAmt.Value) - Convert.ToDouble(this.gvDiscountAmt.Value)) * Convert.ToDouble(this.gvDiscPerc.Value)) / 100.0, ",  TAXAMOUNT = ", Convert.ToDouble(this.gvTaxAmt.Value), " WHERE CUSTORDERMASTERID = ", this.txtBillNo.Tag });
                    this.SqlCmd = new SqlCommand(this.StrSql, ParaDine.GlobalVariables.SqlConn);
                    this.SqlCmd.ExecuteNonQuery();
                    MessageBox.Show("BILL UPDATED SUCCESSFULLY!");
                    this.RefreshData();
                    this.ClearData();
                    this.grpfields.Enabled = false;
                    this.btnModify.Text = "&Modify";
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Update Bill");
            }
        }

        private void btnRetreive_Click(object sender, EventArgs e)
        {
            try
            {
                if ((this.txtBillNo.Text.Trim() != "") && (Convert.ToInt32(this.cmbPaymode.SelectedValue) > 0))
                {
                    this.StrSql = string.Concat(new object[] { "SELECT CM.CUSTORDERMASTERID, CV.GROSSAMOUNT, CM.DISCAMOUNT, CM.TAXAMOUNT, CV.NETAMOUNT  FROM RES_VW_CUSTORDERMASTER CV, RES_CUSTORDERMASTER CM  WHERE CV.CUSTORDERMASTERID = CM.CUSTORDERMASTERID AND CONVERT(VARCHAR, CM.CUSTORDERDATE, 112) = CONVERT(VARCHAR, CONVERT(DATETIME, '", this.dtpBillDate.Text, "'), 112) AND CV.CUSTORDERNO = '", this.txtBillNo.Text.Trim(), "' AND CV.MACHINENO = ", this.cmbMachineNo.SelectedValue, " AND PAYMENTLIST IN ('", this.cmbPaymode.Text, "')" });
                    this.SqlCmd = new SqlCommand(this.StrSql, ParaDine.GlobalVariables.SqlConn);
                    SqlDataReader reader = this.SqlCmd.ExecuteReader();
                    if (reader.Read())
                    {
                        this.txtBillNo.Tag = Convert.ToInt64(reader["CUSTORDERMASTERID"]);
                        this.gvGrossAmt.Value = Convert.ToDecimal(Convert.ToDouble(reader["GROSSAMOUNT"]).ToString());
                        this.gvDiscountAmt.Value = Convert.ToDecimal(Convert.ToDouble(reader["DISCAMOUNT"]).ToString());
                        this.gvTaxAmt.Value = Convert.ToDecimal(Convert.ToDouble(reader["TAXAMOUNT"]).ToString());
                        this.gvNetAmount.Value = Convert.ToDecimal(Convert.ToDouble(reader["NETAMOUNT"]).ToString());
                        this.gvDiscountAmt.Focus();
                    }
                    else
                    {
                        MessageBox.Show("BILL NOT FOUND!");
                    }
                    reader.Close();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Retrieving Values");
            }
        }

        private void CalculateTotal()
        {
            this.gvNetAmount.Value = Convert.ToDecimal((double)(((Convert.ToDouble(this.gvGrossAmt.Value) - Convert.ToDouble(this.gvDiscountAmt.Value)) + Convert.ToDouble(this.gvTaxAmt.Value)) - (((Convert.ToDouble(this.gvGrossAmt.Value) - Convert.ToDouble(this.gvDiscountAmt.Value)) * Convert.ToDouble(this.gvDiscPerc.Value)) / 100.0)));
        }

        private void ClearData()
        {
            this.gvDiscPerc.Value = Convert.ToDecimal("");
            this.gvDiscountAmt.Value = Convert.ToDecimal("");
            this.gvGrossAmt.Value = Convert.ToDecimal("");
            this.gvNetAmount.Value = Convert.ToDecimal("");
            this.gvTaxAmt.Value = Convert.ToDecimal("");
            this.txtBillNo.Text = "";
            this.cmbMachineNo.SelectedIndex = 0;
            this.cmbPaymode.SelectedIndex = 0;
        }

       

        private void frmBillModify_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.grpfields.Enabled = false;
            this.RefreshData();
        }

        private void gvDiscountAmt_Validated(object sender, EventArgs e)
        {
            this.CalculateTotal();
        }

        private void gvDiscPerc_Validated(object sender, EventArgs e)
        {
            this.CalculateTotal();
        }

        private void gvTaxAmt_Validated(object sender, EventArgs e)
        {
            this.CalculateTotal();
        }

     

        private void RefreshData()
        {
            ParaDine.GlobalFill.FillCombo("SELECT TERMINALID, TERMINALNAME FROM GLB_TERMINAL ORDER BY 2", this.cmbMachineNo);
            ParaDine.GlobalFill.FillCombo("SELECT PAYMENTMODEID, PAYMENTMODE FROM GLB_PAYMENTMODE ORDER BY 2", this.cmbPaymode);
        }
    }
}
