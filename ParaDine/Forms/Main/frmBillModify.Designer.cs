﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Main
{
    partial class frmBillModify
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grpfields = new System.Windows.Forms.Panel();
            this.Label9 = new System.Windows.Forms.Label();
            this.dtpBillDate = new System.Windows.Forms.DateTimePicker();
            this.cmbMachineNo = new System.Windows.Forms.ComboBox();
            this.lblMachineNo = new System.Windows.Forms.Label();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.gvDiscPerc = new ParaSysCom.NumControl();
            this.label6 = new System.Windows.Forms.Label();
            this.gvNetAmount = new ParaSysCom.NumControl();
            this.Label7 = new System.Windows.Forms.Label();
            this.gvTaxAmt = new ParaSysCom.NumControl();
            this.Label5 = new System.Windows.Forms.Label();
            this.gvDiscountAmt = new ParaSysCom.NumControl();
            this.Label4 = new System.Windows.Forms.Label();
            this.gvGrossAmt = new ParaSysCom.NumControl();
            this.Label3 = new System.Windows.Forms.Label();
            this.btnRetreive = new System.Windows.Forms.Button();
            this.cmbPaymode = new System.Windows.Forms.ComboBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.txtBillNo = new System.Windows.Forms.TextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnModify = new System.Windows.Forms.Button();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.grpfields.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpfields
            // 
            this.grpfields.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.grpfields.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.grpfields.Controls.Add(this.Label9);
            this.grpfields.Controls.Add(this.dtpBillDate);
            this.grpfields.Controls.Add(this.cmbMachineNo);
            this.grpfields.Controls.Add(this.lblMachineNo);
            this.grpfields.Controls.Add(this.GroupBox1);
            this.grpfields.Controls.Add(this.btnRetreive);
            this.grpfields.Controls.Add(this.cmbPaymode);
            this.grpfields.Controls.Add(this.Label2);
            this.grpfields.Controls.Add(this.Label1);
            this.grpfields.Controls.Add(this.txtBillNo);
            this.grpfields.Location = new System.Drawing.Point(6, 7);
            this.grpfields.Name = "grpfields";
            this.grpfields.Size = new System.Drawing.Size(410, 233);
            this.grpfields.TabIndex = 1;
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(256, 22);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(33, 13);
            this.Label9.TabIndex = 11;
            this.Label9.Text = "Date:";
            // 
            // dtpBillDate
            // 
            this.dtpBillDate.CustomFormat = "dd/MMM/yy";
            this.dtpBillDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBillDate.Location = new System.Drawing.Point(291, 18);
            this.dtpBillDate.Name = "dtpBillDate";
            this.dtpBillDate.Size = new System.Drawing.Size(105, 20);
            this.dtpBillDate.TabIndex = 2;
            // 
            // cmbMachineNo
            // 
            this.cmbMachineNo.BackColor = System.Drawing.Color.Ivory;
            this.cmbMachineNo.FormattingEnabled = true;
            this.cmbMachineNo.Location = new System.Drawing.Point(79, 17);
            this.cmbMachineNo.Name = "cmbMachineNo";
            this.cmbMachineNo.Size = new System.Drawing.Size(121, 21);
            this.cmbMachineNo.TabIndex = 1;
            // 
            // lblMachineNo
            // 
            this.lblMachineNo.AutoSize = true;
            this.lblMachineNo.Location = new System.Drawing.Point(8, 21);
            this.lblMachineNo.Name = "lblMachineNo";
            this.lblMachineNo.Size = new System.Drawing.Size(71, 13);
            this.lblMachineNo.TabIndex = 5;
            this.lblMachineNo.Text = "Machine No :";
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.gvDiscPerc);
            this.GroupBox1.Controls.Add(this.label6);
            this.GroupBox1.Controls.Add(this.gvNetAmount);
            this.GroupBox1.Controls.Add(this.Label7);
            this.GroupBox1.Controls.Add(this.gvTaxAmt);
            this.GroupBox1.Controls.Add(this.Label5);
            this.GroupBox1.Controls.Add(this.gvDiscountAmt);
            this.GroupBox1.Controls.Add(this.Label4);
            this.GroupBox1.Controls.Add(this.gvGrossAmt);
            this.GroupBox1.Controls.Add(this.Label3);
            this.GroupBox1.Location = new System.Drawing.Point(11, 110);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(385, 111);
            this.GroupBox1.TabIndex = 6;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Bill Details";
            // 
            // gvDiscPerc
            // 
            this.gvDiscPerc.DecimalRequired = true;
            this.gvDiscPerc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvDiscPerc.Location = new System.Drawing.Point(273, 51);
            this.gvDiscPerc.Name = "gvDiscPerc";
            this.gvDiscPerc.Size = new System.Drawing.Size(94, 20);
            this.gvDiscPerc.SymbolRequired = true;
            this.gvDiscPerc.TabIndex = 15;
            this.gvDiscPerc.Text = "`0.00";
            this.gvDiscPerc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvDiscPerc.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvDiscPerc.Validated += new System.EventHandler(this.gvDiscPerc_Validated);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(196, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Bill Disc%";
            // 
            // gvNetAmount
            // 
            this.gvNetAmount.DecimalRequired = true;
            this.gvNetAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvNetAmount.Location = new System.Drawing.Point(224, 85);
            this.gvNetAmount.Name = "gvNetAmount";
            this.gvNetAmount.Size = new System.Drawing.Size(94, 20);
            this.gvNetAmount.SymbolRequired = true;
            this.gvNetAmount.TabIndex = 10;
            this.gvNetAmount.TabStop = false;
            this.gvNetAmount.Text = "`0.00";
            this.gvNetAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvNetAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label7.Location = new System.Drawing.Point(147, 89);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(77, 13);
            this.Label7.TabIndex = 14;
            this.Label7.Text = "Net Amount.";
            // 
            // gvTaxAmt
            // 
            this.gvTaxAmt.DecimalRequired = true;
            this.gvTaxAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvTaxAmt.Location = new System.Drawing.Point(273, 19);
            this.gvTaxAmt.Name = "gvTaxAmt";
            this.gvTaxAmt.Size = new System.Drawing.Size(94, 20);
            this.gvTaxAmt.SymbolRequired = false;
            this.gvTaxAmt.TabIndex = 8;
            this.gvTaxAmt.Text = "0.00";
            this.gvTaxAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvTaxAmt.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvTaxAmt.Validated += new System.EventHandler(this.gvTaxAmt_Validated);
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.Location = new System.Drawing.Point(195, 23);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(78, 13);
            this.Label5.TabIndex = 10;
            this.Label5.Text = "Tax Amount.";
            // 
            // gvDiscountAmt
            // 
            this.gvDiscountAmt.DecimalRequired = true;
            this.gvDiscountAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvDiscountAmt.Location = new System.Drawing.Point(90, 51);
            this.gvDiscountAmt.Name = "gvDiscountAmt";
            this.gvDiscountAmt.Size = new System.Drawing.Size(94, 20);
            this.gvDiscountAmt.SymbolRequired = true;
            this.gvDiscountAmt.TabIndex = 7;
            this.gvDiscountAmt.Text = "`0.00";
            this.gvDiscountAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvDiscountAmt.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvDiscountAmt.Validated += new System.EventHandler(this.gvDiscountAmt_Validated);
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.Location = new System.Drawing.Point(4, 55);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(86, 13);
            this.Label4.TabIndex = 8;
            this.Label4.Text = "Discount Amt.";
            // 
            // gvGrossAmt
            // 
            this.gvGrossAmt.DecimalRequired = true;
            this.gvGrossAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvGrossAmt.Location = new System.Drawing.Point(90, 19);
            this.gvGrossAmt.Name = "gvGrossAmt";
            this.gvGrossAmt.Size = new System.Drawing.Size(94, 20);
            this.gvGrossAmt.SymbolRequired = true;
            this.gvGrossAmt.TabIndex = 6;
            this.gvGrossAmt.Text = "`0.00";
            this.gvGrossAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvGrossAmt.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(22, 23);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(68, 13);
            this.Label3.TabIndex = 6;
            this.Label3.Text = "Gross Amt.";
            // 
            // btnRetreive
            // 
            this.btnRetreive.Location = new System.Drawing.Point(145, 81);
            this.btnRetreive.Name = "btnRetreive";
            this.btnRetreive.Size = new System.Drawing.Size(120, 23);
            this.btnRetreive.TabIndex = 5;
            this.btnRetreive.Text = "Retreive";
            this.btnRetreive.UseVisualStyleBackColor = true;
            this.btnRetreive.Click += new System.EventHandler(this.btnRetreive_Click);
            // 
            // cmbPaymode
            // 
            this.cmbPaymode.BackColor = System.Drawing.Color.Ivory;
            this.cmbPaymode.FormattingEnabled = true;
            this.cmbPaymode.Location = new System.Drawing.Point(291, 44);
            this.cmbPaymode.Name = "cmbPaymode";
            this.cmbPaymode.Size = new System.Drawing.Size(105, 21);
            this.cmbPaymode.TabIndex = 4;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(207, 47);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(82, 13);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "Enter Paymode.";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(11, 48);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(68, 13);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "Enter Bill No.";
            // 
            // txtBillNo
            // 
            this.txtBillNo.BackColor = System.Drawing.Color.Ivory;
            this.txtBillNo.Location = new System.Drawing.Point(80, 44);
            this.txtBillNo.Name = "txtBillNo";
            this.txtBillNo.Size = new System.Drawing.Size(121, 20);
            this.txtBillNo.TabIndex = 3;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(213, 249);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 13;
            this.btnClose.TabStop = false;
            this.btnClose.Text = "C&lose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnModify
            // 
            this.btnModify.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModify.Location = new System.Drawing.Point(132, 249);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(75, 23);
            this.btnModify.TabIndex = 12;
            this.btnModify.Text = "&Modify";
            this.btnModify.UseVisualStyleBackColor = true;
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // frmBillModify
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(422, 281);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnModify);
            this.Controls.Add(this.grpfields);
            this.Name = "frmBillModify";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bill Update :";
            this.Load += new System.EventHandler(this.frmBillModify_Load);
            this.grpfields.ResumeLayout(false);
            this.grpfields.PerformLayout();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
    }
}