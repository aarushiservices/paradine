﻿//using Microsoft.Office.Interop.Excel;
using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Main
{
    partial class FrmBilling
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlMaster = new System.Windows.Forms.Panel();
            this.lblBalHeader = new System.Windows.Forms.Label();
            this.lblBalance = new System.Windows.Forms.Label();
            this.lblBillTotHeader = new System.Windows.Forms.Label();
            this.lblBillTotal = new System.Windows.Forms.Label();
            this.lblCardBalHeader = new System.Windows.Forms.Label();
            this.lblCardBalance = new System.Windows.Forms.Label();
            this.lblCardCustomer = new System.Windows.Forms.Label();
            this.chkKotPrntNotReq = new System.Windows.Forms.CheckBox();
            this.chkBillPrntNotReq = new System.Windows.Forms.CheckBox();
            this.cmbCustOrderNo = new System.Windows.Forms.ComboBox();
            this.cmbSource = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.cmbSteward = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.txtMno = new System.Windows.Forms.TextBox();
            this.txtCover = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.trVwOrder = new System.Windows.Forms.TreeView();
            this.lstVwOrder = new System.Windows.Forms.ListView();
            this.pnlKOT = new System.Windows.Forms.Panel();
            this.label31 = new System.Windows.Forms.Label();
            this.txtRefNo = new System.Windows.Forms.TextBox();
            this.btnNewKOT = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpStartTime = new System.Windows.Forms.DateTimePicker();
            this.btnKOT = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.txtKOTNo = new System.Windows.Forms.TextBox();
            this.pnlItem = new System.Windows.Forms.Panel();
            this.label35 = new System.Windows.Forms.Label();
            this.gvItemTaxPerc = new ParaSysCom.NumControl();
            this.gvItemFreeQty = new ParaSysCom.NumControl();
            this.gvItemSaleQty = new ParaSysCom.NumControl();
            this.chkItemParcelStatus = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.gvItemServTax = new ParaSysCom.NumControl();
            this.txtItemNote = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.gvItemAmount = new ParaSysCom.NumControl();
            this.label13 = new System.Windows.Forms.Label();
            this.gvItemDiscPerc = new ParaSysCom.NumControl();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.gvItemSalePrice = new ParaSysCom.NumControl();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbItemUOM = new System.Windows.Forms.ComboBox();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtItemCode = new System.Windows.Forms.TextBox();
            this.pnlControls = new System.Windows.Forms.Panel();
            this.btnordermerge = new System.Windows.Forms.Button();
            this.btnbillrecall = new System.Windows.Forms.Button();
            this.btnBillParking = new System.Windows.Forms.Button();
            this.btnTax = new System.Windows.Forms.Button();
            this.btnOrdDiscount = new System.Windows.Forms.Button();
            this.btnFinalAmts = new System.Windows.Forms.Button();
            this.btnCalculator = new System.Windows.Forms.Button();
            this.btnGenerateKOT = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnReprint = new System.Windows.Forms.Button();
            this.btnTempBill = new System.Windows.Forms.Button();
            this.btnSplitPayment = new System.Windows.Forms.Button();
            this.btnCash = new System.Windows.Forms.Button();
            this.btnChageTable = new System.Windows.Forms.Button();
            this.btnCustomer = new System.Windows.Forms.Button();
            this.pnlCustomer = new System.Windows.Forms.Panel();
            this.btnBrowseDeliveryOrder = new System.Windows.Forms.Button();
            this.label39 = new System.Windows.Forms.Label();
            this.txtDeliveryOrder = new System.Windows.Forms.TextBox();
            this.btnCopyAddress = new System.Windows.Forms.Button();
            this.btnOrderDetails = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.txtCustPhone = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtDelvAddress = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.btnBrowseCustomer = new System.Windows.Forms.Button();
            this.txtCustName = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtCustCode = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.pnlFinalAmts = new System.Windows.Forms.Panel();
            this.gvRefund = new ParaSysCom.NumControl();
            this.label40 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.gvOrderTaxPercAmt = new ParaSysCom.NumControl();
            this.gvOrderTaxPerc = new ParaSysCom.NumControl();
            this.gvChange = new ParaSysCom.NumControl();
            this.label37 = new System.Windows.Forms.Label();
            this.gvAmtTend = new ParaSysCom.NumControl();
            this.label36 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.gvOrderDiscPercAmt = new ParaSysCom.NumControl();
            this.gvOrderDiscPerc = new ParaSysCom.NumControl();
            this.gvOrderTotalItems = new ParaSysCom.NumControl();
            this.gvOrderTotalKOTs = new ParaSysCom.NumControl();
            this.btnOptions = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.gvOrderItemTaxAmt = new ParaSysCom.NumControl();
            this.label24 = new System.Windows.Forms.Label();
            this.gvOrderItemDiscAmt = new ParaSysCom.NumControl();
            this.label23 = new System.Windows.Forms.Label();
            this.gvOrderItemServTax = new ParaSysCom.NumControl();
            this.label22 = new System.Windows.Forms.Label();
            this.gvOrderNetAmt = new ParaSysCom.NumControl();
            this.label21 = new System.Windows.Forms.Label();
            this.gvOrderTaxAmt = new ParaSysCom.NumControl();
            this.label20 = new System.Windows.Forms.Label();
            this.gvOrderDiscAmt = new ParaSysCom.NumControl();
            this.label19 = new System.Windows.Forms.Label();
            this.gvOrderGrossAmt = new ParaSysCom.NumControl();
            this.label18 = new System.Windows.Forms.Label();
            this.btnImagePnl = new System.Windows.Forms.Button();
            this.btnNewSource = new System.Windows.Forms.Button();
            this.btnNewSteward = new System.Windows.Forms.Button();
            this.btnOrderSearch = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.dgrdVwItems = new System.Windows.Forms.DataGridView();
            this.dgrdVwCategory = new System.Windows.Forms.DataGridView();
            this.pnlDispCategory = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.cntxMenuGrdItems = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cntxMenuLvwItems = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.increaseQtyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.decreaseQtyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.parcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tmrChange = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlMaster.SuspendLayout();
            this.pnlKOT.SuspendLayout();
            this.pnlItem.SuspendLayout();
            this.pnlControls.SuspendLayout();
            this.pnlCustomer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlFinalAmts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrdVwItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgrdVwCategory)).BeginInit();
            this.pnlDispCategory.SuspendLayout();
            this.cntxMenuLvwItems.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            this.splitContainer1.Panel1.Controls.Add(this.btnImagePnl);
            this.splitContainer1.Panel1.Controls.Add(this.btnNewSource);
            this.splitContainer1.Panel1.Controls.Add(this.btnNewSteward);
            this.splitContainer1.Panel1.Controls.Add(this.btnOrderSearch);
            this.splitContainer1.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel1_Paint);
            this.splitContainer1.Panel1.Resize += new System.EventHandler(this.splitContainer1_Panel1_Resize);
            this.splitContainer1.Panel1MinSize = 0;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitter1);
            this.splitContainer1.Panel2.Controls.Add(this.dgrdVwItems);
            this.splitContainer1.Panel2.Controls.Add(this.dgrdVwCategory);
            this.splitContainer1.Panel2.Controls.Add(this.pnlDispCategory);
            this.splitContainer1.Panel2MinSize = 0;
            this.splitContainer1.Size = new System.Drawing.Size(1092, 618);
            this.splitContainer1.SplitterDistance = 725;
            this.splitContainer1.TabIndex = 0;
            this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pnlMaster);
            this.panel1.Controls.Add(this.trVwOrder);
            this.panel1.Controls.Add(this.lstVwOrder);
            this.panel1.Controls.Add(this.pnlKOT);
            this.panel1.Controls.Add(this.pnlItem);
            this.panel1.Controls.Add(this.pnlControls);
            this.panel1.Controls.Add(this.pnlCustomer);
            this.panel1.Controls.Add(this.pnlFinalAmts);
            this.panel1.Location = new System.Drawing.Point(0, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(717, 582);
            this.panel1.TabIndex = 0;
            // 
            // pnlMaster
            // 
            this.pnlMaster.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMaster.Controls.Add(this.lblBalHeader);
            this.pnlMaster.Controls.Add(this.lblBalance);
            this.pnlMaster.Controls.Add(this.lblBillTotHeader);
            this.pnlMaster.Controls.Add(this.lblBillTotal);
            this.pnlMaster.Controls.Add(this.lblCardBalHeader);
            this.pnlMaster.Controls.Add(this.lblCardBalance);
            this.pnlMaster.Controls.Add(this.lblCardCustomer);
            this.pnlMaster.Controls.Add(this.chkKotPrntNotReq);
            this.pnlMaster.Controls.Add(this.chkBillPrntNotReq);
            this.pnlMaster.Controls.Add(this.cmbCustOrderNo);
            this.pnlMaster.Controls.Add(this.cmbSource);
            this.pnlMaster.Controls.Add(this.label1);
            this.pnlMaster.Controls.Add(this.label2);
            this.pnlMaster.Controls.Add(this.label17);
            this.pnlMaster.Controls.Add(this.cmbSteward);
            this.pnlMaster.Controls.Add(this.label4);
            this.pnlMaster.Controls.Add(this.txtOrderNo);
            this.pnlMaster.Controls.Add(this.txtMno);
            this.pnlMaster.Controls.Add(this.txtCover);
            this.pnlMaster.Controls.Add(this.label3);
            this.pnlMaster.Location = new System.Drawing.Point(7, 4);
            this.pnlMaster.Name = "pnlMaster";
            this.pnlMaster.Size = new System.Drawing.Size(703, 78);
            this.pnlMaster.TabIndex = 1;
            this.pnlMaster.Leave += new System.EventHandler(this.pnlMaster_Leave);
            // 
            // lblBalHeader
            // 
            this.lblBalHeader.AutoSize = true;
            this.lblBalHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBalHeader.Location = new System.Drawing.Point(509, 58);
            this.lblBalHeader.Name = "lblBalHeader";
            this.lblBalHeader.Size = new System.Drawing.Size(61, 13);
            this.lblBalHeader.TabIndex = 23;
            this.lblBalHeader.Text = "Balance .";
            // 
            // lblBalance
            // 
            this.lblBalance.AutoSize = true;
            this.lblBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBalance.Location = new System.Drawing.Point(622, 48);
            this.lblBalance.Name = "lblBalance";
            this.lblBalance.Size = new System.Drawing.Size(49, 24);
            this.lblBalance.TabIndex = 22;
            this.lblBalance.Text = "0.00";
            // 
            // lblBillTotHeader
            // 
            this.lblBillTotHeader.AutoSize = true;
            this.lblBillTotHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBillTotHeader.Location = new System.Drawing.Point(492, 38);
            this.lblBillTotHeader.Name = "lblBillTotHeader";
            this.lblBillTotHeader.Size = new System.Drawing.Size(78, 13);
            this.lblBillTotHeader.TabIndex = 21;
            this.lblBillTotHeader.Text = "Bill Amount .";
            // 
            // lblBillTotal
            // 
            this.lblBillTotal.AutoSize = true;
            this.lblBillTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBillTotal.Location = new System.Drawing.Point(635, 32);
            this.lblBillTotal.Name = "lblBillTotal";
            this.lblBillTotal.Size = new System.Drawing.Size(36, 16);
            this.lblBillTotal.TabIndex = 20;
            this.lblBillTotal.Text = "0.00";
            // 
            // lblCardBalHeader
            // 
            this.lblCardBalHeader.AutoSize = true;
            this.lblCardBalHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCardBalHeader.Location = new System.Drawing.Point(479, 18);
            this.lblCardBalHeader.Name = "lblCardBalHeader";
            this.lblCardBalHeader.Size = new System.Drawing.Size(91, 13);
            this.lblCardBalHeader.TabIndex = 19;
            this.lblCardBalHeader.Text = "Card Balance .";
            // 
            // lblCardBalance
            // 
            this.lblCardBalance.AutoSize = true;
            this.lblCardBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCardBalance.Location = new System.Drawing.Point(635, 12);
            this.lblCardBalance.Name = "lblCardBalance";
            this.lblCardBalance.Size = new System.Drawing.Size(36, 16);
            this.lblCardBalance.TabIndex = 18;
            this.lblCardBalance.Text = "0.00";
            // 
            // lblCardCustomer
            // 
            this.lblCardCustomer.AutoSize = true;
            this.lblCardCustomer.Location = new System.Drawing.Point(483, 3);
            this.lblCardCustomer.Name = "lblCardCustomer";
            this.lblCardCustomer.Size = new System.Drawing.Size(76, 13);
            this.lblCardCustomer.TabIndex = 17;
            this.lblCardCustomer.Text = "Card Customer";
            // 
            // chkKotPrntNotReq
            // 
            this.chkKotPrntNotReq.AutoSize = true;
            this.chkKotPrntNotReq.Location = new System.Drawing.Point(241, 45);
            this.chkKotPrntNotReq.Name = "chkKotPrntNotReq";
            this.chkKotPrntNotReq.Size = new System.Drawing.Size(115, 17);
            this.chkKotPrntNotReq.TabIndex = 16;
            this.chkKotPrntNotReq.TabStop = false;
            this.chkKotPrntNotReq.Tag = "NoClear";
            this.chkKotPrntNotReq.Text = "KOT Print Not Req";
            this.chkKotPrntNotReq.UseVisualStyleBackColor = true;
            // 
            // chkBillPrntNotReq
            // 
            this.chkBillPrntNotReq.AutoSize = true;
            this.chkBillPrntNotReq.Location = new System.Drawing.Point(359, 45);
            this.chkBillPrntNotReq.Name = "chkBillPrntNotReq";
            this.chkBillPrntNotReq.Size = new System.Drawing.Size(106, 17);
            this.chkBillPrntNotReq.TabIndex = 15;
            this.chkBillPrntNotReq.TabStop = false;
            this.chkBillPrntNotReq.Tag = "NoClear";
            this.chkBillPrntNotReq.Text = "Bill Print Not Req";
            this.chkBillPrntNotReq.UseVisualStyleBackColor = true;
            // 
            // cmbCustOrderNo
            // 
            this.cmbCustOrderNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCustOrderNo.Enabled = false;
            this.cmbCustOrderNo.FormattingEnabled = true;
            this.cmbCustOrderNo.Location = new System.Drawing.Point(17, 18);
            this.cmbCustOrderNo.Name = "cmbCustOrderNo";
            this.cmbCustOrderNo.Size = new System.Drawing.Size(77, 21);
            this.cmbCustOrderNo.TabIndex = 14;
            this.cmbCustOrderNo.Tag = "NoClear";
            this.TTip.SetToolTip(this.cmbCustOrderNo, "Select Table or Room from List");
            this.cmbCustOrderNo.SelectionChangeCommitted += new System.EventHandler(this.cmbCustOrderNo_SelectionChangeCommitted);
            // 
            // cmbSource
            // 
            this.cmbSource.BackColor = System.Drawing.Color.White;
            this.cmbSource.FormattingEnabled = true;
            this.cmbSource.Location = new System.Drawing.Point(103, 18);
            this.cmbSource.Name = "cmbSource";
            this.cmbSource.Size = new System.Drawing.Size(110, 21);
            this.cmbSource.TabIndex = 1;
            this.cmbSource.Tag = "NoClear";
            this.TTip.SetToolTip(this.cmbSource, "Select Table or Room from List");
            this.cmbSource.SelectionChangeCommitted += new System.EventHandler(this.cmbSource_SelectionChangeCommitted);
            this.cmbSource.DropDownClosed += new System.EventHandler(this.cmbSource_DropDownClosed);
            this.cmbSource.Enter += new System.EventHandler(this.cmbSource_Enter);
            this.cmbSource.Validating += new System.ComponentModel.CancelEventHandler(this.cmbSource_Validating);
            this.cmbSource.Validated += new System.EventHandler(this.cmbSource_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Order No.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(109, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Table/Room.";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(100, 50);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(51, 13);
            this.label17.TabIndex = 12;
            this.label17.Text = "Machine.";
            // 
            // cmbSteward
            // 
            this.cmbSteward.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSteward.FormattingEnabled = true;
            this.cmbSteward.Location = new System.Drawing.Point(241, 18);
            this.cmbSteward.Name = "cmbSteward";
            this.cmbSteward.Size = new System.Drawing.Size(107, 21);
            this.cmbSteward.TabIndex = 2;
            this.TTip.SetToolTip(this.cmbSteward, "Select Steward from List");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(354, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Cover.";
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.BackColor = System.Drawing.Color.Ivory;
            this.txtOrderNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOrderNo.Enabled = false;
            this.txtOrderNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrderNo.Location = new System.Drawing.Point(18, 45);
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.ReadOnly = true;
            this.txtOrderNo.Size = new System.Drawing.Size(76, 20);
            this.txtOrderNo.TabIndex = 0;
            this.txtOrderNo.TabStop = false;
            // 
            // txtMno
            // 
            this.txtMno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMno.Enabled = false;
            this.txtMno.Location = new System.Drawing.Point(154, 45);
            this.txtMno.Name = "txtMno";
            this.txtMno.Size = new System.Drawing.Size(32, 20);
            this.txtMno.TabIndex = 4;
            this.TTip.SetToolTip(this.txtMno, "Enter No Of Persons");
            // 
            // txtCover
            // 
            this.txtCover.BackColor = System.Drawing.Color.Ivory;
            this.txtCover.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCover.Location = new System.Drawing.Point(354, 19);
            this.txtCover.Name = "txtCover";
            this.txtCover.Size = new System.Drawing.Size(84, 20);
            this.txtCover.TabIndex = 3;
            this.txtCover.Tag = "NoTheme";
            this.TTip.SetToolTip(this.txtCover, "Enter No Of Persons Covered ");
            this.txtCover.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCover_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(244, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Steward.";
            // 
            // trVwOrder
            // 
            this.trVwOrder.BackColor = System.Drawing.Color.White;
            this.trVwOrder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.trVwOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trVwOrder.Location = new System.Drawing.Point(7, 82);
            this.trVwOrder.Name = "trVwOrder";
            this.trVwOrder.Size = new System.Drawing.Size(386, 120);
            this.trVwOrder.TabIndex = 1;
            this.trVwOrder.TabStop = false;
            this.trVwOrder.Tag = "NoTheme";
            this.trVwOrder.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.trVwOrder_NodeMouseClick);
            // 
            // lstVwOrder
            // 
            this.lstVwOrder.BackColor = System.Drawing.Color.White;
            this.lstVwOrder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstVwOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstVwOrder.ForeColor = System.Drawing.Color.Navy;
            this.lstVwOrder.Location = new System.Drawing.Point(7, 202);
            this.lstVwOrder.MultiSelect = false;
            this.lstVwOrder.Name = "lstVwOrder";
            this.lstVwOrder.Size = new System.Drawing.Size(703, 171);
            this.lstVwOrder.TabIndex = 33;
            this.lstVwOrder.UseCompatibleStateImageBehavior = false;
            this.lstVwOrder.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstVwOrder_KeyDown);
            this.lstVwOrder.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lstVwOrder_MouseClick);
            this.lstVwOrder.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstVwOrder_MouseDoubleClick);
            // 
            // pnlKOT
            // 
            this.pnlKOT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlKOT.Controls.Add(this.label31);
            this.pnlKOT.Controls.Add(this.txtRefNo);
            this.pnlKOT.Controls.Add(this.btnNewKOT);
            this.pnlKOT.Controls.Add(this.label7);
            this.pnlKOT.Controls.Add(this.dtpEndTime);
            this.pnlKOT.Controls.Add(this.label6);
            this.pnlKOT.Controls.Add(this.dtpStartTime);
            this.pnlKOT.Controls.Add(this.btnKOT);
            this.pnlKOT.Controls.Add(this.label5);
            this.pnlKOT.Controls.Add(this.txtKOTNo);
            this.pnlKOT.Location = new System.Drawing.Point(392, 82);
            this.pnlKOT.Name = "pnlKOT";
            this.pnlKOT.Size = new System.Drawing.Size(318, 120);
            this.pnlKOT.TabIndex = 1;
            this.pnlKOT.Leave += new System.EventHandler(this.pnlKOT_Leave);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(26, 36);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(44, 13);
            this.label31.TabIndex = 10;
            this.label31.Text = "Ref No.";
            // 
            // txtRefNo
            // 
            this.txtRefNo.BackColor = System.Drawing.Color.White;
            this.txtRefNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRefNo.Location = new System.Drawing.Point(75, 32);
            this.txtRefNo.Name = "txtRefNo";
            this.txtRefNo.Size = new System.Drawing.Size(137, 20);
            this.txtRefNo.TabIndex = 1;
            this.txtRefNo.TabStop = false;
            this.txtRefNo.Text = "345";
            this.TTip.SetToolTip(this.txtRefNo, "Displays KOT No");
            // 
            // btnNewKOT
            // 
            this.btnNewKOT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewKOT.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNewKOT.ImageKey = "(none)";
            this.btnNewKOT.Location = new System.Drawing.Point(197, 67);
            this.btnNewKOT.Name = "btnNewKOT";
            this.btnNewKOT.Size = new System.Drawing.Size(113, 35);
            this.btnNewKOT.TabIndex = 4;
            this.btnNewKOT.TabStop = false;
            this.btnNewKOT.Text = "&New KOT";
            this.btnNewKOT.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNewKOT.UseVisualStyleBackColor = true;
            this.btnNewKOT.Click += new System.EventHandler(this.btnNewKOT_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 86);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "End Time.";
            // 
            // dtpEndTime
            // 
            this.dtpEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpEndTime.Location = new System.Drawing.Point(75, 82);
            this.dtpEndTime.Name = "dtpEndTime";
            this.dtpEndTime.ShowCheckBox = true;
            this.dtpEndTime.Size = new System.Drawing.Size(110, 20);
            this.dtpEndTime.TabIndex = 3;
            this.dtpEndTime.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Start Time.";
            // 
            // dtpStartTime
            // 
            this.dtpStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpStartTime.Location = new System.Drawing.Point(75, 56);
            this.dtpStartTime.Name = "dtpStartTime";
            this.dtpStartTime.ShowCheckBox = true;
            this.dtpStartTime.Size = new System.Drawing.Size(110, 20);
            this.dtpStartTime.TabIndex = 2;
            this.dtpStartTime.TabStop = false;
            // 
            // btnKOT
            // 
            this.btnKOT.ImageList = this.imageList1;
            this.btnKOT.Location = new System.Drawing.Point(219, 5);
            this.btnKOT.Name = "btnKOT";
            this.btnKOT.Size = new System.Drawing.Size(32, 23);
            this.btnKOT.TabIndex = 3;
            this.btnKOT.TabStop = false;
            this.btnKOT.UseVisualStyleBackColor = true;
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "KOT No.";
            // 
            // txtKOTNo
            // 
            this.txtKOTNo.BackColor = System.Drawing.Color.Ivory;
            this.txtKOTNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtKOTNo.Location = new System.Drawing.Point(75, 8);
            this.txtKOTNo.Name = "txtKOTNo";
            this.txtKOTNo.ReadOnly = true;
            this.txtKOTNo.Size = new System.Drawing.Size(137, 20);
            this.txtKOTNo.TabIndex = 0;
            this.txtKOTNo.TabStop = false;
            this.TTip.SetToolTip(this.txtKOTNo, "Displays KOT No");
            // 
            // pnlItem
            // 
            this.pnlItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlItem.Controls.Add(this.label35);
            this.pnlItem.Controls.Add(this.gvItemTaxPerc);
            this.pnlItem.Controls.Add(this.gvItemFreeQty);
            this.pnlItem.Controls.Add(this.gvItemSaleQty);
            this.pnlItem.Controls.Add(this.chkItemParcelStatus);
            this.pnlItem.Controls.Add(this.label16);
            this.pnlItem.Controls.Add(this.label15);
            this.pnlItem.Controls.Add(this.gvItemServTax);
            this.pnlItem.Controls.Add(this.txtItemNote);
            this.pnlItem.Controls.Add(this.label14);
            this.pnlItem.Controls.Add(this.gvItemAmount);
            this.pnlItem.Controls.Add(this.label13);
            this.pnlItem.Controls.Add(this.gvItemDiscPerc);
            this.pnlItem.Controls.Add(this.label12);
            this.pnlItem.Controls.Add(this.label11);
            this.pnlItem.Controls.Add(this.label10);
            this.pnlItem.Controls.Add(this.gvItemSalePrice);
            this.pnlItem.Controls.Add(this.label9);
            this.pnlItem.Controls.Add(this.cmbItemUOM);
            this.pnlItem.Controls.Add(this.txtItemName);
            this.pnlItem.Controls.Add(this.label8);
            this.pnlItem.Controls.Add(this.txtItemCode);
            this.pnlItem.Location = new System.Drawing.Point(7, 373);
            this.pnlItem.Name = "pnlItem";
            this.pnlItem.Size = new System.Drawing.Size(703, 82);
            this.pnlItem.TabIndex = 2;
            this.pnlItem.Tag = "NoValidation";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(547, 2);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(51, 13);
            this.label35.TabIndex = 20;
            this.label35.Text = "Item Tax.";
            // 
            // gvItemTaxPerc
            // 
            this.gvItemTaxPerc.BackColor = System.Drawing.Color.White;
            this.gvItemTaxPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvItemTaxPerc.DecimalRequired = true;
            this.gvItemTaxPerc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemTaxPerc.Location = new System.Drawing.Point(545, 17);
            this.gvItemTaxPerc.Name = "gvItemTaxPerc";
            this.gvItemTaxPerc.Size = new System.Drawing.Size(52, 20);
            this.gvItemTaxPerc.SymbolRequired = false;
            this.gvItemTaxPerc.TabIndex = 7;
            this.gvItemTaxPerc.TabStop = false;
            this.gvItemTaxPerc.Text = "0.00";
            this.gvItemTaxPerc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvItemTaxPerc.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvItemTaxPerc.Leave += new System.EventHandler(this.gvItemTaxPerc_Leave);
            // 
            // gvItemFreeQty
            // 
            this.gvItemFreeQty.BackColor = System.Drawing.Color.White;
            this.gvItemFreeQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvItemFreeQty.DecimalRequired = true;
            this.gvItemFreeQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemFreeQty.Location = new System.Drawing.Point(368, 17);
            this.gvItemFreeQty.Name = "gvItemFreeQty";
            this.gvItemFreeQty.Size = new System.Drawing.Size(52, 20);
            this.gvItemFreeQty.SymbolRequired = false;
            this.gvItemFreeQty.TabIndex = 4;
            this.gvItemFreeQty.TabStop = false;
            this.gvItemFreeQty.Text = "0.00";
            this.gvItemFreeQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvItemFreeQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // gvItemSaleQty
            // 
            this.gvItemSaleQty.BackColor = System.Drawing.Color.White;
            this.gvItemSaleQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvItemSaleQty.DecimalRequired = true;
            this.gvItemSaleQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemSaleQty.Location = new System.Drawing.Point(234, 17);
            this.gvItemSaleQty.Name = "gvItemSaleQty";
            this.gvItemSaleQty.Size = new System.Drawing.Size(55, 20);
            this.gvItemSaleQty.SymbolRequired = false;
            this.gvItemSaleQty.TabIndex = 2;
            this.gvItemSaleQty.Text = "0.00";
            this.gvItemSaleQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvItemSaleQty, "Enter Sale Quantity");
            this.gvItemSaleQty.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvItemSaleQty.Enter += new System.EventHandler(this.gvItemSaleQty_Enter);
            this.gvItemSaleQty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gvItemSaleQty_KeyDownEvent);
            this.gvItemSaleQty.Leave += new System.EventHandler(this.gvItemSaleQty_Leave);
            this.gvItemSaleQty.Validating += new System.ComponentModel.CancelEventHandler(this.gvItemSaleQty_Validating);
            // 
            // chkItemParcelStatus
            // 
            this.chkItemParcelStatus.AutoSize = true;
            this.chkItemParcelStatus.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkItemParcelStatus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.chkItemParcelStatus.Location = new System.Drawing.Point(625, 49);
            this.chkItemParcelStatus.Name = "chkItemParcelStatus";
            this.chkItemParcelStatus.Size = new System.Drawing.Size(54, 17);
            this.chkItemParcelStatus.TabIndex = 10;
            this.chkItemParcelStatus.Text = "Parcel";
            this.chkItemParcelStatus.UseVisualStyleBackColor = true;
            this.chkItemParcelStatus.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(370, 51);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(33, 13);
            this.label16.TabIndex = 18;
            this.label16.Text = "Note.";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(488, 2);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 13);
            this.label15.TabIndex = 17;
            this.label15.Text = "Serv Tax.";
            // 
            // gvItemServTax
            // 
            this.gvItemServTax.BackColor = System.Drawing.Color.White;
            this.gvItemServTax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvItemServTax.DecimalRequired = true;
            this.gvItemServTax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemServTax.Location = new System.Drawing.Point(486, 17);
            this.gvItemServTax.Name = "gvItemServTax";
            this.gvItemServTax.Size = new System.Drawing.Size(52, 20);
            this.gvItemServTax.SymbolRequired = false;
            this.gvItemServTax.TabIndex = 6;
            this.gvItemServTax.TabStop = false;
            this.gvItemServTax.Text = "0.00";
            this.gvItemServTax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvItemServTax.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvItemServTax.Leave += new System.EventHandler(this.gvItemServTax_Leave);
            // 
            // txtItemNote
            // 
            this.txtItemNote.BackColor = System.Drawing.Color.White;
            this.txtItemNote.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemNote.Location = new System.Drawing.Point(413, 49);
            this.txtItemNote.Name = "txtItemNote";
            this.txtItemNote.Size = new System.Drawing.Size(206, 20);
            this.txtItemNote.TabIndex = 9;
            this.txtItemNote.Tag = "NoHandler";
            this.txtItemNote.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItemNote_KeyDown);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(607, 2);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(46, 13);
            this.label14.TabIndex = 14;
            this.label14.Text = "Amount.";
            // 
            // gvItemAmount
            // 
            this.gvItemAmount.BackColor = System.Drawing.Color.White;
            this.gvItemAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvItemAmount.DecimalRequired = true;
            this.gvItemAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemAmount.Location = new System.Drawing.Point(604, 17);
            this.gvItemAmount.Name = "gvItemAmount";
            this.gvItemAmount.Size = new System.Drawing.Size(75, 20);
            this.gvItemAmount.SymbolRequired = true;
            this.gvItemAmount.TabIndex = 8;
            this.gvItemAmount.TabStop = false;
            this.gvItemAmount.Tag = "NoHandler";
            this.gvItemAmount.Text = "`0.00";
            this.gvItemAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvItemAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvItemAmount.TextChanged += new System.EventHandler(this.gvItemAmount_Change);
            this.gvItemAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gvItemAmount_KeyDownEvent);
            this.gvItemAmount.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gvItemAmount_KeyUpEvent);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(429, 2);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Disc(%)";
            // 
            // gvItemDiscPerc
            // 
            this.gvItemDiscPerc.BackColor = System.Drawing.Color.White;
            this.gvItemDiscPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvItemDiscPerc.DecimalRequired = true;
            this.gvItemDiscPerc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemDiscPerc.Location = new System.Drawing.Point(427, 17);
            this.gvItemDiscPerc.Name = "gvItemDiscPerc";
            this.gvItemDiscPerc.Size = new System.Drawing.Size(52, 20);
            this.gvItemDiscPerc.SymbolRequired = false;
            this.gvItemDiscPerc.TabIndex = 5;
            this.gvItemDiscPerc.TabStop = false;
            this.gvItemDiscPerc.Text = "0.00";
            this.gvItemDiscPerc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvItemDiscPerc.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvItemDiscPerc.Leave += new System.EventHandler(this.gvItemDiscPerc_Leave);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(370, 2);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "Free Qty.";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(238, 2);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 13);
            this.label11.TabIndex = 8;
            this.label11.Text = "Qty.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(296, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Price.";
            // 
            // gvItemSalePrice
            // 
            this.gvItemSalePrice.BackColor = System.Drawing.Color.White;
            this.gvItemSalePrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvItemSalePrice.DecimalRequired = true;
            this.gvItemSalePrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvItemSalePrice.Location = new System.Drawing.Point(292, 17);
            this.gvItemSalePrice.Name = "gvItemSalePrice";
            this.gvItemSalePrice.Size = new System.Drawing.Size(69, 20);
            this.gvItemSalePrice.SymbolRequired = true;
            this.gvItemSalePrice.TabIndex = 3;
            this.gvItemSalePrice.TabStop = false;
            this.gvItemSalePrice.Text = "`0.00";
            this.gvItemSalePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TTip.SetToolTip(this.gvItemSalePrice, "Enter Rate Of the Item");
            this.gvItemSalePrice.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvItemSalePrice.Leave += new System.EventHandler(this.gvItemSalePrice_Leave);
            this.gvItemSalePrice.Validating += new System.ComponentModel.CancelEventHandler(this.gvItemSalePrice_Validating);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(109, 2);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "UOM.";
            // 
            // cmbItemUOM
            // 
            this.cmbItemUOM.BackColor = System.Drawing.Color.Ivory;
            this.cmbItemUOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbItemUOM.FormattingEnabled = true;
            this.cmbItemUOM.Location = new System.Drawing.Point(105, 17);
            this.cmbItemUOM.Name = "cmbItemUOM";
            this.cmbItemUOM.Size = new System.Drawing.Size(123, 21);
            this.cmbItemUOM.TabIndex = 1;
            this.TTip.SetToolTip(this.cmbItemUOM, "Select UOM from List");
            this.cmbItemUOM.SelectedValueChanged += new System.EventHandler(this.cmbItemUOM_SelectedValueChanged);
            // 
            // txtItemName
            // 
            this.txtItemName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllSystemSources;
            this.txtItemName.BackColor = System.Drawing.Color.Ivory;
            this.txtItemName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtItemName.Location = new System.Drawing.Point(22, 42);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.ReadOnly = true;
            this.txtItemName.Size = new System.Drawing.Size(339, 30);
            this.txtItemName.TabIndex = 9;
            this.txtItemName.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Item Code.";
            // 
            // txtItemCode
            // 
            this.txtItemCode.BackColor = System.Drawing.Color.Ivory;
            this.txtItemCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemCode.Location = new System.Drawing.Point(22, 17);
            this.txtItemCode.Name = "txtItemCode";
            this.txtItemCode.Size = new System.Drawing.Size(76, 20);
            this.txtItemCode.TabIndex = 0;
            this.TTip.SetToolTip(this.txtItemCode, "Enter Item Code");
            this.txtItemCode.TextChanged += new System.EventHandler(this.txtItemCode_TextChanged);
            this.txtItemCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItemCode_KeyDown);
            this.txtItemCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemCode_KeyPress);
            // 
            // pnlControls
            // 
            this.pnlControls.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlControls.Controls.Add(this.btnordermerge);
            this.pnlControls.Controls.Add(this.btnbillrecall);
            this.pnlControls.Controls.Add(this.btnBillParking);
            this.pnlControls.Controls.Add(this.btnTax);
            this.pnlControls.Controls.Add(this.btnOrdDiscount);
            this.pnlControls.Controls.Add(this.btnFinalAmts);
            this.pnlControls.Controls.Add(this.btnCalculator);
            this.pnlControls.Controls.Add(this.btnGenerateKOT);
            this.pnlControls.Controls.Add(this.btnClear);
            this.pnlControls.Controls.Add(this.btnReprint);
            this.pnlControls.Controls.Add(this.btnTempBill);
            this.pnlControls.Controls.Add(this.btnSplitPayment);
            this.pnlControls.Controls.Add(this.btnCash);
            this.pnlControls.Controls.Add(this.btnChageTable);
            this.pnlControls.Controls.Add(this.btnCustomer);
            this.pnlControls.Location = new System.Drawing.Point(7, 455);
            this.pnlControls.Name = "pnlControls";
            this.pnlControls.Size = new System.Drawing.Size(703, 123);
            this.pnlControls.TabIndex = 32;
            // 
            // btnordermerge
            // 
            this.btnordermerge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnordermerge.Location = new System.Drawing.Point(589, 77);
            this.btnordermerge.Name = "btnordermerge";
            this.btnordermerge.Size = new System.Drawing.Size(104, 28);
            this.btnordermerge.TabIndex = 14;
            this.btnordermerge.Text = "Order Merge";
            this.btnordermerge.UseVisualStyleBackColor = true;
            this.btnordermerge.Click += new System.EventHandler(this.btnordermerge_Click);
            // 
            // btnbillrecall
            // 
            this.btnbillrecall.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbillrecall.Location = new System.Drawing.Point(589, 46);
            this.btnbillrecall.Name = "btnbillrecall";
            this.btnbillrecall.Size = new System.Drawing.Size(104, 28);
            this.btnbillrecall.TabIndex = 13;
            this.btnbillrecall.Text = "Bill ReCall";
            this.btnbillrecall.UseVisualStyleBackColor = true;
            this.btnbillrecall.Click += new System.EventHandler(this.btnbillrecall_Click);
            // 
            // btnBillParking
            // 
            this.btnBillParking.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBillParking.Location = new System.Drawing.Point(589, 15);
            this.btnBillParking.Name = "btnBillParking";
            this.btnBillParking.Size = new System.Drawing.Size(104, 28);
            this.btnBillParking.TabIndex = 12;
            this.btnBillParking.Text = "Bill Park";
            this.btnBillParking.UseVisualStyleBackColor = true;
            this.btnBillParking.Click += new System.EventHandler(this.btnBillParking_Click);
            // 
            // btnTax
            // 
            this.btnTax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTax.Location = new System.Drawing.Point(154, 15);
            this.btnTax.Name = "btnTax";
            this.btnTax.Size = new System.Drawing.Size(130, 28);
            this.btnTax.TabIndex = 11;
            this.btnTax.Text = "F11 - Tax";
            this.btnTax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTax.UseVisualStyleBackColor = true;
            this.btnTax.Click += new System.EventHandler(this.btnTax_Click);
            // 
            // btnOrdDiscount
            // 
            this.btnOrdDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOrdDiscount.Location = new System.Drawing.Point(9, 15);
            this.btnOrdDiscount.Name = "btnOrdDiscount";
            this.btnOrdDiscount.Size = new System.Drawing.Size(130, 28);
            this.btnOrdDiscount.TabIndex = 10;
            this.btnOrdDiscount.Text = "F7 - Discount";
            this.btnOrdDiscount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOrdDiscount.UseVisualStyleBackColor = true;
            this.btnOrdDiscount.Click += new System.EventHandler(this.btnOrdDiscount_Click);
            // 
            // btnFinalAmts
            // 
            this.btnFinalAmts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinalAmts.Location = new System.Drawing.Point(9, 77);
            this.btnFinalAmts.Name = "btnFinalAmts";
            this.btnFinalAmts.Size = new System.Drawing.Size(130, 28);
            this.btnFinalAmts.TabIndex = 8;
            this.btnFinalAmts.Text = "Home - Final Totals";
            this.btnFinalAmts.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFinalAmts.UseVisualStyleBackColor = true;
            this.btnFinalAmts.Click += new System.EventHandler(this.btnFinalAmts_Click);
            this.btnFinalAmts.ChangeUICues += new System.Windows.Forms.UICuesEventHandler(this.btnFinalAmts_ChangeUICues);
            // 
            // btnCalculator
            // 
            this.btnCalculator.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalculator.Location = new System.Drawing.Point(444, 15);
            this.btnCalculator.Name = "btnCalculator";
            this.btnCalculator.Size = new System.Drawing.Size(130, 28);
            this.btnCalculator.TabIndex = 7;
            this.btnCalculator.Text = "F10 - Calculator";
            this.btnCalculator.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCalculator.UseVisualStyleBackColor = true;
            this.btnCalculator.Click += new System.EventHandler(this.btnCalculator_Click);
            // 
            // btnGenerateKOT
            // 
            this.btnGenerateKOT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerateKOT.Location = new System.Drawing.Point(154, 77);
            this.btnGenerateKOT.Name = "btnGenerateKOT";
            this.btnGenerateKOT.Size = new System.Drawing.Size(130, 28);
            this.btnGenerateKOT.TabIndex = 6;
            this.btnGenerateKOT.Text = "F8 - Generate KOT";
            this.btnGenerateKOT.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGenerateKOT.UseVisualStyleBackColor = true;
            this.btnGenerateKOT.Click += new System.EventHandler(this.btnGenerateKOT_Click);
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Location = new System.Drawing.Point(299, 15);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(130, 28);
            this.btnClear.TabIndex = 5;
            this.btnClear.Text = "Esc - Clear";
            this.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnReprint
            // 
            this.btnReprint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReprint.Location = new System.Drawing.Point(444, 46);
            this.btnReprint.Name = "btnReprint";
            this.btnReprint.Size = new System.Drawing.Size(130, 28);
            this.btnReprint.TabIndex = 4;
            this.btnReprint.Text = "F4 - Reprint Bill";
            this.btnReprint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReprint.UseVisualStyleBackColor = true;
            this.btnReprint.Click += new System.EventHandler(this.btnReprint_Click);
            // 
            // btnTempBill
            // 
            this.btnTempBill.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTempBill.Location = new System.Drawing.Point(444, 77);
            this.btnTempBill.Name = "btnTempBill";
            this.btnTempBill.Size = new System.Drawing.Size(130, 28);
            this.btnTempBill.TabIndex = 3;
            this.btnTempBill.Text = "F3 - Temporary Bill";
            this.btnTempBill.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTempBill.UseVisualStyleBackColor = true;
            this.btnTempBill.Click += new System.EventHandler(this.btnTempBill_Click);
            // 
            // btnSplitPayment
            // 
            this.btnSplitPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSplitPayment.Location = new System.Drawing.Point(299, 77);
            this.btnSplitPayment.Name = "btnSplitPayment";
            this.btnSplitPayment.Size = new System.Drawing.Size(130, 28);
            this.btnSplitPayment.TabIndex = 2;
            this.btnSplitPayment.Text = "F9 - Split Payment";
            this.btnSplitPayment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSplitPayment.UseVisualStyleBackColor = true;
            this.btnSplitPayment.Click += new System.EventHandler(this.btnSplitPayment_Click);
            // 
            // btnCash
            // 
            this.btnCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCash.Location = new System.Drawing.Point(299, 46);
            this.btnCash.Name = "btnCash";
            this.btnCash.Size = new System.Drawing.Size(130, 28);
            this.btnCash.TabIndex = 1;
            this.btnCash.Text = "F5 - Cash Payment";
            this.btnCash.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCash.UseVisualStyleBackColor = true;
            this.btnCash.Click += new System.EventHandler(this.btnCash_Click);
            // 
            // btnChageTable
            // 
            this.btnChageTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChageTable.Location = new System.Drawing.Point(154, 46);
            this.btnChageTable.Name = "btnChageTable";
            this.btnChageTable.Size = new System.Drawing.Size(130, 28);
            this.btnChageTable.TabIndex = 0;
            this.btnChageTable.Text = "F12 - Change Table";
            this.btnChageTable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnChageTable.UseVisualStyleBackColor = true;
            this.btnChageTable.Click += new System.EventHandler(this.btnChageTable_Click);
            // 
            // btnCustomer
            // 
            this.btnCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCustomer.Location = new System.Drawing.Point(9, 46);
            this.btnCustomer.Name = "btnCustomer";
            this.btnCustomer.Size = new System.Drawing.Size(130, 28);
            this.btnCustomer.TabIndex = 9;
            this.btnCustomer.Text = "F6 - Customer";
            this.btnCustomer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCustomer.UseVisualStyleBackColor = true;
            this.btnCustomer.Click += new System.EventHandler(this.btnCustomer_Click);
            // 
            // pnlCustomer
            // 
            this.pnlCustomer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCustomer.Controls.Add(this.btnBrowseDeliveryOrder);
            this.pnlCustomer.Controls.Add(this.label39);
            this.pnlCustomer.Controls.Add(this.txtDeliveryOrder);
            this.pnlCustomer.Controls.Add(this.btnCopyAddress);
            this.pnlCustomer.Controls.Add(this.btnOrderDetails);
            this.pnlCustomer.Controls.Add(this.label30);
            this.pnlCustomer.Controls.Add(this.txtCustPhone);
            this.pnlCustomer.Controls.Add(this.label29);
            this.pnlCustomer.Controls.Add(this.txtDelvAddress);
            this.pnlCustomer.Controls.Add(this.pictureBox1);
            this.pnlCustomer.Controls.Add(this.txtAddress);
            this.pnlCustomer.Controls.Add(this.btnBrowseCustomer);
            this.pnlCustomer.Controls.Add(this.txtCustName);
            this.pnlCustomer.Controls.Add(this.label27);
            this.pnlCustomer.Controls.Add(this.txtCustCode);
            this.pnlCustomer.Controls.Add(this.label28);
            this.pnlCustomer.Location = new System.Drawing.Point(7, 455);
            this.pnlCustomer.Name = "pnlCustomer";
            this.pnlCustomer.Size = new System.Drawing.Size(703, 123);
            this.pnlCustomer.TabIndex = 3;
            this.pnlCustomer.Leave += new System.EventHandler(this.pnlCustomer_Leave);
            // 
            // btnBrowseDeliveryOrder
            // 
            this.btnBrowseDeliveryOrder.ImageList = this.imageList1;
            this.btnBrowseDeliveryOrder.Location = new System.Drawing.Point(526, 16);
            this.btnBrowseDeliveryOrder.Name = "btnBrowseDeliveryOrder";
            this.btnBrowseDeliveryOrder.Size = new System.Drawing.Size(29, 24);
            this.btnBrowseDeliveryOrder.TabIndex = 15;
            this.btnBrowseDeliveryOrder.TabStop = false;
            this.btnBrowseDeliveryOrder.UseVisualStyleBackColor = true;
            this.btnBrowseDeliveryOrder.Click += new System.EventHandler(this.btnBrowseDeliveryOrder_Click);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(438, 4);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(100, 13);
            this.label39.TabIndex = 14;
            this.label39.Text = "Delivery Order No. :";
            // 
            // txtDeliveryOrder
            // 
            this.txtDeliveryOrder.BackColor = System.Drawing.Color.White;
            this.txtDeliveryOrder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDeliveryOrder.Location = new System.Drawing.Point(441, 18);
            this.txtDeliveryOrder.Name = "txtDeliveryOrder";
            this.txtDeliveryOrder.Size = new System.Drawing.Size(84, 20);
            this.txtDeliveryOrder.TabIndex = 13;
            this.txtDeliveryOrder.TabStop = false;
            this.txtDeliveryOrder.Validating += new System.ComponentModel.CancelEventHandler(this.txtDeliveryOrder_Validating);
            // 
            // btnCopyAddress
            // 
            this.btnCopyAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCopyAddress.Location = new System.Drawing.Point(247, 76);
            this.btnCopyAddress.Name = "btnCopyAddress";
            this.btnCopyAddress.Size = new System.Drawing.Size(42, 23);
            this.btnCopyAddress.TabIndex = 12;
            this.btnCopyAddress.TabStop = false;
            this.btnCopyAddress.Text = ">>";
            this.btnCopyAddress.UseVisualStyleBackColor = true;
            this.btnCopyAddress.Click += new System.EventHandler(this.btnCopyAddress_Click);
            // 
            // btnOrderDetails
            // 
            this.btnOrderDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOrderDetails.Location = new System.Drawing.Point(554, 87);
            this.btnOrderDetails.Name = "btnOrderDetails";
            this.btnOrderDetails.Size = new System.Drawing.Size(135, 25);
            this.btnOrderDetails.TabIndex = 4;
            this.btnOrderDetails.Text = "Order Details";
            this.btnOrderDetails.UseVisualStyleBackColor = true;
            this.btnOrderDetails.Click += new System.EventHandler(this.btnOrderDetails_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(265, 4);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 13);
            this.label30.TabIndex = 10;
            this.label30.Text = "Phone.";
            // 
            // txtCustPhone
            // 
            this.txtCustPhone.BackColor = System.Drawing.Color.White;
            this.txtCustPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCustPhone.Location = new System.Drawing.Point(265, 18);
            this.txtCustPhone.Name = "txtCustPhone";
            this.txtCustPhone.ReadOnly = true;
            this.txtCustPhone.Size = new System.Drawing.Size(150, 20);
            this.txtCustPhone.TabIndex = 1;
            this.txtCustPhone.TabStop = false;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(302, 41);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(73, 13);
            this.label29.TabIndex = 8;
            this.label29.Text = "Delv Address.";
            // 
            // txtDelvAddress
            // 
            this.txtDelvAddress.BackColor = System.Drawing.Color.White;
            this.txtDelvAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDelvAddress.Location = new System.Drawing.Point(301, 57);
            this.txtDelvAddress.Multiline = true;
            this.txtDelvAddress.Name = "txtDelvAddress";
            this.txtDelvAddress.Size = new System.Drawing.Size(200, 59);
            this.txtDelvAddress.TabIndex = 3;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(582, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(82, 71);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // txtAddress
            // 
            this.txtAddress.BackColor = System.Drawing.Color.White;
            this.txtAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddress.Location = new System.Drawing.Point(32, 57);
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.ReadOnly = true;
            this.txtAddress.Size = new System.Drawing.Size(200, 59);
            this.txtAddress.TabIndex = 2;
            this.txtAddress.TabStop = false;
            // 
            // btnBrowseCustomer
            // 
            this.btnBrowseCustomer.ImageList = this.imageList1;
            this.btnBrowseCustomer.Location = new System.Drawing.Point(229, 16);
            this.btnBrowseCustomer.Name = "btnBrowseCustomer";
            this.btnBrowseCustomer.Size = new System.Drawing.Size(29, 24);
            this.btnBrowseCustomer.TabIndex = 3;
            this.btnBrowseCustomer.TabStop = false;
            this.btnBrowseCustomer.Text = "Cus";
            this.btnBrowseCustomer.UseVisualStyleBackColor = true;
            this.btnBrowseCustomer.Click += new System.EventHandler(this.btnBrowseCustomer_Click);
            // 
            // txtCustName
            // 
            this.txtCustName.BackColor = System.Drawing.Color.White;
            this.txtCustName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCustName.Location = new System.Drawing.Point(72, 18);
            this.txtCustName.Name = "txtCustName";
            this.txtCustName.ReadOnly = true;
            this.txtCustName.Size = new System.Drawing.Size(156, 20);
            this.txtCustName.TabIndex = 1;
            this.txtCustName.TabStop = false;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(7, 2);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(56, 13);
            this.label27.TabIndex = 1;
            this.label27.Text = "CustCode.";
            // 
            // txtCustCode
            // 
            this.txtCustCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCustCode.Location = new System.Drawing.Point(12, 18);
            this.txtCustCode.Name = "txtCustCode";
            this.txtCustCode.Size = new System.Drawing.Size(57, 20);
            this.txtCustCode.TabIndex = 0;
            this.txtCustCode.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtCustCode_KeyUp);
            this.txtCustCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtCustCode_Validating);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(31, 41);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(48, 13);
            this.label28.TabIndex = 5;
            this.label28.Text = "Address.";
            // 
            // pnlFinalAmts
            // 
            this.pnlFinalAmts.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFinalAmts.Controls.Add(this.gvRefund);
            this.pnlFinalAmts.Controls.Add(this.label40);
            this.pnlFinalAmts.Controls.Add(this.label38);
            this.pnlFinalAmts.Controls.Add(this.gvOrderTaxPercAmt);
            this.pnlFinalAmts.Controls.Add(this.gvOrderTaxPerc);
            this.pnlFinalAmts.Controls.Add(this.gvChange);
            this.pnlFinalAmts.Controls.Add(this.label37);
            this.pnlFinalAmts.Controls.Add(this.gvAmtTend);
            this.pnlFinalAmts.Controls.Add(this.label36);
            this.pnlFinalAmts.Controls.Add(this.label34);
            this.pnlFinalAmts.Controls.Add(this.gvOrderDiscPercAmt);
            this.pnlFinalAmts.Controls.Add(this.gvOrderDiscPerc);
            this.pnlFinalAmts.Controls.Add(this.gvOrderTotalItems);
            this.pnlFinalAmts.Controls.Add(this.gvOrderTotalKOTs);
            this.pnlFinalAmts.Controls.Add(this.btnOptions);
            this.pnlFinalAmts.Controls.Add(this.label26);
            this.pnlFinalAmts.Controls.Add(this.label25);
            this.pnlFinalAmts.Controls.Add(this.gvOrderItemTaxAmt);
            this.pnlFinalAmts.Controls.Add(this.label24);
            this.pnlFinalAmts.Controls.Add(this.gvOrderItemDiscAmt);
            this.pnlFinalAmts.Controls.Add(this.label23);
            this.pnlFinalAmts.Controls.Add(this.gvOrderItemServTax);
            this.pnlFinalAmts.Controls.Add(this.label22);
            this.pnlFinalAmts.Controls.Add(this.gvOrderNetAmt);
            this.pnlFinalAmts.Controls.Add(this.label21);
            this.pnlFinalAmts.Controls.Add(this.gvOrderTaxAmt);
            this.pnlFinalAmts.Controls.Add(this.label20);
            this.pnlFinalAmts.Controls.Add(this.gvOrderDiscAmt);
            this.pnlFinalAmts.Controls.Add(this.label19);
            this.pnlFinalAmts.Controls.Add(this.gvOrderGrossAmt);
            this.pnlFinalAmts.Controls.Add(this.label18);
            this.pnlFinalAmts.Location = new System.Drawing.Point(7, 455);
            this.pnlFinalAmts.Name = "pnlFinalAmts";
            this.pnlFinalAmts.Size = new System.Drawing.Size(703, 123);
            this.pnlFinalAmts.TabIndex = 13;
            // 
            // gvRefund
            // 
            this.gvRefund.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvRefund.DecimalRequired = true;
            this.gvRefund.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvRefund.Location = new System.Drawing.Point(124, 68);
            this.gvRefund.Name = "gvRefund";
            this.gvRefund.Size = new System.Drawing.Size(126, 20);
            this.gvRefund.SymbolRequired = true;
            this.gvRefund.TabIndex = 45;
            this.gvRefund.TabStop = false;
            this.gvRefund.Text = "`0.00";
            this.gvRefund.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvRefund.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(14, 71);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(109, 13);
            this.label40.TabIndex = 44;
            this.label40.Text = "Refund/Adv Amt :";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(76, 27);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(45, 13);
            this.label38.TabIndex = 43;
            this.label38.Text = "Tax(%)";
            // 
            // gvOrderTaxPercAmt
            // 
            this.gvOrderTaxPercAmt.BackColor = System.Drawing.Color.White;
            this.gvOrderTaxPercAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvOrderTaxPercAmt.DecimalRequired = true;
            this.gvOrderTaxPercAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvOrderTaxPercAmt.Location = new System.Drawing.Point(170, 24);
            this.gvOrderTaxPercAmt.Name = "gvOrderTaxPercAmt";
            this.gvOrderTaxPercAmt.Size = new System.Drawing.Size(80, 20);
            this.gvOrderTaxPercAmt.SymbolRequired = true;
            this.gvOrderTaxPercAmt.TabIndex = 42;
            this.gvOrderTaxPercAmt.Text = "`0.00";
            this.gvOrderTaxPercAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvOrderTaxPercAmt.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvOrderTaxPercAmt.Validating += new System.ComponentModel.CancelEventHandler(this.gvOrderTaxPercAmt_Validating);
            // 
            // gvOrderTaxPerc
            // 
            this.gvOrderTaxPerc.BackColor = System.Drawing.Color.White;
            this.gvOrderTaxPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvOrderTaxPerc.DecimalRequired = true;
            this.gvOrderTaxPerc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvOrderTaxPerc.Location = new System.Drawing.Point(124, 24);
            this.gvOrderTaxPerc.Name = "gvOrderTaxPerc";
            this.gvOrderTaxPerc.Size = new System.Drawing.Size(43, 20);
            this.gvOrderTaxPerc.SymbolRequired = false;
            this.gvOrderTaxPerc.TabIndex = 41;
            this.gvOrderTaxPerc.Text = "0.00";
            this.gvOrderTaxPerc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvOrderTaxPerc.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvOrderTaxPerc.Validating += new System.ComponentModel.CancelEventHandler(this.gvOrderTaxPerc_Validating);
            // 
            // gvChange
            // 
            this.gvChange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvChange.DecimalRequired = true;
            this.gvChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvChange.Location = new System.Drawing.Point(596, 69);
            this.gvChange.Name = "gvChange";
            this.gvChange.Size = new System.Drawing.Size(91, 20);
            this.gvChange.SymbolRequired = true;
            this.gvChange.TabIndex = 40;
            this.gvChange.TabStop = false;
            this.gvChange.Text = "`0.00";
            this.gvChange.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvChange.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(535, 73);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(58, 13);
            this.label37.TabIndex = 39;
            this.label37.Text = "Change :";
            // 
            // gvAmtTend
            // 
            this.gvAmtTend.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvAmtTend.DecimalRequired = true;
            this.gvAmtTend.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvAmtTend.Location = new System.Drawing.Point(376, 70);
            this.gvAmtTend.Name = "gvAmtTend";
            this.gvAmtTend.Size = new System.Drawing.Size(89, 20);
            this.gvAmtTend.SymbolRequired = true;
            this.gvAmtTend.TabIndex = 38;
            this.gvAmtTend.Tag = "NoHandler";
            this.gvAmtTend.Text = "`0.00";
            this.gvAmtTend.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvAmtTend.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvAmtTend.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gvAmtTend_KeyDownEvent);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(279, 73);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(94, 13);
            this.label36.TabIndex = 37;
            this.label36.Text = "Amount Given :";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(72, 5);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(49, 13);
            this.label34.TabIndex = 36;
            this.label34.Text = "Disc(%)";
            // 
            // gvOrderDiscPercAmt
            // 
            this.gvOrderDiscPercAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvOrderDiscPercAmt.DecimalRequired = true;
            this.gvOrderDiscPercAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvOrderDiscPercAmt.Location = new System.Drawing.Point(170, 2);
            this.gvOrderDiscPercAmt.Name = "gvOrderDiscPercAmt";
            this.gvOrderDiscPercAmt.Size = new System.Drawing.Size(80, 20);
            this.gvOrderDiscPercAmt.SymbolRequired = true;
            this.gvOrderDiscPercAmt.TabIndex = 35;
            this.gvOrderDiscPercAmt.Text = "`0.00";
            this.gvOrderDiscPercAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvOrderDiscPercAmt.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvOrderDiscPercAmt.Validating += new System.ComponentModel.CancelEventHandler(this.gvOrderDiscPercAmt_Validating);
            // 
            // gvOrderDiscPerc
            // 
            this.gvOrderDiscPerc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvOrderDiscPerc.DecimalRequired = true;
            this.gvOrderDiscPerc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvOrderDiscPerc.Location = new System.Drawing.Point(124, 2);
            this.gvOrderDiscPerc.Name = "gvOrderDiscPerc";
            this.gvOrderDiscPerc.Size = new System.Drawing.Size(43, 20);
            this.gvOrderDiscPerc.SymbolRequired = false;
            this.gvOrderDiscPerc.TabIndex = 34;
            this.gvOrderDiscPerc.Text = "0.00";
            this.gvOrderDiscPerc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvOrderDiscPerc.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.gvOrderDiscPerc.Validating += new System.ComponentModel.CancelEventHandler(this.gvOrderDiscPerc_Validating);
            // 
            // gvOrderTotalItems
            // 
            this.gvOrderTotalItems.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvOrderTotalItems.DecimalRequired = false;
            this.gvOrderTotalItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvOrderTotalItems.Location = new System.Drawing.Point(207, 46);
            this.gvOrderTotalItems.Name = "gvOrderTotalItems";
            this.gvOrderTotalItems.Size = new System.Drawing.Size(43, 20);
            this.gvOrderTotalItems.SymbolRequired = false;
            this.gvOrderTotalItems.TabIndex = 33;
            this.gvOrderTotalItems.TabStop = false;
            this.gvOrderTotalItems.Text = "0";
            this.gvOrderTotalItems.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvOrderTotalItems.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // gvOrderTotalKOTs
            // 
            this.gvOrderTotalKOTs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvOrderTotalKOTs.DecimalRequired = false;
            this.gvOrderTotalKOTs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvOrderTotalKOTs.Location = new System.Drawing.Point(124, 46);
            this.gvOrderTotalKOTs.Name = "gvOrderTotalKOTs";
            this.gvOrderTotalKOTs.Size = new System.Drawing.Size(43, 20);
            this.gvOrderTotalKOTs.SymbolRequired = false;
            this.gvOrderTotalKOTs.TabIndex = 32;
            this.gvOrderTotalKOTs.TabStop = false;
            this.gvOrderTotalKOTs.Text = "0";
            this.gvOrderTotalKOTs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvOrderTotalKOTs.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // btnOptions
            // 
            this.btnOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOptions.Location = new System.Drawing.Point(18, 91);
            this.btnOptions.Name = "btnOptions";
            this.btnOptions.Size = new System.Drawing.Size(114, 25);
            this.btnOptions.TabIndex = 31;
            this.btnOptions.Text = "Options";
            this.btnOptions.UseVisualStyleBackColor = true;
            this.btnOptions.Click += new System.EventHandler(this.btnOptions_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(52, 49);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(69, 13);
            this.label26.TabIndex = 29;
            this.label26.Text = "Tot KOTs :";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(170, 49);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(34, 13);
            this.label25.TabIndex = 27;
            this.label25.Text = "Qty :";
            // 
            // gvOrderItemTaxAmt
            // 
            this.gvOrderItemTaxAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvOrderItemTaxAmt.DecimalRequired = true;
            this.gvOrderItemTaxAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvOrderItemTaxAmt.Location = new System.Drawing.Point(376, 46);
            this.gvOrderItemTaxAmt.Name = "gvOrderItemTaxAmt";
            this.gvOrderItemTaxAmt.Size = new System.Drawing.Size(89, 20);
            this.gvOrderItemTaxAmt.SymbolRequired = true;
            this.gvOrderItemTaxAmt.TabIndex = 26;
            this.gvOrderItemTaxAmt.TabStop = false;
            this.gvOrderItemTaxAmt.Text = "`0.00";
            this.gvOrderItemTaxAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvOrderItemTaxAmt.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(276, 49);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(97, 13);
            this.label24.TabIndex = 25;
            this.label24.Text = "Total Item Tax :";
            // 
            // gvOrderItemDiscAmt
            // 
            this.gvOrderItemDiscAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvOrderItemDiscAmt.DecimalRequired = true;
            this.gvOrderItemDiscAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvOrderItemDiscAmt.Location = new System.Drawing.Point(376, 24);
            this.gvOrderItemDiscAmt.Name = "gvOrderItemDiscAmt";
            this.gvOrderItemDiscAmt.Size = new System.Drawing.Size(89, 20);
            this.gvOrderItemDiscAmt.SymbolRequired = true;
            this.gvOrderItemDiscAmt.TabIndex = 24;
            this.gvOrderItemDiscAmt.TabStop = false;
            this.gvOrderItemDiscAmt.Text = "`0.00";
            this.gvOrderItemDiscAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvOrderItemDiscAmt.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(272, 27);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(101, 13);
            this.label23.TabIndex = 23;
            this.label23.Text = "Total Item Disc :";
            // 
            // gvOrderItemServTax
            // 
            this.gvOrderItemServTax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvOrderItemServTax.DecimalRequired = true;
            this.gvOrderItemServTax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvOrderItemServTax.Location = new System.Drawing.Point(376, 2);
            this.gvOrderItemServTax.Name = "gvOrderItemServTax";
            this.gvOrderItemServTax.Size = new System.Drawing.Size(89, 20);
            this.gvOrderItemServTax.SymbolRequired = true;
            this.gvOrderItemServTax.TabIndex = 22;
            this.gvOrderItemServTax.TabStop = false;
            this.gvOrderItemServTax.Text = "`0.00";
            this.gvOrderItemServTax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvOrderItemServTax.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(274, 5);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(99, 13);
            this.label22.TabIndex = 21;
            this.label22.Text = "Total Serv Tax :";
            // 
            // gvOrderNetAmt
            // 
            this.gvOrderNetAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvOrderNetAmt.DecimalRequired = true;
            this.gvOrderNetAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvOrderNetAmt.Location = new System.Drawing.Point(559, 94);
            this.gvOrderNetAmt.Name = "gvOrderNetAmt";
            this.gvOrderNetAmt.Size = new System.Drawing.Size(130, 20);
            this.gvOrderNetAmt.SymbolRequired = true;
            this.gvOrderNetAmt.TabIndex = 20;
            this.gvOrderNetAmt.TabStop = false;
            this.gvOrderNetAmt.Text = "`0.00";
            this.gvOrderNetAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvOrderNetAmt.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(467, 99);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(81, 13);
            this.label21.TabIndex = 19;
            this.label21.Text = "Net Amount :";
            // 
            // gvOrderTaxAmt
            // 
            this.gvOrderTaxAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvOrderTaxAmt.DecimalRequired = true;
            this.gvOrderTaxAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvOrderTaxAmt.Location = new System.Drawing.Point(596, 46);
            this.gvOrderTaxAmt.Name = "gvOrderTaxAmt";
            this.gvOrderTaxAmt.Size = new System.Drawing.Size(91, 20);
            this.gvOrderTaxAmt.SymbolRequired = true;
            this.gvOrderTaxAmt.TabIndex = 18;
            this.gvOrderTaxAmt.TabStop = false;
            this.gvOrderTaxAmt.Text = "`0.00";
            this.gvOrderTaxAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvOrderTaxAmt.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(532, 49);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(61, 13);
            this.label20.TabIndex = 17;
            this.label20.Text = "Tax Amt :";
            // 
            // gvOrderDiscAmt
            // 
            this.gvOrderDiscAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvOrderDiscAmt.DecimalRequired = true;
            this.gvOrderDiscAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvOrderDiscAmt.Location = new System.Drawing.Point(596, 24);
            this.gvOrderDiscAmt.Name = "gvOrderDiscAmt";
            this.gvOrderDiscAmt.Size = new System.Drawing.Size(91, 20);
            this.gvOrderDiscAmt.SymbolRequired = true;
            this.gvOrderDiscAmt.TabIndex = 16;
            this.gvOrderDiscAmt.TabStop = false;
            this.gvOrderDiscAmt.Text = "`0.00";
            this.gvOrderDiscAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvOrderDiscAmt.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(503, 27);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(90, 13);
            this.label19.TabIndex = 15;
            this.label19.Text = "Discount Amt :";
            // 
            // gvOrderGrossAmt
            // 
            this.gvOrderGrossAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gvOrderGrossAmt.DecimalRequired = true;
            this.gvOrderGrossAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvOrderGrossAmt.Location = new System.Drawing.Point(596, 2);
            this.gvOrderGrossAmt.Name = "gvOrderGrossAmt";
            this.gvOrderGrossAmt.Size = new System.Drawing.Size(91, 20);
            this.gvOrderGrossAmt.SymbolRequired = true;
            this.gvOrderGrossAmt.TabIndex = 14;
            this.gvOrderGrossAmt.TabStop = false;
            this.gvOrderGrossAmt.Text = "`0.00";
            this.gvOrderGrossAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvOrderGrossAmt.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(521, 5);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(72, 13);
            this.label18.TabIndex = 13;
            this.label18.Text = "Gross Amt :";
            // 
            // btnImagePnl
            // 
            this.btnImagePnl.ImageList = this.imageList1;
            this.btnImagePnl.Location = new System.Drawing.Point(98, 3);
            this.btnImagePnl.Name = "btnImagePnl";
            this.btnImagePnl.Size = new System.Drawing.Size(31, 25);
            this.btnImagePnl.TabIndex = 13;
            this.btnImagePnl.TabStop = false;
            this.btnImagePnl.UseVisualStyleBackColor = true;
            this.btnImagePnl.Visible = false;
            this.btnImagePnl.Click += new System.EventHandler(this.btnImagePnl_Click);
            // 
            // btnNewSource
            // 
            this.btnNewSource.ImageList = this.imageList1;
            this.btnNewSource.Location = new System.Drawing.Point(132, 3);
            this.btnNewSource.Name = "btnNewSource";
            this.btnNewSource.Size = new System.Drawing.Size(31, 25);
            this.btnNewSource.TabIndex = 6;
            this.btnNewSource.TabStop = false;
            this.btnNewSource.UseVisualStyleBackColor = true;
            this.btnNewSource.Visible = false;
            // 
            // btnNewSteward
            // 
            this.btnNewSteward.ImageList = this.imageList1;
            this.btnNewSteward.Location = new System.Drawing.Point(169, 2);
            this.btnNewSteward.Name = "btnNewSteward";
            this.btnNewSteward.Size = new System.Drawing.Size(31, 25);
            this.btnNewSteward.TabIndex = 7;
            this.btnNewSteward.TabStop = false;
            this.btnNewSteward.UseVisualStyleBackColor = true;
            this.btnNewSteward.Visible = false;
            // 
            // btnOrderSearch
            // 
            this.btnOrderSearch.ImageList = this.imageList1;
            this.btnOrderSearch.Location = new System.Drawing.Point(60, 3);
            this.btnOrderSearch.Name = "btnOrderSearch";
            this.btnOrderSearch.Size = new System.Drawing.Size(31, 25);
            this.btnOrderSearch.TabIndex = 2;
            this.btnOrderSearch.TabStop = false;
            this.btnOrderSearch.UseVisualStyleBackColor = true;
            this.btnOrderSearch.Click += new System.EventHandler(this.btnOrderSearch_Click);
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.DimGray;
            this.splitter1.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 307);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(361, 10);
            this.splitter1.TabIndex = 2;
            this.splitter1.TabStop = false;
            this.splitter1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitter1_SplitterMoved);
            // 
            // dgrdVwItems
            // 
            this.dgrdVwItems.AllowUserToAddRows = false;
            this.dgrdVwItems.AllowUserToDeleteRows = false;
            this.dgrdVwItems.BackgroundColor = System.Drawing.SystemColors.ScrollBar;
            this.dgrdVwItems.ColumnHeadersHeight = 10;
            this.dgrdVwItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgrdVwItems.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgrdVwItems.Location = new System.Drawing.Point(0, 307);
            this.dgrdVwItems.Name = "dgrdVwItems";
            this.dgrdVwItems.ReadOnly = true;
            this.dgrdVwItems.RowHeadersVisible = false;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgrdVwItems.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgrdVwItems.RowTemplate.Height = 20;
            this.dgrdVwItems.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgrdVwItems.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrdVwItems.Size = new System.Drawing.Size(361, 309);
            this.dgrdVwItems.TabIndex = 4;
            this.dgrdVwItems.Enter += new System.EventHandler(this.dgrdVwItems_Enter);
            this.dgrdVwItems.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgrdVwItems_KeyDown);
            this.dgrdVwItems.Leave += new System.EventHandler(this.dgrdVwItems_Leave);
            this.dgrdVwItems.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dgrdVwItems_MouseDoubleClick);
            // 
            // dgrdVwCategory
            // 
            this.dgrdVwCategory.AllowUserToAddRows = false;
            this.dgrdVwCategory.AllowUserToDeleteRows = false;
            this.dgrdVwCategory.AllowUserToOrderColumns = true;
            this.dgrdVwCategory.AllowUserToResizeRows = false;
            this.dgrdVwCategory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgrdVwCategory.BackgroundColor = System.Drawing.SystemColors.ScrollBar;
            this.dgrdVwCategory.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal;
            this.dgrdVwCategory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrdVwCategory.ColumnHeadersVisible = false;
            this.dgrdVwCategory.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgrdVwCategory.Location = new System.Drawing.Point(0, 27);
            this.dgrdVwCategory.MultiSelect = false;
            this.dgrdVwCategory.Name = "dgrdVwCategory";
            this.dgrdVwCategory.ReadOnly = true;
            this.dgrdVwCategory.RowHeadersVisible = false;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.dgrdVwCategory.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgrdVwCategory.RowTemplate.DividerHeight = 1;
            this.dgrdVwCategory.RowTemplate.Height = 30;
            this.dgrdVwCategory.RowTemplate.ReadOnly = true;
            this.dgrdVwCategory.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgrdVwCategory.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgrdVwCategory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrdVwCategory.Size = new System.Drawing.Size(361, 280);
            this.dgrdVwCategory.TabIndex = 2;
            this.dgrdVwCategory.SelectionChanged += new System.EventHandler(this.dgrdVwCategory_SelectionChanged);
            this.dgrdVwCategory.Enter += new System.EventHandler(this.dgrdVwCategory_Enter);
            this.dgrdVwCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgrdVwCategory_KeyDown);
            this.dgrdVwCategory.Leave += new System.EventHandler(this.dgrdVwCategory_Leave);
            // 
            // pnlDispCategory
            // 
            this.pnlDispCategory.Controls.Add(this.label32);
            this.pnlDispCategory.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlDispCategory.Location = new System.Drawing.Point(0, 0);
            this.pnlDispCategory.Name = "pnlDispCategory";
            this.pnlDispCategory.Size = new System.Drawing.Size(361, 27);
            this.pnlDispCategory.TabIndex = 1;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(27, 8);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(74, 13);
            this.label32.TabIndex = 0;
            this.label32.Text = "Item Menu :";
            // 
            // cntxMenuGrdItems
            // 
            this.cntxMenuGrdItems.Name = "cntxMenuItem";
            this.cntxMenuGrdItems.Size = new System.Drawing.Size(61, 4);
            this.cntxMenuGrdItems.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.cntxMenuItem_ItemClicked);
            // 
            // cntxMenuLvwItems
            // 
            this.cntxMenuLvwItems.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.increaseQtyToolStripMenuItem,
            this.decreaseQtyToolStripMenuItem,
            this.deleteItemToolStripMenuItem,
            this.parcelToolStripMenuItem});
            this.cntxMenuLvwItems.Name = "cntxMenuLvwItems";
            this.cntxMenuLvwItems.Size = new System.Drawing.Size(144, 114);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.editToolStripMenuItem.Text = "Edit";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // increaseQtyToolStripMenuItem
            // 
            this.increaseQtyToolStripMenuItem.Name = "increaseQtyToolStripMenuItem";
            this.increaseQtyToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.increaseQtyToolStripMenuItem.Text = "Increase Qty";
            this.increaseQtyToolStripMenuItem.Click += new System.EventHandler(this.increaseQtyToolStripMenuItem_Click);
            // 
            // decreaseQtyToolStripMenuItem
            // 
            this.decreaseQtyToolStripMenuItem.Name = "decreaseQtyToolStripMenuItem";
            this.decreaseQtyToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.decreaseQtyToolStripMenuItem.Text = "Decrease Qty";
            this.decreaseQtyToolStripMenuItem.Click += new System.EventHandler(this.decreaseQtyToolStripMenuItem_Click);
            // 
            // deleteItemToolStripMenuItem
            // 
            this.deleteItemToolStripMenuItem.Name = "deleteItemToolStripMenuItem";
            this.deleteItemToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.deleteItemToolStripMenuItem.Text = "Delete Item";
            this.deleteItemToolStripMenuItem.Click += new System.EventHandler(this.deleteItemToolStripMenuItem_Click);
            // 
            // parcelToolStripMenuItem
            // 
            this.parcelToolStripMenuItem.Name = "parcelToolStripMenuItem";
            this.parcelToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.parcelToolStripMenuItem.Text = "Parcel";
            this.parcelToolStripMenuItem.Click += new System.EventHandler(this.parcelToolStripMenuItem_Click);
            // 
            // tmrChange
            // 
            this.tmrChange.Interval = 500;
            this.tmrChange.Tick += new System.EventHandler(this.tmrChange_Tick);
            // 
            // FrmBilling
            // 
            this.ClientSize = new System.Drawing.Size(1092, 618);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainer1);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmBilling";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customer Order";
            this.Activated += new System.EventHandler(this.FrmBilling_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmBilling_FormClosed);
            this.Load += new System.EventHandler(this.FrmBilling_Load);
            this.Shown += new System.EventHandler(this.FrmBilling_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmBilling_KeyDown);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.pnlMaster.ResumeLayout(false);
            this.pnlMaster.PerformLayout();
            this.pnlKOT.ResumeLayout(false);
            this.pnlKOT.PerformLayout();
            this.pnlItem.ResumeLayout(false);
            this.pnlItem.PerformLayout();
            this.pnlControls.ResumeLayout(false);
            this.pnlCustomer.ResumeLayout(false);
            this.pnlCustomer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlFinalAmts.ResumeLayout(false);
            this.pnlFinalAmts.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrdVwItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgrdVwCategory)).EndInit();
            this.pnlDispCategory.ResumeLayout(false);
            this.pnlDispCategory.PerformLayout();
            this.cntxMenuLvwItems.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
    }
}