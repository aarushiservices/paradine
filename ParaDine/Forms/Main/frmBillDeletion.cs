﻿using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Main
{
    public partial class frmBillDeletion : Form
    {
        internal Button BtnDelete;
        internal Button BtnDisplay;
        internal Button BtnExit;
       // private IContainer components = null;
        private DataSet DS = new DataSet();
        private DataTable DT;
        internal DateTimePicker dtpBillFrDate;
        internal NumControl gvAmount;
        internal NumControl gvTotalAmt;
        private ImageList ImgLst;
        internal Label Label1;
        internal Label Label11;
        internal Label Label13;
        internal Label Label2;
        internal Label Label3;
        internal Label Label5;
        internal Label Label6;
        internal Label Label8;
        internal Label Label9;
        internal Label lblCard;
        internal Label lblCash;
        internal Label lblNo;
        internal Label lblOthers;
        private Label lblPbar;
        internal Label lblTotAmount;
        internal ListView lvwBillItems;
        private BillSettings ObjBillsettings = new BillSettings(1);
        internal Panel Panel1;
        private ProgressBar Pbar;
        internal Panel pnlFilter;
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private SqlCommand SqlCmd = new SqlCommand();
        private string StrSql;
        internal TextBox txtBillFrNo;
        internal TextBox txtBillToNo;
        internal TextBox txtMNO;
        private Level uLevel;

        public frmBillDeletion()
        {
            this.InitializeComponent();
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            Exception exception;
            try
            {
                if ((ParaDine.GlobalVariables.BussDate == Convert.ToDateTime(this.dtpBillFrDate.Text)) && this.ObjBillsettings.HIDING)
                {
                    MessageBox.Show("Cannot delete current Bussiness sales");
                }
                else
                {
                    if (this.lvwBillItems.Items.Count > 0)
                    {
                        if (MessageBox.Show("Are You Sure To Delete All These Records?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) == DialogResult.Yes)
                        {
                            this.Pbar.Maximum = Convert.ToInt16(this.DT.Rows.Count);
                            this.lblPbar.Text = Convert.ToString("Deleting Bills, Wait...");
                            this.Pbar.Visible = true;
                            this.lblPbar.Visible = true;
                            foreach (DataRow row in this.DT.Rows)
                            {
                                SqlTransaction transaction = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
                                try
                                {
                                    this.SqlCmd.Transaction = transaction;
                                    this.StrSql = "RES_PROC_CUSTORDERMASTER 'BILLDELETE', " + Convert.ToInt64(row["CUSTORDERMASTERID"]);
                                    this.SqlCmd.CommandText = this.StrSql;
                                    this.SqlCmd.Connection = ParaDine.GlobalVariables.SqlConn;
                                    this.SqlCmd.ExecuteNonQuery();
                                    transaction.Commit();
                                    this.Pbar.Increment(1);
                                }
                                catch (Exception exception1)
                                {
                                    exception = exception1;
                                    transaction.Rollback();
                                    MessageBox.Show(exception.Message);
                                }
                                finally
                                {
                                    transaction.Dispose();
                                }
                            }
                            MessageBox.Show("Deleted Successfully!", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                            this.Pbar.Visible = false;
                            this.lblPbar.Visible = false;
                            this.RefreshData();
                            this.gvAmount.Value = Convert.ToDecimal("0.00");
                        }
                        ParaDine.GlobalFill.FillListView(this.lvwBillItems, this.DT);
                        this.SetColWidth();
                        this.gvTotalAmt.Value = Convert.ToDecimal(Convert.ToDouble(ParaDine.GlobalFunctions.GetColumnTotal(this.DT, "NETAMOUNT")));
                    }
                    else
                    {
                        MessageBox.Show("There are no Records To Delete", "Bill Delete", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    this.lvwBillItems.Items.Clear();
                }
            }
            catch (Exception exception2)
            {
                exception = exception2;
                MessageBox.Show(exception.Message, "Error in Deleting Bills");
            }
        }

        private void BtnDisplay_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dtpBillFrDate.Value == ParaDine.GlobalVariables.BusinessDate)
                {
                    MessageBox.Show("U cannot delete todays data");
                }
                this.lvwBillItems.Items.Clear();
                this.StrSql = " SELECT COM.CUSTORDERMASTERID, COM.CUSTORDERNO AS ORDERNO,COM.CUSTORDERDATE,DBO.RES_FUNC_GETORDERPAYMODES(COM.CUSTORDERMASTERID) AS PAYMENTMODE  ,  SUM(((COI.SALEPRICE * COI.SALEQTY - COI.SALEPRICE * COI.SALEQTY * COI.ITEMDISCPERC / 100))) AS NETAMOUNT  FROM RES_CUSTORDERMASTER COM  INNER JOIN RES_CUSTORDERKOT COK ON COK.CUSTORDERMASTERID = COM.CUSTORDERMASTERID  INNER JOIN RES_CUSTORDERITEM COI ON COK.CUSTORDERKOTID = COI.CUSTORDERKOTID  WHERE  CUSTORDERNO <> '' ";
                if (this.txtBillFrNo.Text.Trim() != "")
                {
                    this.StrSql = this.StrSql + " AND COM.CUSTORDERNO >= '" + this.txtBillFrNo.Text + "'";
                }
                if (this.txtBillToNo.Text.Trim() != "")
                {
                    this.StrSql = this.StrSql + " AND COM.CUSTORDERNO <= '" + this.txtBillToNo.Text + "'";
                }
                if (this.dtpBillFrDate.Checked)
                {
                    this.StrSql = this.StrSql + " AND CONVERT(VARCHAR, COM.CUSTORDERDATE, 112) = CONVERT(VARCHAR, CONVERT(DATETIME, '" + this.dtpBillFrDate.Text + "'), 112)";
                }
                if (this.txtMNO.Text != "")
                {
                    this.StrSql = this.StrSql + " AND COM.MACHINENO = " + this.txtMNO.Text;
                }
                this.StrSql = this.StrSql + " GROUP BY COM.CUSTORDERMASTERID, COM.CUSTORDERNO ,COM.CUSTORDERDATE,COM.MACHINENO,DBO.RES_FUNC_GETORDERPAYMODES(COM.CUSTORDERMASTERID) ";
                this.StrSql = this.StrSql + " ORDER BY CUSTORDERMASTERID DESC";
                this.SqlCmd = new SqlCommand(this.StrSql, ParaDine.GlobalVariables.SqlConn);
                this.SqlCmd.CommandTimeout = 0x186a0;
                this.SDA.SelectCommand = this.SqlCmd;
                this.DS = new DataSet();
                this.SDA.Fill(this.DS, "DETAILS");
                if (this.DS.Tables["DETAILS"].Rows.Count > 0)
                {
                    double num = 0.0;
                    this.DT = this.DS.Tables["DETAILS"].Clone();
                    foreach (DataRow row in this.DS.Tables["DETAILS"].Rows)
                    {
                        if (num < Convert.ToDouble(this.gvAmount.Value))
                        {
                            num += Convert.ToDouble(row["NETAMOUNT"]);
                            this.DT.ImportRow(row);
                        }
                        else
                        {
                            break;
                        }
                    }
                    ParaDine.GlobalFill.FillListView(this.lvwBillItems, this.DT);
                    this.SetColWidth();
                    this.gvTotalAmt.Value = Convert.ToDecimal(Convert.ToDouble(ParaDine.GlobalFunctions.GetColumnTotal(this.DT, "NETAMOUNT")));
                }
                else
                {
                    MessageBox.Show("No Records To Display!");
                    this.lvwBillItems.Items.Clear();
                    this.gvAmount.Focus();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Display Data");
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void CleanDatabase()
        {
            SqlTransaction transaction = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
            this.SqlCmd.Transaction = transaction;
            this.StrSql = "DELETE FROM RES_CUSTORDERMASTER WHERE CUSTORDERDATE < '" + ParaDine.GlobalVariables.BusinessDate.AddDays(-2.0) + "'";
            this.SqlCmd.CommandText = this.StrSql;
            this.SqlCmd.Connection = ParaDine.GlobalVariables.SqlConn;
            this.SqlCmd.ExecuteNonQuery();
            transaction.Commit();
            transaction = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
            this.SqlCmd.Transaction = transaction;
            this.StrSql = "DELETE FROM GLB_MONEYMASTER WHERE TRANSACTIONDATE < '" + ParaDine.GlobalVariables.BusinessDate.AddDays(-2.0) + "'";
            this.SqlCmd.CommandText = this.StrSql;
            this.SqlCmd.Connection = ParaDine.GlobalVariables.SqlConn;
            this.SqlCmd.ExecuteNonQuery();
            transaction.Commit();
        }
        private void dtpBillFrDate_Validated(object sender, EventArgs e)
        {

        }

        private void dtpBillFrDate_Validating(object sender, CancelEventArgs e)
        {

        }

        private void dtpBillFrDate_ValueChanged(object sender, EventArgs e)
        {
            this.RefreshData();
        }

        private void frmBillDeletion_Load(object sender, EventArgs e)
        {
            this.uLevel = new Level(new User(ParaDine.GlobalVariables.UserID).LevelID);
            this.StrSql = " SELECT ALLOWREAD FROM GLB_SECURITY WHERE PERMISSIONID = (SELECT PERMISSIONID FROM GLB_PERMISSION WHERE PERMISSIONNAME = 'Bill &View')  AND LEVELID = " + this.uLevel.LevelID;
            if (!Convert.ToBoolean(ParaDine.GlobalFunctions.GetQueryValue(this.StrSql)))
            {
                this.BtnDelete.Enabled = false;
            }
            new GlobalTheme().applyTheme(this);
            this.RefreshData();
            this.dtpBillFrDate.Value = ParaDine.GlobalVariables.BusinessDate;
        }
        private void RefreshData()
        {
            this.StrSql = " SELECT   ISNULL(SUM(((COI.SALEPRICE * COI.SALEQTY - COI.SALEPRICE * COI.SALEQTY * COI.ITEMDISCPERC / 100)  + (COI.SALEPRICE * COI.SALEQTY - COI.SALEPRICE * COI.SALEQTY * COI.ITEMDISCPERC / 100) * COI.ITEMTAXPERC / 100)  + (COI.SALEPRICE * COI.SALEQTY - COI.SALEPRICE * COI.SALEQTY * COI.ITEMDISCPERC / 100) * COI.SERVICETAXPERC / 100),0)  AS NETAMOUNT FROM RES_CUSTORDERITEM COI  INNER JOIN RES_CUSTORDERKOT COK ON COK.CUSTORDERKOTID = COI.CUSTORDERKOTID  INNER JOIN RES_CUSTORDERMASTER COM ON COK.CUSTORDERMASTERID = COM.CUSTORDERMASTERID  WHERE CONVERT(VARCHAR, COM.CUSTORDERDATE, 112) = CONVERT(VARCHAR, CONVERT(DATETIME, '" + this.dtpBillFrDate.Text + "'), 112) AND DBO.RES_FUNC_GETORDERPAYMODES(COM.CUSTORDERMASTERID) IN ('CASH')";
            this.SqlCmd = new SqlCommand(this.StrSql, ParaDine.GlobalVariables.SqlConn);
            this.SqlCmd.CommandTimeout = 0x186a0;
            this.SDA.SelectCommand = this.SqlCmd;
            this.DS = new DataSet();
            this.SDA.Fill(this.DS, "CASH");
            if (this.DS.Tables["CASH"].Rows.Count > 0)
            {
                foreach (DataRow row in this.DS.Tables["CASH"].Rows)
                {
                    if (row["NETAMOUNT"].ToString() != "")
                    {
                        this.lblCash.Text = Convert.ToString(Convert.ToDouble(ParaDine.GlobalFunctions.GetColumnTotal(this.DS.Tables["CASH"], "NETAMOUNT")));
                    }
                }
            }
            else
            {
                this.lblCash.Text = "0.00";
            }
            this.StrSql = " SELECT  ISNULL( SUM(((COI.SALEPRICE * COI.SALEQTY - COI.SALEPRICE * COI.SALEQTY * COI.ITEMDISCPERC / 100)  + (COI.SALEPRICE * COI.SALEQTY - COI.SALEPRICE * COI.SALEQTY * COI.ITEMDISCPERC / 100) * COI.ITEMTAXPERC / 100)  + (COI.SALEPRICE * COI.SALEQTY - COI.SALEPRICE * COI.SALEQTY * COI.ITEMDISCPERC / 100) * COI.SERVICETAXPERC / 100),0)  AS NETAMOUNT FROM RES_CUSTORDERITEM COI  INNER JOIN RES_CUSTORDERKOT COK ON COK.CUSTORDERKOTID = COI.CUSTORDERKOTID  INNER JOIN RES_CUSTORDERMASTER COM ON COK.CUSTORDERMASTERID = COM.CUSTORDERMASTERID  WHERE CONVERT(VARCHAR, COM.CUSTORDERDATE, 112) = CONVERT(VARCHAR, CONVERT(DATETIME, '" + this.dtpBillFrDate.Text + "'), 112) AND DBO.RES_FUNC_GETORDERPAYMODES(COM.CUSTORDERMASTERID) IN ('CARD')";
            this.SqlCmd = new SqlCommand(this.StrSql, ParaDine.GlobalVariables.SqlConn);
            this.SqlCmd.CommandTimeout = 0x186a0;
            this.SDA.SelectCommand = this.SqlCmd;
            this.DS = new DataSet();
            this.SDA.Fill(this.DS, "CARD");
            if (this.DS.Tables["CARD"].Rows.Count > 0)
            {
                foreach (DataRow row in this.DS.Tables["CARD"].Rows)
                {
                    if (row["NETAMOUNT"] == null)
                    {
                        this.lblCard.Text = Convert.ToString(Convert.ToDouble(ParaDine.GlobalFunctions.GetColumnTotal(this.DS.Tables["CARD"], "NETAMOUNT")));
                    }
                }
            }
            else
            {
                this.lblCard.Text = "0.00";
            }
            this.StrSql = " SELECT  ISNULL( SUM(((COI.SALEPRICE * COI.SALEQTY - COI.SALEPRICE * COI.SALEQTY * COI.ITEMDISCPERC / 100)  + (COI.SALEPRICE * COI.SALEQTY - COI.SALEPRICE * COI.SALEQTY * COI.ITEMDISCPERC / 100) * COI.ITEMTAXPERC / 100)  + (COI.SALEPRICE * COI.SALEQTY - COI.SALEPRICE * COI.SALEQTY * COI.ITEMDISCPERC / 100) * COI.SERVICETAXPERC / 100),0)  AS NETAMOUNT FROM RES_CUSTORDERITEM COI  INNER JOIN RES_CUSTORDERKOT COK ON COK.CUSTORDERKOTID = COI.CUSTORDERKOTID  INNER JOIN RES_CUSTORDERMASTER COM ON COK.CUSTORDERMASTERID = COM.CUSTORDERMASTERID  WHERE CONVERT(VARCHAR, COM.CUSTORDERDATE, 112) = CONVERT(VARCHAR, CONVERT(DATETIME, '" + this.dtpBillFrDate.Text + "'), 112) AND DBO.RES_FUNC_GETORDERPAYMODES(COM.CUSTORDERMASTERID) NOT IN ('CASH','CARD')";
            this.SqlCmd = new SqlCommand(this.StrSql, ParaDine.GlobalVariables.SqlConn);
            this.SqlCmd.CommandTimeout = 0x186a0;
            this.SDA.SelectCommand = this.SqlCmd;
            this.DS = new DataSet();
            this.SDA.Fill(this.DS, "OTHER");
            if (this.DS.Tables["OTHER"].Rows.Count > 0)
            {
                foreach (DataRow row in this.DS.Tables["OTHER"].Rows)
                {
                    if (row["NETAMOUNT"] != null)
                    {
                        this.lblOthers.Text = Convert.ToString(Convert.ToDouble(ParaDine.GlobalFunctions.GetColumnTotal(this.DS.Tables["OTHER"], "NETAMOUNT")));
                    }
                }
            }
            else
            {
                this.lblOthers.Text = "0.00";
            }
            this.lblTotAmount.Text = Convert.ToString((double)((Convert.ToDouble(this.lblCash.Text) + Convert.ToDouble(this.lblCard.Text)) + Convert.ToDouble(this.lblOthers.Text)));
            this.lvwBillItems.Items.Clear();
        }

        private void SetColWidth()
        {
            this.lvwBillItems.Columns[0].Width = 100;
            this.lvwBillItems.Columns[1].Width = 130;
            this.lvwBillItems.Columns[2].Width = 100;
            this.lvwBillItems.Columns[3].Width = 110;
        }
    }
}
