﻿//using CrystalDecisions.Windows.Forms;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Main
{
    partial class FrmBillView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.BtnDisplay = new System.Windows.Forms.Button();
            this.btnReprint = new System.Windows.Forms.Button();
            this.BtnExit = new System.Windows.Forms.Button();
            this.BtnBillCancel = new System.Windows.Forms.Button();
            this.pnlTools = new System.Windows.Forms.Panel();
            this.chkReturnBills = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbTable = new System.Windows.Forms.ComboBox();
            this.cmbSteward = new System.Windows.Forms.ComboBox();
            this.cmbMachineNo = new System.Windows.Forms.ComboBox();
            this.cmbStockPoint = new System.Windows.Forms.ComboBox();
            this.dtpToOrderDate = new System.Windows.Forms.DateTimePicker();
            this.dtpFrOrderDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtOrderNoTo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtOrderNoFr = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.DgvBillView = new System.Windows.Forms.DataGridView();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.pnlMain = new Panel();
            this.BtnDisplay = new Button();
            this.btnReprint = new Button();
            this.BtnExit = new Button();
            this.BtnBillCancel = new Button();
            this.pnlTools = new Panel();
            this.chkReturnBills = new CheckBox();
            this.label8 = new Label();
            this.label7 = new Label();
            this.label6 = new Label();
            this.label5 = new Label();
            this.cmbTable = new ComboBox();
            this.cmbSteward = new ComboBox();
            this.cmbMachineNo = new ComboBox();
            this.cmbStockPoint = new ComboBox();
            this.dtpToOrderDate = new DateTimePicker();
            this.dtpFrOrderDate = new DateTimePicker();
            this.label4 = new Label();
            this.label3 = new Label();
            this.txtOrderNoTo = new TextBox();
            this.label2 = new Label();
            this.txtOrderNoFr = new TextBox();
            this.label1 = new Label();
            //this.CerRptMain = new CrystalReportViewer();
            this.DgvBillView = new DataGridView();
            this.dateTimePicker1 = new DateTimePicker();
            this.pnlMain.SuspendLayout();
            this.pnlTools.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvBillView)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlMain.Controls.Add(this.BtnDisplay);
            this.pnlMain.Controls.Add(this.btnReprint);
            this.pnlMain.Controls.Add(this.BtnExit);
            this.pnlMain.Controls.Add(this.BtnBillCancel);
            this.pnlMain.Controls.Add(this.pnlTools);
            //this.pnlMain.Controls.Add(this.CerRptMain);
            this.pnlMain.Controls.Add(this.DgvBillView);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1027, 670);
            this.pnlMain.TabIndex = 0;
            this.pnlMain.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlMain_Paint);
            // 
            // BtnDisplay
            // 
            this.BtnDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDisplay.Location = new System.Drawing.Point(269, 601);
            this.BtnDisplay.Name = "BtnDisplay";
            this.BtnDisplay.Size = new System.Drawing.Size(121, 25);
            this.BtnDisplay.TabIndex = 9;
            this.BtnDisplay.Text = "&Display";
            this.BtnDisplay.UseVisualStyleBackColor = true;
            this.BtnDisplay.Click += new System.EventHandler(this.BtnDisplay_Click);
            // 
            // btnReprint
            // 
            this.btnReprint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReprint.Location = new System.Drawing.Point(545, 599);
            this.btnReprint.Name = "btnReprint";
            this.btnReprint.Size = new System.Drawing.Size(139, 28);
            this.btnReprint.TabIndex = 19;
            this.btnReprint.Text = "Re&Print";
            this.btnReprint.UseVisualStyleBackColor = true;
            this.btnReprint.Click += new System.EventHandler(this.btnReprint_Click);
            // 
            // BtnExit
            // 
            this.BtnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExit.Location = new System.Drawing.Point(694, 599);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(139, 28);
            this.BtnExit.TabIndex = 18;
            this.BtnExit.Text = "E&xit";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // BtnBillCancel
            // 
            this.BtnBillCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBillCancel.Location = new System.Drawing.Point(396, 599);
            this.BtnBillCancel.Name = "BtnBillCancel";
            this.BtnBillCancel.Size = new System.Drawing.Size(139, 28);
            this.BtnBillCancel.TabIndex = 17;
            this.BtnBillCancel.Text = "Bill &Cancel";
            this.BtnBillCancel.UseVisualStyleBackColor = true;
            this.BtnBillCancel.Click += new System.EventHandler(this.BtnBillCancel_Click);
            // 
            // pnlTools
            // 
            this.pnlTools.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlTools.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTools.Controls.Add(this.chkReturnBills);
            this.pnlTools.Controls.Add(this.label8);
            this.pnlTools.Controls.Add(this.label7);
            this.pnlTools.Controls.Add(this.label6);
            this.pnlTools.Controls.Add(this.label5);
            this.pnlTools.Controls.Add(this.cmbTable);
            this.pnlTools.Controls.Add(this.cmbSteward);
            this.pnlTools.Controls.Add(this.cmbMachineNo);
            this.pnlTools.Controls.Add(this.cmbStockPoint);
            this.pnlTools.Controls.Add(this.dtpToOrderDate);
            this.pnlTools.Controls.Add(this.dtpFrOrderDate);
            this.pnlTools.Controls.Add(this.label4);
            this.pnlTools.Controls.Add(this.label3);
            this.pnlTools.Controls.Add(this.txtOrderNoTo);
            this.pnlTools.Controls.Add(this.label2);
            this.pnlTools.Controls.Add(this.txtOrderNoFr);
            this.pnlTools.Controls.Add(this.label1);
            this.pnlTools.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTools.Location = new System.Drawing.Point(0, 0);
            this.pnlTools.Name = "pnlTools";
            this.pnlTools.Size = new System.Drawing.Size(1027, 91);
            this.pnlTools.TabIndex = 2;
            // 
            // chkReturnBills
            // 
            this.chkReturnBills.AutoSize = true;
            this.chkReturnBills.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkReturnBills.Location = new System.Drawing.Point(929, 59);
            this.chkReturnBills.Name = "chkReturnBills";
            this.chkReturnBills.Size = new System.Drawing.Size(91, 17);
            this.chkReturnBills.TabIndex = 45;
            this.chkReturnBills.Text = "Return Bills";
            this.chkReturnBills.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(674, 41);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Table/Room :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(804, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "StewardName :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(410, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Machine No :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(544, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "StockPoint";
            // 
            // cmbTable
            // 
            this.cmbTable.FormattingEnabled = true;
            this.cmbTable.Location = new System.Drawing.Point(672, 57);
            this.cmbTable.Name = "cmbTable";
            this.cmbTable.Size = new System.Drawing.Size(121, 21);
            this.cmbTable.TabIndex = 7;
            // 
            // cmbSteward
            // 
            this.cmbSteward.FormattingEnabled = true;
            this.cmbSteward.Location = new System.Drawing.Point(803, 56);
            this.cmbSteward.Name = "cmbSteward";
            this.cmbSteward.Size = new System.Drawing.Size(121, 21);
            this.cmbSteward.TabIndex = 8;
            // 
            // cmbMachineNo
            // 
            this.cmbMachineNo.FormattingEnabled = true;
            this.cmbMachineNo.Location = new System.Drawing.Point(410, 57);
            this.cmbMachineNo.Name = "cmbMachineNo";
            this.cmbMachineNo.Size = new System.Drawing.Size(121, 21);
            this.cmbMachineNo.TabIndex = 5;
            // 
            // cmbStockPoint
            // 
            this.cmbStockPoint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStockPoint.FormattingEnabled = true;
            this.cmbStockPoint.Location = new System.Drawing.Point(541, 57);
            this.cmbStockPoint.Name = "cmbStockPoint";
            this.cmbStockPoint.Size = new System.Drawing.Size(121, 21);
            this.cmbStockPoint.TabIndex = 6;
            this.cmbStockPoint.SelectionChangeCommitted += new System.EventHandler(this.cmbRestaurant_SelectionChangeCommitted);
            this.cmbStockPoint.SelectedValueChanged += new System.EventHandler(this.cmbRestaurant_SelectedValueChanged);
            // 
            // dtpToOrderDate
            // 
            this.dtpToOrderDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpToOrderDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToOrderDate.Location = new System.Drawing.Point(295, 57);
            this.dtpToOrderDate.Name = "dtpToOrderDate";
            this.dtpToOrderDate.ShowCheckBox = true;
            this.dtpToOrderDate.Size = new System.Drawing.Size(105, 20);
            this.dtpToOrderDate.TabIndex = 4;
            // 
            // dtpFrOrderDate
            // 
            this.dtpFrOrderDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpFrOrderDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFrOrderDate.Location = new System.Drawing.Point(180, 57);
            this.dtpFrOrderDate.Name = "dtpFrOrderDate";
            this.dtpFrOrderDate.ShowCheckBox = true;
            this.dtpFrOrderDate.Size = new System.Drawing.Size(105, 20);
            this.dtpFrOrderDate.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(298, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Order Date To :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(184, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Order Date From :";
            // 
            // txtOrderNoTo
            // 
            this.txtOrderNoTo.Location = new System.Drawing.Point(92, 57);
            this.txtOrderNoTo.Name = "txtOrderNoTo";
            this.txtOrderNoTo.Size = new System.Drawing.Size(78, 20);
            this.txtOrderNoTo.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(96, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Order No :";
            // 
            // txtOrderNoFr
            // 
            this.txtOrderNoFr.Location = new System.Drawing.Point(4, 57);
            this.txtOrderNoFr.Name = "txtOrderNoFr";
            this.txtOrderNoFr.Size = new System.Drawing.Size(78, 20);
            this.txtOrderNoFr.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "OrderNo:";
            // 
            // DgvBillView
            // 
            //this.CerRptMain.ActiveViewIndex = -1;
            //this.CerRptMain.BorderStyle = BorderStyle.FixedSingle;
            //// this.CerRptMain.DisplayGroupTree = false;
            //this.CerRptMain.Location = new Point(670, 0x60);
            //this.CerRptMain.Name = "CerRptMain";
            //this.CerRptMain.SelectionFormula = "";
            //this.CerRptMain.Size = new Size(0x160, 0x1e9);
            //this.CerRptMain.TabIndex = 1;
            //this.CerRptMain.ViewTimeSelectionFormula = "";
            this.DgvBillView.AllowUserToAddRows = false;
            this.DgvBillView.AllowUserToDeleteRows = false;
            this.DgvBillView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvBillView.Location = new System.Drawing.Point(5, 96);
            this.DgvBillView.Name = "DgvBillView";
            this.DgvBillView.ReadOnly = true;
            this.DgvBillView.RowHeadersVisible = false;
            this.DgvBillView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvBillView.Size = new System.Drawing.Size(664, 489);
            this.DgvBillView.TabIndex = 0;
            this.DgvBillView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvBillView_CellDoubleClick);
            this.DgvBillView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DgvBillView_KeyDown);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(0, 0);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 0;
            // 
            // FrmBillView
            // 
            this.ClientSize = new System.Drawing.Size(1027, 670);
            this.Controls.Add(this.pnlMain);
            this.Name = "FrmBillView";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bill View";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmBillView_Load);
            this.pnlMain.ResumeLayout(false);
            this.pnlTools.ResumeLayout(false);
            this.pnlTools.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvBillView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
    }
}