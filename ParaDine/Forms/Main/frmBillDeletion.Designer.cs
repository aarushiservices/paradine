﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Main
{
    partial class frmBillDeletion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ImgLst = new System.Windows.Forms.ImageList(this.components);
            this.lblTotAmount = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.lblOthers = new System.Windows.Forms.Label();
            this.Label11 = new System.Windows.Forms.Label();
            this.lblCard = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.lblCash = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.lblPbar = new System.Windows.Forms.Label();
            this.Pbar = new System.Windows.Forms.ProgressBar();
            this.Label1 = new System.Windows.Forms.Label();
            this.gvTotalAmt = new ParaSysCom.NumControl();
            this.lvwBillItems = new System.Windows.Forms.ListView();
            this.pnlFilter = new System.Windows.Forms.Panel();
            this.dtpBillFrDate = new System.Windows.Forms.DateTimePicker();
            this.txtMNO = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.gvAmount = new ParaSysCom.NumControl();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.BtnDisplay = new System.Windows.Forms.Button();
            this.txtBillFrNo = new System.Windows.Forms.TextBox();
            this.txtBillToNo = new System.Windows.Forms.TextBox();
            this.lblNo = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.BtnExit = new System.Windows.Forms.Button();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.Panel1.SuspendLayout();
            this.pnlFilter.SuspendLayout();
            this.SuspendLayout();
            // 
            // ImgLst
            // 
            this.ImgLst.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.ImgLst.ImageSize = new System.Drawing.Size(16, 16);
            this.ImgLst.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // lblTotAmount
            // 
            this.lblTotAmount.AutoSize = true;
            this.lblTotAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotAmount.Location = new System.Drawing.Point(733, 15);
            this.lblTotAmount.Name = "lblTotAmount";
            this.lblTotAmount.Size = new System.Drawing.Size(32, 13);
            this.lblTotAmount.TabIndex = 38;
            this.lblTotAmount.Text = "0.00";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Location = new System.Drawing.Point(650, 15);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(76, 13);
            this.Label13.TabIndex = 37;
            this.Label13.Text = "Total Amount :";
            // 
            // lblOthers
            // 
            this.lblOthers.AutoSize = true;
            this.lblOthers.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOthers.Location = new System.Drawing.Point(454, 15);
            this.lblOthers.Name = "lblOthers";
            this.lblOthers.Size = new System.Drawing.Size(32, 13);
            this.lblOthers.TabIndex = 36;
            this.lblOthers.Text = "0.00";
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(403, 15);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(44, 13);
            this.Label11.TabIndex = 35;
            this.Label11.Text = "Others :";
            // 
            // lblCard
            // 
            this.lblCard.AutoSize = true;
            this.lblCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCard.Location = new System.Drawing.Point(286, 15);
            this.lblCard.Name = "lblCard";
            this.lblCard.Size = new System.Drawing.Size(32, 13);
            this.lblCard.TabIndex = 34;
            this.lblCard.Text = "0.00";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(209, 15);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(74, 13);
            this.Label9.TabIndex = 33;
            this.Label9.Text = "Card Amount :";
            // 
            // lblCash
            // 
            this.lblCash.AutoSize = true;
            this.lblCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCash.Location = new System.Drawing.Point(112, 15);
            this.lblCash.Name = "lblCash";
            this.lblCash.Size = new System.Drawing.Size(32, 13);
            this.lblCash.TabIndex = 32;
            this.lblCash.Text = "0.00";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(29, 15);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(76, 13);
            this.Label6.TabIndex = 31;
            this.Label6.Text = "Cash Amount :";
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel1.Controls.Add(this.lblPbar);
            this.Panel1.Controls.Add(this.Pbar);
            this.Panel1.Controls.Add(this.Label1);
            this.Panel1.Controls.Add(this.gvTotalAmt);
            this.Panel1.Controls.Add(this.lvwBillItems);
            this.Panel1.Controls.Add(this.pnlFilter);
            this.Panel1.Location = new System.Drawing.Point(8, 35);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(827, 404);
            this.Panel1.TabIndex = 39;
            // 
            // lblPbar
            // 
            this.lblPbar.AutoSize = true;
            this.lblPbar.Location = new System.Drawing.Point(34, 360);
            this.lblPbar.Name = "lblPbar";
            this.lblPbar.Size = new System.Drawing.Size(35, 13);
            this.lblPbar.TabIndex = 42;
            this.lblPbar.Text = "label4";
            this.lblPbar.Visible = false;
            // 
            // Pbar
            // 
            this.Pbar.Location = new System.Drawing.Point(20, 376);
            this.Pbar.Name = "Pbar";
            this.Pbar.Size = new System.Drawing.Size(566, 17);
            this.Pbar.TabIndex = 52;
            this.Pbar.Visible = false;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(592, 376);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(90, 13);
            this.Label1.TabIndex = 51;
            this.Label1.Text = "Total Amount :";
            // 
            // gvTotalAmt
            // 
            this.gvTotalAmt.DecimalRequired = true;
            this.gvTotalAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvTotalAmt.Location = new System.Drawing.Point(693, 370);
            this.gvTotalAmt.Name = "gvTotalAmt";
            this.gvTotalAmt.Size = new System.Drawing.Size(113, 20);
            this.gvTotalAmt.SymbolRequired = true;
            this.gvTotalAmt.TabIndex = 46;
            this.gvTotalAmt.Text = "`0.00";
            this.gvTotalAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvTotalAmt.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // lvwBillItems
            // 
            this.lvwBillItems.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.lvwBillItems.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.lvwBillItems.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lvwBillItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvwBillItems.ForeColor = System.Drawing.Color.Navy;
            this.lvwBillItems.HotTracking = true;
            this.lvwBillItems.HoverSelection = true;
            this.lvwBillItems.Location = new System.Drawing.Point(6, 77);
            this.lvwBillItems.Name = "lvwBillItems";
            this.lvwBillItems.Size = new System.Drawing.Size(812, 281);
            this.lvwBillItems.TabIndex = 45;
            this.lvwBillItems.TabStop = false;
            this.lvwBillItems.UseCompatibleStateImageBehavior = false;
            // 
            // pnlFilter
            // 
            this.pnlFilter.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFilter.Controls.Add(this.dtpBillFrDate);
            this.pnlFilter.Controls.Add(this.txtMNO);
            this.pnlFilter.Controls.Add(this.Label2);
            this.pnlFilter.Controls.Add(this.gvAmount);
            this.pnlFilter.Controls.Add(this.Label8);
            this.pnlFilter.Controls.Add(this.Label5);
            this.pnlFilter.Controls.Add(this.BtnDisplay);
            this.pnlFilter.Controls.Add(this.txtBillFrNo);
            this.pnlFilter.Controls.Add(this.txtBillToNo);
            this.pnlFilter.Controls.Add(this.lblNo);
            this.pnlFilter.Controls.Add(this.Label3);
            this.pnlFilter.Location = new System.Drawing.Point(6, 8);
            this.pnlFilter.Name = "pnlFilter";
            this.pnlFilter.Size = new System.Drawing.Size(811, 66);
            this.pnlFilter.TabIndex = 0;
            // 
            // dtpBillFrDate
            // 
            this.dtpBillFrDate.CalendarMonthBackground = System.Drawing.Color.Snow;
            this.dtpBillFrDate.CustomFormat = "dd/MMM/yy";
            this.dtpBillFrDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBillFrDate.Location = new System.Drawing.Point(368, 24);
            this.dtpBillFrDate.Name = "dtpBillFrDate";
            this.dtpBillFrDate.ShowCheckBox = true;
            this.dtpBillFrDate.Size = new System.Drawing.Size(108, 20);
            this.dtpBillFrDate.TabIndex = 3;
            this.dtpBillFrDate.ValueChanged += new System.EventHandler(this.dtpBillFrDate_ValueChanged);
            this.dtpBillFrDate.Validating += new System.ComponentModel.CancelEventHandler(this.dtpBillFrDate_Validating);
            this.dtpBillFrDate.Validated += new System.EventHandler(this.dtpBillFrDate_Validated);
            // 
            // txtMNO
            // 
            this.txtMNO.Location = new System.Drawing.Point(250, 24);
            this.txtMNO.Name = "txtMNO";
            this.txtMNO.Size = new System.Drawing.Size(85, 20);
            this.txtMNO.TabIndex = 2;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(250, 8);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(68, 13);
            this.Label2.TabIndex = 54;
            this.Label2.Text = "MachineNo :";
            // 
            // gvAmount
            // 
            this.gvAmount.DecimalRequired = true;
            this.gvAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvAmount.Location = new System.Drawing.Point(505, 23);
            this.gvAmount.Name = "gvAmount";
            this.gvAmount.Size = new System.Drawing.Size(90, 20);
            this.gvAmount.SymbolRequired = true;
            this.gvAmount.TabIndex = 4;
            this.gvAmount.Text = "`0.00";
            this.gvAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(505, 7);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(49, 13);
            this.Label8.TabIndex = 49;
            this.Label8.Text = "Amount :";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(130, 8);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(40, 13);
            this.Label5.TabIndex = 39;
            this.Label5.Text = "BillNo :";
            // 
            // BtnDisplay
            // 
            this.BtnDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDisplay.Location = new System.Drawing.Point(657, 14);
            this.BtnDisplay.Name = "BtnDisplay";
            this.BtnDisplay.Size = new System.Drawing.Size(91, 31);
            this.BtnDisplay.TabIndex = 5;
            this.BtnDisplay.Text = "&Display";
            this.BtnDisplay.UseVisualStyleBackColor = true;
            this.BtnDisplay.Click += new System.EventHandler(this.BtnDisplay_Click);
            // 
            // txtBillFrNo
            // 
            this.txtBillFrNo.Location = new System.Drawing.Point(13, 24);
            this.txtBillFrNo.Name = "txtBillFrNo";
            this.txtBillFrNo.Size = new System.Drawing.Size(78, 20);
            this.txtBillFrNo.TabIndex = 0;
            // 
            // txtBillToNo
            // 
            this.txtBillToNo.Location = new System.Drawing.Point(129, 24);
            this.txtBillToNo.Name = "txtBillToNo";
            this.txtBillToNo.Size = new System.Drawing.Size(78, 20);
            this.txtBillToNo.TabIndex = 1;
            // 
            // lblNo
            // 
            this.lblNo.AutoSize = true;
            this.lblNo.Location = new System.Drawing.Point(13, 8);
            this.lblNo.Name = "lblNo";
            this.lblNo.Size = new System.Drawing.Size(40, 13);
            this.lblNo.TabIndex = 28;
            this.lblNo.Text = "BillNo :";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(370, 8);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(55, 13);
            this.Label3.TabIndex = 32;
            this.Label3.Text = "Bill Date : ";
            // 
            // BtnExit
            // 
            this.BtnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExit.Location = new System.Drawing.Point(426, 456);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(96, 24);
            this.BtnExit.TabIndex = 41;
            this.BtnExit.Text = "E&xit";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Location = new System.Drawing.Point(321, 456);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(96, 24);
            this.BtnDelete.TabIndex = 40;
            this.BtnDelete.Text = "&Repair";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // frmBillDeletion
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(843, 494);
            this.Controls.Add(this.BtnExit);
            this.Controls.Add(this.BtnDelete);
            this.Controls.Add(this.Panel1);
            this.Controls.Add(this.lblTotAmount);
            this.Controls.Add(this.Label13);
            this.Controls.Add(this.lblOthers);
            this.Controls.Add(this.Label11);
            this.Controls.Add(this.lblCard);
            this.Controls.Add(this.Label9);
            this.Controls.Add(this.lblCash);
            this.Controls.Add(this.Label6);
            this.Name = "frmBillDeletion";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Repair Database :";
            this.Load += new System.EventHandler(this.frmBillDeletion_Load);
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            this.pnlFilter.ResumeLayout(false);
            this.pnlFilter.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
    }
}