﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.GlobalForms
{
    partial class frmChangePassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlChgPaswrd = new System.Windows.Forms.Panel();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnTrans = new System.Windows.Forms.Button();
            this.txtRetypepaswrd = new System.Windows.Forms.TextBox();
            this.txtNewPaswrd = new System.Windows.Forms.TextBox();
            this.txtOldPaswrd = new System.Windows.Forms.TextBox();
            this.cmbUser = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.pnlChgPaswrd.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlChgPaswrd
            // 
            this.pnlChgPaswrd.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlChgPaswrd.Controls.Add(this.btnExit);
            this.pnlChgPaswrd.Controls.Add(this.btnTrans);
            this.pnlChgPaswrd.Controls.Add(this.txtRetypepaswrd);
            this.pnlChgPaswrd.Controls.Add(this.txtNewPaswrd);
            this.pnlChgPaswrd.Controls.Add(this.txtOldPaswrd);
            this.pnlChgPaswrd.Controls.Add(this.cmbUser);
            this.pnlChgPaswrd.Controls.Add(this.label4);
            this.pnlChgPaswrd.Controls.Add(this.label3);
            this.pnlChgPaswrd.Controls.Add(this.label2);
            this.pnlChgPaswrd.Controls.Add(this.label1);
            this.pnlChgPaswrd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlChgPaswrd.Location = new System.Drawing.Point(0, 0);
            this.pnlChgPaswrd.Name = "pnlChgPaswrd";
            this.pnlChgPaswrd.Size = new System.Drawing.Size(233, 220);
            this.pnlChgPaswrd.TabIndex = 0;
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(129, 177);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 9;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(37, 177);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 8;
            this.btnTrans.Text = "Change";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // txtRetypepaswrd
            // 
            this.txtRetypepaswrd.BackColor = System.Drawing.Color.Ivory;
            this.txtRetypepaswrd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRetypepaswrd.Location = new System.Drawing.Point(101, 133);
            this.txtRetypepaswrd.Name = "txtRetypepaswrd";
            this.txtRetypepaswrd.Size = new System.Drawing.Size(121, 20);
            this.txtRetypepaswrd.TabIndex = 7;
            this.TTip.SetToolTip(this.txtRetypepaswrd, "Please Retype the New Password :");
            this.txtRetypepaswrd.UseSystemPasswordChar = true;
            // 
            // txtNewPaswrd
            // 
            this.txtNewPaswrd.BackColor = System.Drawing.Color.Ivory;
            this.txtNewPaswrd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNewPaswrd.Location = new System.Drawing.Point(101, 96);
            this.txtNewPaswrd.Name = "txtNewPaswrd";
            this.txtNewPaswrd.Size = new System.Drawing.Size(121, 20);
            this.txtNewPaswrd.TabIndex = 6;
            this.TTip.SetToolTip(this.txtNewPaswrd, "Please Enter the New Password");
            this.txtNewPaswrd.UseSystemPasswordChar = true;
            // 
            // txtOldPaswrd
            // 
            this.txtOldPaswrd.BackColor = System.Drawing.Color.Ivory;
            this.txtOldPaswrd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOldPaswrd.Location = new System.Drawing.Point(101, 59);
            this.txtOldPaswrd.Name = "txtOldPaswrd";
            this.txtOldPaswrd.Size = new System.Drawing.Size(121, 20);
            this.txtOldPaswrd.TabIndex = 5;
            this.TTip.SetToolTip(this.txtOldPaswrd, "Please enter the Old Password :");
            this.txtOldPaswrd.UseSystemPasswordChar = true;
            // 
            // cmbUser
            // 
            this.cmbUser.BackColor = System.Drawing.Color.Ivory;
            this.cmbUser.Enabled = false;
            this.cmbUser.FormattingEnabled = true;
            this.cmbUser.Location = new System.Drawing.Point(101, 21);
            this.cmbUser.Name = "cmbUser";
            this.cmbUser.Size = new System.Drawing.Size(121, 21);
            this.cmbUser.TabIndex = 4;
            this.TTip.SetToolTip(this.cmbUser, "Display User ;");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "New Password :";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(24, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 33);
            this.label3.TabIndex = 2;
            this.label3.Text = "Retype New PassWord :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Old Password :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(60, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "User :";
            // 
            // frmChangePassword
            // 
            this.ClientSize = new System.Drawing.Size(233, 220);
            this.Controls.Add(this.pnlChgPaswrd);
            this.Name = "frmChangePassword";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ChangePassword";
            this.Load += new System.EventHandler(this.frmChangePassword_Load);
            this.pnlChgPaswrd.ResumeLayout(false);
            this.pnlChgPaswrd.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
    }
}