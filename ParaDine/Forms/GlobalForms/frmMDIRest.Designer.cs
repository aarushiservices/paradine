﻿using ParaDine.Properties;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.GlobalForms
{
    partial class frmMDIRest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMDIRest));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.departmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.categoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.taxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uOMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockPointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.terminalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stewardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shiftToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockEntryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockReturnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockTransferToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consumedGoodsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockWriteOffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restaurantToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.BillingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.directBillToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BillViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BillCancelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deliveryOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableArrangementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billUpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billDeleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.counterTransToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierPaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerPaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shiftBeginToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shiftCloseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cardTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rechargeCardsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cardRechargeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cardReturnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orderSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kOTsSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.counterSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemSalesSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemListingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.delivertOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deliveryOrderSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deliveryOrderBillSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deliveryOrderItemsSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseOrderToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.printPOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pOBillsSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pOItemsSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockReOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockEntryToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.stockEntrySummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockEntryBillsSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockEntryItemsSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockReturnToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.stockReturnSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockReturnBillsSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockReturnItemsSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indentToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.indentSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indentBillsSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indentItemsSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockTransferToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.stockTransferBillSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockTransferItemSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockWriteOffToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.stockWriteOffSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockWriteOffBillsSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockWriteOffItemsSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockConsumeReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockConsumeItemSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockAdjustmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockAdjustmentSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sTOCKaDJUSTMENTToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.stockAdjItemSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productionToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.productionSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productionBillSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockInventoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outletStockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerLedgerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierLedgerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemSalesHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dayReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.systemSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockReportSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.themesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.noThemeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.homeSteadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.metalicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.brickToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backUpRestoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rEPRINTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dayEndToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registerProductToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.securityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.authorisationSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historySettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userAccountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userLevelsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hintsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changePasswToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.TSBussDate = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblAlert = new System.Windows.Forms.ToolStripStatusLabel();
            this.ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.btnCancelBill = new System.Windows.Forms.Button();
            this.ImgLstMain = new System.Windows.Forms.ImageList(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.LblBussDate = new System.Windows.Forms.Label();
            this.PnlNoTheme = new System.Windows.Forms.Panel();
            this.BtnEndOfDay = new System.Windows.Forms.Button();
            this.btn_Tables = new System.Windows.Forms.Button();
            this.btnOrders = new System.Windows.Forms.Button();
            this.btnCustOrder = new System.Windows.Forms.Button();
            this.btnDirectOrder = new System.Windows.Forms.Button();
            this.Label12 = new System.Windows.Forms.Label();
            this.BtnReservation = new System.Windows.Forms.Button();
            this.Label11 = new System.Windows.Forms.Label();
            this.btnShifts = new System.Windows.Forms.Button();
            this.Label10 = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.lblCustomerorder = new System.Windows.Forms.Label();
            this.BtnKitchen_Bar = new System.Windows.Forms.Button();
            this.btnSendMail = new System.Windows.Forms.Button();
            this.IMgLst = new System.Windows.Forms.ImageList(this.components);
            this.ImgLstAlert = new System.Windows.Forms.ImageList(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lbllefttime = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.menuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.panel1.SuspendLayout();
            this.PnlNoTheme.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuStrip.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configurationToolStripMenuItem,
            this.stockToolStripMenuItem,
            this.restaurantToolStripMenuItem1,
            this.cardToolStripMenuItem,
            this.reportsToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.toolsMenu,
            this.securityToolStripMenuItem,
            this.quitToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(908, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            this.menuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip_ItemClicked);
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.departmentToolStripMenuItem,
            this.categoryToolStripMenuItem,
            this.sectionToolStripMenuItem,
            this.taxToolStripMenuItem,
            this.uOMToolStripMenuItem,
            this.itemToolStripMenuItem,
            this.stockPointToolStripMenuItem,
            this.tableToolStripMenuItem,
            this.terminalToolStripMenuItem,
            this.paymentModeToolStripMenuItem,
            this.stewardToolStripMenuItem,
            this.supplierToolStripMenuItem,
            this.customerToolStripMenuItem,
            this.shiftToolStripMenuItem});
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.configurationToolStripMenuItem.Text = "&Configuration";
            // 
            // departmentToolStripMenuItem
            // 
            this.departmentToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.departmentToolStripMenuItem.Name = "departmentToolStripMenuItem";
            this.departmentToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.departmentToolStripMenuItem.Text = "Department";
            this.departmentToolStripMenuItem.Click += new System.EventHandler(this.departmentToolStripMenuItem_Click);
            // 
            // categoryToolStripMenuItem
            // 
            this.categoryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.categoryToolStripMenuItem.Name = "categoryToolStripMenuItem";
            this.categoryToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.categoryToolStripMenuItem.Text = "Category";
            this.categoryToolStripMenuItem.Click += new System.EventHandler(this.categoryToolStripMenuItem_Click);
            // 
            // sectionToolStripMenuItem
            // 
            this.sectionToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.sectionToolStripMenuItem.Name = "sectionToolStripMenuItem";
            this.sectionToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.sectionToolStripMenuItem.Text = "Section";
            this.sectionToolStripMenuItem.Click += new System.EventHandler(this.sectionToolStripMenuItem_Click);
            // 
            // taxToolStripMenuItem
            // 
            this.taxToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.taxToolStripMenuItem.Name = "taxToolStripMenuItem";
            this.taxToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.taxToolStripMenuItem.Text = "Tax";
            this.taxToolStripMenuItem.Click += new System.EventHandler(this.taxToolStripMenuItem_Click);
            // 
            // uOMToolStripMenuItem
            // 
            this.uOMToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.uOMToolStripMenuItem.Name = "uOMToolStripMenuItem";
            this.uOMToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.uOMToolStripMenuItem.Text = "UOM";
            this.uOMToolStripMenuItem.Click += new System.EventHandler(this.uOMToolStripMenuItem_Click);
            // 
            // itemToolStripMenuItem
            // 
            this.itemToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.itemToolStripMenuItem.Name = "itemToolStripMenuItem";
            this.itemToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.itemToolStripMenuItem.Text = "Item";
            this.itemToolStripMenuItem.Click += new System.EventHandler(this.itemToolStripMenuItem_Click);
            // 
            // stockPointToolStripMenuItem
            // 
            this.stockPointToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockPointToolStripMenuItem.Name = "stockPointToolStripMenuItem";
            this.stockPointToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.stockPointToolStripMenuItem.Text = "Stock Point";
            this.stockPointToolStripMenuItem.Click += new System.EventHandler(this.stockPointToolStripMenuItem_Click);
            // 
            // tableToolStripMenuItem
            // 
            this.tableToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.tableToolStripMenuItem.Name = "tableToolStripMenuItem";
            this.tableToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.tableToolStripMenuItem.Text = "Table";
            this.tableToolStripMenuItem.Click += new System.EventHandler(this.tableToolStripMenuItem_Click);
            // 
            // terminalToolStripMenuItem
            // 
            this.terminalToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.terminalToolStripMenuItem.Name = "terminalToolStripMenuItem";
            this.terminalToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.terminalToolStripMenuItem.Text = "Terminal";
            this.terminalToolStripMenuItem.Click += new System.EventHandler(this.terminalToolStripMenuItem_Click);
            // 
            // paymentModeToolStripMenuItem
            // 
            this.paymentModeToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.paymentModeToolStripMenuItem.Name = "paymentModeToolStripMenuItem";
            this.paymentModeToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.paymentModeToolStripMenuItem.Text = "Payment Mode";
            this.paymentModeToolStripMenuItem.Click += new System.EventHandler(this.paymentModeToolStripMenuItem_Click);
            // 
            // stewardToolStripMenuItem
            // 
            this.stewardToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stewardToolStripMenuItem.Name = "stewardToolStripMenuItem";
            this.stewardToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.stewardToolStripMenuItem.Text = "Steward";
            this.stewardToolStripMenuItem.Click += new System.EventHandler(this.stewardToolStripMenuItem_Click);
            // 
            // supplierToolStripMenuItem
            // 
            this.supplierToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.supplierToolStripMenuItem.Name = "supplierToolStripMenuItem";
            this.supplierToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.supplierToolStripMenuItem.Text = "Supplier";
            this.supplierToolStripMenuItem.Click += new System.EventHandler(this.supplierToolStripMenuItem_Click);
            // 
            // customerToolStripMenuItem
            // 
            this.customerToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.customerToolStripMenuItem.Name = "customerToolStripMenuItem";
            this.customerToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.customerToolStripMenuItem.Text = "Customer";
            this.customerToolStripMenuItem.Click += new System.EventHandler(this.customerToolStripMenuItem_Click);
            // 
            // shiftToolStripMenuItem
            // 
            this.shiftToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.shiftToolStripMenuItem.Name = "shiftToolStripMenuItem";
            this.shiftToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.shiftToolStripMenuItem.Text = "Shift";
            this.shiftToolStripMenuItem.Click += new System.EventHandler(this.shiftToolStripMenuItem_Click);
            // 
            // stockToolStripMenuItem
            // 
            this.stockToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.purchaseOrderToolStripMenuItem,
            this.stockEntryToolStripMenuItem,
            this.stockReturnToolStripMenuItem,
            this.stockTransferToolStripMenuItem,
            this.indentToolStripMenuItem,
            this.consumedGoodsToolStripMenuItem,
            this.productionToolStripMenuItem,
            this.stockWriteOffToolStripMenuItem});
            this.stockToolStripMenuItem.Name = "stockToolStripMenuItem";
            this.stockToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.stockToolStripMenuItem.Text = "Stock";
            // 
            // purchaseOrderToolStripMenuItem
            // 
            this.purchaseOrderToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.purchaseOrderToolStripMenuItem.Name = "purchaseOrderToolStripMenuItem";
            this.purchaseOrderToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.purchaseOrderToolStripMenuItem.Text = "Purchase Order";
            this.purchaseOrderToolStripMenuItem.Click += new System.EventHandler(this.purchaseOrderToolStripMenuItem_Click);
            // 
            // stockEntryToolStripMenuItem
            // 
            this.stockEntryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockEntryToolStripMenuItem.Name = "stockEntryToolStripMenuItem";
            this.stockEntryToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.stockEntryToolStripMenuItem.Text = "Stock Entry";
            this.stockEntryToolStripMenuItem.Click += new System.EventHandler(this.stockEntryToolStripMenuItem_Click);
            // 
            // stockReturnToolStripMenuItem
            // 
            this.stockReturnToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockReturnToolStripMenuItem.Name = "stockReturnToolStripMenuItem";
            this.stockReturnToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.stockReturnToolStripMenuItem.Text = "Stock Return";
            this.stockReturnToolStripMenuItem.Click += new System.EventHandler(this.stockReturnToolStripMenuItem_Click);
            // 
            // stockTransferToolStripMenuItem
            // 
            this.stockTransferToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockTransferToolStripMenuItem.Name = "stockTransferToolStripMenuItem";
            this.stockTransferToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.stockTransferToolStripMenuItem.Text = "Stock Transfer";
            this.stockTransferToolStripMenuItem.Click += new System.EventHandler(this.stockTransferToolStripMenuItem_Click);
            // 
            // indentToolStripMenuItem
            // 
            this.indentToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.indentToolStripMenuItem.Name = "indentToolStripMenuItem";
            this.indentToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.indentToolStripMenuItem.Text = "Indent";
            this.indentToolStripMenuItem.Click += new System.EventHandler(this.indentToolStripMenuItem_Click);
            // 
            // consumedGoodsToolStripMenuItem
            // 
            this.consumedGoodsToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.consumedGoodsToolStripMenuItem.Name = "consumedGoodsToolStripMenuItem";
            this.consumedGoodsToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.consumedGoodsToolStripMenuItem.Text = "Consumed Goods";
            this.consumedGoodsToolStripMenuItem.Click += new System.EventHandler(this.consumedGoodsToolStripMenuItem_Click);
            // 
            // productionToolStripMenuItem
            // 
            this.productionToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.productionToolStripMenuItem.Name = "productionToolStripMenuItem";
            this.productionToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.productionToolStripMenuItem.Text = "Production";
            this.productionToolStripMenuItem.Click += new System.EventHandler(this.productionToolStripMenuItem_Click);
            // 
            // stockWriteOffToolStripMenuItem
            // 
            this.stockWriteOffToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockWriteOffToolStripMenuItem.Name = "stockWriteOffToolStripMenuItem";
            this.stockWriteOffToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.stockWriteOffToolStripMenuItem.Text = "Stock Write Off";
            this.stockWriteOffToolStripMenuItem.Click += new System.EventHandler(this.stockWriteOffToolStripMenuItem_Click);
            // 
            // restaurantToolStripMenuItem1
            // 
            this.restaurantToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BillingToolStripMenuItem,
            this.directBillToolStripMenuItem,
            this.BillViewToolStripMenuItem,
            this.BillCancelToolStripMenuItem,
            this.deliveryOrderToolStripMenuItem,
            this.tableArrangementToolStripMenuItem,
            this.billUpdateToolStripMenuItem,
            this.billDeleteToolStripMenuItem,
            this.counterTransToolStripMenuItem,
            this.supplierPaymentToolStripMenuItem,
            this.customerPaymentToolStripMenuItem,
            this.shiftBeginToolStripMenuItem,
            this.shiftCloseToolStripMenuItem});
            this.restaurantToolStripMenuItem1.Name = "restaurantToolStripMenuItem1";
            this.restaurantToolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.restaurantToolStripMenuItem1.Size = new System.Drawing.Size(83, 20);
            this.restaurantToolStripMenuItem1.Text = "&Restaurant";
            // 
            // BillingToolStripMenuItem
            // 
            this.BillingToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.BillingToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.BillingToolStripMenuItem.Name = "BillingToolStripMenuItem";
            this.BillingToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.BillingToolStripMenuItem.Text = "&KOT Bill";
            this.BillingToolStripMenuItem.Click += new System.EventHandler(this.BillingToolStripMenuItem_Click);
            // 
            // directBillToolStripMenuItem
            // 
            this.directBillToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.directBillToolStripMenuItem.Name = "directBillToolStripMenuItem";
            this.directBillToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.directBillToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.directBillToolStripMenuItem.Text = "Direct Bill";
            this.directBillToolStripMenuItem.Click += new System.EventHandler(this.directBillToolStripMenuItem_Click);
            // 
            // BillViewToolStripMenuItem
            // 
            this.BillViewToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.BillViewToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.BillViewToolStripMenuItem.Name = "BillViewToolStripMenuItem";
            this.BillViewToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.BillViewToolStripMenuItem.Text = "Bill &View";
            this.BillViewToolStripMenuItem.Click += new System.EventHandler(this.BillViewToolStripMenuItem_Click);
            // 
            // BillCancelToolStripMenuItem
            // 
            this.BillCancelToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.BillCancelToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.BillCancelToolStripMenuItem.Name = "BillCancelToolStripMenuItem";
            this.BillCancelToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.BillCancelToolStripMenuItem.Text = "Bill &Cancel";
            this.BillCancelToolStripMenuItem.Visible = false;
            this.BillCancelToolStripMenuItem.Click += new System.EventHandler(this.BillCancelToolStripMenuItem_Click);
            // 
            // deliveryOrderToolStripMenuItem
            // 
            this.deliveryOrderToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.deliveryOrderToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.deliveryOrderToolStripMenuItem.Name = "deliveryOrderToolStripMenuItem";
            this.deliveryOrderToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.deliveryOrderToolStripMenuItem.Text = "Delivery Order";
            this.deliveryOrderToolStripMenuItem.Click += new System.EventHandler(this.deliveryOrderToolStripMenuItem_Click);
            // 
            // tableArrangementToolStripMenuItem
            // 
            this.tableArrangementToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.tableArrangementToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.tableArrangementToolStripMenuItem.Name = "tableArrangementToolStripMenuItem";
            this.tableArrangementToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.tableArrangementToolStripMenuItem.Text = "&Table Arrangement";
            this.tableArrangementToolStripMenuItem.Click += new System.EventHandler(this.tableArrangementToolStripMenuItem_Click);
            // 
            // billUpdateToolStripMenuItem
            // 
            this.billUpdateToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.billUpdateToolStripMenuItem.Name = "billUpdateToolStripMenuItem";
            this.billUpdateToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.billUpdateToolStripMenuItem.Text = "Bill Update";
            this.billUpdateToolStripMenuItem.Click += new System.EventHandler(this.billUpdateToolStripMenuItem_Click);
            // 
            // billDeleteToolStripMenuItem
            // 
            this.billDeleteToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.billDeleteToolStripMenuItem.Name = "billDeleteToolStripMenuItem";
            this.billDeleteToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.billDeleteToolStripMenuItem.Text = "Del Check";
            this.billDeleteToolStripMenuItem.Click += new System.EventHandler(this.billDeleteToolStripMenuItem_Click);
            // 
            // counterTransToolStripMenuItem
            // 
            this.counterTransToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.counterTransToolStripMenuItem.Name = "counterTransToolStripMenuItem";
            this.counterTransToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.counterTransToolStripMenuItem.Text = "Counter Trans";
            this.counterTransToolStripMenuItem.Click += new System.EventHandler(this.counterTransToolStripMenuItem_Click);
            // 
            // supplierPaymentToolStripMenuItem
            // 
            this.supplierPaymentToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.supplierPaymentToolStripMenuItem.Name = "supplierPaymentToolStripMenuItem";
            this.supplierPaymentToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.supplierPaymentToolStripMenuItem.Text = "Supplier Payment";
            this.supplierPaymentToolStripMenuItem.Click += new System.EventHandler(this.supplierPaymentToolStripMenuItem_Click);
            // 
            // customerPaymentToolStripMenuItem
            // 
            this.customerPaymentToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.customerPaymentToolStripMenuItem.Name = "customerPaymentToolStripMenuItem";
            this.customerPaymentToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.customerPaymentToolStripMenuItem.Text = "Customer Payment";
            this.customerPaymentToolStripMenuItem.Click += new System.EventHandler(this.customerPaymentToolStripMenuItem_Click);
            // 
            // shiftBeginToolStripMenuItem
            // 
            this.shiftBeginToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.shiftBeginToolStripMenuItem.Name = "shiftBeginToolStripMenuItem";
            this.shiftBeginToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.shiftBeginToolStripMenuItem.Text = "Shift Begin";
            this.shiftBeginToolStripMenuItem.Click += new System.EventHandler(this.shiftBeginToolStripMenuItem_Click);
            // 
            // shiftCloseToolStripMenuItem
            // 
            this.shiftCloseToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.shiftCloseToolStripMenuItem.Name = "shiftCloseToolStripMenuItem";
            this.shiftCloseToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.shiftCloseToolStripMenuItem.Text = "Shift Close";
            // 
            // cardToolStripMenuItem
            // 
            this.cardToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cardTypeToolStripMenuItem,
            this.rechargeCardsToolStripMenuItem,
            this.cToolStripMenuItem,
            this.cardRechargeToolStripMenuItem,
            this.cardReturnToolStripMenuItem});
            this.cardToolStripMenuItem.Name = "cardToolStripMenuItem";
            this.cardToolStripMenuItem.Size = new System.Drawing.Size(102, 20);
            this.cardToolStripMenuItem.Text = "Recharge Card";
            // 
            // cardTypeToolStripMenuItem
            // 
            this.cardTypeToolStripMenuItem.Name = "cardTypeToolStripMenuItem";
            this.cardTypeToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.cardTypeToolStripMenuItem.Text = "Card Type";
            this.cardTypeToolStripMenuItem.Click += new System.EventHandler(this.cardTypeToolStripMenuItem_Click);
            // 
            // rechargeCardsToolStripMenuItem
            // 
            this.rechargeCardsToolStripMenuItem.Name = "rechargeCardsToolStripMenuItem";
            this.rechargeCardsToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.rechargeCardsToolStripMenuItem.Text = "Recharge Cards";
            this.rechargeCardsToolStripMenuItem.Click += new System.EventHandler(this.rechargeCardsToolStripMenuItem_Click);
            // 
            // cToolStripMenuItem
            // 
            this.cToolStripMenuItem.Name = "cToolStripMenuItem";
            this.cToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.cToolStripMenuItem.Text = "Card Issue";
            this.cToolStripMenuItem.Click += new System.EventHandler(this.cToolStripMenuItem_Click);
            // 
            // cardRechargeToolStripMenuItem
            // 
            this.cardRechargeToolStripMenuItem.Name = "cardRechargeToolStripMenuItem";
            this.cardRechargeToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.cardRechargeToolStripMenuItem.Text = "Card Recharge";
            this.cardRechargeToolStripMenuItem.Click += new System.EventHandler(this.cardRechargeToolStripMenuItem_Click);
            // 
            // cardReturnToolStripMenuItem
            // 
            this.cardReturnToolStripMenuItem.Name = "cardReturnToolStripMenuItem";
            this.cardReturnToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.cardReturnToolStripMenuItem.Text = "Card Return";
            this.cardReturnToolStripMenuItem.Click += new System.EventHandler(this.cardReturnToolStripMenuItem_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.orderSummaryToolStripMenuItem,
            this.kOTsSummaryToolStripMenuItem,
            this.counterSummaryToolStripMenuItem,
            this.itemSalesSummaryToolStripMenuItem,
            this.itemListingToolStripMenuItem,
            this.delivertOrderToolStripMenuItem,
            this.purchaseOrderToolStripMenuItem1,
            this.stockEntryToolStripMenuItem1,
            this.stockReturnToolStripMenuItem1,
            this.indentToolStripMenuItem1,
            this.stockTransferToolStripMenuItem1,
            this.stockWriteOffToolStripMenuItem1,
            this.stockConsumeReportsToolStripMenuItem,
            this.stockAdjustmentToolStripMenuItem,
            this.productionToolStripMenuItem1,
            this.stockInventoryToolStripMenuItem,
            this.customerLedgerToolStripMenuItem,
            this.supplierLedgerToolStripMenuItem,
            this.logHistoryToolStripMenuItem,
            this.itemSalesHistoryToolStripMenuItem,
            this.dayReportToolStripMenuItem});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.reportsToolStripMenuItem.Text = "Reports";
            // 
            // orderSummaryToolStripMenuItem
            // 
            this.orderSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.orderSummaryToolStripMenuItem.Name = "orderSummaryToolStripMenuItem";
            this.orderSummaryToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.orderSummaryToolStripMenuItem.Text = "Order Summary";
            this.orderSummaryToolStripMenuItem.Click += new System.EventHandler(this.orderSummaryToolStripMenuItem_Click);
            // 
            // kOTsSummaryToolStripMenuItem
            // 
            this.kOTsSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.kOTsSummaryToolStripMenuItem.Name = "kOTsSummaryToolStripMenuItem";
            this.kOTsSummaryToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.kOTsSummaryToolStripMenuItem.Text = "KOTs Summary";
            this.kOTsSummaryToolStripMenuItem.Click += new System.EventHandler(this.kOTsSummaryToolStripMenuItem_Click);
            // 
            // counterSummaryToolStripMenuItem
            // 
            this.counterSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.counterSummaryToolStripMenuItem.Name = "counterSummaryToolStripMenuItem";
            this.counterSummaryToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.counterSummaryToolStripMenuItem.Text = "Counter Summary";
            this.counterSummaryToolStripMenuItem.Click += new System.EventHandler(this.counterSummaryToolStripMenuItem_Click);
            // 
            // itemSalesSummaryToolStripMenuItem
            // 
            this.itemSalesSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.itemSalesSummaryToolStripMenuItem.Name = "itemSalesSummaryToolStripMenuItem";
            this.itemSalesSummaryToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.itemSalesSummaryToolStripMenuItem.Text = "Item Sales Summary";
            this.itemSalesSummaryToolStripMenuItem.Click += new System.EventHandler(this.itemSalesSummaryToolStripMenuItem_Click);
            // 
            // itemListingToolStripMenuItem
            // 
            this.itemListingToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.itemListingToolStripMenuItem.Name = "itemListingToolStripMenuItem";
            this.itemListingToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.itemListingToolStripMenuItem.Text = "Item Listing";
            this.itemListingToolStripMenuItem.Click += new System.EventHandler(this.itemListingToolStripMenuItem_Click);
            // 
            // delivertOrderToolStripMenuItem
            // 
            this.delivertOrderToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.delivertOrderToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deliveryOrderSummaryToolStripMenuItem,
            this.deliveryOrderBillSummaryToolStripMenuItem,
            this.deliveryOrderItemsSummaryToolStripMenuItem});
            this.delivertOrderToolStripMenuItem.Name = "delivertOrderToolStripMenuItem";
            this.delivertOrderToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.delivertOrderToolStripMenuItem.Text = "&Delivery Order Reports";
            // 
            // deliveryOrderSummaryToolStripMenuItem
            // 
            this.deliveryOrderSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.deliveryOrderSummaryToolStripMenuItem.Name = "deliveryOrderSummaryToolStripMenuItem";
            this.deliveryOrderSummaryToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.deliveryOrderSummaryToolStripMenuItem.Text = "Delivery Order Summary";
            this.deliveryOrderSummaryToolStripMenuItem.Click += new System.EventHandler(this.deliveryOrderSummaryToolStripMenuItem_Click);
            // 
            // deliveryOrderBillSummaryToolStripMenuItem
            // 
            this.deliveryOrderBillSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.deliveryOrderBillSummaryToolStripMenuItem.Name = "deliveryOrderBillSummaryToolStripMenuItem";
            this.deliveryOrderBillSummaryToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.deliveryOrderBillSummaryToolStripMenuItem.Text = "Delivery Order Bill Summary";
            this.deliveryOrderBillSummaryToolStripMenuItem.Click += new System.EventHandler(this.pendingDeliveryOrderToolStripMenuItem_Click);
            // 
            // deliveryOrderItemsSummaryToolStripMenuItem
            // 
            this.deliveryOrderItemsSummaryToolStripMenuItem.Name = "deliveryOrderItemsSummaryToolStripMenuItem";
            this.deliveryOrderItemsSummaryToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.deliveryOrderItemsSummaryToolStripMenuItem.Text = "Delivery Order Items Summary";
            this.deliveryOrderItemsSummaryToolStripMenuItem.Click += new System.EventHandler(this.deliveryOrderItemsSummaryToolStripMenuItem_Click);
            // 
            // purchaseOrderToolStripMenuItem1
            // 
            this.purchaseOrderToolStripMenuItem1.BackColor = System.Drawing.Color.White;
            this.purchaseOrderToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printPOToolStripMenuItem,
            this.pOBillsSummaryToolStripMenuItem,
            this.pOItemsSummaryToolStripMenuItem,
            this.stockReOrderToolStripMenuItem});
            this.purchaseOrderToolStripMenuItem1.Name = "purchaseOrderToolStripMenuItem1";
            this.purchaseOrderToolStripMenuItem1.Size = new System.Drawing.Size(224, 22);
            this.purchaseOrderToolStripMenuItem1.Text = "Purchase Order Reports";
            this.purchaseOrderToolStripMenuItem1.Visible = false;
            // 
            // printPOToolStripMenuItem
            // 
            this.printPOToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.printPOToolStripMenuItem.Name = "printPOToolStripMenuItem";
            this.printPOToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.printPOToolStripMenuItem.Text = "PO Summary";
            this.printPOToolStripMenuItem.Click += new System.EventHandler(this.printPOToolStripMenuItem_Click);
            // 
            // pOBillsSummaryToolStripMenuItem
            // 
            this.pOBillsSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.pOBillsSummaryToolStripMenuItem.Name = "pOBillsSummaryToolStripMenuItem";
            this.pOBillsSummaryToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.pOBillsSummaryToolStripMenuItem.Text = "PO Bills Summary";
            this.pOBillsSummaryToolStripMenuItem.Click += new System.EventHandler(this.pOBillsSummaryToolStripMenuItem_Click);
            // 
            // pOItemsSummaryToolStripMenuItem
            // 
            this.pOItemsSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.pOItemsSummaryToolStripMenuItem.Name = "pOItemsSummaryToolStripMenuItem";
            this.pOItemsSummaryToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.pOItemsSummaryToolStripMenuItem.Text = "PO Items Summary";
            this.pOItemsSummaryToolStripMenuItem.Click += new System.EventHandler(this.pOItemsSummaryToolStripMenuItem_Click);
            // 
            // stockReOrderToolStripMenuItem
            // 
            this.stockReOrderToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockReOrderToolStripMenuItem.Name = "stockReOrderToolStripMenuItem";
            this.stockReOrderToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.stockReOrderToolStripMenuItem.Text = "Stock ReOrder";
            this.stockReOrderToolStripMenuItem.Click += new System.EventHandler(this.stockReOrderToolStripMenuItem_Click_1);
            // 
            // stockEntryToolStripMenuItem1
            // 
            this.stockEntryToolStripMenuItem1.BackColor = System.Drawing.Color.White;
            this.stockEntryToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stockEntrySummaryToolStripMenuItem,
            this.stockEntryBillsSummaryToolStripMenuItem,
            this.stockEntryItemsSummaryToolStripMenuItem});
            this.stockEntryToolStripMenuItem1.Name = "stockEntryToolStripMenuItem1";
            this.stockEntryToolStripMenuItem1.Size = new System.Drawing.Size(224, 22);
            this.stockEntryToolStripMenuItem1.Text = "Stock Entry Reports";
            this.stockEntryToolStripMenuItem1.Click += new System.EventHandler(this.stockEntryToolStripMenuItem1_Click);
            // 
            // stockEntrySummaryToolStripMenuItem
            // 
            this.stockEntrySummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockEntrySummaryToolStripMenuItem.Name = "stockEntrySummaryToolStripMenuItem";
            this.stockEntrySummaryToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.stockEntrySummaryToolStripMenuItem.Text = "Stock Entry Summary";
            this.stockEntrySummaryToolStripMenuItem.Click += new System.EventHandler(this.stockEntrySummaryToolStripMenuItem_Click);
            // 
            // stockEntryBillsSummaryToolStripMenuItem
            // 
            this.stockEntryBillsSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockEntryBillsSummaryToolStripMenuItem.Name = "stockEntryBillsSummaryToolStripMenuItem";
            this.stockEntryBillsSummaryToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.stockEntryBillsSummaryToolStripMenuItem.Text = "Stock Entry Bills Summary";
            this.stockEntryBillsSummaryToolStripMenuItem.Click += new System.EventHandler(this.stockEntryBillsSummaryToolStripMenuItem_Click);
            // 
            // stockEntryItemsSummaryToolStripMenuItem
            // 
            this.stockEntryItemsSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockEntryItemsSummaryToolStripMenuItem.Name = "stockEntryItemsSummaryToolStripMenuItem";
            this.stockEntryItemsSummaryToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.stockEntryItemsSummaryToolStripMenuItem.Text = "Stock Entry Items Summary";
            this.stockEntryItemsSummaryToolStripMenuItem.Click += new System.EventHandler(this.stockEntryItemsSummaryToolStripMenuItem_Click);
            // 
            // stockReturnToolStripMenuItem1
            // 
            this.stockReturnToolStripMenuItem1.BackColor = System.Drawing.Color.White;
            this.stockReturnToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stockReturnSummaryToolStripMenuItem,
            this.stockReturnBillsSummaryToolStripMenuItem,
            this.stockReturnItemsSummaryToolStripMenuItem});
            this.stockReturnToolStripMenuItem1.Name = "stockReturnToolStripMenuItem1";
            this.stockReturnToolStripMenuItem1.Size = new System.Drawing.Size(224, 22);
            this.stockReturnToolStripMenuItem1.Text = "Stock Return Reports";
            // 
            // stockReturnSummaryToolStripMenuItem
            // 
            this.stockReturnSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockReturnSummaryToolStripMenuItem.Name = "stockReturnSummaryToolStripMenuItem";
            this.stockReturnSummaryToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.stockReturnSummaryToolStripMenuItem.Text = "Stock Return Summary";
            this.stockReturnSummaryToolStripMenuItem.Click += new System.EventHandler(this.stockReturnSummaryToolStripMenuItem_Click);
            // 
            // stockReturnBillsSummaryToolStripMenuItem
            // 
            this.stockReturnBillsSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockReturnBillsSummaryToolStripMenuItem.Name = "stockReturnBillsSummaryToolStripMenuItem";
            this.stockReturnBillsSummaryToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.stockReturnBillsSummaryToolStripMenuItem.Text = "Stock Return Bills Summary";
            this.stockReturnBillsSummaryToolStripMenuItem.Click += new System.EventHandler(this.stockReturnBillsSummaryToolStripMenuItem_Click);
            // 
            // stockReturnItemsSummaryToolStripMenuItem
            // 
            this.stockReturnItemsSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockReturnItemsSummaryToolStripMenuItem.Name = "stockReturnItemsSummaryToolStripMenuItem";
            this.stockReturnItemsSummaryToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.stockReturnItemsSummaryToolStripMenuItem.Text = "Stock Return Items Summary";
            this.stockReturnItemsSummaryToolStripMenuItem.Click += new System.EventHandler(this.stockReturnItemsSummaryToolStripMenuItem_Click);
            // 
            // indentToolStripMenuItem1
            // 
            this.indentToolStripMenuItem1.BackColor = System.Drawing.Color.White;
            this.indentToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.indentSummaryToolStripMenuItem,
            this.indentBillsSummaryToolStripMenuItem,
            this.indentItemsSummaryToolStripMenuItem});
            this.indentToolStripMenuItem1.Name = "indentToolStripMenuItem1";
            this.indentToolStripMenuItem1.Size = new System.Drawing.Size(224, 22);
            this.indentToolStripMenuItem1.Text = "Indent Reports";
            // 
            // indentSummaryToolStripMenuItem
            // 
            this.indentSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.indentSummaryToolStripMenuItem.Name = "indentSummaryToolStripMenuItem";
            this.indentSummaryToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.indentSummaryToolStripMenuItem.Text = "Indent Summary";
            this.indentSummaryToolStripMenuItem.Click += new System.EventHandler(this.indentSummaryToolStripMenuItem_Click);
            // 
            // indentBillsSummaryToolStripMenuItem
            // 
            this.indentBillsSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.indentBillsSummaryToolStripMenuItem.Name = "indentBillsSummaryToolStripMenuItem";
            this.indentBillsSummaryToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.indentBillsSummaryToolStripMenuItem.Text = "Indent Bills Summary";
            this.indentBillsSummaryToolStripMenuItem.Click += new System.EventHandler(this.indentBillsSummaryToolStripMenuItem_Click);
            // 
            // indentItemsSummaryToolStripMenuItem
            // 
            this.indentItemsSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.indentItemsSummaryToolStripMenuItem.Name = "indentItemsSummaryToolStripMenuItem";
            this.indentItemsSummaryToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.indentItemsSummaryToolStripMenuItem.Text = "Indent Items Summary";
            this.indentItemsSummaryToolStripMenuItem.Click += new System.EventHandler(this.indentItemsSummaryToolStripMenuItem_Click);
            // 
            // stockTransferToolStripMenuItem1
            // 
            this.stockTransferToolStripMenuItem1.BackColor = System.Drawing.Color.White;
            this.stockTransferToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stockTransferBillSummaryToolStripMenuItem,
            this.stockTransferItemSummaryToolStripMenuItem});
            this.stockTransferToolStripMenuItem1.Name = "stockTransferToolStripMenuItem1";
            this.stockTransferToolStripMenuItem1.Size = new System.Drawing.Size(224, 22);
            this.stockTransferToolStripMenuItem1.Text = "Stock Transfer Reports";
            // 
            // stockTransferBillSummaryToolStripMenuItem
            // 
            this.stockTransferBillSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockTransferBillSummaryToolStripMenuItem.Name = "stockTransferBillSummaryToolStripMenuItem";
            this.stockTransferBillSummaryToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.stockTransferBillSummaryToolStripMenuItem.Text = "Stock Transfer Bill Summary";
            this.stockTransferBillSummaryToolStripMenuItem.Click += new System.EventHandler(this.stockTransferBillSummaryToolStripMenuItem_Click);
            // 
            // stockTransferItemSummaryToolStripMenuItem
            // 
            this.stockTransferItemSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockTransferItemSummaryToolStripMenuItem.Name = "stockTransferItemSummaryToolStripMenuItem";
            this.stockTransferItemSummaryToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.stockTransferItemSummaryToolStripMenuItem.Text = "Stock Transfer Item Summary";
            this.stockTransferItemSummaryToolStripMenuItem.Click += new System.EventHandler(this.stockTransferItemSummaryToolStripMenuItem_Click);
            // 
            // stockWriteOffToolStripMenuItem1
            // 
            this.stockWriteOffToolStripMenuItem1.BackColor = System.Drawing.Color.White;
            this.stockWriteOffToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stockWriteOffSummaryToolStripMenuItem,
            this.stockWriteOffBillsSummaryToolStripMenuItem,
            this.stockWriteOffItemsSummaryToolStripMenuItem});
            this.stockWriteOffToolStripMenuItem1.Name = "stockWriteOffToolStripMenuItem1";
            this.stockWriteOffToolStripMenuItem1.Size = new System.Drawing.Size(224, 22);
            this.stockWriteOffToolStripMenuItem1.Text = "Stock Write Off Reports";
            this.stockWriteOffToolStripMenuItem1.Visible = false;
            // 
            // stockWriteOffSummaryToolStripMenuItem
            // 
            this.stockWriteOffSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockWriteOffSummaryToolStripMenuItem.Name = "stockWriteOffSummaryToolStripMenuItem";
            this.stockWriteOffSummaryToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.stockWriteOffSummaryToolStripMenuItem.Text = "Stock Write Off Summary";
            this.stockWriteOffSummaryToolStripMenuItem.Click += new System.EventHandler(this.stockWriteOffSummaryToolStripMenuItem_Click);
            // 
            // stockWriteOffBillsSummaryToolStripMenuItem
            // 
            this.stockWriteOffBillsSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockWriteOffBillsSummaryToolStripMenuItem.Name = "stockWriteOffBillsSummaryToolStripMenuItem";
            this.stockWriteOffBillsSummaryToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.stockWriteOffBillsSummaryToolStripMenuItem.Text = "Stock Write Off Bills Summary";
            this.stockWriteOffBillsSummaryToolStripMenuItem.Click += new System.EventHandler(this.stockWriteOffBillsSummaryToolStripMenuItem_Click);
            // 
            // stockWriteOffItemsSummaryToolStripMenuItem
            // 
            this.stockWriteOffItemsSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockWriteOffItemsSummaryToolStripMenuItem.Name = "stockWriteOffItemsSummaryToolStripMenuItem";
            this.stockWriteOffItemsSummaryToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.stockWriteOffItemsSummaryToolStripMenuItem.Text = "Stock Write Off Items Summary";
            this.stockWriteOffItemsSummaryToolStripMenuItem.Click += new System.EventHandler(this.stockWriteOffItemsSummaryToolStripMenuItem_Click);
            // 
            // stockConsumeReportsToolStripMenuItem
            // 
            this.stockConsumeReportsToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockConsumeReportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stockConsumeItemSummaryToolStripMenuItem});
            this.stockConsumeReportsToolStripMenuItem.Name = "stockConsumeReportsToolStripMenuItem";
            this.stockConsumeReportsToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.stockConsumeReportsToolStripMenuItem.Text = "Stock Consume Reports";
            // 
            // stockConsumeItemSummaryToolStripMenuItem
            // 
            this.stockConsumeItemSummaryToolStripMenuItem.Name = "stockConsumeItemSummaryToolStripMenuItem";
            this.stockConsumeItemSummaryToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.stockConsumeItemSummaryToolStripMenuItem.Text = "Stock Consume Item Summary";
            this.stockConsumeItemSummaryToolStripMenuItem.Click += new System.EventHandler(this.stockConsumeItemSummaryToolStripMenuItem_Click);
            // 
            // stockAdjustmentToolStripMenuItem
            // 
            this.stockAdjustmentToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockAdjustmentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stockAdjustmentSummaryToolStripMenuItem,
            this.sTOCKaDJUSTMENTToolStripMenuItem1,
            this.stockAdjItemSummaryToolStripMenuItem});
            this.stockAdjustmentToolStripMenuItem.Name = "stockAdjustmentToolStripMenuItem";
            this.stockAdjustmentToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.stockAdjustmentToolStripMenuItem.Text = "Stock Adjustment Reports";
            // 
            // stockAdjustmentSummaryToolStripMenuItem
            // 
            this.stockAdjustmentSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockAdjustmentSummaryToolStripMenuItem.Name = "stockAdjustmentSummaryToolStripMenuItem";
            this.stockAdjustmentSummaryToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.stockAdjustmentSummaryToolStripMenuItem.Text = "Stock Adjustment Summary";
            this.stockAdjustmentSummaryToolStripMenuItem.Click += new System.EventHandler(this.stockAdjustmentSummaryToolStripMenuItem_Click);
            // 
            // sTOCKaDJUSTMENTToolStripMenuItem1
            // 
            this.sTOCKaDJUSTMENTToolStripMenuItem1.BackColor = System.Drawing.Color.White;
            this.sTOCKaDJUSTMENTToolStripMenuItem1.Name = "sTOCKaDJUSTMENTToolStripMenuItem1";
            this.sTOCKaDJUSTMENTToolStripMenuItem1.Size = new System.Drawing.Size(252, 22);
            this.sTOCKaDJUSTMENTToolStripMenuItem1.Text = "Stock adjustment Bill Summary";
            this.sTOCKaDJUSTMENTToolStripMenuItem1.Click += new System.EventHandler(this.sTOCKaDJUSTMENTToolStripMenuItem1_Click);
            // 
            // stockAdjItemSummaryToolStripMenuItem
            // 
            this.stockAdjItemSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockAdjItemSummaryToolStripMenuItem.Name = "stockAdjItemSummaryToolStripMenuItem";
            this.stockAdjItemSummaryToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.stockAdjItemSummaryToolStripMenuItem.Text = "Stock Adj Item Summary";
            this.stockAdjItemSummaryToolStripMenuItem.Click += new System.EventHandler(this.stockAdjustmentItemsSummaryToolStripMenuItem_Click);
            // 
            // productionToolStripMenuItem1
            // 
            this.productionToolStripMenuItem1.BackColor = System.Drawing.Color.White;
            this.productionToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.productionSummaryToolStripMenuItem,
            this.productionBillSummaryToolStripMenuItem});
            this.productionToolStripMenuItem1.Name = "productionToolStripMenuItem1";
            this.productionToolStripMenuItem1.Size = new System.Drawing.Size(224, 22);
            this.productionToolStripMenuItem1.Text = "Production Reports";
            // 
            // productionSummaryToolStripMenuItem
            // 
            this.productionSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.productionSummaryToolStripMenuItem.Name = "productionSummaryToolStripMenuItem";
            this.productionSummaryToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.productionSummaryToolStripMenuItem.Text = "Production Item Summary";
            this.productionSummaryToolStripMenuItem.Click += new System.EventHandler(this.productionItemsSummaryToolStripMenuItem_Click);
            // 
            // productionBillSummaryToolStripMenuItem
            // 
            this.productionBillSummaryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.productionBillSummaryToolStripMenuItem.Name = "productionBillSummaryToolStripMenuItem";
            this.productionBillSummaryToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.productionBillSummaryToolStripMenuItem.Text = "Production Bill Summary";
            this.productionBillSummaryToolStripMenuItem.Click += new System.EventHandler(this.productionBillSummaryToolStripMenuItem_Click);
            // 
            // stockInventoryToolStripMenuItem
            // 
            this.stockInventoryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockInventoryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.outletStockToolStripMenuItem});
            this.stockInventoryToolStripMenuItem.Name = "stockInventoryToolStripMenuItem";
            this.stockInventoryToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.stockInventoryToolStripMenuItem.Text = "Stock In Hand Reports";
            this.stockInventoryToolStripMenuItem.Click += new System.EventHandler(this.stockInventoryToolStripMenuItem_Click);
            // 
            // outletStockToolStripMenuItem
            // 
            this.outletStockToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.outletStockToolStripMenuItem.Name = "outletStockToolStripMenuItem";
            this.outletStockToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.outletStockToolStripMenuItem.Text = "Outlet Stock";
            this.outletStockToolStripMenuItem.Click += new System.EventHandler(this.outletStockToolStripMenuItem_Click_1);
            // 
            // customerLedgerToolStripMenuItem
            // 
            this.customerLedgerToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.customerLedgerToolStripMenuItem.Name = "customerLedgerToolStripMenuItem";
            this.customerLedgerToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.customerLedgerToolStripMenuItem.Text = "Customer Ledger";
            this.customerLedgerToolStripMenuItem.Click += new System.EventHandler(this.customerLedgerToolStripMenuItem_Click);
            // 
            // supplierLedgerToolStripMenuItem
            // 
            this.supplierLedgerToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.supplierLedgerToolStripMenuItem.Name = "supplierLedgerToolStripMenuItem";
            this.supplierLedgerToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.supplierLedgerToolStripMenuItem.Text = "SupplierLedger";
            this.supplierLedgerToolStripMenuItem.Click += new System.EventHandler(this.supplierLedgerToolStripMenuItem_Click);
            // 
            // logHistoryToolStripMenuItem
            // 
            this.logHistoryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.logHistoryToolStripMenuItem.Name = "logHistoryToolStripMenuItem";
            this.logHistoryToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.logHistoryToolStripMenuItem.Text = "Log History";
            this.logHistoryToolStripMenuItem.Click += new System.EventHandler(this.logHistoryToolStripMenuItem_Click);
            // 
            // itemSalesHistoryToolStripMenuItem
            // 
            this.itemSalesHistoryToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.itemSalesHistoryToolStripMenuItem.Name = "itemSalesHistoryToolStripMenuItem";
            this.itemSalesHistoryToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.itemSalesHistoryToolStripMenuItem.Text = "Item Sales History";
            this.itemSalesHistoryToolStripMenuItem.Click += new System.EventHandler(this.itemSalesHistoryToolStripMenuItem_Click);
            // 
            // dayReportToolStripMenuItem
            // 
            this.dayReportToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.dayReportToolStripMenuItem.Name = "dayReportToolStripMenuItem";
            this.dayReportToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.dayReportToolStripMenuItem.Text = "Day Report";
            this.dayReportToolStripMenuItem.Click += new System.EventHandler(this.dayReportToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.systemSettingsToolStripMenuItem,
            this.clientSettingsToolStripMenuItem,
            this.stockSettingsToolStripMenuItem,
            this.stockReportSettingsToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.settingsToolStripMenuItem.Text = "Se&ttings";
            // 
            // systemSettingsToolStripMenuItem
            // 
            this.systemSettingsToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.systemSettingsToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.systemSettingsToolStripMenuItem.Name = "systemSettingsToolStripMenuItem";
            this.systemSettingsToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.systemSettingsToolStripMenuItem.Text = "&Bill Settings";
            this.systemSettingsToolStripMenuItem.Click += new System.EventHandler(this.systemSettingsToolStripMenuItem_Click);
            // 
            // clientSettingsToolStripMenuItem
            // 
            this.clientSettingsToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.clientSettingsToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.clientSettingsToolStripMenuItem.Name = "clientSettingsToolStripMenuItem";
            this.clientSettingsToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.clientSettingsToolStripMenuItem.Text = "&Client Settings";
            this.clientSettingsToolStripMenuItem.Click += new System.EventHandler(this.clientSettingsToolStripMenuItem_Click);
            // 
            // stockSettingsToolStripMenuItem
            // 
            this.stockSettingsToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockSettingsToolStripMenuItem.Name = "stockSettingsToolStripMenuItem";
            this.stockSettingsToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.stockSettingsToolStripMenuItem.Text = "&Stock Settings";
            this.stockSettingsToolStripMenuItem.Click += new System.EventHandler(this.stockSettingsToolStripMenuItem_Click);
            // 
            // stockReportSettingsToolStripMenuItem
            // 
            this.stockReportSettingsToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.stockReportSettingsToolStripMenuItem.Name = "stockReportSettingsToolStripMenuItem";
            this.stockReportSettingsToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.stockReportSettingsToolStripMenuItem.Text = "&Stock Report Settings";
            this.stockReportSettingsToolStripMenuItem.Click += new System.EventHandler(this.stockReportSettingsToolStripMenuItem_Click);
            // 
            // toolsMenu
            // 
            this.toolsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.themesToolStripMenuItem,
            this.backUpRestoreToolStripMenuItem,
            this.rEPRINTToolStripMenuItem,
            this.dayEndToolStripMenuItem,
            this.registerProductToolStripMenuItem});
            this.toolsMenu.Name = "toolsMenu";
            this.toolsMenu.Size = new System.Drawing.Size(49, 20);
            this.toolsMenu.Text = "&Tools";
            // 
            // themesToolStripMenuItem
            // 
            this.themesToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.themesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.noThemeToolStripMenuItem,
            this.homeSteadToolStripMenuItem,
            this.metalicToolStripMenuItem,
            this.brickToolStripMenuItem,
            this.blueToolStripMenuItem});
            this.themesToolStripMenuItem.Enabled = false;
            this.themesToolStripMenuItem.Image = global::ParaDine.Properties.Resources.color2_16x16;
            this.themesToolStripMenuItem.Name = "themesToolStripMenuItem";
            this.themesToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.themesToolStripMenuItem.Text = "T&hemes";
            // 
            // noThemeToolStripMenuItem
            // 
            this.noThemeToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.noThemeToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.noThemeToolStripMenuItem.Name = "noThemeToolStripMenuItem";
            this.noThemeToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.noThemeToolStripMenuItem.Tag = "0";
            this.noThemeToolStripMenuItem.Text = "No Theme";
            // 
            // homeSteadToolStripMenuItem
            // 
            this.homeSteadToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.homeSteadToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.homeSteadToolStripMenuItem.Image = global::ParaDine.Properties.Resources.Homestead_Sample;
            this.homeSteadToolStripMenuItem.Name = "homeSteadToolStripMenuItem";
            this.homeSteadToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.homeSteadToolStripMenuItem.Tag = "1";
            this.homeSteadToolStripMenuItem.Text = "HomeStead";
            // 
            // metalicToolStripMenuItem
            // 
            this.metalicToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.metalicToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.metalicToolStripMenuItem.Image = global::ParaDine.Properties.Resources.Metallic_Sample;
            this.metalicToolStripMenuItem.Name = "metalicToolStripMenuItem";
            this.metalicToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.metalicToolStripMenuItem.Tag = "2";
            this.metalicToolStripMenuItem.Text = "Metalic";
            // 
            // brickToolStripMenuItem
            // 
            this.brickToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.brickToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.brickToolStripMenuItem.Image = global::ParaDine.Properties.Resources.Brick_Sample;
            this.brickToolStripMenuItem.Name = "brickToolStripMenuItem";
            this.brickToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.brickToolStripMenuItem.Tag = "3";
            this.brickToolStripMenuItem.Text = "Brick";
            // 
            // blueToolStripMenuItem
            // 
            this.blueToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.blueToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.blueToolStripMenuItem.Image = global::ParaDine.Properties.Resources.Blue_Sample2;
            this.blueToolStripMenuItem.Name = "blueToolStripMenuItem";
            this.blueToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.blueToolStripMenuItem.Tag = "4";
            this.blueToolStripMenuItem.Text = "Blue";
            this.blueToolStripMenuItem.Click += new System.EventHandler(this.blueToolStripMenuItem_Click);
            // 
            // backUpRestoreToolStripMenuItem
            // 
            this.backUpRestoreToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.backUpRestoreToolStripMenuItem.Name = "backUpRestoreToolStripMenuItem";
            this.backUpRestoreToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.backUpRestoreToolStripMenuItem.Text = "BackUp/Restore";
            // 
            // rEPRINTToolStripMenuItem
            // 
            this.rEPRINTToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.rEPRINTToolStripMenuItem.Name = "rEPRINTToolStripMenuItem";
            this.rEPRINTToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.rEPRINTToolStripMenuItem.Text = "&RePrint";
            this.rEPRINTToolStripMenuItem.Click += new System.EventHandler(this.rEPRINTToolStripMenuItem_Click);
            // 
            // dayEndToolStripMenuItem
            // 
            this.dayEndToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.dayEndToolStripMenuItem.Name = "dayEndToolStripMenuItem";
            this.dayEndToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.dayEndToolStripMenuItem.Text = "Day End";
            this.dayEndToolStripMenuItem.Click += new System.EventHandler(this.dayEndToolStripMenuItem_Click);
            // 
            // registerProductToolStripMenuItem
            // 
            this.registerProductToolStripMenuItem.Name = "registerProductToolStripMenuItem";
            this.registerProductToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.registerProductToolStripMenuItem.Text = "Register Product";
            this.registerProductToolStripMenuItem.Click += new System.EventHandler(this.registerProductToolStripMenuItem_Click);
            // 
            // securityToolStripMenuItem
            // 
            this.securityToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.authorisationSettingsToolStripMenuItem,
            this.historySettingsToolStripMenuItem,
            this.userAccountToolStripMenuItem,
            this.userLevelsToolStripMenuItem,
            this.hintsToolStripMenuItem,
            this.changePasswToolStripMenuItem});
            this.securityToolStripMenuItem.Name = "securityToolStripMenuItem";
            this.securityToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.securityToolStripMenuItem.Text = "Security";
            // 
            // authorisationSettingsToolStripMenuItem
            // 
            this.authorisationSettingsToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.authorisationSettingsToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.authorisationSettingsToolStripMenuItem.Name = "authorisationSettingsToolStripMenuItem";
            this.authorisationSettingsToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.authorisationSettingsToolStripMenuItem.Text = "&Authorization Settings";
            this.authorisationSettingsToolStripMenuItem.Click += new System.EventHandler(this.authorisationSettingsToolStripMenuItem_Click);
            // 
            // historySettingsToolStripMenuItem
            // 
            this.historySettingsToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.historySettingsToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.historySettingsToolStripMenuItem.Name = "historySettingsToolStripMenuItem";
            this.historySettingsToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.historySettingsToolStripMenuItem.Text = "&History Settings";
            this.historySettingsToolStripMenuItem.Click += new System.EventHandler(this.historySettingsToolStripMenuItem_Click);
            // 
            // userAccountToolStripMenuItem
            // 
            this.userAccountToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.userAccountToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.userAccountToolStripMenuItem.Name = "userAccountToolStripMenuItem";
            this.userAccountToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.userAccountToolStripMenuItem.Text = "&User Login Account";
            this.userAccountToolStripMenuItem.Click += new System.EventHandler(this.userAccountToolStripMenuItem_Click);
            // 
            // userLevelsToolStripMenuItem
            // 
            this.userLevelsToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.userLevelsToolStripMenuItem.Name = "userLevelsToolStripMenuItem";
            this.userLevelsToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.userLevelsToolStripMenuItem.Text = "&User Levels";
            this.userLevelsToolStripMenuItem.Click += new System.EventHandler(this.userLevelsToolStripMenuItem_Click);
            // 
            // hintsToolStripMenuItem
            // 
            this.hintsToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.hintsToolStripMenuItem.Name = "hintsToolStripMenuItem";
            this.hintsToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.hintsToolStripMenuItem.Text = "Hints";
            this.hintsToolStripMenuItem.Click += new System.EventHandler(this.hintsToolStripMenuItem_Click);
            // 
            // changePasswToolStripMenuItem
            // 
            this.changePasswToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.changePasswToolStripMenuItem.Name = "changePasswToolStripMenuItem";
            this.changePasswToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.changePasswToolStripMenuItem.Text = "Change Password";
            this.changePasswToolStripMenuItem.Click += new System.EventHandler(this.changePasswToolStripMenuItem_Click);
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logOutToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
            this.quitToolStripMenuItem.Text = "&Quit";
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.logOutToolStripMenuItem.Text = "&Log Out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.AutoSize = false;
            this.statusStrip.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSBussDate,
            this.lblAlert});
            this.statusStrip.Location = new System.Drawing.Point(0, 548);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(908, 23);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            this.statusStrip.Visible = false;
            // 
            // TSBussDate
            // 
            this.TSBussDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.TSBussDate.Name = "TSBussDate";
            this.TSBussDate.Size = new System.Drawing.Size(861, 18);
            this.TSBussDate.Spring = true;
            this.TSBussDate.Text = "Status";
            // 
            // lblAlert
            // 
            this.lblAlert.AutoSize = false;
            this.lblAlert.AutoToolTip = true;
            this.lblAlert.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.lblAlert.Name = "lblAlert";
            this.lblAlert.Size = new System.Drawing.Size(32, 18);
            this.lblAlert.ToolTipText = "Pending delivery orders for today.";
            this.lblAlert.Click += new System.EventHandler(this.lblAlert_Click);
            // 
            // btnCancelBill
            // 
            this.btnCancelBill.BackColor = System.Drawing.Color.Transparent;
            this.btnCancelBill.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancelBill.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelBill.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnCancelBill.FlatAppearance.BorderSize = 0;
            this.btnCancelBill.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnCancelBill.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnCancelBill.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnCancelBill.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelBill.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelBill.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnCancelBill.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCancelBill.ImageList = this.ImgLstMain;
            this.btnCancelBill.Location = new System.Drawing.Point(209, 79);
            this.btnCancelBill.Name = "btnCancelBill";
            this.btnCancelBill.Size = new System.Drawing.Size(105, 67);
            this.btnCancelBill.TabIndex = 13;
            this.btnCancelBill.Text = "&Bill Cancel";
            this.btnCancelBill.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ToolTip.SetToolTip(this.btnCancelBill, "To cancel an existing bill.");
            this.btnCancelBill.UseVisualStyleBackColor = false;
            // 
            // ImgLstMain
            // 
            this.ImgLstMain.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImgLstMain.ImageStream")));
            this.ImgLstMain.TransparentColor = System.Drawing.Color.Transparent;
            this.ImgLstMain.Images.SetKeyName(0, "ImgLstMain0.png");
            this.ImgLstMain.Images.SetKeyName(1, "ImgLstMain1.png");
            this.ImgLstMain.Images.SetKeyName(2, "ImgLstMain2.png");
            this.ImgLstMain.Images.SetKeyName(3, "ImgLstMain3.png");
            this.ImgLstMain.Images.SetKeyName(4, "ImgLstMain4.png");
            this.ImgLstMain.Images.SetKeyName(5, "ImgLstMain5.png");
            this.ImgLstMain.Images.SetKeyName(6, "ImgLstMain6.png");
            this.ImgLstMain.Images.SetKeyName(7, "ImgLstMain7.png");
            this.ImgLstMain.Images.SetKeyName(8, "ImgLstMain8.png");
            this.ImgLstMain.Images.SetKeyName(9, "ImgLstMain9.png");
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel1.Controls.Add(this.LblBussDate);
            this.panel1.Controls.Add(this.PnlNoTheme);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 502);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(908, 69);
            this.panel1.TabIndex = 4;
            // 
            // LblBussDate
            // 
            this.LblBussDate.AutoSize = true;
            this.LblBussDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBussDate.Location = new System.Drawing.Point(13, 26);
            this.LblBussDate.Name = "LblBussDate";
            this.LblBussDate.Size = new System.Drawing.Size(47, 15);
            this.LblBussDate.TabIndex = 27;
            this.LblBussDate.Text = "label1";
            // 
            // PnlNoTheme
            // 
            this.PnlNoTheme.BackColor = System.Drawing.Color.Black;
            this.PnlNoTheme.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlNoTheme.Controls.Add(this.BtnEndOfDay);
            this.PnlNoTheme.Controls.Add(this.btn_Tables);
            this.PnlNoTheme.Controls.Add(this.btnOrders);
            this.PnlNoTheme.Controls.Add(this.btnCustOrder);
            this.PnlNoTheme.Controls.Add(this.btnDirectOrder);
            this.PnlNoTheme.Controls.Add(this.btnCancelBill);
            this.PnlNoTheme.Controls.Add(this.Label12);
            this.PnlNoTheme.Controls.Add(this.BtnReservation);
            this.PnlNoTheme.Controls.Add(this.Label11);
            this.PnlNoTheme.Controls.Add(this.btnShifts);
            this.PnlNoTheme.Controls.Add(this.Label10);
            this.PnlNoTheme.Controls.Add(this.Label9);
            this.PnlNoTheme.Controls.Add(this.Label8);
            this.PnlNoTheme.Controls.Add(this.Label7);
            this.PnlNoTheme.Controls.Add(this.Label6);
            this.PnlNoTheme.Controls.Add(this.Label5);
            this.PnlNoTheme.Controls.Add(this.lblCustomerorder);
            this.PnlNoTheme.Controls.Add(this.BtnKitchen_Bar);
            this.PnlNoTheme.Controls.Add(this.btnSendMail);
            this.PnlNoTheme.Location = new System.Drawing.Point(192, 1);
            this.PnlNoTheme.Name = "PnlNoTheme";
            this.PnlNoTheme.Size = new System.Drawing.Size(525, 65);
            this.PnlNoTheme.TabIndex = 26;
            this.PnlNoTheme.Tag = "NoTheme";
            // 
            // BtnEndOfDay
            // 
            this.BtnEndOfDay.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.BtnEndOfDay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnEndOfDay.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnEndOfDay.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BtnEndOfDay.FlatAppearance.BorderSize = 0;
            this.BtnEndOfDay.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BtnEndOfDay.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.BtnEndOfDay.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BtnEndOfDay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnEndOfDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEndOfDay.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BtnEndOfDay.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnEndOfDay.ImageIndex = 0;
            this.BtnEndOfDay.Location = new System.Drawing.Point(419, -2);
            this.BtnEndOfDay.Name = "BtnEndOfDay";
            this.BtnEndOfDay.Size = new System.Drawing.Size(105, 67);
            this.BtnEndOfDay.TabIndex = 14;
            this.BtnEndOfDay.Text = "&End of Day";
            this.BtnEndOfDay.UseVisualStyleBackColor = false;
            this.BtnEndOfDay.Click += new System.EventHandler(this.BtnEndOfDay_Click);
            // 
            // btn_Tables
            // 
            this.btn_Tables.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_Tables.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Tables.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Tables.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_Tables.FlatAppearance.BorderSize = 0;
            this.btn_Tables.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btn_Tables.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btn_Tables.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btn_Tables.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Tables.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Tables.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_Tables.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_Tables.ImageIndex = 4;
            this.btn_Tables.Location = new System.Drawing.Point(314, -2);
            this.btn_Tables.Name = "btn_Tables";
            this.btn_Tables.Size = new System.Drawing.Size(105, 67);
            this.btn_Tables.TabIndex = 4;
            this.btn_Tables.Text = "&Table";
            this.btn_Tables.UseVisualStyleBackColor = false;
            this.btn_Tables.Click += new System.EventHandler(this.btn_Tables_Click);
            // 
            // btnOrders
            // 
            this.btnOrders.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnOrders.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnOrders.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOrders.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnOrders.FlatAppearance.BorderSize = 0;
            this.btnOrders.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnOrders.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnOrders.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnOrders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOrders.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnOrders.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnOrders.ImageIndex = 1;
            this.btnOrders.Location = new System.Drawing.Point(209, -2);
            this.btnOrders.Name = "btnOrders";
            this.btnOrders.Size = new System.Drawing.Size(105, 67);
            this.btnOrders.TabIndex = 1;
            this.btnOrders.Text = "&Bill View";
            this.btnOrders.UseVisualStyleBackColor = false;
            this.btnOrders.Click += new System.EventHandler(this.btnOrders_Click);
            // 
            // btnCustOrder
            // 
            this.btnCustOrder.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCustOrder.BackgroundImage = global::ParaDine.Properties.Resources.kitchen_mgnt;
            this.btnCustOrder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCustOrder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCustOrder.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnCustOrder.FlatAppearance.BorderSize = 0;
            this.btnCustOrder.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnCustOrder.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnCustOrder.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnCustOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCustOrder.ForeColor = System.Drawing.Color.PaleVioletRed;
            this.btnCustOrder.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCustOrder.Location = new System.Drawing.Point(100, -2);
            this.btnCustOrder.Name = "btnCustOrder";
            this.btnCustOrder.Size = new System.Drawing.Size(110, 67);
            this.btnCustOrder.TabIndex = 0;
            this.btnCustOrder.Text = "&KOT Order";
            this.btnCustOrder.UseVisualStyleBackColor = false;
            this.btnCustOrder.Click += new System.EventHandler(this.btnCustOrder_Click);
            // 
            // btnDirectOrder
            // 
            this.btnDirectOrder.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnDirectOrder.BackgroundImage = global::ParaDine.Properties.Resources.Order_Now;
            this.btnDirectOrder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDirectOrder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDirectOrder.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnDirectOrder.FlatAppearance.BorderSize = 0;
            this.btnDirectOrder.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnDirectOrder.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnDirectOrder.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnDirectOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDirectOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDirectOrder.ForeColor = System.Drawing.Color.Crimson;
            this.btnDirectOrder.Location = new System.Drawing.Point(-1, -2);
            this.btnDirectOrder.Name = "btnDirectOrder";
            this.btnDirectOrder.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnDirectOrder.Size = new System.Drawing.Size(105, 67);
            this.btnDirectOrder.TabIndex = 15;
            this.btnDirectOrder.Text = "&Direct Order";
            this.btnDirectOrder.UseVisualStyleBackColor = false;
            this.btnDirectOrder.Click += new System.EventHandler(this.btnDirectOrder_Click);
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label12.ForeColor = System.Drawing.Color.White;
            this.Label12.Location = new System.Drawing.Point(649, 72);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(75, 13);
            this.Label12.TabIndex = 11;
            this.Label12.Tag = "NoTheme";
            this.Label12.Text = "Switch User";
            // 
            // BtnReservation
            // 
            this.BtnReservation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnReservation.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnReservation.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BtnReservation.FlatAppearance.BorderSize = 0;
            this.BtnReservation.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BtnReservation.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.BtnReservation.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BtnReservation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnReservation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnReservation.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BtnReservation.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnReservation.ImageIndex = 4;
            this.BtnReservation.ImageList = this.ImgLstMain;
            this.BtnReservation.Location = new System.Drawing.Point(734, -1);
            this.BtnReservation.Name = "BtnReservation";
            this.BtnReservation.Size = new System.Drawing.Size(105, 67);
            this.BtnReservation.TabIndex = 12;
            this.BtnReservation.Text = "&Reservation";
            this.BtnReservation.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnReservation.UseVisualStyleBackColor = false;
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label11.ForeColor = System.Drawing.Color.White;
            this.Label11.Location = new System.Drawing.Point(541, 72);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(72, 13);
            this.Label11.TabIndex = 9;
            this.Label11.Tag = "NoTheme";
            this.Label11.Text = "End Of Day";
            // 
            // btnShifts
            // 
            this.btnShifts.BackColor = System.Drawing.Color.Transparent;
            this.btnShifts.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnShifts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnShifts.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnShifts.FlatAppearance.BorderSize = 0;
            this.btnShifts.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnShifts.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnShifts.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnShifts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShifts.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShifts.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnShifts.ImageIndex = 4;
            this.btnShifts.ImageList = this.ImgLstMain;
            this.btnShifts.Location = new System.Drawing.Point(844, -1);
            this.btnShifts.Name = "btnShifts";
            this.btnShifts.Size = new System.Drawing.Size(105, 54);
            this.btnShifts.TabIndex = 3;
            this.btnShifts.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnShifts.UseVisualStyleBackColor = false;
            this.btnShifts.Visible = false;
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label10.ForeColor = System.Drawing.Color.White;
            this.Label10.Location = new System.Drawing.Point(434, 72);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(75, 13);
            this.Label10.TabIndex = 7;
            this.Label10.Tag = "NoTheme";
            this.Label10.Text = "Kitchen\\Bar";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label9.ForeColor = System.Drawing.Color.White;
            this.Label9.Location = new System.Drawing.Point(864, 53);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(63, 13);
            this.Label9.TabIndex = 7;
            this.Label9.Tag = "NoTheme";
            this.Label9.Text = "Send Mail";
            this.Label9.Visible = false;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label8.ForeColor = System.Drawing.Color.White;
            this.Label8.Location = new System.Drawing.Point(343, 72);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(45, 13);
            this.Label8.TabIndex = 7;
            this.Label8.Tag = "NoTheme";
            this.Label8.Text = "Tables";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label7.ForeColor = System.Drawing.Color.White;
            this.Label7.Location = new System.Drawing.Point(877, 53);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(39, 13);
            this.Label7.TabIndex = 7;
            this.Label7.Tag = "NoTheme";
            this.Label7.Text = "Shifts";
            this.Label7.Visible = false;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.ForeColor = System.Drawing.Color.White;
            this.Label6.Location = new System.Drawing.Point(229, 72);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(51, 13);
            this.Label6.TabIndex = 7;
            this.Label6.Tag = "NoTheme";
            this.Label6.Text = "Masters";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.ForeColor = System.Drawing.Color.White;
            this.Label5.Location = new System.Drawing.Point(135, 72);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(44, 13);
            this.Label5.TabIndex = 7;
            this.Label5.Tag = "NoTheme";
            this.Label5.Text = "Orders";
            // 
            // lblCustomerorder
            // 
            this.lblCustomerorder.AutoSize = true;
            this.lblCustomerorder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerorder.ForeColor = System.Drawing.Color.White;
            this.lblCustomerorder.Location = new System.Drawing.Point(4, 72);
            this.lblCustomerorder.Name = "lblCustomerorder";
            this.lblCustomerorder.Size = new System.Drawing.Size(94, 13);
            this.lblCustomerorder.TabIndex = 7;
            this.lblCustomerorder.Tag = "NoTheme";
            this.lblCustomerorder.Text = "Customer Order";
            // 
            // BtnKitchen_Bar
            // 
            this.BtnKitchen_Bar.BackColor = System.Drawing.Color.Transparent;
            this.BtnKitchen_Bar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnKitchen_Bar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnKitchen_Bar.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BtnKitchen_Bar.FlatAppearance.BorderSize = 0;
            this.BtnKitchen_Bar.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BtnKitchen_Bar.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.BtnKitchen_Bar.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BtnKitchen_Bar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnKitchen_Bar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnKitchen_Bar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BtnKitchen_Bar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnKitchen_Bar.ImageIndex = 1;
            this.BtnKitchen_Bar.ImageList = this.ImgLstMain;
            this.BtnKitchen_Bar.Location = new System.Drawing.Point(419, 79);
            this.BtnKitchen_Bar.Name = "BtnKitchen_Bar";
            this.BtnKitchen_Bar.Size = new System.Drawing.Size(105, 67);
            this.BtnKitchen_Bar.TabIndex = 6;
            this.BtnKitchen_Bar.Text = "&Cook Status";
            this.BtnKitchen_Bar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnKitchen_Bar.UseVisualStyleBackColor = false;
            // 
            // btnSendMail
            // 
            this.btnSendMail.BackColor = System.Drawing.Color.Transparent;
            this.btnSendMail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSendMail.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSendMail.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnSendMail.FlatAppearance.BorderSize = 0;
            this.btnSendMail.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnSendMail.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnSendMail.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnSendMail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSendMail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSendMail.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSendMail.ImageIndex = 3;
            this.btnSendMail.ImageList = this.ImgLstMain;
            this.btnSendMail.Location = new System.Drawing.Point(842, -1);
            this.btnSendMail.Name = "btnSendMail";
            this.btnSendMail.Size = new System.Drawing.Size(105, 54);
            this.btnSendMail.TabIndex = 5;
            this.btnSendMail.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSendMail.UseVisualStyleBackColor = false;
            this.btnSendMail.Visible = false;
            // 
            // IMgLst
            // 
            this.IMgLst.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("IMgLst.ImageStream")));
            this.IMgLst.TransparentColor = System.Drawing.Color.Transparent;
            this.IMgLst.Images.SetKeyName(0, "IMgLst0.png");
            this.IMgLst.Images.SetKeyName(1, "IMgLst1.png");
            this.IMgLst.Images.SetKeyName(2, "IMgLst2.png");
            this.IMgLst.Images.SetKeyName(3, "IMgLst3.png");
            this.IMgLst.Images.SetKeyName(4, "IMgLst4.png");
            this.IMgLst.Images.SetKeyName(5, "IMgLst5.png");
            this.IMgLst.Images.SetKeyName(6, "IMgLst6.png");
            // 
            // ImgLstAlert
            // 
            this.ImgLstAlert.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImgLstAlert.ImageStream")));
            this.ImgLstAlert.TransparentColor = System.Drawing.Color.Transparent;
            this.ImgLstAlert.Images.SetKeyName(0, "ImgLstAlert0.png");
            this.ImgLstAlert.Images.SetKeyName(1, "ImgLstAlert1.png");
            this.ImgLstAlert.Images.SetKeyName(2, "ImgLstAlert2.png");
            this.ImgLstAlert.Images.SetKeyName(3, "ImgLstAlert3.png");
            this.ImgLstAlert.Images.SetKeyName(4, "ImgLstAlert4.png");
            this.ImgLstAlert.Images.SetKeyName(5, "ImgLstAlert5.png");
            this.ImgLstAlert.Images.SetKeyName(6, "ImgLstAlert6.png");
            this.ImgLstAlert.Images.SetKeyName(7, "ImgLstAlert7.png");
            this.ImgLstAlert.Images.SetKeyName(8, "ImgLstAlert8.png");
            this.ImgLstAlert.Images.SetKeyName(9, "ImgLstAlert9.png");
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 60000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lbllefttime
            // 
            this.lbllefttime.AutoSize = true;
            this.lbllefttime.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.lbllefttime.Location = new System.Drawing.Point(737, 5);
            this.lbllefttime.Name = "lbllefttime";
            this.lbllefttime.Size = new System.Drawing.Size(0, 13);
            this.lbllefttime.TabIndex = 6;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.label13.Location = new System.Drawing.Point(636, 5);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(95, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "Demo Time (Mins):";
            this.label13.Visible = false;
            // 
            // frmMDIRest
            // 
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(908, 571);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.lbllefttime);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.statusStrip);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "frmMDIRest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ParaDine 4.5";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMDIRest_FormClosed);
            this.Load += new System.EventHandler(this.frmMDIRest_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.PnlNoTheme.ResumeLayout(false);
            this.PnlNoTheme.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

    }
}