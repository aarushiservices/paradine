﻿using ParaDine.Properties;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.GlobalForms
{
    partial class frmGridView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHeader = new Label();
            this.pnlMain = new Panel();
            this.dgview = new DataGridView();
            this.btnExit = new Button();
            this.pnlMain.SuspendLayout();
            ((ISupportInitialize)this.dgview).BeginInit();
            base.SuspendLayout();
            this.lblHeader.AutoSize = true;
            this.lblHeader.BackColor = Color.Transparent;
            this.lblHeader.Font = new Font("Calibri", 11.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.lblHeader.ForeColor = Color.White;
            this.lblHeader.Location = new Point(7, 9);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new Size(0x62, 0x12);
            this.lblHeader.TabIndex = 0x18;
            this.lblHeader.Text = "Report Header";
           // this.pnlMain.BackgroundImage = Resources.Brick_Sample;
            this.pnlMain.BackgroundImageLayout = ImageLayout.Stretch;
            this.pnlMain.BorderStyle = BorderStyle.FixedSingle;
            this.pnlMain.Controls.Add(this.dgview);
            this.pnlMain.Controls.Add(this.btnExit);
            this.pnlMain.Location = new Point(4, 0x35);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new Size(0x364, 0x27f);
            this.pnlMain.TabIndex = 0x17;
            this.dgview.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgview.Location = new Point(4, 6);
            this.dgview.Name = "dgview";
            this.dgview.Size = new Size(0x35a, 0x251);
            this.dgview.TabIndex = 7;
            this.btnExit.Font = new Font("Calibri", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnExit.Location = new Point(0x306, 0x25d);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new Size(0x55, 0x1a);
            this.btnExit.TabIndex = 6;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new EventHandler(this.btnExit_Click);
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            //base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x36c, 0x2b9);
            base.ControlBox = false;
            base.Controls.Add(this.lblHeader);
            base.Controls.Add(this.pnlMain);
            //            base.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            base.Name = "frmGridView";
            base.StartPosition = FormStartPosition.CenterScreen;
            base.Load += new EventHandler(this.frmGridView_Load);
            this.pnlMain.ResumeLayout(false);
            ((ISupportInitialize)this.dgview).EndInit();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        #endregion
    }
}