﻿using ParControls;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.GlobalForms
{
    partial class FrmTableLocation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(FrmTableLocation));
            this.PnlStatus = new Panel();
            this.lblCovered = new Label();
            this.lblVacant = new Label();
            this.lblOccupied = new Label();
            this.lblNotActive = new Label();
            this.ToolTip1 = new ToolTip(this.components);
            this.ImgTable = new ImageList(this.components);
            this.CntxtTable = new ContextMenuStrip(this.components);
            this.ShowOrderToolStripMenuItem = new ToolStripMenuItem();
            this.DeactivateTableToolStripMenuItem = new ToolStripMenuItem();
            this.pnlTableSettings = new Panel();
            this.btnOrdDiscount = new Button();
            this.CHKLock = new CheckBox();
            this.pnlTables = new ParPanel();
            this.timer1 = new Timer(this.components);
            this.PnlStatus.SuspendLayout();
            this.CntxtTable.SuspendLayout();
            this.pnlTableSettings.SuspendLayout();
            base.SuspendLayout();
            this.PnlStatus.BorderStyle = BorderStyle.FixedSingle;
            this.PnlStatus.Controls.Add(this.lblCovered);
            this.PnlStatus.Controls.Add(this.lblVacant);
            this.PnlStatus.Controls.Add(this.lblOccupied);
            this.PnlStatus.Controls.Add(this.lblNotActive);
            this.PnlStatus.Dock = DockStyle.Bottom;
            this.PnlStatus.Location = new Point(0, 0x1e9);
            this.PnlStatus.Name = "PnlStatus";
            this.PnlStatus.Size = new Size(0x390, 0x26);
            this.PnlStatus.TabIndex = 0;
            this.lblCovered.AutoSize = true;
            this.lblCovered.BackColor = Color.Transparent;
            this.lblCovered.Font = new Font("Microsoft Sans Serif", 6.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.lblCovered.ForeColor = Color.Black;
            this.lblCovered.Location = new Point(0x1c8, 12);
            this.lblCovered.Name = "lblCovered";
            this.lblCovered.Size = new Size(0x42, 12);
            this.lblCovered.TabIndex = 13;
            this.lblCovered.Tag = "NoTheme";
            this.lblCovered.Text = "COVERED -";
            this.lblVacant.AutoSize = true;
            this.lblVacant.BackColor = Color.Transparent;
            this.lblVacant.Font = new Font("Microsoft Sans Serif", 6.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.lblVacant.ForeColor = Color.Green;
            this.lblVacant.Location = new Point(0x146, 12);
            this.lblVacant.Name = "lblVacant";
            this.lblVacant.Size = new Size(0x3a, 12);
            this.lblVacant.TabIndex = 12;
            this.lblVacant.Tag = "NoTheme";
            this.lblVacant.Text = "VACANT -";
            this.lblOccupied.AutoSize = true;
            this.lblOccupied.BackColor = Color.Transparent;
            this.lblOccupied.Font = new Font("Microsoft Sans Serif", 6.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.lblOccupied.ForeColor = Color.Orange;
            this.lblOccupied.Location = new Point(0xa6, 12);
            this.lblOccupied.Name = "lblOccupied";
            this.lblOccupied.Size = new Size(70, 12);
            this.lblOccupied.TabIndex = 11;
            this.lblOccupied.Tag = "NoTheme";
            this.lblOccupied.Text = "OCCUPIED -";
            this.lblNotActive.AutoSize = true;
            this.lblNotActive.BackColor = Color.Transparent;
            this.lblNotActive.Font = new Font("Microsoft Sans Serif", 6.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.lblNotActive.ForeColor = Color.Red;
            this.lblNotActive.Location = new Point(20, 12);
            this.lblNotActive.Name = "lblNotActive";
            this.lblNotActive.Size = new Size(0x4e, 12);
            this.lblNotActive.TabIndex = 10;
            this.lblNotActive.Tag = "NoTheme";
            this.lblNotActive.Text = "NOT ACTIVE -";
            //this.ImgTable.ImageStream = (ImageListStreamer)manager.GetObject("ImgTable.ImageStream");
            //this.ImgTable.TransparentColor = Color.Transparent;
           // this.ImgTable.Images.SetKeyName(0, "user5_(delete)_16x16.gif");
           // this.ImgTable.Images.SetKeyName(1, "user5_(add)_16x16.gif");
           // this.ImgTable.Images.SetKeyName(2, "user5_(edit)_16x16.gif");
            this.CntxtTable.Items.AddRange(new ToolStripItem[] { this.ShowOrderToolStripMenuItem, this.DeactivateTableToolStripMenuItem });
            this.CntxtTable.Name = "CntxtTable";
            this.CntxtTable.Size = new Size(0xa2, 0x30);
            this.ShowOrderToolStripMenuItem.DisplayStyle = ToolStripItemDisplayStyle.Text;
            this.ShowOrderToolStripMenuItem.Name = "ShowOrderToolStripMenuItem";
            this.ShowOrderToolStripMenuItem.Size = new Size(0xa1, 0x16);
            this.ShowOrderToolStripMenuItem.Text = "&New Order";
            this.ShowOrderToolStripMenuItem.Click += new EventHandler(this.ShowOrderToolStripMenuItem_Click);
            this.DeactivateTableToolStripMenuItem.CheckOnClick = true;
            this.DeactivateTableToolStripMenuItem.DisplayStyle = ToolStripItemDisplayStyle.Text;
            this.DeactivateTableToolStripMenuItem.Name = "DeactivateTableToolStripMenuItem";
            this.DeactivateTableToolStripMenuItem.Size = new Size(0xa1, 0x16);
            this.DeactivateTableToolStripMenuItem.Text = "&Deactivate Table";
            this.DeactivateTableToolStripMenuItem.Click += new EventHandler(this.DeactivateTableToolStripMenuItem_Click);
            this.pnlTableSettings.BorderStyle = BorderStyle.FixedSingle;
            this.pnlTableSettings.Controls.Add(this.btnOrdDiscount);
            this.pnlTableSettings.Controls.Add(this.CHKLock);
            this.pnlTableSettings.Dock = DockStyle.Top;
            this.pnlTableSettings.Location = new Point(0, 0);
            this.pnlTableSettings.Name = "pnlTableSettings";
            this.pnlTableSettings.Size = new Size(0x390, 0x33);
            this.pnlTableSettings.TabIndex = 2;
            this.pnlTableSettings.Paint += new PaintEventHandler(this.pnlTableSettings_Paint);
            this.btnOrdDiscount.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnOrdDiscount.Location = new Point(11, 11);
            this.btnOrdDiscount.Name = "btnOrdDiscount";
            this.btnOrdDiscount.Size = new Size(0x69, 0x1c);
            this.btnOrdDiscount.TabIndex = 11;
            this.btnOrdDiscount.Text = "Refresh Tables";
            this.btnOrdDiscount.TextAlign = ContentAlignment.MiddleLeft;
            this.btnOrdDiscount.UseVisualStyleBackColor = true;
            this.btnOrdDiscount.Click += new EventHandler(this.btnOrdDiscount_Click);
            this.CHKLock.AutoSize = true;
            this.CHKLock.Location = new Point(0x86, 0x16);
            this.CHKLock.Name = "CHKLock";
            this.CHKLock.Size = new Size(0x81, 0x11);
            this.CHKLock.TabIndex = 5;
            this.CHKLock.Text = "Lock Table Locations";
            this.CHKLock.UseVisualStyleBackColor = true;
            this.CHKLock.CheckedChanged += new EventHandler(this.CHKLock_CheckedChanged);
            this.pnlTables.BackColor = Color.White;
            this.pnlTables.BorderColor = Color.Gray;
            this.pnlTables.BorderStyle = BorderStyle.FixedSingle;
            this.pnlTables.Dock = DockStyle.Fill;
            this.pnlTables.GradientEndColor = Color.Transparent;
            this.pnlTables.GradientStartColor = Color.Transparent;
            this.pnlTables.HeaderTextFontSize = 10;
            this.pnlTables.Image = null;
            this.pnlTables.ImageLocation = new Point(4, 4);
            this.pnlTables.Location = new Point(0, 0x33);
            this.pnlTables.Name = "pnlTables";
            this.pnlTables.RoundCornerRadius = 2;
            this.pnlTables.ShadowOffSet = 0;
            this.pnlTables.Size = new Size(0x390, 0x1b6);
            this.pnlTables.TabIndex = 3;
            this.pnlTables.Tag = "NoTheme";
            this.timer1.Interval = 0x2710;
            this.timer1.Tick += new EventHandler(this.timer1_Tick);
            base.AutoScaleDimensions = new SizeF(6f, 13f);
//            base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x390, 0x20f);
            base.ControlBox = false;
            base.Controls.Add(this.pnlTables);
            base.Controls.Add(this.pnlTableSettings);
            base.Controls.Add(this.PnlStatus);
//            base.FormBorderStyle = FormBorderStyle.None;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "FrmTableLocation";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            base.StartPosition = FormStartPosition.CenterScreen;
            base.WindowState = FormWindowState.Maximized;
            base.Deactivate += new EventHandler(this.FrmTableLocation_Deactivate);
            base.Load += new EventHandler(this.FrmTableLocation_Load);
            base.Paint += new PaintEventHandler(this.FrmTableLocation_Paint);
            base.Activated += new EventHandler(this.FrmTableLocation_Activated);
            base.FormClosing += new FormClosingEventHandler(this.FrmTableLocation_FormClosing);
            this.PnlStatus.ResumeLayout(false);
            this.PnlStatus.PerformLayout();
            this.CntxtTable.ResumeLayout(false);
            this.pnlTableSettings.ResumeLayout(false);
            this.pnlTableSettings.PerformLayout();
            base.ResumeLayout(false);
        }
        #endregion
    }
}