﻿using ParaDine.Forms.Main;
using ParControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.GlobalForms
{
    public partial class FrmTableLocation : Form
    {
        private Button btnOrdDiscount;
        private CheckBox CHKLock;
        private Button clickBtn;
        internal ContextMenuStrip CntxtTable;
       // private IContainer components = null;
        private Point cordinate;
        internal ToolStripMenuItem DeactivateTableToolStripMenuItem;
        private DataSet ds = new DataSet();
        internal ImageList ImgTable;
        internal Label lblCovered;
        internal Label lblNotActive;
        internal Label lblOccupied;
        internal Label lblVacant;
        private LinkLabel linkbtn = new LinkLabel();
        private bool mdown;
        private Panel PnlStatus;
        private ParPanel pnlTables;
        private Panel pnlTableSettings;
        private SqlDataAdapter sda = new SqlDataAdapter();
        internal ToolStripMenuItem ShowOrderToolStripMenuItem;
        private SqlCommand sqlcmd = new SqlCommand();
        private Steward stew = new Steward();
        private string StrSql;
        private Button tablebtn = new Button();
        private int TableID;
        private Table tbl = new Table();
        private ParaDine.Layout TblLayout = new ParaDine.Layout();
        private TableLayoutcollection TblLayoutCollection = new TableLayoutcollection();
        private bool tblLock = false;
        private string tblName;
        private TableToSteward TblToSteward = new TableToSteward();
        private int tg;
        private Timer timer1;
        internal ToolTip ToolTip1;
        private double x;
        private double y;

        public FrmTableLocation()
        {
            this.InitializeComponent();
        }

        private void btnOrdDiscount_Click(object sender, EventArgs e)
        {
            this.pnlTables.Controls.Clear();
            this.CreateTables();
        }

        private void CHKLock_CheckedChanged(object sender, EventArgs e)
        {
            this.tblLock = this.CHKLock.Checked;
        }

        private void counttables()
        {
            int num = 0;
            foreach (Label label in this.PnlStatus.Controls)
            {
                if (label.Text.Contains("COVERED"))
                {
                    continue;
                }
                string str = "";
                foreach (char ch in label.Text)
                {
                    if (ch == '-')
                    {
                        break;
                    }
                    str = str + ch;
                }
                str = str + " - ";
                foreach (Button button in this.pnlTables.Controls)
                {
                    if (label.ForeColor.ToArgb() == button.BackColor.ToArgb())
                    {
                        num++;
                    }
                }
                label.Text = str + Convert.ToString(num);
                num = 0;
            }
        }

        public void CreateTables()
        {
            if (this.TblLayout.GetTableCount == 0)
            {
                this.DefaultLocation();
            }
            else
            {
                this.StrSql = " SELECT * FROM GLB_LAYOUT L  INNER JOIN RES_TABLE T ON T.TABLEID = L.SOURCEID AND L.SOURCETYPE = 'T'";
                GlobalFill.FillDataSet(this.StrSql, "GLB_LAYOUT", this.ds, this.sda);
                this.TblLayoutCollection.Clear();
                foreach (DataRow row in this.ds.Tables["GLB_LAYOUT"].Rows)
                {
                    this.TblLayout = new ParaDine.Layout();
                    this.TblLayout.SourceID = Convert.ToInt32(row["SOURCEID"]);
                    this.TblLayout.Xpos = Convert.ToInt32(row["XPOS"]);
                    this.TblLayout.Ypos = Convert.ToInt32(row["YPOS"]);
                    this.TblLayout.SourceType = Convert.ToChar(row["SOURCETYPE"]);
                    this.TblLayoutCollection.Add(this.TblLayout);
                }
                this.StrSql = "SELECT * FROM RES_TABLE WHERE TABLEID NOT IN (SELECT SOURCEID FROM GLB_LAYOUT WHERE SOURCETYPE = 'T')";
                GlobalFill.FillDataSet(this.StrSql, "TABLE", this.ds, this.sda);
                foreach (DataRow row in this.ds.Tables["TABLE"].Rows)
                {
                    this.TblLayout = new ParaDine.Layout();
                    this.TblLayout.SourceID = Convert.ToInt32(row["TABLEID"]);
                    this.TblLayout.Xpos = 10.0;
                    this.TblLayout.Ypos = 10.0;
                    this.TblLayout.SourceType = 'T';
                    this.TblLayoutCollection.Add(this.TblLayout);
                }
                this.LoadFields();
            }
            this.counttables();
        }

        private void DblClick(object sender, EventArgs e)
        {
            this.ShowOrderToolStripMenuItem_Click(sender, e);
        }

        private void DeactivateTableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.tbl.TableID = Convert.ToInt16(this.CntxtTable.SourceControl.Tag);
            this.tbl.TableStatus = this.DeactivateTableToolStripMenuItem.Checked;
            this.sqlcmd = new SqlCommand(this.StrSql, GlobalVariables.SqlConn);
            this.sqlcmd.ExecuteNonQuery();
            this.SetStatus();
            this.counttables();
        }

        public void DefaultLocation()
        {
            this.StrSql = "SELECT * FROM RES_TABLE ORDER BY TABLEID";
            GlobalFill.FillDataSet(this.StrSql, "DEFAULT", this.ds, this.sda);
            this.TblLayoutCollection.Clear();
            int count = this.ds.Tables["DEFAULT"].Rows.Count;
            if (count > 0)
            {
                this.pnlTables.Controls.Clear();
                double num3 = this.pnlTables.Width / 10;
                double num4 = ((this.pnlTables.Height * 6) / count) + 4;
                this.x = this.pnlTables.Location.X + 5;
                this.y = this.pnlTables.Location.Y + 5;
                int num2 = 0;
                for (double i = this.y; this.y <= this.pnlTables.Height; i += num4)
                {
                    this.cordinate.Y = Convert.ToInt16(this.y);
                    this.y += num4;
                    for (double j = this.x; j <= this.pnlTables.Width; j += num3)
                    {
                        if (num2 != count)
                        {
                            this.cordinate.X = Convert.ToInt16(this.x);
                            this.tblName = Convert.ToString(this.ds.Tables["DEFAULT"].Rows[num2][1]);
                            this.drawTables(Convert.ToInt16(this.ds.Tables["DEFAULT"].Rows[num2][0]), this.cordinate, this.tblName);
                            this.TblLayout = new ParaDine.Layout();
                            this.TblLayout.LayoutID = 0;
                            this.TblLayout.SourceID = Convert.ToInt16(this.ds.Tables["DEFAULT"].Rows[num2][0]);
                            this.TblLayout.SourceType = Convert.ToChar("T");
                            this.TblLayout.Xpos = this.cordinate.X;
                            this.TblLayout.Ypos = this.cordinate.Y;
                            this.TblLayoutCollection.Add(this.TblLayout);
                            this.x += num3;
                            num2++;
                        }
                    }
                    this.x = this.pnlTables.Location.X + 5;
                }
                this.SetStatus();
            }
        }

        private void drawlabel(int tg)
        {
            Table table = new Table(Convert.ToInt16(tg));
            int x = 50;
            foreach (DataRow row in table.ExistingOrdersList.Rows)
            {
                if (table.ExistingOrdersList.Rows.Count <= 4)
                {
                    this.linkbtn = new LinkLabel();
                    this.linkbtn.Text = "" + row[1].ToString().Trim() + "";
                    this.linkbtn.Tag = Convert.ToInt64(row[0]);
                    this.linkbtn.LinkClicked += new LinkLabelLinkClickedEventHandler(this.linklabelClicked);
                    this.linkbtn.TabIndex = tg;
                    this.linkbtn.BackColor = Color.Transparent;
                    this.linkbtn.ForeColor = Color.Blue;
                    this.linkbtn.Font = new Font("Microsoft Sans Serif", 7f);
                    this.linkbtn.Location = new Point(x, 0x1b);
                    x -= 15;
                    this.tablebtn.Controls.Add(this.linkbtn);
                }
            }
        }

        private void drawTables(int xi, Point loc, string tbltxt)
        {
            this.tg = xi;
            this.tablebtn = new Button();
            this.tablebtn.Name = "tablebtn" + this.tg;
            this.tablebtn.BackgroundImageLayout = ImageLayout.Stretch;
            this.setbuttonproperties(this.tablebtn);
            this.tablebtn.Tag = this.tg;
            if (tbltxt == "")
            {
                this.tablebtn.Text = "table" + this.tg;
            }
            else
            {
                this.tablebtn.Text = tbltxt;
                this.drawlabel(this.tg);
            }
            this.tablebtn.Location = loc;
            this.pnlTables.Controls.Add(this.tablebtn);
            this.handler(this.tablebtn);
            this.tablebtn = null;
        }

        private void FrmTableLocation_Activated(object sender, EventArgs e)
        {
        }

        private void FrmTableLocation_Deactivate(object sender, EventArgs e)
        {
            this.LoadLayoutEntities();
            SqlTransaction sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
            try
            {
                string cmdText = "DELETE FROM GLB_LAYOUT WHERE SOURCETYPE = 'T'";
                new SqlCommand(cmdText, GlobalVariables.SqlConn) { Transaction = sqlTrans }.ExecuteNonQuery();
                foreach (ParaDine.Layout layout in this.TblLayoutCollection)
                {
                    layout.Add(sqlTrans, false);
                }
                sqlTrans.Commit();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "ERROR IN SAVING LAYOUTS");
                sqlTrans.Rollback();
            }
        }

        private void FrmTableLocation_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                this.LoadLayoutEntities();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In Saving Positions");
                throw;
            }
        }

        private void FrmTableLocation_Load(object sender, EventArgs e)
        {
            this.CreateTables();
            this.tblLock = false;
            new GlobalTheme().applyTheme(this);
        }

        private void FrmTableLocation_Paint(object sender, PaintEventArgs e)
        {
        }

        private void GlbMenuItemClick(object sender, EventArgs e)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)sender;
            new FrmBilling(Convert.ToInt16(this.CntxtTable.SourceControl.Tag), Convert.ToInt64(item.Tag)) { MdiParent = base.MdiParent }.Show();
        }

        private void handler(Button btnname)
        {
            this.tablebtn.MouseDown += new MouseEventHandler(this.StartDrag);
            this.tablebtn.MouseMove += new MouseEventHandler(this.Moveit);
            this.tablebtn.MouseUp += new MouseEventHandler(this.mouserealese);
            this.tablebtn.DoubleClick += new EventHandler(this.DblClick);
        }

  
        private void linklabelClicked(object sender, EventArgs e)
        {
            LinkLabel label = (LinkLabel)sender;
            new FrmBilling(Convert.ToInt16(label.TabIndex), Convert.ToInt64(label.Tag)) { MdiParent = base.MdiParent }.Show();
        }

        private void lnklabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
        }

        private void LoadFields()
        {
            int num = 0;
            foreach (ParaDine.Layout layout in this.TblLayoutCollection)
            {
                Table table = new Table(layout.SourceID);
                string tbltxt = Convert.ToString(table.TableName + "\n\n");
                if (table.CoverStr != "")
                {
                    tbltxt = tbltxt + "(" + table.CoverStr + ")";
                }
                this.drawTables(layout.SourceID, new Point(Convert.ToInt16(layout.Xpos), Convert.ToInt16(layout.Ypos)), tbltxt);
                num += table.TotCover;
            }
            this.lblCovered.Text = "COVERED - " + num.ToString();
            this.SetStatus();
        }

        private void LoadLayoutEntities()
        {
            this.TblLayoutCollection.Clear();
            foreach (Button button in this.pnlTables.Controls)
            {
                this.TblLayout = new ParaDine.Layout();
                this.TblLayout.SourceID = Convert.ToInt16(button.Tag);
                this.TblLayout.Xpos = Convert.ToDouble(button.Left);
                this.TblLayout.Ypos = Convert.ToDouble(button.Top);
                this.TblLayout.SourceType = Convert.ToChar("T");
                this.TblLayoutCollection.Add(this.TblLayout);
            }
        }

        private void LoadTableToStewardEntities(int tblid, int stewardid, string transtype)
        {
            this.TblToSteward.TableID = tblid;
            this.TblToSteward.stewardID = stewardid;
            SqlTransaction sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
            try
            {
                if (transtype == "INSERT")
                {
                    this.TblToSteward.Add(sqlTrans, false);
                }
                else if (transtype == "DELETE")
                {
                    this.TblToSteward.Delete(sqlTrans, false);
                }
                sqlTrans.Commit();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                sqlTrans.Rollback();
            }
        }

        private void mouserealese(object sender, MouseEventArgs e)
        {
            this.mdown = false;
        }

        private void Moveit(object sender, MouseEventArgs e)
        {
            if (!(((e.Button != MouseButtons.Left) || !this.mdown) || this.tblLock))
            {
                this.cordinate.X = Convert.ToInt16((double)(this.clickBtn.Location.X + (Cursor.Position.X - this.x)));
                this.cordinate.Y = Convert.ToInt16((double)(this.clickBtn.Location.Y + (Cursor.Position.Y - this.y)));
                this.x = Cursor.Position.X;
                this.y = Cursor.Position.Y;
                this.clickBtn.Location = this.cordinate;
            }
        }

        private void pnlTables_Paint(object sender, PaintEventArgs e)
        {
        }

        private void pnlTableSettings_Paint(object sender, PaintEventArgs e)
        {
        }

        private void setbuttonproperties(Button btntbl)
        {
            btntbl.Height = 60;
            btntbl.Width = 70;
            btntbl.TextAlign = ContentAlignment.BottomCenter;
            btntbl.Cursor = Cursors.Hand;
            btntbl.ContextMenuStrip = this.CntxtTable;
            btntbl.FlatStyle = FlatStyle.Flat;
            btntbl.Font = this.lblNotActive.Font;
            btntbl.ImageList = this.ImgTable;
            btntbl.ImageAlign = ContentAlignment.TopCenter;
        }

        public void SetStatus()
        {
            foreach (Button button in this.pnlTables.Controls)
            {
                SqlCommand command = new SqlCommand("SELECT INACTIVE FROM RES_TABLE WHERE TABLEID=" + button.Tag, GlobalVariables.SqlConn);
                if (Convert.ToInt16(command.ExecuteScalar()) != 0)
                {
                    button.BackColor = this.lblNotActive.ForeColor;
                }
                else if (Convert.ToInt16(new SqlCommand(string.Concat(new object[] { "SELECT COUNT(*) FROM RES_CUSTORDERMASTER CM  WHERE SOURCETYPE = 'T' AND SOURCETYPEID = ", button.Tag, " AND CONVERT(DATETIME, CONVERT(VARCHAR, CUSTORDERDATE, 112)) = '", GlobalVariables.BusinessDate.ToString("dd/MMM/yy"), "' AND CM.CANCELLEDSTATUS = 0 AND CUSTORDERMASTERID NOT IN (SELECT TRANSACTIONSOURCEID FROM GLB_MONEYMASTER GM WHERE GM.TRANSACTIONSOURCE = 'BILL')" }), GlobalVariables.SqlConn).ExecuteScalar()) == 0)
                {
                    button.BackColor = this.lblVacant.ForeColor;
                    button.ForeColor = Color.White;
                }
                else
                {
                    button.BackColor = this.lblOccupied.ForeColor;
                    button.ImageIndex = 2;
                }
            }
        }

        private void ShowOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int tableID = this.tbl.TableID;
            this.StrSql = "SELECT INACTIVE FROM RES_TABLE WHERE TABLEID = " + tableID;
            GlobalFill.FillDataSet(this.StrSql, "INACTIVE", this.ds, this.sda);
            if (this.ds.Tables["INACTIVE"].Rows.Count > 0)
            {
                foreach (DataRow row in this.ds.Tables["INACTIVE"].Rows)
                {
                    if (!Convert.ToBoolean(row["INACTIVE"]))
                    {
                        new FrmBilling(this.tbl.TableID) { MdiParent = base.MdiParent }.Show();
                    }
                    else
                    {
                        MessageBox.Show("THIS TABLE IS INACTIVE , YOU CANNOT MAKE ORDER FOR THIS TABLE");
                    }
                }
            }
        }

        private void StartDrag(object sender, MouseEventArgs e)
        {
            this.clickBtn = (Button)sender;
            if (!((e.Button != MouseButtons.Left) || this.tblLock))
            {
                this.mdown = true;
                this.x = Cursor.Position.X;
                this.y = Cursor.Position.Y;
            }
            else if ((e.Button == MouseButtons.Right) && !this.tblLock)
            {
                this.CntxtTable.Items.Clear();
                ToolStripMenuItem item = new ToolStripMenuItem
                {
                    Text = "New Order",
                    ForeColor = Color.Black,
                    BackColor = Color.White,
                    Tag = 0
                };
                item.Click += new EventHandler(this.GlbMenuItemClick);
                this.CntxtTable.Items.Add(item);
                Table table = new Table(Convert.ToInt16(this.clickBtn.Tag));
                foreach (DataRow row in table.ExistingOrdersList.Rows)
                {
                    item = new ToolStripMenuItem
                    {
                        Text = "Show Order - (" + row[1].ToString().Trim() + ")",
                        ForeColor = Color.White,
                        BackColor = Color.Green,
                        Tag = Convert.ToInt64(row[0])
                    };
                    item.Click += new EventHandler(this.GlbMenuItemClick);
                    if (Convert.ToInt16(row["MACHINENO"].ToString()) != GlobalVariables.TerminalNo)
                    {
                        item.Enabled = false;
                    }
                    this.CntxtTable.Items.Add(item);
                }
                if (this.CntxtTable.Items.Count == 0)
                {
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
        }
    }
}
