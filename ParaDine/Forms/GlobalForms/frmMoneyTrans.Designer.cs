﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.GlobalForms
{
    partial class frmMoneyTrans
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.btnTrans = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.dtpTransDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTransSource = new System.Windows.Forms.TextBox();
            this.lvwMoneyTrans = new System.Windows.Forms.ListView();
            this.pnlTrans = new System.Windows.Forms.Panel();
            this.txtPaymentDetails = new System.Windows.Forms.TextBox();
            this.dtpPaymentDate = new System.Windows.Forms.DateTimePicker();
            this.txtPaymentNo = new System.Windows.Forms.TextBox();
            this.gvPaidAmount = new ParaSysCom.NumControl();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbPayment = new System.Windows.Forms.ComboBox();
            this.txtNarration = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pnlMaster = new System.Windows.Forms.Panel();
            this.gvPayableAmt = new ParaSysCom.NumControl();
            this.gvTotalAmt = new ParaSysCom.NumControl();
            this.pnlTrans.SuspendLayout();
            this.pnlMaster.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(296, 338);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 1;
            this.btnTrans.Text = "&Save";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(388, 338);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // dtpTransDate
            // 
            this.dtpTransDate.CustomFormat = "dd/MMM/yy";
            this.dtpTransDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTransDate.Location = new System.Drawing.Point(9, 21);
            this.dtpTransDate.Name = "dtpTransDate";
            this.dtpTransDate.Size = new System.Drawing.Size(125, 20);
            this.dtpTransDate.TabIndex = 0;
            this.dtpTransDate.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Transaction Date :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(156, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Source :";
            // 
            // txtTransSource
            // 
            this.txtTransSource.BackColor = System.Drawing.Color.White;
            this.txtTransSource.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTransSource.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTransSource.Location = new System.Drawing.Point(153, 21);
            this.txtTransSource.Name = "txtTransSource";
            this.txtTransSource.ReadOnly = true;
            this.txtTransSource.Size = new System.Drawing.Size(100, 20);
            this.txtTransSource.TabIndex = 1;
            this.txtTransSource.TabStop = false;
            // 
            // lvwMoneyTrans
            // 
            this.lvwMoneyTrans.BackColor = System.Drawing.Color.White;
            this.lvwMoneyTrans.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lvwMoneyTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvwMoneyTrans.ForeColor = System.Drawing.Color.Navy;
            this.lvwMoneyTrans.Location = new System.Drawing.Point(3, 47);
            this.lvwMoneyTrans.MultiSelect = false;
            this.lvwMoneyTrans.Name = "lvwMoneyTrans";
            this.lvwMoneyTrans.Size = new System.Drawing.Size(450, 115);
            this.lvwMoneyTrans.TabIndex = 3;
            this.lvwMoneyTrans.TabStop = false;
            this.lvwMoneyTrans.UseCompatibleStateImageBehavior = false;
            this.lvwMoneyTrans.DoubleClick += new System.EventHandler(this.lvwMoneyTrans_DoubleClick);
            this.lvwMoneyTrans.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lvwMoneyTrans_KeyDown);
            // 
            // pnlTrans
            // 
            this.pnlTrans.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTrans.Controls.Add(this.txtPaymentDetails);
            this.pnlTrans.Controls.Add(this.dtpPaymentDate);
            this.pnlTrans.Controls.Add(this.txtPaymentNo);
            this.pnlTrans.Controls.Add(this.gvPaidAmount);
            this.pnlTrans.Controls.Add(this.label6);
            this.pnlTrans.Controls.Add(this.label5);
            this.pnlTrans.Controls.Add(this.label4);
            this.pnlTrans.Controls.Add(this.label7);
            this.pnlTrans.Controls.Add(this.label3);
            this.pnlTrans.Controls.Add(this.cmbPayment);
            this.pnlTrans.Location = new System.Drawing.Point(3, 161);
            this.pnlTrans.Name = "pnlTrans";
            this.pnlTrans.Size = new System.Drawing.Size(450, 76);
            this.pnlTrans.TabIndex = 2;
            this.pnlTrans.TabStop = true;
            // 
            // txtPaymentDetails
            // 
            this.txtPaymentDetails.BackColor = System.Drawing.Color.White;
            this.txtPaymentDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPaymentDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaymentDetails.Location = new System.Drawing.Point(109, 49);
            this.txtPaymentDetails.Name = "txtPaymentDetails";
            this.txtPaymentDetails.Size = new System.Drawing.Size(327, 20);
            this.txtPaymentDetails.TabIndex = 4;
            this.txtPaymentDetails.Tag = "NoHandler";
            this.txtPaymentDetails.TextChanged += new System.EventHandler(this.txtPaymentDetails_TextChanged);
            this.txtPaymentDetails.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPaymentDetails_KeyDown);
            // 
            // dtpPaymentDate
            // 
            this.dtpPaymentDate.CustomFormat = "dd/MMM/yy";
            this.dtpPaymentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPaymentDate.Location = new System.Drawing.Point(344, 21);
            this.dtpPaymentDate.Name = "dtpPaymentDate";
            this.dtpPaymentDate.Size = new System.Drawing.Size(95, 20);
            this.dtpPaymentDate.TabIndex = 3;
            // 
            // txtPaymentNo
            // 
            this.txtPaymentNo.BackColor = System.Drawing.Color.White;
            this.txtPaymentNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPaymentNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaymentNo.Location = new System.Drawing.Point(200, 21);
            this.txtPaymentNo.Name = "txtPaymentNo";
            this.txtPaymentNo.Size = new System.Drawing.Size(135, 20);
            this.txtPaymentNo.TabIndex = 2;
            // 
            // gvPaidAmount
            // 
            this.gvPaidAmount.DecimalRequired = true;
            this.gvPaidAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvPaidAmount.Location = new System.Drawing.Point(116, 21);
            this.gvPaidAmount.Name = "gvPaidAmount";
            this.gvPaidAmount.Size = new System.Drawing.Size(75, 20);
            this.gvPaidAmount.SymbolRequired = true;
            this.gvPaidAmount.TabIndex = 1;
            this.gvPaidAmount.Text = "`0.00";
            this.gvPaidAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvPaidAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(348, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Payment Date :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(208, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Payment No.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(121, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Paid Amt :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(58, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Details :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Payment :";
            // 
            // cmbPayment
            // 
            this.cmbPayment.BackColor = System.Drawing.Color.Ivory;
            this.cmbPayment.FormattingEnabled = true;
            this.cmbPayment.Location = new System.Drawing.Point(9, 21);
            this.cmbPayment.Name = "cmbPayment";
            this.cmbPayment.Size = new System.Drawing.Size(98, 21);
            this.cmbPayment.TabIndex = 0;
            this.cmbPayment.SelectedIndexChanged += new System.EventHandler(this.cmbPayment_SelectedIndexChanged);
            this.cmbPayment.Leave += new System.EventHandler(this.cmbPayment_Leave);
            this.cmbPayment.Validated += new System.EventHandler(this.cmbPayment_Validated);
            // 
            // txtNarration
            // 
            this.txtNarration.BackColor = System.Drawing.Color.White;
            this.txtNarration.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNarration.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNarration.Location = new System.Drawing.Point(3, 259);
            this.txtNarration.Name = "txtNarration";
            this.txtNarration.Size = new System.Drawing.Size(450, 20);
            this.txtNarration.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 243);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Narration :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(247, 290);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Net Amount :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(329, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "Amount To Paid :";
            // 
            // pnlMaster
            // 
            this.pnlMaster.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMaster.Controls.Add(this.label10);
            this.pnlMaster.Controls.Add(this.gvPayableAmt);
            this.pnlMaster.Controls.Add(this.label9);
            this.pnlMaster.Controls.Add(this.gvTotalAmt);
            this.pnlMaster.Controls.Add(this.label8);
            this.pnlMaster.Controls.Add(this.txtNarration);
            this.pnlMaster.Controls.Add(this.pnlTrans);
            this.pnlMaster.Controls.Add(this.lvwMoneyTrans);
            this.pnlMaster.Controls.Add(this.txtTransSource);
            this.pnlMaster.Controls.Add(this.label2);
            this.pnlMaster.Controls.Add(this.label1);
            this.pnlMaster.Controls.Add(this.dtpTransDate);
            this.pnlMaster.Location = new System.Drawing.Point(5, 12);
            this.pnlMaster.Name = "pnlMaster";
            this.pnlMaster.Size = new System.Drawing.Size(458, 320);
            this.pnlMaster.TabIndex = 0;
            // 
            // gvPayableAmt
            // 
            this.gvPayableAmt.DecimalRequired = true;
            this.gvPayableAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvPayableAmt.Location = new System.Drawing.Point(317, 18);
            this.gvPayableAmt.Name = "gvPayableAmt";
            this.gvPayableAmt.Size = new System.Drawing.Size(133, 20);
            this.gvPayableAmt.SymbolRequired = true;
            this.gvPayableAmt.TabIndex = 11;
            this.gvPayableAmt.TabStop = false;
            this.gvPayableAmt.Text = "`0.00";
            this.gvPayableAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvPayableAmt.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // gvTotalAmt
            // 
            this.gvTotalAmt.DecimalRequired = true;
            this.gvTotalAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvTotalAmt.Location = new System.Drawing.Point(332, 285);
            this.gvTotalAmt.Name = "gvTotalAmt";
            this.gvTotalAmt.Size = new System.Drawing.Size(121, 20);
            this.gvTotalAmt.SymbolRequired = true;
            this.gvTotalAmt.TabIndex = 9;
            this.gvTotalAmt.TabStop = false;
            this.gvTotalAmt.Text = "`0.00";
            this.gvTotalAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvTotalAmt.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // frmMoneyTrans
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(468, 373);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnTrans);
            this.Controls.Add(this.pnlMaster);
            this.Name = "frmMoneyTrans";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MoneyTrans";
            this.TTip.SetToolTip(this, "Select Payment Mode");
            this.Load += new System.EventHandler(this.frmMoneyTrans_Load);
            this.pnlTrans.ResumeLayout(false);
            this.pnlTrans.PerformLayout();
            this.pnlMaster.ResumeLayout(false);
            this.pnlMaster.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}