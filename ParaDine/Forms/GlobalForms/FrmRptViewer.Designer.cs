﻿
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.GlobalForms
{
    partial class FrmRptViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(FrmRptViewer));
            //this.crystalReportViewer1 = new CrystalReportViewer();
            //base.SuspendLayout();
            //this.crystalReportViewer1.ActiveViewIndex = -1;
            //this.crystalReportViewer1.BorderStyle = BorderStyle.FixedSingle;
            ////  this.crystalReportViewer1.DisplayGroupTree = false;
            //this.crystalReportViewer1.Dock = DockStyle.Fill;
            //this.crystalReportViewer1.Location = new Point(0, 0);
            //this.crystalReportViewer1.Name = "crystalReportViewer1";
            //this.crystalReportViewer1.SelectionFormula = "";
            //this.crystalReportViewer1.Size = new Size(0x3ab, 0x1d5);
            //this.crystalReportViewer1.TabIndex = 0;
            //this.crystalReportViewer1.ViewTimeSelectionFormula = "";
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            //            base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x3ab, 0x1d5);
            //base.Controls.Add(this.crystalReportViewer1);
            //            base.Icon = (Icon) manager.GetObject("$this.Icon");
            base.Name = "FrmRptViewer";
            base.ShowInTaskbar = false;
            base.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "Report Viewer";
            base.WindowState = FormWindowState.Maximized;
            base.ResumeLayout(false);
        }
        #endregion
    }
}