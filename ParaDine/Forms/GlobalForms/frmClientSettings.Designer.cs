﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.GlobalForms
{
    partial class frmClientSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlClientSettings = new System.Windows.Forms.Panel();
            this.chkNegativeBill = new System.Windows.Forms.CheckBox();
            this.cmbBillPrintingDevice = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.ChkKotStubReq = new System.Windows.Forms.CheckBox();
            this.cmbKOTPrintingDevice = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbBackEnd = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbTerminalNo = new System.Windows.Forms.ComboBox();
            this.chkStubReq = new System.Windows.Forms.CheckBox();
            this.cmbPoleDisplay = new System.Windows.Forms.ComboBox();
            this.cmbFinancialYear = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.chkPoleDisplay = new System.Windows.Forms.CheckBox();
            this.cmbServerName = new System.Windows.Forms.ComboBox();
            this.cmbBillPrinter = new System.Windows.Forms.ComboBox();
            this.cmbKOTPrinter = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.pnlClientSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlClientSettings
            // 
            this.pnlClientSettings.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlClientSettings.Controls.Add(this.chkNegativeBill);
            this.pnlClientSettings.Controls.Add(this.cmbBillPrintingDevice);
            this.pnlClientSettings.Controls.Add(this.label9);
            this.pnlClientSettings.Controls.Add(this.ChkKotStubReq);
            this.pnlClientSettings.Controls.Add(this.cmbKOTPrintingDevice);
            this.pnlClientSettings.Controls.Add(this.label7);
            this.pnlClientSettings.Controls.Add(this.cmbBackEnd);
            this.pnlClientSettings.Controls.Add(this.label6);
            this.pnlClientSettings.Controls.Add(this.cmbTerminalNo);
            this.pnlClientSettings.Controls.Add(this.chkStubReq);
            this.pnlClientSettings.Controls.Add(this.cmbPoleDisplay);
            this.pnlClientSettings.Controls.Add(this.cmbFinancialYear);
            this.pnlClientSettings.Controls.Add(this.label8);
            this.pnlClientSettings.Controls.Add(this.txtDatabase);
            this.pnlClientSettings.Controls.Add(this.chkPoleDisplay);
            this.pnlClientSettings.Controls.Add(this.cmbServerName);
            this.pnlClientSettings.Controls.Add(this.cmbBillPrinter);
            this.pnlClientSettings.Controls.Add(this.cmbKOTPrinter);
            this.pnlClientSettings.Controls.Add(this.label5);
            this.pnlClientSettings.Controls.Add(this.label4);
            this.pnlClientSettings.Controls.Add(this.label3);
            this.pnlClientSettings.Controls.Add(this.label2);
            this.pnlClientSettings.Controls.Add(this.label1);
            this.pnlClientSettings.Location = new System.Drawing.Point(8, 12);
            this.pnlClientSettings.Name = "pnlClientSettings";
            this.pnlClientSettings.Size = new System.Drawing.Size(300, 367);
            this.pnlClientSettings.TabIndex = 0;
            // 
            // chkNegativeBill
            // 
            this.chkNegativeBill.AutoSize = true;
            this.chkNegativeBill.Location = new System.Drawing.Point(102, 340);
            this.chkNegativeBill.Name = "chkNegativeBill";
            this.chkNegativeBill.Size = new System.Drawing.Size(134, 17);
            this.chkNegativeBill.TabIndex = 20;
            this.chkNegativeBill.Text = "Negative Qty Required";
            this.chkNegativeBill.UseVisualStyleBackColor = true;
            // 
            // cmbBillPrintingDevice
            // 
            this.cmbBillPrintingDevice.FormattingEnabled = true;
            this.cmbBillPrintingDevice.Location = new System.Drawing.Point(111, 150);
            this.cmbBillPrintingDevice.Name = "cmbBillPrintingDevice";
            this.cmbBillPrintingDevice.Size = new System.Drawing.Size(177, 21);
            this.cmbBillPrintingDevice.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(2, 153);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Bill Printing Device :";
            // 
            // ChkKotStubReq
            // 
            this.ChkKotStubReq.AutoSize = true;
            this.ChkKotStubReq.Location = new System.Drawing.Point(102, 317);
            this.ChkKotStubReq.Name = "ChkKotStubReq";
            this.ChkKotStubReq.Size = new System.Drawing.Size(113, 17);
            this.ChkKotStubReq.TabIndex = 11;
            this.ChkKotStubReq.Text = "Kot Stub Required";
            this.ChkKotStubReq.UseVisualStyleBackColor = true;
            // 
            // cmbKOTPrintingDevice
            // 
            this.cmbKOTPrintingDevice.FormattingEnabled = true;
            this.cmbKOTPrintingDevice.Location = new System.Drawing.Point(111, 122);
            this.cmbKOTPrintingDevice.Name = "cmbKOTPrintingDevice";
            this.cmbKOTPrintingDevice.Size = new System.Drawing.Size(177, 21);
            this.cmbKOTPrintingDevice.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(2, 125);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "KOT Printing Device :";
            // 
            // cmbBackEnd
            // 
            this.cmbBackEnd.FormattingEnabled = true;
            this.cmbBackEnd.Location = new System.Drawing.Point(111, 10);
            this.cmbBackEnd.Name = "cmbBackEnd";
            this.cmbBackEnd.Size = new System.Drawing.Size(177, 21);
            this.cmbBackEnd.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(43, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Back End :";
            // 
            // cmbTerminalNo
            // 
            this.cmbTerminalNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTerminalNo.FormattingEnabled = true;
            this.cmbTerminalNo.Location = new System.Drawing.Point(111, 260);
            this.cmbTerminalNo.Name = "cmbTerminalNo";
            this.cmbTerminalNo.Size = new System.Drawing.Size(121, 21);
            this.cmbTerminalNo.TabIndex = 9;
            // 
            // chkStubReq
            // 
            this.chkStubReq.AutoSize = true;
            this.chkStubReq.Location = new System.Drawing.Point(102, 291);
            this.chkStubReq.Name = "chkStubReq";
            this.chkStubReq.Size = new System.Drawing.Size(94, 17);
            this.chkStubReq.TabIndex = 10;
            this.chkStubReq.Text = "Stub Required";
            this.chkStubReq.UseVisualStyleBackColor = true;
            // 
            // cmbPoleDisplay
            // 
            this.cmbPoleDisplay.FormattingEnabled = true;
            this.cmbPoleDisplay.Location = new System.Drawing.Point(111, 207);
            this.cmbPoleDisplay.Name = "cmbPoleDisplay";
            this.cmbPoleDisplay.Size = new System.Drawing.Size(121, 21);
            this.cmbPoleDisplay.TabIndex = 7;
            // 
            // cmbFinancialYear
            // 
            this.cmbFinancialYear.FormattingEnabled = true;
            this.cmbFinancialYear.Location = new System.Drawing.Point(111, 234);
            this.cmbFinancialYear.Name = "cmbFinancialYear";
            this.cmbFinancialYear.Size = new System.Drawing.Size(121, 21);
            this.cmbFinancialYear.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(23, 237);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Financial Year :";
            // 
            // txtDatabase
            // 
            this.txtDatabase.Location = new System.Drawing.Point(111, 179);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(177, 20);
            this.txtDatabase.TabIndex = 6;
            this.txtDatabase.Leave += new System.EventHandler(this.txtDatabase_Leave);
            // 
            // chkPoleDisplay
            // 
            this.chkPoleDisplay.AutoSize = true;
            this.chkPoleDisplay.Location = new System.Drawing.Point(19, 209);
            this.chkPoleDisplay.Name = "chkPoleDisplay";
            this.chkPoleDisplay.Size = new System.Drawing.Size(84, 17);
            this.chkPoleDisplay.TabIndex = 6;
            this.chkPoleDisplay.Text = "Pole Display";
            this.chkPoleDisplay.UseVisualStyleBackColor = true;
            this.chkPoleDisplay.CheckedChanged += new System.EventHandler(this.chkPoleDisplay_CheckedChanged);
            // 
            // cmbServerName
            // 
            this.cmbServerName.FormattingEnabled = true;
            this.cmbServerName.Location = new System.Drawing.Point(111, 37);
            this.cmbServerName.Name = "cmbServerName";
            this.cmbServerName.Size = new System.Drawing.Size(177, 21);
            this.cmbServerName.TabIndex = 1;
            // 
            // cmbBillPrinter
            // 
            this.cmbBillPrinter.FormattingEnabled = true;
            this.cmbBillPrinter.Location = new System.Drawing.Point(111, 94);
            this.cmbBillPrinter.Name = "cmbBillPrinter";
            this.cmbBillPrinter.Size = new System.Drawing.Size(177, 21);
            this.cmbBillPrinter.TabIndex = 3;
            // 
            // cmbKOTPrinter
            // 
            this.cmbKOTPrinter.FormattingEnabled = true;
            this.cmbKOTPrinter.Location = new System.Drawing.Point(111, 66);
            this.cmbKOTPrinter.Name = "cmbKOTPrinter";
            this.cmbKOTPrinter.Size = new System.Drawing.Size(177, 21);
            this.cmbKOTPrinter.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(43, 182);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "DataBase :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(44, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Bill Printer :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(35, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "KOT Printer :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(50, 264);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Terminal :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Server Name :";
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(156, 391);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(77, 27);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnApply
            // 
            this.btnApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApply.Location = new System.Drawing.Point(73, 391);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(77, 27);
            this.btnApply.TabIndex = 1;
            this.btnApply.Text = "&Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // frmClientSettings
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(316, 429);
            this.Controls.Add(this.pnlClientSettings);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnApply);
            this.Name = "frmClientSettings";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Client Settings";
            this.Load += new System.EventHandler(this.frmClientSettings_Load);
            this.pnlClientSettings.ResumeLayout(false);
            this.pnlClientSettings.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}