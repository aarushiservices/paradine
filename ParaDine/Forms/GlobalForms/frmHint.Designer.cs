﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.GlobalForms
{
    partial class frmHint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new Container();
            this.pnlHint = new Panel();
            this.btnExit = new Button();
            this.btnTrans = new Button();
            this.label1 = new Label();
            this.txtHint = new TextBox();
            this.TTip = new ToolTip(this.components);
            this.pnlHint.SuspendLayout();
            base.SuspendLayout();
            this.pnlHint.BorderStyle = BorderStyle.FixedSingle;
            this.pnlHint.Controls.Add(this.btnExit);
            this.pnlHint.Controls.Add(this.btnTrans);
            this.pnlHint.Controls.Add(this.label1);
            this.pnlHint.Controls.Add(this.txtHint);
            this.pnlHint.Dock = DockStyle.Fill;
            this.pnlHint.Location = new Point(0, 0);
            this.pnlHint.Name = "pnlHint";
            this.pnlHint.Size = new Size(240, 0x6b);
            this.pnlHint.TabIndex = 4;
            this.btnExit.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnExit.Location = new Point(0x81, 0x3e);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new Size(0x4b, 0x17);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new EventHandler(this.btnExit_Click);
            this.btnTrans.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnTrans.Location = new Point(0x24, 0x3e);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new Size(0x4b, 0x17);
            this.btnTrans.TabIndex = 2;
            this.btnTrans.Text = "Mode";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new EventHandler(this.btnTrans_Click);
            this.label1.AutoSize = true;
            this.label1.Location = new Point(20, 0x18);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x20, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Hint :";
            this.txtHint.BackColor = Color.Ivory;
            this.txtHint.BorderStyle = BorderStyle.FixedSingle;
            this.txtHint.CharacterCasing = CharacterCasing.Upper;
            this.txtHint.Location = new Point(0x44, 20);
            this.txtHint.Name = "txtHint";
            this.txtHint.Size = new Size(150, 20);
            this.txtHint.TabIndex = 0;
            base.AutoScaleDimensions = new SizeF(6f, 13f);
          //base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(240, 0x6b);
            base.Controls.Add(this.pnlHint);
            base.Name = "frmHint";
            base.ShowIcon = false;
            base.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "Hint";
            base.Load += new EventHandler(this.frmHint_Load);
            this.pnlHint.ResumeLayout(false);
            this.pnlHint.PerformLayout();
            base.ResumeLayout(false);
        }
        #endregion
    }
}