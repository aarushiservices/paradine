﻿using SysSecCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace ParaDine.Forms.GlobalForms
{
    public partial class frmLogIn : Form
    {
        private Button btnExit;
        private Button btnLogIn;
        private Button btnPwd;
        private ComboBox cmbHint;
       //private IContainer components = null;
        private FlowLayoutPanel flPnlHint;
        private ImageList imageList1;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label lblFrgtPsw;
        private Panel panel1;
        private PictureBox pictureBox1;
        private PictureBox pictureBox2;
        private Panel pnlLogin;
        private Timer timer1;
        private ToolTip TTip;
        private TextBox txtAns;
        private TextBox txtPassword;
        private TextBox txtUsrName;
        private User varUser = new User();

        public frmLogIn()
        {
            this.InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Dispose();
            base.Close();
        }

        private void btnLogIn_Click(object sender, EventArgs e)
        {
            try
            {
                if (GlobalValidations.ValidateFields(this.pnlLogin, this.TTip))
                {
                    this.varUser = new User(this.txtUsrName.Text);
                    if (this.varUser.UserId > 0)
                    {
                        if (this.varUser.Password.ToUpper() != this.txtPassword.Text.ToUpper())
                        {
                            this.cmbHint.SelectedValue = this.varUser.HintId;
                            throw new Exception("You've entered an incorrect password \nUse Forgot Password Link.....");
                        }
                        GlobalVariables.UserID = Convert.ToInt16(this.varUser.UserId);
                        new frmMDIRest().Show();
                        base.Hide();
                    }
                    else
                    {
                        this.txtUsrName.Focus();
                        this.txtUsrName.SelectAll();
                        throw new Exception("UserName Not Found");
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "LogIn");
            }
        }

        private void btnPwd_Click(object sender, EventArgs e)
        {
            if ((this.varUser.HintId == Convert.ToInt32(this.cmbHint.SelectedValue)) && (this.varUser.HintAns.ToUpper() == this.txtAns.Text.ToUpper()))
            {
                MessageBox.Show("Password is " + this.varUser.Password);
            }
            else
            {
                MessageBox.Show("Sorry................ \nU Cannot Login");
                base.Close();
            }
        }

        private void DataBaseCheck()
        {
            Exception exception;
            try
            {
                string cmdText = "";
                SqlCommand command = null;
                double num = 0.0;
                cmdText = "SELECT TOP 1 VERSIONNO, STOPPREVIOUSVERSION FROM GLB_SYSTEMSETTINGS";
                command = new SqlCommand(cmdText, GlobalVariables.SqlConn);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    num = Convert.ToDouble(reader["VERSIONNO"]);
                    GlobalVariables.StopPrevVersion = Convert.ToBoolean(reader["STOPPREVIOUSVERSION"]);
                }
                reader.Close();
                if ((num > GlobalVariables.VersionNo) & GlobalVariables.StopPrevVersion)
                {
                    MessageBox.Show("Version Changed Cannot Open");
                    Environment.Exit(0);
                }
                if (GlobalVariables.VersionNo > num)
                {
                    GlobalVariables.StopPrevVersion = false;
                    cmdText = "UPDATE GLB_SYSTEMSETTINGS SET STOPPREVIOUSVERSION = 0";
                    command = new SqlCommand(cmdText, GlobalVariables.SqlConn);
                    command.ExecuteNonQuery();
                }
                if (!GlobalVariables.StopPrevVersion)
                {
                    for (int i = Convert.ToInt32(num); i <= Convert.ToInt32(GlobalVariables.VersionNo); i++)
                    {
                        string str2 = ".DSV" + i.ToString() + ".xml";
                        string name = Assembly.GetExecutingAssembly().GetName().Name + ".DSV" + str2;
                        using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(name))
                        {
                            XmlReader reader2 = XmlReader.Create(stream);
                            DataSet set = new DataSet();
                            set.ReadXml(reader2);
                            foreach (DataRow row in set.Tables["DBASEUPDATE"].Rows)
                            {
                                command = new SqlCommand(string.Concat(new object[] { "SELECT * FROM GLB_DBASEUPDATE WHERE SL = ", row["SL"], " AND VERSIONNO = ", i.ToString() }), GlobalVariables.SqlConn);
                                SqlDataReader reader3 = command.ExecuteReader();
                                if (!reader3.Read())
                                {
                                    reader3.Close();
                                    try
                                    {
                                        command = new SqlCommand(Convert.ToString(row["QUERY"]), GlobalVariables.SqlConn);
                                        command.ExecuteNonQuery();
                                        command = new SqlCommand(string.Concat(new object[] { "INSERT INTO GLB_DBASEUPDATE (VERSIONNO, SL) VALUES (", i.ToString(), ",", row["SL"], ")" }), GlobalVariables.SqlConn);
                                        command.ExecuteNonQuery();
                                    }
                                    catch (Exception exception1)
                                    {
                                        exception = exception1;
                                        MessageBox.Show(string.Concat(new object[] { "Error While Executing Query SL ", row["SL"], ". ", row["QUERY"], "\n", exception.Message }), "Error in DatabaseCheck Loop");
                                        if (MessageBox.Show("Do U Want To Continue ???", "Error in DatabaseCheck Loop", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.Yes)
                                        {
                                            continue;
                                        }
                                    }
                                }
                                else
                                {
                                    reader3.Close();
                                }
                            }
                        }
                    }
                    if (MessageBox.Show("Do u Want to Commit this Version and Stop Previous Version", "Database Check", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        new SqlCommand("UPDATE GLB_SYSTEMSETTINGS SET VERSIONNO = " + GlobalVariables.VersionNo.ToString(), GlobalVariables.SqlConn).ExecuteNonQuery();
                        cmdText = "UPDATE GLB_SYSTEMSETTINGS SET STOPPREVIOUSVERSION = 1";
                        GlobalFunctions.GetExecuteQuery(cmdText);
                        MessageBox.Show("Previous Version Stopped", "Database Check", MessageBoxButtons.OK);
                    }
                }
            }
            catch (Exception exception2)
            {
                exception = exception2;
                MessageBox.Show(exception.Message, "Error in DatabaseCheck", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                Environment.Exit(0);
            }
        }      
        private void frmLogIn_Load(object sender, EventArgs e)
        {
            try
            {
                frmSplash splash = new frmSplash();
                splash.Show();
                splash.Refresh();
                GlobalFunctions.AssignClientSettings();
                GlobalFunctions.GetConnection();
                if (!SystemSecurity.VerifyLicense(SystemSecurity.SoftWares.ParaDine))
                {
                    new frmTimer().Show();
                }
                this.DataBaseCheck();
                splash.Close();
                GlobalFunctions.AddCompHandler(this.pnlLogin);
                this.RefreshData();
                if ((GlobalVariables.MobileNoStr != "") && Sms.IsConnectionAvailable())
                {
                    Sms.SendSMSQueue();
                }
                this.timer1.Start();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Loading Form");
            }
        }
        private void label3_Click(object sender, EventArgs e)
        {
        }

        private void lblFrgtPsw_Click(object sender, EventArgs e)
        {
            int num;
            this.flPnlHint.Visible = !this.flPnlHint.Visible;
            if (this.flPnlHint.Visible)
            {
                for (num = 0xae; num < 250; num++)
                {
                    this.flPnlHint.Height++;
                    base.Height++;
                    this.Refresh();
                }
                this.txtAns.Focus();
                this.txtUsrName.SelectAll();
            }
            else
            {
                for (num = 250; num > 0xae; num--)
                {
                    this.flPnlHint.Height--;
                    base.Height--;
                    this.Refresh();
                }
                this.txtUsrName.Focus();
                this.txtUsrName.SelectAll();
            }
        }

        private void RefreshData()
        {
            try
            {
                GlobalFill.FillCombo("SELECT HINTID, HINT FROM GLB_HINT", this.cmbHint);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Refresh Data");
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.pictureBox1.Visible)
            {
                this.pictureBox2.Visible = true;
                this.pictureBox1.Visible = false;
            }
            else
            {
                this.pictureBox1.Visible = true;
                this.pictureBox2.Visible = false;
            }
        }
    }
}
