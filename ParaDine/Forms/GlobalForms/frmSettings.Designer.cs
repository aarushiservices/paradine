﻿using ParaSysCom;
using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.GlobalForms
{
    partial class frmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpfields = new System.Windows.Forms.Panel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtKOTBillHeader = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtDirectBillHeader = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.chkDw1 = new System.Windows.Forms.CheckBox();
            this.txtBillHdr1 = new System.Windows.Forms.TextBox();
            this.txtBillHdr2 = new System.Windows.Forms.TextBox();
            this.txtBillHdr3 = new System.Windows.Forms.TextBox();
            this.txtBillHdr4 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.chkDw2 = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chkDw3 = new System.Windows.Forms.CheckBox();
            this.chkDw4 = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.gvServiceCharge = new ParaSysCom.NumControl();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gvBillTax = new ParaSysCom.NumControl();
            this.chkBillTaxIncl = new System.Windows.Forms.CheckBox();
            this.txtBackupPath = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.chkAllTaxesIncl = new System.Windows.Forms.CheckBox();
            this.chkMouseMode = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtBillPrefix = new System.Windows.Forms.TextBox();
            this.gvCatViewHeight = new ParaSysCom.NumControl();
            this.chkserviceTaxOnDirectBilling = new System.Windows.Forms.CheckBox();
            this.gvPurgngPeriod = new ParaSysCom.NumControl();
            this.label8 = new System.Windows.Forms.Label();
            this.chkPurgingReq = new System.Windows.Forms.CheckBox();
            this.chkRefund = new System.Windows.Forms.CheckBox();
            this.ChkHiding = new System.Windows.Forms.CheckBox();
            this.ChkAmtTendReq = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rtbStubNRA = new System.Windows.Forms.RadioButton();
            this.rtbStubRNP = new System.Windows.Forms.RadioButton();
            this.rtbStubRP = new System.Windows.Forms.RadioButton();
            this.rtbStubRA = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkrefKotnoUnique = new System.Windows.Forms.CheckBox();
            this.chkRefKOTNoMan = new System.Windows.Forms.CheckBox();
            this.chkCoveredNoman = new System.Windows.Forms.CheckBox();
            this.chkKOTPrinting = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rtbOneRupee = new System.Windows.Forms.RadioButton();
            this.rtb50Paise = new System.Windows.Forms.RadioButton();
            this.grpBillNo = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtResetChar = new System.Windows.Forms.TextBox();
            this.chkResetTerminal = new System.Windows.Forms.CheckBox();
            this.gvrefreshAfter = new ParaSysCom.NumControl();
            this.rdbAfterbills = new System.Windows.Forms.RadioButton();
            this.rdbEveryDay = new System.Windows.Forms.RadioButton();
            this.rdbEveryMonth = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbItemView = new System.Windows.Forms.ComboBox();
            this.cmbCatgView = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.grpfields.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpBillNo.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpfields
            // 
            this.grpfields.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.grpfields.Controls.Add(this.groupBox6);
            this.grpfields.Controls.Add(this.groupBox5);
            this.grpfields.Controls.Add(this.groupBox4);
            this.grpfields.Controls.Add(this.groupBox3);
            this.grpfields.Controls.Add(this.groupBox2);
            this.grpfields.Controls.Add(this.groupBox1);
            this.grpfields.Controls.Add(this.grpBillNo);
            this.grpfields.Location = new System.Drawing.Point(3, 4);
            this.grpfields.Name = "grpfields";
            this.grpfields.Size = new System.Drawing.Size(438, 557);
            this.grpfields.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtKOTBillHeader);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.txtDirectBillHeader);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.chkDw1);
            this.groupBox6.Controls.Add(this.txtBillHdr1);
            this.groupBox6.Controls.Add(this.txtBillHdr2);
            this.groupBox6.Controls.Add(this.txtBillHdr3);
            this.groupBox6.Controls.Add(this.txtBillHdr4);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this.chkDw2);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Controls.Add(this.chkDw3);
            this.groupBox6.Controls.Add(this.chkDw4);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Location = new System.Drawing.Point(8, 381);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(419, 169);
            this.groupBox6.TabIndex = 15;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Bill Format";
            // 
            // txtKOTBillHeader
            // 
            this.txtKOTBillHeader.Location = new System.Drawing.Point(111, 143);
            this.txtKOTBillHeader.Name = "txtKOTBillHeader";
            this.txtKOTBillHeader.Size = new System.Drawing.Size(189, 20);
            this.txtKOTBillHeader.TabIndex = 41;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 146);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 13);
            this.label12.TabIndex = 42;
            this.label12.Text = "KOT Bill Header :";
            // 
            // txtDirectBillHeader
            // 
            this.txtDirectBillHeader.Location = new System.Drawing.Point(111, 117);
            this.txtDirectBillHeader.Name = "txtDirectBillHeader";
            this.txtDirectBillHeader.Size = new System.Drawing.Size(189, 20);
            this.txtDirectBillHeader.TabIndex = 39;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 120);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(95, 13);
            this.label11.TabIndex = 40;
            this.label11.Text = "Direct Bill Header :";
            // 
            // chkDw1
            // 
            this.chkDw1.AutoSize = true;
            this.chkDw1.Location = new System.Drawing.Point(305, 21);
            this.chkDw1.Name = "chkDw1";
            this.chkDw1.Size = new System.Drawing.Size(99, 17);
            this.chkDw1.TabIndex = 16;
            this.chkDw1.Text = "Dbl. Width Req";
            this.chkDw1.UseVisualStyleBackColor = true;
            // 
            // txtBillHdr1
            // 
            this.txtBillHdr1.Location = new System.Drawing.Point(87, 19);
            this.txtBillHdr1.Name = "txtBillHdr1";
            this.txtBillHdr1.Size = new System.Drawing.Size(212, 20);
            this.txtBillHdr1.TabIndex = 15;
            // 
            // txtBillHdr2
            // 
            this.txtBillHdr2.Location = new System.Drawing.Point(87, 43);
            this.txtBillHdr2.Name = "txtBillHdr2";
            this.txtBillHdr2.Size = new System.Drawing.Size(212, 20);
            this.txtBillHdr2.TabIndex = 17;
            // 
            // txtBillHdr3
            // 
            this.txtBillHdr3.Location = new System.Drawing.Point(87, 67);
            this.txtBillHdr3.Name = "txtBillHdr3";
            this.txtBillHdr3.Size = new System.Drawing.Size(212, 20);
            this.txtBillHdr3.TabIndex = 19;
            // 
            // txtBillHdr4
            // 
            this.txtBillHdr4.Location = new System.Drawing.Point(87, 91);
            this.txtBillHdr4.Name = "txtBillHdr4";
            this.txtBillHdr4.Size = new System.Drawing.Size(212, 20);
            this.txtBillHdr4.TabIndex = 21;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 94);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 13);
            this.label7.TabIndex = 38;
            this.label7.Text = "Bill Header4 :";
            // 
            // chkDw2
            // 
            this.chkDw2.AutoSize = true;
            this.chkDw2.Location = new System.Drawing.Point(305, 45);
            this.chkDw2.Name = "chkDw2";
            this.chkDw2.Size = new System.Drawing.Size(99, 17);
            this.chkDw2.TabIndex = 18;
            this.chkDw2.Text = "Dbl. Width Req";
            this.chkDw2.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 70);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 37;
            this.label6.Text = "Bill Header3 :";
            // 
            // chkDw3
            // 
            this.chkDw3.AutoSize = true;
            this.chkDw3.Location = new System.Drawing.Point(305, 69);
            this.chkDw3.Name = "chkDw3";
            this.chkDw3.Size = new System.Drawing.Size(99, 17);
            this.chkDw3.TabIndex = 20;
            this.chkDw3.Text = "Dbl. Width Req";
            this.chkDw3.UseVisualStyleBackColor = true;
            // 
            // chkDw4
            // 
            this.chkDw4.AutoSize = true;
            this.chkDw4.Location = new System.Drawing.Point(305, 93);
            this.chkDw4.Name = "chkDw4";
            this.chkDw4.Size = new System.Drawing.Size(99, 17);
            this.chkDw4.TabIndex = 22;
            this.chkDw4.Text = "Dbl. Width Req";
            this.chkDw4.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 36;
            this.label5.Text = "Bill Header2 :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 35;
            this.label4.Text = "Bill Header1 :";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.gvServiceCharge);
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.gvBillTax);
            this.groupBox5.Controls.Add(this.chkBillTaxIncl);
            this.groupBox5.Controls.Add(this.txtBackupPath);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Location = new System.Drawing.Point(8, 253);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(192, 121);
            this.groupBox5.TabIndex = 15;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Taxes";
            // 
            // gvServiceCharge
            // 
            this.gvServiceCharge.DecimalRequired = true;
            this.gvServiceCharge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvServiceCharge.Location = new System.Drawing.Point(87, 14);
            this.gvServiceCharge.Name = "gvServiceCharge";
            this.gvServiceCharge.Size = new System.Drawing.Size(49, 20);
            this.gvServiceCharge.SymbolRequired = false;
            this.gvServiceCharge.TabIndex = 9;
            this.gvServiceCharge.Text = "0.00";
            this.gvServiceCharge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvServiceCharge.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Bill Tax ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Serv. Charge ";
            // 
            // gvBillTax
            // 
            this.gvBillTax.DecimalRequired = true;
            this.gvBillTax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvBillTax.Location = new System.Drawing.Point(88, 40);
            this.gvBillTax.Name = "gvBillTax";
            this.gvBillTax.Size = new System.Drawing.Size(49, 20);
            this.gvBillTax.SymbolRequired = false;
            this.gvBillTax.TabIndex = 10;
            this.gvBillTax.Text = "0.00";
            this.gvBillTax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvBillTax.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // chkBillTaxIncl
            // 
            this.chkBillTaxIncl.AutoSize = true;
            this.chkBillTaxIncl.Location = new System.Drawing.Point(87, 66);
            this.chkBillTaxIncl.Name = "chkBillTaxIncl";
            this.chkBillTaxIncl.Size = new System.Drawing.Size(68, 17);
            this.chkBillTaxIncl.TabIndex = 11;
            this.chkBillTaxIncl.Text = "Inclusive";
            this.chkBillTaxIncl.UseVisualStyleBackColor = true;
            // 
            // txtBackupPath
            // 
            this.txtBackupPath.Location = new System.Drawing.Point(9, 95);
            this.txtBackupPath.Name = "txtBackupPath";
            this.txtBackupPath.Size = new System.Drawing.Size(166, 20);
            this.txtBackupPath.TabIndex = 41;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 79);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 13);
            this.label9.TabIndex = 40;
            this.label9.Text = "Backup Path ";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.chkAllTaxesIncl);
            this.groupBox4.Controls.Add(this.chkMouseMode);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.txtBillPrefix);
            this.groupBox4.Controls.Add(this.gvCatViewHeight);
            this.groupBox4.Controls.Add(this.chkserviceTaxOnDirectBilling);
            this.groupBox4.Controls.Add(this.gvPurgngPeriod);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.chkPurgingReq);
            this.groupBox4.Controls.Add(this.chkRefund);
            this.groupBox4.Controls.Add(this.ChkHiding);
            this.groupBox4.Controls.Add(this.ChkAmtTendReq);
            this.groupBox4.Location = new System.Drawing.Point(8, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(192, 244);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "General";
            // 
            // chkAllTaxesIncl
            // 
            this.chkAllTaxesIncl.AutoSize = true;
            this.chkAllTaxesIncl.Location = new System.Drawing.Point(18, 188);
            this.chkAllTaxesIncl.Name = "chkAllTaxesIncl";
            this.chkAllTaxesIncl.Size = new System.Drawing.Size(114, 17);
            this.chkAllTaxesIncl.TabIndex = 48;
            this.chkAllTaxesIncl.Text = "All Taxes Inclusive";
            this.chkAllTaxesIncl.UseVisualStyleBackColor = true;
            // 
            // chkMouseMode
            // 
            this.chkMouseMode.AutoSize = true;
            this.chkMouseMode.Location = new System.Drawing.Point(18, 210);
            this.chkMouseMode.Name = "chkMouseMode";
            this.chkMouseMode.Size = new System.Drawing.Size(164, 17);
            this.chkMouseMode.TabIndex = 47;
            this.chkMouseMode.Text = "Mouse Mode - Item Selection";
            this.chkMouseMode.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 48);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(109, 13);
            this.label10.TabIndex = 46;
            this.label10.Text = "Category View Height";
            // 
            // txtBillPrefix
            // 
            this.txtBillPrefix.Location = new System.Drawing.Point(76, 19);
            this.txtBillPrefix.Name = "txtBillPrefix";
            this.txtBillPrefix.Size = new System.Drawing.Size(103, 20);
            this.txtBillPrefix.TabIndex = 0;
            // 
            // gvCatViewHeight
            // 
            this.gvCatViewHeight.DecimalRequired = true;
            this.gvCatViewHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvCatViewHeight.Location = new System.Drawing.Point(130, 45);
            this.gvCatViewHeight.Name = "gvCatViewHeight";
            this.gvCatViewHeight.Size = new System.Drawing.Size(49, 20);
            this.gvCatViewHeight.SymbolRequired = false;
            this.gvCatViewHeight.TabIndex = 45;
            this.gvCatViewHeight.Text = "0.00";
            this.gvCatViewHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvCatViewHeight.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // chkserviceTaxOnDirectBilling
            // 
            this.chkserviceTaxOnDirectBilling.AutoSize = true;
            this.chkserviceTaxOnDirectBilling.Location = new System.Drawing.Point(18, 100);
            this.chkserviceTaxOnDirectBilling.Name = "chkserviceTaxOnDirectBilling";
            this.chkserviceTaxOnDirectBilling.Size = new System.Drawing.Size(161, 17);
            this.chkserviceTaxOnDirectBilling.TabIndex = 5;
            this.chkserviceTaxOnDirectBilling.Text = "Service Tax On DirectBilling.";
            this.chkserviceTaxOnDirectBilling.UseVisualStyleBackColor = true;
            this.chkserviceTaxOnDirectBilling.Visible = false;
            this.chkserviceTaxOnDirectBilling.CheckedChanged += new System.EventHandler(this.chkKOTPrinting_CheckedChanged);
            // 
            // gvPurgngPeriod
            // 
            this.gvPurgngPeriod.DecimalRequired = false;
            this.gvPurgngPeriod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvPurgngPeriod.Location = new System.Drawing.Point(130, 45);
            this.gvPurgngPeriod.Name = "gvPurgngPeriod";
            this.gvPurgngPeriod.Size = new System.Drawing.Size(52, 20);
            this.gvPurgngPeriod.SymbolRequired = false;
            this.gvPurgngPeriod.TabIndex = 3;
            this.gvPurgngPeriod.Text = "0";
            this.gvPurgngPeriod.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvPurgngPeriod.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 39;
            this.label8.Text = "Bill Prefix ";
            // 
            // chkPurgingReq
            // 
            this.chkPurgingReq.AutoSize = true;
            this.chkPurgingReq.Location = new System.Drawing.Point(18, 78);
            this.chkPurgingReq.Name = "chkPurgingReq";
            this.chkPurgingReq.Size = new System.Drawing.Size(108, 17);
            this.chkPurgingReq.TabIndex = 2;
            this.chkPurgingReq.Text = "Purging Required";
            this.chkPurgingReq.UseVisualStyleBackColor = true;
            this.chkPurgingReq.CheckedChanged += new System.EventHandler(this.chkPurgingReq_CheckedChanged);
            // 
            // chkRefund
            // 
            this.chkRefund.AutoSize = true;
            this.chkRefund.Location = new System.Drawing.Point(18, 166);
            this.chkRefund.Name = "chkRefund";
            this.chkRefund.Size = new System.Drawing.Size(126, 17);
            this.chkRefund.TabIndex = 6;
            this.chkRefund.Text = "KOT Refund Allowed";
            this.chkRefund.UseVisualStyleBackColor = true;
            this.chkRefund.CheckedChanged += new System.EventHandler(this.chkRefund_CheckedChanged);
            // 
            // ChkHiding
            // 
            this.ChkHiding.AutoSize = true;
            this.ChkHiding.Location = new System.Drawing.Point(18, 144);
            this.ChkHiding.Name = "ChkHiding";
            this.ChkHiding.Size = new System.Drawing.Size(84, 17);
            this.ChkHiding.TabIndex = 8;
            this.ChkHiding.Text = "Bold Screen";
            this.ChkHiding.UseVisualStyleBackColor = true;
            // 
            // ChkAmtTendReq
            // 
            this.ChkAmtTendReq.AutoSize = true;
            this.ChkAmtTendReq.Location = new System.Drawing.Point(18, 122);
            this.ChkAmtTendReq.Name = "ChkAmtTendReq";
            this.ChkAmtTendReq.Size = new System.Drawing.Size(157, 17);
            this.ChkAmtTendReq.TabIndex = 7;
            this.ChkAmtTendReq.Text = "Amount Tendered Required";
            this.ChkAmtTendReq.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rtbStubNRA);
            this.groupBox3.Controls.Add(this.rtbStubRNP);
            this.groupBox3.Controls.Add(this.rtbStubRP);
            this.groupBox3.Controls.Add(this.rtbStubRA);
            this.groupBox3.Location = new System.Drawing.Point(208, 115);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(220, 121);
            this.groupBox3.TabIndex = 44;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "KOT - STUB";
            // 
            // rtbStubNRA
            // 
            this.rtbStubNRA.AutoSize = true;
            this.rtbStubNRA.Location = new System.Drawing.Point(22, 101);
            this.rtbStubNRA.Name = "rtbStubNRA";
            this.rtbStubNRA.Size = new System.Drawing.Size(145, 17);
            this.rtbStubNRA.TabIndex = 3;
            this.rtbStubNRA.TabStop = true;
            this.rtbStubNRA.Text = "Not Required for All Items";
            this.rtbStubNRA.UseVisualStyleBackColor = true;
            // 
            // rtbStubRNP
            // 
            this.rtbStubRNP.AutoSize = true;
            this.rtbStubRNP.Location = new System.Drawing.Point(22, 73);
            this.rtbStubRNP.Name = "rtbStubRNP";
            this.rtbStubRNP.Size = new System.Drawing.Size(191, 17);
            this.rtbStubRNP.TabIndex = 2;
            this.rtbStubRNP.TabStop = true;
            this.rtbStubRNP.Text = "Required for Non Parcel Items Only";
            this.rtbStubRNP.UseVisualStyleBackColor = true;
            // 
            // rtbStubRP
            // 
            this.rtbStubRP.AutoSize = true;
            this.rtbStubRP.Location = new System.Drawing.Point(22, 46);
            this.rtbStubRP.Name = "rtbStubRP";
            this.rtbStubRP.Size = new System.Drawing.Size(168, 17);
            this.rtbStubRP.TabIndex = 1;
            this.rtbStubRP.TabStop = true;
            this.rtbStubRP.Text = "Required for Parcel Items Only";
            this.rtbStubRP.UseVisualStyleBackColor = true;
            // 
            // rtbStubRA
            // 
            this.rtbStubRA.AutoSize = true;
            this.rtbStubRA.Location = new System.Drawing.Point(22, 19);
            this.rtbStubRA.Name = "rtbStubRA";
            this.rtbStubRA.Size = new System.Drawing.Size(125, 17);
            this.rtbStubRA.TabIndex = 0;
            this.rtbStubRA.TabStop = true;
            this.rtbStubRA.Text = "Required for All Items";
            this.rtbStubRA.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkrefKotnoUnique);
            this.groupBox2.Controls.Add(this.chkRefKOTNoMan);
            this.groupBox2.Controls.Add(this.chkCoveredNoman);
            this.groupBox2.Controls.Add(this.chkKOTPrinting);
            this.groupBox2.Location = new System.Drawing.Point(208, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(220, 107);
            this.groupBox2.TabIndex = 43;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "K O T";
            // 
            // chkrefKotnoUnique
            // 
            this.chkrefKotnoUnique.AutoSize = true;
            this.chkrefKotnoUnique.Location = new System.Drawing.Point(8, 84);
            this.chkrefKotnoUnique.Name = "chkrefKotnoUnique";
            this.chkrefKotnoUnique.Size = new System.Drawing.Size(115, 17);
            this.chkrefKotnoUnique.TabIndex = 7;
            this.chkrefKotnoUnique.Text = "Ref KOT is Unique";
            this.chkrefKotnoUnique.UseVisualStyleBackColor = true;
            // 
            // chkRefKOTNoMan
            // 
            this.chkRefKOTNoMan.AutoSize = true;
            this.chkRefKOTNoMan.Location = new System.Drawing.Point(8, 63);
            this.chkRefKOTNoMan.Name = "chkRefKOTNoMan";
            this.chkRefKOTNoMan.Size = new System.Drawing.Size(132, 17);
            this.chkRefKOTNoMan.TabIndex = 6;
            this.chkRefKOTNoMan.Text = "Ref KOT No Madatory";
            this.chkRefKOTNoMan.UseVisualStyleBackColor = true;
            // 
            // chkCoveredNoman
            // 
            this.chkCoveredNoman.AutoSize = true;
            this.chkCoveredNoman.Location = new System.Drawing.Point(8, 42);
            this.chkCoveredNoman.Name = "chkCoveredNoman";
            this.chkCoveredNoman.Size = new System.Drawing.Size(130, 17);
            this.chkCoveredNoman.TabIndex = 5;
            this.chkCoveredNoman.Text = "Covered No Madatory";
            this.chkCoveredNoman.UseVisualStyleBackColor = true;
            // 
            // chkKOTPrinting
            // 
            this.chkKOTPrinting.AutoSize = true;
            this.chkKOTPrinting.Location = new System.Drawing.Point(8, 21);
            this.chkKOTPrinting.Name = "chkKOTPrinting";
            this.chkKOTPrinting.Size = new System.Drawing.Size(132, 17);
            this.chkKOTPrinting.TabIndex = 4;
            this.chkKOTPrinting.Text = "KOT Printing Required";
            this.chkKOTPrinting.UseVisualStyleBackColor = true;
            this.chkKOTPrinting.CheckedChanged += new System.EventHandler(this.chkKOTPrinting_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rtbOneRupee);
            this.groupBox1.Controls.Add(this.rtb50Paise);
            this.groupBox1.Location = new System.Drawing.Point(208, 242);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(220, 42);
            this.groupBox1.TabIndex = 42;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Net Amount Round Criteria";
            // 
            // rtbOneRupee
            // 
            this.rtbOneRupee.AutoSize = true;
            this.rtbOneRupee.Checked = true;
            this.rtbOneRupee.Location = new System.Drawing.Point(119, 17);
            this.rtbOneRupee.Name = "rtbOneRupee";
            this.rtbOneRupee.Size = new System.Drawing.Size(80, 17);
            this.rtbOneRupee.TabIndex = 1;
            this.rtbOneRupee.TabStop = true;
            this.rtbOneRupee.Text = "One Rupee";
            this.rtbOneRupee.UseVisualStyleBackColor = true;
            // 
            // rtb50Paise
            // 
            this.rtb50Paise.AutoSize = true;
            this.rtb50Paise.Location = new System.Drawing.Point(26, 17);
            this.rtb50Paise.Name = "rtb50Paise";
            this.rtb50Paise.Size = new System.Drawing.Size(69, 17);
            this.rtb50Paise.TabIndex = 0;
            this.rtb50Paise.Text = "50 Paise ";
            this.rtb50Paise.UseVisualStyleBackColor = true;
            // 
            // grpBillNo
            // 
            this.grpBillNo.Controls.Add(this.label13);
            this.grpBillNo.Controls.Add(this.txtResetChar);
            this.grpBillNo.Controls.Add(this.chkResetTerminal);
            this.grpBillNo.Controls.Add(this.gvrefreshAfter);
            this.grpBillNo.Controls.Add(this.rdbAfterbills);
            this.grpBillNo.Controls.Add(this.rdbEveryDay);
            this.grpBillNo.Controls.Add(this.rdbEveryMonth);
            this.grpBillNo.Location = new System.Drawing.Point(208, 284);
            this.grpBillNo.Name = "grpBillNo";
            this.grpBillNo.Size = new System.Drawing.Size(218, 99);
            this.grpBillNo.TabIndex = 12;
            this.grpBillNo.TabStop = false;
            this.grpBillNo.Text = "Bill No Reset";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(111, 47);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Reset Char";
            // 
            // txtResetChar
            // 
            this.txtResetChar.Location = new System.Drawing.Point(171, 43);
            this.txtResetChar.MaxLength = 2;
            this.txtResetChar.Name = "txtResetChar";
            this.txtResetChar.Size = new System.Drawing.Size(39, 20);
            this.txtResetChar.TabIndex = 5;
            // 
            // chkResetTerminal
            // 
            this.chkResetTerminal.AutoSize = true;
            this.chkResetTerminal.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkResetTerminal.Location = new System.Drawing.Point(105, 20);
            this.chkResetTerminal.Name = "chkResetTerminal";
            this.chkResetTerminal.Size = new System.Drawing.Size(97, 17);
            this.chkResetTerminal.TabIndex = 4;
            this.chkResetTerminal.Text = "Terminal Reset";
            this.chkResetTerminal.UseVisualStyleBackColor = true;
            // 
            // gvrefreshAfter
            // 
            this.gvrefreshAfter.DecimalRequired = false;
            this.gvrefreshAfter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvrefreshAfter.Location = new System.Drawing.Point(83, 69);
            this.gvrefreshAfter.Name = "gvrefreshAfter";
            this.gvrefreshAfter.Size = new System.Drawing.Size(46, 20);
            this.gvrefreshAfter.SymbolRequired = false;
            this.gvrefreshAfter.TabIndex = 3;
            this.gvrefreshAfter.Text = "0";
            this.gvrefreshAfter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gvrefreshAfter.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // rdbAfterbills
            // 
            this.rdbAfterbills.AutoSize = true;
            this.rdbAfterbills.Location = new System.Drawing.Point(8, 70);
            this.rdbAfterbills.Name = "rdbAfterbills";
            this.rdbAfterbills.Size = new System.Drawing.Size(157, 17);
            this.rdbAfterbills.TabIndex = 2;
            this.rdbAfterbills.TabStop = true;
            this.rdbAfterbills.Text = "After every                  Bill(s).";
            this.rdbAfterbills.UseVisualStyleBackColor = true;
            this.rdbAfterbills.CheckedChanged += new System.EventHandler(this.rdbAfterbills_CheckedChanged);
            // 
            // rdbEveryDay
            // 
            this.rdbEveryDay.AutoSize = true;
            this.rdbEveryDay.Location = new System.Drawing.Point(8, 20);
            this.rdbEveryDay.Name = "rdbEveryDay";
            this.rdbEveryDay.Size = new System.Drawing.Size(72, 17);
            this.rdbEveryDay.TabIndex = 0;
            this.rdbEveryDay.TabStop = true;
            this.rdbEveryDay.Text = "Each Day";
            this.rdbEveryDay.UseVisualStyleBackColor = true;
            // 
            // rdbEveryMonth
            // 
            this.rdbEveryMonth.AutoSize = true;
            this.rdbEveryMonth.Location = new System.Drawing.Point(8, 45);
            this.rdbEveryMonth.Name = "rdbEveryMonth";
            this.rdbEveryMonth.Size = new System.Drawing.Size(83, 17);
            this.rdbEveryMonth.TabIndex = 1;
            this.rdbEveryMonth.TabStop = true;
            this.rdbEveryMonth.Text = "Each Month";
            this.rdbEveryMonth.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(52, 539);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Item View :";
            this.label3.Visible = false;
            // 
            // cmbItemView
            // 
            this.cmbItemView.FormattingEnabled = true;
            this.cmbItemView.Location = new System.Drawing.Point(3, 509);
            this.cmbItemView.Name = "cmbItemView";
            this.cmbItemView.Size = new System.Drawing.Size(45, 21);
            this.cmbItemView.TabIndex = 13;
            this.cmbItemView.Visible = false;
            // 
            // cmbCatgView
            // 
            this.cmbCatgView.FormattingEnabled = true;
            this.cmbCatgView.Location = new System.Drawing.Point(3, 536);
            this.cmbCatgView.Name = "cmbCatgView";
            this.cmbCatgView.Size = new System.Drawing.Size(45, 21);
            this.cmbCatgView.TabIndex = 9;
            this.cmbCatgView.Visible = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(234, 567);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(95, 28);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnApply
            // 
            this.btnApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApply.Location = new System.Drawing.Point(123, 567);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(95, 28);
            this.btnApply.TabIndex = 2;
            this.btnApply.Text = "&Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // frmSettings
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(445, 603);
            this.Controls.Add(this.grpfields);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.cmbItemView);
            this.Controls.Add(this.cmbCatgView);
            this.Controls.Add(this.label3);
            this.Name = "frmSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bill Settings";
            this.Load += new System.EventHandler(this.frmSettings_Load);
            this.grpfields.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpBillNo.ResumeLayout(false);
            this.grpBillNo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
    }
}