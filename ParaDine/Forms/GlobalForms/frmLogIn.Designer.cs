﻿using ParaDine.Properties;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.GlobalForms
{
    partial class frmLogIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new Container();
            System.ComponentModel.ComponentResourceManager manager = new System.ComponentModel.ComponentResourceManager(typeof(frmLogIn));
            this.TTip = new ToolTip(this.components);
            this.txtPassword = new TextBox();
            this.txtUsrName = new TextBox();
            this.panel1 = new Panel();
            this.btnExit = new Button();
            this.imageList1 = new ImageList(this.components);
            this.btnLogIn = new Button();
            this.pnlLogin = new Panel();
            this.label3 = new Label();
            this.lblFrgtPsw = new Label();
            this.flPnlHint = new FlowLayoutPanel();
            this.btnPwd = new Button();
            this.txtAns = new TextBox();
            this.cmbHint = new ComboBox();
            this.label2 = new Label();
            this.label1 = new Label();
            this.pictureBox2 = new PictureBox();
            this.pictureBox1 = new PictureBox();
            this.timer1 = new Timer(this.components);
            this.panel1.SuspendLayout();
            this.pnlLogin.SuspendLayout();
            this.flPnlHint.SuspendLayout();
            ((ISupportInitialize)this.pictureBox2).BeginInit();
            ((ISupportInitialize)this.pictureBox1).BeginInit();
            base.SuspendLayout();
            this.txtPassword.BackColor = Color.Ivory;
            this.txtPassword.BorderStyle = BorderStyle.FixedSingle;
            this.txtPassword.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.txtPassword.Location = new Point(0xd6, 0x4d);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new Size(0xa7, 20);
            this.txtPassword.TabIndex = 1;
            this.txtPassword.TextAlign = HorizontalAlignment.Center;
            this.TTip.SetToolTip(this.txtPassword, "Enter Password");
            this.txtPassword.UseSystemPasswordChar = true;
            this.txtUsrName.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.txtUsrName.BackColor = Color.Ivory;
            this.txtUsrName.BorderStyle = BorderStyle.FixedSingle;
            this.txtUsrName.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            //            this.txtUsrName.ImeMode = ImeMode.NoControl;
            this.txtUsrName.Location = new Point(0xd6, 0x2e);
            this.txtUsrName.Name = "txtUsrName";
            this.txtUsrName.Size = new Size(0xa7, 20);
            this.txtUsrName.TabIndex = 0;
            this.txtUsrName.TextAlign = HorizontalAlignment.Center;
            this.TTip.SetToolTip(this.txtUsrName, "Enter User Name");
            this.panel1.Controls.Add(this.btnExit);
            this.panel1.Controls.Add(this.btnLogIn);
            this.panel1.Dock = DockStyle.Bottom;
            this.panel1.Location = new Point(0, 0x89);
            this.panel1.Name = "panel1";
            this.panel1.Size = new Size(0x18a, 0x30);
            this.panel1.TabIndex = 2;
            this.btnExit.Dock = DockStyle.Right;
            this.btnExit.FlatStyle = FlatStyle.Flat;
            this.btnExit.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnExit.ImageAlign = ContentAlignment.MiddleLeft;
            this.btnExit.ImageIndex = 1;
            this.btnExit.ImageList = this.imageList1;
            this.btnExit.Location = new Point(0xc5, 0);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new Size(0xc5, 0x30);
            this.btnExit.TabIndex = 1;
            this.btnExit.Text = "Cancel";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new EventHandler(this.btnExit_Click);
            //this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(manager.GetObject("imageList1.ImageStream"))); 
            this.imageList1.TransparentColor = Color.Transparent;
            //this.imageList1.Images.SetKeyName(0, "security10.gif");
            //this.imageList1.Images.SetKeyName(1, "topic_security.jpg");
            //this.imageList1.Images.SetKeyName(2, "Key.bmp");
            this.btnLogIn.Dock = DockStyle.Left;
            this.btnLogIn.FlatStyle = FlatStyle.Flat;
            this.btnLogIn.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.btnLogIn.Image = Resources.access_16x16;
            this.btnLogIn.ImageAlign = ContentAlignment.MiddleLeft;
            this.btnLogIn.Location = new Point(0, 0);
            this.btnLogIn.Name = "btnLogIn";
            this.btnLogIn.Size = new Size(0xc5, 0x30);
            this.btnLogIn.TabIndex = 0;
            this.btnLogIn.Text = "Log In";
            this.btnLogIn.UseVisualStyleBackColor = true;
            this.btnLogIn.Click += new EventHandler(this.btnLogIn_Click);
            this.pnlLogin.Controls.Add(this.label3);
            this.pnlLogin.Controls.Add(this.lblFrgtPsw);
            this.pnlLogin.Controls.Add(this.flPnlHint);
            this.pnlLogin.Controls.Add(this.txtPassword);
            this.pnlLogin.Controls.Add(this.txtUsrName);
            this.pnlLogin.Controls.Add(this.label2);
            this.pnlLogin.Controls.Add(this.label1);
            this.pnlLogin.Controls.Add(this.pictureBox2);
            this.pnlLogin.Controls.Add(this.pictureBox1);
            this.pnlLogin.Dock = DockStyle.Fill;
            this.pnlLogin.Location = new Point(0, 0);
            this.pnlLogin.Name = "pnlLogin";
            this.pnlLogin.Size = new Size(0x18a, 0x89);
            this.pnlLogin.TabIndex = 0;
            this.label3.BackColor = Color.SaddleBrown;
            this.label3.BorderStyle = BorderStyle.FixedSingle;
            this.label3.Dock = DockStyle.Top;
            this.label3.Font = new Font("Sylfaen", 16f, FontStyle.Italic | FontStyle.Bold, GraphicsUnit.Point, 0);
            this.label3.ForeColor = Color.WhiteSmoke;
            this.label3.Location = new Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new Size(0x18a, 30);
            this.label3.TabIndex = 8;
            this.label3.Text = "Security  Log";
            this.label3.TextAlign = ContentAlignment.TopCenter;
            this.label3.Click += new EventHandler(this.label3_Click);
            this.lblFrgtPsw.AutoSize = true;
            this.lblFrgtPsw.Cursor = Cursors.Hand;
            this.lblFrgtPsw.FlatStyle = FlatStyle.Flat;
            this.lblFrgtPsw.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.lblFrgtPsw.Location = new Point(0x114, 0x6a);
            this.lblFrgtPsw.Name = "lblFrgtPsw";
            this.lblFrgtPsw.Size = new Size(0x65, 13);
            this.lblFrgtPsw.TabIndex = 6;
            this.lblFrgtPsw.Text = "Forgot Password";
            this.lblFrgtPsw.Click += new EventHandler(this.lblFrgtPsw_Click);
            this.flPnlHint.Controls.Add(this.btnPwd);
            this.flPnlHint.Controls.Add(this.txtAns);
            this.flPnlHint.Controls.Add(this.cmbHint);
            this.flPnlHint.FlowDirection = FlowDirection.BottomUp;
            this.flPnlHint.Location = new Point(150, 0x7a);
            this.flPnlHint.Name = "flPnlHint";
            this.flPnlHint.Size = new Size(0xe7, 10);
            this.flPnlHint.TabIndex = 5;
            this.flPnlHint.Visible = false;
            this.btnPwd.FlatStyle = FlatStyle.Flat;
            this.btnPwd.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.btnPwd.Location = new Point(3, -16);
            this.btnPwd.Name = "btnPwd";
            this.btnPwd.Size = new Size(0xdf, 0x17);
            this.btnPwd.TabIndex = 4;
            this.btnPwd.Text = "Get Password";
            this.btnPwd.UseVisualStyleBackColor = true;
            this.btnPwd.Click += new EventHandler(this.btnPwd_Click);
            this.txtAns.BackColor = Color.White;
            this.txtAns.BorderStyle = BorderStyle.FixedSingle;
            this.txtAns.Location = new Point(0xe8, -13);
            this.txtAns.Name = "txtAns";
            this.txtAns.Size = new Size(0xdf, 20);
            this.txtAns.TabIndex = 3;
            this.cmbHint.Enabled = false;
            this.cmbHint.FormattingEnabled = true;
            this.cmbHint.Location = new Point(0x1cd, -14);
            this.cmbHint.Name = "cmbHint";
            this.cmbHint.Size = new Size(0xdf, 0x15);
            this.cmbHint.TabIndex = 2;
            this.label2.AutoSize = true;
            this.label2.Location = new Point(0x95, 0x51);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0x3b, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Password :";
            this.label1.AutoSize = true;
            this.label1.Location = new Point(0x8e, 50);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x42, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "User Name :";
            this.pictureBox2.Image = Resources.Vista_icons_02;
            this.pictureBox2.InitialImage = Resources.Vista_icons_02;
            this.pictureBox2.Location = new Point(12, 0x1f);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Size(0x75, 0x6b);
            this.pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 7;
            this.pictureBox2.TabStop = false;
            this.pictureBox1.Image = Resources.Vista_icons_08;
            this.pictureBox1.InitialImage = Resources.Vista_icons_08;
            this.pictureBox1.Location = new Point(12, 0x1f);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Size(0x75, 0x6b);
            this.pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            this.timer1.Interval = 0x927c0;
            this.timer1.Tick += new EventHandler(this.timer1_Tick);
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            //base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x18a, 0xb9);
            base.ControlBox = false;
            base.Controls.Add(this.pnlLogin);
            base.Controls.Add(this.panel1);
            // base.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            base.Name = "frmLogIn";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            //base.SizeGripStyle = SizeGripStyle.Hide;
            base.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "LogIn";
            base.Load += new EventHandler(this.frmLogIn_Load);
            this.panel1.ResumeLayout(false);
            this.pnlLogin.ResumeLayout(false);
            this.pnlLogin.PerformLayout();
            this.flPnlHint.ResumeLayout(false);
            this.flPnlHint.PerformLayout();
            ((ISupportInitialize)this.pictureBox2).EndInit();
            ((ISupportInitialize)this.pictureBox1).EndInit();
            base.ResumeLayout(false);
        }

        #endregion
    }
}