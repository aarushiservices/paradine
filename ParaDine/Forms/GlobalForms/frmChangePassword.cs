﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.GlobalForms
{
    public partial class frmChangePassword : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private ComboBox cmbUser;
       // private IContainer components = null;
        private User EntId = new User();
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Panel pnlChgPaswrd;
        private ToolTip TTip;
        private TextBox txtNewPaswrd;
        private TextBox txtOldPaswrd;
        private TextBox txtRetypepaswrd;

        public frmChangePassword(string Mode, int ID)
        {
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes((long)ID);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (this.txtOldPaswrd.Text == this.EntId.Password)
            {
                if (GlobalValidations.ValidateFields(this.pnlChgPaswrd, this.TTip))
                {
                    if (this.txtNewPaswrd.Text != this.txtRetypepaswrd.Text)
                    {
                        MessageBox.Show("Retype the Password correctly : ");
                    }
                    else
                    {
                        this.LoadEntities();
                        SqlTransaction sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
                        try
                        {
                            string str = this.btnTrans.Text.ToUpper();
                            if ((str != null) && (str == "CHANGE"))
                            {
                                this.EntId.Modify(sqlTrans, true);
                            }
                            sqlTrans.Commit();
                            base.Close();
                        }
                        catch (Exception exception)
                        {
                            MessageBox.Show(exception.Message, exception.Source);
                            sqlTrans.Rollback();
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Please Check the Old Password : ");
            }
        }
        private void frmChangePassword_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                this.LoadFields();
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                base.Close();
            }
        }

  

        private void LoadEntities()
        {
            this.EntId.UserId = Convert.ToInt16(this.cmbUser.Tag);
            this.EntId.UserName = Convert.ToString(this.cmbUser.Text);
            this.EntId.Password = Convert.ToString(this.txtNewPaswrd.Text);
        }

        private void LoadFields()
        {
            this.cmbUser.Tag = this.EntId.UserId;
            this.cmbUser.SelectedText = this.EntId.UserName;
        }
    }
}
