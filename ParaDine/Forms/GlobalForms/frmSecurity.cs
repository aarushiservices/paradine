﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.GlobalForms
{
    public partial class frmSecurity : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private ComboBox cmbLevel;
       //private IContainer components = null;
        private DataGridView DgvSecurity;
        private DataSet DS = new DataSet();
        private Label label1;
        private DataGridViewTextBoxColumn PARENTID;
        private DataGridViewTextBoxColumn Permission;
        private DataGridViewTextBoxColumn PERMISSIONID;
        private Panel pnlSecurity;
        private DataGridViewCheckBoxColumn Read;
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private DataGridViewTextBoxColumn SecurityId;
        private string StrSql;
        private ToolTip TTip;
        private DataGridViewCheckBoxColumn Write;

        public frmSecurity(SecurityClass paramSecurity)
        {
            this.InitializeComponent();
        }

        private void AddingPermissions(int LevelId, int ParentId, ref int pLoop)
        {
            this.StrSql = "SELECT SECURITYID, PERMISSIONNAME, ALLOWREAD, ALLOWWRITE, PERMISSIONID, PARENTID  FROM GLB_VW_SECURITY WHERE LEVELID = " + Convert.ToString(LevelId) + " AND PARENTID = " + Convert.ToString(ParentId);
            GlobalFill.FillDataSet(this.StrSql, "SubSec", this.DS, this.SDA);
            DataTable table = this.DS.Tables["SubSec"].Copy();
            foreach (DataRow row in table.Rows)
            {
                string str = Convert.ToString(row["PERMISSIONNAME"]).Replace("&", "");
                str = new string('.', pLoop * 5) + str;
                this.DgvSecurity.Rows.Add(new object[] { Convert.ToString(row["SecurityId"]), str, Convert.ToBoolean(row["AllowRead"]), Convert.ToBoolean(row["AllowWrite"]), Convert.ToInt32(row["PERMISSIONID"]), Convert.ToInt32(row["PARENTID"]) });
                if (pLoop == 1)
                {
                    this.DgvSecurity.Rows[this.DgvSecurity.Rows.Count - 1].Cells["Permission"].Style.ForeColor = Color.DarkRed;
                }
                else if (pLoop == 2)
                {
                    this.DgvSecurity.Rows[this.DgvSecurity.Rows.Count - 1].Cells["Permission"].Style.ForeColor = Color.DimGray;
                }
                else
                {
                    this.DgvSecurity.Rows[this.DgvSecurity.Rows.Count - 1].Cells["Permission"].Style.ForeColor = Color.DarkBlue;
                }
                pLoop++;
                this.AddingPermissions(LevelId, Convert.ToInt32(row["PermissionId"]), ref pLoop);
                pLoop--;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (this.btnTrans.Text == "&Save")
            {
                foreach (DataGridViewRow row in (IEnumerable)this.DgvSecurity.Rows)
                {
                    SecurityClass class2 = new SecurityClass((long)Convert.ToInt32(row.Cells["SECURITYID"].Value));
                    SqlTransaction sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
                    class2.AllowRead = Convert.ToBoolean(row.Cells["READ"].Value);
                    class2.AllowWrite = Convert.ToBoolean(row.Cells["WRITE"].Value);
                    class2.GetExecuteCommand("UPDATE", sqlTrans);
                    sqlTrans.Commit();
                }
                MessageBox.Show("Successfully Updated");
            }
        }

        private void cmbLevel_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                this.DgvSecurity.DataSource = null;
                this.DgvSecurity.Rows.Clear();
                int pLoop = 0;
                this.AddingPermissions(Convert.ToInt32(this.cmbLevel.SelectedValue), 0, ref pLoop);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private bool ContainCheckedPermissions(int pPermissionId, int CellNo)
        {
            foreach (DataGridViewRow row in (IEnumerable)this.DgvSecurity.Rows)
            {
                if ((Convert.ToInt32(row.Cells["PARENTID"].Value) == pPermissionId) && Convert.ToBoolean(row.Cells[CellNo].Value))
                {
                    return true;
                }
            }
            return false;
        }

        private bool ContainSubPermissions(int pPermissionId)
        {
            foreach (DataGridViewRow row in (IEnumerable)this.DgvSecurity.Rows)
            {
                if (Convert.ToInt32(row.Cells["PARENTID"].Value) == pPermissionId)
                {
                    return true;
                }
            }
            return false;
        }

        private void DgvSecurity_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }
        private void frmSecurity_Load(object sender, EventArgs e)
        {
            try
            {
                this.RefreshData();
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Form Load");
                base.Close();
            }
        }

        private DataGridViewRow GetDataGridRow(int pParentId)
        {
            foreach (DataGridViewRow row in (IEnumerable)this.DgvSecurity.Rows)
            {
                if (Convert.ToInt32(row.Cells["PERMISSIONID"].Value) == pParentId)
                {
                    return row;
                }
            }
            return null;
        }
        private void RefreshData()
        {
            try
            {
                GlobalFill.FillCombo("SELECT LEVELID, LEVELNAME FROM GLB_LEVEL", this.cmbLevel);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in RefreshData");
            }
        }
    }
}
