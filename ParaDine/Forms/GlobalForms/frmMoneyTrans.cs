﻿using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.GlobalForms
{
    public partial class frmMoneyTrans : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private ComboBox cmbPayment;
       // private IContainer components = null;
        private DateTimePicker dtpPaymentDate;
        private DateTimePicker dtpTransDate;
        private MoneyMaster EntId = new MoneyMaster();
        private bool GlbBlnCredit = true;
        private NumControl gvPaidAmount;
        private NumControl gvPayableAmt;
        private NumControl gvTotalAmt;
        private Label label1;
        private Label label10;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        internal ListView lvwMoneyTrans;
        private Panel pnlMaster;
        private Panel pnlTrans;
        private ToolTip TTip;
        private TextBox txtNarration;
        private TextBox txtPaymentDetails;
        private TextBox txtPaymentNo;
        private TextBox txtTransSource;

        public frmMoneyTrans(string Mode, long ID, double PayableAmt, bool BlnCredit)
        {
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes(ID);
            ParaDine.GlobalFunctions.AddCompHandler(this.pnlTrans);
            this.gvPayableAmt.Value = Convert.ToDecimal(PayableAmt);
            this.GlbBlnCredit = BlnCredit;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.DialogResult = DialogResult.No;
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (ParaDine.GlobalValidations.ValidateFields(this.pnlMaster, this.TTip))
            {
                try
                {
                    this.LoadEntities();
                    base.DialogResult = DialogResult.Yes;
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, exception.Source);
                }
            }
        }

        private void cmbPayment_Leave(object sender, EventArgs e)
        {
        }

        private void cmbPayment_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void cmbPayment_Validated(object sender, EventArgs e)
        {
            this.gvPaidAmount.Value = Convert.ToDecimal((double)(Convert.ToDouble(this.gvPayableAmt.Value) - this.EntId.GetTransAmount));
        }

        private void frmMoneyTrans_Load(object sender, EventArgs e)
        {
            try
            {
                new GlobalTheme().applyTheme(this);
                this.RefreshData();
                this.LoadFields();
                this.PopulateMoneyTrans();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private int GetTransTableId()
        {
            if (this.lvwMoneyTrans.SelectedItems.Count > 0)
            {
                return (Convert.ToInt32(this.lvwMoneyTrans.SelectedItems[0].SubItems[this.lvwMoneyTrans.SelectedItems[0].SubItems.Count - 1].Text) - 1);
            }
            return 0;
        }

    

        private void LoadEntities()
        {
            this.EntId.MoneyMasterID = Convert.ToInt64(this.dtpTransDate.Tag);
            this.EntId.TransactionDate = Convert.ToDateTime(this.dtpTransDate.Value);
            this.EntId.TransactionSource = Convert.ToString(this.txtTransSource.Text);
            this.EntId.TransactionSourceID = Convert.ToInt64(this.txtTransSource.Tag);
            this.EntId.Narration = Convert.ToString(this.txtNarration.Text);
        }

        private void LoadFields()
        {
            this.dtpTransDate.Value = Convert.ToDateTime(this.EntId.TransactionDate);
            this.txtTransSource.Text = Convert.ToString(this.EntId.TransactionSource);
            this.txtTransSource.Tag = Convert.ToInt64(this.EntId.TransactionSourceID);
            this.dtpTransDate.Tag = Convert.ToInt64(this.EntId.MoneyMasterID);
            this.txtNarration.Text = Convert.ToString(this.EntId.Narration);
        }

        private void LoadMoneyTransEntities(MoneyTrans paramMoneyTrans)
        {
            paramMoneyTrans.MoneyMasterID = this.EntId.MoneyMasterID;
            paramMoneyTrans.MoneyTransID = 0L;
            paramMoneyTrans.PaidAmount = Convert.ToDouble(this.gvPaidAmount.Value);
            paramMoneyTrans.PaymentModeID = Convert.ToInt32(this.cmbPayment.SelectedValue);
            paramMoneyTrans.PaySourceDate = Convert.ToDateTime(this.dtpPaymentDate.Value);
            paramMoneyTrans.PaySourceDetails = Convert.ToString(this.txtPaymentDetails.Text);
            paramMoneyTrans.PaySourceNo = Convert.ToString(this.txtPaymentNo.Text);
            paramMoneyTrans.PaySourceType = "";
        }

        private void LoadMoneyTransFileds(MoneyTrans paramMoneyTrans)
        {
            this.cmbPayment.SelectedValue = Convert.ToInt16(paramMoneyTrans.PaymentModeID);
            this.gvPaidAmount.Value = Convert.ToDecimal(paramMoneyTrans.PaidAmount);
            this.txtPaymentNo.Text = Convert.ToString(paramMoneyTrans.PaySourceNo);
            this.dtpPaymentDate.Value = Convert.ToDateTime(paramMoneyTrans.PaySourceDate);
            this.txtPaymentDetails.Text = Convert.ToString(paramMoneyTrans.PaySourceDetails);
            if (this.cmbPayment.Enabled)
            {
                this.cmbPayment.Focus();
            }
        }

        private void lvwMoneyTrans_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (this.lvwMoneyTrans.SelectedItems.Count > 0)
                {
                    this.pnlTrans.Tag = this.GetTransTableId();
                    this.LoadMoneyTransFileds(this.EntId.MoneyTransCollection[Convert.ToInt32(this.pnlTrans.Tag)]);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In lvwMoneyTrans_DoubleClick");
                throw;
            }
        }

        private void lvwMoneyTrans_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    this.EntId.MoneyTransCollection[this.GetTransTableId()].Deleted = true;
                    this.PopulateMoneyTrans();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    this.lvwMoneyTrans_DoubleClick(sender, new EventArgs());
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In lvwUnit_Keydown");
                throw;
            }
        }

        private void PopulateMoneyTrans()
        {
            ParaDine.GlobalFill.FillListView(this.lvwMoneyTrans, this.EntId.MoneyTransCollection.GetCollectionTable());
            this.SetLvwColWidth();
            this.gvTotalAmt.Value = Convert.ToDecimal(this.EntId.GetTransAmount);
        }

        private void RefreshData()
        {
            try
            {
                string str = "";
                if (!this.GlbBlnCredit)
                {
                    str = " WHERE CREDIT = 0";
                }
                ParaDine.GlobalFill.FillCombo("SELECT PAYMENTMODEID, PAYMENTMODE FROM GLB_PAYMENTMODE" + str, this.cmbPayment);
                this.pnlTrans.Tag = "-1";
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error In RefreshData");
            }
        }

        private void SetLvwColWidth()
        {
            try
            {
                this.lvwMoneyTrans.Columns[0].Width = 80;
                this.lvwMoneyTrans.Columns[1].Width = 80;
                this.lvwMoneyTrans.Columns[2].Width = 100;
                this.lvwMoneyTrans.Columns[3].Width = 100;
                this.lvwMoneyTrans.Columns[4].Width = 100;
                this.lvwMoneyTrans.Columns[5].Width = 100;
                this.lvwMoneyTrans.Columns[6].Width = 0;
                this.lvwMoneyTrans.Columns[7].Width = 0;
                this.lvwMoneyTrans.Columns[8].Width = 0;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in Setting Col. Width");
            }
        }

        private void txtPaymentDetails_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (Convert.ToInt16(this.pnlTrans.Tag) < 0)
                    {
                        MoneyTrans varEntityMoneyTrans = new MoneyTrans
                        {
                            Added = true,
                            MoneyMasterID = this.EntId.MoneyMasterID,
                            MoneyTransID = 0L,
                            PaidAmount = Convert.ToDouble(this.gvPaidAmount.Value),
                            PaymentModeID = Convert.ToInt32(this.cmbPayment.SelectedValue),
                            PaySourceDate = Convert.ToDateTime(this.dtpPaymentDate.Value),
                            PaySourceDetails = Convert.ToString(this.txtPaymentDetails.Text),
                            PaySourceNo = Convert.ToString(this.txtPaymentNo.Text)
                        };
                        if (Convert.ToDouble(this.gvPaidAmount.Value) > (Convert.ToDouble(this.gvPayableAmt.Value) - this.EntId.GetTransAmount))
                        {
                            throw new Exception("You Entered Excess Amount");
                        }
                        this.EntId.MoneyTransCollection.Add(varEntityMoneyTrans);
                        this.PopulateMoneyTrans();
                    }
                    else
                    {
                        this.EntId.MoneyTransCollection[Convert.ToInt16(this.pnlTrans.Tag)].Modified = true;
                        this.EntId.MoneyTransCollection[Convert.ToInt16(this.pnlTrans.Tag)].PaidAmount = Convert.ToDouble(this.gvPaidAmount.Value);
                        this.EntId.MoneyTransCollection[Convert.ToInt16(this.pnlTrans.Tag)].PaymentModeID = Convert.ToInt32(this.cmbPayment.SelectedValue);
                        this.EntId.MoneyTransCollection[Convert.ToInt16(this.pnlTrans.Tag)].PaySourceDate = Convert.ToDateTime(this.dtpPaymentDate.Value);
                        this.EntId.MoneyTransCollection[Convert.ToInt16(this.pnlTrans.Tag)].PaySourceDetails = Convert.ToString(this.txtPaymentDetails.Text);
                        this.EntId.MoneyTransCollection[Convert.ToInt16(this.pnlTrans.Tag)].PaySourceNo = Convert.ToString(this.txtPaymentNo.Text);
                        this.PopulateMoneyTrans();
                    }
                    ParaDine.GlobalFunctions.ClearFields(this.pnlTrans);
                    this.gvPaidAmount.Value = Convert.ToDecimal((double)(Convert.ToDouble(this.gvPayableAmt.Value) - Convert.ToDouble(this.gvTotalAmt.Value)));
                    if (this.cmbPayment.Enabled)
                    {
                        this.cmbPayment.Focus();
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in txtPaymentDtails");
            }
        }

        private void txtPaymentDetails_TextChanged(object sender, EventArgs e)
        {
        }

        public MoneyMaster GetMoneyMaster
        {
            get
            {
                return this.EntId;
            }
        }
    }
}
