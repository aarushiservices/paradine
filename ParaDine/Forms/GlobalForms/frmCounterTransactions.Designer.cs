﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.GlobalForms
{
    partial class frmCounterTransactions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlCounterTrans = new System.Windows.Forms.Panel();
            this.BtnTransaction = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbpayType = new System.Windows.Forms.ComboBox();
            this.txtVoucherNo = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpVoucherDate = new System.Windows.Forms.DateTimePicker();
            this.Label1 = new System.Windows.Forms.Label();
            this.cmbMachineNo = new System.Windows.Forms.ComboBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.BtnTrans = new System.Windows.Forms.Button();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.pnlCounterTrans.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlCounterTrans
            // 
            this.pnlCounterTrans.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCounterTrans.Controls.Add(this.BtnTransaction);
            this.pnlCounterTrans.Controls.Add(this.label5);
            this.pnlCounterTrans.Controls.Add(this.label2);
            this.pnlCounterTrans.Controls.Add(this.cmbpayType);
            this.pnlCounterTrans.Controls.Add(this.txtVoucherNo);
            this.pnlCounterTrans.Controls.Add(this.Label4);
            this.pnlCounterTrans.Controls.Add(this.label3);
            this.pnlCounterTrans.Controls.Add(this.dtpVoucherDate);
            this.pnlCounterTrans.Controls.Add(this.Label1);
            this.pnlCounterTrans.Controls.Add(this.cmbMachineNo);
            this.pnlCounterTrans.Location = new System.Drawing.Point(0, 0);
            this.pnlCounterTrans.Name = "pnlCounterTrans";
            this.pnlCounterTrans.Size = new System.Drawing.Size(307, 153);
            this.pnlCounterTrans.TabIndex = 0;
            // 
            // BtnTransaction
            // 
            this.BtnTransaction.Location = new System.Drawing.Point(86, 118);
            this.BtnTransaction.Name = "BtnTransaction";
            this.BtnTransaction.Size = new System.Drawing.Size(134, 23);
            this.BtnTransaction.TabIndex = 22;
            this.BtnTransaction.Text = "button1";
            this.BtnTransaction.UseVisualStyleBackColor = true;
            this.BtnTransaction.Click += new System.EventHandler(this.BtnTransaction_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(214, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Amount :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(155, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "PayType :";
            // 
            // cmbpayType
            // 
            this.cmbpayType.BackColor = System.Drawing.Color.Ivory;
            this.cmbpayType.FormattingEnabled = true;
            this.cmbpayType.Location = new System.Drawing.Point(155, 82);
            this.cmbpayType.Name = "cmbpayType";
            this.cmbpayType.Size = new System.Drawing.Size(121, 21);
            this.cmbpayType.TabIndex = 19;
            this.cmbpayType.TabStop = false;
            this.cmbpayType.SelectedIndexChanged += new System.EventHandler(this.cmbpayType_SelectedIndexChanged);
            // 
            // txtVoucherNo
            // 
            this.txtVoucherNo.BackColor = System.Drawing.Color.White;
            this.txtVoucherNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVoucherNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVoucherNo.Location = new System.Drawing.Point(25, 31);
            this.txtVoucherNo.Name = "txtVoucherNo";
            this.txtVoucherNo.ReadOnly = true;
            this.txtVoucherNo.Size = new System.Drawing.Size(74, 20);
            this.txtVoucherNo.TabIndex = 17;
            this.txtVoucherNo.TabStop = false;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(116, 15);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(79, 13);
            this.Label4.TabIndex = 13;
            this.Label4.Text = "Voucher Date :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Voucher No :";
            // 
            // dtpVoucherDate
            // 
            this.dtpVoucherDate.CustomFormat = "dd/MMM/yy";
            this.dtpVoucherDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpVoucherDate.Location = new System.Drawing.Point(116, 31);
            this.dtpVoucherDate.Name = "dtpVoucherDate";
            this.dtpVoucherDate.Size = new System.Drawing.Size(92, 20);
            this.dtpVoucherDate.TabIndex = 8;
            this.dtpVoucherDate.TabStop = false;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(28, 66);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(68, 13);
            this.Label1.TabIndex = 9;
            this.Label1.Text = "MachineNo :";
            // 
            // cmbMachineNo
            // 
            this.cmbMachineNo.BackColor = System.Drawing.Color.Ivory;
            this.cmbMachineNo.FormattingEnabled = true;
            this.cmbMachineNo.Location = new System.Drawing.Point(28, 82);
            this.cmbMachineNo.Name = "cmbMachineNo";
            this.cmbMachineNo.Size = new System.Drawing.Size(121, 21);
            this.cmbMachineNo.TabIndex = 10;
            this.cmbMachineNo.TabStop = false;
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(158, 159);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // BtnTrans
            // 
            this.BtnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnTrans.Location = new System.Drawing.Point(74, 159);
            this.BtnTrans.Name = "BtnTrans";
            this.BtnTrans.Size = new System.Drawing.Size(75, 23);
            this.BtnTrans.TabIndex = 6;
            this.BtnTrans.Text = "Trans";
            this.BtnTrans.UseVisualStyleBackColor = true;
            this.BtnTrans.Click += new System.EventHandler(this.BtnTrans_Click);
            // 
            // frmCounterTransactions
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(307, 185);
            this.Controls.Add(this.BtnTrans);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.pnlCounterTrans);
            this.Name = "frmCounterTransactions";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Counter Transactions :";
            this.Load += new System.EventHandler(this.frmCounterTransactions_Load);
            this.pnlCounterTrans.ResumeLayout(false);
            this.pnlCounterTrans.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
    }
}