﻿using ParaDine.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.GlobalForms
{
    public partial class frmGridView : Form
    {
        private Button btnExit;
        //private IContainer components;
        private DataGridView dgview;
        private Label lblHeader;
        private Panel pnlMain;

        public frmGridView()
        {
            this.components = null;
            this.InitializeComponent();
        }

        public frmGridView(DataTable dt, string strHeader)
        {
            this.components = null;
            this.InitializeComponent();
            this.dgview.DataSource = dt;
            this.lblHeader.Text = strHeader;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void frmGridView_Load(object sender, EventArgs e)
        {
            GlobalTheme theme = new GlobalTheme();
            theme.applyTheme(this);
            theme.ApplyDataGridTheme(this.dgview);
            this.dgview.RowHeadersVisible = false;
        }
    }
}
