﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.GlobalForms
{
    partial class frmSecurity
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlSecurity = new System.Windows.Forms.Panel();
            this.btnTrans = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.DgvSecurity = new System.Windows.Forms.DataGridView();
            this.SecurityId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Permission = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Read = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Write = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PERMISSIONID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PARENTID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbLevel = new System.Windows.Forms.ComboBox();
            this.TTip = new System.Windows.Forms.ToolTip(this.components);
            this.pnlSecurity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvSecurity)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSecurity
            // 
            this.pnlSecurity.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlSecurity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSecurity.Controls.Add(this.btnTrans);
            this.pnlSecurity.Controls.Add(this.btnExit);
            this.pnlSecurity.Controls.Add(this.DgvSecurity);
            this.pnlSecurity.Controls.Add(this.label1);
            this.pnlSecurity.Controls.Add(this.cmbLevel);
            this.pnlSecurity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSecurity.Location = new System.Drawing.Point(0, 0);
            this.pnlSecurity.Name = "pnlSecurity";
            this.pnlSecurity.Size = new System.Drawing.Size(774, 553);
            this.pnlSecurity.TabIndex = 0;
            // 
            // btnTrans
            // 
            this.btnTrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrans.Location = new System.Drawing.Point(312, 517);
            this.btnTrans.Name = "btnTrans";
            this.btnTrans.Size = new System.Drawing.Size(75, 23);
            this.btnTrans.TabIndex = 14;
            this.btnTrans.Text = "&Save";
            this.btnTrans.UseVisualStyleBackColor = true;
            this.btnTrans.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(419, 517);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // DgvSecurity
            // 
            this.DgvSecurity.AllowUserToAddRows = false;
            this.DgvSecurity.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvSecurity.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DgvSecurity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvSecurity.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SecurityId,
            this.Permission,
            this.Read,
            this.Write,
            this.PERMISSIONID,
            this.PARENTID});
            this.DgvSecurity.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvSecurity.DefaultCellStyle = dataGridViewCellStyle2;
            this.DgvSecurity.Location = new System.Drawing.Point(5, 55);
            this.DgvSecurity.Name = "DgvSecurity";
            this.DgvSecurity.RowHeadersVisible = false;
            this.DgvSecurity.Size = new System.Drawing.Size(757, 444);
            this.DgvSecurity.TabIndex = 2;
            this.DgvSecurity.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DgvSecurity_ColumnHeaderMouseClick);
            // 
            // SecurityId
            // 
            this.SecurityId.HeaderText = "SecurityId";
            this.SecurityId.Name = "SecurityId";
            this.SecurityId.Visible = false;
            // 
            // Permission
            // 
            this.Permission.HeaderText = "Permission";
            this.Permission.Name = "Permission";
            this.Permission.Width = 350;
            // 
            // Read
            // 
            this.Read.HeaderText = "Read Only";
            this.Read.Name = "Read";
            this.Read.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Read.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Read.Width = 150;
            // 
            // Write
            // 
            this.Write.HeaderText = "Full";
            this.Write.Name = "Write";
            this.Write.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Write.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Write.Width = 150;
            // 
            // PERMISSIONID
            // 
            this.PERMISSIONID.HeaderText = "PERMISSIONID";
            this.PERMISSIONID.Name = "PERMISSIONID";
            this.PERMISSIONID.Visible = false;
            // 
            // PARENTID
            // 
            this.PARENTID.HeaderText = "PARENTID";
            this.PARENTID.Name = "PARENTID";
            this.PARENTID.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(294, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Level :";
            // 
            // cmbLevel
            // 
            this.cmbLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLevel.FormattingEnabled = true;
            this.cmbLevel.Location = new System.Drawing.Point(351, 21);
            this.cmbLevel.Name = "cmbLevel";
            this.cmbLevel.Size = new System.Drawing.Size(161, 21);
            this.cmbLevel.TabIndex = 0;
            this.cmbLevel.SelectionChangeCommitted += new System.EventHandler(this.cmbLevel_SelectionChangeCommitted);
            // 
            // frmSecurity
            // 
            this.ClientSize = new System.Drawing.Size(774, 553);
            this.Controls.Add(this.pnlSecurity);
            this.Name = "frmSecurity";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Security";
            this.Load += new System.EventHandler(this.frmSecurity_Load);
            this.pnlSecurity.ResumeLayout(false);
            this.pnlSecurity.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvSecurity)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
    }
}