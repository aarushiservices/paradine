﻿//using CrystalDecisions.Shared;
using ParaDine.Forms.Main;
using ParaDine.Forms.Masters;
using ParaDine.Forms.Printing;
using ParaDine.Forms.ReportForms;
using ParaDine.Forms.RestaurantItm;
using ParaDine.Forms.Security;
using ParaDine.Forms.Settings;
using ParaDine.Forms.Stock;
using ParaDine.Forms.Tools;
using ParaDine.Properties;
using SysSecCom;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace ParaDine.Forms.GlobalForms
{
    public partial class frmMDIRest : Form
    {
        private ToolStripMenuItem authorisationSettingsToolStripMenuItem;
        private ToolStripMenuItem backUpRestoreToolStripMenuItem;
        private ToolStripMenuItem BillCancelToolStripMenuItem;
        private ToolStripMenuItem billDeleteToolStripMenuItem;
        private ToolStripMenuItem BillingToolStripMenuItem;
        private ToolStripMenuItem billUpdateToolStripMenuItem;
        private ToolStripMenuItem BillViewToolStripMenuItem;
        private ToolStripMenuItem blueToolStripMenuItem;
        private ToolStripMenuItem brickToolStripMenuItem;
        internal Button btn_Tables;
        internal Button btnCancelBill;
        internal Button btnCustOrder;
        internal Button btnDirectOrder;
        internal Button BtnEndOfDay;
        internal Button BtnKitchen_Bar;
        internal Button btnOrders;
        internal Button BtnReservation;
        internal Button btnSendMail;
        internal Button btnShifts;
        private ToolStripMenuItem cardRechargeToolStripMenuItem;
        private ToolStripMenuItem cardReturnToolStripMenuItem;
        private ToolStripMenuItem cardToolStripMenuItem;
        private ToolStripMenuItem cardTypeToolStripMenuItem;
        private ToolStripMenuItem categoryToolStripMenuItem;
        private ToolStripMenuItem changePasswToolStripMenuItem;
        private ToolStripMenuItem clientSettingsToolStripMenuItem;
       // private IContainer components = null;
        private ToolStripMenuItem configurationToolStripMenuItem;
        private ToolStripMenuItem consumedGoodsToolStripMenuItem;
        private ToolStripMenuItem counterSummaryToolStripMenuItem;
        private ToolStripMenuItem counterTransToolStripMenuItem;
        private ToolStripMenuItem cToolStripMenuItem;
        private ToolStripMenuItem customerLedgerToolStripMenuItem;
        private ToolStripMenuItem customerPaymentToolStripMenuItem;
        private ToolStripMenuItem customerToolStripMenuItem;
        private ToolStripMenuItem dayEndToolStripMenuItem;
        private ToolStripMenuItem dayReportToolStripMenuItem;
        private ToolStripMenuItem delivertOrderToolStripMenuItem;
        private ToolStripMenuItem deliveryOrderBillSummaryToolStripMenuItem;
        private ToolStripMenuItem deliveryOrderItemsSummaryToolStripMenuItem;
        private ToolStripMenuItem deliveryOrderSummaryToolStripMenuItem;
        private ToolStripMenuItem deliveryOrderToolStripMenuItem;
        private ToolStripMenuItem departmentToolStripMenuItem;
        private ToolStripMenuItem directBillToolStripMenuItem;
        private DataSet DS = new DataSet();
        private ToolStripMenuItem exitToolStripMenuItem;
        private Form frmMain;
        private Form frmMaster;
        private Form frmReport;
        private Form frmrest;
        private Form frmSecurity = new Form();
        private Form frmSetting;
        private Form frmStock;
        private Form frmTableLocation;
        private Form frmtool = new Form();
        private GlobalTheme GT = new GlobalTheme();
        private ToolStripMenuItem hintsToolStripMenuItem;
        private ToolStripMenuItem historySettingsToolStripMenuItem;
        private ToolStripMenuItem homeSteadToolStripMenuItem;
        internal ImageList IMgLst;
        private ImageList ImgLstAlert;
        internal ImageList ImgLstMain;
        private ToolStripMenuItem indentBillsSummaryToolStripMenuItem;
        private ToolStripMenuItem indentItemsSummaryToolStripMenuItem;
        private ToolStripMenuItem indentSummaryToolStripMenuItem;
        private ToolStripMenuItem indentToolStripMenuItem;
        private ToolStripMenuItem indentToolStripMenuItem1;
        private ToolStripMenuItem itemListingToolStripMenuItem;
        private ToolStripMenuItem itemSalesHistoryToolStripMenuItem;
        private ToolStripMenuItem itemSalesSummaryToolStripMenuItem;
        private ToolStripMenuItem itemToolStripMenuItem;
        private ToolStripMenuItem kOTsSummaryToolStripMenuItem;
        internal Label Label10;
        internal Label Label11;
        internal Label Label12;
        private Label label13;
        internal Label Label5;
        internal Label Label6;
        internal Label Label7;
        internal Label Label8;
        internal Label Label9;
        private ToolStripStatusLabel lblAlert;
        private Label LblBussDate;
        internal Label lblCustomerorder;
        private Label lbllefttime;
        private int LevelId;
        private ToolStripMenuItem logHistoryToolStripMenuItem;
        private ToolStripMenuItem logOutToolStripMenuItem;
        private MenuStrip menuStrip;
        private ToolStripMenuItem metalicToolStripMenuItem;
       //private bool ModFlag;
        private ToolStripMenuItem noThemeToolStripMenuItem;
        private BillSettings ObjBillsettings = new BillSettings(1);
        private ToolStripMenuItem orderSummaryToolStripMenuItem;
        private ToolStripMenuItem outletStockToolStripMenuItem;
        private Panel panel1;
        private ToolStripMenuItem paymentModeToolStripMenuItem;
        internal Panel PnlNoTheme;
        private ToolStripMenuItem pOBillsSummaryToolStripMenuItem;
        private ToolStripMenuItem pOItemsSummaryToolStripMenuItem;
        private ToolStripMenuItem printPOToolStripMenuItem;
        private ToolStripMenuItem productionBillSummaryToolStripMenuItem;
        private ToolStripMenuItem productionSummaryToolStripMenuItem;
        private ToolStripMenuItem productionToolStripMenuItem;
        private ToolStripMenuItem productionToolStripMenuItem1;
        private ToolStripMenuItem purchaseOrderToolStripMenuItem;
        private ToolStripMenuItem purchaseOrderToolStripMenuItem1;
        private ToolStripMenuItem quitToolStripMenuItem;
        private ToolStripMenuItem rechargeCardsToolStripMenuItem;
        private ToolStripMenuItem registerProductToolStripMenuItem;
        private ToolStripMenuItem reportsToolStripMenuItem;
        private ToolStripMenuItem rEPRINTToolStripMenuItem;
        private ToolStripMenuItem restaurantToolStripMenuItem1;
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private ToolStripMenuItem sectionToolStripMenuItem;
        private ToolStripMenuItem securityToolStripMenuItem;
        private ToolStripMenuItem settingsToolStripMenuItem;
        private ToolStripMenuItem shiftBeginToolStripMenuItem;
        private ToolStripMenuItem shiftCloseToolStripMenuItem;
        private ToolStripMenuItem shiftToolStripMenuItem;
        private SqlCommand SqlCmd = new SqlCommand();
        private StatusStrip statusStrip;
        private ToolStripMenuItem stewardToolStripMenuItem;
        private ToolStripMenuItem stockAdjItemSummaryToolStripMenuItem;
        private ToolStripMenuItem stockAdjustmentSummaryToolStripMenuItem;
        private ToolStripMenuItem stockAdjustmentToolStripMenuItem;
        private ToolStripMenuItem sTOCKaDJUSTMENTToolStripMenuItem1;
        private ToolStripMenuItem stockConsumeItemSummaryToolStripMenuItem;
        private ToolStripMenuItem stockConsumeReportsToolStripMenuItem;
        private ToolStripMenuItem stockEntryBillsSummaryToolStripMenuItem;
        private ToolStripMenuItem stockEntryItemsSummaryToolStripMenuItem;
        private ToolStripMenuItem stockEntrySummaryToolStripMenuItem;
        private ToolStripMenuItem stockEntryToolStripMenuItem;
        private ToolStripMenuItem stockEntryToolStripMenuItem1;
        private ToolStripMenuItem stockInventoryToolStripMenuItem;
        private ToolStripMenuItem stockPointToolStripMenuItem;
        private ToolStripMenuItem stockReOrderToolStripMenuItem;
        private ToolStripMenuItem stockReportSettingsToolStripMenuItem;
        private ToolStripMenuItem stockReturnBillsSummaryToolStripMenuItem;
        private ToolStripMenuItem stockReturnItemsSummaryToolStripMenuItem;
        private ToolStripMenuItem stockReturnSummaryToolStripMenuItem;
        private ToolStripMenuItem stockReturnToolStripMenuItem;
        private ToolStripMenuItem stockReturnToolStripMenuItem1;
        private ToolStripMenuItem stockSettingsToolStripMenuItem;
        private ToolStripMenuItem stockToolStripMenuItem;
        private ToolStripMenuItem stockTransferBillSummaryToolStripMenuItem;
        private ToolStripMenuItem stockTransferItemSummaryToolStripMenuItem;
        private ToolStripMenuItem stockTransferToolStripMenuItem;
        private ToolStripMenuItem stockTransferToolStripMenuItem1;
        private ToolStripMenuItem stockWriteOffBillsSummaryToolStripMenuItem;
        private ToolStripMenuItem stockWriteOffItemsSummaryToolStripMenuItem;
        private ToolStripMenuItem stockWriteOffSummaryToolStripMenuItem;
        private ToolStripMenuItem stockWriteOffToolStripMenuItem;
        private ToolStripMenuItem stockWriteOffToolStripMenuItem1;
        private ToolStripMenuItem supplierLedgerToolStripMenuItem;
        private ToolStripMenuItem supplierPaymentToolStripMenuItem;
        private ToolStripMenuItem supplierToolStripMenuItem;
        private Systemsettings syssettings;
        private ToolStripMenuItem systemSettingsToolStripMenuItem;
        private ToolStripMenuItem tableArrangementToolStripMenuItem;
        private ToolStripMenuItem tableToolStripMenuItem;
        private ToolStripMenuItem taxToolStripMenuItem;
        private ToolStripMenuItem terminalToolStripMenuItem;
        private ToolStripMenuItem themesToolStripMenuItem;
        private Timer timer1;
        private ToolStripMenuItem toolsMenu;
        private System.Windows.Forms.ToolTip ToolTip;
        private ToolStripStatusLabel TSBussDate;
        private ToolStripMenuItem uOMToolStripMenuItem;
        private ToolStripMenuItem userAccountToolStripMenuItem;
        private ToolStripMenuItem userLevelsToolStripMenuItem;

        public frmMDIRest()
        {
            this.InitializeComponent();
        }

        private void AddHandlers()
        {
            this.noThemeToolStripMenuItem.Click += new EventHandler(this.Theme_Click);
            this.homeSteadToolStripMenuItem.Click += new EventHandler(this.Theme_Click);
            this.metalicToolStripMenuItem.Click += new EventHandler(this.Theme_Click);
            this.brickToolStripMenuItem.Click += new EventHandler(this.Theme_Click);
            this.blueToolStripMenuItem.Click += new EventHandler(this.Theme_Click);
        }

        private void AddPermissions(ToolStripMenuItem paramTSMItem)
        {
            Exception exception;
            Permission permission = new Permission(paramTSMItem.Text);
            SqlTransaction sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
            try
            {
                if (permission.PermissionID <= 0)
                {
                    permission.PermissionName = paramTSMItem.Text;
                    if (paramTSMItem.OwnerItem != null)
                    {
                        permission.ParentId = Convert.ToInt32(paramTSMItem.OwnerItem.Tag);
                    }
                    else
                    {
                        permission.ParentId = 0;
                    }
                    permission.Add(sqlTrans, false);
                }
                paramTSMItem.Tag = permission.PermissionID;
                sqlTrans.Commit();
            }
            catch (Exception exception1)
            {
                exception = exception1;
                MessageBox.Show(exception.Message, "Error in Saving Permission");
                sqlTrans.Rollback();
            }
            SecurityClass class2 = new SecurityClass(this.LevelId, permission.PermissionID);
            sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
            try
            {
                if (class2.SecurityID <= 0L)
                {
                    class2.LevelID = this.LevelId;
                    class2.PermissionID = permission.PermissionID;
                    class2.AllowRead = true;
                    class2.AllowWrite = false;
                    class2.AllowDeny = false;
                    class2.GetExecuteCommand("INSERT", sqlTrans);
                }
                sqlTrans.Commit();
            }
            catch (Exception exception2)
            {
                exception = exception2;
                MessageBox.Show(exception.Message, "Error in Saving Security");
                sqlTrans.Rollback();
            }
            foreach (ToolStripMenuItem item in paramTSMItem.DropDownItems)
            {
                this.AddPermissions(item);
            }
        }

        private void authorisationSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ParaDine.Forms.GlobalForms.frmSecurity(new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID)).ShowDialog();
        }

        private void BillCancelToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void billDeleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmBillDeletion();
            this.frmMaster.ShowDialog();
        }

        private void BillingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.frmMain != null)
            {
                this.frmMain.Dispose();
            }
            this.frmMain = new FrmBilling("Edit", 0L, false);
            this.frmMain.MdiParent = this;
            this.frmMain.Dock = DockStyle.None;
            this.frmMain.Show();
        }

        private void billUpdateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmBillModify();
            this.frmMaster.ShowDialog();
        }
        private void BillViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.frmMain != null)
            {
                this.frmMain.Dispose();
            }
            this.frmMain = new FrmBillView();
            this.frmMain.MdiParent = this;
            this.frmMain.Dock = DockStyle.Fill;
            this.frmMain.Show();
        }
        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        public void btn_Tables_Click(object sender, EventArgs e)
        {
            if (this.frmTableLocation != null)
            {
                this.frmTableLocation.Dispose();
            }
            this.frmTableLocation = new FrmTableLocation();
            this.frmTableLocation.MdiParent = this;
            this.frmTableLocation.Dock = DockStyle.Fill;
            this.frmTableLocation.Show();
        }

        private void btnCustOrder_Click(object sender, EventArgs e)
        {
            this.BillingToolStripMenuItem_Click(sender, e);
        }

        private void btnDirectOrder_Click(object sender, EventArgs e)
        {
            this.directBillToolStripMenuItem_Click(sender, e);
        }

        private void BtnEndOfDay_Click(object sender, EventArgs e)
        {
            this.dayEndToolStripMenuItem_Click(sender, e);
        }

        private void BtnKitchen_Bar_Click(object sender, EventArgs e)
        {
        }

        private void btnOrders_Click(object sender, EventArgs e)
        {
            this.BillViewToolStripMenuItem_Click(sender, e);
        }

        private void cardRechargeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmTransactions("CardRecharge", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID)).ShowDialog();
        }

        private void cardReturnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmTransactions("CardReturn", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID)).ShowDialog();
        }

        private void cardTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("CardType", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }

        private void categoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("Category", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }

        private void changePasswToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmChangePassword("Change", GlobalVariables.UserID).ShowDialog();
        }

        private void clientSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmSetting = new frmClientSettings();
            this.frmSetting.ShowDialog();
        }

        private void companyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void consumedGoodsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmStock = new frmTransactions("ConsumedGoods", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmStock.ShowDialog();
        }

        private void cookStatusToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void counterSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmRptCounterSummary();
            this.frmMaster.ShowDialog();
        }

        private void counterTransToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("CounterTrans");
            this.frmMaster.ShowDialog();
        }

        private void cToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmTransactions("CardIssue", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID)).ShowDialog();
        }

        private void customerLedgerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new FrmRptLedger('C');
            this.frmReport.ShowDialog();
        }

        private void customerPaymentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmrest = new FrmPayment_Reciept('C');
            this.frmrest.ShowDialog();
        }

        private void customerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("Customer", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }

        private void dayEndToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmtool = new frmDayEnd();
            this.frmtool.ShowDialog();
        }

        private void dayReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmRptDayReport().ShowDialog();
        }

        private void deliveryOrderItemsSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new frmDeliveryOrderItemsSumm();
            this.frmReport.ShowDialog();
        }

        private void deliveryOrderSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new frmRptStockTrans("DELIVERY_ORDER");
            this.frmReport.ShowDialog();
        }

        private void deliveryOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmStock = new frmTransactions("DeliveryOrder", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmStock.ShowDialog();
        }

        private void departmentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("Department", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }

        private void directBillToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.frmMain != null)
            {
                this.frmMain.Dispose();
            }
            this.frmMain = new FrmBilling("Edit", 0L, true);
            this.frmMain.MdiParent = this;
            this.frmMain.Dock = DockStyle.Fill;
            this.frmMain.Show();
        }        
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void frmMDIRest_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
        private void frmMDIRest_Load(object sender, EventArgs e)
        {
            GlobalFunctions.GetConnection();
            this.GT.applyTheme(this);
            this.LblBussDate.Text = "Bussiness Date : " + GlobalVariables.BusinessDate.ToString("dd/MMM/yy");
            this.PnlNoTheme.Left = (base.Width - this.PnlNoTheme.Width) / 2;
            this.LevelId = new User(GlobalVariables.UserID).LevelID;
            if (!new Level(this.LevelId).IsAdmin)
            {
                this.btn_Tables_Click(sender, e);
            }
            this.AddHandlers();
            if (this.LevelId != 1)
            {
                this.authorisationSettingsToolStripMenuItem.Visible = false;
            }
            DeliveryOrder order = new DeliveryOrder();
            int num = Convert.ToInt16(order.DeliveryOrderAlert);
            this.lblAlert.BackgroundImage = this.ImgLstAlert.Images[num];
            this.lblAlert.Tag = num;
            foreach (ToolStripMenuItem item in this.menuStrip.Items)
            {
                item.Tag = 0;
                this.AddPermissions(item);
            }
            foreach (ToolStripMenuItem item in this.menuStrip.Items)
            {
                this.SetDenyPermission(item);
            }

            if (GlobalVariables.RestaurantFormName != "")
            {
                this.Text = GlobalVariables.RestaurantFormName;
            }
            if (!GlobalVariables.ToShowStockForms)
            {
                this.stockToolStripMenuItem.Visible = false;
                this.stockEntryToolStripMenuItem1.Visible = false;
                this.stockReturnToolStripMenuItem1.Visible = false;
                this.stockAdjustmentToolStripMenuItem.Visible = false;
                this.stockWriteOffToolStripMenuItem1.Visible = false;
                this.indentToolStripMenuItem1.Visible = false;
            }
            GlobalFunctions.AssignCurrentShift();
            this.Text = this.Text + " - " + Convert.ToString(GlobalFunctions.GetQueryValue("SELECT STOCKPOINTNAME FROM RES_STOCKPOINT WHERE STOCKPOINTID = " + Convert.ToString(GlobalVariables.StockPointId)));
            if (!this.ObjBillsettings.CardSystemReq)
            {
                this.cardToolStripMenuItem.Visible = false;
            }
        }
        public static string GetEncryptedDate(string ParamStr)
        {
            string str = "";
            string str2 = "";
            string str3 = "";
            foreach (char ch in ParamStr)
            {
                switch (ch)
                {
                    case 'A':
                        str2 = "1";
                        break;
                    case 'B':
                        str2 = "2";
                        break;
                    case 'C':
                        str2 = "3";
                        break;
                    case 'D':
                        str2 = "4";
                        break;
                    case 'F':
                        str2 = "6";
                        break;
                    case 'G':
                        str2 = "7";
                        break;
                    case 'H':
                        str2 = "8";
                        break;
                    case 'I':
                        str2 = "9";
                        break;
                    case 'Z':
                        str2 = "0";
                        break;
                    case 'S':
                        str3 = "/";
                        break;
                }
                if (ch != 'S')
                {
                    str = str + Convert.ToString(str2);
                }
                else
                {
                    str = str + Convert.ToString(str3);
                }
            }
            return str;
        }
        private void GlbClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void hintsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("Hint", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }
        private void historySettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.frmSecurity != null)
            {
                this.frmSecurity.Dispose();
            }
            this.frmSecurity = new FrmLog();
            this.frmSecurity.Show();
        }
        private void indentBillsSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new frmRptIndentBillsSumm();
            this.frmReport.ShowDialog();
        }
        private void indentItemsSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new frmRptIndentItemSummary();
            this.frmReport.ShowDialog();
        }
        private void indentSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new frmRptStockTrans("INDENT");
            this.frmReport.ShowDialog();
        }
        private void indentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmStock = new frmTransactions("Indent", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmStock.ShowDialog();
        }
        private void ItemHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        private void itemListingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmRptItemListing();
            this.frmMaster.ShowDialog();
        }
        private void itemSalesHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmItemHistory().ShowDialog();
        }
        private void itemSalesSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmRptSalesSummary2();
            this.frmMaster.ShowDialog();
        }
        private void itemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("Item", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }
        private void kitchenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("Kitchen", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }
        private void kOTsSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmRptCustOrderKOT();
            this.frmMaster.ShowDialog();
        }
        private void lblAlert_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(this.lblAlert.Tag) == 1)
            {
                string str = "";
                string str2 = " WHERE ";
                this.SqlCmd = new SqlCommand();
                this.SqlCmd.Connection = GlobalVariables.SqlConn;
                this.SqlCmd.CommandType = CommandType.Text;
                str2 = string.Concat(new object[] { str2, " CONVERT(DATETIME,CONVERT(VARCHAR,DELIVERYDATE,112)) = CONVERT(DATETIME, CONVERT(VARCHAR,CONVERT(DATETIME,'", GlobalVariables.BusinessDate, "'),112))" });
                this.SqlCmd.CommandText = "SELECT * FROM RES_VW_PENDINGDELIVERYORDER " + str2;
                this.SDA.SelectCommand = this.SqlCmd;
                if (!object.ReferenceEquals(this.DS.Tables["PENDINGORDERS"], null))
                {
                    this.DS.Tables["PENDINGORDERS"].Clear();
                }
                this.SDA.Fill(this.DS, "PENDINGORDERS");
                //ParameterFields fields = new ParameterFields();
                //ParameterField parameterField = new ParameterField
                //{
                //    Name = "Parameter"
                //};
                ////ParameterDiscreteValue value2 = new ParameterDiscreteValue
                //{
                //    Value = str
                //};
                //parameterField.CurrentValues.Add((ParameterValue)value2);
                //fields.Add(parameterField);
                //if (this.DS.Tables["PENDINGORDERS"].Rows.Count > 0)
                //{
                //}
            }
        }
        private void logHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new FrmRptLog();
            this.frmReport.ShowDialog();
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmLogIn().Show();
            base.Hide();
        }

        private void menuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void orderSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmRptCustOrderMaster(new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }

        private void outletStockToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.frmReport = new frmRptStockPointStock();
            this.frmReport.ShowDialog();
        }

        private void paymentModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("PaymentMode", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }

        private void pendingDeliveryOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new FrmRptDeliveryOrder();
            this.frmReport.ShowDialog();
        }

        private void pOBillsSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new frmRptPOBillsSummary();
            this.frmReport.ShowDialog();
        }

        private void pOItemsSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new frmRptPOItemsSummary();
            this.frmReport.ShowDialog();
        }

        private void prepaidCardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("PrepaidCard", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }

        private void printPOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new frmRptStockTrans("PO");
            this.frmReport.ShowDialog();
        }

        private void productionBillSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FrmRptProductionBillSumm().ShowDialog();
        }

        private void productionItemsSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FRMRPTPRODUCTIONITEMSUMM().ShowDialog();
        }
        private void productionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmStock = new frmTransactions("Production", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmStock.ShowDialog();
        }

        private void purchaseOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmStock = new frmMasters("PurchaseOrder", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmStock.ShowDialog();
        }

        private void rechargeCardsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("Card", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }

        private void recipeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("Recipe", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }

        private void registerProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SystemSecurity.OpenRegistrationWindow();
        }
        public void rEPRINTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.frmtool != null)
            {
                this.frmtool.Dispose();
            }
            this.frmtool = new FrmPrinting(GlobalVariables.TerminalNo, "0", "0", DateTime.Now, "");
            this.frmtool.ShowDialog();
        }
        private void restaurantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("Restaurant", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }

        private void sectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("Section", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }

        private void SecurityClassToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("Security", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }
        private void SetDenyPermission(ToolStripMenuItem paramTSMItem)
        {
            SecurityClass class2 = new SecurityClass(this.LevelId, new Permission(paramTSMItem.Text).PermissionID);
            paramTSMItem.Visible = class2.AllowRead;
            foreach (ToolStripMenuItem item in paramTSMItem.DropDownItems)
            {
                this.SetDenyPermission(item);
            }
        }

        private void shiftBeginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmrest = new frmShiftBegin();
            this.frmrest.ShowDialog();
        }

        private void shiftToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("Shift", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }

        private void stewardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("Steward", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }
        private void stockAdjustmentItemsSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FrmRptStockAdjItemSumm().ShowDialog();
        }

        private void stockAdjustmentSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new frmRptStockTrans("STOCKADJUSTMENT");
            this.frmReport.ShowDialog();
        }

        private void sTOCKaDJUSTMENTToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new frmrptStockAdjBillsSummary().ShowDialog();
        }

        private void stockConsumeItemSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new fmRptConsumeItemSummary();
            this.frmReport.ShowDialog();
        }

        private void stockEntryBillsSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new frmRptStockEntryBillsSummary();
            this.frmReport.ShowDialog();
        }

        private void stockEntryItemsSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new frmRptStockEntryItemsSummary();
            this.frmReport.ShowDialog();
        }

        private void stockEntrySummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new frmRptStockTrans("STOCKENTRY");        
            this.frmReport.ShowDialog();
        }
        private void stockEntryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmStock = new frmTransactions("StockEntry", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmStock.ShowDialog();
        }
        private void stockEntryToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void stockInventoryToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void stockPointToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("StockPoint", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }

        private void stockReOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void stockReOrderToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.frmReport = new frmRptStockReOrder();
            this.frmReport.ShowDialog();
        }

        private void stockReportSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FrmStockReportsSettings().ShowDialog();
        }

        private void stockReturnBillsSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new frmRptStockReturnBillsSummary();
            this.frmReport.ShowDialog();
        }

        private void stockReturnItemsSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new frmRptStockReturnItemsSummary();
            this.frmReport.ShowDialog();
        }

        private void stockReturnSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new frmRptStockTrans("STOCKRETURN");
            this.frmReport.ShowDialog();
        }

        private void stockReturnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmStock = new frmTransactions("StockReturn", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmStock.ShowDialog();
        }

        private void stockSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmSetting = new FRMSTOCKSETTINGS();
            this.frmSetting.ShowDialog();
        }

        private void stockTakeToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.frmStock = new frmTransactions("StockTake", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmStock.ShowDialog();
        }

        private void stockTransferBillSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new frmRptStockTransfer();
            this.frmReport.ShowDialog();
        }

        private void stockTransferItemSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new frmRptStockTransferItemSumm();
            this.frmReport.ShowDialog();
        }

        private void stockTransferToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmStock = new frmTransactions("StockTransfer", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmStock.ShowDialog();
        }

        private void stockWriteOffBillsSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new frmRptStockWriteOffBillsSumm();
            this.frmReport.ShowDialog();
        }

        private void stockWriteOffItemsSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new FrmRptStockWriteOffItemsSumm();
            this.frmReport.ShowDialog();
        }

        private void stockWriteOffSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new frmRptStockTrans("WRITEOFF");
            this.frmReport.ShowDialog();
        }

        private void stockWriteOffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmrest = new frmTransactions("StockWriteOff", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmrest.ShowDialog();
        }

        private void storeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("Store", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }

        private void supplierLedgerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmReport = new FrmRptLedger('S');
            this.frmReport.ShowDialog();
        }

        private void supplierPaymentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmrest = new FrmPayment_Reciept('S');
            this.frmrest.ShowDialog();
        }

        private void supplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("Supplier", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }

        private void systemSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmSetting = new frmSettings();
            this.frmSetting.ShowDialog();
        }

        private void tableArrangementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.btn_Tables_Click(sender, e);
        }

        private void tableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("Table", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }

        private void taxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("Tax", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }

        private void terminalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("Terminal", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }

        private void Theme_Click(object sender, EventArgs e)
        {
            this.syssettings = new Systemsettings();
            this.syssettings.THEME = Convert.ToInt16(((ToolStripMenuItem)sender).Tag);
            this.GT.applyTheme(this);
            foreach (Form form in base.MdiChildren)
            {
                this.GT.applyTheme(form);
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {

        }
        private void uOMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("UOM", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }
        private void userAccountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("User", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }
        private void userLevelsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.frmMaster = new frmMasters("Level", new SecurityClass(this.LevelId, new Permission(sender.ToString()).PermissionID));
            this.frmMaster.ShowDialog();
        }
        private void wareHouseStockTakeToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
