﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.GlobalForms
{
    public partial class frmLevel : Form
    {
        private Button btnExit;
        private Button btnTrans;
        private CheckBox chkIsAdmin;
       //private IContainer components = null;
        private Level EntId = new Level();
        private Label label1;
        private Panel pnlLevel;
        private ToolTip TTip;
        private ImageList imageList1;
        private TextBox txtLevel;

        public frmLevel(string Mode, int ID, SecurityClass paramSecurity)
        {
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes((long)ID);
            this.EntId.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (GlobalValidations.ValidateFields(this.pnlLevel, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.btnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_007D;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_008D;
                            }
                        }
                        else
                        {
                            this.EntId.Add(sqlTrans, true);
                        }
                    }
                    goto Label_009F;
                Label_007D:
                    this.EntId.Modify(sqlTrans, true);
                    goto Label_009F;
                Label_008D:
                    this.EntId.Delete(sqlTrans, true);
                Label_009F:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, exception.Source);
                    sqlTrans.Rollback();
                }
            }
        }
        private void frmLevel_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                this.LoadFields();
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                base.Close();
            }
        }
     

        private void LoadEntities()
        {
            this.EntId.LevelID = Convert.ToInt32(this.txtLevel.Tag);
            this.EntId.LevelName = Convert.ToString(this.txtLevel.Text);
            this.EntId.IsAdmin = Convert.ToBoolean(this.chkIsAdmin.Checked);
        }

        private void LoadFields()
        {
            this.txtLevel.Tag = this.EntId.LevelID;
            this.txtLevel.Text = this.EntId.LevelName;
            this.chkIsAdmin.Checked = this.EntId.IsAdmin;
        }
    }
}
