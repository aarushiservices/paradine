﻿using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.GlobalForms
{
    public partial class frmSettings : Form
    {
        private int BillNoReset = 0;
        private Button btnApply;
        private Button btnCancel;
        private CheckBox chkAllTaxesIncl;
        private CheckBox ChkAmtTendReq;
        private CheckBox chkBillTaxIncl;
        private CheckBox chkCoveredNoman;
        private CheckBox chkDw1;
        private CheckBox chkDw2;
        private CheckBox chkDw3;
        private CheckBox chkDw4;
        private CheckBox ChkHiding;
        private CheckBox chkKOTPrinting;
        private CheckBox chkMouseMode;
        private CheckBox chkPurgingReq;
        private CheckBox chkRefKOTNoMan;
        private CheckBox chkrefKotnoUnique;
        private CheckBox chkRefund;
        private CheckBox chkResetTerminal;
        private CheckBox chkserviceTaxOnDirectBilling;
        private ComboBox cmbCatgView;
        private ComboBox cmbItemView;
        //private IContainer components = null;
        private DataSet DS = new DataSet();
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private GroupBox groupBox3;
        private GroupBox groupBox4;
        private GroupBox groupBox5;
        private GroupBox groupBox6;
        private GroupBox grpBillNo;
        private Panel grpfields;
        private NumControl gvBillTax;
        private NumControl gvCatViewHeight;
        private NumControl gvPurgngPeriod;
        private NumControl gvrefreshAfter;
        private NumControl gvServiceCharge;
        private Label label1;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label13;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private RadioButton rdbAfterbills;
        private RadioButton rdbEveryDay;
        private RadioButton rdbEveryMonth;
        private RadioButton rtb50Paise;
        private RadioButton rtbOneRupee;
        private RadioButton rtbStubNRA;
        private RadioButton rtbStubRA;
        private RadioButton rtbStubRNP;
        private RadioButton rtbStubRP;
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private string StrSql;
        private TextBox txtBackupPath;
        private TextBox txtBillHdr1;
        private TextBox txtBillHdr2;
        private TextBox txtBillHdr3;
        private TextBox txtBillHdr4;
        private TextBox txtBillPrefix;
        private TextBox txtDirectBillHeader;
        private TextBox txtKOTBillHeader;
        private TextBox txtResetChar;

        public frmSettings()
        {
            this.InitializeComponent();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
                this.UpdateValues("INSERT", sqlTrans);
                sqlTrans.Commit();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnSetdefault_Click(object sender, EventArgs e)
        {
        }

        private void chkKOTPrinting_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void chkPurgingReq_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.chkPurgingReq.Checked)
                {
                    this.gvPurgngPeriod.Enabled = true;
                    this.gvPurgngPeriod.Focus();
                }
                else
                {
                    this.gvPurgngPeriod.Enabled = false;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void chkRefund_CheckedChanged(object sender, EventArgs e)
        {
        }
        private void frmSettings_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.RefreshData();
            this.gvrefreshAfter.Enabled = false;
            this.gvPurgngPeriod.Enabled = false;
            this.LoadFields();
        }
        private void LoadFields()
        {
            this.StrSql = "SELECT * FROM RES_BILLSETTINGS";
            ParaDine.GlobalFill.FillDataSet(this.StrSql, "BILLSETTINGS", this.DS, this.SDA);
            if (this.DS.Tables["BILLSETTINGS"].Rows.Count > 0)
            {
                foreach (DataRow row in this.DS.Tables["BILLSETTINGS"].Rows)
                {
                    this.chkKOTPrinting.Checked = Convert.ToBoolean(row["KOTPRINTREQ"]);
                    this.txtBillPrefix.Text = Convert.ToString(row["BILLPREFIX"]);
                    this.BillNoReset = Convert.ToInt32(row["BILLNORESET"]);
                    this.chkResetTerminal.Checked = Convert.ToBoolean(row["BILLNORESETONTERMINAL"]);
                    this.txtResetChar.Text = Convert.ToString(row["BILLNORESETONCHAR"]).Trim();
                    if (this.BillNoReset == 0)
                    {
                        this.rdbEveryDay.Checked = true;
                    }
                    else if (this.BillNoReset == 1)
                    {
                        this.rdbEveryMonth.Checked = true;
                    }
                    else
                    {
                        this.rdbAfterbills.Checked = true;
                    }
                    this.gvrefreshAfter.Value = Convert.ToDecimal(row["RESETBILLNO"]);
                    this.chkserviceTaxOnDirectBilling.Checked = Convert.ToBoolean(row["SERVICETAXONDIRECTBILLING"]);
                    string str = Convert.ToString(row["BILLSTUBREQ"]);
                    this.rtbStubNRA.Checked = false;
                    this.rtbStubRA.Checked = false;
                    this.rtbStubRNP.Checked = false;
                    this.rtbStubRP.Checked = false;
                    switch (str)
                    {
                        case "RA":
                            this.rtbStubRA.Checked = true;
                            break;

                        case "RP":
                            this.rtbStubRP.Checked = true;
                            break;

                        case "NRP":
                            this.rtbStubRNP.Checked = true;
                            break;

                        case "NRA":
                            this.rtbStubNRA.Checked = true;
                            break;
                    }
                    this.chkPurgingReq.Checked = Convert.ToBoolean(row["BILLPERGREQ"]);
                    this.gvPurgngPeriod.Value = Convert.ToDecimal(row["PERGINGPERIOD"]);
                    this.chkRefund.Checked = Convert.ToBoolean(row["KOTREFUNDALLOWED"]);
                    this.chkAllTaxesIncl.Checked = Convert.ToBoolean(row["ALLTAXESINCLUSIVE"]);
                    this.gvServiceCharge.Value = Convert.ToDecimal(row["SERVICECHARGE"]);
                    this.cmbCatgView.SelectedIndex = Convert.ToInt32(row["CATEGORYVIEW"]);
                    this.cmbItemView.SelectedIndex = Convert.ToInt32(row["ITEMVIEW"]);
                    this.ChkAmtTendReq.Checked = Convert.ToBoolean(row["AMTTENDREQ"]);
                    this.txtBillHdr1.Text = Convert.ToString(row["BILLHEADER1"]);
                    this.chkDw1.Checked = Convert.ToBoolean(row["DWREQ1"]);
                    this.txtBillHdr2.Text = Convert.ToString(row["BILLHEADER2"]);
                    this.chkDw2.Checked = Convert.ToBoolean(row["DWREQ2"]);
                    this.txtBillHdr3.Text = Convert.ToString(row["BILLHEADER3"]);
                    this.chkDw3.Checked = Convert.ToBoolean(row["DWREQ3"]);
                    this.txtBillHdr4.Text = Convert.ToString(row["BILLHEADER4"]);
                    this.chkDw4.Checked = Convert.ToBoolean(row["DWREQ4"]);
                    this.gvBillTax.Value = Convert.ToDecimal(row["BILLTAX"]);
                    this.ChkHiding.Checked = Convert.ToBoolean(row["HIDING"]);
                    this.chkBillTaxIncl.Checked = Convert.ToBoolean(row["BILLTAXINCL"]);
                    this.txtBackupPath.Text = Convert.ToString(row["BACKUPPATH"]);
                    this.gvCatViewHeight.Value = Convert.ToDecimal(row["CATEGORYVIEWHEIGHT"]);
                    this.txtDirectBillHeader.Text = Convert.ToString(row["DIRECTBILLHEADER"]);
                    this.txtKOTBillHeader.Text = Convert.ToString(row["KOTBILLHEADER"]);
                    this.chkRefKOTNoMan.Checked = Convert.ToBoolean(row["REFKOTNOOPTIONAL"]);
                    this.chkCoveredNoman.Checked = Convert.ToBoolean(row["COVEREDNOOPTIONAL"]);
                    this.chkrefKotnoUnique.Checked = Convert.ToBoolean(row["REFKOTNOIDENTITY"]);
                    this.chkMouseMode.Checked = Convert.ToBoolean(row["MOUSEMODEREQ"]);
                    if (row["ROUNDTYPE"].ToString().ToUpper() == "50PAISE")
                    {
                        this.rtb50Paise.Checked = true;
                        this.rtbOneRupee.Checked = false;
                    }
                    else
                    {
                        this.rtb50Paise.Checked = false;
                        this.rtbOneRupee.Checked = true;
                    }
                }
            }
        }
        private void rdbAfterbills_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.rdbAfterbills.Checked)
                {
                    this.gvrefreshAfter.Enabled = true;
                    this.gvrefreshAfter.Focus();
                }
                else
                {
                    this.gvrefreshAfter.Enabled = false;
                    this.gvrefreshAfter.Value = Convert.ToDecimal(0);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
        private void RefreshData()
        {
            this.cmbCatgView.Items.Add("DETAILS VIEW");
            this.cmbCatgView.Items.Add("ICON VIEW");
            this.cmbCatgView.Items.Add("LIST VIEW");
            this.cmbCatgView.SelectedIndex = 0;
            this.cmbItemView.Items.Add("DETAILS VIEW");
            this.cmbItemView.Items.Add("ICON VIEW");
            this.cmbItemView.Items.Add("LIST VIEW");
            this.cmbItemView.SelectedIndex = 0;
        }

        private int UpdateValues(string TransType, SqlTransaction SqlTrans)
        {
            if (this.rdbEveryDay.Checked)
            {
                this.BillNoReset = 0;
            }
            else if (this.rdbEveryMonth.Checked)
            {
                this.BillNoReset = 1;
            }
            else if (this.rdbAfterbills.Checked)
            {
                this.BillNoReset = 2;
            }
            SqlCommand command = new SqlCommand
            {
                Connection = ParaDine.GlobalVariables.SqlConn,
                Transaction = SqlTrans,
                CommandText = "RES_PROC_BILLSETTINGS",
                CommandType = CommandType.StoredProcedure
            };
            command.Parameters.AddWithValue("@TRANSTYPE", TransType);
            command.Parameters.AddWithValue("@BILLSETTINGSID", 1);
            command.Parameters.AddWithValue("@KOTPRINTREQ", this.chkKOTPrinting.Checked);
            command.Parameters.AddWithValue("@BILLPREFIX", this.txtBillPrefix.Text);
            command.Parameters.AddWithValue("@BILLNORESET", this.BillNoReset);
            command.Parameters.AddWithValue("@RESETBILLNO", this.gvrefreshAfter.Value);
            command.Parameters.AddWithValue("@SERVICETAXONDIRECTBILLING", this.chkserviceTaxOnDirectBilling.Checked);
            command.Parameters.AddWithValue("@BILLPERGREQ", this.chkPurgingReq.Checked);
            command.Parameters.AddWithValue("@PERGINGPERIOD", this.gvPurgngPeriod.Value);
            command.Parameters.AddWithValue("@KOTREFUNDALLOWED", this.chkRefund.Checked);
            command.Parameters.AddWithValue("@ALLTAXESINCLUSIVE", this.chkAllTaxesIncl.Checked);
            command.Parameters.AddWithValue("@SERVICECHARGE", this.gvServiceCharge.Value);
            command.Parameters.AddWithValue("@CATEGORYVIEW", this.cmbCatgView.SelectedIndex);
            command.Parameters.AddWithValue("@ITEMVIEW", this.cmbItemView.SelectedIndex);
            command.Parameters.AddWithValue("@AMTTENDREQ", this.ChkAmtTendReq.Checked);
            command.Parameters.AddWithValue("@BILLHEADER1", this.txtBillHdr1.Text);
            command.Parameters.AddWithValue("@DWREQ1", this.chkDw1.Checked);
            command.Parameters.AddWithValue("@BILLHEADER2", this.txtBillHdr2.Text);
            command.Parameters.AddWithValue("@DWREQ2", this.chkDw2.Checked);
            command.Parameters.AddWithValue("@BILLHEADER3", this.txtBillHdr3.Text);
            command.Parameters.AddWithValue("@DWREQ3", this.chkDw3.Checked);
            command.Parameters.AddWithValue("@BILLHEADER4", this.txtBillHdr4.Text);
            command.Parameters.AddWithValue("@DWREQ4", this.chkDw4.Checked);
            command.Parameters.AddWithValue("@BILLTAX", this.gvBillTax.Value);
            command.Parameters.AddWithValue("@BILLTAXINCL", this.chkBillTaxIncl.Checked);
            command.Parameters.AddWithValue("@HIDING", this.ChkHiding.Checked);
            command.Parameters.AddWithValue("@BACKUPPATH", this.txtBackupPath.Text.ToString());
            command.Parameters.AddWithValue("@CATEGORYVIEWHEIGHT", this.gvCatViewHeight.Value);
            command.Parameters.AddWithValue("@DIRECTBILLHEADER", this.txtDirectBillHeader.Text);
            command.Parameters.AddWithValue("@KOTBILLHEADER", this.txtKOTBillHeader.Text);
            command.Parameters.AddWithValue("@BILLNORESETONTERMINAL", this.chkResetTerminal.Checked);
            command.Parameters.AddWithValue("@BILLNORESETONCHAR", this.txtResetChar.Text.Trim());
            command.Parameters.AddWithValue("@REFKOTNOOPTIONAL", this.chkRefKOTNoMan.Checked);
            command.Parameters.AddWithValue("@COVEREDNOOPTIONAL", this.chkCoveredNoman.Checked);
            command.Parameters.AddWithValue("@REFKOTNOIDENTITY", this.chkrefKotnoUnique.Checked);
            command.Parameters.AddWithValue("@MOUSEMODEREQ", this.chkMouseMode.Checked);
            string str = "NRA";
            if (this.rtbStubRA.Checked)
            {
                str = "RA";
            }
            else if (this.rtbStubRP.Checked)
            {
                str = "RP";
            }
            else if (this.rtbStubRNP.Checked)
            {
                str = "NRP";
            }
            else if (this.rtbStubNRA.Checked)
            {
                str = "NRA";
            }
            command.Parameters.AddWithValue("@BILLSTUBREQ", str);
            if (this.rtb50Paise.Checked)
            {
                command.Parameters.AddWithValue("@ROUNDTYPE", "50PAISE");
            }
            else
            {
                command.Parameters.AddWithValue("@ROUNDTYPE", "ONERUPEE");
            }
            command.Parameters.Add("RetVal", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            if (command.ExecuteNonQuery() > 0)
            {
                MessageBox.Show("Saved Successfully!");
                base.Close();
            }
            return 0;
        }
    }
}
