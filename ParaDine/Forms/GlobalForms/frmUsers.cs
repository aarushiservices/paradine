﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.GlobalForms
{
    public partial class frmUsers : Form
    {
        private Button btnExit;
        private Button btnTrans; 
        private ComboBox cmbHint;
        private ComboBox cmbLevel;
      //private IContainer components = null;
        private User EntId = new User();
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Panel PnlUsers;
        private ToolTip TTip;
        private TextBox txtAnswer;
        private TextBox txtName;
        private TextBox txtPwd;
        private TextBox txtRePwd;

        public frmUsers(string Mode, int ID, SecurityClass paramSecurity)
        {
            this.InitializeComponent();
            this.btnTrans.Text = Mode;
            this.EntId.LoadAttributes((long)ID);
            this.EntId.Security = paramSecurity;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (this.txtPwd.Text == this.txtRePwd.Text)
            {
                if (GlobalValidations.ValidateFields(this.PnlUsers, this.TTip))
                {
                    this.LoadEntities();
                    SqlTransaction sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
                    try
                    {
                        string str = this.btnTrans.Text.ToUpper();
                        if (str != null)
                        {
                            if (!(str == "&ADD"))
                            {
                                if (str == "&EDIT")
                                {
                                    goto Label_0096;
                                }
                            }
                            else
                            {
                                this.EntId.Add(sqlTrans, true);
                            }
                        }
                        goto Label_00A8;
                    Label_0096:
                        this.EntId.Modify(sqlTrans, true);
                    Label_00A8:
                        sqlTrans.Commit();
                        base.Close();
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(exception.Message, exception.Source);
                        sqlTrans.Rollback();
                    }
                }
            }
            else
            {
                MessageBox.Show("Please ReType the Password Correctly", "Error in Saving Data");
            }
        }

        private void frmUsers_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.EntId.Security.AllowRead)
                {
                    throw new Exception("Permission Denied");
                }
                this.RefreshData();
                this.LoadFields();
                new GlobalTheme().applyTheme(this);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                base.Close();
            }
        }

       

        private void LoadEntities()
        {
            this.EntId.UserId = Convert.ToInt16(this.txtName.Tag);
            this.EntId.UserName = Convert.ToString(this.txtName.Text);
            this.EntId.Password = Convert.ToString(this.txtPwd.Text);
            this.EntId.HintId = Convert.ToInt16(this.cmbHint.SelectedValue);
            this.EntId.HintAns = Convert.ToString(this.txtAnswer.Text);
            this.EntId.LevelID = Convert.ToInt16(this.cmbLevel.SelectedValue);
        }

        private void LoadFields()
        {
            this.txtName.Tag = Convert.ToString(this.EntId.UserId);
            this.txtName.Text = Convert.ToString(this.EntId.UserName);
            this.txtPwd.Text = Convert.ToString(this.EntId.Password);
            this.cmbHint.SelectedValue = Convert.ToInt16(this.EntId.HintId);
            this.txtAnswer.Text = Convert.ToString(this.EntId.HintAns);
            this.cmbLevel.SelectedValue = Convert.ToInt16(this.EntId.LevelID);
        }

        private void RefreshData()
        {
            try
            {
                GlobalFill.FillCombo("SELECT HINTID, HINT FROM GLB_HINT", this.cmbHint);
                GlobalFill.FillCombo("SELECT LEVELID, LEVELNAME FROM GLB_LEVEL", this.cmbLevel);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error in RefreshData");
            }
        }
    }
}
