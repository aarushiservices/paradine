﻿using ParaDine;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.IO.Ports;
using System.Windows.Forms;
using System.Xml;

namespace ParaDine.Forms.GlobalForms
{
    public partial class frmClientSettings : Form
    {
        private Button btnApply;
        private Button btnCancel;
        private CheckBox ChkKotStubReq;
        private CheckBox chkNegativeBill;
        private CheckBox chkPoleDisplay;
        private CheckBox chkStubReq;
        private ComboBox cmbBackEnd;
        private ComboBox cmbBillPrinter;
        private ComboBox cmbBillPrintingDevice;
        private ComboBox cmbFinancialYear;
        private ComboBox cmbKOTPrinter;
        private ComboBox cmbKOTPrintingDevice;
        private ComboBox cmbPoleDisplay;
        private ComboBox cmbServerName;
        private ComboBox cmbTerminalNo;
      //private IContainer components = null;
        private DataSet DS = new DataSet();
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private Panel pnlClientSettings;
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private SqlCommand sqlcmd = new SqlCommand();
        private TextBox txtDatabase;
        private DataSet XMLDS = new DataSet();

        public frmClientSettings()
        {
            this.InitializeComponent();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                XmlDocument document = new XmlDocument();
                document.Load(Application.StartupPath + @"\ClientSettings.xml");
                XmlNodeList list = document.SelectNodes("//CLIENTSETTINGS");
                foreach (XmlNode node in list)
                {
                    node.SelectNodes("//BACKEND").Item(0).InnerText = this.cmbBackEnd.Text;
                    node.SelectNodes("//SERVERNAME").Item(0).InnerText = this.cmbServerName.Text;
                    node.SelectNodes("//TERMINALNO").Item(0).InnerText = Convert.ToString(this.cmbTerminalNo.SelectedValue);
                    node.SelectNodes("//KOTPRINTERID").Item(0).InnerText = Convert.ToString(Convert.ToInt16(this.cmbKOTPrinter.SelectedValue));
                    node.SelectNodes("//BILLPRINTERID").Item(0).InnerText = Convert.ToString(Convert.ToInt16(this.cmbBillPrinter.SelectedValue));
                    node.SelectNodes("//DATABASE").Item(0).InnerText = this.txtDatabase.Text.Trim();
                    node.SelectNodes("//POLEDISPLAY").Item(0).InnerText = this.chkPoleDisplay.Checked.ToString().ToUpper();
                    node.SelectNodes("//POLE_DISPLAY_PORT").Item(0).InnerText = this.cmbPoleDisplay.Text;
                    node.SelectNodes("//FYEAR").Item(0).InnerText = this.cmbFinancialYear.Text;
                    node.SelectNodes("//KOTPRINTINGDEVICE").Item(0).InnerText = this.cmbKOTPrintingDevice.Text.ToUpper();
                    node.SelectNodes("//BILLPRINTINGDEVICE").Item(0).InnerText = this.cmbBillPrintingDevice.Text.ToUpper();
                    node.SelectNodes("//NEGATIVEQTYREQ").Item(0).InnerText = this.chkNegativeBill.Checked.ToString().ToUpper();
                }
                document.Save(Application.StartupPath + @"\ClientSettings.xml");
                if (MessageBox.Show("New Settings are Saved.\nWould U Like To Restart the Application Now..??", "Client Settings", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    Application.Restart();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void chkPoleDisplay_CheckedChanged(object sender, EventArgs e)
        {
            this.cmbPoleDisplay.Enabled = this.chkPoleDisplay.Checked;
        }
        private void frmClientSettings_Load(object sender, EventArgs e)
        {
            this.PopulateAll();
            this.LoadFields();
        }
        private void LoadFields()
        {
            new GlobalTheme().applyTheme(this);
            this.XMLDS.ReadXml(Application.StartupPath + @"\ClientSettings.xml");
            foreach (DataRow row in this.XMLDS.Tables[0].Rows)
            {
                this.cmbTerminalNo.SelectedValue = Convert.ToString(row["TERMINALNO"]);
                this.cmbKOTPrinter.SelectedValue = Convert.ToInt16(row["KOTPRINTERID"]);
                this.cmbBillPrinter.SelectedValue = Convert.ToInt16(row["BILLPRINTERID"]);
                this.chkPoleDisplay.Checked = Convert.ToBoolean(row["POLEDISPLAY"]);
                this.cmbPoleDisplay.Text = Convert.ToString(row["POLE_DISPLAY_PORT"]);
                this.cmbFinancialYear.Text = Convert.ToString(row["FYEAR"]);
                this.cmbKOTPrintingDevice.Text = Convert.ToString(row["KOTPRINTINGDEVICE"]);
                this.cmbBillPrintingDevice.Text = Convert.ToString(row["BILLPRINTINGDEVICE"]);
                this.chkNegativeBill.Checked = Convert.ToBoolean(row["NEGATIVEQTYREQ"]);
            }
            this.chkPoleDisplay_CheckedChanged(new object(), new EventArgs());
        }

        private void LoadFinancialYear()
        {
            this.cmbFinancialYear.Items.Clear();
            if (this.txtDatabase.Text.Trim() != "")
            {
                string str = "";
                GlobalFill.FillDataSet("SP_DATABASES", "DBASE", this.DS, this.SDA);
                for (int i = 0; i <= (this.DS.Tables["DBASE"].Rows.Count - 1); i++)
                {
                    str = this.DS.Tables["DBASE"].Rows[i][0].ToString().Trim();
                    if (str.Length != (this.txtDatabase.Text.Trim().Length + 4))
                    {
                        break;
                    }
                    if (str.Substring(0, this.txtDatabase.Text.Trim().Length).ToUpper() == this.txtDatabase.Text.Trim().ToUpper())
                    {
                        this.cmbFinancialYear.Items.Add(str.Substring(str.Length - 5, 4));
                    }
                }
            }
        }

        private void LoadServers(DataTable dt)
        {
            foreach (DataRow row in dt.Rows)
            {
                string item = "";
                if (row[1].ToString().Trim() != "")
                {
                    item = row[0] + @"\" + row[1];
                }
                else
                {
                    item = row[0].ToString().Trim();
                }
                if (item != "")
                {
                    this.cmbServerName.Items.Add(item);
                }
            }
        }

        private void PopulateAll()
        {
            DataTable dataSources = SqlDataSourceEnumerator.Instance.GetDataSources();
            this.LoadServers(dataSources);
            this.RefreshData();
            this.LoadFinancialYear();
        }

        private void RefreshData()
        {
            GlobalFill.FillCombo("SELECT PRINTERID, PRINTERNAME + '(' + CAST(DESCRIPTION AS VARCHAR) + ')' FROM RES_PRINTER", this.cmbBillPrinter);
            GlobalFill.FillCombo("SELECT PRINTERID, PRINTERNAME + '(' + CAST(DESCRIPTION AS VARCHAR) + ')' FROM RES_PRINTER", this.cmbKOTPrinter);
            GlobalFill.FillCombo("SELECT TERMINALID, TERMINALNAME FROM GLB_TERMINAL ORDER BY 2", this.cmbTerminalNo);
            foreach (string str in SerialPort.GetPortNames())
            {
                this.cmbPoleDisplay.Items.Add(str);
            }
            this.cmbBackEnd.Items.Clear();
            this.cmbBackEnd.Items.Add("SQL2005");
            this.cmbBackEnd.Items.Add("SQLEXPRESSEDITION");
            this.cmbKOTPrintingDevice.Items.Clear();
            foreach (string str2 in PrinterSettings.InstalledPrinters)
            {
                this.cmbKOTPrintingDevice.Items.Add(str2);
                this.cmbBillPrintingDevice.Items.Add(str2);
            }
        }

        private void txtDatabase_Leave(object sender, EventArgs e)
        {
            this.LoadFinancialYear();
        }
    }
}
