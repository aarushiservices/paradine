﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.GlobalForms
{
    public partial class frmBrowseData : Form
    {
        internal Button btnCondition;
        private Button Cancel_Button;
        internal ComboBox cmbFilter;
       // private IContainer components = null;
        internal DataGridView DgvItems;
        private Control GlbCntrl;
        internal Label Label1;
        internal Label Label2;
        internal Label Label3;
        internal Label lblHeader;
        internal ListView lvwItems;
        private Button OK_Button;
        private SqlDataAdapter sda = new SqlDataAdapter();
        private DataTable searchDT = new DataTable();
        private string strCondition = "";
        internal TextBox txtFilter;
        public frmBrowseData(DataTable dt, Control cntrl, string HdrName, int DefaultFilterColomn)
        {
            this.InitializeComponent();
            this.searchDT = dt;
            this.GlbCntrl = cntrl;
            this.lblHeader.Text = HdrName;
            this.cmbFilter.Items.Clear();
            for (int i = 1; i < dt.Columns.Count; i++)
            {
                this.cmbFilter.Items.Add(dt.Columns[i].Caption);
            }
            this.cmbFilter.SelectedIndex = DefaultFilterColomn - 1;
            this.PopulateView(this.searchDT);
            this.searchDT.DefaultView.RowFilter = "";
        }
        private void btnCondition_Click(object sender, EventArgs e)
        {
            if ((this.cmbFilter.Text != "") && !(this.txtFilter.Text == ""))
            {
                this.strCondition = this.strCondition + this.cmbFilter.Text + " LIKE '%" + this.txtFilter.Text + "%' AND ";
                this.txtFilter.Text = "";
                this.txtFilter.Focus();
            }
        }
        private void Cancel_Button_Click(object sender, EventArgs e)
        {
            base.DialogResult = DialogResult.Cancel;
            this.strCondition = "";
            base.Close();
        }
        private void DgvItems_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.OK_Button_Click(sender, e);
            }
        }
        private void frmBrowseData_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.DgvItems.Refresh();
            this.txtFilter.Focus();
        }

        private void OK_Button_Click(object sender, EventArgs e)
        {
            base.DialogResult = DialogResult.OK;
            if (this.DgvItems.SelectedRows.Count > 0)
            {
                this.GlbCntrl.Tag = (long)this.DgvItems.SelectedRows[0].Cells[0].Value;
                this.GlbCntrl.Text = (string)this.DgvItems.SelectedRows[0].Cells[1].Value;
                this.GlbCntrl.Focus();
                this.strCondition = "";
                base.Close();
            }
            else if (this.DgvItems.Rows.Count > 0)
            {
                this.GlbCntrl.Tag = (int)this.DgvItems.Rows[0].Cells[0].Value;
                this.GlbCntrl.Text = (string)this.DgvItems.Rows[0].Cells[1].Value;
                this.GlbCntrl.Focus();
                this.strCondition = "";
                base.Close();
            }
        }
        private void PopulateView(DataTable dt)
        {
            this.DgvItems.DataSource = dt;
            this.DgvItems.Columns[0].Visible = false;
            for (int i = 1; i < this.DgvItems.Columns.Count; i++)
            {
                if (this.DgvItems.Columns[i].HeaderText.IndexOf(" (Rs)") == 0)
                {
                    this.DgvItems.Columns[i].DefaultCellStyle.Format = "N2";
                    this.DgvItems.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    this.DgvItems.Columns[i].Width = 70;
                }
                if (i == 2)
                {
                    this.DgvItems.Columns[i].Width = 400;
                }
            }
        }
        private void txtFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                if (this.DgvItems.Rows.Count > 0)
                {
                    this.DgvItems.Focus();
                    this.DgvItems.Rows[0].Selected = true;
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    this.OK_Button_Click(sender, e);
                }
                else if (e.KeyCode == Keys.F1)
                {
                    this.btnCondition_Click(sender, e);
                }
            }
        }

        private void txtFilter_TextChanged(object sender, EventArgs e)
        {
            DataTable table = new DataTable();
            if (this.cmbFilter.Text != "")
            {
                this.searchDT.DefaultView.RowFilter = this.strCondition + this.cmbFilter.Text + " LIKE '%" + this.txtFilter.Text + "%'";
                this.DgvItems.DataSource = this.searchDT.DefaultView;
                this.Label1.Text = this.DgvItems.Rows.Count.ToString();
            }
        }
    }
}
