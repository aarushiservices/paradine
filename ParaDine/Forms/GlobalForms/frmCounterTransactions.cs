﻿using ParaSysCom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.GlobalForms
{
    public partial class frmCounterTransactions : Form
    {
        private Button btnExit;
        private Button BtnTrans;
        private Button BtnTransaction;
        internal ComboBox cmbMachineNo;
        internal ComboBox cmbpayType;
       // private IContainer components;
        private DataSet DS;
        internal DateTimePicker dtpVoucherDate;
        private CounterTrans EntID;
        private NumControl gvAmount;
        internal Label Label1;
        internal Label label2;
        private Label label3;
        internal Label Label4;
        internal Label label5;
        private Panel pnlCounterTrans;
        private SqlDataAdapter SDA;
        private ToolTip TTip;
        private TextBox txtVoucherNo;
        public frmCounterTransactions()
        {
            this.components = null;
            this.DS = new DataSet();
            this.SDA = new SqlDataAdapter();
            this.EntID = new CounterTrans();
            this.InitializeComponent();
        }
        public frmCounterTransactions(string Mode, long ID)
        {
            this.components = null;
            this.DS = new DataSet();
            this.SDA = new SqlDataAdapter();
            this.EntID = new CounterTrans();
            this.InitializeComponent();
            this.BtnTrans.Text = Mode;
            this.EntID.LoadAttributes(ID);
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void BtnTrans_Click(object sender, EventArgs e)
        {
            if (ParaDine.GlobalValidations.ValidateFields(this.pnlCounterTrans, this.TTip))
            {
                this.LoadEntities();
                SqlTransaction sqlTrans = ParaDine.GlobalVariables.SqlConn.BeginTransaction();
                try
                {
                    string str = this.BtnTrans.Text.ToUpper();
                    if (str != null)
                    {
                        if (!(str == "&ADD"))
                        {
                            if (str == "&EDIT")
                            {
                                goto Label_007D;
                            }
                            if (str == "&DELETE")
                            {
                                goto Label_008D;
                            }
                        }
                        else
                        {
                            this.EntID.Add(sqlTrans, true);
                        }
                    }
                    goto Label_009F;
                Label_007D:
                    this.EntID.Modify(sqlTrans, true);
                    goto Label_009F;
                Label_008D:
                    this.EntID.Delete(sqlTrans, true);
                Label_009F:
                    sqlTrans.Commit();
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error Occured in CounterTrans");
                    sqlTrans.Rollback();
                }
            }
        }
        private void BtnTransaction_Click(object sender, EventArgs e)
        {
            string mode = "";
            if (this.EntID.MoneyMaster.MoneyTransCollection.Count > 0)
            {
                mode = "&Edit";
            }
            else
            {
                mode = "&Add";
            }
            frmMoneyTrans trans = new frmMoneyTrans(mode, Convert.ToInt64(this.EntID.MoneyMaster.MoneyMasterID), Convert.ToDouble(this.gvAmount.Value), true);
            trans.ShowDialog();
            if (trans.DialogResult == DialogResult.Yes)
            {
                this.EntID.MoneyMaster = trans.GetMoneyMaster;
                this.EntID.MoneyMaster.TransactionSource = "CTRANS";
                this.EntID.MoneyMaster.TransactionSourceID = Convert.ToInt64(this.txtVoucherNo.Tag);
                this.LoadFields();
            }
        }
        private void cmbpayType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cmbpayType.Text == "PAYMENT")
            {
                this.BtnTransaction.Text = "Make Payment";
            }
            else
            {
                this.BtnTransaction.Text = "Make Receipt";
            }
        }    
        private void frmCounterTransactions_Load(object sender, EventArgs e)
        {
            this.RefreshData();
            this.LoadFields();
        }
        private void LoadEntities()
        {
            this.EntID.VoucherID = Convert.ToInt64(this.txtVoucherNo.Tag);
            this.EntID.VoucherNo = Convert.ToString(this.txtVoucherNo.Text);
            this.EntID.VoucherDate = Convert.ToDateTime(this.dtpVoucherDate.Text);
            this.EntID.MachineNo = Convert.ToInt32(this.cmbMachineNo.SelectedValue);
            if (this.cmbpayType.Text == "PAYMENT")
            {
                this.EntID.PayType = 'P';
            }
            else if (this.cmbpayType.Text == "RECEIPT")
            {
                this.EntID.PayType = 'R';
            }
        }
        private void LoadFields()
        {
            this.txtVoucherNo.Tag = Convert.ToInt64(this.EntID.VoucherID);
            if (Convert.ToInt64(this.EntID.VoucherID) <= 0L)
            {
                this.EntID.VoucherNo = this.EntID.MaxCode();
            }
            this.txtVoucherNo.Text = this.EntID.VoucherNo;
            this.dtpVoucherDate.Value = Convert.ToDateTime(this.EntID.VoucherDate);
            this.cmbMachineNo.SelectedValue = Convert.ToInt32(this.EntID.MachineNo);
            if (Convert.ToChar(this.EntID.PayType) == 'P')
            {
                this.cmbpayType.Text = "PAYMENT";
            }
            else if (Convert.ToChar(this.EntID.PayType) == 'R')
            {
                this.cmbpayType.Text = "RECEIPT";
            }
            this.gvAmount.Value = Convert.ToDecimal(this.EntID.MoneyMaster.GetTransAmount);
        }
        private void RefreshData()
        {
            ParaDine.GlobalFill.FillCombo("SELECT TERMINALID, TERMINALNAME FROM GLB_TERMINAL", this.cmbMachineNo);
            this.cmbpayType.Items.Add("PAYMENT");
            this.cmbpayType.Items.Add("RECEIPT");
            this.cmbpayType.SelectedIndex = 0;
            this.cmbMachineNo.SelectedValue = ParaDine.GlobalVariables.TerminalNo;
        }
    }
}
