﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Settings
{
    public partial class FRMSTOCKSETTINGS : Form
    {
        private Button btnApply;
        private Button btnCancel;
       // private IContainer components = null;
        private DataGridView dgStockPoint;
        private SqlCommand sqlcmd = new SqlCommand();
        private SqlTransaction SqlTrans;

        public FRMSTOCKSETTINGS()
        {
            this.InitializeComponent();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                this.SqlTrans = GlobalVariables.SqlConn.BeginTransaction();
                this.sqlcmd = new SqlCommand("TRUNCATE TABLE RES_STOCKSETTINGS", GlobalVariables.SqlConn);
                this.sqlcmd.Transaction = this.SqlTrans;
                if (this.sqlcmd.ExecuteNonQuery() > 0)
                {
                    throw new Exception("Error raised while saving");
                }
                if (!this.UpdateValues("INSERT", this.SqlTrans, this.dgStockPoint))
                {
                    throw new Exception("Error while saving Stock settings");
                }
                this.SqlTrans.Commit();
                MessageBox.Show("Applied Successfully");
                base.Close();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                this.SqlTrans.Rollback();
            }
            finally
            {
                this.sqlcmd.Dispose();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }
        private void FRMSTOCKSETTINGS_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.RefreshData();
        }

  

        private void RefreshData()
        {
            this.dgStockPoint.DataSource = GlobalFill.FillDataTable("EXEC RES_PROC_STOCKSETTINGS 'LIST'");
            this.dgStockPoint.Columns[0].Visible = false;
        }

        private bool UpdateValues(string TransType, SqlTransaction SqlTrans, DataGridView DGVIEW)
        {
            foreach (DataRow row in ((DataTable)DGVIEW.DataSource).Rows)
            {
                this.sqlcmd = new SqlCommand();
                this.sqlcmd.Connection = GlobalVariables.SqlConn;
                this.sqlcmd.Transaction = SqlTrans;
                this.sqlcmd.CommandText = "RES_PROC_STOCKSETTINGS";
                this.sqlcmd.CommandType = CommandType.StoredProcedure;
                this.sqlcmd.Parameters.AddWithValue("@TRANSTYPE", TransType);
                this.sqlcmd.Parameters.AddWithValue("@STOCKPOINTID", row["STOCKPOINTID"]);
                this.sqlcmd.Parameters.AddWithValue("@NEGATIVEQTY", row["NEGATIVE_QTY"]);
                this.sqlcmd.Parameters.AddWithValue("@NEGATIVEQTYWITHMSG", row["DISPLAY_MSG"]);
                this.sqlcmd.Parameters.AddWithValue("@DONTFORWARD", row["DONTFORWARD"]);
                this.sqlcmd.Parameters.Add("RetVal", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                if (this.sqlcmd.ExecuteNonQuery() <= 0)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
