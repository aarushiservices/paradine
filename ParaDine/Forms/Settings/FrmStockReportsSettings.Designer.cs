﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaDine.Forms.Settings
{
    partial class FrmStockReportsSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpfields = new System.Windows.Forms.Panel();
            this.chkstockreport = new System.Windows.Forms.CheckBox();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.grpfields.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpfields
            // 
            this.grpfields.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.grpfields.Controls.Add(this.chkstockreport);
            this.grpfields.Location = new System.Drawing.Point(4, 12);
            this.grpfields.Name = "grpfields";
            this.grpfields.Size = new System.Drawing.Size(282, 200);
            this.grpfields.TabIndex = 0;
            // 
            // chkstockreport
            // 
            this.chkstockreport.AutoSize = true;
            this.chkstockreport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.chkstockreport.Location = new System.Drawing.Point(36, 80);
            this.chkstockreport.Name = "chkstockreport";
            this.chkstockreport.Size = new System.Drawing.Size(213, 20);
            this.chkstockreport.TabIndex = 0;
            this.chkstockreport.Text = "Stock To Be Carry Forward";
            this.chkstockreport.UseVisualStyleBackColor = true;
            // 
            // btnApply
            // 
            this.btnApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApply.Location = new System.Drawing.Point(41, 223);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(90, 30);
            this.btnApply.TabIndex = 4;
            this.btnApply.Text = "&Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(152, 223);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(90, 30);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // FrmStockReportsSettings
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.grpfields);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmStockReportsSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "StockReportsSettings";
            this.Load += new System.EventHandler(this.FrmStockReportsSettings_Load);
            this.grpfields.ResumeLayout(false);
            this.grpfields.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
    }
}