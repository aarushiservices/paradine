﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Settings
{
    public partial class FrmStockReportsSettings : Form
    {
        private Button btnApply;
        private Button btnCancel;
        private CheckBox chkstockreport;
        //private IContainer components = null;
        private DataSet DS = new DataSet();
        private Panel grpfields;
        private SqlDataAdapter SDA = new SqlDataAdapter();
        private SqlCommand sqlcmd = new SqlCommand();
        private SqlTransaction SqlTrans;
        private string StrSql;

        public FrmStockReportsSettings()
        {
            this.InitializeComponent();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                SqlTransaction sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
                this.UpdateValues("INSERT", sqlTrans);
                sqlTrans.Commit();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }
        private void FrmStockReportsSettings_Load(object sender, EventArgs e)
        {
            new GlobalTheme().applyTheme(this);
            this.LoadFields();
        }
        private void LoadFields()
        {
            this.StrSql = "SELECT * FROM RES_STOCKREPORTSETTINGS";
            GlobalFill.FillDataSet(this.StrSql, "STOCKREPORTSETTINGS", this.DS, this.SDA);
            if (this.DS.Tables["STOCKREPORTSETTINGS"].Rows.Count > 0)
            {
                foreach (DataRow row in this.DS.Tables["STOCKREPORTSETTINGS"].Rows)
                {
                    this.chkstockreport.Checked = Convert.ToBoolean(row["STOCKTOBEFORWARD"]);
                }
            }
        }

        private int UpdateValues(string TransType, SqlTransaction SqlTrans)
        {
            SqlCommand command = new SqlCommand
            {
                Connection = GlobalVariables.SqlConn,
                Transaction = SqlTrans,
                CommandText = "RES_PROC_STOCKREPORTSETTINGS",
                CommandType = CommandType.StoredProcedure
            };
            command.Parameters.AddWithValue("@TRANSTYPE", TransType);
            command.Parameters.AddWithValue("@STOCKREPORTSETTINGID", 1);
            command.Parameters.AddWithValue("@STOCKTOBEFORWARD", this.chkstockreport.Checked);
            command.Parameters.Add("RetVal", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            if (command.ExecuteNonQuery() > 0)
            {
                MessageBox.Show("Saved Successfully!");
                base.Close();
            }
            return 0;
        }
    }
}
