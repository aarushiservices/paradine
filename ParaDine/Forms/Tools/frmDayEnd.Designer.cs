﻿namespace ParaDine.Forms.Tools
{
    partial class frmDayEnd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpNxtBussDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpPrstBussDate = new System.Windows.Forms.DateTimePicker();
            this.btnDayEnd = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.dtpNxtBussDate);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.dtpPrstBussDate);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(335, 74);
            this.panel1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(149, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "==>>";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(189, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Next BusinessDate :";
            // 
            // dtpNxtBussDate
            // 
            this.dtpNxtBussDate.CustomFormat = "dd/MMM/yy";
            this.dtpNxtBussDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNxtBussDate.Location = new System.Drawing.Point(187, 34);
            this.dtpNxtBussDate.Name = "dtpNxtBussDate";
            this.dtpNxtBussDate.Size = new System.Drawing.Size(123, 20);
            this.dtpNxtBussDate.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Present BusinessDate :";
            // 
            // dtpPrstBussDate
            // 
            this.dtpPrstBussDate.CustomFormat = "dd/MMM/yy";
            this.dtpPrstBussDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPrstBussDate.Location = new System.Drawing.Point(23, 34);
            this.dtpPrstBussDate.Name = "dtpPrstBussDate";
            this.dtpPrstBussDate.Size = new System.Drawing.Size(123, 20);
            this.dtpPrstBussDate.TabIndex = 0;
            // 
            // btnDayEnd
            // 
            this.btnDayEnd.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnDayEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDayEnd.Location = new System.Drawing.Point(95, 95);
            this.btnDayEnd.Name = "btnDayEnd";
            this.btnDayEnd.Size = new System.Drawing.Size(75, 23);
            this.btnDayEnd.TabIndex = 1;
            this.btnDayEnd.Text = "DayEnd";
            this.btnDayEnd.UseVisualStyleBackColor = true;
            this.btnDayEnd.Click += new System.EventHandler(this.btnDayEnd_Click);
            // 
            // btnExit
            // 
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(189, 95);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmDayEnd
            // 
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(359, 128);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnDayEnd);
            this.Controls.Add(this.panel1);
            this.Name = "frmDayEnd";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DayEnd";
            this.Load += new System.EventHandler(this.frmDayEnd_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}