﻿using ParaDine.Forms.ReportForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaDine.Forms.Tools
{
    public partial class frmDayEnd : Form
    {
        private Button btnDayEnd;
        private Button btnExit;
       // private IContainer components = null;
        private DateTimePicker dtpNxtBussDate;
        private DateTimePicker dtpPrstBussDate;
        public Terminal EntId = new Terminal(GlobalVariables.TerminalNo);
        private Label label1;
        private Label label2;
        private Label label3;
        private Panel panel1;
        private SqlCommand SqlCmd = new SqlCommand();
        public string StrBackupPath = "";
        private string StrSql = "";
        private Level uLevel;
        public frmDayEnd()
        {
            this.InitializeComponent();
        }
        private void btnDayEnd_Click(object sender, EventArgs e)
         {
            if (GlobalVariables.TerminalNo <= 1)
            {
                DateTime now = DateTime.Now;
                new frmRptDayReport(now, true).btnPrint_Click(sender, e);
                EmailSettings pESetting = new EmailSettings(3L);
                GlobalFunctions.SendEmailQueue(pESetting, pESetting.EmailTo, "Day Report", Application.StartupPath + @"\Temp\DAYREPORT" + now.ToString("ddMMMyyyy") + ".pdf");
                GlobalFunctions.ProcessEmailQueue();
            }
            if (new StockPoint((long)GlobalVariables.StockPointId).IsBillPoint)
            {
                this.StrSql = "SELECT TABLEID FROM RES_TABLE";
                DataTable table = new DataTable("PendingTables");
                table = GlobalFill.FillDataTable(this.StrSql);
                foreach (DataRow row in table.Rows)
                {
                    Table table2 = new Table(Convert.ToInt32(row["TABLEID"]));
                    if (table2.PendingOrders().Rows.Count > 0)
                    {
                        if (MessageBox.Show("Table " + table2.TableName + " not Settled.. Do u want to Cancel this order and Continue DayEnd??", "PendingOrders", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            return;
                        }
                        foreach (DataRow row2 in table2.PendingOrders().Rows)
                        {
                            new CustOrderMaster(Convert.ToInt64(row2["CUSTORDERMASTERID"])).CancelBill();
                        }
                    }
                }
            }
            this.SqlCmd = new SqlCommand();
            this.SqlCmd.Connection = GlobalVariables.SqlConn;
            this.SqlCmd.CommandType = CommandType.StoredProcedure;
            this.SqlCmd.CommandText = "RES_PROC_BACKUP";
            this.SqlCmd.Parameters.Clear();
            this.SqlCmd.Parameters.AddWithValue("@DESTINATIONFILE", this.StrBackupPath + @"\PARADINE_" + DateTime.Now.ToString(@"dd\MMM\yy") + ".BAK");
            this.SqlCmd.Parameters.AddWithValue("@SOURCEDATABASE", GlobalVariables.DBaseName);
            SqlParameter parameter = this.SqlCmd.Parameters["@DESTINATIONFILE"];
            parameter.Direction = ParameterDirection.InputOutput;
            this.SqlCmd.ExecuteNonQuery();
            if (GlobalVariables.MobileNoStr != "")
            {
                this.GenerateSalesSMS();
                if (Sms.IsConnectionAvailable())
                {
                    Sms.SendSMSQueue();
                }
            }
            if (!(Convert.ToString(parameter.Value) != ""))
            {
                throw new Exception(@"Problem in Databackup.. Please Check E:\Databackup");
            }
            MessageBox.Show("Backup Done Successfully to " + Convert.ToString(parameter.Value));
            if (GlobalVariables.TerminalNo == 0)
            {
                this.StrSql = "SELECT * FROM GLB_TERMINAL ";
                DataTable table3 = new DataTable("TERMINAL");
                table3 = GlobalFill.FillDataTable(this.StrSql);
                foreach (DataRow row3 in table3.Rows)
                {
                    if (Convert.ToDateTime(row3["BUSINESSDATE"]) == Convert.ToDateTime(this.dtpPrstBussDate.Value))
                    {
                        this.StrSql = string.Concat(new object[] { "UPDATE GLB_TERMINAL SET BUSINESSDATE = '", this.dtpNxtBussDate.Value, "' WHERE TERMINALID = ", Convert.ToInt16(row3["TERMINALID"]) });
                        this.SqlCmd = new SqlCommand(this.StrSql, GlobalVariables.SqlConn);
                        this.SqlCmd.ExecuteNonQuery();
                    }
                    else
                    {
                        MessageBox.Show("DAY END CAN'T PROCESS FURTHER... AS MNO (" + Convert.ToInt16(row3["TERMINALID"]) + ") WITH DIFFERENT BUSINESSDATE");
                        Application.Exit();
                    }
                }
                MessageBox.Show("Day End Successful. \n Application is Exiting Now....", "Day End", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                Application.Exit();
            }
            else if (this.EntId.MakeDayEnd(this.dtpNxtBussDate.Value))
            {
                MessageBox.Show("Day End Successful. \n Application is Exiting Now....", "Day End", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                Application.Exit();
            }
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            base.Close();
        }
        private void frmDayEnd_Load(object sender, EventArgs e)
        {
            this.uLevel = new Level(new User(GlobalVariables.UserID).LevelID);
            this.dtpPrstBussDate.Value = this.EntId.BusinessDate;
            this.dtpNxtBussDate.Value = this.EntId.BusinessDate.AddDays(1.0);
            this.dtpNxtBussDate.Enabled = false;
            this.dtpPrstBussDate.Enabled = false;
            if (!this.uLevel.IsAdmin)
            {
                this.dtpNxtBussDate.MinDate = this.EntId.BusinessDate.AddDays(1.0);
            }
            this.StrBackupPath = Convert.ToString(GlobalFunctions.GetQueryValue("SELECT BACKUPPATH FROM RES_BILLSETTINGS"));
        }
        private void GenerateSalesSMS()
        {
            string strSql = "SELECT SUM(NETAMOUNT) FROM RES_VW_CUSTORDERMASTER  WHERE CONVERT(DATETIME, CONVERT(VARCHAR, CUSTORDERDATE, 112)) = '" + this.EntId.BusinessDate.ToString("dd/MMM/yy") + "'";
            if (GlobalVariables.TerminalNo > 0)
            {
                strSql = strSql + " AND MACHINENO = " + GlobalVariables.TerminalNo.ToString();
            }
            string str2 = Convert.ToString(GlobalFunctions.GetQueryValue(strSql));
            SqlTransaction sqlTrans = GlobalVariables.SqlConn.BeginTransaction();
            foreach (string str3 in GlobalVariables.MobileNoStr.Split(new char[] { ';' }))
            {
                string str4 = "";
                if (GlobalVariables.TerminalNo > 0)
                {
                    str4 = str4 + "Machine: " + GlobalVariables.TerminalNo.ToString();
                }
                else
                {
                    str4 = str4 + "All Machines ";
                }
                string str5 = str4;
                str4 = str5 + "\nSales for " + this.EntId.BusinessDate.ToString("dd/MMM/yy") + " : " + str2;
                new Sms { Message = str4, MobileNo = str3, Added = true, SmsDate = GlobalVariables.BusinessDate, Response = "" }.Add(sqlTrans, false);
            }
            sqlTrans.Commit();
        }

    
    }
}
