﻿namespace ParaDinePrint
{
    using Microsoft.VisualBasic;
    using System;

    public class ClsRptDosPrint
    {
        private bool AmtInWrds;
        private int BackFeeds;
        private bool CutterRequired;
        private bool DblSdRequired;
        private int DblSdSpace;
        private int FtrLines;
        private int HdrLines;
        private bool LineRequired;
        private int LnsPerPage;
        private short NoOfPrintsReq;
        private bool PgBrkRequired;
        private string[] PgHeader = new string[6];
        private int PgHeight;
        private bool PgNoRequired;
        private bool[] RptDoubleRequired = new bool[5];
        private string[] RptFooter = new string[4];
        private bool RptHdrRequired;
        private string[] RptHeader = new string[5];
        private string RptType;
        private bool SlNoRequired;
        private bool VrtlRptTot;

        public bool AmountInWords
        {
            get
            {
                return this.AmtInWrds;
            }
            set
            {
                this.AmtInWrds = value;
            }
        }

        public int BackFeedLines
        {
            get
            {
                return this.BackFeeds;
            }
            set
            {
                this.BackFeeds = value;
            }
        }

        public bool Cutter
        {
            get
            {
                return this.CutterRequired;
            }
            set
            {
                this.CutterRequired = value;
            }
        }

        public bool DoubleSideRequired
        {
            get
            {
                return this.DblSdRequired;
            }
            set
            {
                this.DblSdRequired = value;
            }
        }

        public int DoubleSideSpace
        {
            get
            {
                return this.DblSdSpace;
            }
            set
            {
                this.DblSdSpace = value;
            }
        }

        public int FooterLines
        {
            get
            {
                return this.FtrLines;
            }
            set
            {
                this.FtrLines = value;
            }
        }

        public int HeaderLines
        {
            get
            {
                return this.HdrLines;
            }
            set
            {
                this.HdrLines = value;
            }
        }

        public int LinesPerPage
        {
            get
            {
                return this.LnsPerPage;
            }
            set
            {
                this.LnsPerPage = value;
            }
        }

        public bool LinesReq
        {
            get
            {
                return this.LineRequired;
            }
            set
            {
                this.LineRequired = value;
            }
        }

        public object NoOfPgHeaders
        {
            get
            {
                return Information.UBound(this.PgHeader, 1);
            }
        }

        public short NoOfPrints
        {
            get
            {
                return this.NoOfPrintsReq;
            }
            set
            {
                this.NoOfPrintsReq = value;
            }
        }

        public object NoOfRptHeaders
        {
            get
            {
                return Information.UBound(this.RptHeader, 1);
            }
        }

        public bool PageBrkRequired
        {
            get
            {
                return this.PgBrkRequired;
            }
            set
            {
                this.PgBrkRequired = value;
            }
        }

        public string this[int index]
        {
            get
            {
                return this.PgHeader[index];
            }
            set
            {
                this.PgHeader[index] = value;
            }
        }

        public int PageHeight
        {
            get
            {
                return this.PgHeight;
            }
            set
            {
                this.PgHeight = value;
            }
        }

        public bool PageNosRequired
        {
            get
            {
                return this.PgNoRequired;
            }
            set
            {
                this.PgNoRequired = value;
            }
        }
        //public bool this[int index]
        //{
        //    get
        //    {
        //        return this.RptDoubleRequired[index];
        //    }
        //    set
        //    {
        //        this.RptDoubleRequired[index] = value;
        //    }
        //}

        //public string this[int index]
        //{
        //    get
        //    {
        //        return this.RptFooter[index];
        //    }
        //    set
        //    {
        //        this.RptFooter[index] = value;
        //    }
        //}

        //public string this[int Index]
        //{
        //    get
        //    {
        //        return this.RptHeader[Index];
        //    }
        //    set
        //    {
        //        this.RptHeader[Index] = value;
        //    }
        //}

        public string ReportType
        {
            get
            {
                return this.RptType;
            }
            set
            {
                this.RptType = value;
            }
        }
        public bool RptHdrReq
        {
            get
            {
                return this.RptHdrRequired;
            }
            set
            {
                this.RptHdrRequired = value;
            }
        }

        public bool SerialNoRequired
        {
            get
            {
                return this.SlNoRequired;
            }
            set
            {
                this.SlNoRequired = value;
            }
        }

        public bool VerticalReportTotal
        {
            get
            {
                return this.VrtlRptTot;
            }
            set
            {
                this.VrtlRptTot = value;
            }
        }

        public string get_PageHeader(int index)
        {
            return this.PgHeader[index];
        }

        public void set_PageHeader(int index, String Value)
        {
            this.PgHeader[index] = Value;
        }

        public string get_ReportHeader(int index)
        {
            return this.RptHeader[index];
        }

        public void set_ReportHeader(int index,string Value)
        {
            this.RptHeader[index] = Value;
        }

        public bool get_RepDubReq(int index)
        {
            return this.RptDoubleRequired[index];
        }

        public void set_RepDubReq(int index,bool Value)
        {
            this.RptDoubleRequired[index] = Value;
        }

        public string get_ReportFooter(int index)
        {
            return this.RptFooter[index];
        }

        public void set_ReportFooter(int index,string Value)
        {
            this.RptFooter[index] = Value;
        }
    }
}

