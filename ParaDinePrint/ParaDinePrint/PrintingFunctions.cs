﻿namespace ParaDinePrint
{
    using Microsoft.VisualBasic;
    using Microsoft.VisualBasic.CompilerServices;
    using System;
    using System.Collections;
    using System.Data;
    using System.Data.SqlClient;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class PrintingFunctions
    {
        private object FillDataSet(string strQry, string dsName, DataSet sqlDS, SqlDataAdapter sda, [Optional, DefaultParameterValue(null)] SqlConnection ParamConn)
        {
            object obj2;
            try
            {
                sda = new SqlDataAdapter(strQry, ParamConn);
                sda.SelectCommand.CommandTimeout = 0x3e8;
                if (sqlDS.Tables.Contains(dsName))
                {
                    sqlDS.Tables.Remove(dsName);
                }
                sda.Fill(sqlDS, dsName);
                obj2 = true;
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "FillDataSet Function");
                obj2 = false;
                ProjectData.ClearProjectError();
                return obj2;
                ProjectData.ClearProjectError();
            }
            return obj2;
        }

        public bool SETCOLDATA(ref ClsColDosPrint[] ParamPrnItem, string RptName, DataSet Ds, SqlDataAdapter SDA, int PRNID, SqlConnection Conn)
        {
            bool flag;
            try
            {
                string strQry = "SELECT D.* FROM RES_REPORTDETAILS D, RES_REPORTSETTINGS S  WHERE S.REPORTID = D.REPORTID AND S.PRINTERID = '" + Conversions.ToString(PRNID) + "' AND REPORTNAME = '" + RptName + "' ORDER BY PRIORITY";
                this.FillDataSet(strQry, "REPORTCOLS", Ds, SDA, Conn);
                if (Ds.Tables["REPORTCOLS"].Rows.Count > 0)
                {
                    IEnumerator enumerator = null;
                    int index = 0;
                    try
                    {
                        enumerator = Ds.Tables["REPORTCOLS"].Rows.GetEnumerator();
                        while (enumerator.MoveNext())
                        {
                            DataRow current = (DataRow) enumerator.Current;
                            ParamPrnItem = (ClsColDosPrint[]) Utils.CopyArray((Array) ParamPrnItem, new ClsColDosPrint[index + 1]);
                            ParamPrnItem[index] = new ClsColDosPrint();
                            ParamPrnItem[index].ColumnPosition = Conversions.ToInteger(current["ColPos"]);
                            ParamPrnItem[index].ColumnName = Conversions.ToString(Interaction.IIf(Information.IsDBNull(RuntimeHelpers.GetObjectValue(current["ColName"])), "", RuntimeHelpers.GetObjectValue(current["ColName"])));
                            ParamPrnItem[index].ColumnWidth = Conversions.ToInteger(current["ColWidth"]);
                            ParamPrnItem[index].ColumnDoulbeWidth = Conversions.ToBoolean(current["ColDblWidth"]);
                            ParamPrnItem[index].ColumnAlignment = Conversions.ToInteger(current["ColAlignment"]);
                            ParamPrnItem[index].ColumnDataType = Conversions.ToString(Interaction.IIf(Information.IsDBNull(RuntimeHelpers.GetObjectValue(current["ColType"])), "", RuntimeHelpers.GetObjectValue(current["ColType"])));
                            ParamPrnItem[index].ColumnLineBreak = Conversions.ToBoolean(current["LineBreak"]);
                            ParamPrnItem[index].ISGroupHeader = Conversions.ToBoolean(current["GrpHdrRequired"]);
                            ParamPrnItem[index].GroupTotalRequired = Conversions.ToBoolean(current["GrpTotRequired"]);
                            ParamPrnItem[index].PageTotalRequired = Conversions.ToBoolean(current["PgTotRequired"]);
                            ParamPrnItem[index].ReportTotalRequired = Conversions.ToBoolean(current["RptTotRequired"]);
                            ParamPrnItem[index].IsPageHeader = Conversions.ToBoolean(current["IsPgHeader"]);
                            ParamPrnItem[index].IsRptFooter = Conversions.ToBoolean(current["IsRptFooter"]);
                            ParamPrnItem[index].IsHrzFooter = Conversions.ToBoolean(current["IsHrzFooter"]);
                            ParamPrnItem[index].BoldRequired = Conversions.ToBoolean(current["BoldRequired"]);
                            ParamPrnItem[index].ColumnHeaderRequired = Conversions.ToBoolean(current["ColHdrRequired"]);
                            ParamPrnItem[index].ZeroPrintRequired = Conversions.ToBoolean(current["ZeroPrintRequired"]);
                            index++;
                        }
                    }
                    finally
                    {
                        if (enumerator is IDisposable)
                        {
                            (enumerator as IDisposable).Dispose();
                        }
                    }
                }
                flag = true;
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "SETCOLDATA");
                flag = false;
                ProjectData.ClearProjectError();
                return flag;
                ProjectData.ClearProjectError();
            }
            return flag;
        }

        public bool SETREPORTDATA(ref ClsRptDosPrint ParamPrnRpt, string RptName, DataSet Ds, SqlDataAdapter SDA, int PRNID, SqlConnection Conn)
        {
            bool flag;
            try
            {
                string strQry = "SELECT * FROM RES_BILLSETTINGS";
                this.FillDataSet(strQry, "BILLHEAD", Ds, SDA, Conn);
                if (Ds.Tables["BILLHEAD"].Rows.Count > 0)
                {
                    strQry = "SELECT * FROM RES_REPORTSETTINGS WHERE PRINTERID = " + Conversions.ToString(PRNID) + " AND REPORTNAME='" + RptName + "'";
                    this.FillDataSet(strQry, "REPORT", Ds, SDA, Conn);
                    DataTable table = Ds.Tables["REPORT"];
                    if (table.Rows.Count > 0)
                    {
                        IEnumerator enumerator = null;
                        try
                        {
                            enumerator = table.Rows.GetEnumerator();
                            while (enumerator.MoveNext())
                            {
                                DataRow current = (DataRow) enumerator.Current;
                                ParamPrnRpt.set_ReportHeader(0, Conversions.ToString(Interaction.IIf(Information.IsDBNull(RuntimeHelpers.GetObjectValue(Ds.Tables["BILLHEAD"].Rows[0]["BILLHEADER1"])), "", RuntimeHelpers.GetObjectValue(Ds.Tables["BILLHEAD"].Rows[0]["BILLHEADER1"]))));
                                ParamPrnRpt.set_RepDubReq(0, Conversions.ToBoolean(Interaction.IIf(Information.IsDBNull(RuntimeHelpers.GetObjectValue(Ds.Tables["BILLHEAD"].Rows[0]["DWReq1"])), "", RuntimeHelpers.GetObjectValue(Ds.Tables["BILLHEAD"].Rows[0]["DWReq1"]))));
                                ParamPrnRpt.set_ReportHeader(1, Conversions.ToString(Interaction.IIf(Information.IsDBNull(RuntimeHelpers.GetObjectValue(Ds.Tables["BILLHEAD"].Rows[0]["BILLHEADER2"])), "", RuntimeHelpers.GetObjectValue(Ds.Tables["BILLHEAD"].Rows[0]["BILLHEADER2"]))));
                                ParamPrnRpt.set_RepDubReq(1, Conversions.ToBoolean(Interaction.IIf(Information.IsDBNull(RuntimeHelpers.GetObjectValue(Ds.Tables["BILLHEAD"].Rows[0]["DWReq2"])), "", RuntimeHelpers.GetObjectValue(Ds.Tables["BILLHEAD"].Rows[0]["DWReq2"]))));
                                ParamPrnRpt.set_ReportHeader(2, Conversions.ToString(Interaction.IIf(Information.IsDBNull(RuntimeHelpers.GetObjectValue(Ds.Tables["BILLHEAD"].Rows[0]["BILLHEADER3"])), "", RuntimeHelpers.GetObjectValue(Ds.Tables["BILLHEAD"].Rows[0]["BILLHEADER3"]))));
                                ParamPrnRpt.set_RepDubReq(2, Conversions.ToBoolean(Interaction.IIf(Information.IsDBNull(RuntimeHelpers.GetObjectValue(Ds.Tables["BILLHEAD"].Rows[0]["DWReq3"])), "", RuntimeHelpers.GetObjectValue(Ds.Tables["BILLHEAD"].Rows[0]["DWReq3"]))));
                                ParamPrnRpt.set_ReportHeader(3, Conversions.ToString(Interaction.IIf(Information.IsDBNull(RuntimeHelpers.GetObjectValue(Ds.Tables["BILLHEAD"].Rows[0]["BILLHEADER4"])), "", RuntimeHelpers.GetObjectValue(Ds.Tables["BILLHEAD"].Rows[0]["BILLHEADER4"]))));
                                ParamPrnRpt.set_RepDubReq(3, Conversions.ToBoolean(Interaction.IIf(Information.IsDBNull(RuntimeHelpers.GetObjectValue(Ds.Tables["BILLHEAD"].Rows[0]["DWReq4"])), "", RuntimeHelpers.GetObjectValue(Ds.Tables["BILLHEAD"].Rows[0]["DWReq4"]))));
                                ParamPrnRpt.set_PageHeader(0, Conversions.ToString(Interaction.IIf(Information.IsDBNull(RuntimeHelpers.GetObjectValue(current["PageHeader"])), "", RuntimeHelpers.GetObjectValue(current["PageHeader"]))));
                                ParamPrnRpt.set_ReportFooter(0, Conversions.ToString(Interaction.IIf(Information.IsDBNull(RuntimeHelpers.GetObjectValue(current["ReportFooter"])), "", RuntimeHelpers.GetObjectValue(current["ReportFooter"]))));
                                ParamPrnRpt.set_ReportFooter(1, Conversions.ToString(Interaction.IIf(Information.IsDBNull(RuntimeHelpers.GetObjectValue(current["ReportFooter2"])), "", RuntimeHelpers.GetObjectValue(current["ReportFooter2"]))));
                                ParamPrnRpt.PageHeight = Conversions.ToInteger(current["PageHeight"]);
                                ParamPrnRpt.PageBrkRequired = Conversions.ToBoolean(current["PgBrkRequired"]);
                                ParamPrnRpt.HeaderLines = Conversions.ToInteger(current["HeaderLines"]);
                                ParamPrnRpt.FooterLines = Conversions.ToInteger(current["FooterLines"]);
                                ParamPrnRpt.SerialNoRequired = Conversions.ToBoolean(current["SlNoRequired"]);
                                ParamPrnRpt.PageNosRequired = Conversions.ToBoolean(current["PgNoRequired"]);
                                ParamPrnRpt.DoubleSideRequired = Conversions.ToBoolean(current["DblSdRequired"]);
                                ParamPrnRpt.DoubleSideSpace = Conversions.ToInteger(current["DblSdSpace"]);
                                ParamPrnRpt.VerticalReportTotal = Conversions.ToBoolean(current["VrtlTotRequired"]);
                                ParamPrnRpt.AmountInWords = Conversions.ToBoolean(current["AmtInWrdsRequired"]);
                                ParamPrnRpt.LinesReq = Conversions.ToBoolean(current["LinesRequired"]);
                                ParamPrnRpt.RptHdrReq = Conversions.ToBoolean(current["RptHdrRequired"]);
                                ParamPrnRpt.BackFeedLines = Conversions.ToInteger(current["BackFeeds"]);
                                ParamPrnRpt.Cutter = Conversions.ToBoolean(current["Cutter"]);
                                ParamPrnRpt.NoOfPrints = Conversions.ToShort(current["NoOfPrints"]);
                            }
                        }
                        finally
                        {
                            if (enumerator is IDisposable)
                            {
                                (enumerator as IDisposable).Dispose();
                            }
                        }
                    }
                    table = null;
                }
                flag = true;
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "SETREPORTDATA");
                flag = false;
                ProjectData.ClearProjectError();
                return flag;
                ProjectData.ClearProjectError();
            }
            return flag;
        }
    }
}

