﻿namespace ParaDinePrint
{
    using Microsoft.VisualBasic;
    using Microsoft.VisualBasic.CompilerServices;
    using System;
    using System.Collections;
    using System.Data;
    using System.Data.SqlClient;
    using System.Drawing.Printing;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;

    public class ClsDosPrint
    {
        private int BodyLines;
        private DataTable GlbDtPrn;
        private ClsPrinter GlbPrinter = new ClsPrinter();
        private ClsColDosPrint[] GlbPrnCols;
        private ClsRptDosPrint GlbPrnRpt = new ClsRptDosPrint();
        private bool ItemsRem;
        private int MaxBodyLines;
        private int PageNo = 1;
        private int PageWidth;
        public SqlConnection SQLConn = new SqlConnection();
        private string StrLine = "";

        public ClsDosPrint(DataTable dtPrn, ClsColDosPrint[] PrnCols, ClsRptDosPrint PrnRpt, SqlConnection Conn, [Optional, DefaultParameterValue(0)] int ParamPrinterId)
        {
            this.GlbPrnCols = PrnCols;
            this.GlbPrnRpt = PrnRpt;
            this.GlbDtPrn = dtPrn;
            this.SQLConn = Conn;
            this.SetPrinter(Conversions.ToString(ParamPrinterId));
            this.BuildPrintString();
        }

        public string AlignCenter(string txt, int maxLen)
        {
            string str;
            try
            {
                txt = Strings.Trim(txt);
                int num = maxLen - Strings.Len(txt);
                if (Strings.Len(txt) < maxLen)
                {
                    maxLen--;
                    return (Strings.Space((int) Math.Round((double) (((double) num) / 2.0))) + txt + Strings.Space(num - Convert.ToInt16((double) (((double) num) / 2.0))));
                }
                if ((Strings.Len(txt) > maxLen) & (maxLen > 2))
                {
                    maxLen--;
                    return (Strings.Mid(txt, 1, maxLen - 2) + "..");
                }
                if (maxLen > 0)
                {
                    return Strings.Mid(txt, 1, Strings.Len(txt) - 1);
                }
                return "";
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "AlignCentre");
                str = "";
                ProjectData.ClearProjectError();
                return str;
                ProjectData.ClearProjectError();
            }
            return str;
        }

        public string AlignLeft(string txt, int maxLen)
        {
            string str;
            try
            {
                txt = Strings.Trim(txt);
                if (Strings.Len(txt) < maxLen)
                {
                    maxLen--;
                    return (txt + Strings.Space(maxLen - Strings.Len(txt)) + Strings.Space(1));
                }
                if ((Strings.Len(txt) > maxLen) & (maxLen > 2))
                {
                    maxLen--;
                    return (Strings.Mid(txt, 1, maxLen - 2) + ".." + Strings.Space(1));
                }
                if (maxLen > 0)
                {
                    return (Strings.Mid(txt, 1, Strings.Len(txt) - 1) + Strings.Space(1));
                }
                return "";
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "AlignLeft");
                str = "";
                ProjectData.ClearProjectError();
                return str;
                ProjectData.ClearProjectError();
            }
            return str;
        }

        public string AlignRight(string txt, int maxLen)
        {
            string str;
            try
            {
                txt = Strings.Trim(txt);
                if (Strings.Len(txt) < maxLen)
                {
                    maxLen--;
                    return (Strings.Space(maxLen - Strings.Len(txt)) + txt + Strings.Space(1));
                }
                if ((Strings.Len(txt) > maxLen) & (maxLen > 2))
                {
                    maxLen--;
                    return (Strings.Mid(txt, 1, maxLen - 2) + ".." + Strings.Space(1));
                }
                if (maxLen > 0)
                {
                    return (Strings.Mid(txt, 1, Strings.Len(txt) - 1) + Strings.Space(1));
                }
                return "";
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "AlignRight");
                str = "";
                ProjectData.ClearProjectError();
                return str;
                ProjectData.ClearProjectError();
            }
            return str;
        }

        private void BuildPrintString()
        {
            if (this.GlbDtPrn.Rows.Count > 0)
            {
                this.BodyLines = 0;
                this.PageNo = 1;
                this.MaxBodyLines = this.GlbPrnRpt.PageHeight - (this.GlbPrnRpt.HeaderLines + this.GlbPrnRpt.FooterLines);
                this.PageWidth = 0;
                if (this.GlbPrnRpt.SerialNoRequired)
                {
                    this.PageWidth += 4;
                }
                int num4 = Information.UBound(this.GlbPrnCols, 1);
                for (int i = 0; i <= num4; i++)
                {
                    if (!this.GlbPrnCols[i].IsPageHeader & !this.GlbPrnCols[i].IsRptFooter)
                    {
                        this.PageWidth = Conversions.ToInteger(Microsoft.VisualBasic.CompilerServices.Operators.AddObject(this.PageWidth, Interaction.IIf(this.GlbPrnCols[i].ColumnDoulbeWidth, this.GlbPrnCols[i].ColumnWidth * 2, this.GlbPrnCols[i].ColumnWidth)));
                    }
                }
                this.StrLine = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Strings.StrDup(this.PageWidth, "-"), Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space(this.GlbPrnRpt.DoubleSideSpace) + Strings.StrDup(this.PageWidth, "-"), "")));
                PrintingVariables.strprint = "";
                PrintingVariables.strprint = PrintingVariables.strprint + this.GetCompressed();
                int backFeedLines = this.GlbPrnRpt.BackFeedLines;
                for (int j = 1; j <= backFeedLines; j++)
                {
                    PrintingVariables.strprint = PrintingVariables.strprint + this.GetBackFeeds() + "\r\n";
                }
                this.ReportHeader();
                this.PageHeader();
                this.ReportBody();
                this.ReportTotal();
                this.ReportFooter();
                int num6 = Conversions.ToInteger(Interaction.IIf(this.GlbPrnRpt.PageBrkRequired, this.GlbPrnRpt.PageHeight - this.BodyLines, this.GlbPrnRpt.FooterLines));
                for (int k = 0; k <= num6; k++)
                {
                    PrintingVariables.strprint = PrintingVariables.strprint + "\r\n";
                }
                if (this.GlbPrnRpt.Cutter)
                {
                    PrintingVariables.strprint = PrintingVariables.strprint + this.GetCutter();
                }
            }
        }

        private void CalculateHeaderTotal(DataRow dr)
        {
            try
            {
                int num3 = Information.UBound(this.GlbPrnCols, 1);
                for (int i = 0; i <= num3; i++)
                {
                    if (this.GlbPrnCols[i].GroupTotalRequired)
                    {
                        int num4 = Information.UBound(this.GlbPrnCols, 1);
                        for (int j = 0; j <= num4; j++)
                        {
                            if (this.GlbPrnCols[j].ISGroupHeader)
                            {
                                this.GlbPrnCols[i].set_GroupTotal(j, Conversions.ToDouble(Microsoft.VisualBasic.CompilerServices.Operators.AddObject(this.GlbPrnCols[i].get_GroupTotal(j), dr[this.GlbPrnCols[i].ColumnPosition])));
                            }
                        }
                    }
                }
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "Calculate Header Total");
                ProjectData.ClearProjectError();
            }
        }

        private void CheckHeaderChanged(DataRow DR)
        {
            try
            {
                int num4;
                string str = "";
                bool flag = false;
                int num3 = Information.UBound(this.GlbPrnCols, 1);
                for (int i = 0; i <= num3; i++)
                {
                    if (this.GlbPrnCols[i].ISGroupHeader)
                    {
                        this.GlbPrnCols[i].GroupPrint = flag;
                        if (Conversions.ToBoolean(Microsoft.VisualBasic.CompilerServices.Operators.OrObject(Microsoft.VisualBasic.CompilerServices.Operators.CompareObjectNotEqual(this.GlbPrnCols[i].GroupValue, DR[this.GlbPrnCols[i].ColumnPosition], false), this.GlbPrnCols[i].GroupPrint)))
                        {
                            if (!Information.IsNothing(this.GlbPrnCols[i].GroupValue))
                            {
                                this.GlbPrnCols[i].GroupPrint = true;
                                flag = true;
                            }
                            str = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(str + this.AlignLeft(Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(this.GlbPrnCols[i].ColumnName + " : ", DR[this.GlbPrnCols[i].ColumnPosition])), this.PageWidth), Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space(this.GlbPrnRpt.DoubleSideSpace) + this.AlignLeft(Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(this.GlbPrnCols[i].ColumnName + " : ", DR[this.GlbPrnCols[i].ColumnPosition])), this.PageWidth), "")), "\r\n"));
                            this.LineCheck(1, true);
                            this.GlbPrnCols[i].GroupValue = Conversions.ToString(DR[this.GlbPrnCols[i].ColumnPosition]);
                        }
                    }
                }
                int index = Information.UBound(this.GlbPrnCols, 1);
                goto Label_0221;
            Label_01D1:
                if (this.GlbPrnCols[index].ISGroupHeader && (this.GlbPrnCols[index].GroupPrint && !Information.IsNothing(this.GlbPrnCols[index].GroupValue)))
                {
                    this.GroupTotal(index);
                }
                index += -1;
            Label_0221:
                num4 = 0;
                if (index >= num4)
                {
                    goto Label_01D1;
                }
                if (Strings.Trim(str) != "")
                {
                    PrintingVariables.strprint = PrintingVariables.strprint + str;
                }
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "Check Header Changed");
                ProjectData.ClearProjectError();
            }
        }

        public string ColAlignment(string StrCol, int StrWdth, int StrColAlign)
        {
            string str;
            try
            {
                if (StrWdth > 0)
                {
                    if (StrColAlign == 2)
                    {
                        return this.AlignRight(StrCol, StrWdth);
                    }
                    if (StrColAlign == 1)
                    {
                        return this.AlignCenter(StrCol, StrWdth);
                    }
                    return this.AlignLeft(StrCol, StrWdth);
                }
                str = "";
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "ColAlignment");
                str = "";
                ProjectData.ClearProjectError();
                return str;
                ProjectData.ClearProjectError();
            }
            return str;
        }

        private object FillDataSet(string strQry, string dsName, DataSet sqlDS, SqlDataAdapter sda, [Optional, DefaultParameterValue(null)] SqlConnection ParamConn)
        {
            object obj2;
            try
            {
                sda = new SqlDataAdapter(strQry, this.SQLConn);
                sda.SelectCommand.CommandTimeout = 0x3e8;
                if (sqlDS.Tables.Contains(dsName))
                {
                    sqlDS.Tables.Remove(dsName);
                }
                sda.Fill(sqlDS, dsName);
                obj2 = true;
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "FillDataSet Function");
                obj2 = false;
                ProjectData.ClearProjectError();
                return obj2;
                ProjectData.ClearProjectError();
            }
            return obj2;
        }

        private string GetBackFeeds()
        {
            string str;
            try
            {
                string str2 = "";
                if (Microsoft.VisualBasic.CompilerServices.Operators.ConditionalCompareObjectEqual(this.GlbPrinter.PrinterName, "DM", false))
                {
                    str2 = str2 + "\x001bjd\r\n";
                }
                else if (Microsoft.VisualBasic.CompilerServices.Operators.ConditionalCompareObjectEqual(this.GlbPrinter.PrinterName, "THERMAL", false))
                {

                }
                str = str2;
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "Error in GetBackFeeds");
                str = "";
                ProjectData.ClearProjectError();
                return str;
                ProjectData.ClearProjectError();
            }
            return str;
        }

        private string GetCompressed()
        {
            string str2 = "";
            if (Microsoft.VisualBasic.CompilerServices.Operators.ConditionalCompareObjectEqual(this.GlbPrinter.PrinterName, "DM", false))
            {
                return "\x0014";
            }
            if (Microsoft.VisualBasic.CompilerServices.Operators.ConditionalCompareObjectEqual(this.GlbPrinter.PrinterName, "THERMAL", false))
            {
                str2 = "\x001b!\x0002";
            }
            return str2;
        }

        private string GetCutter()
        {
            string str2 = "";
            if (Conversions.ToBoolean(this.GlbPrinter.Autocutter))
            {
                if (Microsoft.VisualBasic.CompilerServices.Operators.ConditionalCompareObjectEqual(this.GlbPrinter.PrinterName, "DM", false))
                {
                    return "";
                }
                if (Microsoft.VisualBasic.CompilerServices.Operators.ConditionalCompareObjectEqual(this.GlbPrinter.PrinterName, "THERMAL", false))
                {
                    str2 = "\x001bi";
                }
            }
            return str2;
        }

        public object GetDataFormat(object ColVal, string Frmt)
        {
            object obj2;
            try
            {
                if (Strings.UCase(Frmt) == "MONEY")
                {
                    return new SqlCommand("SELECT DBO.CURRENCY(" + Strings.Format(Conversions.ToDouble(ColVal), "####0.00") + ")", this.SQLConn).ExecuteScalar();
                }
                if (Strings.UCase(Frmt) == "DATE")
                {
                    return Convert.ToDateTime(RuntimeHelpers.GetObjectValue(ColVal)).ToString("dd/MMM/yy hh:mm tt");
                }
                if (Strings.UCase(Frmt) == "BIT")
                {
                    return Interaction.IIf(Microsoft.VisualBasic.CompilerServices.Operators.ConditionalCompareObjectEqual(ColVal, false, false), "Active", "InActive");
                }
                if (Strings.UCase(Frmt) == "INTEGER")
                {
                    return Conversion.Val(RuntimeHelpers.GetObjectValue(ColVal));
                }
                return ColVal;
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "GetDataFormat");
                obj2 = "";
                ProjectData.ClearProjectError();
                return obj2;
                ProjectData.ClearProjectError();
            }
            return obj2;
        }

        private string GetDoubleWidth()
        {
            string str2 = "";
            if (Microsoft.VisualBasic.CompilerServices.Operators.ConditionalCompareObjectEqual(this.GlbPrinter.PrinterName, "DM", false))
            {
                return "\x000e\x000f";
            }
            if (Microsoft.VisualBasic.CompilerServices.Operators.ConditionalCompareObjectEqual(this.GlbPrinter.PrinterName, "THERMAL", false))
            {
                str2 = "\x001b! ";
            }
            return str2;
        }

        private void GroupTotal(int ColPos)
        {
            try
            {
                string str = "";
                int strWdth = 0;
                if (this.GlbPrnRpt.SerialNoRequired)
                {
                    strWdth += 5;
                }
                int num3 = Information.UBound(this.GlbPrnCols, 1);
                for (int i = 0; i <= num3; i++)
                {
                    if (!this.GlbPrnCols[i].IsPageHeader & !this.GlbPrnCols[i].IsRptFooter)
                    {
                        if (this.GlbPrnCols[i].GroupTotalRequired)
                        {
                            if ((Strings.Trim(str) == "") & (strWdth > (Strings.Len(this.GlbPrnCols[ColPos].ColumnName) + 7)))
                            {
                                str = str + this.ColAlignment(this.GlbPrnCols[ColPos].ColumnName + " TOTAL : ", strWdth, 2);
                                strWdth = 0;
                            }
                            else
                            {
                                str = str + Strings.Space(strWdth);
                            }
                            str = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(str, this.PrintFormat(Conversions.ToString(this.GlbPrnCols[i].get_GroupTotal(ColPos)), i, true)));
                        }
                        else
                        {
                            strWdth += this.GlbPrnCols[i].ColumnWidth;
                        }
                        if (this.GlbPrnCols[i].GroupTotalRequired)
                        {
                            this.GlbPrnCols[i].set_GroupTotal(ColPos, 0.0);
                        }
                    }
                }
                PrintingVariables.strprint = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject((PrintingVariables.strprint + this.StrLine) + "\r\n" + str, Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space(this.GlbPrnRpt.DoubleSideSpace) + str, "")), "\r\n"), this.StrLine), "\r\n"));
                this.LineCheck(3, true);
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "GroupTotal");
                ProjectData.ClearProjectError();
            }
        }

        private void LineCheck(int NoOfLns, [Optional, DefaultParameterValue(true)] bool NxtPage)
        {
            try
            {
                if (this.GlbPrnRpt.PageBrkRequired)
                {
                    this.BodyLines += NoOfLns;
                    if ((this.BodyLines >= this.MaxBodyLines) & NxtPage)
                    {
                        int footerLines = this.GlbPrnRpt.FooterLines;
                        if (Microsoft.VisualBasic.CompilerServices.Operators.ConditionalCompareObjectEqual(this.PageTotal(), true, false))
                        {
                            footerLines -= 3;
                        }
                        if (this.GlbPrnRpt.PageNosRequired)
                        {
                            PrintingVariables.strprint = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(PrintingVariables.strprint + this.AlignRight("Page (" + Conversions.ToString(this.PageNo) + ")", this.PageWidth), Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space(this.GlbPrnRpt.DoubleSideSpace) + this.AlignRight("Page (" + Conversions.ToString(this.PageNo) + ")", this.PageWidth), "")), "\r\n"));
                            this.PageNo++;
                            footerLines--;
                        }
                        int num4 = (footerLines - (this.BodyLines - this.MaxBodyLines)) + 1;
                        for (int i = 1; i <= num4; i++)
                        {
                            PrintingVariables.strprint = PrintingVariables.strprint + "\r\n";
                        }
                        int headerLines = this.GlbPrnRpt.HeaderLines;
                        for (int j = 1; j <= headerLines; j++)
                        {
                            PrintingVariables.strprint = PrintingVariables.strprint + "\r\n";
                        }
                        this.BodyLines = 0;
                        this.PageHeader();
                    }
                }
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "Line Check");
                ProjectData.ClearProjectError();
            }
        }

        private void PageHeader()
        {
            try
            {
                string str = "";
                string left = "";
                int num4 = Conversions.ToInteger(Microsoft.VisualBasic.CompilerServices.Operators.SubtractObject(this.GlbPrnRpt.NoOfPgHeaders, 1));
                for (int i = 0; i <= num4; i++)
                {
                    if (Strings.Trim(this.GlbPrnRpt.get_PageHeader(i)) != "")
                    {
                        PrintingVariables.strprint = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(PrintingVariables.strprint + this.AlignCenter(this.GlbPrnRpt.get_PageHeader(i), this.PageWidth), Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space(this.GlbPrnRpt.DoubleSideSpace) + this.AlignCenter(this.GlbPrnRpt.get_PageHeader(i), this.PageWidth), "")), "\r\n"));
                        this.LineCheck(1, true);
                    }
                }
                int num5 = Information.UBound(this.GlbPrnCols, 1);
                for (int j = 0; j <= num5; j++)
                {
                    if (this.GlbPrnCols[j].IsPageHeader && ((((this.GlbPrnCols[j].ColumnDataType == "STRING") | (this.GlbPrnCols[j].ColumnDataType == "DATE")) & (Strings.Trim(Conversions.ToString(this.GlbDtPrn.Rows[0][this.GlbPrnCols[j].ColumnPosition])) != "")) | (((this.GlbPrnCols[j].ColumnDataType == "INTEGER") | (this.GlbPrnCols[j].ColumnDataType == "MONEY")) & (Conversion.Val(RuntimeHelpers.GetObjectValue(this.GlbDtPrn.Rows[0][this.GlbPrnCols[j].ColumnPosition])) > 0.0))))
                    {
                        left = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(left, Interaction.IIf(this.GlbPrnCols[j].ColumnHeaderRequired, this.ColAlignment(this.GlbPrnCols[j].ColumnName + " : ", Strings.Len(this.GlbPrnCols[j].ColumnName) + 5, this.GlbPrnCols[j].ColumnAlignment), "")), this.PrintFormat(Conversions.ToString(this.GlbDtPrn.Rows[0][this.GlbPrnCols[j].ColumnPosition]), j, true)));
                        if (this.GlbPrnCols[j].ColumnLineBreak)
                        {
                            left = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(left, Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space(Conversions.ToInteger(Microsoft.VisualBasic.CompilerServices.Operators.AddObject(Interaction.IIf((this.PageWidth - Strings.Len(left)) > 0, this.PageWidth - Strings.Len(left), 0), this.GlbPrnRpt.DoubleSideSpace))) + left, "")));
                            str = str + left + "\r\n";
                            left = "";
                            this.LineCheck(1, true);
                        }
                    }
                }
                if (Strings.Trim(left) != "")
                {
                    left = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(left, Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space((this.PageWidth - Strings.Len(left)) + this.GlbPrnRpt.DoubleSideSpace) + left, "")));
                    str = str + left + "\r\n";
                }
                if (Strings.Trim(str) != "")
                {
                    PrintingVariables.strprint = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(PrintingVariables.strprint, Interaction.IIf(this.GlbPrnRpt.LinesReq, this.StrLine, "")), "\r\n"), str));
                }
                PrintingVariables.strprint = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(PrintingVariables.strprint, Interaction.IIf(this.GlbPrnRpt.LinesReq, this.StrLine, "")), "\r\n"));
                str = "";
                if (this.GlbPrnRpt.SerialNoRequired)
                {
                    str = str + this.AlignLeft("SNo.", 5);
                }
                int num6 = Information.UBound(this.GlbPrnCols, 1);
                for (int k = 0; k <= num6; k++)
                {
                    if (!this.GlbPrnCols[k].IsPageHeader & !this.GlbPrnCols[k].IsRptFooter)
                    {
                        str = str + this.ColAlignment(Conversions.ToString(Interaction.IIf(this.GlbPrnCols[k].ColumnHeaderRequired, this.GlbPrnCols[k].ColumnName, "")), this.GlbPrnCols[k].ColumnWidth, this.GlbPrnCols[k].ColumnAlignment);
                    }
                }
                PrintingVariables.strprint = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(PrintingVariables.strprint + str, Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space(this.GlbPrnRpt.DoubleSideSpace) + str, "")), "\r\n"), Interaction.IIf(this.GlbPrnRpt.LinesReq, this.StrLine, "")), "\r\n"));
                this.LineCheck(4, true);
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "Page Header");
                ProjectData.ClearProjectError();
            }
        }

        private object PageTotal()
        {
            object obj2;
            try
            {
                string str = "";
                int strWdth = 0;
                if (this.GlbPrnRpt.SerialNoRequired)
                {
                    strWdth += 5;
                }
                int num3 = Information.UBound(this.GlbPrnCols, 1);
                for (int i = 0; i <= num3; i++)
                {
                    if (!this.GlbPrnCols[i].IsPageHeader & !this.GlbPrnCols[i].IsRptFooter)
                    {
                        if (this.GlbPrnCols[i].PageTotalRequired)
                        {
                            if ((Strings.Trim(str) == "") & (strWdth > Strings.Len("PAGE TOTAL :")))
                            {
                                str = str + this.ColAlignment("PAGE TOTAL :", strWdth, 2);
                                strWdth = 0;
                            }
                            else
                            {
                                str = str + Strings.Space(strWdth);
                            }
                            str = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(str, this.PrintFormat(Conversions.ToString(this.GlbPrnCols[i].PageTotal), i, true)));
                        }
                        else
                        {
                            strWdth += this.GlbPrnCols[i].ColumnWidth;
                        }
                        if (this.GlbPrnCols[i].PageTotalRequired)
                        {
                            this.GlbPrnCols[i].PageTotal = 0.0;
                        }
                    }
                }
                if (Strings.Trim(str) != "")
                {
                    PrintingVariables.strprint = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject((PrintingVariables.strprint + this.StrLine) + "\r\n" + str, Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space(this.GlbPrnRpt.DoubleSideSpace) + str, "")), "\r\n"), this.StrLine), "\r\n"));
                    return true;
                }
                obj2 = false;
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "Report Total");
                obj2 = false;
                ProjectData.ClearProjectError();
                return obj2;
                ProjectData.ClearProjectError();
            }
            return obj2;
        }

        public void PrintData([Optional, DefaultParameterValue("PRINTER")] string Target, [Optional, DefaultParameterValue("")] string printer)
        {
            try
            {
                short noOfPrints = this.GlbPrnRpt.NoOfPrints;
                for (short i = 1; i <= noOfPrints; i = (short) (i + 1))
                {
                    if (i == 2)
                    {
                        PrintingVariables.strprint = this.AlignRight("Duplicate Copy", this.PageWidth) + "\r\n" + PrintingVariables.strprint;
                    }
                    if ((i == this.GlbPrnRpt.NoOfPrints) & (this.GlbPrnRpt.ReportType == "BILL"))
                    {
                        PrintingVariables.strprint = PrintingVariables.strprint + "\x001bp\0\x0019" + Conversions.ToString(Strings.Chr(250));
                    }
                    if (Strings.UCase(Target) == "DISK")
                    {
                        FolderBrowserDialog dialog = new FolderBrowserDialog();
                        dialog.ShowDialog();
                        StreamWriter writer = new StreamWriter(dialog.SelectedPath + @"\" + this.GlbPrnRpt.get_PageHeader(0) + ".TXT", false);
                        writer.WriteLine(PrintingVariables.strprint);
                        writer.Close();
                    }
                    else if (Strings.UCase(Target) == "PRINTER")
                    {
                        if ((printer == "") | (Strings.UCase(printer) == "NULL"))
                        {
                            PrintDialog dialog2 = new PrintDialog();
                            dialog2.PrinterSettings = new PrinterSettings();
                            RawPrinterHelper.SendStringToPrinter(dialog2.PrinterSettings.PrinterName, PrintingVariables.strprint);
                        }
                        else
                        {
                            RawPrinterHelper.SendStringToPrinter(printer, PrintingVariables.strprint);
                        }
                    }
                }
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "PrintData");
                ProjectData.ClearProjectError();
            }
        }

        public object PrintFormat(string strData, int ColPos, [Optional, DefaultParameterValue(false)] bool formatVal)
        {
            object obj2;
            try
            {
                string doubleWidth = "";
                if (formatVal)
                {
                    strData = Conversions.ToString(this.GetDataFormat(strData, this.GlbPrnCols[ColPos].ColumnDataType));
                }
                if (this.GlbPrnCols[ColPos].BoldRequired)
                {
                    doubleWidth = "\x001bE";
                }
                if (this.GlbPrnCols[ColPos].ColumnDoulbeWidth)
                {
                    doubleWidth = this.GetDoubleWidth();
                }
                doubleWidth = doubleWidth + this.ColAlignment(strData, Conversions.ToInteger(Interaction.IIf(this.GlbPrnCols[ColPos].ColumnDoulbeWidth, ((double) this.GlbPrnCols[ColPos].ColumnWidth) / 2.0, this.GlbPrnCols[ColPos].ColumnWidth)), this.GlbPrnCols[ColPos].ColumnAlignment);
                if (this.GlbPrnCols[ColPos].ColumnDoulbeWidth)
                {
                    doubleWidth = doubleWidth + this.GetCompressed();
                }
                if (this.GlbPrnCols[ColPos].BoldRequired)
                {
                    doubleWidth = doubleWidth + "\x001bF";
                }
                obj2 = doubleWidth;
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception prompt = exception1;
                Interaction.MsgBox(prompt, (MsgBoxStyle) Conversions.ToInteger("PrintFormat"), null);
                obj2 = "";
                ProjectData.ClearProjectError();
                return obj2;
                ProjectData.ClearProjectError();
            }
            return obj2;
        }

        private void ReportBody()
        {
            try
            {
                IEnumerator enumerator = null;
                int num9;
                string left = "";
                this.ItemsRem = true;
                int num6 = Information.UBound(this.GlbPrnCols, 1);
                for (int i = 0; i <= num6; i++)
                {
                    if (this.GlbPrnCols[i].GroupTotalRequired)
                    {
                        int num7 = Information.UBound(this.GlbPrnCols, 1);
                        for (int j = 0; j <= num7; j++)
                        {
                            if (this.GlbPrnCols[j].ISGroupHeader)
                            {
                                this.GlbPrnCols[i].AllocateGrpTotal(j);
                            }
                        }
                    }
                }
                int num = 0;
                try
                {
                    enumerator = this.GlbDtPrn.Rows.GetEnumerator();
                    while (enumerator.MoveNext())
                    {
                        DataRow current = (DataRow) enumerator.Current;
                        num++;
                        this.CheckHeaderChanged(current);
                        this.CalculateHeaderTotal(current);
                        if (this.GlbPrnRpt.SerialNoRequired)
                        {
                            left = left + this.AlignLeft(Conversions.ToString(num), 5);
                        }
                        int num8 = Information.UBound(this.GlbPrnCols, 1);
                        for (int k = 0; k <= num8; k++)
                        {
                            if (!this.GlbPrnCols[k].IsPageHeader & !this.GlbPrnCols[k].IsRptFooter)
                            {
                                left = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(left, this.PrintFormat(Conversions.ToString(current[this.GlbPrnCols[k].ColumnPosition]), k, true)));
                                if ((this.GlbPrnCols[k].ColumnDataType == "MONEY") | (this.GlbPrnCols[k].ColumnDataType == "INTEGER"))
                                {
                                    if (this.GlbPrnCols[k].PageTotalRequired)
                                    {
                                        this.GlbPrnCols[k].PageTotal = Conversions.ToDouble(Microsoft.VisualBasic.CompilerServices.Operators.AddObject(this.GlbPrnCols[k].PageTotal, current[this.GlbPrnCols[k].ColumnPosition]));
                                    }
                                    if (this.GlbPrnCols[k].ReportTotalRequired)
                                    {
                                        this.GlbPrnCols[k].ReportTotal = Conversions.ToDouble(Microsoft.VisualBasic.CompilerServices.Operators.AddObject(this.GlbPrnCols[k].ReportTotal, current[this.GlbPrnCols[k].ColumnPosition]));
                                    }
                                }
                            }
                        }
                        if (num >= this.GlbDtPrn.Rows.Count)
                        {
                            this.ItemsRem = false;
                        }
                        PrintingVariables.strprint = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(PrintingVariables.strprint + left, Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space(this.GlbPrnRpt.DoubleSideSpace) + left, "")), "\r\n"));
                        this.LineCheck(1, this.ItemsRem);
                        left = "";
                    }
                }
                finally
                {
                    if (enumerator is IDisposable)
                    {
                        (enumerator as IDisposable).Dispose();
                    }
                }
                int index = Information.UBound(this.GlbPrnCols, 1);
                goto Label_034D;
            Label_0328:
                if (this.GlbPrnCols[index].ISGroupHeader)
                {
                    this.GroupTotal(index);
                }
                index += -1;
            Label_034D:
                num9 = 0;
                if (index >= num9)
                {
                    goto Label_0328;
                }
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "Report Body");
                ProjectData.ClearProjectError();
            }
        }

        private void ReportFooter()
        {
            try
            {
                string str = "";
                string str2 = "";
                string str3 = "";
                string expression = "";
                int num3 = Information.UBound(this.GlbPrnCols, 1);
                for (int i = 0; i <= num3; i++)
                {
                    if ((this.GlbPrnCols[i].IsRptFooter & this.GlbPrnCols[i].IsHrzFooter) && ((((this.GlbPrnCols[i].ColumnDataType == "STRING") | (this.GlbPrnCols[i].ColumnDataType == "DATE")) & ((Strings.Trim(Conversions.ToString(this.GlbDtPrn.Rows[0][this.GlbPrnCols[i].ColumnPosition])) != "") | this.GlbPrnCols[i].ZeroPrintRequired)) | (((this.GlbPrnCols[i].ColumnDataType == "INTEGER") | (this.GlbPrnCols[i].ColumnDataType == "MONEY")) & ((Conversion.Val(RuntimeHelpers.GetObjectValue(this.GlbDtPrn.Rows[0][this.GlbPrnCols[i].ColumnPosition])) != 0.0) | this.GlbPrnCols[i].ZeroPrintRequired))))
                    {
                        if (this.GlbPrnCols[i].ColumnDoulbeWidth)
                        {
                            str2 = str2 + this.GetDoubleWidth();
                        }
                        str2 = str2 + this.AlignRight(this.GlbPrnCols[i].ColumnName + ":", Conversions.ToInteger(Interaction.IIf(this.GlbPrnCols[i].ColumnDoulbeWidth, (((double) this.PageWidth) / 2.0) - 8.0, this.PageWidth - 0x10))) + this.ColAlignment(Strings.Format(RuntimeHelpers.GetObjectValue(this.GlbDtPrn.Rows[0][this.GlbPrnCols[i].ColumnPosition]), "####0.00"), Conversions.ToInteger(Interaction.IIf(this.GlbPrnCols[i].ColumnDoulbeWidth, 8, 0x10)), 2);
                        if (this.GlbPrnRpt.DoubleSideRequired)
                        {
                            str2 = str2 + Strings.Space(Conversions.ToInteger(Interaction.IIf(this.GlbPrnCols[i].ColumnDoulbeWidth, ((double) this.GlbPrnRpt.DoubleSideSpace) / 2.0, this.GlbPrnRpt.DoubleSideSpace))) + str2;
                        }
                        str = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(str + str2, Interaction.IIf(this.GlbPrnCols[i].ColumnDoulbeWidth, this.GetCompressed(), "")), "\r\n"));
                        this.LineCheck(1, this.ItemsRem);
                        str2 = "";
                    }
                }
                if (Strings.Trim(str) != "")
                {
                    str = this.StrLine + "\r\n" + str + this.StrLine + "\r\n";
                    this.LineCheck(2, this.ItemsRem);
                }
                int num4 = Information.UBound(this.GlbPrnCols, 1);
                for (int j = 0; j <= num4; j++)
                {
                    if (this.GlbPrnCols[j].IsRptFooter & !this.GlbPrnCols[j].IsHrzFooter)
                    {
                        if ((((this.GlbPrnCols[j].ColumnDataType == "STRING") | (this.GlbPrnCols[j].ColumnDataType == "DATE")) & (Strings.Trim(Conversions.ToString(this.GlbDtPrn.Rows[0][this.GlbPrnCols[j].ColumnPosition])) != "")) | (((this.GlbPrnCols[j].ColumnDataType == "INTEGER") | (this.GlbPrnCols[j].ColumnDataType == "MONEY")) & (Conversion.Val(RuntimeHelpers.GetObjectValue(this.GlbDtPrn.Rows[0][this.GlbPrnCols[j].ColumnPosition])) > 0.0)))
                        {
                            expression = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(expression + this.ColAlignment(this.GlbPrnCols[j].ColumnName + ":", Conversions.ToInteger(Interaction.IIf(this.GlbPrnCols[j].ColumnWidth > 0, Strings.Len(this.GlbPrnCols[j].ColumnName) + 3, 0)), 2), this.PrintFormat(Conversions.ToString(this.GlbDtPrn.Rows[0][this.GlbPrnCols[j].ColumnPosition]), j, false)));
                        }
                        string str5 = expression;
                        while (str5 != "")
                        {
                            if (Strings.Len(expression) > this.PageWidth)
                            {
                                str5 = expression;
                                expression = Strings.Mid(str5, 1, this.PageWidth);
                                expression = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(expression, Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space((this.PageWidth - Strings.Len(expression)) + this.GlbPrnRpt.DoubleSideSpace) + expression, "")));
                                str3 = str3 + expression + "\r\n";
                                expression = "";
                                this.LineCheck(1, this.ItemsRem);
                                if (Strings.Len(str5) > this.PageWidth)
                                {
                                    expression = Strings.Mid(str5, this.PageWidth + 1, Conversions.ToInteger(Interaction.IIf(Strings.Len(Strings.Trim(str5)) > this.PageWidth, Strings.Len(Strings.Trim(str5)) - this.PageWidth, Strings.Len(Strings.Trim(str5)))));
                                }
                                else
                                {
                                    str5 = "";
                                }
                            }
                            else
                            {
                                if (this.GlbPrnCols[j].ColumnLineBreak)
                                {
                                    expression = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(expression, Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space((this.PageWidth - Strings.Len(expression)) + this.GlbPrnRpt.DoubleSideSpace) + expression, "")));
                                    str3 = str3 + expression + "\r\n";
                                    expression = "";
                                    this.LineCheck(1, this.ItemsRem);
                                }
                                str5 = "";
                            }
                        }
                    }
                }
                if (Strings.Trim(expression) != "")
                {
                    expression = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(expression, Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space((this.PageWidth - Strings.Len(expression)) + this.GlbPrnRpt.DoubleSideSpace) + expression, "")));
                    str3 = str3 + expression + "\r\n";
                }
                if (Strings.Trim(str3) != "")
                {
                    str3 = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(str3, Interaction.IIf(this.GlbPrnRpt.LinesReq, this.StrLine, "")));
                }
                PrintingVariables.strprint = PrintingVariables.strprint + str + str3 + "\r\n";
                this.LineCheck(1, this.ItemsRem);
                if (Strings.Trim(this.GlbPrnRpt.get_ReportFooter(0)) != "")
                {
                    PrintingVariables.strprint = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(PrintingVariables.strprint + "\r\n" + this.ColAlignment("*" + this.GlbPrnRpt.get_ReportFooter(0) + "*", this.PageWidth - 1, 1), Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space(this.GlbPrnRpt.DoubleSideSpace) + this.ColAlignment("*" + this.GlbPrnRpt.get_ReportFooter(0) + "*", this.PageWidth - 1, 1), "")), "\r\n"));
                    this.LineCheck(2, this.ItemsRem);
                }
                if (Strings.Trim(this.GlbPrnRpt.get_ReportFooter(1)) != "")
                {
                    PrintingVariables.strprint = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(PrintingVariables.strprint + this.ColAlignment("*" + this.GlbPrnRpt.get_ReportFooter(1) + "*", this.PageWidth - 1, 1), Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space(this.GlbPrnRpt.DoubleSideSpace) + this.ColAlignment("*" + this.GlbPrnRpt.get_ReportFooter(1) + "*", this.PageWidth - 1, 1), "")), "\r\n"));
                    this.LineCheck(1, this.ItemsRem);
                }
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "Report Footer");
                ProjectData.ClearProjectError();
            }
        }

        private void ReportHeader()
        {
            try
            {
                if (this.GlbPrnRpt.RptHdrReq)
                {
                    if (Strings.Trim(this.GlbPrnRpt.get_ReportHeader(0)) != "")
                    {
                        if (this.GlbPrnRpt.get_RepDubReq(0))
                        {
                            PrintingVariables.strprint = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject((PrintingVariables.strprint + this.GetDoubleWidth()) + this.AlignCenter(this.GlbPrnRpt.get_ReportHeader(0), (int) Math.Round((double) (((double) this.PageWidth) / 2.0))) + this.GetCompressed(), Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space(this.GlbPrnRpt.DoubleSideSpace) + this.GetDoubleWidth() + this.AlignCenter(this.GlbPrnRpt.get_ReportHeader(0), (int) Math.Round((double) (((double) this.PageWidth) / 2.0))), "")), this.GetCompressed()), "\r\n"));
                        }
                        else
                        {
                            PrintingVariables.strprint = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(PrintingVariables.strprint + this.AlignCenter(this.GlbPrnRpt.get_ReportHeader(0), this.PageWidth), Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space(this.GlbPrnRpt.DoubleSideSpace) + this.AlignCenter(this.GlbPrnRpt.get_ReportHeader(0), this.PageWidth), "")), "\r\n"));
                        }
                    }
                    if (Strings.Trim(this.GlbPrnRpt.get_ReportHeader(1)) != "")
                    {
                        if (this.GlbPrnRpt.get_RepDubReq(1))
                        {
                            PrintingVariables.strprint = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject((PrintingVariables.strprint + this.GetDoubleWidth()) + this.AlignCenter(this.GlbPrnRpt.get_ReportHeader(1), (int) Math.Round((double) (((double) this.PageWidth) / 2.0))) + this.GetCompressed(), Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space(this.GlbPrnRpt.DoubleSideSpace) + this.GetDoubleWidth() + this.AlignCenter(this.GlbPrnRpt.get_ReportHeader(1), (int) Math.Round((double) (((double) this.PageWidth) / 2.0))), "")), this.GetCompressed()), "\r\n"));
                        }
                        else
                        {
                            PrintingVariables.strprint = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(PrintingVariables.strprint + this.AlignCenter(this.GlbPrnRpt.get_ReportHeader(1), this.PageWidth), Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space(this.GlbPrnRpt.DoubleSideSpace) + this.AlignCenter(this.GlbPrnRpt.get_ReportHeader(1), this.PageWidth), "")), "\r\n"));
                        }
                    }
                    if (Strings.Trim(this.GlbPrnRpt.get_ReportHeader(2)) != "")
                    {
                        if (this.GlbPrnRpt.get_RepDubReq(2))
                        {
                            PrintingVariables.strprint = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject((PrintingVariables.strprint + this.GetDoubleWidth()) + this.AlignCenter(this.GlbPrnRpt.get_ReportHeader(2), (int) Math.Round((double) (((double) this.PageWidth) / 2.0))) + this.GetCompressed(), Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space(this.GlbPrnRpt.DoubleSideSpace) + this.GetDoubleWidth() + this.AlignCenter(this.GlbPrnRpt.get_ReportHeader(2), (int) Math.Round((double) (((double) this.PageWidth) / 2.0))), "")), this.GetCompressed()), "\r\n"));
                        }
                        else
                        {
                            PrintingVariables.strprint = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(PrintingVariables.strprint + this.AlignCenter(this.GlbPrnRpt.get_ReportHeader(2), this.PageWidth), Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space(this.GlbPrnRpt.DoubleSideSpace) + this.AlignCenter(this.GlbPrnRpt.get_ReportHeader(2), this.PageWidth), "")), "\r\n"));
                        }
                    }
                    if (Strings.Trim(this.GlbPrnRpt.get_ReportHeader(3)) != "")
                    {
                        if (this.GlbPrnRpt.get_RepDubReq(3))
                        {
                            PrintingVariables.strprint = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject((PrintingVariables.strprint + this.GetDoubleWidth()) + this.AlignCenter(this.GlbPrnRpt.get_ReportHeader(3), (int) Math.Round((double) (((double) this.PageWidth) / 2.0))) + this.GetCompressed(), Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space(this.GlbPrnRpt.DoubleSideSpace) + this.GetDoubleWidth() + this.AlignCenter(this.GlbPrnRpt.get_ReportHeader(3), (int) Math.Round((double) (((double) this.PageWidth) / 2.0))), "")), this.GetCompressed()), "\r\n"));
                        }
                        else
                        {
                            PrintingVariables.strprint = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(PrintingVariables.strprint + this.AlignCenter(this.GlbPrnRpt.get_ReportHeader(3), this.PageWidth), Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space(this.GlbPrnRpt.DoubleSideSpace) + this.AlignCenter(this.GlbPrnRpt.get_ReportHeader(3), this.PageWidth), "")), "\r\n"));
                        }
                    }
                }
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "Report Header");
                ProjectData.ClearProjectError();
            }
        }

        private void ReportTotal()
        {
            try
            {
                string str = "";
                int strWdth = 0;
                if (this.GlbPrnRpt.SerialNoRequired)
                {
                    strWdth += 5;
                }
                int num3 = Information.UBound(this.GlbPrnCols, 1);
                for (int i = 0; i <= num3; i++)
                {
                    if (!this.GlbPrnCols[i].IsPageHeader & !this.GlbPrnCols[i].IsRptFooter)
                    {
                        if (this.GlbPrnCols[i].ReportTotalRequired)
                        {
                            if (this.GlbPrnRpt.VerticalReportTotal)
                            {
                                str = (str + this.GetDoubleWidth() + this.AlignRight(this.GlbPrnCols[i].ColumnName + ":", (int) Math.Round((double) ((((double) this.PageWidth) / 2.0) - 15.0)))) + this.AlignRight(Conversions.ToString(this.GetDataFormat(this.GlbPrnCols[i].ReportTotal, "MONEY")), 15) + "\r\n";
                                this.LineCheck(1, this.ItemsRem);
                                str = str + this.GetCompressed();
                            }
                            else
                            {
                                if ((Strings.Trim(str) == "") & (strWdth > Strings.Len("REPORT TOTAL :")))
                                {
                                    str = str + this.ColAlignment("REPORT TOTAL :", strWdth, 2);
                                    strWdth = 0;
                                }
                                else
                                {
                                    str = str + Strings.Space(strWdth);
                                }
                                str = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(str, this.PrintFormat(Conversions.ToString(this.GlbPrnCols[i].ReportTotal), i, true)));
                            }
                        }
                        else
                        {
                            strWdth += this.GlbPrnCols[i].ColumnWidth;
                        }
                    }
                }
                if (Strings.Trim(str) != "")
                {
                    PrintingVariables.strprint = Conversions.ToString(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject(Microsoft.VisualBasic.CompilerServices.Operators.ConcatenateObject((PrintingVariables.strprint + this.StrLine) + "\r\n" + str, Interaction.IIf(this.GlbPrnRpt.DoubleSideRequired, Strings.Space(this.GlbPrnRpt.DoubleSideSpace) + str, "")), "\r\n"), this.StrLine), "\r\n"));
                    if (this.GlbPrnRpt.VerticalReportTotal)
                    {
                        this.LineCheck(2, this.ItemsRem);
                    }
                    else
                    {
                        this.LineCheck(3, this.ItemsRem);
                    }
                }
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "Report Total");
                ProjectData.ClearProjectError();
            }
        }

        private void SetPrinter(string PrinterId)
        {
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                string strQry = "";
                DataSet sqlDS = new DataSet();
                strQry = "SELECT * FROM RES_PRINTER WHERE PRINTERID = '" + PrinterId + "'";
                this.FillDataSet(strQry, "PRINTER", sqlDS, adapter, null);
                if (sqlDS.Tables["PRINTER"].Rows.Count > 0)
                {
                    IEnumerator enumerator;
                    try
                    {
                        enumerator = sqlDS.Tables["PRINTER"].Rows.GetEnumerator();
                        while (enumerator.MoveNext())
                        {
                            DataRow current = (DataRow) enumerator.Current;
                            this.GlbPrinter.PrinterName = RuntimeHelpers.GetObjectValue(current["PRINTERNAME"]);
                            this.GlbPrinter.Autocutter = RuntimeHelpers.GetObjectValue(current["AUTOCUTTER"]);
                            this.GlbPrinter.MaxWidth = RuntimeHelpers.GetObjectValue(current["MAXWIDTH"]);
                        }
                    }
                    finally
                    {
                        //Modified by bharath
                        //if (enumerator is IDisposable)
                        //{
                        //    (enumerator as IDisposable).Dispose();
                        //}
                    }
                }
            }
            catch (Exception exception1)
            {
                ProjectData.SetProjectError(exception1);
                Exception exception = exception1;
                Interaction.MsgBox(exception.Message, MsgBoxStyle.Critical, "ERROR IN SETPRINTER");
                ProjectData.ClearProjectError();
            }
        }

        public string GetPrintData
        {
            get
            {
                return PrintingVariables.strprint;
            }
        }
    }
}

