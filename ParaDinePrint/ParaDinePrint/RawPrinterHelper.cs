﻿namespace ParaDinePrint
{
    using System;
    using System.IO;
    using System.Runtime.InteropServices;

    public class RawPrinterHelper
    {
        [DllImport("winspool.Drv", CallingConvention=CallingConvention.StdCall, CharSet=CharSet.Unicode, SetLastError=true, ExactSpelling=true)]
        public static extern bool ClosePrinter(IntPtr hPrinter);
        [DllImport("winspool.Drv", CallingConvention=CallingConvention.StdCall, CharSet=CharSet.Unicode, SetLastError=true, ExactSpelling=true)]
        public static extern bool EndDocPrinter(IntPtr hPrinter);
        [DllImport("winspool.Drv", CallingConvention=CallingConvention.StdCall, CharSet=CharSet.Unicode, SetLastError=true, ExactSpelling=true)]
        public static extern bool EndPagePrinter(IntPtr hPrinter);
        [DllImport("winspool.Drv", EntryPoint="OpenPrinterW", CallingConvention=CallingConvention.StdCall, CharSet=CharSet.Unicode, SetLastError=true, ExactSpelling=true)]
        public static extern bool OpenPrinter(string src, ref IntPtr hPrinter, int pd);
        public static bool SendBytesToPrinter(string szPrinterName, IntPtr pBytes, int dwCount)
        {
            DOCINFOW docinfow = new DOCINFOW();
            IntPtr ptr =new IntPtr();
            docinfow.pDocName = "Restro data";
            docinfow.pDataType = "RAW";
            bool flag = false;
            if (OpenPrinter(szPrinterName, ref ptr, 0))
            {
                if (StartDocPrinter(ptr, 1, ref docinfow))
                {
                    if (StartPagePrinter(ptr))
                    {
                        int num2=0;
                        flag = WritePrinter(ptr, pBytes, dwCount, ref num2);
                        EndPagePrinter(ptr);
                    }
                    EndDocPrinter(ptr);
                }
                ClosePrinter(ptr);
            }
            if (!flag)
            {
                int num = Marshal.GetLastWin32Error();
            }
            return flag;
        }

        public static bool SendFileToPrinter(string szPrinterName, string szFileName)
        {
            FileStream input = new FileStream(szFileName, FileMode.Open);
            BinaryReader reader = new BinaryReader(input);
            byte[] source = new byte[((int) input.Length) + 1];
            source = reader.ReadBytes((int) input.Length);
            IntPtr destination = Marshal.AllocCoTaskMem((int) input.Length);
            Marshal.Copy(source, 0, destination, (int) input.Length);
            bool flag = SendBytesToPrinter(szPrinterName, destination, (int) input.Length);
            Marshal.FreeCoTaskMem(destination);
            return flag;
        }

        public static object SendStringToPrinter(string szPrinterName, string szString)
        {
            object obj2 = new object();
            int length = szString.Length;
            IntPtr pBytes = Marshal.StringToCoTaskMemAnsi(szString);
            SendBytesToPrinter(szPrinterName, pBytes, length);
            Marshal.FreeCoTaskMem(pBytes);
            return obj2;
        }

        [DllImport("winspool.Drv", EntryPoint="StartDocPrinterW", CallingConvention=CallingConvention.StdCall, CharSet=CharSet.Unicode, SetLastError=true, ExactSpelling=true)]
        public static extern bool StartDocPrinter(IntPtr hPrinter, int level, ref DOCINFOW pDI);
        [DllImport("winspool.Drv", CallingConvention=CallingConvention.StdCall, CharSet=CharSet.Unicode, SetLastError=true, ExactSpelling=true)]
        public static extern bool StartPagePrinter(IntPtr hPrinter);
        [DllImport("winspool.Drv", CallingConvention=CallingConvention.StdCall, CharSet=CharSet.Unicode, SetLastError=true, ExactSpelling=true)]
        public static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, int dwCount, ref int dwWritten);

        [StructLayout(LayoutKind.Sequential)]
        public struct DOCINFOW
        {
            [MarshalAs(UnmanagedType.LPWStr)]
            public string pDocName;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string pOutputFile;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string pDataType;
        }
    }
}

