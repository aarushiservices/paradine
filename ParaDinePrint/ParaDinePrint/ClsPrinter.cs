﻿namespace ParaDinePrint
{
    using Microsoft.VisualBasic.CompilerServices;
    using System;

    public class ClsPrinter
    {
        private bool VarAutoCutter;
        private int VarMaxWidth;
        private string VarPrnName;

        public object Autocutter
        {
            get
            {
                return this.VarAutoCutter;
            }
            set
            {
                this.VarAutoCutter = Conversions.ToBoolean(value);
            }
        }

        public object MaxWidth
        {
            get
            {
                return this.VarMaxWidth;
            }
            set
            {
                this.VarMaxWidth = Conversions.ToInteger(value);
            }
        }

        public object PrinterName
        {
            get
            {
                return this.VarPrnName;
            }
            set
            {
                this.VarPrnName = Conversions.ToString(value);
            }
        }
    }
}

