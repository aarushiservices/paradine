﻿namespace ParaDinePrint
{
    using Microsoft.VisualBasic.CompilerServices;
    using System;

    public class ClsColDosPrint
    {
        private bool BldReq;
        private int ColAlign;
        private bool ColDblWidth;
        private bool ColHdrReq;
        private string ColName;
        private int ColPos;
        private string ColType;
        private int ColWidth;
        private bool GrpHdr;
        private bool GrpPrint;
        private double[] GrpTotal;
        private bool GrpTotReq;
        private string GrpValue;
        private bool LineBreak;
        private bool PageTotReq;
        private bool PgHdrFld;
        private double PgTotal;
        private bool RptFtrFld;
        private bool RptHrzFtr;
        private double RptTotal;
        private bool RptTotReq;
        private bool ZeroPrnReq;

        public void AllocateGrpTotal(int GrpCol)
        {
            this.GrpTotal = (double[]) Utils.CopyArray((Array) this.GrpTotal, new double[GrpCol + 1]);
        }

        public void set_GroupTotal(int i, double Value)
        {
            this.GrpTotal[i]=Value;
        }

        public double get_GroupTotal(int i)
        {
            return this.GrpTotal[i];
        }

        public bool BoldRequired
        {
            get
            {
                return this.BldReq;
            }
            set
            {
                this.BldReq = value;
            }
        }

        public int ColumnAlignment
        {
            get
            {
                return this.ColAlign;
            }
            set
            {
                this.ColAlign = value;
            }
        }

        public string ColumnDataType
        {
            get
            {
                return this.ColType;
            }
            set
            {
                this.ColType = value;
            }
        }

        public bool ColumnDoulbeWidth
        {
            get
            {
                return this.ColDblWidth;
            }
            set
            {
                this.ColDblWidth = value;
            }
        }

        public bool ColumnHeaderRequired
        {
            get
            {
                return this.ColHdrReq;
            }
            set
            {
                this.ColHdrReq = value;
            }
        }

        public bool ColumnLineBreak
        {
            get
            {
                return this.LineBreak;
            }
            set
            {
                this.LineBreak = value;
            }
        }

        public string ColumnName
        {
            get
            {
                return this.ColName;
            }
            set
            {
                this.ColName = value;
            }
        }

        public int ColumnPosition
        {
            get
            {
                return this.ColPos;
            }
            set
            {
                this.ColPos = value;
            }
        }

        public int ColumnWidth
        {
            get
            {
                return this.ColWidth;
            }
            set
            {
                this.ColWidth = value;
            }
        }

        public bool GroupPrint
        {
            get
            {
                return this.GrpPrint;
            }
            set
            {
                this.GrpPrint = value;
            }
        }

        public double this[int GrpPos]
        {
            get
            {
                return this.GrpTotal[GrpPos];
            }
            set
            {
                this.GrpTotal[GrpPos] = value;
            }
        }

        public bool GroupTotalRequired
        {
            get
            {
                return this.GrpTotReq;
            }
            set
            {
                this.GrpTotReq = value;
            }
        }

        public string GroupValue
        {
            get
            {
                return this.GrpValue;
            }
            set
            {
                this.GrpValue = value;
            }
        }

        public bool ISGroupHeader
        {
            get
            {
                return this.GrpHdr;
            }
            set
            {
                this.GrpHdr = value;
            }
        }

        public bool IsHrzFooter
        {
            get
            {
                return this.RptHrzFtr;
            }
            set
            {
                this.RptHrzFtr = value;
            }
        }

        public bool IsPageHeader
        {
            get
            {
                return this.PgHdrFld;
            }
            set
            {
                this.PgHdrFld = value;
            }
        }

        public bool IsRptFooter
        {
            get
            {
                return this.RptFtrFld;
            }
            set
            {
                this.RptFtrFld = value;
            }
        }

        public double PageTotal
        {
            get
            {
                return this.PgTotal;
            }
            set
            {
                this.PgTotal = value;
            }
        }

        public bool PageTotalRequired
        {
            get
            {
                return this.PageTotReq;
            }
            set
            {
                this.PageTotReq = value;
            }
        }

        public double ReportTotal
        {
            get
            {
                return this.RptTotal;
            }
            set
            {
                this.RptTotal = value;
            }
        }

        public bool ReportTotalRequired
        {
            get
            {
                return this.RptTotReq;
            }
            set
            {
                this.RptTotReq = value;
            }
        }

        public bool ZeroPrintRequired
        {
            get
            {
                return this.ZeroPrnReq;
            }
            set
            {
                this.ZeroPrnReq = value;
            }
        }
    }
}

