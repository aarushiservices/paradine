﻿namespace ParControls
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Windows.Forms;

    public class RibbonMouseSensor : IDisposable
    {
        private System.Windows.Forms.Control _control;
        private bool _disposed;
        private RibbonItem _hittedItem;
        private RibbonPanel _hittedPanel;
        private RibbonItem _hittedSubItem;
        private RibbonTab _hittedTab;
        private bool _hittedTabScrollLeft;
        private bool _hittedTabScrollRight;
        private List<RibbonItem> _items;
        private IEnumerable<RibbonItem> _itemsLimit;
        private RibbonPanel _panelLimit;
        private List<RibbonPanel> _panels;
        private ParControls.Ribbon _ribbon;
        private RibbonItem _selectedItem;
        private RibbonPanel _selectedPanel;
        private RibbonItem _selectedSubItem;
        private RibbonTab _selectedTab;
        private bool _suspended;
        private RibbonTab _tabLimit;
        private List<RibbonTab> _tabs;

        private RibbonMouseSensor()
        {
            this._tabs = new List<RibbonTab>();
            this._panels = new List<RibbonPanel>();
            this._items = new List<RibbonItem>();
        }

        public RibbonMouseSensor(System.Windows.Forms.Control control, ParControls.Ribbon ribbon) : this()
        {
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }
            if (ribbon == null)
            {
                throw new ArgumentNullException("ribbon");
            }
            this._control = control;
            this._ribbon = ribbon;
            this.AddHandlers();
        }

        public RibbonMouseSensor(System.Windows.Forms.Control control, ParControls.Ribbon ribbon, RibbonTab tab) : this(control, ribbon)
        {
            this.Tabs.Add(tab);
            this.Panels.AddRange(tab.Panels);
            foreach (RibbonPanel panel in tab.Panels)
            {
                this.Items.AddRange(panel.Items);
            }
        }

        public RibbonMouseSensor(System.Windows.Forms.Control control, ParControls.Ribbon ribbon, IEnumerable<RibbonItem> itemsSource) : this(control, ribbon)
        {
            this.ItemsSource = itemsSource;
        }

        public RibbonMouseSensor(System.Windows.Forms.Control control, ParControls.Ribbon ribbon, IEnumerable<RibbonTab> tabs, IEnumerable<RibbonPanel> panels, IEnumerable<RibbonItem> items) : this(control, ribbon)
        {
            if (tabs != null)
            {
                this.Tabs.AddRange(tabs);
            }
            if (panels != null)
            {
                this.Panels.AddRange(panels);
            }
            if (items != null)
            {
                this.Items.AddRange(items);
            }
        }

        private void AddHandlers()
        {
            if (this.Control == null)
            {
                throw new ApplicationException("Control is Null, cant Add RibbonMouseSensor Handles");
            }
            this.Control.MouseMove += new MouseEventHandler(this.Control_MouseMove);
            this.Control.MouseLeave += new EventHandler(this.Control_MouseLeave);
            this.Control.MouseDown += new MouseEventHandler(this.Control_MouseDown);
            this.Control.MouseUp += new MouseEventHandler(this.Control_MouseUp);
            this.Control.MouseClick += new MouseEventHandler(this.Control_MouseClick);
            this.Control.MouseDoubleClick += new MouseEventHandler(this.Control_MouseDoubleClick);
        }

        private void Control_MouseClick(object sender, MouseEventArgs e)
        {
            if (!this.IsSupsended && !this.Disposed)
            {
                if (this.HittedPanel != null)
                {
                    this.HittedPanel.OnClick(e);
                }
                if (this.HittedItem != null)
                {
                    this.HittedItem.OnClick(e);
                }
                if (this.HittedSubItem != null)
                {
                    this.HittedSubItem.OnClick(e);
                }
            }
        }

        private void Control_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (!this.IsSupsended && !this.Disposed)
            {
                if (this.HittedPanel != null)
                {
                    this.HittedPanel.OnDoubleClick(e);
                }
                if (this.HittedItem != null)
                {
                    this.HittedItem.OnDoubleClick(e);
                }
                if (this.HittedSubItem != null)
                {
                    this.HittedSubItem.OnDoubleClick(e);
                }
            }
        }

        private void Control_MouseDown(object sender, MouseEventArgs e)
        {
            if (!this.IsSupsended && !this.Disposed)
            {
                this.HitTest(e.Location);
                if (this.HittedTab != null)
                {
                    if (this.HittedTabScrollLeft)
                    {
                        this.HittedTab.SetScrollLeftPressed(true);
                        this.Control.Invalidate(this.HittedTab.ScrollLeftBounds);
                    }
                    if (this.HittedTabScrollRight)
                    {
                        this.HittedTab.SetScrollRightPressed(true);
                        this.Control.Invalidate(this.HittedTab.ScrollRightBounds);
                    }
                }
                if (this.HittedPanel != null)
                {
                    this.HittedPanel.SetPressed(true);
                    this.HittedPanel.OnMouseDown(e);
                    this.Control.Invalidate(this.HittedPanel.Bounds);
                }
                if (this.HittedItem != null)
                {
                    this.HittedItem.SetPressed(true);
                    this.HittedItem.OnMouseDown(e);
                    this.Control.Invalidate(this.HittedItem.Bounds);
                }
                if (this.HittedSubItem != null)
                {
                    this.HittedSubItem.SetPressed(true);
                    this.HittedSubItem.OnMouseDown(e);
                    this.Control.Invalidate(Rectangle.Intersect(this.HittedItem.Bounds, this.HittedSubItem.Bounds));
                }
            }
        }

        private void Control_MouseLeave(object sender, EventArgs e)
        {
            if (!this.IsSupsended && !this.Disposed)
            {
            }
        }

        private void Control_MouseMove(object sender, MouseEventArgs e)
        {
            if (!this.IsSupsended && !this.Disposed)
            {
                this.HitTest(e.Location);
                if ((this.SelectedPanel != null) && (this.SelectedPanel != this.HittedPanel))
                {
                    this.SelectedPanel.SetSelected(false);
                    this.SelectedPanel.OnMouseLeave(e);
                    this.Control.Invalidate(this.SelectedPanel.Bounds);
                }
                if ((this.SelectedItem != null) && (this.SelectedItem != this.HittedItem))
                {
                    this.SelectedItem.SetSelected(false);
                    this.SelectedItem.OnMouseLeave(e);
                    this.Control.Invalidate(this.SelectedItem.Bounds);
                }
                if ((this.SelectedSubItem != null) && (this.SelectedSubItem != this.HittedSubItem))
                {
                    this.SelectedSubItem.SetSelected(false);
                    this.SelectedSubItem.OnMouseLeave(e);
                    this.Control.Invalidate(Rectangle.Intersect(this.SelectedItem.Bounds, this.SelectedSubItem.Bounds));
                }
                if (this.HittedTab != null)
                {
                    if (this.HittedTab.ScrollLeftVisible)
                    {
                        this.HittedTab.SetScrollLeftSelected(this.HittedTabScrollLeft);
                        this.Control.Invalidate(this.HittedTab.ScrollLeftBounds);
                    }
                    if (this.HittedTab.ScrollRightVisible)
                    {
                        this.HittedTab.SetScrollRightSelected(this.HittedTabScrollRight);
                        this.Control.Invalidate(this.HittedTab.ScrollRightBounds);
                    }
                }
                if (this.HittedPanel != null)
                {
                    if (this.HittedPanel == this.SelectedPanel)
                    {
                        this.HittedPanel.OnMouseMove(e);
                    }
                    else
                    {
                        this.HittedPanel.SetSelected(true);
                        this.HittedPanel.OnMouseEnter(e);
                        this.Control.Invalidate(this.HittedPanel.Bounds);
                    }
                }
                if (this.HittedItem != null)
                {
                    if (this.HittedItem == this.SelectedItem)
                    {
                        this.HittedItem.OnMouseMove(e);
                    }
                    else
                    {
                        this.HittedItem.SetSelected(true);
                        this.HittedItem.OnMouseEnter(e);
                        this.Control.Invalidate(this.HittedItem.Bounds);
                    }
                }
                if (this.HittedSubItem != null)
                {
                    if (this.HittedSubItem == this.SelectedSubItem)
                    {
                        this.HittedSubItem.OnMouseMove(e);
                    }
                    else
                    {
                        this.HittedSubItem.SetSelected(true);
                        this.HittedSubItem.OnMouseEnter(e);
                        this.Control.Invalidate(Rectangle.Intersect(this.HittedItem.Bounds, this.HittedSubItem.Bounds));
                    }
                }
            }
        }

        private void Control_MouseUp(object sender, MouseEventArgs e)
        {
            if (!this.IsSupsended && !this.Disposed)
            {
                if (this.HittedTab != null)
                {
                    if (this.HittedTab.ScrollLeftVisible)
                    {
                        this.HittedTab.SetScrollLeftPressed(false);
                        this.Control.Invalidate(this.HittedTab.ScrollLeftBounds);
                    }
                    if (this.HittedTab.ScrollRightVisible)
                    {
                        this.HittedTab.SetScrollRightPressed(false);
                        this.Control.Invalidate(this.HittedTab.ScrollRightBounds);
                    }
                }
                if (this.HittedPanel != null)
                {
                    this.HittedPanel.SetPressed(false);
                    this.HittedPanel.OnMouseUp(e);
                    this.Control.Invalidate(this.HittedPanel.Bounds);
                }
                if (this.HittedItem != null)
                {
                    this.HittedItem.SetPressed(false);
                    this.HittedItem.OnMouseUp(e);
                    this.Control.Invalidate(this.HittedItem.Bounds);
                }
                if (this.HittedSubItem != null)
                {
                    this.HittedSubItem.SetPressed(false);
                    this.HittedSubItem.OnMouseUp(e);
                    this.Control.Invalidate(Rectangle.Intersect(this.HittedItem.Bounds, this.HittedSubItem.Bounds));
                }
            }
        }

        public void Dispose()
        {
            this._disposed = true;
            this.RemoveHandlers();
        }

        internal void HitTest(Point p)
        {
            this.SelectedTab = this.HittedTab;
            this.SelectedPanel = this.HittedPanel;
            this.SelectedItem = this.HittedItem;
            this.SelectedSubItem = this.HittedSubItem;
            this.HittedTab = null;
            this.HittedTabScrollLeft = false;
            this.HittedTabScrollRight = false;
            this.HittedPanel = null;
            this.HittedItem = null;
            this.HittedSubItem = null;
            if (this.TabLimit != null)
            {
                if (this.TabLimit.TabContentBounds.Contains(p))
                {
                    this.HittedTab = this.TabLimit;
                }
            }
            else
            {
                foreach (RibbonTab tab in this.Tabs)
                {
                    if (tab.TabContentBounds.Contains(p))
                    {
                        this.HittedTab = tab;
                        break;
                    }
                }
            }
            if (this.HittedTab != null)
            {
                this.HittedTabScrollLeft = this.HittedTab.ScrollLeftVisible && this.HittedTab.ScrollLeftBounds.Contains(p);
                this.HittedTabScrollRight = this.HittedTab.ScrollRightVisible && this.HittedTab.ScrollRightBounds.Contains(p);
            }
            if (!this.HittedTabScroll)
            {
                if (this.PanelLimit != null)
                {
                    if (this.PanelLimit.Bounds.Contains(p))
                    {
                        this.HittedPanel = this.PanelLimit;
                    }
                }
                else
                {
                    foreach (RibbonPanel panel in this.Panels)
                    {
                        if (panel.Bounds.Contains(p))
                        {
                            this.HittedPanel = panel;
                            break;
                        }
                    }
                }
                IEnumerable<RibbonItem> itemsSource = this.Items;
                if (this.ItemsSource != null)
                {
                    itemsSource = this.ItemsSource;
                }
                foreach (RibbonItem item in itemsSource)
                {
                    if ((((item.OwnerPanel == null) || !item.OwnerPanel.OverflowMode) || (this.Control is RibbonPanelPopup)) && item.Bounds.Contains(p))
                    {
                        this.HittedItem = item;
                        break;
                    }
                }
                IContainsSelectableRibbonItems hittedItem = this.HittedItem as IContainsSelectableRibbonItems;
                IScrollableRibbonItem item2 = this.HittedItem as IScrollableRibbonItem;
                if (hittedItem != null)
                {
                    Rectangle rect = (item2 != null) ? item2.ContentBounds : this.HittedItem.Bounds;
                    foreach (RibbonItem item in hittedItem.GetItems())
                    {
                        Rectangle bounds = item.Bounds;
                        bounds.Intersect(rect);
                        if (bounds.Contains(p))
                        {
                            this.HittedSubItem = item;
                        }
                    }
                }
            }
        }

        private void RemoveHandlers()
        {
            foreach (RibbonItem item in this.Items)
            {
                item.SetSelected(false);
                item.SetPressed(false);
            }
            this.Control.MouseMove -= new MouseEventHandler(this.Control_MouseMove);
            this.Control.MouseLeave -= new EventHandler(this.Control_MouseLeave);
            this.Control.MouseDown -= new MouseEventHandler(this.Control_MouseDown);
            this.Control.MouseUp -= new MouseEventHandler(this.Control_MouseUp);
        }

        public void Resume()
        {
            this._suspended = false;
        }

        public void Suspend()
        {
            this._suspended = true;
        }

        public System.Windows.Forms.Control Control
        {
            get
            {
                return this._control;
            }
        }

        public bool Disposed
        {
            get
            {
                return this._disposed;
            }
        }

        internal RibbonItem HittedItem
        {
            get
            {
                return this._hittedItem;
            }
            set
            {
                this._hittedItem = value;
            }
        }

        internal RibbonPanel HittedPanel
        {
            get
            {
                return this._hittedPanel;
            }
            set
            {
                this._hittedPanel = value;
            }
        }

        internal RibbonItem HittedSubItem
        {
            get
            {
                return this._hittedSubItem;
            }
            set
            {
                this._hittedSubItem = value;
            }
        }

        internal RibbonTab HittedTab
        {
            get
            {
                return this._hittedTab;
            }
            set
            {
                this._hittedTab = value;
            }
        }

        internal bool HittedTabScroll
        {
            get
            {
                return (this.HittedTabScrollLeft || this.HittedTabScrollRight);
            }
        }

        internal bool HittedTabScrollLeft
        {
            get
            {
                return this._hittedTabScrollLeft;
            }
            set
            {
                this._hittedTabScrollLeft = value;
            }
        }

        internal bool HittedTabScrollRight
        {
            get
            {
                return this._hittedTabScrollRight;
            }
            set
            {
                this._hittedTabScrollRight = value;
            }
        }

        public bool IsSupsended
        {
            get
            {
                return this._suspended;
            }
        }

        public List<RibbonItem> Items
        {
            get
            {
                return this._items;
            }
        }

        public IEnumerable<RibbonItem> ItemsSource
        {
            get
            {
                return this._itemsLimit;
            }
            set
            {
                this._itemsLimit = value;
            }
        }

        public RibbonPanel PanelLimit
        {
            get
            {
                return this._panelLimit;
            }
            set
            {
                this._panelLimit = value;
            }
        }

        public List<RibbonPanel> Panels
        {
            get
            {
                return this._panels;
            }
        }

        public ParControls.Ribbon Ribbon
        {
            get
            {
                return this._ribbon;
            }
        }

        internal RibbonItem SelectedItem
        {
            get
            {
                return this._selectedItem;
            }
            set
            {
                this._selectedItem = value;
            }
        }

        internal RibbonPanel SelectedPanel
        {
            get
            {
                return this._selectedPanel;
            }
            set
            {
                this._selectedPanel = value;
            }
        }

        internal RibbonItem SelectedSubItem
        {
            get
            {
                return this._selectedSubItem;
            }
            set
            {
                this._selectedSubItem = value;
            }
        }

        internal RibbonTab SelectedTab
        {
            get
            {
                return this._selectedTab;
            }
            set
            {
                this._selectedTab = value;
            }
        }

        public RibbonTab TabLimit
        {
            get
            {
                return this._tabLimit;
            }
            set
            {
                this._tabLimit = value;
            }
        }

        public List<RibbonTab> Tabs
        {
            get
            {
                return this._tabs;
            }
        }
    }
}

