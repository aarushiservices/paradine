﻿namespace ParControls
{
    using System;
    using System.ComponentModel.Design;

    public class RibbonItemCollectionEditor : CollectionEditor
    {
        public RibbonItemCollectionEditor() : base(typeof(RibbonItemCollection))
        {
        }

        protected override Type CreateCollectionItemType()
        {
            return typeof(RibbonButton);
        }

        protected override Type[] CreateNewItemTypes()
        {
            return new Type[] { typeof(RibbonButton), typeof(RibbonButtonList), typeof(RibbonItemGroup), typeof(RibbonSeparator) };
        }
    }
}

