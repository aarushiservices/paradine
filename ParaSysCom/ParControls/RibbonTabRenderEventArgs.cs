﻿namespace ParControls
{
    using System;
    using System.Drawing;

    public sealed class RibbonTabRenderEventArgs : RibbonRenderEventArgs
    {
        private RibbonTab _tab;

        public RibbonTabRenderEventArgs(Ribbon owner, Graphics g, Rectangle clip, RibbonTab tab) : base(owner, g, clip)
        {
            this.Tab = tab;
        }

        public RibbonTab Tab
        {
            get
            {
                return this._tab;
            }
            set
            {
                this._tab = value;
            }
        }
    }
}

