﻿namespace ParControls
{
    using System;
    using System.Windows.Forms;
    using System.Windows.Forms.Design.Behavior;

    public class RibbonTabGlyphBehavior : System.Windows.Forms.Design.Behavior.Behavior
    {
        private RibbonDesigner _designer;
        private Ribbon _ribbon;

        public RibbonTabGlyphBehavior(RibbonDesigner designer, Ribbon ribbon)
        {
            this._designer = designer;
            this._ribbon = ribbon;
        }

        public override bool OnMouseUp(Glyph g, MouseButtons button)
        {
            this._designer.AddTabVerb(this, EventArgs.Empty);
            return base.OnMouseUp(g, button);
        }
    }
}

