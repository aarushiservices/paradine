﻿namespace ParControls
{
    internal class RibbonButtonListDesigner : RibbonElementWithItemCollectionDesigner
    {
        public override RibbonItemCollection Collection
        {
            get
            {
                if (base.Component is RibbonButtonList)
                {
                    return (base.Component as RibbonButtonList).Buttons;
                }
                return null;
            }
        }

        public override ParControls.Ribbon Ribbon
        {
            get
            {
                if (base.Component is RibbonButtonList)
                {
                    return (base.Component as RibbonButtonList).Owner;
                }
                return null;
            }
        }
    }
}

