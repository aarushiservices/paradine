﻿namespace ParControls
{ 
    using System;
    using System.Collections.Generic;

    public class RibbonItemGroupItemCollection : RibbonItemCollection
    {
        private RibbonItemGroup _ownerGroup;

        internal RibbonItemGroupItemCollection(RibbonItemGroup ownerGroup)
        {
            this._ownerGroup = ownerGroup;
        }

        public new void Add(RibbonItem item)
        {
            item.MaxSizeMode = RibbonElementSizeMode.Compact;
            item.SetOwnerGroup(this.OwnerGroup);
            base.Add(item);
        }
        public new void AddRange(IEnumerable<RibbonItem> items)
        {
            foreach (RibbonItem item in items)
            {
                item.MaxSizeMode = RibbonElementSizeMode.Compact;
                item.SetOwnerGroup(this.OwnerGroup);
            }
            base.AddRange(items);
        }

        public void Insert(int index, RibbonItem item)
        {
            item.MaxSizeMode = RibbonElementSizeMode.Compact;
            item.SetOwnerGroup(this.OwnerGroup);
            base.Insert(index, item);
        }

        public RibbonItemGroup OwnerGroup
        {
            get
            {
                return this._ownerGroup;
            }
        }
    }
}

