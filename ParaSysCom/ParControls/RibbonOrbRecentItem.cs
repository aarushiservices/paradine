﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    public partial class RibbonOrbRecentItem : RibbonButton
    {
        public RibbonOrbRecentItem()
        {

        }

        public RibbonOrbRecentItem(string text)
            : this()
        {
            this.Text = text;
        }

        internal override Rectangle OnGetImageBounds(RibbonElementSizeMode sMode, Rectangle bounds)
        {
            return Rectangle.Empty;
        }

        internal override Rectangle OnGetTextBounds(RibbonElementSizeMode sMode, Rectangle bounds)
        {
            Rectangle rectangle = base.OnGetTextBounds(sMode, bounds);
            rectangle.X = base.Bounds.Left + 3;
            return rectangle;
        }
    }
}
