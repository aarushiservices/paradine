﻿namespace ParControls
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    public class RibbonElementPaintEventArgs : EventArgs
    {
        private Rectangle _clip;
        private System.Windows.Forms.Control _control;
        private System.Drawing.Graphics _graphics;
        private RibbonElementSizeMode _mode;

        internal RibbonElementPaintEventArgs(Rectangle clip, System.Drawing.Graphics graphics, RibbonElementSizeMode mode)
        {
            this._clip = clip;
            this._graphics = graphics;
            this._mode = mode;
        }

        internal RibbonElementPaintEventArgs(Rectangle clip, System.Drawing.Graphics graphics, RibbonElementSizeMode mode, System.Windows.Forms.Control control) : this(clip, graphics, mode)
        {
            this._control = control;
        }

        public Rectangle Clip
        {
            get
            {
                return this._clip;
            }
        }

        public System.Windows.Forms.Control Control
        {
            get
            {
                return this._control;
            }
        }

        public System.Drawing.Graphics Graphics
        {
            get
            {
                return this._graphics;
            }
        }

        public RibbonElementSizeMode Mode
        {
            get
            {
                return this._mode;
            }
        }
    }
}

