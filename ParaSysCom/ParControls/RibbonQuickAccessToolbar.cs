﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    public partial class RibbonQuickAccessToolbar : RibbonItem, IContainsSelectableRibbonItems, IContainsRibbonComponents
    {
        private RibbonButton _dropDownButton;
        private bool _DropDownButtonVisible;
        private RibbonQuickAccessToolbarItemCollection _items;
        private System.Windows.Forms.Padding _margin;
        private bool _menuButtonVisible;
        private System.Windows.Forms.Padding _padding;
        private RibbonMouseSensor _sensor;

        internal RibbonQuickAccessToolbar(Ribbon ownerRibbon)
        {
            if (ownerRibbon == null)
            {
                throw new ArgumentNullException("ownerRibbon");
            }
            this.SetOwner(ownerRibbon);
            this._dropDownButton = new RibbonButton();
            this._dropDownButton.SetOwner(ownerRibbon);
            this._dropDownButton.SmallImage = this.CreateDropDownButtonImage();
            this._margin = new System.Windows.Forms.Padding(9);
            this._padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this._items = new RibbonQuickAccessToolbarItemCollection(this);
            this._sensor = new RibbonMouseSensor(ownerRibbon, ownerRibbon, this.Items);
            this._DropDownButtonVisible = true;
        }

        private Image CreateDropDownButtonImage()
        {
            Bitmap image = new Bitmap(7, 7);
            RibbonProfessionalRenderer renderer = base.Owner.Renderer as RibbonProfessionalRenderer;
            Color navy = Color.Navy;
            Color white = Color.White;
            if (renderer != null)
            {
                navy = renderer.ColorTable.Arrow;
                white = renderer.ColorTable.ArrowLight;
            }
            using (Graphics graphics = Graphics.FromImage(image))
            {
                this.DrawDropDownButtonArrow(graphics, white, 0, 1);
                this.DrawDropDownButtonArrow(graphics, navy, 0, 0);
            }
            return image;
        }

        private void DrawDropDownButtonArrow(Graphics g, Color c, int x, int y)
        {
            using (Pen pen = new Pen(c))
            {
                using (SolidBrush brush = new SolidBrush(c))
                {
                    g.DrawLine(pen, x, y, x + 4, y);
                    Point[] points = new Point[] { new Point(x, y + 3), new Point(x + 5, y + 3), new Point(x + 2, y + 6) };
                    g.FillPolygon(brush, points);
                }
            }
        }

        public IEnumerable<Component> GetAllChildComponents()
        {
            return this.Items.ToArray();
        }

        public Rectangle GetContentBounds()
        {
            return base.Bounds;
        }

        public IEnumerable<RibbonItem> GetItems()
        {
            return this.Items;
        }

        public override Size MeasureSize(object sender, RibbonElementMeasureSizeEventArgs e)
        {
            int horizontal = this.Padding.Horizontal;
            int num2 = 0x10;
            foreach (RibbonItem item in this.Items)
            {
                if (!item.Equals(this.DropDownButton))
                {
                    Size size = item.MeasureSize(this, new RibbonElementMeasureSizeEventArgs(e.Graphics, RibbonElementSizeMode.Compact));
                    horizontal += size.Width + 1;
                    num2 = Math.Max(num2, size.Height);
                }
            }
            horizontal--;
            if ((this.Site != null) && this.Site.DesignMode)
            {
                horizontal += 0x10;
            }
            Size size2 = new Size(horizontal, num2);
            base.SetLastMeasuredSize(size2);
            return size2;
        }

        public override void OnPaint(object sender, RibbonElementPaintEventArgs e)
        {
            base.Owner.Renderer.OnRenderRibbonQuickAccessToolbarBackground(new RibbonRenderEventArgs(base.Owner, e.Graphics, e.Clip));
            foreach (RibbonItem item in this.Items)
            {
                item.OnPaint(this, new RibbonElementPaintEventArgs(item.Bounds, e.Graphics, RibbonElementSizeMode.Compact));
            }
        }

        public override void SetBounds(Rectangle bounds)
        {
            base.SetBounds(bounds);
            int x = bounds.Left + this.Padding.Left;
            foreach (RibbonItem item in this.Items)
            {
                item.SetBounds(new Rectangle(new Point(x, bounds.Top), item.LastMeasuredSize));
                x = item.Bounds.Right + 1;
            }
            this.DropDownButton.SetBounds(new Rectangle((bounds.Right + (bounds.Height / 2)) + 2, bounds.Top, 12, bounds.Height));
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public RibbonButton DropDownButton
        {
            get
            {
                return this._dropDownButton;
            }
        }

        [DefaultValue(true), Description("Shows or hides the dropdown button of the toolbar")]
        public bool DropDownButtonVisible
        {
            get
            {
                return this._DropDownButtonVisible;
            }
            set
            {
                this._DropDownButtonVisible = value;
                base.Owner.OnRegionsChanged();
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public RibbonQuickAccessToolbarItemCollection Items
        {
            get
            {
                if (this.DropDownButtonVisible)
                {
                    if (!this._items.Contains(this.DropDownButton))
                    {
                        this._items.Add(this.DropDownButton);
                    }
                }
                else if (this._items.Contains(this.DropDownButton))
                {
                    this._items.Remove(this.DropDownButton);
                }
                return this._items;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public System.Windows.Forms.Padding Margin
        {
            get
            {
                return this._margin;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool MenuButtonVisible
        {
            get
            {
                return this._menuButtonVisible;
            }
            set
            {
                this._menuButtonVisible = value;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public System.Windows.Forms.Padding Padding
        {
            get
            {
                return this._padding;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public RibbonMouseSensor Sensor
        {
            get
            {
                return this._sensor;
            }
        }

        [Browsable(false)]
        internal Rectangle SuperBounds
        {
            get
            {
                return Rectangle.FromLTRB(base.Bounds.Left - this.Padding.Horizontal, base.Bounds.Top, this.DropDownButton.Bounds.Right, base.Bounds.Bottom);
            }
        }
    }
}
