﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    public partial class RibbonTextBox : RibbonItem
    {
        private TextBox _actualTextBox;
        private Rectangle _imageBounds;
        private bool _imageVisible;
        private Rectangle _labelBounds;
        private bool _labelVisible;
        private bool _removingTxt;
        private Rectangle _textBoxBounds;
        private string _textBoxText;
        private int _textboxWidth = 100;
        private const int spacing = 3;

        public event EventHandler TextBoxTextChanged;

        private void _actualTextbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Enter)) || (e.KeyCode == Keys.Escape))
            {
                this.RemoveActualTextBox();
            }
        }

        private void _actualTextbox_LostFocus(object sender, EventArgs e)
        {
            this.RemoveActualTextBox();
        }

        private void _actualTextbox_TextChanged(object sender, EventArgs e)
        {
        }

        private void _actualTextBox_VisibleChanged(object sender, EventArgs e)
        {
            if (!((sender as TextBox).Visible || this._removingTxt))
            {
                this.RemoveActualTextBox();
            }
        }

        public void EndEdit()
        {
            this.RemoveActualTextBox();
        }

        protected virtual void InitTextBox(TextBox t)
        {
            t.Text = this.TextBoxText;
            t.BorderStyle = BorderStyle.None;
            t.Width = this.TextBoxBounds.Width - 2;
            t.Location = new Point(this.TextBoxBounds.Left + 2, base.Bounds.Top + ((base.Bounds.Height - t.Height) / 2));
        }

        protected virtual int MeasureHeight()
        {
            return (0x10 + base.Owner.ItemMargin.Vertical);
        }

        public override Size MeasureSize(object sender, RibbonElementMeasureSizeEventArgs e)
        {
            int width = 0;
            int num2 = (this.Image != null) ? (this.Image.Width + 3) : 0;
            int num3 = string.IsNullOrEmpty(this.Text) ? 0 : (e.Graphics.MeasureString(this.Text, base.Owner.Font).ToSize().Width + 3);
            int textBoxWidth = this.TextBoxWidth;
            width += this.TextBoxWidth;
            switch (e.SizeMode)
            {
                case RibbonElementSizeMode.Medium:
                    width += num2;
                    break;

                case RibbonElementSizeMode.Large:
                    width += num2 + num3;
                    break;
            }
            base.SetLastMeasuredSize(new Size(width, this.MeasureHeight()));
            return base.LastMeasuredSize;
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            if (this.Enabled)
            {
                base.OnMouseDown(e);
                if (this.TextBoxBounds.Contains(e.X, e.Y))
                {
                    this.StartEdit();
                }
            }
        }

        public override void OnMouseEnter(MouseEventArgs e)
        {
            if (this.Enabled)
            {
                base.OnMouseEnter(e);
                base.Canvas.Cursor = Cursors.IBeam;
            }
        }

        public override void OnMouseLeave(MouseEventArgs e)
        {
            if (this.Enabled)
            {
                base.OnMouseLeave(e);
                base.Canvas.Cursor = Cursors.Default;
            }
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (this.Enabled)
            {
                base.OnMouseMove(e);
                if (this.TextBoxBounds.Contains(e.X, e.Y))
                {
                    base.Owner.Cursor = Cursors.IBeam;
                }
                else
                {
                    base.Owner.Cursor = Cursors.Default;
                }
            }
        }

        public override void OnPaint(object sender, RibbonElementPaintEventArgs e)
        {
            base.Owner.Renderer.OnRenderRibbonItem(new RibbonItemRenderEventArgs(base.Owner, e.Graphics, base.Bounds, this));
            if (this.ImageVisible)
            {
                base.Owner.Renderer.OnRenderRibbonItemImage(new RibbonItemBoundsEventArgs(base.Owner, e.Graphics, e.Clip, this, this._imageBounds));
            }
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Near;
            format.LineAlignment = StringAlignment.Center;
            format.Trimming = StringTrimming.None;
            format.FormatFlags |= StringFormatFlags.NoWrap;
            base.Owner.Renderer.OnRenderRibbonItemText(new RibbonTextEventArgs(base.Owner, e.Graphics, base.Bounds, this, this.TextBoxTextBounds, this.TextBoxText, format));
            if (this.LabelVisible)
            {
                base.Owner.Renderer.OnRenderRibbonItemText(new RibbonTextEventArgs(base.Owner, e.Graphics, base.Bounds, this, this.LabelBounds, this.Text, format));
            }
        }

        public void OnTextChanged(EventArgs e)
        {
            if (this.Enabled)
            {
                base.NotifyOwnerRegionsChanged();
                if (this.TextBoxTextChanged != null)
                {
                    this.TextBoxTextChanged(this, e);
                }
            }
        }

        protected void PlaceActualTextBox()
        {
            this._actualTextBox = new TextBox();
            this.InitTextBox(this._actualTextBox);
            this._actualTextBox.TextChanged += new EventHandler(this._actualTextbox_TextChanged);
            this._actualTextBox.KeyDown += new KeyEventHandler(this._actualTextbox_KeyDown);
            this._actualTextBox.LostFocus += new EventHandler(this._actualTextbox_LostFocus);
            this._actualTextBox.VisibleChanged += new EventHandler(this._actualTextBox_VisibleChanged);
            this._actualTextBox.Visible = true;
            base.Canvas.Controls.Add(this._actualTextBox);
        }

        protected void RemoveActualTextBox()
        {
            if ((this._actualTextBox != null) && !this._removingTxt)
            {
                this._removingTxt = true;
                this.TextBoxText = this._actualTextBox.Text;
                this._actualTextBox.Visible = false;
                this._actualTextBox.Parent.Controls.Remove(this._actualTextBox);
                this._actualTextBox.Dispose();
                this._actualTextBox = null;
                this.RedrawItem();
                this._removingTxt = false;
            }
        }

        public override void SetBounds(Rectangle bounds)
        {
            base.SetBounds(bounds);
            this._textBoxBounds = Rectangle.FromLTRB(bounds.Right - this.TextBoxWidth, bounds.Top, bounds.Right, bounds.Bottom);
            if (this.Image != null)
            {
                this._imageBounds = new Rectangle(bounds.Left + base.Owner.ItemMargin.Left, bounds.Top + base.Owner.ItemMargin.Top, this.Image.Width, this.Image.Height);
            }
            else
            {
                this._imageBounds = new Rectangle(this.ContentBounds.Location, Size.Empty);
            }
            this._labelBounds = Rectangle.FromLTRB(this._imageBounds.Right + ((this._imageBounds.Width > 0) ? 3 : 0), bounds.Top, this._textBoxBounds.Left - 3, bounds.Bottom - base.Owner.ItemMargin.Bottom);
            if (base.SizeMode == RibbonElementSizeMode.Large)
            {
                this._imageVisible = true;
                this._labelVisible = true;
            }
            else if (base.SizeMode == RibbonElementSizeMode.Medium)
            {
                this._imageVisible = true;
                this._labelVisible = false;
                this._labelBounds = Rectangle.Empty;
            }
            else if (base.SizeMode == RibbonElementSizeMode.Compact)
            {
                this._imageBounds = Rectangle.Empty;
                this._imageVisible = false;
                this._labelBounds = Rectangle.Empty;
                this._labelVisible = false;
            }
        }

        public void StartEdit()
        {
            this.PlaceActualTextBox();
            this._actualTextBox.SelectAll();
            this._actualTextBox.Focus();
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool Editing
        {
            get
            {
                return (this._actualTextBox != null);
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Rectangle ImageBounds
        {
            get
            {
                return this._imageBounds;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public bool ImageVisible
        {
            get
            {
                return this._imageVisible;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual Rectangle LabelBounds
        {
            get
            {
                return this._labelBounds;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public bool LabelVisible
        {
            get
            {
                return this._labelVisible;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public virtual Rectangle TextBoxBounds
        {
            get
            {
                return this._textBoxBounds;
            }
        }

        [Description("Text on the textbox")]
        public string TextBoxText
        {
            get
            {
                return this._textBoxText;
            }
            set
            {
                this._textBoxText = value;
                this.OnTextChanged(EventArgs.Empty);
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual Rectangle TextBoxTextBounds
        {
            get
            {
                return this.TextBoxBounds;
            }
        }

        [DefaultValue(100)]
        public int TextBoxWidth
        {
            get
            {
                return this._textboxWidth;
            }
            set
            {
                this._textboxWidth = value;
                base.NotifyOwnerRegionsChanged();
            }
        }
    }
}
