﻿namespace ParControls
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    public sealed class RibbonPanelCollection : List<RibbonPanel>
    {
        private RibbonTab _ownerTab;

        public RibbonPanelCollection(RibbonTab ownerTab)
        {
            this._ownerTab = ownerTab;
        }

        internal RibbonPanelCollection(Ribbon owner, RibbonTab ownerTab)
        {
            if (ownerTab == null)
            {
                throw new ArgumentNullException("ownerTab");
            }
            this._ownerTab = ownerTab;
        }

        public new void Add(RibbonPanel item)
        {
            item.SetOwner(this.Owner);
            item.SetOwnerTab(this.OwnerTab);
            base.Add(item);
        }

        public new void AddRange(IEnumerable<RibbonPanel> items)
        {
            foreach (RibbonPanel panel in items)
            {
                panel.SetOwner(this.Owner);
                panel.SetOwnerTab(this.OwnerTab);
            }
            base.AddRange(items);
        }

        public new void Insert(int index, RibbonPanel item)
        {
            item.SetOwner(this.Owner);
            item.SetOwnerTab(this.OwnerTab);
            base.Insert(index, item);
        }

        internal void SetOwner(Ribbon owner)
        {
            foreach (RibbonPanel panel in this)
            {
                panel.SetOwner(owner);
            }
        }

        internal void SetOwnerTab(RibbonTab ownerTab)
        {
            this._ownerTab = ownerTab;
            foreach (RibbonPanel panel in this)
            {
                panel.SetOwnerTab(this.OwnerTab);
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Ribbon Owner
        {
            get
            {
                return this._ownerTab.Owner;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public RibbonTab OwnerTab
        {
            get
            {
                return this._ownerTab;
            }
        }
    }
}

