﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    [Designer(typeof(ViewerPageDesigner))]
    public partial class ViewerPage : ParPanel
    {
        private bool _IsFinishPage = false;

        public event ViewerPageEventHandler CloseFromBack;

        public event ViewerPageEventHandler CloseFromNext;

        public event EventHandler ShowFromBack;

        public event EventHandler ShowFromNext;

     

        public void FocusFirstTabIndex()
        {
            Control control = null;
            foreach (Control control2 in base.Controls)
            {
                if (control2.CanFocus && ((control == null) || (control2.TabIndex < control.TabIndex)))
                {
                    control = control2;
                }
            }
            if (control != null)
            {
                control.Focus();
            }
            else
            {
                base.Focus();
            }
        }

        public int OnCloseFromBack(Tabpanel wiz)
        {
            ViewerPageEventArgs e = new ViewerPageEventArgs(wiz.PageIndex - 1, wiz.Pages);
            if (this.CloseFromBack != null)
            {
                this.CloseFromBack(wiz, e);
            }
            return e.PageIndex;
        }

        public int OnCloseFromNext(Tabpanel wiz)
        {
            ViewerPageEventArgs e = new ViewerPageEventArgs(wiz.PageIndex + 1, wiz.Pages);
            if (this.CloseFromNext != null)
            {
                this.CloseFromNext(wiz, e);
            }
            return e.PageIndex;
        }

        public void OnShowFromBack(Tabpanel wiz)
        {
            if (this.ShowFromBack != null)
            {
                this.ShowFromBack(wiz, EventArgs.Empty);
            }
        }

        public void OnShowFromNext(Tabpanel wiz)
        {
            if (this.ShowFromNext != null)
            {
                this.ShowFromNext(wiz, EventArgs.Empty);
            }
        }

        [Category("Wizard")]
        public bool IsFinishPage
        {
            get
            {
                return this._IsFinishPage;
            }
            set
            {
                this._IsFinishPage = value;
            }
        }
    }
}
