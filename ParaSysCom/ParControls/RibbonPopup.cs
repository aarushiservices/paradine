﻿using ParControls;
using ParControls.RibbonHelpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    [ToolboxItem(false)]
    public partial class RibbonPopup : Control
    {
        private int _borderRoundness;
        private RibbonWrappedDropDown _toolStripDropDown;

        public event EventHandler Closed;

        public event ToolStripDropDownClosingEventHandler Closing;

        public event CancelEventHandler Opening;

        public event EventHandler Showed;

        public RibbonPopup()
        {
            base.SetStyle(ControlStyles.Opaque, true);
            base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            base.SetStyle(ControlStyles.Selectable, false);
            this.BorderRoundness = 3;
        }

        public void Close()
        {
            if (this.WrappedDropDown != null)
            {
                this.WrappedDropDown.Close();
            }
        }

        protected virtual void OnClosed(EventArgs e)
        {
            RibbonPopupManager.Unregister(this);
            if (this.Closed != null)
            {
                this.Closed(this, e);
            }
        }

        protected virtual void OnClosing(ToolStripDropDownClosingEventArgs e)
        {
            if (this.Closing != null)
            {
                this.Closing(this, e);
            }
        }

        protected virtual void OnOpening(CancelEventArgs e)
        {
            if (this.Opening != null)
            {
                this.Opening(this, e);
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            using (GraphicsPath path = RibbonProfessionalRenderer.RoundRectangle(new Rectangle(Point.Empty, base.Size), this.BorderRoundness))
            {
                using (Region region = new Region(path))
                {
                    this.WrappedDropDown.Region = region;
                }
            }
        }

        protected virtual void OnShowed(EventArgs e)
        {
            if (this.Showed != null)
            {
                this.Showed(this, e);
            }
        }

        public void Show(Point screenLocation)
        {
            ToolStripControlHost host = new ToolStripControlHost(this);
            this.WrappedDropDown = new RibbonWrappedDropDown();
            this.WrappedDropDown.AutoClose = RibbonDesigner.Current != null;
            this.WrappedDropDown.Items.Add(host);
            this.WrappedDropDown.Padding = Padding.Empty;
            this.WrappedDropDown.Margin = Padding.Empty;
            host.Padding = Padding.Empty;
            host.Margin = Padding.Empty;
            this.WrappedDropDown.Opening += new CancelEventHandler(this.ToolStripDropDown_Opening);
            this.WrappedDropDown.Closing += new ToolStripDropDownClosingEventHandler(this.ToolStripDropDown_Closing);
            this.WrappedDropDown.Closed += new ToolStripDropDownClosedEventHandler(this.ToolStripDropDown_Closed);
            this.WrappedDropDown.Size = base.Size;
            this.WrappedDropDown.Show(screenLocation);
            RibbonPopupManager.Register(this);
            this.OnShowed(EventArgs.Empty);
        }

        private void ToolStripDropDown_Closed(object sender, ToolStripDropDownClosedEventArgs e)
        {
            this.OnClosed(EventArgs.Empty);
        }

        private void ToolStripDropDown_Closing(object sender, ToolStripDropDownClosingEventArgs e)
        {
            this.OnClosing(e);
        }

        private void ToolStripDropDown_Opening(object sender, CancelEventArgs e)
        {
            this.OnOpening(e);
        }

        [Browsable(false)]
        public int BorderRoundness
        {
            get
            {
                return this._borderRoundness;
            }
            set
            {
                this._borderRoundness = value;
            }
        }

        protected override System.Windows.Forms.CreateParams CreateParams
        {
            get
            {
                System.Windows.Forms.CreateParams createParams = base.CreateParams;
                if (WinApi.IsXP)
                {
                    createParams.ClassStyle |= 0x20000;
                }
                return createParams;
            }
        }

        internal RibbonWrappedDropDown WrappedDropDown
        {
            get
            {
                return this._toolStripDropDown;
            }
            set
            {
                this._toolStripDropDown = value;
            }
        }        
    }
}
