﻿namespace ParControls
{
    using System;
    using System.Collections.Generic;

    public class RibbonQuickAccessToolbarItemCollection : RibbonItemCollection
    {
        private RibbonQuickAccessToolbar _ownerToolbar;

        internal RibbonQuickAccessToolbarItemCollection(RibbonQuickAccessToolbar toolbar)
        {
            this._ownerToolbar = toolbar;
            base.SetOwner(toolbar.Owner);
        }

        public void Add(RibbonItem item)
        {
            item.MaxSizeMode = RibbonElementSizeMode.Compact;
            base.Add(item);
        }

        public void AddRange(IEnumerable<RibbonItem> items)
        {
            foreach (RibbonItem item in items)
            {
                item.MaxSizeMode = RibbonElementSizeMode.Compact;
            }
            base.AddRange(items);
        }

        public void Insert(int index, RibbonItem item)
        {
            item.MaxSizeMode = RibbonElementSizeMode.Compact;
            base.Insert(index, item);
        }

        public RibbonQuickAccessToolbar OwnerToolbar
        {
            get
            {
                return this._ownerToolbar;
            }
        }
    }
}

