﻿namespace ParControls
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Drawing.Design;

    [Editor("System.Windows.Forms.RibbonItemCollectionEditor", typeof(UITypeEditor))]
    public class RibbonItemCollection : List<RibbonItem>
    {
        private Ribbon _owner;
        private RibbonPanel _ownerPanel;
        private RibbonTab _ownerTab;

        internal RibbonItemCollection()
        {
        }

        public new void Add(RibbonItem item)
        {
            item.SetOwner(this.Owner);
            item.SetOwnerPanel(this.OwnerPanel);
            item.SetOwnerTab(this.OwnerTab);
            base.Add(item);
        }

        public new void AddRange(IEnumerable<RibbonItem> items)
        {
            foreach (RibbonItem item in items)
            {
                item.SetOwner(this.Owner);
                item.SetOwnerPanel(this.OwnerPanel);
                item.SetOwnerTab(this.OwnerTab);
            }
            base.AddRange(items);
        }

        internal void CenterItemsHorizontallyInto(Rectangle rectangle)
        {
            this.CenterItemsHorizontallyInto(this, rectangle);
        }

        internal void CenterItemsHorizontallyInto(IEnumerable<RibbonItem> items, Rectangle rectangle)
        {
            int x = rectangle.Left + ((rectangle.Width - this.GetItemsWidth(items)) / 2);
            int itemsTop = this.GetItemsTop(items);
            this.MoveTo(items, new Point(x, itemsTop));
        }

        internal void CenterItemsInto(Rectangle rectangle)
        {
            this.CenterItemsInto(this, rectangle);
        }

        internal void CenterItemsInto(IEnumerable<RibbonItem> items, Rectangle rectangle)
        {
            int x = rectangle.Left + ((rectangle.Width - this.GetItemsWidth()) / 2);
            int y = rectangle.Top + ((rectangle.Height - this.GetItemsHeight()) / 2);
            this.MoveTo(items, new Point(x, y));
        }

        internal void CenterItemsVerticallyInto(Rectangle rectangle)
        {
            this.CenterItemsVerticallyInto(this, rectangle);
        }

        internal void CenterItemsVerticallyInto(IEnumerable<RibbonItem> items, Rectangle rectangle)
        {
            int itemsLeft = this.GetItemsLeft(items);
            int y = rectangle.Top + ((rectangle.Height - this.GetItemsHeight(items)) / 2);
            this.MoveTo(items, new Point(itemsLeft, y));
        }

        internal int GetItemsBottom()
        {
            if (base.Count == 0)
            {
                return 0;
            }
            int bottom = -2147483648;
            foreach (RibbonItem item in this)
            {
                if (item.Bounds.Bottom > bottom)
                {
                    bottom = item.Bounds.Bottom;
                }
            }
            return bottom;
        }

        internal int GetItemsBottom(IEnumerable<RibbonItem> items)
        {
            if (base.Count == 0)
            {
                return 0;
            }
            int bottom = -2147483648;
            foreach (RibbonItem item in items)
            {
                if (item.Bounds.Bottom > bottom)
                {
                    bottom = item.Bounds.Bottom;
                }
            }
            return bottom;
        }

        internal Rectangle GetItemsBounds()
        {
            return Rectangle.FromLTRB(this.GetItemsLeft(), this.GetItemsTop(), this.GetItemsRight(), this.GetItemsBottom());
        }

        internal Rectangle GetItemsBounds(IEnumerable<RibbonItem> items)
        {
            return Rectangle.FromLTRB(this.GetItemsLeft(items), this.GetItemsTop(items), this.GetItemsRight(items), this.GetItemsBottom(items));
        }

        internal int GetItemsHeight()
        {
            return (this.GetItemsBottom() - this.GetItemsTop());
        }

        internal int GetItemsHeight(IEnumerable<RibbonItem> items)
        {
            return (this.GetItemsBottom(items) - this.GetItemsTop(items));
        }

        internal int GetItemsLeft()
        {
            if (base.Count == 0)
            {
                return 0;
            }
            int x = 0x7fffffff;
            foreach (RibbonItem item in this)
            {
                if (item.Bounds.X < x)
                {
                    x = item.Bounds.X;
                }
            }
            return x;
        }

        internal int GetItemsLeft(IEnumerable<RibbonItem> items)
        {
            if (base.Count == 0)
            {
                return 0;
            }
            int x = 0x7fffffff;
            foreach (RibbonItem item in items)
            {
                if (item.Bounds.X < x)
                {
                    x = item.Bounds.X;
                }
            }
            return x;
        }

        internal int GetItemsRight()
        {
            if (base.Count == 0)
            {
                return 0;
            }
            int right = -2147483648;
            foreach (RibbonItem item in this)
            {
                if (item.Bounds.Right > right)
                {
                    right = item.Bounds.Right;
                }
            }
            return right;
        }

        internal int GetItemsRight(IEnumerable<RibbonItem> items)
        {
            if (base.Count == 0)
            {
                return 0;
            }
            int right = -2147483648;
            foreach (RibbonItem item in items)
            {
                if (item.Bounds.Right > right)
                {
                    right = item.Bounds.Right;
                }
            }
            return right;
        }

        internal int GetItemsTop()
        {
            if (base.Count == 0)
            {
                return 0;
            }
            int y = 0x7fffffff;
            foreach (RibbonItem item in this)
            {
                if (item.Bounds.Y < y)
                {
                    y = item.Bounds.Y;
                }
            }
            return y;
        }

        internal int GetItemsTop(IEnumerable<RibbonItem> items)
        {
            if (base.Count == 0)
            {
                return 0;
            }
            int y = 0x7fffffff;
            foreach (RibbonItem item in items)
            {
                if (item.Bounds.Y < y)
                {
                    y = item.Bounds.Y;
                }
            }
            return y;
        }

        internal int GetItemsWidth()
        {
            return (this.GetItemsRight() - this.GetItemsLeft());
        }

        internal int GetItemsWidth(IEnumerable<RibbonItem> items)
        {
            return (this.GetItemsRight(items) - this.GetItemsLeft(items));
        }

        public new void Insert(int index, RibbonItem item)
        {
            item.SetOwner(this.Owner);
            item.SetOwnerPanel(this.OwnerPanel);
            item.SetOwnerTab(this.OwnerTab);
            base.Insert(index, item);
        }

        internal void MoveTo(Point p)
        {
            this.MoveTo(this, p);
        }

        internal void MoveTo(IEnumerable<RibbonItem> items, Point p)
        {
            Rectangle itemsBounds = this.GetItemsBounds(items);
            foreach (RibbonItem item in items)
            {
                int num = item.Bounds.X - itemsBounds.Left;
                int num2 = item.Bounds.Y - itemsBounds.Top;
                item.SetBounds(new Rectangle(new Point(p.X + num, p.Y + num2), item.Bounds.Size));
            }
        }

        internal void SetOwner(Ribbon owner)
        {
            this._owner = owner;
            foreach (RibbonItem item in this)
            {
                item.SetOwner(owner);
            }
        }

        internal void SetOwnerPanel(RibbonPanel panel)
        {
            this._ownerPanel = panel;
            foreach (RibbonItem item in this)
            {
                item.SetOwnerPanel(panel);
            }
        }

        internal void SetOwnerTab(RibbonTab tab)
        {
            this._ownerTab = tab;
            foreach (RibbonItem item in this)
            {
                item.SetOwnerTab(tab);
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public Ribbon Owner
        {
            get
            {
                return this._owner;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public RibbonPanel OwnerPanel
        {
            get
            {
                return this._ownerPanel;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public RibbonTab OwnerTab
        {
            get
            {
                return this._ownerTab;
            }
        }
    }
}

