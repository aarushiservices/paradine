﻿namespace ParControls
{
    using ParControls.RibbonHelpers;
    using System;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Text;
    using System.Windows.Forms;
    using System.Windows.Forms.VisualStyles;

    public class RibbonProfessionalRenderer : RibbonRenderer
    {
        private RibbonProfesionalRendererColorTable _colorTable;
        private Size arrowSize = new Size(5, 3);
        private Size moreSize = new Size(7, 7);

        public RibbonProfessionalRenderer()
        {
            this.ColorTable = new RibbonProfesionalRendererColorTable();
        }

        private Corners ButtonCorners(RibbonButton button)
        {
            if (!(button.OwnerItem is RibbonItemGroup))
            {
                return Corners.All;
            }
            RibbonItemGroup ownerItem = button.OwnerItem as RibbonItemGroup;
            Corners none = Corners.None;
            if (button == ownerItem.FirstItem)
            {
                none |= Corners.West;
            }
            if (button == ownerItem.LastItem)
            {
                none |= Corners.East;
            }
            return none;
        }

        private Corners ButtonDdRounding(RibbonButton button)
        {
            if (!(button.OwnerItem is RibbonItemGroup))
            {
                if (button.SizeMode == RibbonElementSizeMode.Large)
                {
                    return Corners.South;
                }
                return Corners.East;
            }
            Corners none = Corners.None;
            RibbonItemGroup ownerItem = button.OwnerItem as RibbonItemGroup;
            if (button == ownerItem.LastItem)
            {
                none |= Corners.East;
            }
            return none;
        }

        private Corners ButtonFaceRounding(RibbonButton button)
        {
            if (!(button.OwnerItem is RibbonItemGroup))
            {
                if (button.SizeMode == RibbonElementSizeMode.Large)
                {
                    return Corners.North;
                }
                return Corners.West;
            }
            Corners none = Corners.None;
            RibbonItemGroup ownerItem = button.OwnerItem as RibbonItemGroup;
            if (button == ownerItem.FirstItem)
            {
                none |= Corners.West;
            }
            return none;
        }

        public Rectangle CenterOn(Rectangle container, Rectangle r)
        {
            return new Rectangle(container.Left + ((container.Width - r.Width) / 2), container.Top + ((container.Height - r.Height) / 2), r.Width, r.Height);
        }

        public GraphicsPath CreateCompleteTabPath(RibbonTab t)
        {
            GraphicsPath path = new GraphicsPath();
            int num = 6;
            path.AddLine(t.TabBounds.Left + num, t.TabBounds.Top, t.TabBounds.Right - num, t.TabBounds.Top);
            path.AddArc(Rectangle.FromLTRB(t.TabBounds.Right - num, t.TabBounds.Top, t.TabBounds.Right, t.TabBounds.Top + num), -90f, 90f);
            path.AddLine(t.TabBounds.Right, t.TabBounds.Top + num, t.TabBounds.Right, t.TabBounds.Bottom - num);
            path.AddArc(Rectangle.FromLTRB(t.TabBounds.Right, t.TabBounds.Bottom - num, t.TabBounds.Right + num, t.TabBounds.Bottom), -180f, -90f);
            path.AddLine(t.TabBounds.Right + num, t.TabBounds.Bottom, t.TabContentBounds.Right - num, t.TabBounds.Bottom);
            path.AddArc(Rectangle.FromLTRB(t.TabContentBounds.Right - num, t.TabBounds.Bottom, t.TabContentBounds.Right, t.TabBounds.Bottom + num), -90f, 90f);
            path.AddLine(t.TabContentBounds.Right, t.TabContentBounds.Top + num, t.TabContentBounds.Right, t.TabContentBounds.Bottom - num);
            path.AddArc(Rectangle.FromLTRB(t.TabContentBounds.Right - num, t.TabContentBounds.Bottom - num, t.TabContentBounds.Right, t.TabContentBounds.Bottom), 0f, 90f);
            path.AddLine(t.TabContentBounds.Right - num, t.TabContentBounds.Bottom, t.TabContentBounds.Left + num, t.TabContentBounds.Bottom);
            path.AddArc(Rectangle.FromLTRB(t.TabContentBounds.Left, t.TabContentBounds.Bottom - num, t.TabContentBounds.Left + num, t.TabContentBounds.Bottom), 90f, 90f);
            path.AddLine(t.TabContentBounds.Left, t.TabContentBounds.Bottom - num, t.TabContentBounds.Left, t.TabBounds.Bottom + num);
            path.AddArc(Rectangle.FromLTRB(t.TabContentBounds.Left, t.TabBounds.Bottom, t.TabContentBounds.Left + num, t.TabBounds.Bottom + num), 180f, 90f);
            path.AddLine(t.TabContentBounds.Left + num, t.TabContentBounds.Top, t.TabBounds.Left - num, t.TabBounds.Bottom);
            path.AddArc(Rectangle.FromLTRB(t.TabBounds.Left - num, t.TabBounds.Bottom - num, t.TabBounds.Left, t.TabBounds.Bottom), 90f, -90f);
            path.AddLine(t.TabBounds.Left, t.TabBounds.Bottom - num, t.TabBounds.Left, t.TabBounds.Top + num);
            path.AddArc(Rectangle.FromLTRB(t.TabBounds.Left, t.TabBounds.Top, t.TabBounds.Left + num, t.TabBounds.Top + num), 180f, 90f);
            path.CloseFigure();
            return path;
        }

        private GraphicsPath CreateQuickAccessPath(Point a, Point b, Point c, Point d, Point e, Rectangle bounds, int offsetx, int offsety, Ribbon ribbon)
        {
            a.Offset(offsetx, offsety);
            b.Offset(offsetx, offsety);
            c.Offset(offsetx, offsety);
            d.Offset(offsetx, offsety);
            e.Offset(offsetx, offsety);
            GraphicsPath path = new GraphicsPath();
            path.AddLine(a, b);
            path.AddArc(new Rectangle(b.X - (bounds.Height / 2), b.Y, bounds.Height, bounds.Height), -90f, 180f);
            path.AddLine(d, c);
            if (ribbon.OrbVisible)
            {
                Point[] points = new Point[] { c, e, a };
                path.AddCurve(points);
                return path;
            }
            path.AddArc(new Rectangle(a.X - (bounds.Height / 2), a.Y, bounds.Height, bounds.Height), 90f, 180f);
            return path;
        }

        public GraphicsPath CreateTabPath(RibbonTab t)
        {
            GraphicsPath path = new GraphicsPath();
            int width = 6;
            int num2 = 1;
            path.AddLine(t.TabBounds.Left, t.TabBounds.Bottom, t.TabBounds.Left, t.TabBounds.Top + width);
            path.AddArc(new Rectangle(t.TabBounds.Left, t.TabBounds.Top, width, width), 180f, 90f);
            path.AddLine(t.TabBounds.Left + width, t.TabBounds.Top, (t.TabBounds.Right - width) - num2, t.TabBounds.Top);
            path.AddArc(new Rectangle((t.TabBounds.Right - width) - num2, t.TabBounds.Top, width, width), -90f, 90f);
            path.AddLine(t.TabBounds.Right - num2, t.TabBounds.Top + width, t.TabBounds.Right - num2, t.TabBounds.Bottom);
            return path;
        }

        public void DrawArrow(Graphics g, Rectangle b, Color c, RibbonArrowDirection d)
        {
            GraphicsPath path = new GraphicsPath();
            Rectangle rectangle = b;
            if (((b.Width % 2) != 0) && (d == RibbonArrowDirection.Up))
            {
                rectangle = new Rectangle(new Point(b.Left - 1, b.Top - 1), new Size(b.Width + 1, b.Height + 1));
            }
            if (d == RibbonArrowDirection.Up)
            {
                path.AddLine(rectangle.Left, rectangle.Bottom, rectangle.Right, rectangle.Bottom);
                path.AddLine(rectangle.Right, rectangle.Bottom, rectangle.Left + (rectangle.Width / 2), rectangle.Top);
            }
            else if (d == RibbonArrowDirection.Down)
            {
                path.AddLine(rectangle.Left, rectangle.Top, rectangle.Right, rectangle.Top);
                path.AddLine(rectangle.Right, rectangle.Top, rectangle.Left + (rectangle.Width / 2), rectangle.Bottom);
            }
            else if (d == RibbonArrowDirection.Left)
            {
                path.AddLine(rectangle.Left, rectangle.Top, rectangle.Right, rectangle.Top + (rectangle.Height / 2));
                path.AddLine(rectangle.Right, rectangle.Top + (rectangle.Height / 2), rectangle.Left, rectangle.Bottom);
            }
            else
            {
                path.AddLine(rectangle.Right, rectangle.Top, rectangle.Left, rectangle.Top + (rectangle.Height / 2));
                path.AddLine(rectangle.Left, rectangle.Top + (rectangle.Height / 2), rectangle.Right, rectangle.Bottom);
            }
            path.CloseFigure();
            using (SolidBrush brush = new SolidBrush(c))
            {
                SmoothingMode smoothingMode = g.SmoothingMode;
                g.SmoothingMode = SmoothingMode.None;
                g.FillPath(brush, path);
                g.SmoothingMode = smoothingMode;
            }
            path.Dispose();
        }

        public void DrawArrowShaded(Graphics g, Rectangle b, RibbonArrowDirection d, bool enabled)
        {
            Size arrowSize = this.arrowSize;
            if ((d == RibbonArrowDirection.Left) || (d == RibbonArrowDirection.Right))
            {
                arrowSize = new Size(this.arrowSize.Height, this.arrowSize.Width);
            }
            Point location = new Point(b.Left + ((b.Width - arrowSize.Width) / 2), b.Top + ((b.Height - arrowSize.Height) / 2));
            Rectangle rectangle = new Rectangle(location, arrowSize);
            Rectangle rectangle2 = rectangle;
            rectangle2.Offset(0, 1);
            Color arrowLight = this.ColorTable.ArrowLight;
            Color arrow = this.ColorTable.Arrow;
            if (!enabled)
            {
                arrowLight = Color.Transparent;
                arrow = this.ColorTable.ArrowDisabled;
            }
            this.DrawArrow(g, rectangle2, arrowLight, d);
            this.DrawArrow(g, rectangle, arrow, d);
        }

        public void DrawButton(Graphics g, Rectangle bounds, Corners corners)
        {
            if ((bounds.Height > 0) && (bounds.Width > 0))
            {
                Rectangle r = Rectangle.FromLTRB(bounds.Left, bounds.Top, bounds.Right - 1, bounds.Bottom - 1);
                Rectangle rectangle2 = Rectangle.FromLTRB(bounds.Left + 1, bounds.Top + 1, bounds.Right - 2, bounds.Bottom - 2);
                Rectangle rectangle3 = Rectangle.FromLTRB(bounds.Left + 1, bounds.Top + 1, bounds.Right - 2, bounds.Top + Convert.ToInt32((double) (bounds.Height * 0.36)));
                using (GraphicsPath path = RoundRectangle(r, 3, corners))
                {
                    GraphicsPath path2;
                    Pen pen;
                    using (SolidBrush brush = new SolidBrush(this.ColorTable.ButtonBgOut))
                    {
                        g.FillPath(brush, path);
                    }
                    using (path2 = new GraphicsPath())
                    {
                        path2.AddEllipse(new Rectangle(bounds.Left, bounds.Top, bounds.Width, bounds.Height * 2));
                        path2.CloseFigure();
                        using (PathGradientBrush brush2 = new PathGradientBrush(path2))
                        {
                            brush2.WrapMode = WrapMode.Clamp;
                            brush2.CenterPoint = new PointF(Convert.ToSingle((int) (bounds.Left + (bounds.Width / 2))), Convert.ToSingle(bounds.Bottom));
                            brush2.CenterColor = this.ColorTable.ButtonBgCenter;
                            brush2.SurroundColors = new Color[] { this.ColorTable.ButtonBgOut };
                            Blend blend = new Blend(3);
                            float[] numArray = new float[3];
                            numArray[1] = 0.8f;
                            blend.Factors = numArray;
                            numArray = new float[3];
                            numArray[1] = 0.3f;
                            numArray[2] = 1f;
                            blend.Positions = numArray;
                            Region clip = g.Clip;
                            Region region2 = new Region(path);
                            region2.Intersect(clip);
                            g.SetClip(region2.GetBounds(g));
                            g.FillPath(brush2, path2);
                            g.Clip = clip;
                        }
                    }
                    using (pen = new Pen(this.ColorTable.ButtonBorderOut))
                    {
                        g.DrawPath(pen, path);
                    }
                    using (path2 = RoundRectangle(rectangle2, 3, corners))
                    {
                        using (pen = new Pen(this.ColorTable.ButtonBorderIn))
                        {
                            g.DrawPath(pen, path2);
                        }
                    }
                    using (path2 = RoundRectangle(rectangle3, 3, (corners & Corners.NorthWest) | (corners & Corners.NorthEast)))
                    {
                        if ((rectangle3.Width > 0) && (rectangle3.Height > 0))
                        {
                            using (LinearGradientBrush brush3 = new LinearGradientBrush(rectangle3, this.ColorTable.ButtonGlossyNorth, this.ColorTable.ButtonGlossySouth, 90f))
                            {
                                brush3.WrapMode = WrapMode.TileFlipXY;
                                g.FillPath(brush3, path2);
                            }
                        }
                    }
                }
            }
        }

        public void DrawButtonChecked(Graphics g, RibbonButton button)
        {
            this.DrawButtonChecked(g, button.Bounds, this.ButtonCorners(button));
        }

        public void DrawButtonChecked(Graphics g, Rectangle bounds, Corners corners)
        {
            Rectangle r = Rectangle.FromLTRB(bounds.Left, bounds.Top, bounds.Right - 1, bounds.Bottom - 1);
            Rectangle rectangle2 = Rectangle.FromLTRB(bounds.Left + 1, bounds.Top + 1, bounds.Right - 2, bounds.Bottom - 2);
            Rectangle rectangle3 = Rectangle.FromLTRB(bounds.Left + 1, bounds.Top + 1, bounds.Right - 2, bounds.Top + Convert.ToInt32((double) (bounds.Height * 0.36)));
            using (GraphicsPath path = RoundRectangle(r, 3, corners))
            {
                GraphicsPath path2;
                Pen pen;
                using (SolidBrush brush = new SolidBrush(this.ColorTable.ButtonCheckedBgOut))
                {
                    g.FillPath(brush, path);
                }
                using (path2 = new GraphicsPath())
                {
                    path2.AddEllipse(new Rectangle(bounds.Left, bounds.Top, bounds.Width, bounds.Height * 2));
                    path2.CloseFigure();
                    using (PathGradientBrush brush2 = new PathGradientBrush(path2))
                    {
                        brush2.WrapMode = WrapMode.Clamp;
                        brush2.CenterPoint = new PointF(Convert.ToSingle((int) (bounds.Left + (bounds.Width / 2))), Convert.ToSingle(bounds.Bottom));
                        brush2.CenterColor = this.ColorTable.ButtonCheckedBgCenter;
                        brush2.SurroundColors = new Color[] { this.ColorTable.ButtonCheckedBgOut };
                        Blend blend = new Blend(3);
                        float[] numArray = new float[3];
                        numArray[1] = 0.8f;
                        blend.Factors = numArray;
                        numArray = new float[3];
                        numArray[1] = 0.3f;
                        numArray[2] = 1f;
                        blend.Positions = numArray;
                        Region clip = g.Clip;
                        Region region2 = new Region(path);
                        region2.Intersect(clip);
                        g.SetClip(region2.GetBounds(g));
                        g.FillPath(brush2, path2);
                        g.Clip = clip;
                    }
                }
                using (pen = new Pen(this.ColorTable.ButtonCheckedBorderOut))
                {
                    g.DrawPath(pen, path);
                }
                using (path2 = RoundRectangle(rectangle2, 3, corners))
                {
                    using (pen = new Pen(this.ColorTable.ButtonCheckedBorderIn))
                    {
                        g.DrawPath(pen, path2);
                    }
                }
                using (path2 = RoundRectangle(rectangle3, 3, (corners & Corners.NorthWest) | (corners & Corners.NorthEast)))
                {
                    using (LinearGradientBrush brush3 = new LinearGradientBrush(rectangle3, this.ColorTable.ButtonCheckedGlossyNorth, this.ColorTable.ButtonCheckedGlossySouth, 90f))
                    {
                        brush3.WrapMode = WrapMode.TileFlipXY;
                        g.FillPath(brush3, path2);
                    }
                }
            }
            this.DrawPressedShadow(g, r);
        }

        public void DrawButtonDisabled(Graphics g, Rectangle bounds, Corners corners)
        {
            Rectangle r = Rectangle.FromLTRB(bounds.Left, bounds.Top, bounds.Right - 1, bounds.Bottom - 1);
            Rectangle rectangle2 = Rectangle.FromLTRB(bounds.Left + 1, bounds.Top + 1, bounds.Right - 2, bounds.Bottom - 2);
            Rectangle rectangle3 = Rectangle.FromLTRB(bounds.Left + 1, bounds.Top + 1, bounds.Right - 2, bounds.Top + Convert.ToInt32((double) (bounds.Height * 0.36)));
            using (GraphicsPath path = RoundRectangle(r, 3, corners))
            {
                GraphicsPath path2;
                Pen pen;
                using (SolidBrush brush = new SolidBrush(this.ColorTable.ButtonDisabledBgOut))
                {
                    g.FillPath(brush, path);
                }
                using (path2 = new GraphicsPath())
                {
                    path2.AddEllipse(new Rectangle(bounds.Left, bounds.Top, bounds.Width, bounds.Height * 2));
                    path2.CloseFigure();
                    using (PathGradientBrush brush2 = new PathGradientBrush(path2))
                    {
                        brush2.WrapMode = WrapMode.Clamp;
                        brush2.CenterPoint = new PointF(Convert.ToSingle((int) (bounds.Left + (bounds.Width / 2))), Convert.ToSingle(bounds.Bottom));
                        brush2.CenterColor = this.ColorTable.ButtonDisabledBgCenter;
                        brush2.SurroundColors = new Color[] { this.ColorTable.ButtonDisabledBgOut };
                        Blend blend = new Blend(3);
                        float[] numArray = new float[3];
                        numArray[1] = 0.8f;
                        blend.Factors = numArray;
                        numArray = new float[3];
                        numArray[1] = 0.3f;
                        numArray[2] = 1f;
                        blend.Positions = numArray;
                        Region clip = g.Clip;
                        Region region2 = new Region(path);
                        region2.Intersect(clip);
                        g.SetClip(region2.GetBounds(g));
                        g.FillPath(brush2, path2);
                        g.Clip = clip;
                    }
                }
                using (pen = new Pen(this.ColorTable.ButtonDisabledBorderOut))
                {
                    g.DrawPath(pen, path);
                }
                using (path2 = RoundRectangle(rectangle2, 3, corners))
                {
                    using (pen = new Pen(this.ColorTable.ButtonDisabledBorderIn))
                    {
                        g.DrawPath(pen, path2);
                    }
                }
                using (path2 = RoundRectangle(rectangle3, 3, (corners & Corners.NorthWest) | (corners & Corners.NorthEast)))
                {
                    using (LinearGradientBrush brush3 = new LinearGradientBrush(rectangle3, this.ColorTable.ButtonDisabledGlossyNorth, this.ColorTable.ButtonDisabledGlossySouth, 90f))
                    {
                        brush3.WrapMode = WrapMode.TileFlipXY;
                        g.FillPath(brush3, path2);
                    }
                }
            }
        }

        public void DrawButtonDropDownArrow(Graphics g, RibbonButton button, Rectangle textLayout)
        {
            Rectangle empty = Rectangle.Empty;
            if ((button.SizeMode == RibbonElementSizeMode.Large) || (button.SizeMode == RibbonElementSizeMode.Overflow))
            {
                empty = this.LargeButtonDropDownArrowBounds(g, button.Owner.Font, button.Text, textLayout);
            }
            else
            {
                empty = textLayout;
            }
            this.DrawArrowShaded(g, empty, button.DropDownArrowDirection, button.Enabled);
        }

        public void DrawButtonList(Graphics g, RibbonButtonList list)
        {
            SolidBrush brush;
            using (GraphicsPath path = RoundRectangle(Rectangle.FromLTRB(list.Bounds.Left, list.Bounds.Top, list.Bounds.Right - 1, list.Bounds.Bottom), 3, Corners.East))
            {
                Color dropDownBg = list.Selected ? this.ColorTable.ButtonListBgSelected : this.ColorTable.ButtonListBg;
                if (list.Canvas is RibbonDropDown)
                {
                    dropDownBg = this.ColorTable.DropDownBg;
                }
                using (brush = new SolidBrush(dropDownBg))
                {
                    g.FillPath(brush, path);
                }
                using (Pen pen = new Pen(this.ColorTable.ButtonListBorder))
                {
                    g.DrawPath(pen, path);
                }
            }
            if ((list.ScrollType == RibbonButtonList.ListScrollType.Scrollbar) && ScrollBarRenderer.IsSupported)
            {
                ScrollBarRenderer.DrawUpperVerticalTrack(g, list.ScrollBarBounds, ScrollBarState.Normal);
                if (list.ThumbPressed)
                {
                    ScrollBarRenderer.DrawVerticalThumb(g, list.ThumbBounds, ScrollBarState.Pressed);
                    ScrollBarRenderer.DrawVerticalThumbGrip(g, list.ThumbBounds, ScrollBarState.Pressed);
                }
                else if (list.ThumbSelected)
                {
                    ScrollBarRenderer.DrawVerticalThumb(g, list.ThumbBounds, ScrollBarState.Hot);
                    ScrollBarRenderer.DrawVerticalThumbGrip(g, list.ThumbBounds, ScrollBarState.Hot);
                }
                else
                {
                    ScrollBarRenderer.DrawVerticalThumb(g, list.ThumbBounds, ScrollBarState.Normal);
                    ScrollBarRenderer.DrawVerticalThumbGrip(g, list.ThumbBounds, ScrollBarState.Normal);
                }
                if (list.ButtonUpPressed)
                {
                    ScrollBarRenderer.DrawArrowButton(g, list.ButtonUpBounds, ScrollBarArrowButtonState.UpPressed);
                }
                else if (list.ButtonUpSelected)
                {
                    ScrollBarRenderer.DrawArrowButton(g, list.ButtonUpBounds, ScrollBarArrowButtonState.UpHot);
                }
                else
                {
                    ScrollBarRenderer.DrawArrowButton(g, list.ButtonUpBounds, ScrollBarArrowButtonState.UpNormal);
                }
                if (list.ButtonDownPressed)
                {
                    ScrollBarRenderer.DrawArrowButton(g, list.ButtonDownBounds, ScrollBarArrowButtonState.DownPressed);
                }
                else if (list.ButtonDownSelected)
                {
                    ScrollBarRenderer.DrawArrowButton(g, list.ButtonDownBounds, ScrollBarArrowButtonState.DownHot);
                }
                else
                {
                    ScrollBarRenderer.DrawArrowButton(g, list.ButtonDownBounds, ScrollBarArrowButtonState.DownNormal);
                }
            }
            else
            {
                if (list.ScrollType == RibbonButtonList.ListScrollType.Scrollbar)
                {
                    using (brush = new SolidBrush(this.ColorTable.ButtonGlossyNorth))
                    {
                        g.FillRectangle(brush, list.ScrollBarBounds);
                    }
                }
                if (!list.ButtonDownEnabled)
                {
                    this.DrawButtonDisabled(g, list.ButtonDownBounds, list.ButtonDropDownPresent ? Corners.None : Corners.SouthEast);
                }
                else if (list.ButtonDownPressed)
                {
                    this.DrawButtonPressed(g, list.ButtonDownBounds, list.ButtonDropDownPresent ? Corners.None : Corners.SouthEast);
                }
                else if (list.ButtonDownSelected)
                {
                    this.DrawButtonSelected(g, list.ButtonDownBounds, list.ButtonDropDownPresent ? Corners.None : Corners.SouthEast);
                }
                else
                {
                    this.DrawButton(g, list.ButtonDownBounds, Corners.None);
                }
                if (!list.ButtonUpEnabled)
                {
                    this.DrawButtonDisabled(g, list.ButtonUpBounds, Corners.NorthEast);
                }
                else if (list.ButtonUpPressed)
                {
                    this.DrawButtonPressed(g, list.ButtonUpBounds, Corners.NorthEast);
                }
                else if (list.ButtonUpSelected)
                {
                    this.DrawButtonSelected(g, list.ButtonUpBounds, Corners.NorthEast);
                }
                else
                {
                    this.DrawButton(g, list.ButtonUpBounds, Corners.NorthEast);
                }
                if (list.ButtonDropDownPresent)
                {
                    if (list.ButtonDropDownPressed)
                    {
                        this.DrawButtonPressed(g, list.ButtonDropDownBounds, Corners.SouthEast);
                    }
                    else if (list.ButtonDropDownSelected)
                    {
                        this.DrawButtonSelected(g, list.ButtonDropDownBounds, Corners.SouthEast);
                    }
                    else
                    {
                        this.DrawButton(g, list.ButtonDropDownBounds, Corners.SouthEast);
                    }
                }
                if ((list.ScrollType == RibbonButtonList.ListScrollType.Scrollbar) && list.ScrollBarEnabled)
                {
                    if (list.ThumbPressed)
                    {
                        this.DrawButtonPressed(g, list.ThumbBounds, Corners.All);
                    }
                    else if (list.ThumbSelected)
                    {
                        this.DrawButtonSelected(g, list.ThumbBounds, Corners.All);
                    }
                    else
                    {
                        this.DrawButton(g, list.ThumbBounds, Corners.All);
                    }
                }
                Color arrow = this.ColorTable.Arrow;
                Color arrowLight = this.ColorTable.ArrowLight;
                Color arrowDisabled = this.ColorTable.ArrowDisabled;
                Rectangle b = this.CenterOn(list.ButtonUpBounds, new Rectangle(Point.Empty, this.arrowSize));
                b.Offset(0, 1);
                Rectangle rectangle3 = this.CenterOn(list.ButtonDownBounds, new Rectangle(Point.Empty, this.arrowSize));
                rectangle3.Offset(0, 1);
                Rectangle rectangle4 = this.CenterOn(list.ButtonDropDownBounds, new Rectangle(Point.Empty, this.arrowSize));
                rectangle4.Offset(0, 3);
                this.DrawArrow(g, b, list.ButtonUpEnabled ? arrowLight : Color.Transparent, RibbonArrowDirection.Up);
                b.Offset(0, -1);
                this.DrawArrow(g, b, list.ButtonUpEnabled ? arrow : arrowDisabled, RibbonArrowDirection.Up);
                this.DrawArrow(g, rectangle3, list.ButtonDownEnabled ? arrowLight : Color.Transparent, RibbonArrowDirection.Down);
                rectangle3.Offset(0, -1);
                this.DrawArrow(g, rectangle3, list.ButtonDownEnabled ? arrow : arrowDisabled, RibbonArrowDirection.Down);
                if (list.ButtonDropDownPresent)
                {
                    using (brush = new SolidBrush(this.ColorTable.Arrow))
                    {
                        SmoothingMode smoothingMode = g.SmoothingMode;
                        g.SmoothingMode = SmoothingMode.None;
                        g.FillRectangle(brush, new Rectangle(new Point(rectangle4.Left - 1, rectangle4.Top - 4), new Size(this.arrowSize.Width + 2, 1)));
                        g.SmoothingMode = smoothingMode;
                    }
                    this.DrawArrow(g, rectangle4, arrowLight, RibbonArrowDirection.Down);
                    rectangle4.Offset(0, -1);
                    this.DrawArrow(g, rectangle4, arrow, RibbonArrowDirection.Down);
                }
            }
        }

        public void DrawButtonMoreGlyph(Graphics gr, Point p, Color color)
        {
            Point point = p;
            Point point2 = new Point((p.X + this.moreSize.Width) - 1, p.Y);
            Point point3 = new Point(p.X, (p.Y + this.moreSize.Height) - 1);
            Point point4 = new Point(p.X + this.moreSize.Width, p.Y + this.moreSize.Height);
            Point point5 = new Point(point4.X, point4.Y - 3);
            Point point6 = new Point(point4.X - 3, point4.Y);
            Point point7 = new Point(point4.X - 3, point4.Y - 3);
            SmoothingMode smoothingMode = gr.SmoothingMode;
            gr.SmoothingMode = SmoothingMode.None;
            using (Pen pen = new Pen(color))
            {
                gr.DrawLine(pen, point, point2);
                gr.DrawLine(pen, point, point3);
                gr.DrawLine(pen, point6, point4);
                gr.DrawLine(pen, point5, point4);
                gr.DrawLine(pen, point6, point5);
                gr.DrawLine(pen, point7, point4);
            }
            gr.SmoothingMode = smoothingMode;
        }

        public void DrawButtonMoreGlyph(Graphics g, Rectangle b, bool enabled)
        {
            Color color = enabled ? this.ColorTable.Arrow : this.ColorTable.ArrowDisabled;
            Color arrowLight = this.ColorTable.ArrowLight;
            Rectangle rectangle = this.CenterOn(b, new Rectangle(Point.Empty, this.moreSize));
            Rectangle rectangle2 = rectangle;
            rectangle2.Offset(1, 1);
            this.DrawButtonMoreGlyph(g, rectangle2.Location, arrowLight);
            this.DrawButtonMoreGlyph(g, rectangle.Location, color);
        }

        public void DrawButtonPressed(Graphics g, RibbonButton button)
        {
            this.DrawButtonPressed(g, button.Bounds, this.ButtonCorners(button));
        }

        public void DrawButtonPressed(Graphics g, Rectangle bounds, Corners corners)
        {
            Rectangle r = Rectangle.FromLTRB(bounds.Left, bounds.Top, bounds.Right - 1, bounds.Bottom - 1);
            Rectangle rectangle2 = Rectangle.FromLTRB(bounds.Left + 1, bounds.Top + 1, bounds.Right - 2, bounds.Bottom - 2);
            Rectangle rectangle3 = Rectangle.FromLTRB(bounds.Left + 1, bounds.Top + 1, bounds.Right - 2, bounds.Top + Convert.ToInt32((double) (bounds.Height * 0.36)));
            using (GraphicsPath path = RoundRectangle(r, 3, corners))
            {
                GraphicsPath path2;
                Pen pen;
                using (SolidBrush brush = new SolidBrush(this.ColorTable.ButtonPressedBgOut))
                {
                    g.FillPath(brush, path);
                }
                using (path2 = new GraphicsPath())
                {
                    path2.AddEllipse(new Rectangle(bounds.Left, bounds.Top, bounds.Width, bounds.Height * 2));
                    path2.CloseFigure();
                    using (PathGradientBrush brush2 = new PathGradientBrush(path2))
                    {
                        brush2.WrapMode = WrapMode.Clamp;
                        brush2.CenterPoint = new PointF(Convert.ToSingle((int) (bounds.Left + (bounds.Width / 2))), Convert.ToSingle(bounds.Bottom));
                        brush2.CenterColor = this.ColorTable.ButtonPressedBgCenter;
                        brush2.SurroundColors = new Color[] { this.ColorTable.ButtonPressedBgOut };
                        Blend blend = new Blend(3);
                        float[] numArray = new float[3];
                        numArray[1] = 0.8f;
                        blend.Factors = numArray;
                        numArray = new float[3];
                        numArray[1] = 0.3f;
                        numArray[2] = 1f;
                        blend.Positions = numArray;
                        Region clip = g.Clip;
                        Region region2 = new Region(path);
                        region2.Intersect(clip);
                        g.SetClip(region2.GetBounds(g));
                        g.FillPath(brush2, path2);
                        g.Clip = clip;
                    }
                }
                using (pen = new Pen(this.ColorTable.ButtonPressedBorderOut))
                {
                    g.DrawPath(pen, path);
                }
                using (path2 = RoundRectangle(rectangle2, 3, corners))
                {
                    using (pen = new Pen(this.ColorTable.ButtonPressedBorderIn))
                    {
                        g.DrawPath(pen, path2);
                    }
                }
                using (path2 = RoundRectangle(rectangle3, 3, (corners & Corners.NorthWest) | (corners & Corners.NorthEast)))
                {
                    using (LinearGradientBrush brush3 = new LinearGradientBrush(rectangle3, this.ColorTable.ButtonPressedGlossyNorth, this.ColorTable.ButtonPressedGlossySouth, 90f))
                    {
                        brush3.WrapMode = WrapMode.TileFlipXY;
                        g.FillPath(brush3, path2);
                    }
                }
            }
            this.DrawPressedShadow(g, r);
        }

        public void DrawButtonSelected(Graphics g, RibbonButton button)
        {
            this.DrawButtonSelected(g, button.Bounds, this.ButtonCorners(button));
        }

        public void DrawButtonSelected(Graphics g, Rectangle bounds, Corners corners)
        {
            Rectangle r = Rectangle.FromLTRB(bounds.Left, bounds.Top, bounds.Right - 1, bounds.Bottom - 1);
            Rectangle rectangle2 = Rectangle.FromLTRB(bounds.Left + 1, bounds.Top + 1, bounds.Right - 2, bounds.Bottom - 2);
            Rectangle rectangle3 = Rectangle.FromLTRB(bounds.Left + 1, bounds.Top + 1, bounds.Right - 2, bounds.Top + Convert.ToInt32((double) (bounds.Height * 0.36)));
            using (GraphicsPath path = RoundRectangle(r, 3, corners))
            {
                GraphicsPath path2;
                Pen pen;
                using (SolidBrush brush = new SolidBrush(this.ColorTable.ButtonSelectedBgOut))
                {
                    g.FillPath(brush, path);
                }
                using (path2 = new GraphicsPath())
                {
                    path2.AddEllipse(new Rectangle(bounds.Left, bounds.Top, bounds.Width, bounds.Height * 2));
                    path2.CloseFigure();
                    using (PathGradientBrush brush2 = new PathGradientBrush(path2))
                    {
                        brush2.WrapMode = WrapMode.Clamp;
                        brush2.CenterPoint = new PointF(Convert.ToSingle((int) (bounds.Left + (bounds.Width / 2))), Convert.ToSingle(bounds.Bottom));
                        brush2.CenterColor = this.ColorTable.ButtonSelectedBgCenter;
                        brush2.SurroundColors = new Color[] { this.ColorTable.ButtonSelectedBgOut };
                        Blend blend = new Blend(3);
                        float[] numArray = new float[3];
                        numArray[1] = 0.8f;
                        blend.Factors = numArray;
                        numArray = new float[3];
                        numArray[1] = 0.3f;
                        numArray[2] = 1f;
                        blend.Positions = numArray;
                        Region clip = g.Clip;
                        Region region2 = new Region(path);
                        region2.Intersect(clip);
                        g.SetClip(region2.GetBounds(g));
                        g.FillPath(brush2, path2);
                        g.Clip = clip;
                    }
                }
                using (pen = new Pen(this.ColorTable.ButtonSelectedBorderOut))
                {
                    g.DrawPath(pen, path);
                }
                using (path2 = RoundRectangle(rectangle2, 3, corners))
                {
                    using (pen = new Pen(this.ColorTable.ButtonSelectedBorderIn))
                    {
                        g.DrawPath(pen, path2);
                    }
                }
                using (path2 = RoundRectangle(rectangle3, 3, (corners & Corners.NorthWest) | (corners & Corners.NorthEast)))
                {
                    using (LinearGradientBrush brush3 = new LinearGradientBrush(rectangle3, this.ColorTable.ButtonSelectedGlossyNorth, this.ColorTable.ButtonSelectedGlossySouth, 90f))
                    {
                        brush3.WrapMode = WrapMode.TileFlipXY;
                        g.FillPath(brush3, path2);
                    }
                }
            }
        }

        public void DrawCaptionBarBackground(Rectangle r, Graphics g)
        {
            SmoothingMode smoothingMode = g.SmoothingMode;
            Rectangle rectangle = new Rectangle(r.Left, r.Top, r.Width, 4);
            Rectangle rectangle2 = new Rectangle(r.Left, rectangle.Bottom, r.Width, 4);
            Rectangle rectangle3 = new Rectangle(r.Left, rectangle2.Bottom, r.Width, r.Height - 8);
            Rectangle rectangle4 = new Rectangle(r.Left, rectangle3.Bottom, r.Width, 1);
            Rectangle[] rectangleArray = new Rectangle[] { rectangle, rectangle2, rectangle3, rectangle4 };
            Color[,] colorArray2 = new Color[4, 2];
            //*(colorArray2[0, 0]) = this.ColorTable.Caption1;
            //*(colorArray2[0, 1]) = this.ColorTable.Caption2;
            //*(colorArray2[1, 0]) = this.ColorTable.Caption3;
            //*(colorArray2[1, 1]) = this.ColorTable.Caption4;
            //*(colorArray2[2, 0]) = this.ColorTable.Caption5;
            //*(colorArray2[2, 1]) = this.ColorTable.Caption6;
            //*(colorArray2[3, 0]) = this.ColorTable.Caption7;
            //*(colorArray2[3, 1]) = this.ColorTable.Caption7;
            colorArray2[0, 0] = this.ColorTable.Caption1;
            colorArray2[0, 1] = this.ColorTable.Caption2;
            colorArray2[1, 0] = this.ColorTable.Caption3;
            colorArray2[1, 1] = this.ColorTable.Caption4;
            colorArray2[2, 0] = this.ColorTable.Caption5;
            colorArray2[2, 1] = this.ColorTable.Caption6;
            colorArray2[3, 0] = this.ColorTable.Caption7;
            colorArray2[3, 1] = this.ColorTable.Caption7;
            Color[,] colorArray = colorArray2;
            g.SmoothingMode = SmoothingMode.None;
            for (int i = 0; i < rectangleArray.Length; i++)
            {
                Rectangle rect = rectangleArray[i];
                rect.Height += 2;
                rect.Y--;
                using (LinearGradientBrush brush = new LinearGradientBrush(rect, colorArray[i, 0], colorArray[i, 1], 90f))
                {
                    g.FillRectangle(brush, rectangleArray[i]);
                }
            }
            g.SmoothingMode = smoothingMode;
        }

        private void DrawCaptionBarText(Rectangle captionBar, RibbonRenderEventArgs e)
        {
            Form form = e.Ribbon.FindForm();
            if (form != null)
            {
                StringFormat format = new StringFormat();
                format.LineAlignment = format.Alignment = StringAlignment.Center;
                format.Trimming = StringTrimming.EllipsisCharacter;
                format.FormatFlags |= StringFormatFlags.NoWrap;
                Font font = new Font(SystemFonts.CaptionFont, FontStyle.Regular);
                TextRenderer.DrawText(e.Graphics, form.Text, font, captionBar, this.ColorTable.FormBorder);
                if (e.Ribbon.ActualBorderMode == RibbonWindowMode.NonClientAreaGlass)
                {
                    using (GraphicsPath path = new GraphicsPath())
                    {
                        path.AddString(form.Text, font.FontFamily, (int) font.Style, font.SizeInPoints + 3f, captionBar, format);
                        if (form.WindowState != FormWindowState.Maximized)
                        {
                            using (Pen pen = new Pen(Color.FromArgb(90, Color.White), 4f))
                            {
                                e.Graphics.DrawPath(pen, path);
                            }
                        }
                        e.Graphics.FillPath((form.WindowState == FormWindowState.Maximized) ? Brushes.White : Brushes.Black, path);
                    }
                }
                else if (e.Ribbon.ActualBorderMode == RibbonWindowMode.NonClientAreaCustomDrawn)
                {
                    TextRenderer.DrawText(e.Graphics, form.Text, font, captionBar, this.ColorTable.FormBorder);
                }
            }
        }

        public void DrawComboxDropDown(Graphics g, RibbonComboBox b)
        {
            if (b.DropDownButtonPressed)
            {
                this.DrawButtonPressed(g, b.DropDownButtonBounds, Corners.None);
            }
            else if (b.DropDownButtonSelected)
            {
                this.DrawButtonSelected(g, b.DropDownButtonBounds, Corners.None);
            }
            else if (b.Selected)
            {
                this.DrawButton(g, b.DropDownButtonBounds, Corners.None);
            }
            this.DrawArrowShaded(g, b.DropDownButtonBounds, RibbonArrowDirection.Down, true);
        }

        public void DrawCompleteTab(RibbonTabRenderEventArgs e)
        {
            GraphicsPath path;
            Pen pen;
            this.DrawTabActive(e);
            using (path = RoundRectangle(e.Tab.TabContentBounds, 4))
            {
                Color tabContentNorth = this.ColorTable.TabContentNorth;
                Color tabContentSouth = this.ColorTable.TabContentSouth;
                if (e.Tab.Contextual)
                {
                    tabContentNorth = this.ColorTable.DropDownBg;
                    tabContentSouth = tabContentNorth;
                }
                using (LinearGradientBrush brush = new LinearGradientBrush(new Point(0, e.Tab.TabContentBounds.Top + 30), new Point(0, e.Tab.TabContentBounds.Bottom - 10), tabContentNorth, tabContentSouth))
                {
                    brush.WrapMode = WrapMode.TileFlipXY;
                    e.Graphics.FillPath(brush, path);
                }
            }
            using (path = RoundRectangle(Rectangle.FromLTRB(e.Tab.TabContentBounds.Left, e.Tab.TabContentBounds.Top, e.Tab.TabContentBounds.Right, e.Tab.TabContentBounds.Top + 0x12), 6, Corners.North))
            {
                using (Brush brush2 = new SolidBrush(Color.FromArgb(30, Color.White)))
                {
                    e.Graphics.FillPath(brush2, path);
                }
            }
            using (path = this.CreateCompleteTabPath(e.Tab))
            {
                using (pen = new Pen(this.ColorTable.TabBorder))
                {
                    e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
                    e.Graphics.DrawPath(pen, path);
                }
            }
            if (e.Tab.Selected)
            {
                using (path = this.CreateTabPath(e.Tab))
                {
                    pen = new Pen(Color.FromArgb(150, Color.Gold));
                    pen.Width = 2f;
                    e.Graphics.DrawPath(pen, path);
                    pen.Dispose();
                }
            }
        }

        public void DrawGripDot(Graphics g, Point location)
        {
            SolidBrush brush;
            Rectangle rect = new Rectangle(location.X - 1, location.Y + 1, 2, 2);
            Rectangle rectangle2 = new Rectangle(location, new Size(2, 2));
            using (brush = new SolidBrush(this.ColorTable.DropDownGripLight))
            {
                g.FillRectangle(brush, rect);
            }
            using (brush = new SolidBrush(this.ColorTable.DropDownGripDark))
            {
                g.FillRectangle(brush, rectangle2);
            }
        }

        public void DrawItemGroup(RibbonItemRenderEventArgs e, RibbonItemGroup grp)
        {
            LinearGradientBrush brush;
            Rectangle r = Rectangle.FromLTRB(grp.Bounds.Left, grp.Bounds.Top, grp.Bounds.Right - 1, grp.Bounds.Bottom - 1);
            Rectangle rectangle2 = Rectangle.FromLTRB(r.Left + 1, r.Top + 1, r.Right - 1, r.Bottom - 1);
            Rectangle rectangle3 = Rectangle.FromLTRB(r.Left + 1, (r.Top + (r.Height / 2)) + 1, r.Right - 1, r.Bottom - 1);
            GraphicsPath path = RoundRectangle(r, 2);
            GraphicsPath path2 = RoundRectangle(rectangle2, 2);
            GraphicsPath path3 = RoundRectangle(rectangle3, 2);
            using (brush = new LinearGradientBrush(rectangle2, this.ColorTable.ItemGroupBgNorth, this.ColorTable.ItemGroupBgSouth, 90f))
            {
                e.Graphics.FillPath(brush, path2);
            }
            using (brush = new LinearGradientBrush(rectangle3, this.ColorTable.ItemGroupBgGlossy, Color.Transparent, 90f))
            {
                e.Graphics.FillPath(brush, path3);
            }
            path.Dispose();
            path2.Dispose();
        }

        public void DrawItemGroupBorder(RibbonItemRenderEventArgs e, RibbonItemGroup grp)
        {
            Pen pen3;
            Rectangle r = Rectangle.FromLTRB(grp.Bounds.Left, grp.Bounds.Top, grp.Bounds.Right - 1, grp.Bounds.Bottom - 1);
            Rectangle rectangle2 = Rectangle.FromLTRB(r.Left + 1, r.Top + 1, r.Right - 1, r.Bottom - 1);
            GraphicsPath path = RoundRectangle(r, 2);
            GraphicsPath path2 = RoundRectangle(rectangle2, 2);
            using (Pen pen = new Pen(this.ColorTable.ItemGroupSeparatorDark))
            {
                using (Pen pen2 = new Pen(this.ColorTable.ItemGroupSeparatorLight))
                {
                    foreach (RibbonItem item in grp.Items)
                    {
                        if (item == grp.LastItem)
                        {
                            goto Label_01E2;
                        }
                        e.Graphics.DrawLine(pen, new Point(item.Bounds.Right, item.Bounds.Top), new Point(item.Bounds.Right, item.Bounds.Bottom));
                        e.Graphics.DrawLine(pen2, new Point(item.Bounds.Right + 1, item.Bounds.Top), new Point(item.Bounds.Right + 1, item.Bounds.Bottom));
                    }
                }
            }
        Label_01E2:
            using (pen3 = new Pen(this.ColorTable.ItemGroupOuterBorder))
            {
                e.Graphics.DrawPath(pen3, path);
            }
            using (pen3 = new Pen(this.ColorTable.ItemGroupInnerBorder))
            {
                e.Graphics.DrawPath(pen3, path2);
            }
            path.Dispose();
            path2.Dispose();
        }

        public void DrawOrb(Graphics g, Rectangle r, Image image, bool selected, bool pressed)
        {
            int num;
            int num2;
            Color orbPressedBackgroundDark;
            Color orbPressedBackgroundMedium;
            Color orbPressedBackgroundLight;
            Color orbPressedLight;
            GraphicsPath path;
            PathGradientBrush brush;
            Blend blend;
            LinearGradientBrush brush2;
            Color[] colorArray;
            float[] numArray;
            Rectangle rect = r;
            rect.Inflate(-1, -1);
            Rectangle rectangle2 = r;
            rectangle2.Offset(1, 1);
            rectangle2.Inflate(2, 2);
            if (pressed)
            {
                orbPressedBackgroundDark = this.ColorTable.OrbPressedBackgroundDark;
                orbPressedBackgroundMedium = this.ColorTable.OrbPressedBackgroundMedium;
                orbPressedBackgroundLight = this.ColorTable.OrbPressedBackgroundLight;
                orbPressedLight = this.ColorTable.OrbPressedLight;
            }
            else if (selected)
            {
                orbPressedBackgroundDark = this.ColorTable.OrbSelectedBackgroundDark;
                orbPressedBackgroundMedium = this.ColorTable.OrbSelectedBackgroundDark;
                orbPressedBackgroundLight = this.ColorTable.OrbSelectedBackgroundLight;
                orbPressedLight = this.ColorTable.OrbSelectedLight;
            }
            else
            {
                orbPressedBackgroundDark = this.ColorTable.OrbBackgroundDark;
                orbPressedBackgroundMedium = this.ColorTable.OrbBackgroundMedium;
                orbPressedBackgroundLight = this.ColorTable.OrbBackgroundLight;
                orbPressedLight = this.ColorTable.OrbLight;
            }
            using (path = new GraphicsPath())
            {
                path.AddEllipse(rectangle2);
                using (brush = new PathGradientBrush(path))
                {
                    brush.WrapMode = WrapMode.Clamp;
                    brush.CenterPoint = new PointF((float) (rectangle2.Left + (rectangle2.Width / 2)), (float) (rectangle2.Top + (rectangle2.Height / 2)));
                    brush.CenterColor = Color.FromArgb(180, Color.Black);
                    colorArray = new Color[] { Color.Transparent };
                    brush.SurroundColors = colorArray;
                    blend = new Blend(3);
                    numArray = new float[3];
                    numArray[1] = 1f;
                    numArray[2] = 1f;
                    blend.Factors = numArray;
                    numArray = new float[3];
                    numArray[1] = 0.2f;
                    numArray[2] = 1f;
                    blend.Positions = numArray;
                    brush.Blend = blend;
                    g.FillPath(brush, path);
                }
            }
            using (Pen pen = new Pen(orbPressedBackgroundDark, 1f))
            {
                g.DrawEllipse(pen, r);
            }
            using (path = new GraphicsPath())
            {
                path.AddEllipse(r);
                using (brush = new PathGradientBrush(path))
                {
                    brush.WrapMode = WrapMode.Clamp;
                    brush.CenterPoint = new PointF(Convert.ToSingle((int) (r.Left + (r.Width / 2))), Convert.ToSingle(r.Bottom));
                    brush.CenterColor = orbPressedBackgroundLight;
                    colorArray = new Color[] { orbPressedBackgroundMedium };
                    brush.SurroundColors = colorArray;
                    blend = new Blend(3);
                    numArray = new float[3];
                    numArray[1] = 0.8f;
                    numArray[2] = 1f;
                    blend.Factors = numArray;
                    numArray = new float[3];
                    numArray[1] = 0.5f;
                    numArray[2] = 1f;
                    blend.Positions = numArray;
                    brush.Blend = blend;
                    g.FillPath(brush, path);
                }
            }
            Rectangle rectangle3 = new Rectangle(0, 0, r.Width / 2, r.Height / 2);
            rectangle3.X = r.X + ((r.Width - rectangle3.Width) / 2);
            rectangle3.Y = r.Y + (r.Height / 2);
            using (path = new GraphicsPath())
            {
                path.AddEllipse(rectangle3);
                using (brush = new PathGradientBrush(path))
                {
                    brush.WrapMode = WrapMode.Clamp;
                    brush.CenterPoint = new PointF(Convert.ToSingle((int) (r.Left + (r.Width / 2))), Convert.ToSingle(r.Bottom));
                    brush.CenterColor = Color.White;
                    colorArray = new Color[] { Color.Transparent };
                    brush.SurroundColors = colorArray;
                    g.FillPath(brush, path);
                }
            }
            using (path = new GraphicsPath())
            {
                num = 160;
                num2 = 180 + ((180 - num) / 2);
                path.AddArc(rect, (float) num2, (float) num);
                Point point = Point.Round(path.PathData.Points[0]);
                Point point2 = Point.Round(path.PathData.Points[path.PathData.Points.Length - 1]);
                Point point3 = new Point(rect.Left + (rect.Width / 2), point2.Y - 3);
                Point[] points = new Point[] { point2, point3, point };
                path.AddCurve(points);
                using (brush = new PathGradientBrush(path))
                {
                    brush.WrapMode = WrapMode.Clamp;
                    brush.CenterPoint = (PointF) point3;
                    brush.CenterColor = Color.Transparent;
                    colorArray = new Color[] { orbPressedLight };
                    brush.SurroundColors = colorArray;
                    blend = new Blend(3);
                    blend.Factors = new float[] { 0.3f, 0.8f, 1f };
                    numArray = new float[3];
                    numArray[1] = 0.5f;
                    numArray[2] = 1f;
                    blend.Positions = numArray;
                    brush.Blend = blend;
                    g.FillPath(brush, path);
                }
                using (brush2 = new LinearGradientBrush(new Point(r.Left, r.Top), new Point(r.Left, point.Y), Color.White, Color.Transparent))
                {
                    blend = new Blend(4);
                    blend.Factors = new float[] { 0f, 0.4f, 0.8f, 1f };
                    blend.Positions = new float[] { 0f, 0.3f, 0.4f, 1f };
                    brush2.Blend = blend;
                    g.FillPath(brush2, path);
                }
            }
            using (path = new GraphicsPath())
            {
                num = 160;
                num2 = 180 + ((180 - num) / 2);
                path.AddArc(rect, (float) num2, (float) num);
                using (Pen pen2 = new Pen(Color.White))
                {
                    g.DrawPath(pen2, path);
                }
            }
            using (path = new GraphicsPath())
            {
                num = 160;
                num2 = (180 - num) / 2;
                path.AddArc(rect, (float) num2, (float) num);
                Point point4 = Point.Round(path.PathData.Points[0]);
                Rectangle rectangle4 = rect;
                rectangle4.Inflate(-1, -1);
                num = 160;
                num2 = (180 - num) / 2;
                path.AddArc(rectangle4, (float) num2, (float) num);
                using (brush2 = new LinearGradientBrush(new Point(rect.Left, rect.Bottom), new Point(rect.Left, point4.Y - 1), orbPressedLight, Color.FromArgb(50, orbPressedLight)))
                {
                    g.FillPath(brush2, path);
                }
            }
            if (image != null)
            {
                Rectangle rectangle5 = new Rectangle(Point.Empty, image.Size);
                rectangle5.X = r.X + ((r.Width - rectangle5.Width) / 2);
                rectangle5.Y = r.Y + ((r.Height - rectangle5.Height) / 2);
                g.DrawImage(image, rectangle5);
            }
        }

        public void DrawOrbOptionButton(Graphics g, Rectangle bounds)
        {
            bounds.Width--;
            bounds.Height--;
            using (GraphicsPath path = RoundRectangle(bounds, 3))
            {
                using (SolidBrush brush = new SolidBrush(this.ColorTable.OrbOptionBackground))
                {
                    g.FillPath(brush, path);
                }
                this.GradientRect(g, Rectangle.FromLTRB(bounds.Left, bounds.Top + (bounds.Height / 2), bounds.Right, bounds.Bottom - 2), this.ColorTable.OrbOptionShine, this.ColorTable.OrbOptionBackground);
                using (Pen pen = new Pen(this.ColorTable.OrbOptionBorder))
                {
                    g.DrawPath(pen, path);
                }
            }
        }

        public void DrawPanelNormal(RibbonPanelRenderEventArgs e)
        {
            Pen pen;
            Rectangle r = Rectangle.FromLTRB(e.Panel.Bounds.Left, e.Panel.Bounds.Top, e.Panel.Bounds.Right, e.Panel.Bounds.Bottom);
            Rectangle rectangle2 = Rectangle.FromLTRB(e.Panel.Bounds.Left + 1, e.Panel.Bounds.Top + 1, e.Panel.Bounds.Right + 1, e.Panel.Bounds.Bottom);
            Rectangle rectangle3 = Rectangle.FromLTRB(e.Panel.Bounds.Left + 1, e.Panel.ContentBounds.Bottom, e.Panel.Bounds.Right - 1, e.Panel.Bounds.Bottom - 1);
            GraphicsPath path = RoundRectangle(r, 3);
            GraphicsPath path2 = RoundRectangle(rectangle2, 3);
            GraphicsPath path3 = RoundRectangle(rectangle3, 3, Corners.South);
            using (pen = new Pen(this.ColorTable.PanelLightBorder))
            {
                e.Graphics.DrawPath(pen, path2);
            }
            using (pen = new Pen(this.ColorTable.PanelDarkBorder))
            {
                e.Graphics.DrawPath(pen, path);
            }
            using (SolidBrush brush = new SolidBrush(this.ColorTable.PanelTextBackground))
            {
                e.Graphics.FillPath(brush, path3);
            }
            if (e.Panel.ButtonMoreVisible)
            {
                this.DrawButtonMoreGlyph(e.Graphics, e.Panel.ButtonMoreBounds, e.Panel.ButtonMoreEnabled && e.Panel.Enabled);
            }
            path3.Dispose();
            path.Dispose();
            path2.Dispose();
        }

        public void DrawPanelOverflowImage(RibbonPanelRenderEventArgs e)
        {
            SolidBrush brush2;
            int num = 3;
            Size size = new Size(0x20, 0x20);
            Rectangle r = new Rectangle(new Point(e.Panel.Bounds.Left + ((e.Panel.Bounds.Width - size.Width) / 2), e.Panel.Bounds.Top + 5), size);
            Rectangle rectangle2 = Rectangle.FromLTRB(r.Left, r.Bottom - 10, r.Right, r.Bottom);
            Rectangle layoutRectangle = Rectangle.FromLTRB(e.Panel.Bounds.Left + num, r.Bottom + num, e.Panel.Bounds.Right - num, e.Panel.Bounds.Bottom - num);
            GraphicsPath path = RoundRectangle(r, 5);
            GraphicsPath path2 = RoundRectangle(rectangle2, 5, Corners.South);
            using (LinearGradientBrush brush = new LinearGradientBrush(r, this.ColorTable.TabContentNorth, this.ColorTable.TabContentSouth, 90f))
            {
                e.Graphics.FillPath(brush, path);
            }
            using (brush2 = new SolidBrush(this.ColorTable.PanelTextBackground))
            {
                e.Graphics.FillPath(brush2, path2);
            }
            using (Pen pen = new Pen(this.ColorTable.PanelDarkBorder))
            {
                e.Graphics.DrawPath(pen, path);
            }
            if (e.Panel.Image != null)
            {
                e.Graphics.DrawImage(e.Panel.Image, r.Left + ((r.Width - e.Panel.Image.Width) / 2), r.Top + (((r.Height - rectangle2.Height) - e.Panel.Image.Height) / 2), e.Panel.Image.Width, e.Panel.Image.Height);
            }
            using (brush2 = new SolidBrush(this.GetTextColor(e.Panel.Enabled)))
            {
                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Near;
                format.Trimming = StringTrimming.Character;
                e.Graphics.DrawString(e.Panel.Text, e.Ribbon.Font, brush2, layoutRectangle, format);
            }
            Rectangle b = this.LargeButtonDropDownArrowBounds(e.Graphics, e.Panel.Owner.Font, e.Panel.Text, layoutRectangle);
            if (b.Right < e.Panel.Bounds.Right)
            {
                Rectangle rectangle5 = b;
                rectangle5.Offset(0, 1);
                Color arrowLight = this.ColorTable.ArrowLight;
                Color arrow = this.ColorTable.Arrow;
                this.DrawArrow(e.Graphics, rectangle5, arrowLight, RibbonArrowDirection.Down);
                this.DrawArrow(e.Graphics, b, arrow, RibbonArrowDirection.Down);
            }
            path.Dispose();
            path2.Dispose();
        }

        public void DrawPanelOverflowNormal(RibbonPanelRenderEventArgs e)
        {
            Pen pen;
            Rectangle r = Rectangle.FromLTRB(e.Panel.Bounds.Left, e.Panel.Bounds.Top, e.Panel.Bounds.Right, e.Panel.Bounds.Bottom);
            Rectangle rectangle2 = Rectangle.FromLTRB(e.Panel.Bounds.Left + 1, e.Panel.Bounds.Top + 1, e.Panel.Bounds.Right - 1, e.Panel.Bounds.Bottom - 1);
            GraphicsPath path = RoundRectangle(r, 3);
            GraphicsPath path2 = RoundRectangle(rectangle2, 3);
            using (pen = new Pen(this.ColorTable.PanelLightBorder))
            {
                e.Graphics.DrawPath(pen, path2);
            }
            using (pen = new Pen(this.ColorTable.PanelDarkBorder))
            {
                e.Graphics.DrawPath(pen, path);
            }
            this.DrawPanelOverflowImage(e);
            path.Dispose();
            path2.Dispose();
        }

        public void DrawPanelOverflowPressed(RibbonPanelRenderEventArgs e)
        {
            LinearGradientBrush brush;
            Pen pen;
            Rectangle r = Rectangle.FromLTRB(e.Panel.Bounds.Left, e.Panel.Bounds.Top, e.Panel.Bounds.Right, e.Panel.Bounds.Bottom);
            Rectangle rectangle2 = Rectangle.FromLTRB(e.Panel.Bounds.Left + 1, e.Panel.Bounds.Top + 1, e.Panel.Bounds.Right - 1, e.Panel.Bounds.Bottom - 1);
            Rectangle rectangle3 = Rectangle.FromLTRB(e.Panel.Bounds.Left, e.Panel.Bounds.Top, e.Panel.Bounds.Right, e.Panel.Bounds.Top + 0x11);
            GraphicsPath path = RoundRectangle(r, 3);
            GraphicsPath path2 = RoundRectangle(rectangle2, 3);
            using (brush = new LinearGradientBrush(rectangle2, this.ColorTable.PanelOverflowBackgroundPressed, this.ColorTable.PanelOverflowBackgroundSelectedSouth, 90f))
            {
                brush.WrapMode = WrapMode.TileFlipXY;
                e.Graphics.FillPath(brush, path);
            }
            using (GraphicsPath path3 = RoundRectangle(rectangle3, 3, Corners.North))
            {
                using (brush = new LinearGradientBrush(rectangle3, Color.FromArgb(150, Color.White), Color.FromArgb(50, Color.White), 90f))
                {
                    brush.WrapMode = WrapMode.TileFlipXY;
                    e.Graphics.FillPath(brush, path3);
                }
            }
            using (pen = new Pen(Color.FromArgb(40, Color.White)))
            {
                e.Graphics.DrawPath(pen, path2);
            }
            using (pen = new Pen(this.ColorTable.PanelDarkBorder))
            {
                e.Graphics.DrawPath(pen, path);
            }
            this.DrawPanelOverflowImage(e);
            this.DrawPressedShadow(e.Graphics, rectangle3);
            path.Dispose();
            path2.Dispose();
        }

        public void DrawPanelSelected(RibbonPanelRenderEventArgs e)
        {
            Pen pen;
            SolidBrush brush;
            Rectangle r = Rectangle.FromLTRB(e.Panel.Bounds.Left, e.Panel.Bounds.Top, e.Panel.Bounds.Right, e.Panel.Bounds.Bottom);
            Rectangle rectangle2 = Rectangle.FromLTRB(e.Panel.Bounds.Left + 1, e.Panel.Bounds.Top + 1, e.Panel.Bounds.Right - 1, e.Panel.Bounds.Bottom - 1);
            Rectangle rectangle3 = Rectangle.FromLTRB(e.Panel.Bounds.Left + 1, e.Panel.ContentBounds.Bottom, e.Panel.Bounds.Right - 1, e.Panel.Bounds.Bottom - 1);
            GraphicsPath path = RoundRectangle(r, 3);
            GraphicsPath path2 = RoundRectangle(rectangle2, 3);
            GraphicsPath path3 = RoundRectangle(rectangle3, 3, Corners.South);
            using (pen = new Pen(this.ColorTable.PanelLightBorder))
            {
                e.Graphics.DrawPath(pen, path2);
            }
            using (pen = new Pen(this.ColorTable.PanelDarkBorder))
            {
                e.Graphics.DrawPath(pen, path);
            }
            using (brush = new SolidBrush(this.ColorTable.PanelBackgroundSelected))
            {
                e.Graphics.FillPath(brush, path2);
            }
            using (brush = new SolidBrush(this.ColorTable.PanelTextBackgroundSelected))
            {
                e.Graphics.FillPath(brush, path3);
            }
            if (e.Panel.ButtonMoreVisible)
            {
                if (e.Panel.ButtonMorePressed)
                {
                    this.DrawButtonPressed(e.Graphics, e.Panel.ButtonMoreBounds, Corners.SouthEast);
                }
                else if (e.Panel.ButtonMoreSelected)
                {
                    this.DrawButtonSelected(e.Graphics, e.Panel.ButtonMoreBounds, Corners.SouthEast);
                }
                this.DrawButtonMoreGlyph(e.Graphics, e.Panel.ButtonMoreBounds, e.Panel.ButtonMoreEnabled && e.Panel.Enabled);
            }
            path3.Dispose();
            path.Dispose();
            path2.Dispose();
        }

        public void DrawPannelOveflowSelected(RibbonPanelRenderEventArgs e)
        {
            Pen pen;
            Rectangle r = Rectangle.FromLTRB(e.Panel.Bounds.Left, e.Panel.Bounds.Top, e.Panel.Bounds.Right, e.Panel.Bounds.Bottom);
            Rectangle rectangle2 = Rectangle.FromLTRB(e.Panel.Bounds.Left + 1, e.Panel.Bounds.Top + 1, e.Panel.Bounds.Right - 1, e.Panel.Bounds.Bottom - 1);
            GraphicsPath path = RoundRectangle(r, 3);
            GraphicsPath path2 = RoundRectangle(rectangle2, 3);
            using (pen = new Pen(this.ColorTable.PanelLightBorder))
            {
                e.Graphics.DrawPath(pen, path2);
            }
            using (pen = new Pen(this.ColorTable.PanelDarkBorder))
            {
                e.Graphics.DrawPath(pen, path);
            }
            using (LinearGradientBrush brush = new LinearGradientBrush(rectangle2, this.ColorTable.PanelOverflowBackgroundSelectedNorth, Color.Transparent, 90f))
            {
                e.Graphics.FillPath(brush, path2);
            }
            this.DrawPanelOverflowImage(e);
            path.Dispose();
            path2.Dispose();
        }

        public void DrawPressedShadow(Graphics g, Rectangle r)
        {
            Rectangle rectangle = Rectangle.FromLTRB(r.Left, r.Top, r.Right, r.Top + 4);
            using (GraphicsPath path = RoundRectangle(rectangle, 3, Corners.North))
            {
                using (LinearGradientBrush brush = new LinearGradientBrush(rectangle, Color.FromArgb(50, Color.Black), Color.FromArgb(0, Color.Black), 90f))
                {
                    brush.WrapMode = WrapMode.TileFlipXY;
                    g.FillPath(brush, path);
                }
            }
        }

        public void DrawSeparator(Graphics g, RibbonSeparator separator)
        {
            Pen pen;
            if (separator.SizeMode == RibbonElementSizeMode.DropDown)
            {
                if (!string.IsNullOrEmpty(separator.Text))
                {
                    using (SolidBrush brush = new SolidBrush(this.ColorTable.SeparatorBg))
                    {
                        g.FillRectangle(brush, separator.Bounds);
                    }
                    using (pen = new Pen(this.ColorTable.SeparatorLine))
                    {
                        g.DrawLine(pen, new Point(separator.Bounds.Left, separator.Bounds.Bottom), new Point(separator.Bounds.Right, separator.Bounds.Bottom));
                    }
                }
                else
                {
                    using (pen = new Pen(this.ColorTable.DropDownImageSeparator))
                    {
                        g.DrawLine(pen, new Point(separator.Bounds.Left + 30, separator.Bounds.Top + 1), new Point(separator.Bounds.Right, separator.Bounds.Top + 1));
                    }
                }
            }
            else
            {
                using (pen = new Pen(this.ColorTable.SeparatorDark))
                {
                    g.DrawLine(pen, new Point(separator.Bounds.Left, separator.Bounds.Top), new Point(separator.Bounds.Left, separator.Bounds.Bottom));
                }
                using (pen = new Pen(this.ColorTable.SeparatorLight))
                {
                    g.DrawLine(pen, new Point(separator.Bounds.Left + 1, separator.Bounds.Top), new Point(separator.Bounds.Left + 1, separator.Bounds.Bottom));
                }
            }
        }

        public void DrawSplitButton(RibbonItemRenderEventArgs e, RibbonButton button)
        {
        }

        public void DrawSplitButtonDropDownPressed(RibbonItemRenderEventArgs e, RibbonButton button)
        {
        }

        public void DrawSplitButtonDropDownSelected(RibbonItemRenderEventArgs e, RibbonButton button)
        {
            Pen pen;
            Rectangle r = Rectangle.FromLTRB(button.DropDownBounds.Left, button.DropDownBounds.Top, button.DropDownBounds.Right - 1, button.DropDownBounds.Bottom - 1);
            Rectangle rectangle2 = Rectangle.FromLTRB(r.Left + 1, r.Top + ((button.SizeMode == RibbonElementSizeMode.Large) ? 1 : 0), r.Right - 1, r.Bottom - 1);
            Rectangle rectangle3 = Rectangle.FromLTRB(button.ButtonFaceBounds.Left, button.ButtonFaceBounds.Top, button.ButtonFaceBounds.Right - 1, button.ButtonFaceBounds.Bottom - 1);
            Rectangle rectangle4 = Rectangle.FromLTRB(rectangle3.Left + 1, rectangle3.Top + 1, rectangle3.Right + ((button.SizeMode == RibbonElementSizeMode.Large) ? -1 : 0), rectangle3.Bottom + ((button.SizeMode == RibbonElementSizeMode.Large) ? 0 : -1));
            Corners corners = this.ButtonFaceRounding(button);
            Corners corners2 = this.ButtonDdRounding(button);
            GraphicsPath path = RoundRectangle(r, 3, corners2);
            GraphicsPath path2 = RoundRectangle(rectangle2, 2, corners2);
            GraphicsPath path3 = RoundRectangle(rectangle3, 3, corners);
            GraphicsPath path4 = RoundRectangle(rectangle4, 2, corners);
            using (SolidBrush brush = new SolidBrush(Color.FromArgb(150, Color.White)))
            {
                e.Graphics.FillPath(brush, path4);
            }
            using (pen = new Pen((button.Pressed && (button.SizeMode != RibbonElementSizeMode.DropDown)) ? this.ColorTable.ButtonPressedBorderIn : this.ColorTable.ButtonSelectedBorderIn))
            {
                e.Graphics.DrawPath(pen, path4);
            }
            using (pen = new Pen((button.Pressed && (button.SizeMode != RibbonElementSizeMode.DropDown)) ? this.ColorTable.ButtonPressedBorderOut : this.ColorTable.ButtonSelectedBorderOut))
            {
                e.Graphics.DrawPath(pen, path3);
            }
            path.Dispose();
            path2.Dispose();
            path3.Dispose();
            path4.Dispose();
        }

        public void DrawSplitButtonPressed(RibbonItemRenderEventArgs e, RibbonButton button)
        {
        }

        public void DrawSplitButtonSelected(RibbonItemRenderEventArgs e, RibbonButton button)
        {
            Pen pen;
            Rectangle r = Rectangle.FromLTRB(button.DropDownBounds.Left, button.DropDownBounds.Top, button.DropDownBounds.Right - 1, button.DropDownBounds.Bottom - 1);
            Rectangle rectangle2 = Rectangle.FromLTRB(r.Left + 1, r.Top + 1, r.Right - 1, r.Bottom - 1);
            Rectangle rectangle3 = Rectangle.FromLTRB(button.ButtonFaceBounds.Left, button.ButtonFaceBounds.Top, button.ButtonFaceBounds.Right - 1, button.ButtonFaceBounds.Bottom - 1);
            Rectangle rectangle4 = Rectangle.FromLTRB(rectangle3.Left + 1, rectangle3.Top + 1, rectangle3.Right + ((button.SizeMode == RibbonElementSizeMode.Large) ? -1 : 0), rectangle3.Bottom + ((button.SizeMode == RibbonElementSizeMode.Large) ? 0 : -1));
            Corners corners = this.ButtonFaceRounding(button);
            Corners corners2 = this.ButtonDdRounding(button);
            GraphicsPath path = RoundRectangle(r, 3, corners2);
            GraphicsPath path2 = RoundRectangle(rectangle2, 2, corners2);
            GraphicsPath path3 = RoundRectangle(rectangle3, 3, corners);
            GraphicsPath path4 = RoundRectangle(rectangle4, 2, corners);
            using (SolidBrush brush = new SolidBrush(Color.FromArgb(150, Color.White)))
            {
                e.Graphics.FillPath(brush, path2);
            }
            using (pen = new Pen((button.Pressed && (button.SizeMode != RibbonElementSizeMode.DropDown)) ? this.ColorTable.ButtonPressedBorderOut : this.ColorTable.ButtonSelectedBorderOut))
            {
                e.Graphics.DrawPath(pen, path);
            }
            using (pen = new Pen((button.Pressed && (button.SizeMode != RibbonElementSizeMode.DropDown)) ? this.ColorTable.ButtonPressedBorderIn : this.ColorTable.ButtonSelectedBorderIn))
            {
                e.Graphics.DrawPath(pen, path4);
            }
            path.Dispose();
            path2.Dispose();
            path3.Dispose();
            path4.Dispose();
        }

        public void DrawTabActive(RibbonTabRenderEventArgs e)
        {
            GraphicsPath path;
            this.DrawTabNormal(e);
            Rectangle r = new Rectangle(e.Tab.TabBounds.Left, e.Tab.TabBounds.Top, e.Tab.TabBounds.Width, 4);
            Rectangle tabBounds = e.Tab.TabBounds;
            tabBounds.Offset(2, 1);
            Rectangle rectangle3 = e.Tab.TabBounds;
            using (path = RoundRectangle(tabBounds, 6, Corners.North))
            {
                using (PathGradientBrush brush = new PathGradientBrush(path))
                {
                    brush.WrapMode = WrapMode.Clamp;
                    ColorBlend blend = new ColorBlend(3);
                    blend.Colors = new Color[] { Color.Transparent, Color.FromArgb(50, Color.Black), Color.FromArgb(100, Color.Black) };
                    float[] numArray = new float[3];
                    numArray[1] = 0.1f;
                    numArray[2] = 1f;
                    blend.Positions = numArray;
                    brush.InterpolationColors = blend;
                    e.Graphics.FillPath(brush, path);
                }
            }
            using (path = RoundRectangle(rectangle3, 6, Corners.North))
            {
                Color tabNorth = this.ColorTable.TabNorth;
                Color tabSouth = this.ColorTable.TabSouth;
                if (e.Tab.Contextual)
                {
                    tabNorth = e.Tab.Context.GlowColor;
                    tabSouth = Color.FromArgb(10, tabNorth);
                }
                using (Pen pen = new Pen(this.ColorTable.TabNorth, 1.6f))
                {
                    e.Graphics.DrawPath(pen, path);
                }
                using (LinearGradientBrush brush2 = new LinearGradientBrush(e.Tab.TabBounds, this.ColorTable.TabNorth, this.ColorTable.TabSouth, 90f))
                {
                    e.Graphics.FillPath(brush2, path);
                }
            }
            using (path = RoundRectangle(r, 6, Corners.North))
            {
                using (Brush brush3 = new SolidBrush(Color.FromArgb(180, Color.White)))
                {
                    e.Graphics.FillPath(brush3, path);
                }
            }
        }

        public void DrawTabNormal(RibbonTabRenderEventArgs e)
        {
            RectangleF clipBounds = e.Graphics.ClipBounds;
            Rectangle rect = Rectangle.FromLTRB(e.Tab.TabBounds.Left, e.Tab.TabBounds.Top, e.Tab.TabBounds.Right, e.Tab.TabBounds.Bottom);
            Rectangle rectangle2 = Rectangle.FromLTRB(e.Tab.TabBounds.Left - 1, e.Tab.TabBounds.Top - 1, e.Tab.TabBounds.Right, e.Tab.TabBounds.Bottom);
            e.Graphics.SetClip(rect);
            using (Brush brush = new SolidBrush(this.ColorTable.RibbonBackground))
            {
                e.Graphics.FillRectangle(brush, rectangle2);
            }
            e.Graphics.SetClip(clipBounds);
        }

        public void DrawTabPressed(RibbonTabRenderEventArgs e)
        {
        }

        public void DrawTabSelected(RibbonTabRenderEventArgs e)
        {
            Pen pen;
            Rectangle r = Rectangle.FromLTRB(e.Tab.TabBounds.Left, e.Tab.TabBounds.Top, e.Tab.TabBounds.Right - 1, e.Tab.TabBounds.Bottom);
            Rectangle rectangle2 = Rectangle.FromLTRB(r.Left + 1, r.Top + 1, r.Right - 1, r.Bottom);
            Rectangle rectangle3 = Rectangle.FromLTRB(rectangle2.Left + 1, rectangle2.Top + 1, rectangle2.Right - 1, rectangle2.Top + (e.Tab.TabBounds.Height / 2));
            GraphicsPath path = RoundRectangle(r, 3, Corners.North);
            GraphicsPath path2 = RoundRectangle(rectangle2, 3, Corners.North);
            GraphicsPath path3 = RoundRectangle(rectangle3, 3, Corners.North);
            using (pen = new Pen(this.ColorTable.TabBorder))
            {
                e.Graphics.DrawPath(pen, path);
            }
            using (pen = new Pen(Color.FromArgb(200, Color.White)))
            {
                e.Graphics.DrawPath(pen, path2);
            }
            using (GraphicsPath path4 = new GraphicsPath())
            {
                path4.AddRectangle(rectangle2);
                path4.CloseFigure();
                PathGradientBrush brush = new PathGradientBrush(path4);
                float x = Convert.ToSingle((int) (rectangle2.Left + (rectangle2.Width / 2)));
                brush.CenterPoint = new PointF(x, Convert.ToSingle((int) (rectangle2.Top - 5)));
                brush.CenterColor = Color.Transparent;
                brush.SurroundColors = new Color[] { this.ColorTable.TabSelectedGlow };
                Blend blend = new Blend(3);
                float[] numArray = new float[3];
                numArray[1] = 0.9f;
                blend.Factors = numArray;
                numArray = new float[3];
                numArray[1] = 0.8f;
                numArray[2] = 1f;
                blend.Positions = numArray;
                brush.Blend = blend;
                e.Graphics.FillPath(brush, path4);
                brush.Dispose();
            }
            using (SolidBrush brush2 = new SolidBrush(Color.FromArgb(100, Color.White)))
            {
                e.Graphics.FillPath(brush2, path3);
            }
            path.Dispose();
            path2.Dispose();
            path3.Dispose();
        }

        public void DrawTextBoxDisabled(Graphics g, Rectangle bounds)
        {
            using (SolidBrush brush = new SolidBrush(SystemColors.Control))
            {
                g.FillRectangle(brush, bounds);
            }
            using (Pen pen = new Pen(this.ColorTable.TextBoxBorder))
            {
                g.DrawRectangle(pen, bounds);
            }
        }

        public void DrawTextBoxSelected(Graphics g, Rectangle bounds)
        {
            using (RoundRectangle(bounds, 3))
            {
                using (SolidBrush brush = new SolidBrush(SystemColors.Window))
                {
                    g.FillRectangle(brush, bounds);
                }
                using (Pen pen = new Pen(this.ColorTable.TextBoxBorder))
                {
                    g.DrawRectangle(pen, bounds);
                }
            }
        }

        public void DrawTextBoxUnselected(Graphics g, Rectangle bounds)
        {
            using (SolidBrush brush = new SolidBrush(this.ColorTable.TextBoxUnselectedBg))
            {
                g.FillRectangle(brush, bounds);
            }
            using (Pen pen = new Pen(this.ColorTable.TextBoxBorder))
            {
                g.DrawRectangle(pen, bounds);
            }
        }

        public Color GetTextColor(bool enabled)
        {
            return this.GetTextColor(enabled, this.ColorTable.Text);
        }

        public Color GetTextColor(bool enabled, Color alternative)
        {
            if (enabled)
            {
                return alternative;
            }
            return this.ColorTable.ArrowDisabled;
        }

        private void GradientRect(Graphics g, Rectangle r, Color northColor, Color southColor)
        {
            using (Brush brush = new LinearGradientBrush(new Point(r.X, r.Y - 1), new Point(r.Left, r.Bottom), northColor, southColor))
            {
                g.FillRectangle(brush, r);
            }
        }

        public Rectangle LargeButtonDropDownArrowBounds(Graphics g, Font font, string text, Rectangle textLayout)
        {
            bool flag = text.Contains(" ");
            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = flag ? StringAlignment.Center : StringAlignment.Near;
            stringFormat.Trimming = StringTrimming.EllipsisCharacter;
            CharacterRange[] ranges = new CharacterRange[] { new CharacterRange(0, text.Length) };
            stringFormat.SetMeasurableCharacterRanges(ranges);
            Region[] regionArray = g.MeasureCharacterRanges(text, font, textLayout, stringFormat);
            Rectangle rectangle2 = Rectangle.Round(regionArray[regionArray.Length - 1].GetBounds(g));
            if (flag)
            {
                return new Rectangle(rectangle2.Right + 3, rectangle2.Top + ((rectangle2.Height - this.arrowSize.Height) / 2), this.arrowSize.Width, this.arrowSize.Height);
            }
            return new Rectangle(textLayout.Left + ((textLayout.Width - this.arrowSize.Width) / 2), rectangle2.Bottom + (((textLayout.Bottom - rectangle2.Bottom) - this.arrowSize.Height) / 2), this.arrowSize.Width, this.arrowSize.Height);
        }

        public override void OnRenderDropDownBackground(RibbonCanvasEventArgs e)
        {
            SolidBrush brush;
            Pen pen;
            Rectangle rect = new Rectangle(0, 0, e.Bounds.Width - 1, e.Bounds.Height - 1);
            Rectangle rectangle2 = new Rectangle(0, 0, 0x1a, e.Bounds.Height);
            RibbonDropDown canvas = e.Canvas as RibbonDropDown;
            using (brush = new SolidBrush(this.ColorTable.DropDownBg))
            {
                e.Graphics.Clear(Color.Transparent);
                SmoothingMode smoothingMode = e.Graphics.SmoothingMode;
                e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
                e.Graphics.FillRectangle(brush, rect);
                e.Graphics.SmoothingMode = smoothingMode;
            }
            if ((canvas != null) && canvas.DrawIconsBar)
            {
                using (brush = new SolidBrush(this.ColorTable.DropDownImageBg))
                {
                    e.Graphics.FillRectangle(brush, rectangle2);
                }
                using (pen = new Pen(this.ColorTable.DropDownImageSeparator))
                {
                    e.Graphics.DrawLine(pen, new Point(rectangle2.Right, rectangle2.Top), new Point(rectangle2.Right, rectangle2.Bottom));
                }
            }
            using (pen = new Pen(this.ColorTable.DropDownBorder))
            {
                if (canvas != null)
                {
                    using (GraphicsPath path = RoundRectangle(new Rectangle(Point.Empty, new Size(canvas.Size.Width - 1, canvas.Size.Height - 1)), canvas.BorderRoundness))
                    {
                        SmoothingMode mode2 = e.Graphics.SmoothingMode;
                        e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
                        e.Graphics.DrawPath(pen, path);
                        e.Graphics.SmoothingMode = mode2;
                    }
                }
                else
                {
                    e.Graphics.DrawRectangle(pen, rect);
                }
            }
            if (canvas.ShowSizingGrip)
            {
                Rectangle rectangle3 = Rectangle.FromLTRB(e.Bounds.Left + 1, e.Bounds.Bottom - canvas.SizingGripHeight, e.Bounds.Right - 1, e.Bounds.Bottom - 1);
                using (LinearGradientBrush brush2 = new LinearGradientBrush(rectangle3, this.ColorTable.DropDownGripNorth, this.ColorTable.DropDownGripSouth, 90f))
                {
                    e.Graphics.FillRectangle(brush2, rectangle3);
                }
                using (pen = new Pen(this.ColorTable.DropDownGripBorder))
                {
                    e.Graphics.DrawLine(pen, rectangle3.Location, new Point(rectangle3.Right - 1, rectangle3.Top));
                }
                this.DrawGripDot(e.Graphics, new Point(rectangle3.Right - 7, rectangle3.Bottom - 3));
                this.DrawGripDot(e.Graphics, new Point(rectangle3.Right - 3, rectangle3.Bottom - 7));
                this.DrawGripDot(e.Graphics, new Point(rectangle3.Right - 3, rectangle3.Bottom - 3));
            }
        }

        public override void OnRenderOrbDropDownBackground(RibbonOrbDropDownEventArgs e)
        {
            Brush brush;
            Pen pen;
            SolidBrush brush2;
            int width = e.RibbonOrbDropDown.Width;
            int height = e.RibbonOrbDropDown.Height;
            Rectangle contentBounds = e.RibbonOrbDropDown.ContentBounds;
            Rectangle contentButtonsBounds = e.RibbonOrbDropDown.ContentButtonsBounds;
            Rectangle r = new Rectangle(0, 0, width - 1, height - 1);
            Rectangle rectangle4 = new Rectangle(1, 1, width - 3, height - 3);
            Rectangle rectangle5 = new Rectangle(1, 1, width - 3, contentBounds.Top / 2);
            Rectangle rectangle6 = new Rectangle(1, rectangle5.Bottom, rectangle5.Width, contentBounds.Top / 2);
            Rectangle rectangle7 = Rectangle.FromLTRB(1, ((height - contentBounds.Bottom) / 2) + contentBounds.Bottom, width - 1, height - 1);
            Color orbDropDownDarkBorder = this.ColorTable.OrbDropDownDarkBorder;
            Color orbDropDownLightBorder = this.ColorTable.OrbDropDownLightBorder;
            Color orbDropDownBack = this.ColorTable.OrbDropDownBack;
            Color orbDropDownNorthA = this.ColorTable.OrbDropDownNorthA;
            Color orbDropDownNorthB = this.ColorTable.OrbDropDownNorthB;
            Color orbDropDownNorthC = this.ColorTable.OrbDropDownNorthC;
            Color orbDropDownNorthD = this.ColorTable.OrbDropDownNorthD;
            Color orbDropDownSouthC = this.ColorTable.OrbDropDownSouthC;
            Color orbDropDownSouthD = this.ColorTable.OrbDropDownSouthD;
            Color orbDropDownContentbg = this.ColorTable.OrbDropDownContentbg;
            Color orbDropDownContentbglight = this.ColorTable.OrbDropDownContentbglight;
            Color orbDropDownSeparatorlight = this.ColorTable.OrbDropDownSeparatorlight;
            Color orbDropDownSeparatordark = this.ColorTable.OrbDropDownSeparatordark;
            GraphicsPath path = RoundRectangle(rectangle4, 6);
            GraphicsPath path2 = RoundRectangle(r, 6);
            e.Graphics.SmoothingMode = SmoothingMode.None;
            using (brush = new SolidBrush(Color.FromArgb(0x8e, 0x8e, 0x8e)))
            {
                e.Graphics.FillRectangle(brush, new Rectangle(width - 10, height - 10, 10, 10));
            }
            using (brush = new SolidBrush(orbDropDownBack))
            {
                e.Graphics.FillPath(brush, path2);
            }
            this.GradientRect(e.Graphics, rectangle5, orbDropDownNorthA, orbDropDownNorthB);
            this.GradientRect(e.Graphics, rectangle6, orbDropDownNorthC, orbDropDownNorthD);
            this.GradientRect(e.Graphics, rectangle7, orbDropDownSouthC, orbDropDownSouthD);
            using (pen = new Pen(orbDropDownDarkBorder))
            {
                e.Graphics.DrawPath(pen, path2);
            }
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            using (pen = new Pen(orbDropDownLightBorder))
            {
                e.Graphics.DrawPath(pen, path);
            }
            path.Dispose();
            path2.Dispose();
            rectangle4 = contentBounds;
            rectangle4.Inflate(0, 0);
            r = contentBounds;
            r.Inflate(1, 1);
            using (brush2 = new SolidBrush(orbDropDownContentbg))
            {
                e.Graphics.FillRectangle(brush2, contentBounds);
            }
            using (brush2 = new SolidBrush(orbDropDownContentbglight))
            {
                e.Graphics.FillRectangle(brush2, contentButtonsBounds);
            }
            using (pen = new Pen(orbDropDownSeparatorlight))
            {
                e.Graphics.DrawLine(pen, contentButtonsBounds.Right, contentButtonsBounds.Top, contentButtonsBounds.Right, contentButtonsBounds.Bottom);
            }
            using (pen = new Pen(orbDropDownSeparatordark))
            {
                e.Graphics.DrawLine(pen, contentButtonsBounds.Right - 1, contentButtonsBounds.Top, contentButtonsBounds.Right - 1, contentButtonsBounds.Bottom);
            }
            using (pen = new Pen(orbDropDownLightBorder))
            {
                e.Graphics.DrawRectangle(pen, r);
            }
            using (pen = new Pen(orbDropDownDarkBorder))
            {
                e.Graphics.DrawRectangle(pen, rectangle4);
            }
            Rectangle rectangle8 = e.Ribbon.RectangleToScreen(e.Ribbon.OrbBounds);
            rectangle8 = e.RibbonOrbDropDown.RectangleToClient(rectangle8);
            this.DrawOrb(e.Graphics, rectangle8, e.Ribbon.OrbImage, e.Ribbon.OrbSelected, e.Ribbon.OrbPressed);
        }

        public override void OnRenderPanelPopupBackground(RibbonCanvasEventArgs e)
        {
            RibbonPanel relatedObject = e.RelatedObject as RibbonPanel;
            if (relatedObject != null)
            {
                Pen pen;
                SolidBrush brush;
                Rectangle r = Rectangle.FromLTRB(e.Bounds.Left, e.Bounds.Top, e.Bounds.Right, e.Bounds.Bottom);
                Rectangle rectangle2 = Rectangle.FromLTRB(e.Bounds.Left + 1, e.Bounds.Top + 1, e.Bounds.Right - 1, e.Bounds.Bottom - 1);
                Rectangle rectangle3 = Rectangle.FromLTRB(e.Bounds.Left + 1, relatedObject.ContentBounds.Bottom, e.Bounds.Right - 1, e.Bounds.Bottom - 1);
                GraphicsPath path = RoundRectangle(r, 3);
                GraphicsPath path2 = RoundRectangle(rectangle2, 3);
                GraphicsPath path3 = RoundRectangle(rectangle3, 3, Corners.South);
                using (pen = new Pen(this.ColorTable.PanelLightBorder))
                {
                    e.Graphics.DrawPath(pen, path2);
                }
                using (pen = new Pen(this.ColorTable.PanelDarkBorder))
                {
                    e.Graphics.DrawPath(pen, path);
                }
                using (brush = new SolidBrush(this.ColorTable.PanelBackgroundSelected))
                {
                    e.Graphics.FillPath(brush, path2);
                }
                using (brush = new SolidBrush(this.ColorTable.PanelTextBackground))
                {
                    e.Graphics.FillPath(brush, path3);
                }
                path3.Dispose();
                path.Dispose();
                path2.Dispose();
            }
        }

        public override void OnRenderRibbonBackground(RibbonRenderEventArgs e)
        {
            e.Graphics.Clear(this.ColorTable.RibbonBackground);
            if (e.Ribbon.ActualBorderMode == RibbonWindowMode.NonClientAreaGlass)
            {
                WinApi.FillForGlass(e.Graphics, new Rectangle(0, 0, e.Ribbon.Width, e.Ribbon.CaptionBarSize + 1));
            }
        }

        public override void OnRenderRibbonCaptionBar(RibbonRenderEventArgs e)
        {
            Rectangle r = new Rectangle(0, 0, e.Ribbon.Width, e.Ribbon.CaptionBarSize);
            if ((e.Ribbon.ActualBorderMode != RibbonWindowMode.NonClientAreaGlass) || (RibbonDesigner.Current != null))
            {
                this.DrawCaptionBarBackground(r, e.Graphics);
            }
            this.DrawCaptionBarText(e.Ribbon.CaptionTextBounds, e);
        }

        public override void OnRenderRibbonItem(RibbonItemRenderEventArgs e)
        {
            if (e.Item is RibbonButton)
            {
                RibbonButton item = e.Item as RibbonButton;
                if (item.Enabled)
                {
                    if (item.Style == RibbonButtonStyle.Normal)
                    {
                        if (item.Pressed && (item.SizeMode != RibbonElementSizeMode.DropDown))
                        {
                            this.DrawButtonPressed(e.Graphics, item);
                        }
                        else if (item.Selected)
                        {
                            if (item.Checked)
                            {
                                this.DrawButtonPressed(e.Graphics, item);
                            }
                            else
                            {
                                this.DrawButtonSelected(e.Graphics, item);
                            }
                        }
                        else if (item.Checked)
                        {
                            this.DrawButtonChecked(e.Graphics, item);
                        }
                        else if (item is RibbonOrbOptionButton)
                        {
                            this.DrawOrbOptionButton(e.Graphics, item.Bounds);
                        }
                    }
                    else if (item.DropDownPressed && (item.SizeMode != RibbonElementSizeMode.DropDown))
                    {
                        this.DrawButtonPressed(e.Graphics, item);
                        this.DrawSplitButtonDropDownSelected(e, item);
                    }
                    else if (item.Pressed && (item.SizeMode != RibbonElementSizeMode.DropDown))
                    {
                        this.DrawButtonPressed(e.Graphics, item);
                        this.DrawSplitButtonSelected(e, item);
                    }
                    else if (item.DropDownSelected)
                    {
                        this.DrawButtonSelected(e.Graphics, item);
                        this.DrawSplitButtonDropDownSelected(e, item);
                    }
                    else if (item.Selected)
                    {
                        this.DrawButtonSelected(e.Graphics, item);
                        this.DrawSplitButtonSelected(e, item);
                    }
                    else if (item.Checked)
                    {
                        this.DrawButtonChecked(e.Graphics, item);
                    }
                    else
                    {
                        this.DrawSplitButton(e, item);
                    }
                }
                if ((item.Style != RibbonButtonStyle.Normal) && ((item.Style != RibbonButtonStyle.DropDown) || (item.SizeMode != RibbonElementSizeMode.Large)))
                {
                    if (item.Style == RibbonButtonStyle.DropDown)
                    {
                        this.DrawButtonDropDownArrow(e.Graphics, item, item.OnGetDropDownBounds(item.SizeMode, item.Bounds));
                    }
                    else
                    {
                        this.DrawButtonDropDownArrow(e.Graphics, item, item.DropDownBounds);
                    }
                }
            }
            else if (e.Item is RibbonItemGroup)
            {
                this.DrawItemGroup(e, e.Item as RibbonItemGroup);
            }
            else if (e.Item is RibbonButtonList)
            {
                this.DrawButtonList(e.Graphics, e.Item as RibbonButtonList);
            }
            else if (e.Item is RibbonSeparator)
            {
                this.DrawSeparator(e.Graphics, e.Item as RibbonSeparator);
            }
            else if (e.Item is RibbonTextBox)
            {
                RibbonTextBox box = e.Item as RibbonTextBox;
                if (box.Enabled)
                {
                    if ((box != null) && (box.Selected || box.Editing))
                    {
                        this.DrawTextBoxSelected(e.Graphics, box.TextBoxBounds);
                    }
                    else
                    {
                        this.DrawTextBoxUnselected(e.Graphics, box.TextBoxBounds);
                    }
                }
                else
                {
                    this.DrawTextBoxDisabled(e.Graphics, box.TextBoxBounds);
                }
                if (box is RibbonComboBox)
                {
                    this.DrawComboxDropDown(e.Graphics, box as RibbonComboBox);
                }
            }
        }

        public override void OnRenderRibbonItemBorder(RibbonItemRenderEventArgs e)
        {
            if (e.Item is RibbonItemGroup)
            {
                this.DrawItemGroupBorder(e, e.Item as RibbonItemGroup);
            }
        }

        public override void OnRenderRibbonItemImage(RibbonItemBoundsEventArgs e)
        {
            Image normalImage = e.Item.Image;
            if ((e.Item is RibbonButton) && ((e.Item.SizeMode != RibbonElementSizeMode.Large) && (e.Item.SizeMode != RibbonElementSizeMode.Overflow)))
            {
                normalImage = (e.Item as RibbonButton).SmallImage;
            }
            if (normalImage != null)
            {
                if (!e.Item.Enabled)
                {
                    normalImage = RibbonRenderer.CreateDisabledImage(normalImage);
                }
                e.Graphics.DrawImage(normalImage, e.Bounds);
            }
        }

        public override void OnRenderRibbonItemText(RibbonTextEventArgs e)
        {
            SolidBrush brush;
            Color arrow = e.Color;
            StringFormat format = e.Format;
            Font prototype = e.Ribbon.Font;
            bool flag = false;
            if (e.Item is RibbonButton)
            {
                RibbonButton item = e.Item as RibbonButton;
                if (item is RibbonCaptionButton)
                {
                    if (WinApi.IsWindows)
                    {
                        prototype = new Font("Marlett", prototype.Size);
                    }
                    flag = true;
                    arrow = this.ColorTable.Arrow;
                }
                if ((item.Style == RibbonButtonStyle.DropDown) && (item.SizeMode == RibbonElementSizeMode.Large))
                {
                    this.DrawButtonDropDownArrow(e.Graphics, item, e.Bounds);
                }
            }
            else if (e.Item is RibbonSeparator)
            {
                arrow = this.GetTextColor(e.Item.Enabled);
            }
            if (flag || !e.Item.Enabled)
            {
                Rectangle bounds = e.Bounds;
                bounds.Y++;
                using (brush = new SolidBrush(this.ColorTable.ArrowLight))
                {
                    e.Graphics.DrawString(e.Text, new Font(prototype, e.Style), brush, bounds, format);
                }
            }
            if (arrow.Equals(Color.Empty))
            {
                arrow = this.GetTextColor(e.Item.Enabled);
            }
            using (brush = new SolidBrush(arrow))
            {
                e.Graphics.DrawString(e.Text, new Font(prototype, e.Style), brush, e.Bounds, format);
            }
        }

        public override void OnRenderRibbonOrb(RibbonRenderEventArgs e)
        {
            if (e.Ribbon.OrbVisible)
            {
                this.DrawOrb(e.Graphics, e.Ribbon.OrbBounds, e.Ribbon.OrbImage, e.Ribbon.OrbSelected, e.Ribbon.OrbPressed);
            }
        }

        public override void OnRenderRibbonPanelBackground(RibbonPanelRenderEventArgs e)
        {
            if (e.Panel.OverflowMode && !(e.Canvas is RibbonPanelPopup))
            {
                if (e.Panel.Pressed)
                {
                    this.DrawPanelOverflowPressed(e);
                }
                else if (e.Panel.Selected)
                {
                    this.DrawPannelOveflowSelected(e);
                }
                else
                {
                    this.DrawPanelOverflowNormal(e);
                }
            }
            else if (e.Panel.Selected)
            {
                this.DrawPanelSelected(e);
            }
            else
            {
                this.DrawPanelNormal(e);
            }
        }

        public override void OnRenderRibbonPanelText(RibbonPanelRenderEventArgs e)
        {
            if (!e.Panel.OverflowMode || (e.Canvas is RibbonPanelPopup))
            {
                Rectangle layoutRectangle = Rectangle.FromLTRB(e.Panel.Bounds.Left + 1, e.Panel.ContentBounds.Bottom, e.Panel.Bounds.Right - 1, e.Panel.Bounds.Bottom - 1);
                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;
                using (Brush brush = new SolidBrush(this.GetTextColor(e.Panel.Enabled, this.ColorTable.PanelText)))
                {
                    e.Graphics.DrawString(e.Panel.Text, e.Ribbon.Font, brush, layoutRectangle, format);
                }
            }
        }

        public override void OnRenderRibbonQuickAccessToolbarBackground(RibbonRenderEventArgs e)
        {
            Pen pen;
            GraphicsPath path;
            Rectangle bounds = e.Ribbon.QuickAcessToolbar.Bounds;
            Padding padding = e.Ribbon.QuickAcessToolbar.Padding;
            Padding margin = e.Ribbon.QuickAcessToolbar.Margin;
            Point a = new Point(bounds.Left - (e.Ribbon.OrbVisible ? margin.Left : 0), bounds.Top);
            Point b = new Point(bounds.Right + padding.Right, bounds.Top);
            Point c = new Point(bounds.Left, bounds.Bottom);
            Point d = new Point(b.X, c.Y);
            Point point5 = new Point(c.X - 2, (a.Y + (bounds.Height / 2)) - 1);
            bool flag = (e.Ribbon.ActualBorderMode == RibbonWindowMode.NonClientAreaGlass) && (RibbonDesigner.Current == null);
            if (!flag)
            {
                using (pen = new Pen(this.ColorTable.QuickAccessBorderLight, 3f))
                {
                    using (path = this.CreateQuickAccessPath(a, b, c, d, point5, bounds, 0, 0, e.Ribbon))
                    {
                        e.Graphics.DrawPath(pen, path);
                    }
                }
            }
            using (path = this.CreateQuickAccessPath(a, b, c, d, point5, bounds, 0, 0, e.Ribbon))
            {
                LinearGradientBrush brush;
                using (pen = new Pen(this.ColorTable.QuickAccessBorderDark))
                {
                    if (flag)
                    {
                        pen.Color = Color.FromArgb(150, 150, 150);
                    }
                    e.Graphics.DrawPath(pen, path);
                }
                if (!flag)
                {
                    using (brush = new LinearGradientBrush(b, d, Color.FromArgb(150, this.ColorTable.QuickAccessUpper), Color.FromArgb(150, this.ColorTable.QuickAccessLower)))
                    {
                        e.Graphics.FillPath(brush, path);
                    }
                }
                else
                {
                    using (brush = new LinearGradientBrush(b, d, Color.FromArgb(0x42, RibbonProfesionalRendererColorTable.ToGray(this.ColorTable.QuickAccessUpper)), Color.FromArgb(0x42, RibbonProfesionalRendererColorTable.ToGray(this.ColorTable.QuickAccessLower))))
                    {
                        e.Graphics.FillPath(brush, path);
                    }
                }
            }
        }

        public override void OnRenderRibbonTab(RibbonTabRenderEventArgs e)
        {
            if (e.Tab.Active)
            {
                this.DrawCompleteTab(e);
            }
            else if (e.Tab.Pressed)
            {
                this.DrawTabPressed(e);
            }
            else if (e.Tab.Selected)
            {
                this.DrawTabSelected(e);
            }
            else
            {
                this.DrawTabNormal(e);
            }
        }

        public override void OnRenderRibbonTabText(RibbonTabRenderEventArgs e)
        {
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            format.Trimming = StringTrimming.EllipsisCharacter;
            format.LineAlignment = StringAlignment.Center;
            format.FormatFlags |= StringFormatFlags.NoWrap;
            Rectangle layoutRectangle = Rectangle.FromLTRB(e.Tab.TabBounds.Left + e.Ribbon.TabTextMargin.Left, e.Tab.TabBounds.Top + e.Ribbon.TabTextMargin.Top, e.Tab.TabBounds.Right - e.Ribbon.TabTextMargin.Right, e.Tab.TabBounds.Bottom - e.Ribbon.TabTextMargin.Bottom);
            using (Brush brush = new SolidBrush(this.GetTextColor(true, e.Tab.Active ? this.ColorTable.TabActiveText : this.ColorTable.TabText)))
            {
                e.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                e.Graphics.DrawString(e.Tab.Text, e.Ribbon.Font, brush, layoutRectangle, format);
            }
        }

        public override void OnRenderTabScrollButtons(RibbonTabRenderEventArgs e)
        {
            if (e.Tab.ScrollLeftVisible)
            {
                if (e.Tab.ScrollLeftSelected)
                {
                    this.DrawButtonSelected(e.Graphics, e.Tab.ScrollLeftBounds, Corners.West);
                }
                else
                {
                    this.DrawButton(e.Graphics, e.Tab.ScrollLeftBounds, Corners.West);
                }
                this.DrawArrowShaded(e.Graphics, e.Tab.ScrollLeftBounds, RibbonArrowDirection.Right, true);
            }
            if (e.Tab.ScrollRightVisible)
            {
                if (e.Tab.ScrollRightSelected)
                {
                    this.DrawButtonSelected(e.Graphics, e.Tab.ScrollRightBounds, Corners.East);
                }
                else
                {
                    this.DrawButton(e.Graphics, e.Tab.ScrollRightBounds, Corners.East);
                }
                this.DrawArrowShaded(e.Graphics, e.Tab.ScrollRightBounds, RibbonArrowDirection.Left, true);
            }
        }

        public static GraphicsPath RoundRectangle(Rectangle r, int radius)
        {
            return RoundRectangle(r, radius, Corners.All);
        }

        public static GraphicsPath RoundRectangle(Rectangle r, int radius, Corners corners)
        {
            GraphicsPath path = new GraphicsPath();
            int num = radius * 2;
            int num2 = ((corners & Corners.NorthWest) == Corners.NorthWest) ? num : 0;
            int num3 = ((corners & Corners.NorthEast) == Corners.NorthEast) ? num : 0;
            int num4 = ((corners & Corners.SouthEast) == Corners.SouthEast) ? num : 0;
            int num5 = ((corners & Corners.SouthWest) == Corners.SouthWest) ? num : 0;
            path.AddLine(r.Left + num2, r.Top, r.Right - num3, r.Top);
            if (num3 > 0)
            {
                path.AddArc(Rectangle.FromLTRB(r.Right - num3, r.Top, r.Right, r.Top + num3), -90f, 90f);
            }
            path.AddLine(r.Right, r.Top + num3, r.Right, r.Bottom - num4);
            if (num4 > 0)
            {
                path.AddArc(Rectangle.FromLTRB(r.Right - num4, r.Bottom - num4, r.Right, r.Bottom), 0f, 90f);
            }
            path.AddLine(r.Right - num4, r.Bottom, r.Left + num5, r.Bottom);
            if (num5 > 0)
            {
                path.AddArc(Rectangle.FromLTRB(r.Left, r.Bottom - num5, r.Left + num5, r.Bottom), 90f, 90f);
            }
            path.AddLine(r.Left, r.Bottom - num5, r.Left, r.Top + num2);
            if (num2 > 0)
            {
                path.AddArc(Rectangle.FromLTRB(r.Left, r.Top, r.Left + num2, r.Top + num2), 180f, 90f);
            }
            path.CloseFigure();
            return path;
        }

        public RibbonProfesionalRendererColorTable ColorTable
        {
            get
            {
                return this._colorTable;
            }
            set
            {
                this._colorTable = value;
            }
        }

        public enum Corners
        {
            All = 30,
            East = 12,
            None = 0,
            North = 6,
            NorthEast = 4,
            NorthWest = 2,
            South = 0x18,
            SouthEast = 8,
            SouthWest = 0x10,
            West = 0x12
        }
    }
}

