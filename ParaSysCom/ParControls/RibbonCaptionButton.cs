﻿using ParControls.RibbonHelpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    public partial class RibbonCaptionButton : RibbonButton
    {
        private CaptionButton _captionButtonType;

        public RibbonCaptionButton(CaptionButton buttonType)
        {
            this.SetCaptionButtonType(buttonType);
        }

        public static string GetCharFor(CaptionButton type)
        {
            if (WinApi.IsWindows)
            {
                switch (type)
                {
                    case CaptionButton.Minimize:
                        return "0";

                    case CaptionButton.Maximize:
                        return "1";

                    case CaptionButton.Restore:
                        return "2";

                    case CaptionButton.Close:
                        return "r";
                }
                return "?";
            }
            switch (type)
            {
                case CaptionButton.Minimize:
                    return "_";

                case CaptionButton.Maximize:
                    return "+";

                case CaptionButton.Restore:
                    return "^";

                case CaptionButton.Close:
                    return "X";
            }
            return "?";
        }

        public override void OnClick(EventArgs e)
        {
            base.OnClick(e);
            Form form = base.Owner.FindForm();
            if (form != null)
            {
                switch (this.CaptionButtonType)
                {
                    case CaptionButton.Minimize:
                        form.WindowState = FormWindowState.Minimized;
                        break;

                    case CaptionButton.Maximize:
                        form.WindowState = FormWindowState.Maximized;
                        break;

                    case CaptionButton.Restore:
                        form.WindowState = FormWindowState.Normal;
                        break;

                    case CaptionButton.Close:
                        form.Close();
                        break;
                }
            }
        }

        internal override Rectangle OnGetTextBounds(RibbonElementSizeMode sMode, Rectangle bounds)
        {
            Rectangle rectangle = bounds;
            rectangle.X = bounds.Left + 3;
            return rectangle;
        }

        internal void SetCaptionButtonType(CaptionButton buttonType)
        {
            this.Text = GetCharFor(buttonType);
            this._captionButtonType = buttonType;
        }

        public CaptionButton CaptionButtonType
        {
            get
            {
                return this._captionButtonType;
            }
        }

        public enum CaptionButton
        {
            Minimize,
            Maximize,
            Restore,
            Close
        }
    }
}
