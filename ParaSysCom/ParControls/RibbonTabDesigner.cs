﻿namespace ParControls
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.Design;
    using System.Windows.Forms.Design.Behavior;

    public class RibbonTabDesigner : ComponentDesigner
    {
        private Adorner panelAdorner;

        public void AddPanel(object sender, EventArgs e)
        {
            IDesignerHost service = this.GetService(typeof(IDesignerHost)) as IDesignerHost;
            if ((service != null) && (this.Tab != null))
            {
                DesignerTransaction transaction = service.CreateTransaction("AddPanel" + base.Component.Site.Name);
                MemberDescriptor member = TypeDescriptor.GetProperties(base.Component)["Panels"];
                base.RaiseComponentChanging(member);
                RibbonPanel item = service.CreateComponent(typeof(RibbonPanel)) as RibbonPanel;
                if (item != null)
                {
                    item.Text = item.Site.Name;
                    this.Tab.Panels.Add(item);
                    this.Tab.Owner.OnRegionsChanged();
                }
                base.RaiseComponentChanged(member, null, null);
                transaction.Commit();
            }
        }

        public override void Initialize(IComponent component)
        {
            base.Initialize(component);
            this.panelAdorner = new Adorner();
            BehaviorService behaviorService = RibbonDesigner.Current.GetBehaviorService();
            if (behaviorService != null)
            {
                behaviorService.Adorners.AddRange(new Adorner[] { this.panelAdorner });
                this.panelAdorner.Glyphs.Add(new RibbonPanelGlyph(behaviorService, this, this.Tab));
            }
        }

        public RibbonTab Tab
        {
            get
            {
                return (base.Component as RibbonTab);
            }
        }

        public override DesignerVerbCollection Verbs
        {
            get
            {
                return new DesignerVerbCollection(new DesignerVerb[] { new DesignerVerb("Add Panel", new EventHandler(this.AddPanel)) });
            }
        }
    }
}

