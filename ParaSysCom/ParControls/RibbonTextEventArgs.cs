﻿namespace ParControls
{
    using System;
    using System.Drawing;

    public class RibbonTextEventArgs : RibbonItemBoundsEventArgs
    {
        private System.Drawing.Color _color;
        private StringFormat _format;
        private FontStyle _style;
        private string _text;

        public RibbonTextEventArgs(Ribbon owner, Graphics g, Rectangle clip, RibbonItem item, Rectangle bounds, string text) : base(owner, g, clip, item, bounds)
        {
            this.Text = text;
            this.Style = FontStyle.Regular;
            this.Format = new StringFormat();
            this.Color = System.Drawing.Color.Empty;
        }

        public RibbonTextEventArgs(Ribbon owner, Graphics g, Rectangle clip, RibbonItem item, Rectangle bounds, string text, FontStyle style) : base(owner, g, clip, item, bounds)
        {
            this.Text = text;
            this.Style = style;
            this.Format = new StringFormat();
            this.Color = System.Drawing.Color.Empty;
        }

        public RibbonTextEventArgs(Ribbon owner, Graphics g, Rectangle clip, RibbonItem item, Rectangle bounds, string text, StringFormat format) : base(owner, g, clip, item, bounds)
        {
            this.Text = text;
            this.Style = FontStyle.Regular;
            this.Format = format;
            this.Color = System.Drawing.Color.Empty;
        }

        public RibbonTextEventArgs(Ribbon owner, Graphics g, Rectangle clip, RibbonItem item, Rectangle bounds, string text, System.Drawing.Color color, FontStyle style, StringFormat format) : base(owner, g, clip, item, bounds)
        {
            this.Text = text;
            this.Style = style;
            this.Format = format;
            this.Color = color;
        }

        public System.Drawing.Color Color
        {
            get
            {
                return this._color;
            }
            set
            {
                this._color = value;
            }
        }

        public StringFormat Format
        {
            get
            {
                return this._format;
            }
            set
            {
                this._format = value;
            }
        }

        public FontStyle Style
        {
            get
            {
                return this._style;
            }
            set
            {
                this._style = value;
            }
        }

        public string Text
        {
            get
            {
                return this._text;
            }
            set
            {
                this._text = value;
            }
        }
    }
}

