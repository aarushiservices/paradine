﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace ParControls
{
    [Designer(typeof(RibbonButtonListDesigner))]
    public partial class RibbonButtonList : RibbonItem, IContainsSelectableRibbonItems, IScrollableRibbonItem, IContainsRibbonComponents
    {
        private bool _avoidNextThumbMeasure;
        private Rectangle _buttonDownBounds;
        private bool _buttonDownEnabled;
        private bool _buttonDownPressed;
        private bool _buttonDownSelected;
        private Rectangle _buttonDropDownBounds;
        private bool _buttonDropDownPressed;
        private bool _buttonDropDownSelected;
        private RibbonButtonCollection _buttons;
        private RibbonElementSizeMode _buttonsSizeMode;
        private Rectangle _buttonUpBounds;
        private bool _buttonUpEnabled;
        private bool _buttonUpPressed;
        private bool _buttonUpSelected;
        private Rectangle _contentBounds;
        private int _controlButtonsWidth;
        private RibbonDropDown _dropDown;
        private RibbonItemCollection _dropDownItems;
        private bool _dropDownVisible;
        private bool _flowToBottom;
        private Size _ItemsInDropwDownMode;
        private int _itemsInLargeMode;
        private int _itemsInMediumMode;
        private int _jumpDownSize;
        private int _jumpUpSize;
        private int _offset;
        private bool _scrollBarEnabled;
        private ListScrollType _scrollType;
        private int _scrollValue;
        private Rectangle _thumbBounds;
        private int _thumbOffset;
        private bool _thumbPressed;
        private bool _thumbSelected;
        private Rectangle fullContentBounds;

        public RibbonButtonList()
        {
            this._buttons = new RibbonButtonCollection(this);
            this._dropDownItems = new RibbonItemCollection();
            this._controlButtonsWidth = 0x10;
            this._itemsInLargeMode = 7;
            this._itemsInMediumMode = 3;
            this._ItemsInDropwDownMode = new Size(7, 5);
            this._buttonsSizeMode = RibbonElementSizeMode.Large;
            this._scrollType = ListScrollType.UpDownButtons;
        }

        public RibbonButtonList(IEnumerable<RibbonButton> buttons)
            : this(buttons, null)
        {
        }

        public RibbonButtonList(IEnumerable<RibbonButton> buttons, IEnumerable<RibbonItem> dropDownItems)
            : this()
        {
            if (buttons != null)
            {
                this._buttons.AddRange(new List<RibbonButton>(buttons).ToArray());
            }
            if (dropDownItems != null)
            {
                this._dropDownItems.AddRange(dropDownItems);
            }
        }

        public void CloseDropDown()
        {
            if (this._dropDown != null)
            {
            }
            this.SetDropDownVisible(false);
        }

        protected override bool ClosesDropDownAt(Point p)
        {
            return (((!this.ButtonDropDownBounds.Contains(p) && !this.ButtonDownBounds.Contains(p)) && !this.ButtonUpBounds.Contains(p)) && ((this.ScrollType != ListScrollType.Scrollbar) || !this.ScrollBarBounds.Contains(p)));
        }

        private void dropDown_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.SetDropDownVisible(false);
        }

        public IEnumerable<Component> GetAllChildComponents()
        {
            List<Component> list = new List<Component>(this.Buttons.ToArray());
            list.AddRange(this.DropDownItems.ToArray());
            return list;
        }

        public Rectangle GetContentBounds()
        {
            return this.ContentBounds;
        }

        public IEnumerable<RibbonItem> GetItems()
        {
            return this.Buttons;
        }

        private void IgnoreDeactivation()
        {
            if (base.Canvas is RibbonPanelPopup)
            {
                (base.Canvas as RibbonPanelPopup).IgnoreNextClickDeactivation();
            }
            if (base.Canvas is RibbonDropDown)
            {
                (base.Canvas as RibbonDropDown).IgnoreNextClickDeactivation();
            }
        }

        public override Size MeasureSize(object sender, RibbonElementMeasureSizeEventArgs e)
        {
            int itemsWideInMediumMode = 0;
            switch (e.SizeMode)
            {
                case RibbonElementSizeMode.Compact:
                    itemsWideInMediumMode = 0;
                    break;

                case RibbonElementSizeMode.Medium:
                    itemsWideInMediumMode = this.ItemsWideInMediumMode;
                    break;

                case RibbonElementSizeMode.Large:
                    itemsWideInMediumMode = this.ItemsWideInLargeMode;
                    break;

                case RibbonElementSizeMode.DropDown:
                    itemsWideInMediumMode = this.ItemsSizeInDropwDownMode.Width;
                    break;
            }
            int height = (base.OwnerPanel.ContentBounds.Height - base.Owner.ItemPadding.Vertical) - 4;
            int num3 = 0;
            int num4 = 1;
            int num5 = 0;
            int num6 = 0;
            bool flag = true;
            foreach (RibbonButton button in this.Buttons)
            {
                Size size = button.MeasureSize(this, new RibbonElementMeasureSizeEventArgs(e.Graphics, this.ButtonsSizeMode));
                if (flag)
                {
                    num4 += size.Width + 1;
                }
                num5 = button.LastMeasuredSize.Height;
                num6 += num5;
                if (++num3 == itemsWideInMediumMode)
                {
                    flag = false;
                }
            }
            if (e.SizeMode == RibbonElementSizeMode.DropDown)
            {
                height = num5 * this.ItemsSizeInDropwDownMode.Height;
            }
            if (ScrollBarRenderer.IsSupported)
            {
                this._thumbBounds = new Rectangle(Point.Empty, ScrollBarRenderer.GetSizeBoxSize(e.Graphics, ScrollBarState.Normal));
            }
            else
            {
                this._thumbBounds = new Rectangle(Point.Empty, new Size(0x10, 0x10));
            }
            base.SetLastMeasuredSize(new Size(num4 + this.ControlButtonsWidth, height));
            return base.LastMeasuredSize;
        }

        public override void OnCanvasChanged(EventArgs e)
        {
            base.OnCanvasChanged(e);
            if (base.Canvas is RibbonDropDown)
            {
                this._scrollType = ListScrollType.Scrollbar;
            }
            else
            {
                this._scrollType = ListScrollType.UpDownButtons;
            }
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if ((this.ButtonDownSelected || this.ButtonUpSelected) || this.ButtonDropDownSelected)
            {
                this.IgnoreDeactivation();
            }
            if (this.ButtonDownSelected && this.ButtonDownEnabled)
            {
                this._buttonDownPressed = true;
                this.ScrollDown();
            }
            if (this.ButtonUpSelected && this.ButtonUpEnabled)
            {
                this._buttonUpPressed = true;
                this.ScrollUp();
            }
            if (this.ButtonDropDownSelected)
            {
                this._buttonDropDownPressed = true;
                this.ShowDropDown();
            }
            if (this.ThumbSelected)
            {
                this._thumbPressed = true;
                this._thumbOffset = e.Y - this._thumbBounds.Y;
            }
            if (((((this.ScrollType == ListScrollType.Scrollbar) && this.ScrollBarBounds.Contains(e.Location)) && ((e.Y >= this.ButtonUpBounds.Bottom) && (e.Y <= this.ButtonDownBounds.Y))) && (!this.ThumbBounds.Contains(e.Location) && !this.ButtonDownBounds.Contains(e.Location))) && !this.ButtonUpBounds.Contains(e.Location))
            {
                if (e.Y < this.ThumbBounds.Y)
                {
                    this.ScrollUp();
                }
                else
                {
                    this.ScrollDown();
                }
            }
        }

        public override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            bool flag = (this._buttonUpSelected || this._buttonDownSelected) || this._buttonDropDownSelected;
            this._buttonUpSelected = false;
            this._buttonDownSelected = false;
            this._buttonDropDownSelected = false;
            if (flag)
            {
                this.RedrawControlButtons();
            }
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if ((this.ButtonDownPressed && this.ButtonDownSelected) && this.ButtonDownEnabled)
            {
                this.ScrollOffset(-1);
            }
            if ((this.ButtonUpPressed && this.ButtonUpSelected) && this.ButtonUpEnabled)
            {
                this.ScrollOffset(1);
            }
            bool flag = this._buttonUpSelected;
            bool flag2 = this._buttonDownSelected;
            bool flag3 = this._buttonDropDownSelected;
            bool flag4 = this._thumbSelected;
            this._buttonUpSelected = this._buttonUpBounds.Contains(e.Location);
            this._buttonDownSelected = this._buttonDownBounds.Contains(e.Location);
            this._buttonDropDownSelected = this._buttonDropDownBounds.Contains(e.Location);
            this._thumbSelected = (this._thumbBounds.Contains(e.Location) && (this.ScrollType == ListScrollType.Scrollbar)) && this.ScrollBarEnabled;
            if ((((flag != this._buttonUpSelected) || (flag2 != this._buttonDownSelected)) || (flag3 != this._buttonDropDownSelected)) || (flag4 != this._thumbSelected))
            {
                this.RedrawControlButtons();
            }
            if (this.ThumbPressed)
            {
                int scrollMinimum = e.Y - this._thumbOffset;
                if (scrollMinimum < this.ScrollMinimum)
                {
                    scrollMinimum = this.ScrollMinimum;
                }
                else if (scrollMinimum > this.ScrollMaximum)
                {
                    scrollMinimum = this.ScrollMaximum;
                }
                this.ScrollValue = scrollMinimum;
                this.RedrawScroll();
            }
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            this._buttonDownPressed = false;
            this._buttonUpPressed = false;
            this._buttonDropDownPressed = false;
            this._thumbPressed = false;
        }

        public override void OnPaint(object sender, RibbonElementPaintEventArgs e)
        {
            base.Owner.Renderer.OnRenderRibbonItem(new RibbonItemRenderEventArgs(base.Owner, e.Graphics, e.Clip, this));
            if (e.Mode != RibbonElementSizeMode.Compact)
            {
                Region clip = e.Graphics.Clip;
                Region region2 = new Region(clip.GetBounds(e.Graphics));
                region2.Intersect(this.ContentBounds);
                e.Graphics.SetClip(region2.GetBounds(e.Graphics));
                foreach (RibbonButton button in this.Buttons)
                {
                    if (!button.Bounds.IsEmpty)
                    {
                        button.OnPaint(this, new RibbonElementPaintEventArgs(button.Bounds, e.Graphics, this.ButtonsSizeMode));
                    }
                }
                e.Graphics.SetClip(clip.GetBounds(e.Graphics));
            }
        }

        private void RedrawControlButtons()
        {
            if (base.Canvas != null)
            {
                if (this.ScrollType == ListScrollType.Scrollbar)
                {
                    base.Canvas.Invalidate(this.ScrollBarBounds);
                }
                else
                {
                    base.Canvas.Invalidate(Rectangle.FromLTRB(this.ButtonUpBounds.Left, this.ButtonUpBounds.Top, this.ButtonDropDownBounds.Right, this.ButtonDropDownBounds.Bottom));
                }
            }
        }

        private void RedrawScroll()
        {
            if (base.Canvas != null)
            {
                base.Canvas.Invalidate(Rectangle.FromLTRB(this.ButtonDownBounds.X, this.ButtonUpBounds.Y, this.ButtonDownBounds.Right, this.ButtonDownBounds.Bottom));
            }
        }

        public void ScrollDown()
        {
            this.ScrollOffset(-(this._jumpDownSize + 1));
        }

        private void ScrollOffset(int amount)
        {
            this.ScrollTo(this._offset + amount);
        }

        private void ScrollTo(int offset)
        {
            int num = (this.ContentBounds.Y - this.fullContentBounds.Height) + this.ContentBounds.Height;
            if (offset < num)
            {
                offset = num;
            }
            this._offset = offset;
            this.SetBounds(base.Bounds);
            this.RedrawItem();
        }

        public void ScrollUp()
        {
            this.ScrollOffset(this._jumpDownSize + 1);
        }

        public override void SetBounds(Rectangle bounds)
        {
            base.SetBounds(bounds);
            if (this.ScrollType != ListScrollType.Scrollbar)
            {
                int num = 3;
                int num2 = bounds.Height / num;
                int num3 = this._controlButtonsWidth;
                this._buttonUpBounds = Rectangle.FromLTRB(bounds.Right - num3, bounds.Top, bounds.Right, bounds.Top + num2);
                this._buttonDownBounds = Rectangle.FromLTRB(this._buttonUpBounds.Left, this._buttonUpBounds.Bottom, bounds.Right, this._buttonUpBounds.Bottom + num2);
                if (num == 2)
                {
                    this._buttonDropDownBounds = Rectangle.Empty;
                }
                else
                {
                    this._buttonDropDownBounds = Rectangle.FromLTRB(this._buttonDownBounds.Left, this._buttonDownBounds.Bottom, bounds.Right, bounds.Bottom + 1);
                }
                this._thumbBounds.Location = Point.Empty;
            }
            else
            {
                int width = this.ThumbBounds.Width;
                int num5 = this.ThumbBounds.Width;
                this._buttonUpBounds = Rectangle.FromLTRB(bounds.Right - width, bounds.Top, bounds.Right, bounds.Top + num5);
                this._buttonDownBounds = Rectangle.FromLTRB(this._buttonUpBounds.Left, bounds.Height - num5, bounds.Right, bounds.Height);
                this._buttonDropDownBounds = Rectangle.Empty;
                this._thumbBounds.X = this._buttonUpBounds.Left;
            }
            this._contentBounds = Rectangle.FromLTRB(bounds.Left, bounds.Top, this._buttonUpBounds.Left, bounds.Bottom);
            this._buttonUpEnabled = this._offset < 0;
            if (!this._buttonUpEnabled)
            {
                this._offset = 0;
            }
            this._buttonDownEnabled = false;
            int x = this.ContentBounds.Left + 1;
            int y = (this.ContentBounds.Top + 1) + this._offset;
            int num8 = y;
            int top = y;
            foreach (RibbonItem item in this.Buttons)
            {
                item.SetBounds(Rectangle.Empty);
            }
            for (int i = 0; i < this.Buttons.Count; i++)
            {
                RibbonButton button = this.Buttons[i] as RibbonButton;
                if (button == null)
                {
                    break;
                }
                if ((x + button.LastMeasuredSize.Width) > this.ContentBounds.Right)
                {
                    x = this.ContentBounds.Left + 1;
                    y = num8 + 1;
                }
                button.SetBounds(new Rectangle(x, y, button.LastMeasuredSize.Width, button.LastMeasuredSize.Height));
                x = button.Bounds.Right + 1;
                num8 = Math.Max(num8, button.Bounds.Bottom);
                if (button.Bounds.Bottom > this.ContentBounds.Bottom)
                {
                    this._buttonDownEnabled = true;
                }
                this._jumpDownSize = button.Bounds.Height;
                this._jumpUpSize = button.Bounds.Height;
            }
            double num11 = num8 - top;
            double height = bounds.Height;
            if ((num11 > height) && (num11 != 0.0))
            {
                double num13 = (num11 > height) ? (height / num11) : 0.0;
                double num14 = this.ButtonDownBounds.Top - this.ButtonUpBounds.Bottom;
                double num15 = Math.Ceiling((double)(num13 * num14));
                if (num15 < 30.0)
                {
                    if (num14 >= 30.0)
                    {
                        num15 = 30.0;
                    }
                    else
                    {
                        num15 = num14;
                    }
                }
                this._thumbBounds.Height = Convert.ToInt32(num15);
                this.fullContentBounds = Rectangle.FromLTRB(this.ContentBounds.Left, top, this.ContentBounds.Right, num8);
                this._scrollBarEnabled = true;
                this.UpdateThumbPos();
            }
            else
            {
                this._scrollBarEnabled = false;
            }
        }

        internal void SetDropDownVisible(bool visible)
        {
            this._dropDownVisible = visible;
        }

        internal override void SetOwner(Ribbon owner)
        {
            base.SetOwner(owner);
            this._buttons.SetOwner(owner);
            this._dropDownItems.SetOwner(owner);
        }

        internal override void SetOwnerPanel(RibbonPanel ownerPanel)
        {
            base.SetOwnerPanel(ownerPanel);
            this._buttons.SetOwnerPanel(ownerPanel);
            this._dropDownItems.SetOwnerPanel(ownerPanel);
        }

        internal override void SetOwnerTab(RibbonTab ownerTab)
        {
            base.SetOwnerTab(ownerTab);
            this._buttons.SetOwnerTab(ownerTab);
            this._dropDownItems.SetOwnerTab(base.OwnerTab);
        }

        internal override void SetSizeMode(RibbonElementSizeMode sizeMode)
        {
            base.SetSizeMode(sizeMode);
            foreach (RibbonItem item in this.Buttons)
            {
                item.SetSizeMode(this.ButtonsSizeMode);
            }
        }

        public void ShowDropDown()
        {
            if (this.DropDownItems.Count == 0)
            {
                this.SetPressed(false);
            }
            else
            {
                this.IgnoreDeactivation();
                this._dropDown = new RibbonDropDown(this, this.DropDownItems, base.Owner);
                this._dropDown.ShowSizingGrip = true;
                Point screenLocation = base.Canvas.PointToScreen(new Point(base.Bounds.Left, base.Bounds.Top));
                this.SetDropDownVisible(true);
                this._dropDown.Show(screenLocation);
            }
        }

        private void UpdateThumbPos()
        {
            if (this._avoidNextThumbMeasure)
            {
                this._avoidNextThumbMeasure = false;
            }
            else
            {
                if (!double.IsInfinity(this.ScrolledPercent))
                {
                    double num2 = this.ScrollMaximum - this.ScrollMinimum;
                    double num3 = Math.Ceiling((double)(num2 * this.ScrolledPercent));
                    this._thumbBounds.Y = this.ScrollMinimum + Convert.ToInt32(num3);
                }
                else
                {
                    this._thumbBounds.Y = this.ScrollMinimum;
                }
                if (this._thumbBounds.Y > this.ScrollMaximum)
                {
                    this._thumbBounds.Y = this.ScrollMaximum;
                }
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Rectangle ButtonDownBounds
        {
            get
            {
                return this._buttonDownBounds;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public bool ButtonDownEnabled
        {
            get
            {
                return this._buttonDownEnabled;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ButtonDownPressed
        {
            get
            {
                return this._buttonDownPressed;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public bool ButtonDownSelected
        {
            get
            {
                return this._buttonDownSelected;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Rectangle ButtonDropDownBounds
        {
            get
            {
                return this._buttonDropDownBounds;
            }
        }

        public bool ButtonDropDownPresent
        {
            get
            {
                return (this.ButtonDropDownBounds.Height > 0);
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ButtonDropDownPressed
        {
            get
            {
                return this._buttonDropDownPressed;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ButtonDropDownSelected
        {
            get
            {
                return this._buttonDropDownSelected;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public RibbonButtonCollection Buttons
        {
            get
            {
                return this._buttons;
            }
        }

        public RibbonElementSizeMode ButtonsSizeMode
        {
            get
            {
                return this._buttonsSizeMode;
            }
            set
            {
                this._buttonsSizeMode = value;
                if (base.Owner != null)
                {
                    base.Owner.OnRegionsChanged();
                }
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public Rectangle ButtonUpBounds
        {
            get
            {
                return this._buttonUpBounds;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public bool ButtonUpEnabled
        {
            get
            {
                return this._buttonUpEnabled;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ButtonUpPressed
        {
            get
            {
                return this._buttonUpPressed;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ButtonUpSelected
        {
            get
            {
                return this._buttonUpSelected;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override Rectangle ContentBounds
        {
            get
            {
                return this._contentBounds;
            }
        }

        [DefaultValue(0x10)]
        public int ControlButtonsWidth
        {
            get
            {
                return this._controlButtonsWidth;
            }
            set
            {
                this._controlButtonsWidth = value;
                if (base.Owner != null)
                {
                    base.Owner.OnRegionsChanged();
                }
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public RibbonItemCollection DropDownItems
        {
            get
            {
                return this._dropDownItems;
            }
        }

        [Description("If activated, buttons will flow to bottom inside the list")]
        public bool FlowToBottom
        {
            get
            {
                return this._flowToBottom;
            }
            set
            {
                this._flowToBottom = value;
            }
        }

        public Size ItemsSizeInDropwDownMode
        {
            get
            {
                return this._ItemsInDropwDownMode;
            }
            set
            {
                this._ItemsInDropwDownMode = value;
                if (base.Owner != null)
                {
                    base.Owner.OnRegionsChanged();
                }
            }
        }

        [DefaultValue(7)]
        public int ItemsWideInLargeMode
        {
            get
            {
                return this._itemsInLargeMode;
            }
            set
            {
                this._itemsInLargeMode = value;
                if (base.Owner != null)
                {
                    base.Owner.OnRegionsChanged();
                }
            }
        }

        [DefaultValue(3)]
        public int ItemsWideInMediumMode
        {
            get
            {
                return this._itemsInMediumMode;
            }
            set
            {
                this._itemsInMediumMode = value;
                if (base.Owner != null)
                {
                    base.Owner.OnRegionsChanged();
                }
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public Rectangle ScrollBarBounds
        {
            get
            {
                return Rectangle.FromLTRB(this.ButtonUpBounds.Left, this.ButtonUpBounds.Top, this.ButtonDownBounds.Right, this.ButtonDownBounds.Bottom);
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public bool ScrollBarEnabled
        {
            get
            {
                return this._scrollBarEnabled;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public double ScrolledPercent
        {
            get
            {
                return ((this.ContentBounds.Top - this.fullContentBounds.Top) / (this.fullContentBounds.Height - this.ContentBounds.Height));
            }
            set
            {
                this._avoidNextThumbMeasure = true;
                this.ScrollTo(-Convert.ToInt32((double)((this.fullContentBounds.Height - this.ContentBounds.Height) * value)));
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int ScrollMaximum
        {
            get
            {
                if (this.ScrollType == ListScrollType.Scrollbar)
                {
                    return (this.ButtonDownBounds.Top - this.ThumbBounds.Height);
                }
                return 0;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int ScrollMinimum
        {
            get
            {
                if (this.ScrollType == ListScrollType.Scrollbar)
                {
                    return this.ButtonUpBounds.Bottom;
                }
                return 0;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public ListScrollType ScrollType
        {
            get
            {
                return this._scrollType;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public int ScrollValue
        {
            get
            {
                return this._scrollValue;
            }
            set
            {
                if ((value > this.ScrollMaximum) || (value < this.ScrollMinimum))
                {
                    throw new IndexOutOfRangeException("Scroll value must exist between ScrollMinimum and Scroll Maximum");
                }
                this._thumbBounds.Y = value;
                double num = value - this.ScrollMinimum;
                double num2 = this.ScrollMaximum - this.ScrollMinimum;
                this.ScrolledPercent = num / num2;
                this._scrollValue = value;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Rectangle ThumbBounds
        {
            get
            {
                return this._thumbBounds;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public bool ThumbPressed
        {
            get
            {
                return this._thumbPressed;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ThumbSelected
        {
            get
            {
                return this._thumbSelected;
            }
        }

        public enum ListScrollType
        {
            UpDownButtons,
            Scrollbar
        }

    }
}
