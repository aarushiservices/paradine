﻿namespace ParControls
{
    internal class RibbonPanelDesigner : RibbonElementWithItemCollectionDesigner
    {
        public override RibbonItemCollection Collection
        {
            get
            {
                if (base.Component is RibbonPanel)
                {
                    return (base.Component as RibbonPanel).Items;
                }
                return null;
            }
        }

        public override ParControls.Ribbon Ribbon
        {
            get
            {
                if (base.Component is RibbonPanel)
                {
                    return (base.Component as RibbonPanel).Owner;
                }
                return null;
            }
        }
    }
}

