﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    public partial class RibbonSeparator : RibbonItem
    {
        public RibbonSeparator()
        {
        }

        public RibbonSeparator(string text)
        {
            this.Text = text;
        }

        public override Size MeasureSize(object sender, RibbonElementMeasureSizeEventArgs e)
        {
            if (e.SizeMode == RibbonElementSizeMode.DropDown)
            {
                if (string.IsNullOrEmpty(this.Text))
                {
                    base.SetLastMeasuredSize(new Size(1, 3));
                }
                else
                {
                    Size size = e.Graphics.MeasureString(this.Text, new Font(base.Owner.Font, FontStyle.Bold)).ToSize();
                    base.SetLastMeasuredSize(new Size(size.Width + base.Owner.ItemMargin.Horizontal, size.Height + base.Owner.ItemMargin.Vertical));
                }
            }
            else
            {
                base.SetLastMeasuredSize(new Size(2, (base.OwnerPanel.ContentBounds.Height - base.Owner.ItemPadding.Vertical) - base.Owner.ItemMargin.Vertical));
            }
            return base.LastMeasuredSize;
        }

        public override void OnPaint(object sender, RibbonElementPaintEventArgs e)
        {
            base.Owner.Renderer.OnRenderRibbonItem(new RibbonItemRenderEventArgs(base.Owner, e.Graphics, e.Clip, this));
            if (!string.IsNullOrEmpty(this.Text))
            {
                base.Owner.Renderer.OnRenderRibbonItemText(new RibbonTextEventArgs(base.Owner, e.Graphics, e.Clip, this, Rectangle.FromLTRB(base.Bounds.Left + base.Owner.ItemMargin.Left, base.Bounds.Top + base.Owner.ItemMargin.Top, base.Bounds.Right - base.Owner.ItemMargin.Right, base.Bounds.Bottom - base.Owner.ItemMargin.Bottom), this.Text, FontStyle.Bold));
            }
        }

        public override void SetBounds(Rectangle bounds)
        {
            base.SetBounds(bounds);
        }
    }
}
