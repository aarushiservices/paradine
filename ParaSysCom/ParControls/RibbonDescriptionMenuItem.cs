﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    public partial class RibbonDescriptionMenuItem : RibbonButton
    {
        private Rectangle _descBounds;
        private string _description;

        public RibbonDescriptionMenuItem()
        {
            base.DropDownArrowDirection = RibbonArrowDirection.Left;
            base.SetDropDownMargin(new Padding(10));
        }

        public RibbonDescriptionMenuItem(string text)
            : this(null, text, null)
        {
        }

        public RibbonDescriptionMenuItem(string text, string description)
            : this(null, text, description)
        {
        }

        public RibbonDescriptionMenuItem(System.Drawing.Image image, string text, string description)
        {
            this.Image = image;
            this.Text = text;
            this.Description = description;
        }

        public override Size MeasureSize(object sender, RibbonElementMeasureSizeEventArgs e)
        {
            Size size = base.MeasureSize(sender, e);
            size.Height = 0x34;
            base.SetLastMeasuredSize(size);
            return size;
        }

        internal override Rectangle OnGetTextBounds(RibbonElementSizeMode sMode, Rectangle bounds)
        {
            Rectangle rectangle = base.OnGetTextBounds(sMode, bounds);
            this.DescriptionBounds = rectangle;
            rectangle.Height = 20;
            this.DescriptionBounds = Rectangle.FromLTRB(this.DescriptionBounds.Left, rectangle.Bottom, this.DescriptionBounds.Right, this.DescriptionBounds.Bottom);
            return rectangle;
        }

        protected override void OnPaintText(RibbonElementPaintEventArgs e)
        {
            if (e.Mode == RibbonElementSizeMode.DropDown)
            {
                StringFormat format = new StringFormat();
                format.LineAlignment = StringAlignment.Center;
                format.Alignment = StringAlignment.Near;
                base.Owner.Renderer.OnRenderRibbonItemText(new RibbonTextEventArgs(base.Owner, e.Graphics, e.Clip, this, base.TextBounds, this.Text, Color.Empty, FontStyle.Bold, format));
                format.Alignment = StringAlignment.Near;
                base.Owner.Renderer.OnRenderRibbonItemText(new RibbonTextEventArgs(base.Owner, e.Graphics, e.Clip, this, this.DescriptionBounds, this.Description, format));
            }
            else
            {
                base.OnPaintText(e);
            }
        }

        public string Description
        {
            get
            {
                return this._description;
            }
            set
            {
                this._description = value;
            }
        }

        public Rectangle DescriptionBounds
        {
            get
            {
                return this._descBounds;
            }
            set
            {
                this._descBounds = value;
            }
        }

        public override System.Drawing.Image Image
        {
            get
            {
                return base.Image;
            }
            set
            {
                base.Image = value;
                this.SmallImage = value;
            }
        }

        [Browsable(false)]
        public override System.Drawing.Image SmallImage
        {
            get
            {
                return base.SmallImage;
            }
            set
            {
                base.SmallImage = value;
            }
        }
    }
}
