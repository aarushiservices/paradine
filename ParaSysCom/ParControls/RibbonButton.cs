﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    [Designer(typeof(RibbonButtonDesigner))]
    public partial class RibbonButton : RibbonItem, IContainsRibbonComponents
    {
        private Rectangle _buttonFaceBounds;
        private bool _checkOnClick;
        private RibbonDropDown _dropDown;
        private RibbonArrowDirection _DropDownArrowDirection;
        private Size _dropDownArrowSize;
        private Rectangle _dropDownBounds;
        private RibbonItemCollection _dropDownItems;
        private Padding _dropDownMargin;
        private bool _dropDownPressed;
        private bool _dropDownResizable;
        private bool _dropDownSelected;
        private bool _dropDownVisible;
        private Rectangle _imageBounds;
        private Point _lastMousePos;
        private Image _smallImage;
        private string _SourceName;
        private RibbonButtonStyle _style;
        private Rectangle _textBounds;
        private const int arrowWidth = 5;

        public event EventHandler DropDownShowing;

        public RibbonButton()
        {
            this._SourceName = "";
            this._dropDownItems = new RibbonItemCollection();
            this._dropDownArrowSize = new Size(5, 3);
            this._dropDownMargin = new Padding(6);
            this._DropDownArrowDirection = RibbonArrowDirection.Down;
            this.Image = this.CreateImage(0x20);
            this.SmallImage = this.CreateImage(0x10);
        }

        public RibbonButton(Image smallImage)
            : this()
        {
            this.SmallImage = smallImage;
        }

        public RibbonButton(string text)
            : this()
        {
            this.Text = text;
        }

        private void _dropDown_Closed(object sender, EventArgs e)
        {
            this.SetPressed(false);
            this._dropDownPressed = false;
            this.SetDropDownVisible(false);
            this.SetSelected(false);
            this.RedrawItem();
        }

        public void CloseDropDown()
        {
            if (this.DropDown != null)
            {
                RibbonPopupManager.Dismiss(this.DropDown, RibbonPopupManager.DismissReason.NewPopup);
            }
            this.SetDropDownVisible(false);
        }

        protected override bool ClosesDropDownAt(Point p)
        {
            if (this.Style == RibbonButtonStyle.DropDown)
            {
                return false;
            }
            if (this.Style == RibbonButtonStyle.SplitDropDown)
            {
                return this.ButtonFaceBounds.Contains(p);
            }
            return true;
        }

        protected virtual void CreateDropDown()
        {
            this._dropDown = new RibbonDropDown(this, this.DropDownItems, base.Owner);
        }

        private Image CreateImage(int size)
        {
            return new Bitmap(size, size);
        }

        private void DropDown_MouseEnter(object sender, EventArgs e)
        {
            this.SetSelected(true);
            this.RedrawItem();
        }

        public IEnumerable<Component> GetAllChildComponents()
        {
            return this.DropDownItems.ToArray();
        }

        public IEnumerable<RibbonItem> GetItems()
        {
            return this.DropDownItems;
        }

        private void IgnoreDeactivation()
        {
            if (base.Canvas is RibbonPanelPopup)
            {
                (base.Canvas as RibbonPanelPopup).IgnoreNextClickDeactivation();
            }
            if (base.Canvas is RibbonDropDown)
            {
                (base.Canvas as RibbonDropDown).IgnoreNextClickDeactivation();
            }
        }

        public override Size MeasureSize(object sender, RibbonElementMeasureSizeEventArgs e)
        {
            RibbonElementSizeMode nearestSize = base.GetNearestSize(e.SizeMode);
            int horizontal = base.Owner.ItemMargin.Horizontal;
            int vertical = base.Owner.ItemMargin.Vertical;
            int num3 = (base.OwnerPanel == null) ? 0 : (base.OwnerPanel.ContentBounds.Height - base.Owner.ItemPadding.Vertical);
            Size size = (this.SmallImage != null) ? this.SmallImage.Size : Size.Empty;
            Size size2 = (this.Image != null) ? this.Image.Size : Size.Empty;
            Size empty = Size.Empty;
            switch (nearestSize)
            {
                case RibbonElementSizeMode.Overflow:
                case RibbonElementSizeMode.Large:
                    empty = MeasureStringLargeSize(e.Graphics, this.Text, base.Owner.Font);
                    if (string.IsNullOrEmpty(this.Text))
                    {
                        horizontal += size2.Width;
                        vertical += size2.Height;
                        break;
                    }
                    horizontal += Math.Max(empty.Width + 1, size2.Width);
                    vertical = num3;
                    break;

                case RibbonElementSizeMode.Compact:
                    horizontal += size.Width;
                    vertical += size.Height;
                    break;

                case RibbonElementSizeMode.Medium:
                case RibbonElementSizeMode.DropDown:
                    empty = TextRenderer.MeasureText(this.Text, base.Owner.Font);
                    if (!string.IsNullOrEmpty(this.Text))
                    {
                        horizontal += empty.Width + 1;
                    }
                    horizontal += size.Width + base.Owner.ItemMargin.Horizontal;
                    vertical += Math.Max(empty.Height, size.Height);
                    break;

                default:
                    throw new ApplicationException("SizeMode not supported: " + e.SizeMode.ToString());
            }
            if (nearestSize == RibbonElementSizeMode.DropDown)
            {
                vertical += 2;
            }
            if (this.Style == RibbonButtonStyle.DropDown)
            {
                horizontal += 5 + base.Owner.ItemMargin.Right;
            }
            else if (this.Style == RibbonButtonStyle.SplitDropDown)
            {
                horizontal += 5 + base.Owner.ItemMargin.Horizontal;
            }
            base.SetLastMeasuredSize(new Size(horizontal, vertical));
            return base.LastMeasuredSize;
        }

        public static Size MeasureStringLargeSize(Graphics g, string text, Font font)
        {
            if (string.IsNullOrEmpty(text))
            {
                return Size.Empty;
            }
            Size size = g.MeasureString(text, font).ToSize();
            string[] strArray = text.Split(new char[] { ' ' });
            string str = string.Empty;
            int width = size.Width;
            for (int i = 0; i < strArray.Length; i++)
            {
                if (strArray[i].Length > str.Length)
                {
                    str = strArray[i];
                }
            }
            if (strArray.Length > 1)
            {
                width = Math.Max(size.Width / 2, g.MeasureString(str, font).ToSize().Width) + 1;
            }
            else
            {
                return g.MeasureString(text, font).ToSize();
            }
            Size size2 = g.MeasureString(text, font, width).ToSize();
            return new Size(size2.Width, size2.Height);
        }

        public override void OnCanvasChanged(EventArgs e)
        {
            base.OnCanvasChanged(e);
            if (base.Canvas is RibbonDropDown)
            {
                this.DropDownArrowDirection = RibbonArrowDirection.Left;
            }
        }

        public override void OnClick(EventArgs e)
        {
            if ((this.Style == RibbonButtonStyle.Normal) || this.ButtonFaceBounds.Contains(this._lastMousePos))
            {
                if (this.CheckOnClick)
                {
                    this.Checked = !this.Checked;
                }
                base.OnClick(e);
            }
        }

        public void OnDropDownShowing(EventArgs e)
        {
            if (this.DropDownShowing != null)
            {
                this.DropDownShowing(this, e);
            }
        }

        internal virtual Rectangle OnGetButtonFaceBounds(RibbonElementSizeMode sMode, Rectangle bounds)
        {
            Rectangle rectangle = Rectangle.FromLTRB((bounds.Right - this._dropDownMargin.Horizontal) - 2, bounds.Top, bounds.Right, bounds.Bottom);
            switch (base.SizeMode)
            {
                case RibbonElementSizeMode.Overflow:
                case RibbonElementSizeMode.Large:
                    return Rectangle.FromLTRB(bounds.Left, bounds.Top, bounds.Right, this._dropDownBounds.Top);

                case RibbonElementSizeMode.Compact:
                case RibbonElementSizeMode.Medium:
                case RibbonElementSizeMode.DropDown:
                    return Rectangle.FromLTRB(bounds.Left, bounds.Top, this._dropDownBounds.Left, bounds.Bottom);
            }
            return Rectangle.Empty;
        }

        internal virtual Rectangle OnGetDropDownBounds(RibbonElementSizeMode sMode, Rectangle bounds)
        {
            Rectangle rectangle = Rectangle.FromLTRB((bounds.Right - this._dropDownMargin.Horizontal) - 2, bounds.Top, bounds.Right, bounds.Bottom);
            switch (base.SizeMode)
            {
                case RibbonElementSizeMode.Overflow:
                case RibbonElementSizeMode.Large:
                    return Rectangle.FromLTRB(bounds.Left, (bounds.Top + this.Image.Height) + base.Owner.ItemMargin.Vertical, bounds.Right, bounds.Bottom);

                case RibbonElementSizeMode.Compact:
                case RibbonElementSizeMode.Medium:
                case RibbonElementSizeMode.DropDown:
                    return rectangle;
            }
            return Rectangle.Empty;
        }

        internal virtual Point OnGetDropDownMenuLocation()
        {
            if (base.Canvas is RibbonDropDown)
            {
                return base.Canvas.PointToScreen(new Point(base.Bounds.Right, base.Bounds.Top));
            }
            return base.Canvas.PointToScreen(new Point(base.Bounds.Left, base.Bounds.Bottom));
        }

        internal virtual Size OnGetDropDownMenuSize()
        {
            return Size.Empty;
        }

        internal virtual Rectangle OnGetImageBounds(RibbonElementSizeMode sMode, Rectangle bounds)
        {
            if (sMode == RibbonElementSizeMode.Large)
            {
                if (this.Image != null)
                {
                    return new Rectangle(base.Bounds.Left + ((base.Bounds.Width - this.Image.Width) / 2), base.Bounds.Top + base.Owner.ItemMargin.Top, this.Image.Width, this.Image.Height);
                }
                return new Rectangle(this.ContentBounds.Location, new Size(0x20, 0x20));
            }
            if (this.SmallImage != null)
            {
                return new Rectangle(base.Bounds.Left + base.Owner.ItemMargin.Left, base.Bounds.Top + ((base.Bounds.Height - this.SmallImage.Height) / 2), this.SmallImage.Width, this.SmallImage.Height);
            }
            return new Rectangle(this.ContentBounds.Location, new Size(0, 0));
        }

        internal virtual Rectangle OnGetTextBounds(RibbonElementSizeMode sMode, Rectangle bounds)
        {
            int width = this._imageBounds.Width;
            int height = this._imageBounds.Height;
            if (sMode == RibbonElementSizeMode.Large)
            {
                return Rectangle.FromLTRB(base.Bounds.Left + base.Owner.ItemMargin.Left, (base.Bounds.Top + base.Owner.ItemMargin.Top) + height, base.Bounds.Right - base.Owner.ItemMargin.Right, base.Bounds.Bottom - base.Owner.ItemMargin.Bottom);
            }
            int num3 = (this.Style != RibbonButtonStyle.Normal) ? this._dropDownMargin.Horizontal : 0;
            return Rectangle.FromLTRB(((base.Bounds.Left + width) + base.Owner.ItemMargin.Horizontal) + base.Owner.ItemMargin.Left, base.Bounds.Top + base.Owner.ItemMargin.Top, base.Bounds.Right - num3, base.Bounds.Bottom - base.Owner.ItemMargin.Bottom);
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            if (this.Enabled)
            {
                if ((this.DropDownSelected || (this.Style == RibbonButtonStyle.DropDown)) && (this.DropDownItems.Count > 0))
                {
                    this._dropDownPressed = true;
                    this.ShowDropDown();
                }
                base.OnMouseDown(e);
            }
        }

        public override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            this._dropDownSelected = false;
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (this.Enabled)
            {
                if (this.Style == RibbonButtonStyle.SplitDropDown)
                {
                    bool flag = this._dropDownSelected;
                    if (this.DropDownBounds.Contains(e.X, e.Y))
                    {
                        this._dropDownSelected = true;
                    }
                    else
                    {
                        this._dropDownSelected = false;
                    }
                    if (flag != this._dropDownSelected)
                    {
                        this.RedrawItem();
                    }
                    flag = this._dropDownSelected;
                }
                this._lastMousePos = new Point(e.X, e.Y);
                base.OnMouseMove(e);
            }
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
        }

        public override void OnPaint(object sender, RibbonElementPaintEventArgs e)
        {
            if (base.Owner != null)
            {
                this.OnPaintBackground(e);
                this.OnPaintImage(e);
                this.OnPaintText(e);
            }
        }

        private void OnPaintBackground(RibbonElementPaintEventArgs e)
        {
            base.Owner.Renderer.OnRenderRibbonItem(new RibbonItemRenderEventArgs(base.Owner, e.Graphics, e.Clip, this));
        }

        private void OnPaintImage(RibbonElementPaintEventArgs e)
        {
            RibbonElementSizeMode nearestSize = base.GetNearestSize(e.Mode);
            if (((nearestSize == RibbonElementSizeMode.Large) && (this.Image != null)) || (this.SmallImage != null))
            {
                base.Owner.Renderer.OnRenderRibbonItemImage(new RibbonItemBoundsEventArgs(base.Owner, e.Graphics, e.Clip, this, this.OnGetImageBounds(nearestSize, base.Bounds)));
            }
        }

        protected virtual void OnPaintText(RibbonElementPaintEventArgs e)
        {
            if (base.SizeMode != RibbonElementSizeMode.Compact)
            {
                StringFormat format = new StringFormat();
                format.LineAlignment = StringAlignment.Center;
                format.Alignment = StringAlignment.Near;
                if (base.SizeMode == RibbonElementSizeMode.Large)
                {
                    format.Alignment = StringAlignment.Center;
                    if (!(string.IsNullOrEmpty(this.Text) || this.Text.Contains(" ")))
                    {
                        format.LineAlignment = StringAlignment.Near;
                    }
                }
                base.Owner.Renderer.OnRenderRibbonItemText(new RibbonTextEventArgs(base.Owner, e.Graphics, e.Clip, this, this.TextBounds, this.Text, format));
            }
        }

        public void PerformClick()
        {
            this.OnClick(EventArgs.Empty);
        }

        public override void SetBounds(Rectangle bounds)
        {
            base.SetBounds(bounds);
            RibbonElementSizeMode nearestSize = base.GetNearestSize(base.SizeMode);
            this._imageBounds = this.OnGetImageBounds(nearestSize, bounds);
            this._textBounds = this.OnGetTextBounds(nearestSize, bounds);
            if (this.Style == RibbonButtonStyle.SplitDropDown)
            {
                this._dropDownBounds = this.OnGetDropDownBounds(nearestSize, bounds);
                this._buttonFaceBounds = this.OnGetButtonFaceBounds(nearestSize, bounds);
            }
        }

        protected void SetDropDownMargin(Padding p)
        {
            this._dropDownMargin = p;
        }

        internal void SetDropDownPressed(bool pressed)
        {
            throw new NotImplementedException();
        }

        internal void SetDropDownSelected(bool selected)
        {
            throw new Exception();
        }

        internal void SetDropDownVisible(bool visible)
        {
            this._dropDownVisible = visible;
        }

        internal override void SetOwner(Ribbon owner)
        {
            base.SetOwner(owner);
            if (this._dropDownItems != null)
            {
                this._dropDownItems.SetOwner(owner);
            }
        }

        internal override void SetOwnerPanel(RibbonPanel ownerPanel)
        {
            base.SetOwnerPanel(ownerPanel);
            if (this._dropDownItems != null)
            {
                this._dropDownItems.SetOwnerPanel(ownerPanel);
            }
        }

        internal override void SetOwnerTab(RibbonTab ownerTab)
        {
            base.SetOwnerTab(ownerTab);
            if (this._dropDownItems != null)
            {
                this._dropDownItems.SetOwnerTab(ownerTab);
            }
        }

        internal override void SetPressed(bool pressed)
        {
            base.SetPressed(pressed);
        }

        internal override void SetSelected(bool selected)
        {
            base.SetSelected(selected);
            this.SetPressed(false);
        }

        internal override void SetSizeMode(RibbonElementSizeMode sizeMode)
        {
            if (sizeMode == RibbonElementSizeMode.Overflow)
            {
                base.SetSizeMode(RibbonElementSizeMode.Large);
            }
            else
            {
                base.SetSizeMode(sizeMode);
            }
        }

        public void ShowDropDown()
        {
            if ((this.Style == RibbonButtonStyle.Normal) || (this.DropDownItems.Count == 0))
            {
                if (this.DropDown != null)
                {
                    RibbonPopupManager.DismissChildren(this.DropDown, RibbonPopupManager.DismissReason.NewPopup);
                }
            }
            else
            {
                if (this.Style == RibbonButtonStyle.DropDown)
                {
                    this.SetPressed(true);
                }
                else
                {
                    this._dropDownPressed = true;
                }
                this.CreateDropDown();
                this.DropDown.MouseEnter += new EventHandler(this.DropDown_MouseEnter);
                this.DropDown.Closed += new EventHandler(this._dropDown_Closed);
                this.DropDown.ShowSizingGrip = this.DropDownResizable;
                RibbonPopup canvas = base.Canvas as RibbonPopup;
                Point screenLocation = this.OnGetDropDownMenuLocation();
                Size size = this.OnGetDropDownMenuSize();
                if (!size.IsEmpty)
                {
                    this.DropDown.MinimumSize = size;
                }
                this.OnDropDownShowing(EventArgs.Empty);
                this.SetDropDownVisible(true);
                this.DropDown.SelectionService = this.GetService(typeof(ISelectionService)) as ISelectionService;
                this.DropDown.Show(screenLocation);
            }
        }

        public override string ToString()
        {
            return string.Format("{1}: {0}", this.Text, base.GetType().Name);
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public Rectangle ButtonFaceBounds
        {
            get
            {
                return this._buttonFaceBounds;
            }
        }

        [Description("Toggles the Checked property of the button when clicked"), DefaultValue(false)]
        public bool CheckOnClick
        {
            get
            {
                return this._checkOnClick;
            }
            set
            {
                this._checkOnClick = value;
            }
        }

        internal RibbonDropDown DropDown
        {
            get
            {
                return this._dropDown;
            }
        }

        public RibbonArrowDirection DropDownArrowDirection
        {
            get
            {
                return this._DropDownArrowDirection;
            }
            set
            {
                this._DropDownArrowDirection = value;
                base.NotifyOwnerRegionsChanged();
            }
        }

        public Size DropDownArrowSize
        {
            get
            {
                return this._dropDownArrowSize;
            }
            set
            {
                this._dropDownArrowSize = value;
                base.NotifyOwnerRegionsChanged();
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public Rectangle DropDownBounds
        {
            get
            {
                return this._dropDownBounds;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public RibbonItemCollection DropDownItems
        {
            get
            {
                return this._dropDownItems;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool DropDownPressed
        {
            get
            {
                return this._dropDownPressed;
            }
        }

        [Description("Makes the DropDown resizable with a grip on the corner"), DefaultValue(false)]
        public bool DropDownResizable
        {
            get
            {
                return this._dropDownResizable;
            }
            set
            {
                this._dropDownResizable = value;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public bool DropDownSelected
        {
            get
            {
                return this._dropDownSelected;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool DropDownVisible
        {
            get
            {
                return this._dropDownVisible;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Rectangle ImageBounds
        {
            get
            {
                return this._imageBounds;
            }
        }

        [DefaultValue((string)null)]
        public virtual Image SmallImage
        {
            get
            {
                return this._smallImage;
            }
            set
            {
                this._smallImage = value;
                base.NotifyOwnerRegionsChanged();
            }
        }

        [DefaultValue("")]
        public virtual string SourceName
        {
            get
            {
                return this._SourceName;
            }
            set
            {
                this._SourceName = value;
            }
        }

        public RibbonButtonStyle Style
        {
            get
            {
                return this._style;
            }
            set
            {
                this._style = value;
                if ((base.Canvas is RibbonPopup) || ((base.OwnerItem != null) && (base.OwnerItem.Canvas is RibbonPopup)))
                {
                    this.DropDownArrowDirection = RibbonArrowDirection.Left;
                }
                base.NotifyOwnerRegionsChanged();
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Rectangle TextBounds
        {
            get
            {
                return this._textBounds;
            }
        }
    }
}
