﻿namespace ParControls
{
    internal class RibbonItemGroupDesigner : RibbonElementWithItemCollectionDesigner
    {
        public override RibbonItemCollection Collection
        {
            get
            {
                if (base.Component is RibbonItemGroup)
                {
                    return (base.Component as RibbonItemGroup).Items;
                }
                return null;
            }
        }

        public override ParControls.Ribbon Ribbon
        {
            get
            {
                if (base.Component is RibbonItemGroup)
                {
                    return (base.Component as RibbonItemGroup).Owner;
                }
                return null;
            }
        }
    }
}

