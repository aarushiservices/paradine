﻿namespace ParControls
{
    using System;
    using System.Drawing;

    public class RibbonProfesionalRendererColorTableBlack : RibbonProfesionalRendererColorTable
    {
        public RibbonProfesionalRendererColorTableBlack()
        {
            base.OrbDropDownDarkBorder = RibbonProfesionalRendererColorTable.ToGray(base.OrbDropDownDarkBorder);
            base.OrbDropDownLightBorder = RibbonProfesionalRendererColorTable.ToGray(base.OrbDropDownLightBorder);
            base.OrbDropDownBack = RibbonProfesionalRendererColorTable.ToGray(base.OrbDropDownBack);
            base.OrbDropDownNorthA = RibbonProfesionalRendererColorTable.ToGray(base.OrbDropDownNorthA);
            base.OrbDropDownNorthB = RibbonProfesionalRendererColorTable.ToGray(base.OrbDropDownNorthB);
            base.OrbDropDownNorthC = RibbonProfesionalRendererColorTable.ToGray(base.OrbDropDownNorthC);
            base.OrbDropDownNorthD = RibbonProfesionalRendererColorTable.ToGray(base.OrbDropDownNorthD);
            base.OrbDropDownSouthC = RibbonProfesionalRendererColorTable.ToGray(base.OrbDropDownSouthC);
            base.OrbDropDownSouthD = RibbonProfesionalRendererColorTable.ToGray(base.OrbDropDownSouthD);
            base.OrbDropDownContentbg = RibbonProfesionalRendererColorTable.ToGray(base.OrbDropDownContentbg);
            base.OrbDropDownContentbglight = RibbonProfesionalRendererColorTable.ToGray(base.OrbDropDownContentbglight);
            base.OrbDropDownSeparatorlight = RibbonProfesionalRendererColorTable.ToGray(base.OrbDropDownSeparatorlight);
            base.OrbDropDownSeparatordark = RibbonProfesionalRendererColorTable.ToGray(base.OrbDropDownSeparatordark);
            base.Caption1 = RibbonProfesionalRendererColorTable.ToGray(base.Caption1);
            base.Caption2 = RibbonProfesionalRendererColorTable.ToGray(base.Caption2);
            base.Caption3 = RibbonProfesionalRendererColorTable.ToGray(base.Caption3);
            base.Caption4 = RibbonProfesionalRendererColorTable.ToGray(base.Caption4);
            base.Caption5 = RibbonProfesionalRendererColorTable.ToGray(base.Caption5);
            base.Caption6 = RibbonProfesionalRendererColorTable.ToGray(base.Caption6);
            base.Caption7 = RibbonProfesionalRendererColorTable.ToGray(base.Caption7);
            base.QuickAccessBorderDark = RibbonProfesionalRendererColorTable.ToGray(base.QuickAccessBorderDark);
            base.QuickAccessBorderLight = RibbonProfesionalRendererColorTable.ToGray(base.QuickAccessBorderLight);
            base.QuickAccessUpper = RibbonProfesionalRendererColorTable.ToGray(base.QuickAccessUpper);
            base.QuickAccessLower = RibbonProfesionalRendererColorTable.ToGray(base.QuickAccessLower);
            base.OrbOptionBorder = RibbonProfesionalRendererColorTable.ToGray(base.OrbOptionBorder);
            base.OrbOptionBackground = RibbonProfesionalRendererColorTable.ToGray(base.OrbOptionBackground);
            base.OrbOptionShine = RibbonProfesionalRendererColorTable.ToGray(base.OrbOptionShine);
            base.Arrow = RibbonProfesionalRendererColorTable.FromHex("#7C7C7C");
            base.ArrowLight = RibbonProfesionalRendererColorTable.FromHex("#EAF2F9");
            base.ArrowDisabled = RibbonProfesionalRendererColorTable.FromHex("#7C7C7C");
            base.Text = RibbonProfesionalRendererColorTable.FromHex("#000000");
            base.RibbonBackground = RibbonProfesionalRendererColorTable.FromHex("#535353");
            base.TabBorder = RibbonProfesionalRendererColorTable.FromHex("#BEBEBE");
            base.TabNorth = RibbonProfesionalRendererColorTable.FromHex("#F1F2F2");
            base.TabSouth = RibbonProfesionalRendererColorTable.FromHex("#D6D9DF");
            base.TabGlow = RibbonProfesionalRendererColorTable.FromHex("#D1FBFF");
            base.TabText = Color.White;
            base.TabActiveText = Color.Black;
            base.TabContentNorth = RibbonProfesionalRendererColorTable.FromHex("#B6BCC6");
            base.TabContentSouth = RibbonProfesionalRendererColorTable.FromHex("#E6F0F1");
            base.TabSelectedGlow = RibbonProfesionalRendererColorTable.FromHex("#E1D2A5");
            base.PanelDarkBorder = RibbonProfesionalRendererColorTable.FromHex("#AEB0B4");
            base.PanelLightBorder = RibbonProfesionalRendererColorTable.FromHex("#E7E9ED");
            base.PanelTextBackground = RibbonProfesionalRendererColorTable.FromHex("#ABAEAE");
            base.PanelTextBackgroundSelected = RibbonProfesionalRendererColorTable.FromHex("#949495");
            base.PanelText = Color.White;
            base.PanelBackgroundSelected = RibbonProfesionalRendererColorTable.FromHex("#F3F5F5");
            base.PanelOverflowBackground = RibbonProfesionalRendererColorTable.FromHex("#B9D1F0");
            base.PanelOverflowBackgroundPressed = RibbonProfesionalRendererColorTable.FromHex("#AAAEB3");
            base.PanelOverflowBackgroundSelectedNorth = Color.FromArgb(100, Color.White);
            base.PanelOverflowBackgroundSelectedSouth = Color.FromArgb(0x66, RibbonProfesionalRendererColorTable.FromHex("#EBEBEB"));
            base.ButtonBgOut = RibbonProfesionalRendererColorTable.FromHex("#B4B9C2");
            base.ButtonBgCenter = RibbonProfesionalRendererColorTable.FromHex("#CDD2D8");
            base.ButtonBorderOut = RibbonProfesionalRendererColorTable.FromHex("#A9B1B8");
            base.ButtonBorderIn = RibbonProfesionalRendererColorTable.FromHex("#DFE2E6");
            base.ButtonGlossyNorth = RibbonProfesionalRendererColorTable.FromHex("#DBDFE4");
            base.ButtonGlossySouth = RibbonProfesionalRendererColorTable.FromHex("#DFE2E8");
            base.ButtonDisabledBgOut = RibbonProfesionalRendererColorTable.FromHex("#E0E4E8");
            base.ButtonDisabledBgCenter = RibbonProfesionalRendererColorTable.FromHex("#E8EBEF");
            base.ButtonDisabledBorderOut = RibbonProfesionalRendererColorTable.FromHex("#C5D1DE");
            base.ButtonDisabledBorderIn = RibbonProfesionalRendererColorTable.FromHex("#F1F3F5");
            base.ButtonDisabledGlossyNorth = RibbonProfesionalRendererColorTable.FromHex("#F0F3F6");
            base.ButtonDisabledGlossySouth = RibbonProfesionalRendererColorTable.FromHex("#EAEDF1");
            base.ButtonSelectedBgOut = RibbonProfesionalRendererColorTable.FromHex("#FFD646");
            base.ButtonSelectedBgCenter = RibbonProfesionalRendererColorTable.FromHex("#FFEAAC");
            base.ButtonSelectedBorderOut = RibbonProfesionalRendererColorTable.FromHex("#C2A978");
            base.ButtonSelectedBorderIn = RibbonProfesionalRendererColorTable.FromHex("#FFF2C7");
            base.ButtonSelectedGlossyNorth = RibbonProfesionalRendererColorTable.FromHex("#FFFDDB");
            base.ButtonSelectedGlossySouth = RibbonProfesionalRendererColorTable.FromHex("#FFE793");
            base.ButtonPressedBgOut = RibbonProfesionalRendererColorTable.FromHex("#F88F2C");
            base.ButtonPressedBgCenter = RibbonProfesionalRendererColorTable.FromHex("#FDF1B0");
            base.ButtonPressedBorderOut = RibbonProfesionalRendererColorTable.FromHex("#8E8165");
            base.ButtonPressedBorderIn = RibbonProfesionalRendererColorTable.FromHex("#F9C65A");
            base.ButtonPressedGlossyNorth = RibbonProfesionalRendererColorTable.FromHex("#FDD5A8");
            base.ButtonPressedGlossySouth = RibbonProfesionalRendererColorTable.FromHex("#FBB062");
            base.ButtonCheckedBgOut = RibbonProfesionalRendererColorTable.FromHex("#F9AA45");
            base.ButtonCheckedBgCenter = RibbonProfesionalRendererColorTable.FromHex("#FDEA9D");
            base.ButtonCheckedBorderOut = RibbonProfesionalRendererColorTable.FromHex("#8E8165");
            base.ButtonCheckedBorderIn = RibbonProfesionalRendererColorTable.FromHex("#F9C65A");
            base.ButtonCheckedGlossyNorth = RibbonProfesionalRendererColorTable.FromHex("#F8DBB7");
            base.ButtonCheckedGlossySouth = RibbonProfesionalRendererColorTable.FromHex("#FED18E");
            base.ItemGroupOuterBorder = RibbonProfesionalRendererColorTable.FromHex("#ADB7BB");
            base.ItemGroupInnerBorder = Color.FromArgb(0x33, Color.White);
            base.ItemGroupSeparatorLight = Color.FromArgb(0x40, Color.White);
            base.ItemGroupSeparatorDark = Color.FromArgb(0x26, RibbonProfesionalRendererColorTable.FromHex("#ADB7BB"));
            base.ItemGroupBgNorth = RibbonProfesionalRendererColorTable.FromHex("#D9E0E1");
            base.ItemGroupBgSouth = RibbonProfesionalRendererColorTable.FromHex("#EDF0F1");
            base.ItemGroupBgGlossy = RibbonProfesionalRendererColorTable.FromHex("#D2D9DB");
            base.ButtonListBorder = RibbonProfesionalRendererColorTable.FromHex("#ACACAC");
            base.ButtonListBg = RibbonProfesionalRendererColorTable.FromHex("#DAE2E2");
            base.ButtonListBgSelected = RibbonProfesionalRendererColorTable.FromHex("#F7F7F7");
            base.DropDownBg = RibbonProfesionalRendererColorTable.FromHex("#FAFAFA");
            base.DropDownImageBg = RibbonProfesionalRendererColorTable.FromHex("#E9EEEE");
            base.DropDownImageSeparator = RibbonProfesionalRendererColorTable.FromHex("#C5C5C5");
            base.DropDownBorder = RibbonProfesionalRendererColorTable.FromHex("#868686");
            base.DropDownGripNorth = RibbonProfesionalRendererColorTable.FromHex("#FFFFFF");
            base.DropDownGripSouth = RibbonProfesionalRendererColorTable.FromHex("#DFE9EF");
            base.DropDownGripBorder = RibbonProfesionalRendererColorTable.FromHex("#DDE7EE");
            base.DropDownGripDark = RibbonProfesionalRendererColorTable.FromHex("#5574A7");
            base.DropDownGripLight = RibbonProfesionalRendererColorTable.FromHex("#FFFFFF");
            base.SeparatorLight = RibbonProfesionalRendererColorTable.FromHex("#E6E8EB");
            base.SeparatorDark = RibbonProfesionalRendererColorTable.FromHex("#C5C5C5");
            base.SeparatorBg = RibbonProfesionalRendererColorTable.FromHex("#EBEBEB");
            base.SeparatorLine = RibbonProfesionalRendererColorTable.FromHex("#C5C5C5");
            base.TextBoxUnselectedBg = RibbonProfesionalRendererColorTable.FromHex("#E8E8E8");
            base.TextBoxBorder = RibbonProfesionalRendererColorTable.FromHex("#898989");
        }
    }
}

