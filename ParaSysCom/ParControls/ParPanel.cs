﻿namespace ParControls
{
    using ParaSysCom;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Text;
    using System.Globalization;
    using System.Windows.Forms;

    public partial class ParPanel : Panel
    {
       // private Color _borderColor = Color.Gray;
      // private int _borderWidth = 1;
      //  private Color _gradientEndColor = Color.Gray;
      //  private Color _gradientStartColor = Color.White;
      // private int _HeaderBorderHeight = 30;
      // private string _HeaderText = "";
      //  private short _HeaderTextFontSize = 10;
      //  private StringAlignment _HearderAlignment = StringAlignment.Center;
      // private System.Drawing.Image _image;
      //  private Point _imageLocation = new Point(4, 4);
      // private int _roundCornerRadius = 4;
      //  private int _shadowOffSet = 5;
      //private IContainer components = null;
        public bool Licensed = false;

        public ParPanel()
        {
            base.SetStyle(ControlStyles.DoubleBuffer, true);
            base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            base.SetStyle(ControlStyles.ResizeRedraw, true);
            base.SetStyle(ControlStyles.UserPaint, true);
            base.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.InitializeComponent();
        }       

        internal static Color FromHex(string hex)
        {
            if (hex.StartsWith("#"))
            {
                hex = hex.Substring(1);
            }
            if (hex.Length != 6)
            {
                throw new Exception("Color not valid");
            }
            return Color.FromArgb(int.Parse(hex.Substring(0, 2), NumberStyles.HexNumber), int.Parse(hex.Substring(2, 2), NumberStyles.HexNumber), int.Parse(hex.Substring(4, 2), NumberStyles.HexNumber));
        }

       

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            base.OnPaintBackground(e);
            if (base.DesignMode && !GlobalVariables.CodeLicensed)
            {
                Application.Exit();
            }
            if (this._gradientEndColor != Color.Transparent)
            {
                this._gradientEndColor = GlobalVariables.MainThemeColor;
            }
            else
            {
                this._gradientStartColor = Color.Transparent;
            }
            int x = Math.Min(Math.Min(this._shadowOffSet, base.Width - 2), base.Height - 2);
            if (this._roundCornerRadius == 0)
            {
                this._roundCornerRadius = 2;
            }
            int depth = Math.Min(Math.Min(this._roundCornerRadius, base.Width - 2), base.Height - 2);
            if ((base.Width > 1) && (base.Height > 1))
            {
                e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
                Rectangle r = new Rectangle(0, 0, (base.Width - x) - 1, (base.Height - x) - 1);
                Rectangle rectangle2 = new Rectangle(x, x, (base.Width - x) - 1, (base.Height - x) - 1);
                GraphicsPath roundPath = ParPanelGraphics.GetRoundPath(rectangle2, depth);
                GraphicsPath path = ParPanelGraphics.GetRoundPath(r, depth);
                if (depth > 0)
                {
                    using (PathGradientBrush brush = new PathGradientBrush(roundPath))
                    {
                        brush.WrapMode = WrapMode.Clamp;
                        ColorBlend blend = new ColorBlend(3);
                        blend.Colors = new Color[] { Color.FromArgb(180, this._gradientStartColor), Color.FromArgb(180, this._gradientStartColor), Color.FromArgb(180, this._gradientEndColor) };
                        float[] numArray = new float[3];
                        numArray[1] = 0.1f;
                        numArray[2] = 1f;
                        blend.Positions = numArray;
                        brush.InterpolationColors = blend;
                        if ((this._gradientStartColor != Color.Transparent) && (this._gradientEndColor != Color.Transparent))
                        {
                            e.Graphics.FillPath(brush, roundPath);
                        }
                    }
                }
                LinearGradientBrush brush2 = new LinearGradientBrush(r, this._gradientStartColor, this._gradientEndColor, LinearGradientMode.BackwardDiagonal);
                if (!(this._gradientStartColor == Color.Transparent) || !(this._gradientEndColor == Color.Transparent))
                {
                    e.Graphics.FillPath(brush2, path);
                    e.Graphics.DrawPath(new Pen(Color.FromArgb(180, this._borderColor), (float)this._borderWidth), path);
                }
                if (this._HeaderText != "")
                {
                    StringFormat format = new StringFormat();
                    format.Alignment = this._HearderAlignment;
                    format.Trimming = StringTrimming.EllipsisCharacter;
                    format.LineAlignment = StringAlignment.Center;
                    format.FormatFlags |= StringFormatFlags.NoWrap;
                    Rectangle rect = new Rectangle(0, 0, base.Width - 1, this._HeaderBorderHeight - 1);
                    if (base.Width == 30)
                    {
                        rect = new Rectangle(0, 0, base.Width - 1, base.Height - 1);
                        format.FormatFlags |= StringFormatFlags.DisplayFormatControl | StringFormatFlags.DirectionVertical;
                    }
                    brush2 = new LinearGradientBrush(rect, this._gradientStartColor, this._gradientEndColor, LinearGradientMode.Vertical);
                    e.Graphics.FillRectangle(brush2, rect);
                    e.Graphics.DrawRectangle(new Pen(this._borderColor), rect);
                    using (Brush brush3 = new SolidBrush(Color.FromArgb(0xff, 0x15, 0x42, 0x8b)))
                    {
                        e.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                        string s = "";
                        if (this._HearderAlignment == StringAlignment.Near)
                        {
                            s = "   " + this._HeaderText;
                        }
                        else if (this._HearderAlignment == StringAlignment.Far)
                        {
                            s = this._HeaderText + "   ";
                        }
                        else
                        {
                            s = this._HeaderText;
                        }
                        if (this._image != null)
                        {
                            s = "    " + s;
                        }
                        e.Graphics.DrawString(s, new Font("Segoe UI", (float)this._HeaderTextFontSize, FontStyle.Bold), brush3, rect, format);
                        if (this._image != null)
                        {
                            e.Graphics.DrawImageUnscaled(this._image, this._imageLocation);
                        }
                    }
                }
            }
        }

        [DefaultValue("Color.Gray"), Browsable(true), Category("A1")]
        public Color BorderColor
        {
            get
            {
                return this._borderColor;
            }
            set
            {
                this._borderColor = value;
                base.Invalidate();
            }
        }

        [DefaultValue(1), Category("A1"), Browsable(true)]
        public int BorderWidth
        {
            get
            {
                return this._borderWidth;
            }
            set
            {
                this._borderWidth = value;
                base.Invalidate();
            }
        }

        [DefaultValue("Color.Gray"), Browsable(true), Category("A1")]
        public Color GradientEndColor
        {
            get
            {
                return this._gradientEndColor;
            }
            set
            {
                this._gradientEndColor = value;
                base.Invalidate();
            }
        }

        [Category("A1"), DefaultValue("Color.White"), Browsable(true)]
        public Color GradientStartColor
        {
            get
            {
                return this._gradientStartColor;
            }
            set
            {
                this._gradientStartColor = value;
                base.Invalidate();
            }
        }

        [DefaultValue(1), Category("A1"), Browsable(true)]
        public StringAlignment HeaderAlignment
        {
            get
            {
                return this._HearderAlignment;
            }
            set
            {
                this._HearderAlignment = value;
                base.Invalidate();
            }
        }

        [Browsable(true), Category("A1"), DefaultValue(30)]
        public int HeaderBoarderHeight
        {
            get
            {
                return this._HeaderBorderHeight;
            }
            set
            {
                this._HeaderBorderHeight = value;
                base.Invalidate();
            }
        }

        [DefaultValue(""), Category("A1"), Browsable(true)]
        public string HeaderText
        {
            get
            {
                return this._HeaderText;
            }
            set
            {
                this._HeaderText = value;
                base.Invalidate();
            }
        }

        [DefaultValue(10), Category("A1"), Browsable(true)]
        public short HeaderTextFontSize
        {
            get
            {
                return this._HeaderTextFontSize;
            }
            set
            {
                this._HeaderTextFontSize = value;
                base.Invalidate();
            }
        }

        [Browsable(true), Category("A1")]
        public System.Drawing.Image Image
        {
            get
            {
                return this._image;
            }
            set
            {
                this._image = value;
                base.Invalidate();
            }
        }

        [Browsable(true), Category("A1"), DefaultValue("4,4")]
        public Point ImageLocation
        {
            get
            {
                return this._imageLocation;
            }
            set
            {
                this._imageLocation = value;
                base.Invalidate();
            }
        }

        [Browsable(true), DefaultValue(4), Category("A1")]
        public int RoundCornerRadius
        {
            get
            {
                return this._roundCornerRadius;
            }
            set
            {
                this._roundCornerRadius = Math.Abs(value);
                base.Invalidate();
            }
        }

        [DefaultValue(5), Category("A1"), Browsable(true)]
        public int ShadowOffSet
        {
            get
            {
                return this._shadowOffSet;
            }
            set
            {
                this._shadowOffSet = Math.Abs(value);
                base.Invalidate();
            }
        }
    }
}

