﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    internal partial class RibbonWrappedDropDown : ToolStripDropDown
    {
        public RibbonWrappedDropDown()
        {
            this.DoubleBuffered = false;
            base.SetStyle(ControlStyles.Opaque, true);
            base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            base.SetStyle(ControlStyles.Selectable, false);
            base.SetStyle(ControlStyles.ResizeRedraw, false);
            this.AutoSize = false;
        }
    }
}
