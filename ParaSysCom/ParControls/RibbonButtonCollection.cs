﻿namespace ParControls
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    public class RibbonButtonCollection : RibbonItemCollection
    {
        private RibbonButtonList _ownerList;

        internal RibbonButtonCollection(RibbonButtonList list)
        {
            this._ownerList = list;
        }

        public void Add(RibbonButton item)
        {
            this.CheckRestrictions(item);
            item.SetOwner(base.Owner);
            item.SetOwnerPanel(base.OwnerPanel);
            item.SetOwnerTab(base.OwnerTab);
            item.SetOwnerItem(this.OwnerList);
            base.Add(item);
        }

        public new void AddRange(IEnumerable<RibbonItem> items)
        {
            foreach (RibbonItem item in items)
            {
                this.CheckRestrictions(item as RibbonButton);
                item.SetOwner(base.Owner);
                item.SetOwnerPanel(base.OwnerPanel);
                item.SetOwnerTab(base.OwnerTab);
                item.SetOwnerItem(this.OwnerList);
            }
            base.AddRange(items);
        }

        private void CheckRestrictions(RibbonButton button)
        {
            if (button == null)
            {
                throw new ApplicationException("The RibbonButtonList only accepts button in the Buttons collection");
            }
            if (button.Style != RibbonButtonStyle.Normal)
            {
                throw new ApplicationException("The only style supported by the RibbonButtonList is Normal");
            }
        }

        public new void  Insert(int index, RibbonItem item)
        {
            this.CheckRestrictions(item as RibbonButton);
            item.SetOwner(base.Owner);
            item.SetOwnerPanel(base.OwnerPanel);
            item.SetOwnerTab(base.OwnerTab);
            item.SetOwnerItem(this.OwnerList);
            base.Insert(index, item);
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public RibbonButtonList OwnerList
        {
            get
            {
                return this._ownerList;
            }
        }
    }
}

