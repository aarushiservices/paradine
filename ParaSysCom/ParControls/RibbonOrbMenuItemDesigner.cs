﻿namespace ParControls
{
    using System;
    using System.ComponentModel.Design;

    internal class RibbonOrbMenuItemDesigner : RibbonElementWithItemCollectionDesigner
    {
        protected override DesignerVerbCollection OnGetVerbs()
        {
            return new DesignerVerbCollection(new DesignerVerb[] { new DesignerVerb("Add DescriptionMenuItem", new EventHandler(this.AddDescriptionMenuItem)), new DesignerVerb("Add Separator", new EventHandler(this.AddSeparator)) });
        }

        public override RibbonItemCollection Collection
        {
            get
            {
                if (base.Component is RibbonButton)
                {
                    return (base.Component as RibbonButton).DropDownItems;
                }
                return null;
            }
        }

        public override ParControls.Ribbon Ribbon
        {
            get
            {
                if (base.Component is RibbonButton)
                {
                    return (base.Component as RibbonButton).Owner;
                }
                return null;
            }
        }
    }
}

