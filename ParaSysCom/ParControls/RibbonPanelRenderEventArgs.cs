﻿namespace ParControls
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    public sealed class RibbonPanelRenderEventArgs : RibbonRenderEventArgs
    {
        private Control _canvas;
        private RibbonPanel _panel;

        public RibbonPanelRenderEventArgs(Ribbon owner, Graphics g, Rectangle clip, RibbonPanel panel, Control canvas) : base(owner, g, clip)
        {
            this.Panel = panel;
            this.Canvas = canvas;
        }

        public Control Canvas
        {
            get
            {
                return this._canvas;
            }
            set
            {
                this._canvas = value;
            }
        }

        public RibbonPanel Panel
        {
            get
            {
                return this._panel;
            }
            set
            {
                this._panel = value;
            }
        }
    }
}

