﻿namespace ParControls
{
    using System;
    using System.Drawing;

    public interface IScrollableRibbonItem
    {
        void ScrollDown();
        void ScrollUp();

        Rectangle ContentBounds { get; }
    }
}

