﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    [DesignTimeVisible(false), Designer(typeof(RibbonTabDesigner))]
    public partial class RibbonTab : Component, IRibbonElement, IContainsRibbonComponents
    {
        private bool _active;
        private RibbonContext _context;
        private int _offset;
        private Ribbon _owner;
        private RibbonPanelCollection _panels;
        private bool _pressed;
        private Rectangle _scrollLeftBounds;
        private bool _scrollLeftPressed;
        private bool _scrollLeftSelected;
        private bool _scrollLeftVisible;
        private Rectangle _scrollRightBounds;
        private bool _scrollRightPressed;
        private bool _scrollRightSelected;
        private bool _scrollRightVisible;
        private bool _selected;
        private Rectangle _tabBounds;
        private Rectangle _tabContentBounds;
        private object _tag;
        private string _text;

        public event EventHandler ActiveChanged;

        public event EventHandler ContextChanged;

        public event MouseEventHandler MouseEnter;

        public event MouseEventHandler MouseLeave;

        public event MouseEventHandler MouseMove;

        public event EventHandler OwnerChanged;

        public event EventHandler PressedChanged;

        public event EventHandler ScrollLeftBoundsChanged;

        public event EventHandler ScrollLeftPressedChanged;

        public event EventHandler ScrollLeftSelectedChanged;

        public event EventHandler ScrollLeftVisibleChanged;

        public event EventHandler ScrollRightBoundsChanged;

        public event EventHandler ScrollRightPressedChanged;

        public event EventHandler ScrollRightSelectedChanged;

        public event EventHandler ScrollRightVisibleChanged;

        public event EventHandler TabBoundsChanged;

        public event EventHandler TabContentBoundsChanged;

        public event EventHandler TextChanged;

        public RibbonTab()
        {
            this._panels = new RibbonPanelCollection(this);
        }

        public RibbonTab(Ribbon owner, string text)
        {
            this._panels = new RibbonPanelCollection(owner, this);
            this._text = text;
        }

        private bool AllPanelsOverflow()
        {
            foreach (RibbonPanel panel in this.Panels)
            {
                if (panel.SizeMode != RibbonElementSizeMode.Overflow)
                {
                    return false;
                }
            }
            return true;
        }

        public IEnumerable<Component> GetAllChildComponents()
        {
            return this.Panels.ToArray();
        }

        private RibbonPanel GetLargerPanel()
        {
            RibbonPanel largerPanel = this.GetLargerPanel(RibbonElementSizeMode.Large);
            if (largerPanel != null)
            {
                return largerPanel;
            }
            RibbonPanel panel2 = this.GetLargerPanel(RibbonElementSizeMode.Medium);
            if (panel2 != null)
            {
                return panel2;
            }
            RibbonPanel panel3 = this.GetLargerPanel(RibbonElementSizeMode.Compact);
            if (panel3 != null)
            {
                return panel3;
            }
            RibbonPanel panel4 = this.GetLargerPanel(RibbonElementSizeMode.Overflow);
            if (panel4 != null)
            {
                return panel4;
            }
            return null;
        }

        private RibbonPanel GetLargerPanel(RibbonElementSizeMode size)
        {
            RibbonPanel panel = null;
            foreach (RibbonPanel panel2 in this.Panels)
            {
                if (panel2.SizeMode == size)
                {
                    if (panel == null)
                    {
                        panel = panel2;
                    }
                    if (panel2.Bounds.Width > panel.Bounds.Width)
                    {
                        panel = panel2;
                    }
                }
            }
            return panel;
        }

        public Size MeasureSize(object sender, RibbonElementMeasureSizeEventArgs e)
        {
            return TextRenderer.MeasureText(this.Text, this.Owner.Font);
        }

        public void OnActiveChanged(EventArgs e)
        {
            if (this.ActiveChanged != null)
            {
                this.ActiveChanged(this, e);
            }
        }

        public void OnContextChanged(EventArgs e)
        {
            if (this.ContextChanged != null)
            {
                this.ContextChanged(this, e);
            }
        }

        public virtual void OnMouseEnter(MouseEventArgs e)
        {
            if (this.MouseEnter != null)
            {
                this.MouseEnter(this, e);
            }
        }

        public virtual void OnMouseLeave(MouseEventArgs e)
        {
            if (this.MouseLeave != null)
            {
                this.MouseLeave(this, e);
            }
        }

        public virtual void OnMouseMove(MouseEventArgs e)
        {
            if (this.MouseMove != null)
            {
                this.MouseMove(this, e);
            }
        }

        public void OnOwnerChanged(EventArgs e)
        {
            if (this.OwnerChanged != null)
            {
                this.OwnerChanged(this, e);
            }
        }

        public void OnPaint(object sender, RibbonElementPaintEventArgs e)
        {
            if (this.Owner != null)
            {
                this.Owner.Renderer.OnRenderRibbonTab(new RibbonTabRenderEventArgs(this.Owner, e.Graphics, e.Clip, this));
                this.Owner.Renderer.OnRenderRibbonTabText(new RibbonTabRenderEventArgs(this.Owner, e.Graphics, e.Clip, this));
                if (this.Active)
                {
                    foreach (RibbonPanel panel in this.Panels)
                    {
                        panel.OnPaint(this, new RibbonElementPaintEventArgs(e.Clip, e.Graphics, panel.SizeMode, e.Control));
                    }
                }
                this.Owner.Renderer.OnRenderTabScrollButtons(new RibbonTabRenderEventArgs(this.Owner, e.Graphics, e.Clip, this));
            }
        }

        public void OnPressedChanged(EventArgs e)
        {
            if (this.PressedChanged != null)
            {
                this.PressedChanged(this, e);
            }
        }

        public void OnScrollLeftBoundsChanged(EventArgs e)
        {
            if (this.ScrollLeftBoundsChanged != null)
            {
                this.ScrollLeftBoundsChanged(this, e);
            }
        }

        public void OnScrollLeftPressedChanged(EventArgs e)
        {
            if (this.ScrollLeftPressedChanged != null)
            {
                this.ScrollLeftPressedChanged(this, e);
            }
        }

        public void OnScrollLeftSelectedChanged(EventArgs e)
        {
            if (this.ScrollLeftSelectedChanged != null)
            {
                this.ScrollLeftSelectedChanged(this, e);
            }
        }

        public void OnScrollLeftVisibleChanged(EventArgs e)
        {
            if (this.ScrollLeftVisibleChanged != null)
            {
                this.ScrollLeftVisibleChanged(this, e);
            }
        }

        public void OnScrollRightBoundsChanged(EventArgs e)
        {
            if (this.ScrollRightBoundsChanged != null)
            {
                this.ScrollRightBoundsChanged(this, e);
            }
        }

        public void OnScrollRightPressedChanged(EventArgs e)
        {
            if (this.ScrollRightPressedChanged != null)
            {
                this.ScrollRightPressedChanged(this, e);
            }
        }

        public void OnScrollRightSelectedChanged(EventArgs e)
        {
            if (this.ScrollRightSelectedChanged != null)
            {
                this.ScrollRightSelectedChanged(this, e);
            }
        }

        public void OnScrollRightVisibleChanged(EventArgs e)
        {
            if (this.ScrollRightVisibleChanged != null)
            {
                this.ScrollRightVisibleChanged(this, e);
            }
        }

        public void OnTabBoundsChanged(EventArgs e)
        {
            if (this.TabBoundsChanged != null)
            {
                this.TabBoundsChanged(this, e);
            }
        }

        public void OnTabContentBoundsChanged(EventArgs e)
        {
            if (this.TabContentBoundsChanged != null)
            {
                this.TabContentBoundsChanged(this, e);
            }
        }

        public void OnTextChanged(EventArgs e)
        {
            if (this.TextChanged != null)
            {
                this.TextChanged(this, e);
            }
        }

        public void ScrollLeft()
        {
            this.ScrollOffset(50);
        }

        public void ScrollOffset(int amount)
        {
            this._offset += amount;
            foreach (RibbonPanel panel in this.Panels)
            {
                panel.SetBounds(new Rectangle(panel.Bounds.Left + amount, panel.Bounds.Top, panel.Bounds.Width, panel.Bounds.Height));
            }
            if ((this.Site != null) && this.Site.DesignMode)
            {
                this.UpdatePanelsRegions();
            }
            this.UpdateScrollBounds();
            this.Owner.Invalidate();
        }

        public void ScrollRight()
        {
            this.ScrollOffset(-50);
        }

        internal void SetActive(bool active)
        {
            bool flag = this._active != active;
            this._active = active;
            if (flag)
            {
                this.OnActiveChanged(EventArgs.Empty);
            }
        }

        public void SetBounds(Rectangle bounds)
        {
            throw new NotSupportedException();
        }

        public void SetContext(RibbonContext context)
        {
            if (!context.Equals(context))
            {
                this.OnContextChanged(EventArgs.Empty);
            }
            this._context = context;
            throw new NotImplementedException();
        }

        internal void SetOwner(Ribbon owner)
        {
            this._owner = owner;
            this.Panels.SetOwner(owner);
            this.OnOwnerChanged(EventArgs.Empty);
        }

        internal void SetPressed(bool pressed)
        {
            this._pressed = pressed;
            this.OnPressedChanged(EventArgs.Empty);
        }

        internal void SetScrollLeftPressed(bool pressed)
        {
            this._scrollLeftPressed = pressed;
            if (pressed)
            {
                this.ScrollLeft();
            }
            this.OnScrollLeftPressedChanged(EventArgs.Empty);
        }

        internal void SetScrollLeftSelected(bool selected)
        {
            this._scrollLeftSelected = selected;
            this.OnScrollLeftSelectedChanged(EventArgs.Empty);
        }

        internal void SetScrollRightPressed(bool pressed)
        {
            this._scrollRightPressed = pressed;
            if (pressed)
            {
                this.ScrollRight();
            }
            this.OnScrollRightPressedChanged(EventArgs.Empty);
        }

        internal void SetScrollRightSelected(bool selected)
        {
            this._scrollRightSelected = selected;
            this.OnScrollRightSelectedChanged(EventArgs.Empty);
        }

        internal void SetSelected(bool selected)
        {
            this._selected = selected;
            if (selected)
            {
                this.OnMouseEnter(new MouseEventArgs(MouseButtons.None, 0, 0, 0, 0));
            }
            else
            {
                this.OnMouseLeave(new MouseEventArgs(MouseButtons.None, 0, 0, 0, 0));
            }
        }

        internal void SetTabBounds(Rectangle tabBounds)
        {
            bool flag = this._tabBounds != tabBounds;
            this._tabBounds = tabBounds;
            this.OnTabBoundsChanged(EventArgs.Empty);
        }

        internal void SetTabContentBounds(Rectangle tabContentBounds)
        {
            bool flag = this._tabContentBounds != tabContentBounds;
            this._tabContentBounds = tabContentBounds;
            this.OnTabContentBoundsChanged(EventArgs.Empty);
        }

        public override string ToString()
        {
            return string.Format("Tab: {0}", this.Text);
        }

        internal void UpdatePanelsRegions()
        {
            if (this.Panels.Count != 0)
            {
                bool flag = (this.Site != null) && this.Site.DesignMode;
                if (!flag)
                {
                    this._offset = 0;
                }
                int x = (this.TabContentBounds.Left + this.Owner.PanelPadding.Left) + this._offset;
                int y = this.TabContentBounds.Top + this.Owner.PanelPadding.Top;
                using (Graphics graphics = this.Owner.CreateGraphics())
                {
                    Size size;
                    foreach (RibbonPanel panel in this.Panels)
                    {
                        RibbonElementSizeMode sizeMode = (panel.FlowsTo == RibbonPanelFlowDirection.Right) ? RibbonElementSizeMode.Medium : RibbonElementSizeMode.Large;
                        panel.SetBounds(new Rectangle(0, 0, 1, this.TabContentBounds.Height - this.Owner.PanelPadding.Vertical));
                        size = panel.MeasureSize(this, new RibbonElementMeasureSizeEventArgs(graphics, sizeMode));
                        Rectangle bounds = new Rectangle(x, y, size.Width, size.Height);
                        panel.SetBounds(bounds);
                        panel.SetSizeMode(sizeMode);
                        x = (bounds.Right + 1) + this.Owner.PanelSpacing;
                    }
                    if (!flag)
                    {
                        while ((x > this.TabContentBounds.Right) && !this.AllPanelsOverflow())
                        {
                            RibbonPanel largerPanel = this.GetLargerPanel();
                            if (largerPanel.SizeMode == RibbonElementSizeMode.Large)
                            {
                                largerPanel.SetSizeMode(RibbonElementSizeMode.Medium);
                            }
                            else if (largerPanel.SizeMode == RibbonElementSizeMode.Medium)
                            {
                                largerPanel.SetSizeMode(RibbonElementSizeMode.Compact);
                            }
                            else if (largerPanel.SizeMode == RibbonElementSizeMode.Compact)
                            {
                                largerPanel.SetSizeMode(RibbonElementSizeMode.Overflow);
                            }
                            size = largerPanel.MeasureSize(this, new RibbonElementMeasureSizeEventArgs(graphics, largerPanel.SizeMode));
                            largerPanel.SetBounds(new Rectangle(largerPanel.Bounds.Location, new Size(size.Width + this.Owner.PanelMargin.Horizontal, size.Height)));
                            x = this.TabContentBounds.Left + this.Owner.PanelPadding.Left;
                            foreach (RibbonPanel panel in this.Panels)
                            {
                                Size size2 = panel.Bounds.Size;
                                panel.SetBounds(new Rectangle(new Point(x, y), size2));
                                x += (panel.Bounds.Width + 1) + this.Owner.PanelSpacing;
                            }
                        }
                    }
                    foreach (RibbonPanel panel in this.Panels)
                    {
                        panel.UpdateItemsRegions(graphics, panel.SizeMode);
                    }
                }
                this.UpdateScrollBounds();
            }
        }

        private void UpdateScrollBounds()
        {
            int right = 13;
            bool flag = this._scrollRightVisible;
            bool flag2 = this._scrollLeftVisible;
            Rectangle rectangle = this._scrollRightBounds;
            Rectangle rectangle2 = this._scrollLeftBounds;
            if (this.Panels.Count != 0)
            {
                if (this.Panels[this.Panels.Count - 1].Bounds.Right > this.TabContentBounds.Right)
                {
                    this._scrollRightVisible = true;
                }
                else
                {
                    this._scrollRightVisible = false;
                }
                if (this._scrollRightVisible != flag)
                {
                    this.OnScrollRightVisibleChanged(EventArgs.Empty);
                }
                if (this._offset < 0)
                {
                    this._scrollLeftVisible = true;
                }
                else
                {
                    this._scrollLeftVisible = false;
                }
                if (this._scrollRightVisible != flag)
                {
                    this.OnScrollLeftVisibleChanged(EventArgs.Empty);
                }
                if (this._scrollLeftVisible || this._scrollRightVisible)
                {
                    this._scrollRightBounds = Rectangle.FromLTRB(this.Owner.ClientRectangle.Right - right, this.TabContentBounds.Top, this.Owner.ClientRectangle.Right, this.TabContentBounds.Bottom);
                    this._scrollLeftBounds = Rectangle.FromLTRB(0, this.TabContentBounds.Top, right, this.TabContentBounds.Bottom);
                    if (this._scrollRightBounds != rectangle)
                    {
                        this.OnScrollRightBoundsChanged(EventArgs.Empty);
                    }
                    if (this._scrollLeftBounds != rectangle2)
                    {
                        this.OnScrollLeftBoundsChanged(EventArgs.Empty);
                    }
                }
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual bool Active
        {
            get
            {
                return this._active;
            }
        }

        public Rectangle Bounds
        {
            get
            {
                return this.TabBounds;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public RibbonContext Context
        {
            get
            {
                return this._context;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual bool Contextual
        {
            get
            {
                return (this._context != null);
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Ribbon Owner
        {
            get
            {
                return this._owner;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public RibbonPanelCollection Panels
        {
            get
            {
                return this._panels;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual bool Pressed
        {
            get
            {
                return this._pressed;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public Rectangle ScrollLeftBounds
        {
            get
            {
                return this._scrollLeftBounds;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public bool ScrollLeftPressed
        {
            get
            {
                return this._scrollLeftPressed;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public bool ScrollLeftSelected
        {
            get
            {
                return this._scrollLeftSelected;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public bool ScrollLeftVisible
        {
            get
            {
                return this._scrollLeftVisible;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public Rectangle ScrollRightBounds
        {
            get
            {
                return this._scrollRightBounds;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public bool ScrollRightPressed
        {
            get
            {
                return this._scrollRightPressed;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public bool ScrollRightSelected
        {
            get
            {
                return this._scrollRightSelected;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public bool ScrollRightVisible
        {
            get
            {
                return this._scrollRightVisible;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public virtual bool Selected
        {
            get
            {
                return this._selected;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Rectangle TabBounds
        {
            get
            {
                return this._tabBounds;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public Rectangle TabContentBounds
        {
            get
            {
                return this._tabContentBounds;
            }
        }

        public object Tag
        {
            get
            {
                return this._tag;
            }
            set
            {
                this._tag = value;
            }
        }

        [Localizable(true)]
        public string Text
        {
            get
            {
                return this._text;
            }
            set
            {
                this._text = value;
                this.OnTextChanged(EventArgs.Empty);
                if (this.Owner != null)
                {
                    this.Owner.OnRegionsChanged();
                }
            }
        }
    }
}
