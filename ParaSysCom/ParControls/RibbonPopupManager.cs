﻿namespace ParControls
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    public static class RibbonPopupManager
    {
        private static List<RibbonPopup> pops = new List<RibbonPopup>();

        public static void Dismiss(DismissReason reason)
        {
            Dismiss(0, reason);
        }

        public static void Dismiss(RibbonPopup startPopup, DismissReason reason)
        {
            int index = pops.IndexOf(startPopup);
            if (index >= 0)
            {
                Dismiss(index, reason);
            }
        }

        private static void Dismiss(int startPopup, DismissReason reason)
        {
            for (int i = pops.Count - 1; i >= startPopup; i--)
            {
                pops[i].Close();
            }
        }

        public static void DismissChildren(RibbonPopup parent, DismissReason reason)
        {
            int index = pops.IndexOf(parent);
            if (index >= 0)
            {
                Dismiss((int) (index + 1), reason);
            }
        }

        internal static void FeedHookClick(MouseEventArgs e)
        {
            foreach (RibbonPopup popup in pops)
            {
                if (popup.WrappedDropDown.Bounds.Contains(e.Location))
                {
                    return;
                }
            }
            Dismiss(DismissReason.AppClicked);
        }

        internal static bool FeedMouseWheel(MouseEventArgs e)
        {
            RibbonDropDown lastPopup = LastPopup as RibbonDropDown;
            if (lastPopup != null)
            {
                foreach (RibbonItem item in lastPopup.Items)
                {
                    if (lastPopup.RectangleToScreen(item.Bounds).Contains(e.Location))
                    {
                        IScrollableRibbonItem item2 = item as IScrollableRibbonItem;
                        if (item2 != null)
                        {
                            if (e.Delta < 0)
                            {
                                item2.ScrollDown();
                            }
                            else
                            {
                                item2.ScrollUp();
                            }
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        internal static void Register(RibbonPopup p)
        {
            if (!pops.Contains(p))
            {
                pops.Add(p);
            }
        }

        internal static void Unregister(RibbonPopup p)
        {
            if (pops.Contains(p))
            {
                pops.Remove(p);
            }
        }

        internal static RibbonPopup LastPopup
        {
            get
            {
                if (pops.Count > 0)
                {
                    return pops[pops.Count - 1];
                }
                return null;
            }
        }

        public enum DismissReason
        {
            ItemClicked,
            AppClicked,
            NewPopup,
            AppFocusChanged,
            EscapePressed
        }
    }
}

