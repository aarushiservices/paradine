﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    public partial class RibbonColorChooser : RibbonButton
    {
        private System.Drawing.Color _color = System.Drawing.Color.Transparent;
        private int _imageColorHeight = 8;
        private int _smallImageColorHeight = 4;

        public event EventHandler ColorChanged;

        private Image CreateColorBmp(System.Drawing.Color c)
        {
            Bitmap image = new Bitmap(0x10, 0x10);
            using (Graphics graphics = Graphics.FromImage(image))
            {
                using (SolidBrush brush = new SolidBrush(c))
                {
                    graphics.FillRectangle(brush, new Rectangle(0, 0, 15, 15));
                }
                graphics.DrawRectangle(Pens.DimGray, new Rectangle(0, 0, 15, 15));
            }
            return image;
        }

        protected void OnColorChanged(EventArgs e)
        {
            if (this.ColorChanged != null)
            {
                this.ColorChanged(this, e);
            }
        }

        public override void OnPaint(object sender, RibbonElementPaintEventArgs e)
        {
            base.OnPaint(sender, e);
            System.Drawing.Color color = this.Color.Equals(System.Drawing.Color.Transparent) ? System.Drawing.Color.White : this.Color;
            int num = (e.Mode == RibbonElementSizeMode.Large) ? this.ImageColorHeight : this.SmallImageColorHeight;
            Rectangle rect = Rectangle.FromLTRB(base.ImageBounds.Left, base.ImageBounds.Bottom - num, base.ImageBounds.Right, base.ImageBounds.Bottom);
            SmoothingMode smoothingMode = e.Graphics.SmoothingMode;
            e.Graphics.SmoothingMode = SmoothingMode.None;
            using (SolidBrush brush = new SolidBrush(color))
            {
                e.Graphics.FillRectangle(brush, rect);
            }
            if (this.Color.Equals(System.Drawing.Color.Transparent))
            {
                e.Graphics.DrawRectangle(Pens.DimGray, rect);
            }
            e.Graphics.SmoothingMode = smoothingMode;
        }

        public System.Drawing.Color Color
        {
            get
            {
                return this._color;
            }
            set
            {
                this._color = value;
                this.RedrawItem();
                this.OnColorChanged(EventArgs.Empty);
            }
        }

        [DefaultValue(8), Description("Height of the color preview on the large image")]
        public int ImageColorHeight
        {
            get
            {
                return this._imageColorHeight;
            }
            set
            {
                this._imageColorHeight = value;
            }
        }

        [Description("Height of the color preview on the small image"), DefaultValue(4)]
        public int SmallImageColorHeight
        {
            get
            {
                return this._smallImageColorHeight;
            }
            set
            {
                this._smallImageColorHeight = value;
            }
        }
    }
}
