﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    [DesignTimeVisible(false)]
    abstract public partial class RibbonItem : Component, IRibbonElement
    {
        private string _altKey;
        private Rectangle _bounds;
        private Control _canvas;
        private bool _checked;
        private bool _enabled = true;
        private System.Drawing.Image _image;
        private Size _lastMeasureSize;
        private RibbonElementSizeMode _maxSize;
        private RibbonElementSizeMode _minSize;
        private Ribbon _owner;
        private RibbonItem _ownerItem;
        private RibbonPanel _ownerPanel;
        private RibbonTab _ownerTab;
        private bool _pressed;
        private bool _selected;
        private RibbonElementSizeMode _sizeMode;
        private object _tag;
        private string _text;
        private string _tooltip;
        private System.Drawing.Image _tooltipImage;
        private string _tooltipTitle;

        public event EventHandler CanvasChanged;

        public event EventHandler Click;

        public event EventHandler DoubleClick;

        public event MouseEventHandler MouseDown;

        public event MouseEventHandler MouseEnter;

        public event MouseEventHandler MouseLeave;

        public event MouseEventHandler MouseMove;

        public event MouseEventHandler MouseUp;

        public RibbonItem()
        {
            this.Click = (EventHandler)Delegate.Combine(this.Click, new EventHandler(this.RibbonItem_Click));
        }

        protected virtual bool ClosesDropDownAt(Point p)
        {
            return true;
        }

        protected RibbonElementSizeMode GetNearestSize(RibbonElementSizeMode sizeMode)
        {
            int num = (int)sizeMode;
            int maxSizeMode = (int)this.MaxSizeMode;
            int minSizeMode = (int)this.MinSizeMode;
            int num4 = (int)sizeMode;
            if ((maxSizeMode > 0) && (num > maxSizeMode))
            {
                num4 = maxSizeMode;
            }
            if ((minSizeMode > 0) && (num < minSizeMode))
            {
                num4 = minSizeMode;
            }
            return (RibbonElementSizeMode)num4;
        }

        public abstract Size MeasureSize(object sender, RibbonElementMeasureSizeEventArgs e);
        protected void NotifyOwnerRegionsChanged()
        {
            if (this.Owner != null)
            {
                if (this.Owner == this.Canvas)
                {
                    this.Owner.OnRegionsChanged();
                }
                else if (this.Canvas != null)
                {
                    if (this.Canvas is RibbonOrbDropDown)
                    {
                        (this.Canvas as RibbonOrbDropDown).OnRegionsChanged();
                    }
                    else
                    {
                        this.Canvas.Invalidate(this.Bounds);
                    }
                }
            }
        }

        public virtual void OnCanvasChanged(EventArgs e)
        {
            if (this.CanvasChanged != null)
            {
                this.CanvasChanged(this, e);
            }
        }

        public virtual void OnClick(EventArgs e)
        {
            if (this.Enabled && (this.Click != null))
            {
                this.Click(this, e);
            }
        }

        public virtual void OnDoubleClick(EventArgs e)
        {
            if (this.Enabled && (this.DoubleClick != null))
            {
                this.DoubleClick(this, e);
            }
        }

        public virtual void OnMouseDown(MouseEventArgs e)
        {
            if (this.Enabled)
            {
                if (this.MouseDown != null)
                {
                    this.MouseDown(this, e);
                }
                RibbonPopup canvas = this.Canvas as RibbonPopup;
                if (canvas != null)
                {
                    if (this.ClosesDropDownAt(e.Location))
                    {
                        RibbonPopupManager.Dismiss(RibbonPopupManager.DismissReason.ItemClicked);
                    }
                    this.OnClick(EventArgs.Empty);
                }
                this.SetPressed(true);
            }
        }

        public virtual void OnMouseEnter(MouseEventArgs e)
        {
            if (this.Enabled && (this.MouseEnter != null))
            {
                this.MouseEnter(this, e);
            }
        }

        public virtual void OnMouseLeave(MouseEventArgs e)
        {
            if (this.Enabled && (this.MouseLeave != null))
            {
                this.MouseLeave(this, e);
            }
        }

        public virtual void OnMouseMove(MouseEventArgs e)
        {
            if (this.Enabled && (this.MouseMove != null))
            {
                this.MouseMove(this, e);
            }
        }

        public virtual void OnMouseUp(MouseEventArgs e)
        {
            if (this.Enabled)
            {
                if (this.MouseUp != null)
                {
                    this.MouseUp(this, e);
                }
                if (this.Pressed)
                {
                    this.SetPressed(false);
                    this.RedrawItem();
                }
            }
        }

        public abstract void OnPaint(object sender, RibbonElementPaintEventArgs e);
        public virtual void RedrawItem()
        {
            if (this.Canvas != null)
            {
                this.Canvas.Invalidate(Rectangle.Inflate(this.Bounds, 1, 1));
            }
        }

        private void RibbonItem_Click(object sender, EventArgs e)
        {
            RibbonDropDown canvas = this.Canvas as RibbonDropDown;
            if ((canvas != null) && (canvas.SelectionService != null))
            {
                canvas.SelectionService.SetSelectedComponents(new Component[] { this }, SelectionTypes.Click);
            }
        }

        public virtual void SetBounds(Rectangle bounds)
        {
            this._bounds = bounds;
        }

        internal void SetCanvas(Control canvas)
        {
            this._canvas = canvas;
            this.SetCanvas(this as IContainsSelectableRibbonItems, canvas);
            this.OnCanvasChanged(EventArgs.Empty);
        }

        private void SetCanvas(IContainsSelectableRibbonItems parent, Control canvas)
        {
            if (parent != null)
            {
                foreach (RibbonItem item in parent.GetItems())
                {
                    item.SetCanvas(canvas);
                }
            }
        }

        protected void SetLastMeasuredSize(Size size)
        {
            this._lastMeasureSize = size;
        }

        internal virtual void SetOwner(Ribbon owner)
        {
            this._owner = owner;
        }

        internal virtual void SetOwnerGroup(RibbonItemGroup ownerGroup)
        {
            this._ownerItem = ownerGroup;
        }

        internal virtual void SetOwnerItem(RibbonItem item)
        {
            this._ownerItem = item;
        }

        internal virtual void SetOwnerPanel(RibbonPanel ownerPanel)
        {
            this._ownerPanel = ownerPanel;
        }

        internal virtual void SetOwnerTab(RibbonTab ownerTab)
        {
            this._ownerTab = ownerTab;
        }

        internal virtual void SetPressed(bool pressed)
        {
            this._pressed = pressed;
        }

        internal virtual void SetSelected(bool selected)
        {
            if (this.Enabled)
            {
                this._selected = selected;
            }
        }

        internal virtual void SetSizeMode(RibbonElementSizeMode sizeMode)
        {
            this._sizeMode = this.GetNearestSize(sizeMode);
        }

        [DefaultValue("")]
        public string AltKey
        {
            get
            {
                return this._altKey;
            }
            set
            {
                this._altKey = value;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Rectangle Bounds
        {
            get
            {
                return this._bounds;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public Control Canvas
        {
            get
            {
                if (!((this._canvas == null) || this._canvas.IsDisposed))
                {
                    return this._canvas;
                }
                return this.Owner;
            }
        }

        [DefaultValue(false)]
        public virtual bool Checked
        {
            get
            {
                return this._checked;
            }
            set
            {
                this._checked = value;
                this.NotifyOwnerRegionsChanged();
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual Rectangle ContentBounds
        {
            get
            {
                return Rectangle.FromLTRB(this.Bounds.Left + this.Owner.ItemMargin.Left, this.Bounds.Top + this.Owner.ItemMargin.Top, this.Bounds.Right - this.Owner.ItemMargin.Right, this.Bounds.Bottom - this.Owner.ItemMargin.Bottom);
            }
        }

        [DefaultValue(true)]
        public virtual bool Enabled
        {
            get
            {
                return this._enabled;
            }
            set
            {
                this._enabled = value;
                IContainsSelectableRibbonItems items = this as IContainsSelectableRibbonItems;
                if (items != null)
                {
                    foreach (RibbonItem item in items.GetItems())
                    {
                        item.Enabled = value;
                    }
                }
                this.NotifyOwnerRegionsChanged();
            }
        }

        public virtual System.Drawing.Image Image
        {
            get
            {
                return this._image;
            }
            set
            {
                this._image = value;
                this.NotifyOwnerRegionsChanged();
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public Size LastMeasuredSize
        {
            get
            {
                return this._lastMeasureSize;
            }
        }

        [DefaultValue(0)]
        public RibbonElementSizeMode MaxSizeMode
        {
            get
            {
                return this._maxSize;
            }
            set
            {
                this._maxSize = value;
                this.NotifyOwnerRegionsChanged();
            }
        }

        [DefaultValue(0)]
        public RibbonElementSizeMode MinSizeMode
        {
            get
            {
                return this._minSize;
            }
            set
            {
                this._minSize = value;
                this.NotifyOwnerRegionsChanged();
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Ribbon Owner
        {
            get
            {
                return this._owner;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public RibbonItem OwnerItem
        {
            get
            {
                return this._ownerItem;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public RibbonPanel OwnerPanel
        {
            get
            {
                return this._ownerPanel;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public RibbonTab OwnerTab
        {
            get
            {
                return this._ownerTab;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public virtual bool Pressed
        {
            get
            {
                return this._pressed;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public virtual bool Selected
        {
            get
            {
                return this._selected;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public RibbonElementSizeMode SizeMode
        {
            get
            {
                return this._sizeMode;
            }
        }

        public object Tag
        {
            get
            {
                return this._tag;
            }
            set
            {
                this._tag = value;
            }
        }

        [DefaultValue(""), Localizable(true)]
        public virtual string Text
        {
            get
            {
                return this._text;
            }
            set
            {
                this._text = value;
                this.NotifyOwnerRegionsChanged();
            }
        }

        [Localizable(true), DefaultValue("")]
        public virtual string ToolTip
        {
            get
            {
                return this._tooltip;
            }
            set
            {
                this._tooltip = value;
            }
        }

        [DefaultValue("")]
        public System.Drawing.Image ToolTipImage
        {
            get
            {
                return this._tooltipImage;
            }
            set
            {
                this._tooltipImage = value;
            }
        }

        [DefaultValue("")]
        public string ToolTipTitle
        {
            get
            {
                return this._tooltipTitle;
            }
            set
            {
                this._tooltipTitle = value;
            }
        }
    }
}
