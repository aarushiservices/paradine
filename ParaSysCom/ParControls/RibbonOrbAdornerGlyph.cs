﻿namespace ParControls
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Windows.Forms.Design.Behavior;

    public class RibbonOrbAdornerGlyph : Glyph
    {
        private BehaviorService _behaviorService;
        private RibbonDesigner _componentDesigner;
        private bool _menuVisible;
        private Ribbon _ribbon;

        public RibbonOrbAdornerGlyph(BehaviorService behaviorService, RibbonDesigner designer, Ribbon ribbon) : base(new RibbonOrbAdornerGlyphBehavior())
        {
            this._behaviorService = behaviorService;
            this._componentDesigner = designer;
            this._ribbon = ribbon;
        }

        public override Cursor GetHitTest(Point p)
        {
            if (this.Bounds.Contains(p))
            {
                return Cursors.Hand;
            }
            return null;
        }

        public override void Paint(PaintEventArgs pe)
        {
        }

        public override Rectangle Bounds
        {
            get
            {
                Point point = this._behaviorService.ControlToAdornerWindow(this._ribbon);
                return new Rectangle(point.X + this._ribbon.OrbBounds.Left, point.Y + this._ribbon.OrbBounds.Top, this._ribbon.OrbBounds.Height, this._ribbon.OrbBounds.Height);
            }
        }

        public bool MenuVisible
        {
            get
            {
                return this._menuVisible;
            }
            set
            {
                this._menuVisible = value;
            }
        }
    }
}

