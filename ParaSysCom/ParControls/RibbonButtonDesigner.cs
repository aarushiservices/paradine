﻿namespace ParControls
{
    using System;

    internal class RibbonButtonDesigner : RibbonElementWithItemCollectionDesigner
    {
        protected override void AddButton(object sender, EventArgs e)
        {
            base.AddButton(sender, e);
        }

        public override RibbonItemCollection Collection
        {
            get
            {
                if (base.Component is RibbonButton)
                {
                    return (base.Component as RibbonButton).DropDownItems;
                }
                return null;
            }
        }

        public override ParControls.Ribbon Ribbon
        {
            get
            {
                if (base.Component is RibbonButton)
                {
                    return (base.Component as RibbonButton).Owner;
                }
                return null;
            }
        }
    }
}

