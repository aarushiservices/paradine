﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    public partial class RibbonOrbDropDown : RibbonPopup
    {
        private Padding _contentMargin;
        private RibbonItemCollection _menuItems;
        private RibbonItemCollection _optionItems;
        private int _optionsPadding;
        private RibbonItemCollection _recentItems;
        private ParControls.Ribbon _ribbon;
        private RibbonMouseSensor _sensor;
        private Rectangle designerSelectedBounds;
        private int glyphGap = 3;
        internal RibbonOrbMenuItem LastPoppedMenuItem;
        internal RibbonOrbDropDown(ParControls.Ribbon ribbon)
        {
            this.DoubleBuffered = true;
            this._ribbon = ribbon;
            this._menuItems = new RibbonItemCollection();
            this._recentItems = new RibbonItemCollection();
            this._optionItems = new RibbonItemCollection();
            this._menuItems.SetOwner(this.Ribbon);
            this._recentItems.SetOwner(this.Ribbon);
            this._optionItems.SetOwner(this.Ribbon);
            this._optionsPadding = 6;
            base.Size = new Size(0x20f, 0x1bf);
            base.BorderRoundness = 8;
        }

        ~RibbonOrbDropDown()
        {
            if (this._sensor != null)
            {
                this._sensor.Dispose();
            }
        }

        internal void HandleDesignerItemRemoved(RibbonItem item)
        {
            if (this.MenuItems.Contains(item))
            {
                this.MenuItems.Remove(item);
            }
            else if (this.RecentItems.Contains(item))
            {
                this.RecentItems.Remove(item);
            }
            else if (this.OptionItems.Contains(item))
            {
                this.OptionItems.Remove(item);
            }
            this.OnRegionsChanged();
        }

        protected override void OnClosed(EventArgs e)
        {
            this.Ribbon.OrbPressed = false;
            this.Ribbon.OrbSelected = false;
            this.LastPoppedMenuItem = null;
            base.OnClosed(e);
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
        }

        protected override void OnMouseDoubleClick(MouseEventArgs e)
        {
            base.OnMouseDoubleClick(e);
            if (this.Ribbon.RectangleToScreen(this.Ribbon.OrbBounds).Contains(base.PointToScreen(e.Location)))
            {
                this.Ribbon.OnOrbDoubleClicked(EventArgs.Empty);
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (this.RibbonInDesignMode)
            {
                if (this.ContentBounds.Contains(e.Location))
                {
                    if (this.ContentButtonsBounds.Contains(e.Location))
                    {
                        foreach (RibbonItem item in this.MenuItems)
                        {
                            if (item.Bounds.Contains(e.Location))
                            {
                                this.SelectOnDesigner(item);
                                break;
                            }
                        }
                    }
                    else if (this.ContentRecentItemsBounds.Contains(e.Location))
                    {
                        foreach (RibbonItem item in this.RecentItems)
                        {
                            if (item.Bounds.Contains(e.Location))
                            {
                                this.SelectOnDesigner(item);
                                break;
                            }
                        }
                    }
                }
                if (this.ButtonsGlyphBounds.Contains(e.Location))
                {
                    RibbonDesigner.Current.CreteOrbMenuItem(typeof(RibbonOrbMenuItem));
                }
                else if (this.ButtonsSeparatorGlyphBounds.Contains(e.Location))
                {
                    RibbonDesigner.Current.CreteOrbMenuItem(typeof(RibbonSeparator));
                }
                else if (this.RecentGlyphBounds.Contains(e.Location))
                {
                    RibbonDesigner.Current.CreteOrbRecentItem(typeof(RibbonOrbRecentItem));
                }
                else if (this.OptionGlyphBounds.Contains(e.Location))
                {
                    RibbonDesigner.Current.CreteOrbOptionItem(typeof(RibbonOrbOptionButton));
                }
                else
                {
                    foreach (RibbonItem item in this.OptionItems)
                    {
                        if (item.Bounds.Contains(e.Location))
                        {
                            this.SelectOnDesigner(item);
                            break;
                        }
                    }
                }
            }
        }

        protected override void OnOpening(CancelEventArgs e)
        {
            base.OnOpening(e);
            this.UpdateRegions();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            this.Ribbon.Renderer.OnRenderOrbDropDownBackground(new RibbonOrbDropDownEventArgs(this.Ribbon, this, e.Graphics, e.ClipRectangle));
            foreach (RibbonItem item in this.AllItems)
            {
                item.OnPaint(this, new RibbonElementPaintEventArgs(e.ClipRectangle, e.Graphics, RibbonElementSizeMode.DropDown));
            }
            if (this.RibbonInDesignMode)
            {
                using (SolidBrush brush = new SolidBrush(Color.FromArgb(50, Color.Blue)))
                {
                    e.Graphics.FillRectangle(brush, this.ButtonsGlyphBounds);
                    e.Graphics.FillRectangle(brush, this.RecentGlyphBounds);
                    e.Graphics.FillRectangle(brush, this.OptionGlyphBounds);
                    e.Graphics.FillRectangle(brush, this.ButtonsSeparatorGlyphBounds);
                }
                using (StringFormat format = new StringFormat())
                {
                    format.Alignment = StringAlignment.Center;
                    format.LineAlignment = StringAlignment.Center;
                    format.Trimming = StringTrimming.None;
                    e.Graphics.DrawString("+", this.Font, Brushes.White, this.ButtonsGlyphBounds, format);
                    e.Graphics.DrawString("+", this.Font, Brushes.White, this.RecentGlyphBounds, format);
                    e.Graphics.DrawString("+", this.Font, Brushes.White, this.OptionGlyphBounds, format);
                    e.Graphics.DrawString("---", this.Font, Brushes.White, this.ButtonsSeparatorGlyphBounds, format);
                }
                using (Pen pen = new Pen(Color.Black))
                {
                    pen.DashStyle = DashStyle.Dot;
                    e.Graphics.DrawRectangle(pen, this.designerSelectedBounds);
                }
            }
        }

        internal void OnRegionsChanged()
        {
            this.UpdateRegions();
            this.UpdateSensor();
            this.UpdateDesignerSelectedBounds();
            base.Invalidate();
        }

        protected override void OnShowed(EventArgs e)
        {
            base.OnShowed(e);
            this.UpdateSensor();
        }

        internal void SelectOnDesigner(RibbonItem item)
        {
            if (RibbonDesigner.Current != null)
            {
                RibbonDesigner.Current.SelectedElement = item;
                this.UpdateDesignerSelectedBounds();
                base.Invalidate();
            }
        }

        private int SeparatorHeight(RibbonSeparator s)
        {
            if (!string.IsNullOrEmpty(s.Text))
            {
                return 20;
            }
            return 3;
        }

        internal void UpdateDesignerSelectedBounds()
        {
            this.designerSelectedBounds = Rectangle.Empty;
            if (this.RibbonInDesignMode)
            {
                RibbonItem selectedElement = RibbonDesigner.Current.SelectedElement as RibbonItem;
                if ((selectedElement != null) && this.AllItems.Contains(selectedElement))
                {
                    this.designerSelectedBounds = selectedElement.Bounds;
                }
            }
        }

        private void UpdateRegions()
        {
            Rectangle rectangle4;
            int y = 0;
            int num2 = 0;
            int height = 0x2c;
            int num4 = 0x16;
            Rectangle contentBounds = this.ContentBounds;
            Rectangle contentButtonsBounds = this.ContentButtonsBounds;
            Rectangle contentRecentItemsBounds = this.ContentRecentItemsBounds;
            int num5 = 1;
            int num6 = 1;
            int num7 = 0;
            int num8 = 0;
            foreach (RibbonItem item in this.AllItems)
            {
                item.SetSizeMode(RibbonElementSizeMode.DropDown);
                item.SetCanvas(this);
            }
            y = contentBounds.Top + 1;
            foreach (RibbonItem item in this.MenuItems)
            {
                rectangle4 = new Rectangle(contentButtonsBounds.Left + num5, y, contentButtonsBounds.Width - (num5 * 2), height);
                if (item is RibbonSeparator)
                {
                    rectangle4.Height = this.SeparatorHeight(item as RibbonSeparator);
                }
                item.SetBounds(rectangle4);
                y += rectangle4.Height;
            }
            num7 = (y - contentBounds.Top) + 1;
            y = contentButtonsBounds.Top;
            foreach (RibbonItem item in this.RecentItems)
            {
                rectangle4 = new Rectangle(contentRecentItemsBounds.Left + num6, y, contentRecentItemsBounds.Width - (num6 * 2), num4);
                if (item is RibbonSeparator)
                {
                    rectangle4.Height = this.SeparatorHeight(item as RibbonSeparator);
                }
                item.SetBounds(rectangle4);
                y += rectangle4.Height;
            }
            num8 = y - contentButtonsBounds.Top;
            int num9 = Math.Max(num7, num8);
            if (RibbonDesigner.Current != null)
            {
                num9 += this.ButtonsGlyphBounds.Height + (this.glyphGap * 2);
            }
            base.Height = num9 + this.ContentMargin.Vertical;
            contentBounds = this.ContentBounds;
            num2 = base.ClientSize.Width - this.ContentMargin.Right;
            using (Graphics graphics = base.CreateGraphics())
            {
                foreach (RibbonItem item in this.OptionItems)
                {
                    Size size = item.MeasureSize(this, new RibbonElementMeasureSizeEventArgs(graphics, RibbonElementSizeMode.DropDown));
                    y = contentBounds.Bottom + ((this.ContentMargin.Bottom - size.Height) / 2);
                    item.SetBounds(new Rectangle(new Point(num2 - size.Width, y), size));
                    num2 = item.Bounds.Left - this.OptionItemsPadding;
                }
            }
        }

        private void UpdateSensor()
        {
            if (this._sensor != null)
            {
                this._sensor.Dispose();
            }
            this._sensor = new RibbonMouseSensor(this, this.Ribbon, this.AllItems);
        }

        internal List<RibbonItem> AllItems
        {
            get
            {
                List<RibbonItem> list = new List<RibbonItem>();
                list.AddRange(this.MenuItems);
                list.AddRange(this.RecentItems);
                list.AddRange(this.OptionItems);
                return list;
            }
        }

        internal Rectangle ButtonsGlyphBounds
        {
            get
            {
                Size size = new Size(50, 0x12);
                Rectangle contentButtonsBounds = this.ContentButtonsBounds;
                Rectangle rectangle2 = new Rectangle(contentButtonsBounds.Left + ((contentButtonsBounds.Width - (size.Width * 2)) / 2), contentButtonsBounds.Top + this.glyphGap, size.Width, size.Height);
                if (this.MenuItems.Count > 0)
                {
                    rectangle2.Y = this.MenuItems[this.MenuItems.Count - 1].Bounds.Bottom + this.glyphGap;
                }
                return rectangle2;
            }
        }

        internal Rectangle ButtonsSeparatorGlyphBounds
        {
            get
            {
                Size size = new Size(0x12, 0x12);
                Rectangle buttonsGlyphBounds = this.ButtonsGlyphBounds;
                buttonsGlyphBounds.X = buttonsGlyphBounds.Right + this.glyphGap;
                return buttonsGlyphBounds;
            }
        }

        [Browsable(false)]
        public Rectangle ContentBounds
        {
            get
            {
                return Rectangle.FromLTRB(this.ContentMargin.Left, this.ContentMargin.Top, base.ClientRectangle.Right - this.ContentMargin.Right, base.ClientRectangle.Bottom - this.ContentMargin.Bottom);
            }
        }

        [Browsable(false)]
        public Rectangle ContentButtonsBounds
        {
            get
            {
                Rectangle contentBounds = this.ContentBounds;
                contentBounds.Width = 150;
                return contentBounds;
            }
        }

        [Browsable(false)]
        public Padding ContentMargin
        {
            get
            {
                if (this._contentMargin.Size.IsEmpty)
                {
                    this._contentMargin = new Padding(6, 0x11, 6, 0x1d);
                }
                return this._contentMargin;
            }
        }

        [Browsable(false)]
        public Rectangle ContentRecentItemsBounds
        {
            get
            {
                Rectangle contentBounds = this.ContentBounds;
                contentBounds.Width -= 150;
                contentBounds.X += 150;
                return contentBounds;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public RibbonItemCollection MenuItems
        {
            get
            {
                return this._menuItems;
            }
        }

        internal Rectangle OptionGlyphBounds
        {
            get
            {
                Size size = new Size(50, 0x12);
                Rectangle contentBounds = this.ContentBounds;
                Rectangle rectangle2 = new Rectangle(contentBounds.Right - size.Width, contentBounds.Bottom + this.glyphGap, size.Width, size.Height);
                if (this.OptionItems.Count > 0)
                {
                    rectangle2.X = (this.OptionItems[this.OptionItems.Count - 1].Bounds.Left - size.Width) - this.glyphGap;
                }
                return rectangle2;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public RibbonItemCollection OptionItems
        {
            get
            {
                return this._optionItems;
            }
        }

        [Description("Spacing between option buttons (those on the bottom)"), DefaultValue(6)]
        public int OptionItemsPadding
        {
            get
            {
                return this._optionsPadding;
            }
            set
            {
                this._optionsPadding = value;
            }
        }

        internal Rectangle RecentGlyphBounds
        {
            get
            {
                Size size = new Size(50, 0x12);
                Rectangle contentRecentItemsBounds = this.ContentRecentItemsBounds;
                Rectangle rectangle2 = new Rectangle(contentRecentItemsBounds.Left + this.glyphGap, contentRecentItemsBounds.Top + this.glyphGap, size.Width, size.Height);
                if (this.RecentItems.Count > 0)
                {
                    rectangle2.Y = this.RecentItems[this.RecentItems.Count - 1].Bounds.Bottom + this.glyphGap;
                }
                return rectangle2;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public RibbonItemCollection RecentItems
        {
            get
            {
                return this._recentItems;
            }
        }

        [Browsable(false)]
        public ParControls.Ribbon Ribbon
        {
            get
            {
                return this._ribbon;
            }
        }

        private bool RibbonInDesignMode
        {
            get
            {
                return (RibbonDesigner.Current != null);
            }
        }

        [Browsable(false)]
        public RibbonMouseSensor Sensor
        {
            get
            {
                return this._sensor;
            }
        }
    }
}
