﻿namespace ParControls
{
    using System;
    using System.Windows.Forms;
    using System.Windows.Forms.Design.Behavior;

    public class RibbonPanelGlyphBehavior : System.Windows.Forms.Design.Behavior.Behavior
    {
        private RibbonTabDesigner _designer;
        private RibbonTab _tab;

        public RibbonPanelGlyphBehavior(RibbonTabDesigner designer, RibbonTab tab)
        {
            this._designer = designer;
            this._tab = tab;
        }

        public override bool OnMouseUp(Glyph g, MouseButtons button)
        {
            this._designer.AddPanel(this, EventArgs.Empty);
            return base.OnMouseUp(g, button);
        }
    }
}

