﻿using ParaSysCom;
using ParControls.RibbonHelpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    [Designer(typeof(RibbonDesigner))]
    public partial class Ribbon : Control
    {        
        public event EventHandler ActiveTabChanged;

        public event EventHandler ActualBorderModeChanged;

        public event EventHandler CaptionButtonsVisibleChanged;

        public event EventHandler OrbClicked;

        public event EventHandler OrbDoubleClick;


        public Ribbon()
        {
            base.SetStyle(ControlStyles.ResizeRedraw, true);
            base.SetStyle(ControlStyles.Selectable, false);
            base.SetStyle(ControlStyles.UserPaint, true);
            base.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.Dock = DockStyle.Top;
            this._tabs = new RibbonTabCollection(this);
            this._contexts = new RibbonContextCollection(this);
            this._tabsMargin = new Padding(12, 0x1a, 20, 0);
            this._tabTextMargin = new Padding(4, 2, 4, 2);
            this._tabsPadding = new Padding(8, 5, 8, 3);
            this._tabContentMargin = new Padding(1, 0, 1, 2);
            this._panelPadding = new Padding(3);
            this._panelMargin = new Padding(3, 2, 3, 15);
            this._panelSpacing = 3;
            this._itemPadding = new Padding(1, 0, 1, 0);
            this._itemMargin = new Padding(4, 2, 4, 2);
            this._tabSpacing = 6;
            this._dropDownMargin = new Padding(2);
            this._renderer = new RibbonProfessionalRenderer();
            this._orbVisible = true;
            this._orbDropDown = new RibbonOrbDropDown(this);
            this._quickAcessToolbar = new RibbonQuickAccessToolbar(this);
            this._quickAcessVisible = true;
            this._MinimizeButton = new RibbonCaptionButton(RibbonCaptionButton.CaptionButton.Minimize);
            this._MaximizeRestoreButton = new RibbonCaptionButton(RibbonCaptionButton.CaptionButton.Maximize);
            this._CloseButton = new RibbonCaptionButton(RibbonCaptionButton.CaptionButton.Close);
            this._MinimizeButton.SetOwner(this);
            this._MaximizeRestoreButton.SetOwner(this);
            this._CloseButton.SetOwner(this);
            this.Font = SystemFonts.CaptionFont;
            this.BorderMode = RibbonWindowMode.NonClientAreaGlass;
            base.Disposed += new EventHandler(this.Ribbon_Disposed);
        }

        private void _keyboardHook_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                RibbonPopupManager.Dismiss(RibbonPopupManager.DismissReason.EscapePressed);
            }
        }

        private void _mouseHook_MouseDown(object sender, MouseEventArgs e)
        {
            if (!base.RectangleToScreen(this.OrbBounds).Contains(e.Location))
            {
                RibbonPopupManager.FeedHookClick(e);
            }
        }

        private void _mouseHook_MouseWheel(object sender, MouseEventArgs e)
        {
            if (!RibbonPopupManager.FeedMouseWheel(e) && base.RectangleToScreen(new Rectangle(Point.Empty, base.Size)).Contains(e.Location))
            {
                this.OnMouseWheel(e);
            }
        }

        public void ActivateNextTab()
        {
            RibbonTab nextTab = this.NextTab;
            if (nextTab != null)
            {
                this.ActiveTab = nextTab;
            }
        }

        public void ActivatePreviousTab()
        {
            RibbonTab previousTab = this.PreviousTab;
            if (previousTab != null)
            {
                this.ActiveTab = previousTab;
            }
        }

        ~Ribbon()
        {
            if (this._mouseHook != null)
            {
                this._mouseHook.Dispose();
            }
        }

        protected virtual void OnActiveTabChanged(EventArgs e)
        {
            if (this.ActiveTabChanged != null)
            {
                this.ActiveTabChanged(this, e);
            }
        }

        protected virtual void OnActualBorderModeChanged(EventArgs e)
        {
            if (this.ActualBorderModeChanged != null)
            {
                this.ActualBorderModeChanged(this, e);
            }
        }

        protected virtual void OnCaptionButtonsVisibleChanged(EventArgs e)
        {
            if (this.CaptionButtonsVisibleChanged != null)
            {
                this.CaptionButtonsVisibleChanged(this, e);
            }
        }

        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);
        }

        protected override void OnMouseDoubleClick(MouseEventArgs e)
        {
            base.OnMouseDoubleClick(e);
            if (this.OrbBounds.Contains(e.Location))
            {
                this.OnOrbDoubleClicked(EventArgs.Empty);
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (this.OrbBounds.Contains(e.Location))
            {
                this.OrbMouseDown();
            }
            else
            {
                this.TabHitTest(e.X, e.Y);
            }
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (this.ActiveTab != null)
            {
                bool flag = false;
                if (!this.ActiveTab.TabContentBounds.Contains(e.X, e.Y))
                {
                    if (!((!this.OrbVisible || !this.OrbBounds.Contains(e.Location)) || this.OrbSelected))
                    {
                        this.OrbSelected = true;
                        base.Invalidate(this.OrbBounds);
                    }
                    else if (!this.QuickAccessVisible || !this.QuickAcessToolbar.Bounds.Contains(e.Location))
                    {
                        foreach (RibbonTab tab in this.Tabs)
                        {
                            if (tab.TabBounds.Contains(e.X, e.Y))
                            {
                                this.SetSelectedTab(tab);
                                flag = true;
                            }
                        }
                    }
                }
                if (!flag)
                {
                    this.SetSelectedTab(null);
                }
                if (!(!this.OrbSelected || this.OrbBounds.Contains(e.Location)))
                {
                    this.OrbSelected = false;
                    base.Invalidate(this.OrbBounds);
                }
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            base.OnMouseWheel(e);
            if ((this.Tabs.Count != 0) && (this.ActiveTab != null))
            {
                int index = this.Tabs.IndexOf(this.ActiveTab);
                if (e.Delta < 0)
                {
                    this._tabSum += 0.4f;
                }
                else
                {
                    this._tabSum -= 0.4f;
                }
                int num2 = Convert.ToInt16(Math.Round((double)this._tabSum));
                if (num2 != 0)
                {
                    index += num2;
                    if (index < 0)
                    {
                        index = 0;
                    }
                    else if (index >= (this.Tabs.Count - 1))
                    {
                        index = this.Tabs.Count - 1;
                    }
                    this.ActiveTab = this.Tabs[index];
                    this._tabSum = 0f;
                }
            }
        }

        internal virtual void OnOrbClicked(EventArgs e)
        {
            if (this.OrbPressed)
            {
                this.OrbDropDown.Close();
            }
            if (this.OrbClicked != null)
            {
                this.OrbClicked(this, e);
            }
        }

        internal virtual void OnOrbDoubleClicked(EventArgs e)
        {
            if (this.OrbDoubleClick != null)
            {
                this.OrbDoubleClick(this, e);
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (!this._updatingSuspended)
            {
                if (base.Size != this._lastSizeMeasured)
                {
                    this.UpdateRegions(e.Graphics);
                }
                this.PaintOn(e.Graphics, e.ClipRectangle);
            }
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            if (base.DesignMode && !GlobalVariables.CodeLicensed)
            {
                Application.Exit();
            }
        }

        protected override void OnParentChanged(EventArgs e)
        {
            base.OnParentChanged(e);
            if ((this.Site == null) || !this.Site.DesignMode)
            {
                this.BorderMode = this.BorderMode;
                if (base.Parent is IRibbonForm)
                {
                    this.FormHelper.Ribbon = this;
                }
                this.SetUpHooks();
            }
        }

        internal void OnRegionsChanged()
        {
            if (!this._updatingSuspended)
            {
                if (this.Tabs.Count == 1)
                {
                    this.ActiveTab = this.Tabs[0];
                }
                this._lastSizeMeasured = Size.Empty;
                this.Refresh();
            }
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            this.UpdateRegions();
            this.RemoveHelperControls();
            base.OnSizeChanged(e);
        }

        internal void OrbMouseDown()
        {
            this.OnOrbClicked(EventArgs.Empty);
        }

        private void PaintDoubleBuffered(Graphics wndGraphics, Rectangle clip)
        {
            using (Bitmap bitmap = new Bitmap(base.Width, base.Height))
            {
                using (Graphics graphics = Graphics.FromImage(bitmap))
                {
                    graphics.Clear(Color.Black);
                    this.PaintOn(graphics, clip);
                    graphics.Flush();
                    WinApi.BitBlt(wndGraphics.GetHdc(), clip.X, clip.Y, clip.Width, clip.Height, graphics.GetHdc(), clip.X, clip.Y, 0xcc0020);
                }
            }
        }

        private void PaintOn(Graphics g, Rectangle clip)
        {
            if (WinApi.IsWindows && (Environment.OSVersion.Platform == PlatformID.Win32NT))
            {
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
            }
            this.Renderer.OnRenderRibbonBackground(new RibbonRenderEventArgs(this, g, clip));
            this.Renderer.OnRenderRibbonCaptionBar(new RibbonRenderEventArgs(this, g, clip));
            if (this.CaptionButtonsVisible)
            {
                this.MinimizeButton.OnPaint(this, new RibbonElementPaintEventArgs(clip, g, RibbonElementSizeMode.Medium));
                this.MaximizeRestoreButton.OnPaint(this, new RibbonElementPaintEventArgs(clip, g, RibbonElementSizeMode.Medium));
                this.CloseButton.OnPaint(this, new RibbonElementPaintEventArgs(clip, g, RibbonElementSizeMode.Medium));
            }
            this.Renderer.OnRenderRibbonOrb(new RibbonRenderEventArgs(this, g, clip));
            this.QuickAcessToolbar.OnPaint(this, new RibbonElementPaintEventArgs(clip, g, RibbonElementSizeMode.Compact));
            foreach (RibbonTab tab in this.Tabs)
            {
                tab.OnPaint(this, new RibbonElementPaintEventArgs(tab.TabBounds, g, RibbonElementSizeMode.None, this));
            }
        }

        public void RedrawArea(Rectangle area)
        {
            this.Sensor.Control.Invalidate(area);
        }

        private void RedrawTab(RibbonTab tab)
        {
            using (Graphics graphics = base.CreateGraphics())
            {
                Rectangle rect = Rectangle.FromLTRB(tab.TabBounds.Left, tab.TabBounds.Top, tab.TabBounds.Right, tab.TabBounds.Bottom);
                graphics.SetClip(rect);
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
                tab.OnPaint(this, new RibbonElementPaintEventArgs(tab.TabBounds, graphics, RibbonElementSizeMode.None));
            }
        }

        private void RemoveHelperControls()
        {
            RibbonPopupManager.Dismiss(RibbonPopupManager.DismissReason.AppClicked);
            while (base.Controls.Count > 0)
            {
                Control control = base.Controls[0];
                control.Visible = false;
                base.Controls.Remove(control);
            }
        }

        private void RenewSensor()
        {
            if (this.ActiveTab != null)
            {
                if (this.Sensor != null)
                {
                    this.Sensor.Dispose();
                }
                this._sensor = new RibbonMouseSensor(this, this, this.ActiveTab);
                if (this.CaptionButtonsVisible)
                {
                    this.Sensor.Items.AddRange(new RibbonItem[] { this.CloseButton, this.MaximizeRestoreButton, this.MinimizeButton });
                }
            }
        }

        internal void ResumeSensor()
        {
            this.Sensor.Resume();
        }

        public void ResumeUpdating()
        {
            this.ResumeUpdating(true);
        }

        public void ResumeUpdating(bool update)
        {
            this._updatingSuspended = false;
            if (update)
            {
                this.OnRegionsChanged();
            }
        }

        private void Ribbon_Disposed(object sender, EventArgs e)
        {
            if (this._mouseHook != null)
            {
                this._mouseHook.Dispose();
            }
            if (this._keyboardHook != null)
            {
                this._keyboardHook.Dispose();
            }
        }

        private void SetActualBorderMode(RibbonWindowMode borderMode)
        {
            bool flag = this._actualBorderMode != borderMode;
            this._actualBorderMode = borderMode;
            if (flag)
            {
                this.OnActualBorderModeChanged(EventArgs.Empty);
            }
            this.SetCaptionButtonsVisible(borderMode == RibbonWindowMode.NonClientAreaCustomDrawn);
        }

        private void SetCaptionButtonsVisible(bool visible)
        {
            bool flag = this._CaptionButtonsVisible != visible;
            this._CaptionButtonsVisible = visible;
            if (flag)
            {
                this.OnCaptionButtonsVisibleChanged(EventArgs.Empty);
            }
        }

        private void SetSelectedTab(RibbonTab tab)
        {
            if (tab != this._lastSelectedTab)
            {
                if (this._lastSelectedTab != null)
                {
                    this._lastSelectedTab.SetSelected(false);
                    this.RedrawTab(this._lastSelectedTab);
                }
                if (tab != null)
                {
                    tab.SetSelected(true);
                    this.RedrawTab(tab);
                }
                this._lastSelectedTab = tab;
            }
        }

        private void SetUpHooks()
        {
            if (!((this.Site != null) && this.Site.DesignMode))
            {
                this._mouseHook = new GlobalHook(GlobalHook.HookTypes.Mouse);
                this._mouseHook.MouseWheel += new MouseEventHandler(this._mouseHook_MouseWheel);
                this._mouseHook.MouseDown += new MouseEventHandler(this._mouseHook_MouseDown);
                this._keyboardHook = new GlobalHook(GlobalHook.HookTypes.Keyboard);
                this._keyboardHook.KeyDown += new KeyEventHandler(this._keyboardHook_KeyDown);
            }
        }

        public void ShowOrbDropDown()
        {
            this.OrbPressed = true;
            this.OrbDropDown.Show(base.PointToScreen(new Point(this.OrbBounds.X - 4, (this.OrbBounds.Bottom - this.OrbDropDown.ContentMargin.Top) + 2)));
        }

        internal void SuspendSensor()
        {
            if (this.Sensor != null)
            {
                this.Sensor.Suspend();
            }
        }

        public void SuspendUpdating()
        {
            this._updatingSuspended = true;
        }

        internal bool TabHitTest(int x, int y)
        {
            if (Rectangle.FromLTRB(base.Right - 10, base.Bottom - 10, base.Right, base.Bottom).Contains(x, y))
            {
                MessageBox.Show(this.cr);
            }
            foreach (RibbonTab tab in this.Tabs)
            {
                if (tab.TabBounds.Contains(x, y))
                {
                    this.ActiveTab = tab;
                    return true;
                }
            }
            return false;
        }

        internal void UpdateRegions()
        {
            this.UpdateRegions(null);
        }

        internal void UpdateRegions(Graphics g)
        {
            bool flag = false;
            if (!base.IsDisposed && !this._updatingSuspended)
            {
                if (g == null)
                {
                    g = base.CreateGraphics();
                    flag = true;
                }
                int x = this.TabsMargin.Left + this.OrbBounds.Width;
                int num2 = 0;
                int num3 = 0;
                foreach (RibbonTab tab in this.Tabs)
                {
                    Size size = tab.MeasureSize(this, new RibbonElementMeasureSizeEventArgs(g, RibbonElementSizeMode.None));
                    Rectangle tabBounds = new Rectangle(x, this.TabsMargin.Top, (this.TabsPadding.Left + size.Width) + this.TabsPadding.Right, (this.TabsPadding.Top + size.Height) + this.TabsPadding.Bottom);
                    tab.SetTabBounds(tabBounds);
                    x = tabBounds.Right + this.TabSpacing;
                    num2 = Math.Max(tabBounds.Width, num2);
                    num3 = Math.Max(tabBounds.Bottom, num3);
                    tab.SetTabContentBounds(Rectangle.FromLTRB(this.TabContentMargin.Left, num3 + this.TabContentMargin.Top, (base.ClientSize.Width - 1) - this.TabContentMargin.Right, (base.ClientSize.Height - 1) - this.TabContentMargin.Bottom));
                    if (tab.Active)
                    {
                        tab.UpdatePanelsRegions();
                    }
                }
                while ((x > base.ClientRectangle.Right) && (num2 > 0))
                {
                    x = this.TabsMargin.Left + this.OrbBounds.Width;
                    num2--;
                    foreach (RibbonTab tab in this.Tabs)
                    {
                        if (tab.TabBounds.Width >= num2)
                        {
                            tab.SetTabBounds(new Rectangle(x, this.TabsMargin.Top, num2, tab.TabBounds.Height));
                        }
                        else
                        {
                            tab.SetTabBounds(new Rectangle(new Point(x, this.TabsMargin.Top), tab.TabBounds.Size));
                        }
                        x = tab.TabBounds.Right + this.TabSpacing;
                    }
                }
                this.QuickAcessToolbar.MeasureSize(this, new RibbonElementMeasureSizeEventArgs(g, RibbonElementSizeMode.Compact));
                this.QuickAcessToolbar.SetBounds(new Rectangle(new Point(this.OrbBounds.Right + this.QuickAcessToolbar.Margin.Left, this.OrbBounds.Top - 2), this.QuickAcessToolbar.LastMeasuredSize));
                if (this.CaptionButtonsVisible)
                {
                    Size size2 = new Size(20, 20);
                    int y = 2;
                    this.CloseButton.SetBounds(new Rectangle(new Point((base.ClientRectangle.Right - size2.Width) - y, y), size2));
                    this.MaximizeRestoreButton.SetBounds(new Rectangle(new Point(this.CloseButton.Bounds.Left - size2.Width, y), size2));
                    this.MinimizeButton.SetBounds(new Rectangle(new Point(this.MaximizeRestoreButton.Bounds.Left - size2.Width, y), size2));
                }
                if (flag)
                {
                    g.Dispose();
                }
                this._lastSizeMeasured = base.Size;
                this.RenewSensor();
            }
        }

        protected override void WndProc(ref Message m)
        {
            bool flag = false;
            if ((WinApi.IsWindows && ((this.ActualBorderMode == RibbonWindowMode.NonClientAreaGlass) || (this.ActualBorderMode == RibbonWindowMode.NonClientAreaCustomDrawn))) && (m.Msg == 0x84))
            {
                Form form = base.FindForm();
                int left = this.QuickAccessVisible ? this.QuickAcessToolbar.Bounds.Right : this.OrbBounds.Right;
                if (this.QuickAccessVisible && this.QuickAcessToolbar.DropDownButtonVisible)
                {
                    left = this.QuickAcessToolbar.DropDownButton.Bounds.Right;
                }
                Rectangle r = Rectangle.FromLTRB(left, 0, base.Width, this.CaptionBarSize);
                Point p = new Point(WinApi.LoWord((int)m.LParam), WinApi.HiWord((int)m.LParam));
                Point pt = base.PointToClient(p);
                bool flag2 = false;
                if (this.CaptionButtonsVisible)
                {
                    flag2 = (this.CloseButton.Bounds.Contains(pt) || this.MinimizeButton.Bounds.Contains(pt)) || this.MaximizeRestoreButton.Bounds.Contains(pt);
                }
                if (!(!base.RectangleToScreen(r).Contains(p) || flag2))
                {
                    Point point3 = base.PointToScreen(p);
                    WinApi.SendMessage(form.Handle, 0x84, m.WParam, WinApi.MakeLParam(point3.X, point3.Y));
                    m.Result = new IntPtr(-1);
                    flag = true;
                }
            }
            if (!flag)
            {
                base.WndProc(ref m);
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public RibbonTab ActiveTab
        {
            get
            {
                return this._activeTab;
            }
            set
            {
                foreach (RibbonTab tab in this.Tabs)
                {
                    if (tab != value)
                    {
                        tab.SetActive(false);
                    }
                    else
                    {
                        tab.SetActive(true);
                    }
                }
                this._activeTab = value;
                this.RemoveHelperControls();
                value.UpdatePanelsRegions();
                base.Invalidate();
                this.RenewSensor();
                this.OnActiveTabChanged(EventArgs.Empty);
            }
        }

        [Browsable(false)]
        public RibbonWindowMode ActualBorderMode
        {
            get
            {
                return this._actualBorderMode;
            }
        }

        [DefaultValue(2), Description("Specifies how the Ribbon is placed on the window border and the non-client area"), Browsable(true), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public RibbonWindowMode BorderMode
        {
            get
            {
                return this._borderMode;
            }
            set
            {
                this._borderMode = value;
                RibbonWindowMode borderMode = value;
                if (!((value != RibbonWindowMode.NonClientAreaGlass) || WinApi.IsGlassEnabled))
                {
                    borderMode = RibbonWindowMode.NonClientAreaCustomDrawn;
                }
                if ((this.FormHelper == null) || ((value == RibbonWindowMode.NonClientAreaCustomDrawn) && (Environment.OSVersion.Platform != PlatformID.Win32NT)))
                {
                    borderMode = RibbonWindowMode.InsideWindow;
                }
                this.SetActualBorderMode(borderMode);
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public int CaptionBarSize
        {
            get
            {
                return CaptionBarHeight;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool CaptionButtonsVisible
        {
            get
            {
                return this._CaptionButtonsVisible;
            }
        }

        internal Rectangle CaptionTextBounds
        {
            get
            {
                int left = 0;
                if (this.OrbVisible)
                {
                    left = this.OrbBounds.Right;
                }
                if (this.QuickAccessVisible)
                {
                    left = this.QuickAcessToolbar.Bounds.Right + 20;
                }
                if (this.QuickAccessVisible && this.QuickAcessToolbar.DropDownButtonVisible)
                {
                    left = this.QuickAcessToolbar.DropDownButton.Bounds.Right;
                }
                return Rectangle.FromLTRB(left, 0, base.Width - 100, this.CaptionBarSize);
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public RibbonCaptionButton CloseButton
        {
            get
            {
                return this._CloseButton;
            }
        }

        public RibbonContextCollection Contexts
        {
            get
            {
                return this._contexts;
            }
        }

        private string cr
        {
            get
            {
                return "Professional Ribbon\n\n2009 Jos\x00e9 Manuel Men\x00e9ndez Poo\nwww.menendezpoo.com";
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), DefaultValue(1)]
        public override DockStyle Dock
        {
            get
            {
                return base.Dock;
            }
            set
            {
                base.Dock = value;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Padding DropDownMargin
        {
            get
            {
                return this._dropDownMargin;
            }
            set
            {
                this._dropDownMargin = value;
            }
        }

        [Browsable(false)]
        public RibbonFormHelper FormHelper
        {
            get
            {
                IRibbonForm parent = base.Parent as IRibbonForm;
                if (parent != null)
                {
                    return parent.Helper;
                }
                return null;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public Padding ItemMargin
        {
            get
            {
                return this._itemMargin;
            }
            set
            {
                this._itemMargin = value;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Padding ItemPadding
        {
            get
            {
                return this._itemPadding;
            }
            set
            {
                this._itemPadding = value;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public RibbonCaptionButton MaximizeRestoreButton
        {
            get
            {
                return this._MaximizeRestoreButton;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override Size MaximumSize
        {
            get
            {
                return new Size(0, 0x8a);
            }
            set
            {
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public RibbonCaptionButton MinimizeButton
        {
            get
            {
                return this._MinimizeButton;
            }
        }

        public bool Minimized
        {
            get
            {
                return this._minimized;
            }
            set
            {
                this._minimized = value;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override Size MinimumSize
        {
            get
            {
                return new Size(0, 0x8a);
            }
            set
            {
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public RibbonTab NextTab
        {
            get
            {
                if ((this.ActiveTab == null) || (this.Tabs.Count == 0))
                {
                    if (this.Tabs.Count == 0)
                    {
                        return null;
                    }
                    return this.Tabs[0];
                }
                int index = this.Tabs.IndexOf(this.ActiveTab);
                if (index == (this.Tabs.Count - 1))
                {
                    return this.ActiveTab;
                }
                return this.Tabs[index + 1];
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public Rectangle OrbBounds
        {
            get
            {
                if (this.OrbVisible)
                {
                    return new Rectangle(4, 4, 0x24, 0x24);
                }
                return new Rectangle(4, 4, 0, 0);
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public RibbonOrbDropDown OrbDropDown
        {
            get
            {
                return this._orbDropDown;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Image OrbImage
        {
            get
            {
                return this._orbImage;
            }
            set
            {
                this._orbImage = value;
                base.Invalidate(this.OrbBounds);
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public bool OrbPressed
        {
            get
            {
                return this._orbPressed;
            }
            set
            {
                this._orbPressed = value;
                base.Invalidate(this.OrbBounds);
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public bool OrbSelected
        {
            get
            {
                return this._orbSelected;
            }
            set
            {
                this._orbSelected = value;
            }
        }

        [DefaultValue(true), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public bool OrbVisible
        {
            get
            {
                return this._orbVisible;
            }
            set
            {
                this._orbVisible = value;
                this.OnRegionsChanged();
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public Padding PanelMargin
        {
            get
            {
                return this._panelMargin;
            }
            set
            {
                this._panelMargin = value;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public Padding PanelPadding
        {
            get
            {
                return this._panelPadding;
            }
            set
            {
                this._panelPadding = value;
            }
        }

        [DefaultValue(2), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int PanelSpacing
        {
            get
            {
                return this._panelSpacing;
            }
            set
            {
                this._panelSpacing = value;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public RibbonTab PreviousTab
        {
            get
            {
                if ((this.ActiveTab == null) || (this.Tabs.Count == 0))
                {
                    if (this.Tabs.Count == 0)
                    {
                        return null;
                    }
                    return this.Tabs[0];
                }
                int index = this.Tabs.IndexOf(this.ActiveTab);
                if (index == 0)
                {
                    return this.ActiveTab;
                }
                return this.Tabs[index - 1];
            }
        }

        [Description("Shows or hides the QuickAccess toolbar"), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(true)]
        public bool QuickAccessVisible
        {
            get
            {
                return this._quickAcessVisible;
            }
            set
            {
                this._quickAcessVisible = value;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public RibbonQuickAccessToolbar QuickAcessToolbar
        {
            get
            {
                return this._quickAcessToolbar;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public RibbonRenderer Renderer
        {
            get
            {
                return this._renderer;
            }
            set
            {
                if (value == null)
                {
                    throw new ApplicationException("Null renderer!");
                }
                this._renderer = value;
                base.Invalidate();
            }
        }

        [Browsable(false)]
        public RibbonMouseSensor Sensor
        {
            get
            {
                return this._sensor;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public Padding TabContentMargin
        {
            get
            {
                return this._tabContentMargin;
            }
            set
            {
                this._tabContentMargin = value;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public Padding TabContentPadding
        {
            get
            {
                return this._tabContentPadding;
            }
            set
            {
                this._tabContentPadding = value;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public RibbonTabCollection Tabs
        {
            get
            {
                return this._tabs;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Padding TabsMargin
        {
            get
            {
                return this._tabsMargin;
            }
            set
            {
                this._tabsMargin = value;
            }
        }

        [DefaultValue(7)]
        public int TabSpacing
        {
            get
            {
                return this._tabSpacing;
            }
            set
            {
                this._tabSpacing = value;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Padding TabsPadding
        {
            get
            {
                return this._tabsPadding;
            }
            set
            {
                this._tabsPadding = value;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public Padding TabTextMargin
        {
            get
            {
                return this._tabTextMargin;
            }
            set
            {
                this._tabTextMargin = value;
            }
        }
    }
}
