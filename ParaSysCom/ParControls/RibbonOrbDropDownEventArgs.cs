﻿namespace ParControls
{
    using System;
    using System.Drawing;

    public class RibbonOrbDropDownEventArgs : RibbonRenderEventArgs
    {
        private ParControls.RibbonOrbDropDown _dropDown;

        public RibbonOrbDropDownEventArgs(Ribbon ribbon, ParControls.RibbonOrbDropDown dropDown, Graphics g, Rectangle clip) : base(ribbon, g, clip)
        {
            this._dropDown = dropDown;
        }

        public ParControls.RibbonOrbDropDown RibbonOrbDropDown
        {
            get
            {
                return this._dropDown;
            }
        }
    }
}

