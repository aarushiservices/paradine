﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    public partial class RibbonDropDown : RibbonPopup
    {
        private bool _iconsBar;
        private bool _ignoreNext;
        private IEnumerable<RibbonItem> _items;
        private RibbonElementSizeMode _MeasuringSize;
        private Ribbon _ownerRibbon;
        private RibbonItem _parentItem;
        private Point _resizeOrigin;
        private Size _resizeSize;
        private bool _resizing;
        private ISelectionService _SelectionService;
        private RibbonMouseSensor _sensor;
        private bool _showSizingGrip;
        private Rectangle _sizingGripBounds;
        private int _sizingGripHeight;

        private RibbonDropDown()
        {
            this.DoubleBuffered = true;
            this.DrawIconsBar = true;
        }

        internal RibbonDropDown(RibbonItem parentItem, IEnumerable<RibbonItem> items, Ribbon ownerRibbon)
            : this(parentItem, items, ownerRibbon, RibbonElementSizeMode.DropDown)
        {
        }

        internal RibbonDropDown(RibbonItem parentItem, IEnumerable<RibbonItem> items, Ribbon ownerRibbon, RibbonElementSizeMode measuringSize)
            : this()
        {
            this._items = items;
            this._ownerRibbon = ownerRibbon;
            this._sizingGripHeight = 12;
            this._parentItem = parentItem;
            this._sensor = new RibbonMouseSensor(this, this.OwnerRibbon, items);
            this._MeasuringSize = measuringSize;
            if (this.Items != null)
            {
                foreach (RibbonItem item in this.Items)
                {
                    item.SetSizeMode(RibbonElementSizeMode.DropDown);
                    item.SetCanvas(this);
                }
            }
            this.UpdateSize();
        }

        public void IgnoreNextClickDeactivation()
        {
            this._ignoreNext = true;
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (this.Cursor == Cursors.SizeNWSE)
            {
                this._resizeOrigin = new Point(e.X, e.Y);
                this._resizeSize = base.Size;
                this._resizing = true;
            }
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            foreach (RibbonItem item in this.Items)
            {
                item.SetSelected(false);
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (this.ShowSizingGrip && this.SizingGripBounds.Contains(e.X, e.Y))
            {
                this.Cursor = Cursors.SizeNWSE;
            }
            else if (this.Cursor == Cursors.SizeNWSE)
            {
                this.Cursor = Cursors.Default;
            }
            if (this._resizing)
            {
                int num = e.X - this._resizeOrigin.X;
                int num2 = e.Y - this._resizeOrigin.Y;
                int width = this._resizeSize.Width + num;
                int height = this._resizeSize.Height + num2;
                if ((width != base.Width) || (height != base.Height))
                {
                    base.Size = new Size(width, height);
                    if (base.WrappedDropDown != null)
                    {
                        base.WrappedDropDown.Size = base.Size;
                    }
                    this.UpdateItemsBounds();
                }
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            if (this._resizing)
            {
                this._resizing = false;
            }
            else if (this._ignoreNext)
            {
                this._ignoreNext = false;
            }
            else if (RibbonDesigner.Current != null)
            {
                base.Close();
            }
        }

        protected override void OnOpening(CancelEventArgs e)
        {
            base.OnOpening(e);
            this.UpdateItemsBounds();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            this.OwnerRibbon.Renderer.OnRenderDropDownBackground(new RibbonCanvasEventArgs(this.OwnerRibbon, e.Graphics, new Rectangle(Point.Empty, base.ClientSize), this, this.ParentItem));
            foreach (RibbonItem item in this.Items)
            {
                item.OnPaint(this, new RibbonElementPaintEventArgs(item.Bounds, e.Graphics, RibbonElementSizeMode.DropDown));
            }
        }

        protected override void OnShowed(EventArgs e)
        {
            base.OnShowed(e);
            foreach (RibbonItem item in this.Items)
            {
                item.SetSelected(false);
            }
        }

        private void UpdateItemsBounds()
        {
            int top = this.OwnerRibbon.DropDownMargin.Top;
            int left = this.OwnerRibbon.DropDownMargin.Left;
            int width = base.ClientSize.Width - this.OwnerRibbon.DropDownMargin.Horizontal;
            int num4 = 0;
            int num5 = 0;
            int num6 = 0;
            int num7 = 0;
            foreach (RibbonItem item in this.Items)
            {
                if (item is IScrollableRibbonItem)
                {
                    num4 += item.LastMeasuredSize.Height;
                    num6++;
                }
                else
                {
                    num5 += item.LastMeasuredSize.Height;
                }
            }
            if (num6 > 0)
            {
                num7 = ((base.Height - num5) - (this.ShowSizingGrip ? this.SizingGripHeight : 0)) / num6;
            }
            foreach (RibbonItem item in this.Items)
            {
                if (item is IScrollableRibbonItem)
                {
                    item.SetBounds(new Rectangle(left, top, width, num7 - 1));
                }
                else
                {
                    item.SetBounds(new Rectangle(left, top, width, item.LastMeasuredSize.Height));
                }
                top += item.Bounds.Height;
            }
            if (this.ShowSizingGrip)
            {
                this._sizingGripBounds = Rectangle.FromLTRB(base.ClientSize.Width - this.SizingGripHeight, base.ClientSize.Height - this.SizingGripHeight, base.ClientSize.Width, base.ClientSize.Height);
            }
            else
            {
                this._sizingGripBounds = Rectangle.Empty;
            }
        }

        private void UpdateSize()
        {
            int vertical = this.OwnerRibbon.DropDownMargin.Vertical;
            int num2 = 0;
            int num3 = 0;
            using (Graphics graphics = base.CreateGraphics())
            {
                foreach (RibbonItem item in this.Items)
                {
                    Size size = item.MeasureSize(this, new RibbonElementMeasureSizeEventArgs(graphics, this.MeasuringSize));
                    vertical += size.Height;
                    num2 = Math.Max(num2, size.Width);
                    if (item is IScrollableRibbonItem)
                    {
                        num3 += size.Height;
                    }
                }
            }
            Size size2 = new Size(num2 + this.OwnerRibbon.DropDownMargin.Horizontal, vertical + (this.ShowSizingGrip ? (this.SizingGripHeight + 2) : 0));
            base.Size = size2;
            if (base.WrappedDropDown != null)
            {
                base.WrappedDropDown.Size = base.Size;
            }
        }

        public bool DrawIconsBar
        {
            get
            {
                return this._iconsBar;
            }
            set
            {
                this._iconsBar = value;
            }
        }

        public IEnumerable<RibbonItem> Items
        {
            get
            {
                return this._items;
            }
        }

        public RibbonElementSizeMode MeasuringSize
        {
            get
            {
                return this._MeasuringSize;
            }
            set
            {
                this._MeasuringSize = value;
            }
        }

        public Ribbon OwnerRibbon
        {
            get
            {
                return this._ownerRibbon;
            }
        }

        public RibbonItem ParentItem
        {
            get
            {
                return this._parentItem;
            }
        }

        internal ISelectionService SelectionService
        {
            get
            {
                return this._SelectionService;
            }
            set
            {
                this._SelectionService = value;
            }
        }

        public RibbonMouseSensor Sensor
        {
            get
            {
                return this._sensor;
            }
        }

        public bool ShowSizingGrip
        {
            get
            {
                return this._showSizingGrip;
            }
            set
            {
                this._showSizingGrip = value;
                this.UpdateSize();
                this.UpdateItemsBounds();
            }
        }

        public Rectangle SizingGripBounds
        {
            get
            {
                return this._sizingGripBounds;
            }
        }

        [DefaultValue(12)]
        public int SizingGripHeight
        {
            get
            {
                return this._sizingGripHeight;
            }
            set
            {
                this._sizingGripHeight = value;
            }
        }
    }
}
