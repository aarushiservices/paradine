﻿namespace ParControls
{
    using ParControls.RibbonHelpers;
    using System;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;

    public class RibbonFormHelper
    {
        private int _capionHeight;
        private System.Windows.Forms.Form _form;
        private bool _frameExtended;
        private FormWindowState _lastState;
        private Padding _margins;
        private bool _marginsChecked;
        private ParControls.Ribbon _ribbon;

        public RibbonFormHelper(System.Windows.Forms.Form f)
        {
            this._form = f;
            this._form.Load += new EventHandler(this.Form_Activated);
            this._form.Paint += new PaintEventHandler(this.Form_Paint);
            this._form.ResizeEnd += new EventHandler(this._form_ResizeEnd);
            this._form.Resize += new EventHandler(this._form_Resize);
            this._form.Layout += new LayoutEventHandler(this._form_Layout);
        }

        private void _form_Layout(object sender, LayoutEventArgs e)
        {
            if (this._lastState != this._form.WindowState)
            {
                this.Form.Invalidate();
                this._lastState = this._form.WindowState;
            }
        }

        private void _form_Resize(object sender, EventArgs e)
        {
            this.UpdateRibbonConditions();
            using (Graphics graphics = this.Form.CreateGraphics())
            {
                using (Brush brush = new SolidBrush(this.Form.BackColor))
                {
                    graphics.FillRectangle(brush, Rectangle.FromLTRB(this.Margins.Left, this.Margins.Top, this.Form.Width - this.Margins.Right, this.Form.Height - this.Margins.Bottom));
                }
            }
        }

        private void _form_ResizeEnd(object sender, EventArgs e)
        {
            this.UpdateRibbonConditions();
            this.Form.Refresh();
        }

        protected virtual void Form_Activated(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                WinApi.MARGINS marInset = new WinApi.MARGINS(this.Margins.Left, this.Margins.Right, this.Margins.Bottom + ParControls.Ribbon.CaptionBarHeight, this.Margins.Bottom);
                if (!(!WinApi.IsVista || this._frameExtended))
                {
                    WinApi.DwmExtendFrameIntoClientArea(this.Form.Handle, ref marInset);
                    this._frameExtended = true;
                }
            }
        }

        public void Form_Paint(object sender, PaintEventArgs e)
        {
            if (!this.DesignMode)
            {
                if (WinApi.IsGlassEnabled)
                {
                    WinApi.FillForGlass(e.Graphics, new Rectangle(0, 0, this.Form.Width, this.Form.Height));
                    using (Brush brush = new SolidBrush(this.Form.BackColor))
                    {
                        e.Graphics.FillRectangle(brush, Rectangle.FromLTRB(this.Margins.Left, this.Margins.Top, this.Form.Width - this.Margins.Right, this.Form.Height - this.Margins.Bottom));
                    }
                }
                else
                {
                    this.PaintTitleBar(e);
                }
            }
        }

        public virtual NonClientHitTestResult NonClientHitTest(Point hitPoint)
        {
            if (this.Form.RectangleToScreen(new Rectangle(0, 0, this.Margins.Left, this.Margins.Left)).Contains(hitPoint))
            {
                return NonClientHitTestResult.TopLeft;
            }
            if (this.Form.RectangleToScreen(new Rectangle(this.Form.Width - this.Margins.Right, 0, this.Margins.Right, this.Margins.Right)).Contains(hitPoint))
            {
                return NonClientHitTestResult.TopRight;
            }
            if (this.Form.RectangleToScreen(new Rectangle(0, this.Form.Height - this.Margins.Bottom, this.Margins.Left, this.Margins.Bottom)).Contains(hitPoint))
            {
                return NonClientHitTestResult.BottomLeft;
            }
            if (this.Form.RectangleToScreen(new Rectangle(this.Form.Width - this.Margins.Right, this.Form.Height - this.Margins.Bottom, this.Margins.Right, this.Margins.Bottom)).Contains(hitPoint))
            {
                return NonClientHitTestResult.BottomRight;
            }
            if (this.Form.RectangleToScreen(new Rectangle(0, 0, this.Form.Width, this.Margins.Left)).Contains(hitPoint))
            {
                return NonClientHitTestResult.Top;
            }
            if (this.Form.RectangleToScreen(new Rectangle(0, this.Margins.Left, this.Form.Width, this.Margins.Top - this.Margins.Left)).Contains(hitPoint))
            {
                return NonClientHitTestResult.Caption;
            }
            if (this.Form.RectangleToScreen(new Rectangle(0, 0, this.Margins.Left, this.Form.Height)).Contains(hitPoint))
            {
                return NonClientHitTestResult.Left;
            }
            if (this.Form.RectangleToScreen(new Rectangle(this.Form.Width - this.Margins.Right, 0, this.Margins.Right, this.Form.Height)).Contains(hitPoint))
            {
                return NonClientHitTestResult.Right;
            }
            if (this.Form.RectangleToScreen(new Rectangle(0, this.Form.Height - this.Margins.Bottom, this.Form.Width, this.Margins.Bottom)).Contains(hitPoint))
            {
                return NonClientHitTestResult.Bottom;
            }
            return NonClientHitTestResult.Client;
        }

        private void PaintTitleBar(PaintEventArgs e)
        {
            int radius = 4;
            int num2 = radius;
            Rectangle r = new Rectangle(Point.Empty, this.Form.Size);
            Rectangle rectangle2 = new Rectangle(Point.Empty, new Size(r.Width - 1, r.Height - 1));
            using (GraphicsPath path = RibbonProfessionalRenderer.RoundRectangle(r, radius))
            {
                using (GraphicsPath path2 = RibbonProfessionalRenderer.RoundRectangle(rectangle2, num2))
                {
                    if ((this.Ribbon != null) && (this.Ribbon.ActualBorderMode == RibbonWindowMode.NonClientAreaCustomDrawn))
                    {
                        RibbonProfessionalRenderer renderer = this.Ribbon.Renderer as RibbonProfessionalRenderer;
                        if (renderer != null)
                        {
                            using (SolidBrush brush = new SolidBrush(renderer.ColorTable.Caption1))
                            {
                                e.Graphics.FillRectangle(brush, new Rectangle(0, 0, this.Form.Width, this.Ribbon.CaptionBarSize));
                            }
                            renderer.DrawCaptionBarBackground(new Rectangle(0, this.Margins.Bottom - 1, this.Form.Width, this.Ribbon.CaptionBarSize), e.Graphics);
                            using (Region region = new Region(path))
                            {
                                this.Form.Region = region;
                                SmoothingMode smoothingMode = e.Graphics.SmoothingMode;
                                e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
                                using (Pen pen = new Pen(renderer.ColorTable.FormBorder, 1f))
                                {
                                    e.Graphics.DrawPath(pen, path2);
                                }
                                e.Graphics.SmoothingMode = smoothingMode;
                            }
                        }
                    }
                }
            }
        }

        private void SetMargins(Padding p)
        {
            this._margins = p;
            Padding padding = p;
            padding.Top = p.Bottom - 1;
            if (!this.DesignMode)
            {
                this.Form.Padding = padding;
            }
        }

        private void UpdateRibbonConditions()
        {
            if (this.Ribbon != null)
            {
                if (this.Ribbon.ActualBorderMode == RibbonWindowMode.NonClientAreaGlass)
                {
                    if (this.Ribbon.Dock != DockStyle.None)
                    {
                        this.Ribbon.Dock = DockStyle.None;
                    }
                    this.Ribbon.SetBounds(this.Margins.Left, this.Margins.Bottom - 1, this.Form.Width - this.Margins.Horizontal, this.Ribbon.Height);
                }
                else if (this.Ribbon.Dock != DockStyle.Top)
                {
                    this.Ribbon.Dock = DockStyle.Top;
                }
            }
        }

        public virtual bool WndProc(ref Message m)
        {
            IntPtr ptr;
            if (this.DesignMode)
            {
                return false;
            }
            bool flag = false;
            if (WinApi.IsVista && (WinApi.DwmDefWindowProc(m.HWnd, m.Msg, m.WParam, m.LParam, out ptr) == 1))
            {
                m.Result = ptr;
                flag = true;
            }
            if (!flag)
            {
                if ((m.Msg == 0x83) && (((int) m.WParam) == 1))
                {
                    WinApi.NCCALCSIZE_PARAMS structure = (WinApi.NCCALCSIZE_PARAMS) Marshal.PtrToStructure(m.LParam, typeof(WinApi.NCCALCSIZE_PARAMS));
                    if (!this.MarginsChecked)
                    {
                        this.SetMargins(new Padding(structure.rect2.Left - structure.rect1.Left, structure.rect2.Top - structure.rect1.Top, structure.rect1.Right - structure.rect2.Right, structure.rect1.Bottom - structure.rect2.Bottom));
                        this.MarginsChecked = true;
                    }
                    Marshal.StructureToPtr(structure, m.LParam, false);
                    m.Result = IntPtr.Zero;
                    return true;
                }
                if (((m.Msg != 0x84) || (((int) m.Result) != 0)) && (((this.Ribbon != null) && (this.Ribbon.ActualBorderMode != RibbonWindowMode.NonClientAreaCustomDrawn)) && ((((m.Msg == 0x112) || (m.Msg == 0x47)) || (m.Msg == 70)) || (m.Msg == 0x2a2))))
                {
                }
            }
            return flag;
        }

        public int CaptionHeight
        {
            get
            {
                return this._capionHeight;
            }
            set
            {
                this._capionHeight = value;
            }
        }

        private bool DesignMode
        {
            get
            {
                return (((this.Form != null) && (this.Form.Site != null)) && this.Form.Site.DesignMode);
            }
        }

        public System.Windows.Forms.Form Form
        {
            get
            {
                return this._form;
            }
        }

        public Padding Margins
        {
            get
            {
                return this._margins;
            }
        }

        private bool MarginsChecked
        {
            get
            {
                return this._marginsChecked;
            }
            set
            {
                this._marginsChecked = value;
            }
        }

        public ParControls.Ribbon Ribbon
        {
            get
            {
                return this._ribbon;
            }
            set
            {
                this._ribbon = value;
                this.UpdateRibbonConditions();
            }
        }

        public enum NonClientHitTestResult
        {
            Bottom = 15,
            BottomLeft = 0x10,
            BottomRight = 0x11,
            Caption = 2,
            Client = 1,
            GrowBox = 4,
            Left = 10,
            MaximizeButton = 9,
            MinimizeButton = 8,
            Nowhere = 0,
            Right = 11,
            Top = 12,
            TopLeft = 13,
            TopRight = 14
        }
    }
}

