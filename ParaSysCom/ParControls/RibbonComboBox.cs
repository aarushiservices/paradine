﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    [Designer(typeof(RibbonComboBoxDesigner))]
    public partial class RibbonComboBox : RibbonTextBox, IContainsRibbonComponents, IDropDownRibbonItem
    {
        private bool _allowTextEdit = true;
        private Rectangle _dropDownBounds;
        private RibbonItemCollection _dropDownItems = new RibbonItemCollection();
        private bool _dropDownPressed;
        private bool _dropDownResizable;
        private bool _dropDownSelected;
        private bool _dropDownVisible = true;

        public event EventHandler DropDownShowing;

        private void AssignHandlers()
        {
            foreach (RibbonItem item in this.DropDownItems)
            {
                item.Click += new EventHandler(this.item_Click);
            }
        }

        protected override bool ClosesDropDownAt(Point p)
        {
            return false;
        }

        private void DropDown_Closed(object sender, EventArgs e)
        {
            this.RemoveHandlers();
        }

        public IEnumerable<Component> GetAllChildComponents()
        {
            return this.DropDownItems.ToArray();
        }

        protected override void InitTextBox(TextBox t)
        {
            base.InitTextBox(t);
            t.Width -= this.DropDownButtonBounds.Width;
        }

        private void item_Click(object sender, EventArgs e)
        {
            base.TextBoxText = (sender as RibbonItem).Text;
        }

        public void OnDropDownShowing(EventArgs e)
        {
            if (this.DropDownShowing != null)
            {
                this.DropDownShowing(this, e);
            }
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            if (this.Enabled)
            {
                if (this.DropDownButtonBounds.Contains(e.X, e.Y))
                {
                    this._dropDownPressed = true;
                    this.ShowDropDown();
                }
                else if (this.TextBoxBounds.Contains(e.X, e.Y) && this.AllowTextEdit)
                {
                    base.StartEdit();
                }
            }
        }

        public override void OnMouseLeave(MouseEventArgs e)
        {
            if (this.Enabled)
            {
                base.OnMouseLeave(e);
                this._dropDownSelected = false;
            }
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (this.Enabled)
            {
                base.OnMouseMove(e);
                bool flag = false;
                if (this.DropDownButtonBounds.Contains(e.X, e.Y))
                {
                    base.Owner.Cursor = Cursors.Default;
                    flag = !this._dropDownSelected;
                    this._dropDownSelected = true;
                }
                else if (this.TextBoxBounds.Contains(e.X, e.Y))
                {
                    base.Owner.Cursor = Cursors.IBeam;
                    flag = this._dropDownSelected;
                    this._dropDownSelected = false;
                }
                else
                {
                    base.Owner.Cursor = Cursors.Default;
                }
                if (flag)
                {
                    this.RedrawItem();
                }
            }
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            if (this.Enabled)
            {
                base.OnMouseUp(e);
                this._dropDownPressed = false;
            }
        }

        private void RemoveHandlers()
        {
            foreach (RibbonItem item in this.DropDownItems)
            {
                item.Click -= new EventHandler(this.item_Click);
            }
        }

        public override void SetBounds(Rectangle bounds)
        {
            base.SetBounds(bounds);
            this._dropDownBounds = Rectangle.FromLTRB(bounds.Right - 15, bounds.Top, bounds.Right + 1, bounds.Bottom + 1);
        }

        internal override void SetOwner(Ribbon owner)
        {
            base.SetOwner(owner);
            this._dropDownItems.SetOwner(owner);
        }

        internal override void SetOwnerPanel(RibbonPanel ownerPanel)
        {
            base.SetOwnerPanel(ownerPanel);
            this._dropDownItems.SetOwnerPanel(ownerPanel);
        }

        internal override void SetOwnerTab(RibbonTab ownerTab)
        {
            base.SetOwnerTab(ownerTab);
            this._dropDownItems.SetOwnerTab(base.OwnerTab);
        }

        public void ShowDropDown()
        {
            this.OnDropDownShowing(EventArgs.Empty);
            this.AssignHandlers();
            RibbonDropDown down = new RibbonDropDown(this, this.DropDownItems, base.Owner);
            down.ShowSizingGrip = this.DropDownResizable;
            down.Closed += new EventHandler(this.DropDown_Closed);
            down.Show(base.Owner.PointToScreen(new Point(this.TextBoxBounds.Left, base.Bounds.Bottom)));
        }

        [Description("Allows user to change the text on the ComboBox"), DefaultValue(true)]
        public bool AllowTextEdit
        {
            get
            {
                return this._allowTextEdit;
            }
            set
            {
                this._allowTextEdit = value;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public Rectangle DropDownButtonBounds
        {
            get
            {
                return this._dropDownBounds;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public bool DropDownButtonPressed
        {
            get
            {
                return this._dropDownPressed;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public bool DropDownButtonSelected
        {
            get
            {
                return this._dropDownSelected;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public bool DropDownButtonVisible
        {
            get
            {
                return this._dropDownVisible;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public RibbonItemCollection DropDownItems
        {
            get
            {
                return this._dropDownItems;
            }
        }

        [DefaultValue(false), Description("Makes the DropDown resizable with a grip on the corner")]
        public bool DropDownResizable
        {
            get
            {
                return this._dropDownResizable;
            }
            set
            {
                this._dropDownResizable = value;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public override Rectangle TextBoxTextBounds
        {
            get
            {
                Rectangle textBoxTextBounds = base.TextBoxTextBounds;
                textBoxTextBounds.Width -= this.DropDownButtonBounds.Width;
                return textBoxTextBounds;
            }
        }
    }
}
