﻿namespace ParControls
{
    using System.Collections.Generic;
    using System.ComponentModel;

    public interface IContainsRibbonComponents
    {
        IEnumerable<Component> GetAllChildComponents();
    }
}

