﻿namespace ParControls
{
    using System;

    public enum RibbonElementSizeMode
    {
        None,
        Overflow,
        Compact,
        Medium,
        Large,
        DropDown
    }
}

