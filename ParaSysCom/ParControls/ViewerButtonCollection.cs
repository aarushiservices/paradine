﻿namespace ParControls
{
    using ParaSysCom;
    using System;
    using System.Collections;
    using System.Reflection;

    public class ViewerButtonCollection : CollectionBase
    {
        private Tabpanel vParent;

        public ViewerButtonCollection(Tabpanel parent)
        {
            this.vParent = parent;
        }

        public int Add(ParButton value)
        {
            return base.List.Add(value);
        }

        public void AddRange(ParButton[] pages)
        {
            foreach (ParButton button in pages)
            {
                this.Add(button);
            }
        }

        public bool Contains(ParButton value)
        {
            return base.List.Contains(value);
        }

        public int IndexOf(ParButton value)
        {
            return base.List.IndexOf(value);
        }

        public void Insert(int index, ParButton value)
        {
            base.List.Insert(index, value);
        }

        protected override void OnInsertComplete(int index, object value)
        {
            base.OnInsertComplete(index, value);
            this.vParent.ButtonIndex = index;
        }

        protected override void OnRemoveComplete(int index, object value)
        {
            base.OnRemoveComplete(index, value);
            if (this.vParent.ButtonIndex == index)
            {
                if (index < base.InnerList.Count)
                {
                    this.vParent.ButtonIndex = index;
                }
                else
                {
                    this.vParent.ButtonIndex = base.InnerList.Count - 1;
                }
            }
        }

        public void Remove(ParButton value)
        {
            base.List.Remove(value);
        }

        public ParButton this[int index]
        {
            get
            {
                return (ParButton) base.List[index];
            }
            set
            {
                base.List[index] = value;
            }
        }

        public Tabpanel Parent
        {
            get
            {
                return this.vParent;
            }
        }
    }
}

