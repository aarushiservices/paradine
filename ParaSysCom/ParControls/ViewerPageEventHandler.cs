﻿namespace ParControls
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void ViewerPageEventHandler(object sender, ViewerPageEventArgs e);
}

