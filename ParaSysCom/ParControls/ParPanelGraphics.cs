﻿namespace ParControls
{
    using System;
    using System.Drawing;
    using System.Drawing.Drawing2D;

    internal class ParPanelGraphics
    {
        public static GraphicsPath GetRoundPath(Rectangle r, int depth)
        {
            if (depth == 0)
            {
                depth = 1;
            }
            GraphicsPath path = new GraphicsPath();
            path.AddArc(r.X, r.Y, depth, depth, 180f, 90f);
            path.AddArc((r.X + r.Width) - depth, r.Y, depth, depth, 270f, 90f);
            path.AddArc((r.X + r.Width) - depth, (r.Y + r.Height) - depth, depth, depth, 0f, 90f);
            path.AddArc(r.X, (r.Y + r.Height) - depth, depth, depth, 90f, 90f);
            path.AddLine(r.X, (r.Y + r.Height) - depth, r.X, r.Y + (depth / 2));
            return path;
        }
    }
}

