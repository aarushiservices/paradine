﻿namespace ParControls
{
    using System;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Windows.Forms;
    using System.Windows.Forms.Design.Behavior;

    public class RibbonTabGlyph : Glyph
    {
        private BehaviorService _behaviorService;
        private RibbonDesigner _componentDesigner;
        private Ribbon _ribbon;
        private Size size;

        public RibbonTabGlyph(BehaviorService behaviorService, RibbonDesigner designer, Ribbon ribbon) : base(new RibbonTabGlyphBehavior(designer, ribbon))
        {
            this._behaviorService = behaviorService;
            this._componentDesigner = designer;
            this._ribbon = ribbon;
            this.size = new Size(60, 0x10);
        }

        public override Cursor GetHitTest(Point p)
        {
            if (this.Bounds.Contains(p))
            {
                return Cursors.Hand;
            }
            return null;
        }

        public override void Paint(PaintEventArgs pe)
        {
            SmoothingMode smoothingMode = pe.Graphics.SmoothingMode;
            pe.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            using (GraphicsPath path = RibbonProfessionalRenderer.RoundRectangle(this.Bounds, 2))
            {
                using (SolidBrush brush = new SolidBrush(Color.FromArgb(50, Color.Blue)))
                {
                    pe.Graphics.FillPath(brush, path);
                }
            }
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;
            pe.Graphics.DrawString("Add Tab", SystemFonts.DefaultFont, Brushes.White, this.Bounds, format);
            pe.Graphics.SmoothingMode = smoothingMode;
        }

        public override Rectangle Bounds
        {
            get
            {
                Point point = this._behaviorService.ControlToAdornerWindow(this._ribbon);
                Point point2 = new Point(5, this._ribbon.OrbBounds.Bottom + 5);
                if (this._ribbon.Tabs.Count > 0)
                {
                    RibbonTab tab = this._ribbon.Tabs[this._ribbon.Tabs.Count - 1];
                    point2.X = tab.Bounds.Right + 5;
                    point2.Y = tab.Bounds.Top + 2;
                }
                return new Rectangle(point.X + point2.X, point.Y + point2.Y, this.size.Width, this.size.Height);
            }
        }
    }
}

