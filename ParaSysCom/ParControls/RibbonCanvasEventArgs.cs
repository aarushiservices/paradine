﻿namespace ParControls
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    public class RibbonCanvasEventArgs : EventArgs
    {
        private Rectangle _bounds;
        private Control _canvas;
        private System.Drawing.Graphics _Graphics;
        private Ribbon _owner;
        private object _relatedObject;

        public RibbonCanvasEventArgs(Ribbon owner, System.Drawing.Graphics g, Rectangle bounds, Control canvas, object relatedObject)
        {
            this.Owner = owner;
            this.Graphics = g;
            this.Bounds = bounds;
            this.Canvas = canvas;
            this.RelatedObject = relatedObject;
        }

        public Rectangle Bounds
        {
            get
            {
                return this._bounds;
            }
            set
            {
                this._bounds = value;
            }
        }

        public Control Canvas
        {
            get
            {
                return this._canvas;
            }
            set
            {
                this._canvas = value;
            }
        }

        public System.Drawing.Graphics Graphics
        {
            get
            {
                return this._Graphics;
            }
            set
            {
                this._Graphics = value;
            }
        }

        public Ribbon Owner
        {
            get
            {
                return this._owner;
            }
            set
            {
                this._owner = value;
            }
        }

        public object RelatedObject
        {
            get
            {
                return this._relatedObject;
            }
            set
            {
                this._relatedObject = value;
            }
        }
    }
}

