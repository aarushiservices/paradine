﻿namespace ParControls
{
    using System;
    using System.Drawing;

    public class RibbonRenderEventArgs : EventArgs
    {
        private Rectangle _clipRectangle;
        private System.Drawing.Graphics _graphics;
        private ParControls.Ribbon _ribbon;

        public RibbonRenderEventArgs(ParControls.Ribbon owner, System.Drawing.Graphics g, Rectangle clip)
        {
            this.Ribbon = owner;
            this.Graphics = g;
            this.ClipRectangle = clip;
        }

        public Rectangle ClipRectangle
        {
            get
            {
                return this._clipRectangle;
            }
            set
            {
                this._clipRectangle = value;
            }
        }

        public System.Drawing.Graphics Graphics
        {
            get
            {
                return this._graphics;
            }
            set
            {
                this._graphics = value;
            }
        }

        public ParControls.Ribbon Ribbon
        {
            get
            {
                return this._ribbon;
            }
            set
            {
                this._ribbon = value;
            }
        }
    }
}

