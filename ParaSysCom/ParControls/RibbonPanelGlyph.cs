﻿namespace ParControls
{
    using System;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Windows.Forms;
    using System.Windows.Forms.Design.Behavior;

    public class RibbonPanelGlyph : Glyph
    {
        private BehaviorService _behaviorService;
        private RibbonTabDesigner _componentDesigner;
        private RibbonTab _tab;
        private Size size;

        public RibbonPanelGlyph(BehaviorService behaviorService, RibbonTabDesigner designer, RibbonTab tab) : base(new RibbonPanelGlyphBehavior(designer, tab))
        {
            this._behaviorService = behaviorService;
            this._componentDesigner = designer;
            this._tab = tab;
            this.size = new Size(60, 0x10);
        }

        public override Cursor GetHitTest(Point p)
        {
            if (this.Bounds.Contains(p))
            {
                return Cursors.Hand;
            }
            return null;
        }

        public override void Paint(PaintEventArgs pe)
        {
            SmoothingMode smoothingMode = pe.Graphics.SmoothingMode;
            pe.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            using (GraphicsPath path = RibbonProfessionalRenderer.RoundRectangle(this.Bounds, 9))
            {
                using (SolidBrush brush = new SolidBrush(Color.FromArgb(50, Color.Blue)))
                {
                    pe.Graphics.FillPath(brush, path);
                }
            }
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;
            pe.Graphics.DrawString("Add Panel", SystemFonts.DefaultFont, Brushes.White, this.Bounds, format);
            pe.Graphics.SmoothingMode = smoothingMode;
        }

        public override Rectangle Bounds
        {
            get
            {
                if (!(this._tab.Active && this._tab.Owner.Tabs.Contains(this._tab)))
                {
                    return Rectangle.Empty;
                }
                Point point = this._behaviorService.ControlToAdornerWindow(this._tab.Owner);
                Point point2 = new Point(5, this._tab.TabBounds.Bottom + 5);
                if (this._tab.Panels.Count > 0)
                {
                    RibbonPanel panel = this._tab.Panels[this._tab.Panels.Count - 1];
                    point2.X = panel.Bounds.Right + 5;
                }
                return new Rectangle(point.X + point2.X, point.Y + point2.Y, this.size.Width, this.size.Height);
            }
        }
    }
}

