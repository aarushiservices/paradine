﻿namespace ParControls
{
    using System;
    using System.Windows.Forms;
    using System.Windows.Forms.Design.Behavior;

    public class RibbonQuickAccessGlyphBehavior : System.Windows.Forms.Design.Behavior.Behavior
    {
        private RibbonDesigner _designer;
        private Ribbon _ribbon;

        public RibbonQuickAccessGlyphBehavior(RibbonDesigner designer, Ribbon ribbon)
        {
            this._designer = designer;
            this._ribbon = ribbon;
        }

        public override bool OnMouseUp(Glyph g, MouseButtons button)
        {
            this._designer.CreateItem(this._ribbon, this._ribbon.QuickAcessToolbar.Items, typeof(RibbonButton));
            return base.OnMouseUp(g, button);
        }
    }
}

