﻿namespace ParControls
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.Design;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Windows.Forms;
    using System.Windows.Forms.Design;
    using System.Windows.Forms.Design.Behavior;

    public class RibbonDesigner : ControlDesigner
    {
        private IRibbonElement _selectedElement;
        public static RibbonDesigner Current;
        private Adorner orbAdorner;
        private Adorner quickAccessAdorner;
        private Adorner tabAdorner;

        public RibbonDesigner()
        {
            Current = this;
        }

        public void AddTabVerb(object sender, EventArgs e)
        {
            ParControls.Ribbon control = this.Control as ParControls.Ribbon;
            if (control != null)
            {
                IDesignerHost service = this.GetService(typeof(IDesignerHost)) as IDesignerHost;
                if (service != null)
                {
                    RibbonTab item = service.CreateComponent(typeof(RibbonTab)) as RibbonTab;
                    if (item != null)
                    {
                        item.Text = item.Site.Name;
                        this.Ribbon.Tabs.Add(item);
                        control.Refresh();
                    }
                }
            }
        }

        private void AssignEventHandler()
        {
        }

        public void changeService_ComponentRemoved(object sender, ComponentEventArgs e)
        {
            RibbonTab component = e.Component as RibbonTab;
            RibbonPanel panel = e.Component as RibbonPanel;
            RibbonItem item = e.Component as RibbonItem;
            IDesignerHost service = this.GetService(typeof(IDesignerHost)) as IDesignerHost;
            if (component != null)
            {
                this.Ribbon.Tabs.Remove(component);
            }
            else if (panel != null)
            {
                panel.OwnerTab.Panels.Remove(panel);
            }
            else if (item != null)
            {
                if (item.Canvas is RibbonOrbDropDown)
                {
                    this.Ribbon.OrbDropDown.HandleDesignerItemRemoved(item);
                }
                else if (item.OwnerItem is RibbonItemGroup)
                {
                    (item.OwnerItem as RibbonItemGroup).Items.Remove(item);
                }
                else if (item.OwnerPanel != null)
                {
                    item.OwnerPanel.Items.Remove(item);
                }
                else if (this.Ribbon.QuickAcessToolbar.Items.Contains(item))
                {
                    this.Ribbon.QuickAcessToolbar.Items.Remove(item);
                }
            }
            this.RemoveRecursive(e.Component as IContainsRibbonComponents, service);
            this.SelectedElement = null;
            this.Ribbon.OnRegionsChanged();
        }

        public virtual void CreateItem(ParControls.Ribbon ribbon, RibbonItemCollection collection, Type t)
        {
            IDesignerHost service = this.GetService(typeof(IDesignerHost)) as IDesignerHost;
            if (((service != null) && (collection != null)) && (ribbon != null))
            {
                DesignerTransaction transaction = service.CreateTransaction("AddRibbonItem_" + base.Component.Site.Name);
                MemberDescriptor member = TypeDescriptor.GetProperties(base.Component)["Items"];
                base.RaiseComponentChanging(member);
                RibbonItem item = service.CreateComponent(t) as RibbonItem;
                if (!(item is RibbonSeparator))
                {
                    item.Text = item.Site.Name;
                }
                collection.Add(item);
                ribbon.OnRegionsChanged();
                base.RaiseComponentChanged(member, null, null);
                transaction.Commit();
            }
        }

        private void CreateOrbItem(string collectionName, RibbonItemCollection collection, Type t)
        {
            if (this.Ribbon != null)
            {
                IDesignerHost service = this.GetService(typeof(IDesignerHost)) as IDesignerHost;
                DesignerTransaction transaction = service.CreateTransaction("AddRibbonOrbItem_" + base.Component.Site.Name);
                MemberDescriptor member = TypeDescriptor.GetProperties(this.Ribbon.OrbDropDown)[collectionName];
                base.RaiseComponentChanging(member);
                RibbonItem item = service.CreateComponent(t) as RibbonItem;
                if (!(item is RibbonSeparator))
                {
                    item.Text = item.Site.Name;
                }
                collection.Add(item);
                this.Ribbon.OrbDropDown.OnRegionsChanged();
                base.RaiseComponentChanged(member, null, null);
                transaction.Commit();
                this.Ribbon.OrbDropDown.SelectOnDesigner(item);
                this.Ribbon.OrbDropDown.WrappedDropDown.Size = this.Ribbon.OrbDropDown.Size;
            }
        }

        public void CreteOrbMenuItem(Type t)
        {
            this.CreateOrbItem("MenuItems", this.Ribbon.OrbDropDown.MenuItems, t);
        }

        public void CreteOrbOptionItem(Type t)
        {
            this.CreateOrbItem("OptionItems", this.Ribbon.OrbDropDown.OptionItems, t);
        }

        public void CreteOrbRecentItem(Type t)
        {
            this.CreateOrbItem("RecentItems", this.Ribbon.OrbDropDown.RecentItems, t);
        }

        ~RibbonDesigner()
        {
            if (Current == this)
            {
                Current = null;
            }
        }

        public BehaviorService GetBehaviorService()
        {
            return base.BehaviorService;
        }

        private void HitOn(int x, int y)
        {
            if ((this.Ribbon.Tabs.Count == 0) || (this.Ribbon.ActiveTab == null))
            {
                this.SelectRibbon();
            }
            else if (this.Ribbon != null)
            {
                if (this.Ribbon.TabHitTest(x, y))
                {
                    this.SelectedElement = this.Ribbon.ActiveTab;
                }
                else
                {
                    if (this.Ribbon.ActiveTab.TabContentBounds.Contains(x, y))
                    {
                        if (this.Ribbon.ActiveTab.ScrollLeftBounds.Contains(x, y) && this.Ribbon.ActiveTab.ScrollLeftVisible)
                        {
                            this.Ribbon.ActiveTab.ScrollLeft();
                            this.SelectedElement = this.Ribbon.ActiveTab;
                            return;
                        }
                        if (this.Ribbon.ActiveTab.ScrollRightBounds.Contains(x, y) && this.Ribbon.ActiveTab.ScrollRightVisible)
                        {
                            this.Ribbon.ActiveTab.ScrollRight();
                            this.SelectedElement = this.Ribbon.ActiveTab;
                            return;
                        }
                    }
                    if (!this.Ribbon.ActiveTab.TabContentBounds.Contains(x, y))
                    {
                        if (!this.Ribbon.QuickAcessToolbar.SuperBounds.Contains(x, y))
                        {
                            if (this.Ribbon.OrbBounds.Contains(x, y))
                            {
                                this.Ribbon.OrbMouseDown();
                            }
                            else
                            {
                                this.SelectRibbon();
                                this.Ribbon.ForceOrbMenu = false;
                                if (this.Ribbon.OrbDropDown.Visible)
                                {
                                    this.Ribbon.OrbDropDown.Close();
                                }
                            }
                        }
                        else
                        {
                            bool flag = false;
                            foreach (RibbonItem item2 in this.Ribbon.QuickAcessToolbar.Items)
                            {
                                if (item2.Bounds.Contains(x, y))
                                {
                                    flag = true;
                                    this.SelectedElement = item2;
                                    break;
                                }
                            }
                            if (!flag)
                            {
                                this.SelectedElement = this.Ribbon.QuickAcessToolbar;
                            }
                        }
                    }
                    else
                    {
                        RibbonPanel panel = null;
                        foreach (RibbonPanel panel2 in this.Ribbon.ActiveTab.Panels)
                        {
                            if (panel2.Bounds.Contains(x, y))
                            {
                                panel = panel2;
                                break;
                            }
                        }
                        if (panel == null)
                        {
                            this.SelectedElement = this.Ribbon.ActiveTab;
                        }
                        else
                        {
                            RibbonItem item = null;
                            foreach (RibbonItem item2 in panel.Items)
                            {
                                if (item2.Bounds.Contains(x, y))
                                {
                                    item = item2;
                                    break;
                                }
                            }
                            if ((item == null) || !(item is IContainsSelectableRibbonItems))
                            {
                                if (item != null)
                                {
                                    this.SelectedElement = item;
                                }
                                else
                                {
                                    this.SelectedElement = panel;
                                }
                            }
                            else
                            {
                                RibbonItem item3 = null;
                                foreach (RibbonItem item4 in (item as IContainsSelectableRibbonItems).GetItems())
                                {
                                    if (item4.Bounds.Contains(x, y))
                                    {
                                        item3 = item4;
                                        break;
                                    }
                                }
                                if (item3 != null)
                                {
                                    this.SelectedElement = item3;
                                }
                                else
                                {
                                    this.SelectedElement = item;
                                }
                            }
                        }
                    }
                }
            }
        }

        public static int HiWord(int dwValue)
        {
            return ((dwValue >> 0x10) & 0xffff);
        }

        public override void Initialize(IComponent component)
        {
            base.Initialize(component);
            IComponentChangeService service = this.GetService(typeof(IComponentChangeService)) as IComponentChangeService;
            IDesignerEventService service2 = this.GetService(typeof(IDesignerEventService)) as IDesignerEventService;
            service.ComponentRemoved += new ComponentEventHandler(this.changeService_ComponentRemoved);
            this.quickAccessAdorner = new Adorner();
            this.orbAdorner = new Adorner();
            this.tabAdorner = new Adorner();
            base.BehaviorService.Adorners.AddRange(new Adorner[] { this.quickAccessAdorner, this.orbAdorner, this.tabAdorner });
            this.quickAccessAdorner.Glyphs.Add(new RibbonQuickAccessToolbarGlyph(base.BehaviorService, this, this.Ribbon));
            this.tabAdorner.Glyphs.Add(new RibbonTabGlyph(base.BehaviorService, this, this.Ribbon));
        }

        public static int LoWord(int dwValue)
        {
            return (dwValue & 0xffff);
        }

        protected override void OnPaintAdornments(PaintEventArgs pe)
        {
            base.OnPaintAdornments(pe);
            using (Pen pen = new Pen(Color.Black))
            {
                pen.DashStyle = DashStyle.Dot;
                ISelectionService service = this.GetService(typeof(ISelectionService)) as ISelectionService;
                if (service != null)
                {
                    foreach (IComponent component in service.GetSelectedComponents())
                    {
                        RibbonItem item = component as RibbonItem;
                        if (!((item == null) || this.Ribbon.OrbDropDown.AllItems.Contains(item)))
                        {
                            pe.Graphics.DrawRectangle(pen, item.Bounds);
                        }
                    }
                }
            }
        }

        public void RemoveRecursive(IContainsRibbonComponents item, IDesignerHost service)
        {
            if ((item != null) && (service != null))
            {
                foreach (Component component in item.GetAllChildComponents())
                {
                    if (component is IContainsRibbonComponents)
                    {
                        this.RemoveRecursive(component as IContainsRibbonComponents, service);
                    }
                    service.DestroyComponent(component);
                }
            }
        }

        private void SelectRibbon()
        {
            ISelectionService service = this.GetService(typeof(ISelectionService)) as ISelectionService;
            if (service != null)
            {
                service.SetSelectedComponents(new Component[] { this.Ribbon }, SelectionTypes.Click);
            }
        }

        protected override void WndProc(ref Message m)
        {
            if (m.HWnd == this.Control.Handle)
            {
                switch (m.Msg)
                {
                    case 0x201:
                    case 0x204:
                        return;

                    case 0x202:
                    case 0x205:
                        this.HitOn(LoWord((int) m.LParam), HiWord((int) m.LParam));
                        return;

                    case 0x203:
                        this.AssignEventHandler();
                        break;
                }
            }
            base.WndProc(ref m);
        }

        public ParControls.Ribbon Ribbon
        {
            get
            {
                return (this.Control as ParControls.Ribbon);
            }
        }

        public IRibbonElement SelectedElement
        {
            get
            {
                return this._selectedElement;
            }
            set
            {
                this._selectedElement = value;
                ISelectionService service = this.GetService(typeof(ISelectionService)) as ISelectionService;
                if ((service != null) && (value != null))
                {
                    service.SetSelectedComponents(new Component[] { value as Component }, SelectionTypes.Click);
                }
                if (value is RibbonButton)
                {
                    (value as RibbonButton).ShowDropDown();
                }
                this.Ribbon.Refresh();
            }
        }

        public override DesignerVerbCollection Verbs
        {
            get
            {
                DesignerVerbCollection verbs = new DesignerVerbCollection();
                verbs.Add(new DesignerVerb("Add Tab", new EventHandler(this.AddTabVerb)));
                return verbs;
            }
        }
    }
}

