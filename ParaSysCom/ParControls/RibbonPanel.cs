﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    [Designer(typeof(RibbonPanelDesigner)), DesignTimeVisible(false)]
    public partial class RibbonPanel : Component, IRibbonElement, IContainsSelectableRibbonItems, IContainsRibbonComponents
    {
        private Rectangle _bounds;
        private bool _butonMoreSelected;
        private Rectangle _buttonMoreBounds;
        private bool _buttonMoreEnabled;
        private bool _buttonMorePressed;
        private bool _buttonMoreVisible;
        private Rectangle _contentBounds;
        private bool _enabled;
        private RibbonPanelFlowDirection _flowsTo;
        private System.Drawing.Image _image;
        private RibbonItemCollection _items;
        private Ribbon _owner;
        private RibbonTab _ownerTab;
        private Control _popUp;
        private bool _popupShowed;
        private bool _pressed;
        private bool _selected;
        private RibbonElementSizeMode _sizeMode;
        private object _tag;
        private string _text;
        internal Rectangle overflowBoundsBuffer;

        public event EventHandler ButtonMoreClick;

        public event EventHandler Click;

        public event EventHandler DoubleClick;

        public event MouseEventHandler MouseDown;

        public event MouseEventHandler MouseEnter;

        public event MouseEventHandler MouseLeave;

        public event MouseEventHandler MouseMove;

        public event MouseEventHandler MouseUp;

        public event PaintEventHandler Paint;

        public event EventHandler Resize;

        public RibbonPanel()
        {
            this._items = new RibbonItemCollection();
            this._sizeMode = RibbonElementSizeMode.None;
            this._flowsTo = RibbonPanelFlowDirection.Bottom;
            this._buttonMoreEnabled = true;
            this._buttonMoreVisible = true;
            this._items.SetOwnerPanel(this);
            this._enabled = true;
        }

        public RibbonPanel(string text)
            : this(text, RibbonPanelFlowDirection.Bottom)
        {
        }

        public RibbonPanel(string text, RibbonPanelFlowDirection flowsTo)
            : this(text, flowsTo, new RibbonItem[0])
        {
        }

        public RibbonPanel(string text, RibbonPanelFlowDirection flowsTo, IEnumerable<RibbonItem> items)
            : this()
        {
            this._text = text;
            this._flowsTo = flowsTo;
            this._items.AddRange(items);
        }

        private void CenterItems()
        {
            this.Items.CenterItemsInto(this.ContentBounds);
        }

        public IEnumerable<Component> GetAllChildComponents()
        {
            return this.Items.ToArray();
        }

        public Rectangle GetContentBounds()
        {
            return this.ContentBounds;
        }

        public IEnumerable<RibbonItem> GetItems()
        {
            return this.Items;
        }

        public Size MeasureSize(object sender, RibbonElementMeasureSizeEventArgs e)
        {
            Size empty = Size.Empty;
            Size size2 = Size.Empty;
            int height = this.OwnerTab.TabContentBounds.Height - this.Owner.PanelPadding.Vertical;
            size2.Width = (e.Graphics.MeasureString(this.Text, this.Owner.Font).ToSize().Width + this.Owner.PanelMargin.Horizontal) + 1;
            if (this.ButtonMoreVisible)
            {
                size2.Width += this.ButtonMoreBounds.Width + 3;
            }
            if (e.SizeMode == RibbonElementSizeMode.Overflow)
            {
                return new Size(RibbonButton.MeasureStringLargeSize(e.Graphics, this.Text, this.Owner.Font).Width + this.Owner.PanelMargin.Horizontal, height);
            }
            switch (this.FlowsTo)
            {
                case RibbonPanelFlowDirection.Bottom:
                    empty = this.MeasureSizeFlowsToBottom(sender, e);
                    break;

                case RibbonPanelFlowDirection.Right:
                    empty = this.MeasureSizeFlowsToRight(sender, e);
                    break;

                default:
                    empty = Size.Empty;
                    break;
            }
            return new Size(Math.Max(empty.Width, size2.Width), height);
        }

        private Size MeasureSizeFlowsToBottom(object sender, RibbonElementMeasureSizeEventArgs e)
        {
            int x = this.Owner.PanelMargin.Left + this.Owner.ItemPadding.Horizontal;
            int y = this.ContentBounds.Top + this.Owner.ItemPadding.Vertical;
            int right = 0;
            int bottom = 0;
            int num5 = ((this.OwnerTab.TabContentBounds.Height - this.Owner.TabContentMargin.Vertical) - this.Owner.PanelPadding.Vertical) - this.Owner.PanelMargin.Vertical;
            int num6 = 0;
            int num7 = 0;
            foreach (RibbonItem item in this.Items)
            {
                Size size = item.MeasureSize(this, new RibbonElementMeasureSizeEventArgs(e.Graphics, e.SizeMode));
                if ((y + size.Height) > this.ContentBounds.Bottom)
                {
                    y = this.ContentBounds.Top + this.Owner.ItemPadding.Vertical;
                    x = num6 + this.Owner.ItemPadding.Horizontal;
                }
                Rectangle rectangle = new Rectangle(x, y, size.Width, size.Height);
                right = rectangle.Right;
                bottom = rectangle.Bottom;
                y = (rectangle.Bottom + this.Owner.ItemPadding.Vertical) + 1;
                num6 = Math.Max(num6, right);
                num7 = Math.Max(num7, bottom);
            }
            return new Size(((num6 + this.Owner.ItemPadding.Right) + this.Owner.PanelMargin.Right) + 1, 0);
        }

        private Size MeasureSizeFlowsToRight(object sender, RibbonElementMeasureSizeEventArgs e)
        {
            int horizontal = this.Owner.PanelMargin.Horizontal;
            int num2 = 0;
            int num3 = 0;
            int num4 = 0;
            foreach (RibbonItem item in this.Items)
            {
                Size size = item.MeasureSize(this, e);
                horizontal += (size.Width + this.Owner.ItemPadding.Horizontal) + 1;
                num2 = Math.Max(num2, size.Width);
                num3 = Math.Max(num3, size.Height);
            }
            switch (e.SizeMode)
            {
                case RibbonElementSizeMode.Compact:
                    num4 = horizontal / 3;
                    break;

                case RibbonElementSizeMode.Medium:
                    num4 = horizontal / 2;
                    break;

                case RibbonElementSizeMode.Large:
                    num4 = horizontal / 1;
                    break;
            }
            num4 += this.Owner.PanelMargin.Horizontal;
            return new Size(Math.Max(num2, num4) + this.Owner.PanelMargin.Horizontal, 0);
        }

        protected void OnButtonMoreClick(EventArgs e)
        {
            if (this.ButtonMoreClick != null)
            {
                this.ButtonMoreClick(this, e);
            }
        }

        public virtual void OnClick(EventArgs e)
        {
            if (this.Enabled)
            {
                if (this.Click != null)
                {
                    this.Click(this, e);
                }
                if (this.Collapsed && (this.PopUp == null))
                {
                    this.ShowOverflowPopup();
                }
            }
        }

        public virtual void OnDoubleClick(EventArgs e)
        {
            if (this.Enabled && (this.DoubleClick != null))
            {
                this.DoubleClick(this, e);
            }
        }

        public virtual void OnMouseDown(MouseEventArgs e)
        {
            if (this.Enabled)
            {
                if (this.MouseDown != null)
                {
                    this.MouseDown(this, e);
                }
                this.SetPressed(true);
                bool buttonMoreSelected = false;
                if (!(((!this.ButtonMoreEnabled || !this.ButtonMoreVisible) || !this.ButtonMoreBounds.Contains(e.X, e.Y)) || this.Collapsed))
                {
                    this.SetMorePressed(true);
                    buttonMoreSelected = true;
                }
                else
                {
                    buttonMoreSelected = this.ButtonMoreSelected;
                    this.SetMorePressed(false);
                }
                if (buttonMoreSelected)
                {
                    this.Owner.Invalidate(this.Bounds);
                }
            }
        }

        public virtual void OnMouseEnter(MouseEventArgs e)
        {
            if (this.Enabled && (this.MouseEnter != null))
            {
                this.MouseEnter(this, e);
            }
        }

        public virtual void OnMouseLeave(MouseEventArgs e)
        {
            if (this.Enabled && (this.MouseLeave != null))
            {
                this.MouseLeave(this, e);
            }
        }

        public virtual void OnMouseMove(MouseEventArgs e)
        {
            if (this.Enabled)
            {
                if (this.MouseMove != null)
                {
                    this.MouseMove(this, e);
                }
                bool buttonMoreSelected = false;
                if (!(((!this.ButtonMoreEnabled || !this.ButtonMoreVisible) || !this.ButtonMoreBounds.Contains(e.X, e.Y)) || this.Collapsed))
                {
                    this.SetMoreSelected(true);
                    buttonMoreSelected = true;
                }
                else
                {
                    buttonMoreSelected = this.ButtonMoreSelected;
                    this.SetMoreSelected(false);
                }
                if (buttonMoreSelected)
                {
                    this.Owner.Invalidate(this.Bounds);
                }
            }
        }

        public virtual void OnMouseUp(MouseEventArgs e)
        {
            if (this.Enabled)
            {
                if (this.MouseUp != null)
                {
                    this.MouseUp(this, e);
                }
                if (!(((!this.ButtonMoreEnabled || !this.ButtonMoreVisible) || !this.ButtonMorePressed) || this.Collapsed))
                {
                    this.OnButtonMoreClick(EventArgs.Empty);
                }
                this.SetPressed(false);
                this.SetMorePressed(false);
            }
        }

        public virtual void OnPaint(object sender, RibbonElementPaintEventArgs e)
        {
            if (this.Paint != null)
            {
                this.Paint(this, new PaintEventArgs(e.Graphics, e.Clip));
            }
            if (this.PopupShowed && (e.Control == this.Owner))
            {
                RibbonPanel panel = new RibbonPanel(this.Text);
                panel.Image = this.Image;
                panel.SetSizeMode(RibbonElementSizeMode.Overflow);
                panel.SetBounds(this.overflowBoundsBuffer);
                panel.SetPressed(true);
                panel.SetOwner(this.Owner);
                this.Owner.Renderer.OnRenderRibbonPanelBackground(new RibbonPanelRenderEventArgs(this.Owner, e.Graphics, e.Clip, panel, e.Control));
                this.Owner.Renderer.OnRenderRibbonPanelText(new RibbonPanelRenderEventArgs(this.Owner, e.Graphics, e.Clip, panel, e.Control));
            }
            else
            {
                this.Owner.Renderer.OnRenderRibbonPanelBackground(new RibbonPanelRenderEventArgs(this.Owner, e.Graphics, e.Clip, this, e.Control));
                this.Owner.Renderer.OnRenderRibbonPanelText(new RibbonPanelRenderEventArgs(this.Owner, e.Graphics, e.Clip, this, e.Control));
            }
            if ((e.Mode != RibbonElementSizeMode.Overflow) || ((e.Control != null) && (e.Control == this.PopUp)))
            {
                foreach (RibbonItem item in this.Items)
                {
                    item.OnPaint(this, new RibbonElementPaintEventArgs(item.Bounds, e.Graphics, item.SizeMode));
                }
            }
        }

        protected virtual void OnResize(EventArgs e)
        {
            if (this.Resize != null)
            {
                this.Resize(this, e);
            }
        }

        public void SetBounds(Rectangle bounds)
        {
            bool flag = this._bounds != bounds;
            this._bounds = bounds;
            this.OnResize(EventArgs.Empty);
            if (this.Owner != null)
            {
                this._contentBounds = Rectangle.FromLTRB(bounds.X + this.Owner.PanelMargin.Left, bounds.Y + this.Owner.PanelMargin.Top, bounds.Right - this.Owner.PanelMargin.Right, bounds.Bottom - this.Owner.PanelMargin.Bottom);
            }
            if (this.ButtonMoreVisible)
            {
                this.SetMoreBounds(Rectangle.FromLTRB(bounds.Right - 15, this._contentBounds.Bottom + 1, bounds.Right, bounds.Bottom));
            }
            else
            {
                this.SetMoreBounds(Rectangle.Empty);
            }
        }

        internal void SetContentBounds(Rectangle contentBounds)
        {
            this._contentBounds = contentBounds;
        }

        internal void SetMoreBounds(Rectangle bounds)
        {
            this._buttonMoreBounds = bounds;
        }

        internal void SetMorePressed(bool pressed)
        {
            this._buttonMorePressed = pressed;
        }

        internal void SetMoreSelected(bool selected)
        {
            this._butonMoreSelected = selected;
        }

        internal void SetOwner(Ribbon owner)
        {
            this._owner = owner;
            this.Items.SetOwner(owner);
        }

        internal void SetOwnerTab(RibbonTab ownerTab)
        {
            this._ownerTab = ownerTab;
            this.Items.SetOwnerTab(this.OwnerTab);
        }

        public void SetPressed(bool pressed)
        {
            this._pressed = pressed;
        }

        internal void SetSelected(bool selected)
        {
            this._selected = selected;
        }

        internal void SetSizeMode(RibbonElementSizeMode sizeMode)
        {
            this._sizeMode = sizeMode;
            foreach (RibbonItem item in this.Items)
            {
                item.SetSizeMode(sizeMode);
            }
        }

        private void ShowOverflowPopup()
        {
            Rectangle bounds = this.Bounds;
            RibbonPanelPopup popup = new RibbonPanelPopup(this);
            Point screenLocation = this.Owner.PointToScreen(new Point(bounds.Left, bounds.Bottom));
            this.PopupShowed = true;
            popup.Show(screenLocation);
        }

        public Size SwitchToSize(Control ctl, Graphics g, RibbonElementSizeMode size)
        {
            Size size2 = this.MeasureSize(this, new RibbonElementMeasureSizeEventArgs(g, size));
            Rectangle bounds = new Rectangle(0, 0, size2.Width, size2.Height);
            this.SetBounds(bounds);
            this.UpdateItemsRegions(g, size);
            return size2;
        }

        public override string ToString()
        {
            return string.Format("Panel: {0} ({1})", this.Text, this.SizeMode);
        }

        internal void UpdateItemsRegions(Graphics g, RibbonElementSizeMode mode)
        {
            switch (this.FlowsTo)
            {
                case RibbonPanelFlowDirection.Bottom:
                    this.UpdateRegionsFlowsToBottom(g, mode);
                    break;

                case RibbonPanelFlowDirection.Right:
                    this.UpdateRegionsFlowsToRight(g, mode);
                    break;
            }
            this.CenterItems();
        }

        private void UpdateRegionsFlowsToBottom(Graphics g, RibbonElementSizeMode mode)
        {
            int x = this.ContentBounds.Left + this.Owner.ItemPadding.Horizontal;
            int y = this.ContentBounds.Top + this.Owner.ItemPadding.Vertical;
            int num3 = x;
            int bottom = 0;
            List<RibbonItem> items = new List<RibbonItem>();
            foreach (RibbonItem item in this.Items)
            {
                Size lastMeasuredSize = item.LastMeasuredSize;
                if ((y + lastMeasuredSize.Height) > this.ContentBounds.Bottom)
                {
                    y = this.ContentBounds.Top + this.Owner.ItemPadding.Vertical;
                    x = num3 + this.Owner.ItemPadding.Horizontal;
                    this.Items.CenterItemsVerticallyInto(items, this.ContentBounds);
                    items.Clear();
                }
                item.SetBounds(new Rectangle(x, y, lastMeasuredSize.Width, lastMeasuredSize.Height));
                num3 = Math.Max(item.Bounds.Right, num3);
                bottom = item.Bounds.Bottom;
                y = (item.Bounds.Bottom + this.Owner.ItemPadding.Vertical) + 1;
                items.Add(item);
            }
            this.Items.CenterItemsVerticallyInto(items, this.Items.GetItemsBounds());
        }

        private void UpdateRegionsFlowsToRight(Graphics g, RibbonElementSizeMode mode)
        {
            int left = this.ContentBounds.Left;
            int top = this.ContentBounds.Top;
            int num3 = (mode == RibbonElementSizeMode.Medium) ? 7 : 0;
            int num4 = 0;
            RibbonItem[] collection = this.Items.ToArray();
            int index = collection.Length - 1;
            while (index >= 0)
            {
                for (int i = 1; i <= index; i++)
                {
                    if (collection[i - 1].LastMeasuredSize.Width < collection[i].LastMeasuredSize.Width)
                    {
                        RibbonItem item = collection[i - 1];
                        collection[i - 1] = collection[i];
                        collection[i] = item;
                    }
                }
                index--;
            }
            List<RibbonItem> list = new List<RibbonItem>(collection);
            while (list.Count > 0)
            {
                RibbonItem item2 = list[0];
                list.Remove(item2);
                if ((left + item2.LastMeasuredSize.Width) > this.ContentBounds.Right)
                {
                    left = this.ContentBounds.Left;
                    top = ((num4 + this.Owner.ItemPadding.Vertical) + 1) + num3;
                }
                item2.SetBounds(new Rectangle(new Point(left, top), item2.LastMeasuredSize));
                left += item2.Bounds.Width + this.Owner.ItemPadding.Horizontal;
                num4 = Math.Max(num4, item2.Bounds.Bottom);
                int num7 = this.ContentBounds.Right - left;
                for (index = 0; index < list.Count; index++)
                {
                    if (list[index].LastMeasuredSize.Width < num7)
                    {
                        list[index].SetBounds(new Rectangle(new Point(left, top), list[index].LastMeasuredSize));
                        left += list[index].Bounds.Width + this.Owner.ItemPadding.Horizontal;
                        num4 = Math.Max(num4, list[index].Bounds.Bottom);
                        num7 = this.ContentBounds.Right - left;
                        list.RemoveAt(index);
                        index = 0;
                    }
                }
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Rectangle Bounds
        {
            get
            {
                return this._bounds;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public Rectangle ButtonMoreBounds
        {
            get
            {
                return this._buttonMoreBounds;
            }
        }

        [Description("Enables/Disables the \"More...\" button"), DefaultValue(true)]
        public bool ButtonMoreEnabled
        {
            get
            {
                return this._buttonMoreEnabled;
            }
            set
            {
                this._buttonMoreEnabled = value;
                if (this.Owner != null)
                {
                    this.Owner.OnRegionsChanged();
                }
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ButtonMorePressed
        {
            get
            {
                return this._buttonMorePressed;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ButtonMoreSelected
        {
            get
            {
                return this._butonMoreSelected;
            }
        }

        [DefaultValue(true), Description("Sets the visibility of the \"More...\" button")]
        public bool ButtonMoreVisible
        {
            get
            {
                return this._buttonMoreVisible;
            }
            set
            {
                this._buttonMoreVisible = value;
                if (this.Owner != null)
                {
                    this.Owner.OnRegionsChanged();
                }
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool Collapsed
        {
            get
            {
                return (this.SizeMode == RibbonElementSizeMode.Overflow);
            }
        }

        public Rectangle ContentBounds
        {
            get
            {
                return this._contentBounds;
            }
        }

        [Description("Sets if the panel should be enabled"), DefaultValue(true)]
        public bool Enabled
        {
            get
            {
                return this._enabled;
            }
            set
            {
                this._enabled = value;
                foreach (RibbonItem item in this.Items)
                {
                    item.Enabled = value;
                }
            }
        }

        [DefaultValue(0)]
        public RibbonPanelFlowDirection FlowsTo
        {
            get
            {
                return this._flowsTo;
            }
            set
            {
                this._flowsTo = value;
                if (this.Owner != null)
                {
                    this.Owner.OnRegionsChanged();
                }
            }
        }

        [DefaultValue((string)null)]
        public System.Drawing.Image Image
        {
            get
            {
                return this._image;
            }
            set
            {
                this._image = value;
                if (this.Owner != null)
                {
                    this.Owner.OnRegionsChanged();
                }
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public RibbonItemCollection Items
        {
            get
            {
                return this._items;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool OverflowMode
        {
            get
            {
                return (this.SizeMode == RibbonElementSizeMode.Overflow);
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public Ribbon Owner
        {
            get
            {
                return this._owner;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public RibbonTab OwnerTab
        {
            get
            {
                return this._ownerTab;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        internal Control PopUp
        {
            get
            {
                return this._popUp;
            }
            set
            {
                this._popUp = value;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        internal bool PopupShowed
        {
            get
            {
                return this._popupShowed;
            }
            set
            {
                this._popupShowed = value;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool Pressed
        {
            get
            {
                return this._pressed;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual bool Selected
        {
            get
            {
                return this._selected;
            }
            set
            {
                this._selected = value;
            }
        }

        public RibbonElementSizeMode SizeMode
        {
            get
            {
                return this._sizeMode;
            }
        }

        public object Tag
        {
            get
            {
                return this._tag;
            }
            set
            {
                this._tag = value;
            }
        }

        [Localizable(true)]
        public string Text
        {
            get
            {
                return this._text;
            }
            set
            {
                this._text = value;
                if (this.Owner != null)
                {
                    this.Owner.OnRegionsChanged();
                }
            }
        }
    }
}
