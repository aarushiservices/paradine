﻿namespace ParControls
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.Design;

    internal abstract class RibbonElementWithItemCollectionDesigner : ComponentDesigner
    {
        protected RibbonElementWithItemCollectionDesigner()
        {
        }

        protected virtual void AddButton(object sender, EventArgs e)
        {
            this.CreateItem(typeof(RibbonButton));
        }

        protected virtual void AddButtonList(object sender, EventArgs e)
        {
            this.CreateItem(typeof(RibbonButtonList));
        }

        protected virtual void AddColorChooser(object sender, EventArgs e)
        {
            this.CreateItem(typeof(RibbonColorChooser));
        }

        protected virtual void AddComboBox(object sender, EventArgs e)
        {
            this.CreateItem(typeof(RibbonComboBox));
        }

        protected virtual void AddDescriptionMenuItem(object sender, EventArgs e)
        {
            this.CreateItem(typeof(RibbonDescriptionMenuItem));
        }

        protected virtual void AddItemGroup(object sender, EventArgs e)
        {
            this.CreateItem(typeof(RibbonItemGroup));
        }

        protected virtual void AddSeparator(object sender, EventArgs e)
        {
            this.CreateItem(typeof(RibbonSeparator));
        }

        protected virtual void AddTextBox(object sender, EventArgs e)
        {
            this.CreateItem(typeof(RibbonTextBox));
        }

        private void CreateItem(Type t)
        {
            this.CreateItem(this.Ribbon, this.Collection, t);
        }

        protected virtual void CreateItem(ParControls.Ribbon ribbon, RibbonItemCollection collection, Type t)
        {
            IDesignerHost service = this.GetService(typeof(IDesignerHost)) as IDesignerHost;
            if (((service != null) && (collection != null)) && (ribbon != null))
            {
                DesignerTransaction transaction = service.CreateTransaction("AddRibbonItem_" + base.Component.Site.Name);
                MemberDescriptor member = TypeDescriptor.GetProperties(base.Component)["Items"];
                base.RaiseComponentChanging(member);
                RibbonItem item = service.CreateComponent(t) as RibbonItem;
                if (!(item is RibbonSeparator))
                {
                    item.Text = item.Site.Name;
                }
                collection.Add(item);
                ribbon.OnRegionsChanged();
                base.RaiseComponentChanged(member, null, null);
                transaction.Commit();
            }
        }

        protected virtual DesignerVerbCollection OnGetVerbs()
        {
            return new DesignerVerbCollection(new DesignerVerb[] { new DesignerVerb("Add Button", new EventHandler(this.AddButton)), new DesignerVerb("Add ButtonList", new EventHandler(this.AddButtonList)), new DesignerVerb("Add ItemGroup", new EventHandler(this.AddItemGroup)), new DesignerVerb("Add Separator", new EventHandler(this.AddSeparator)), new DesignerVerb("Add TextBox", new EventHandler(this.AddTextBox)), new DesignerVerb("Add ComboBox", new EventHandler(this.AddComboBox)), new DesignerVerb("Add ColorChooser", new EventHandler(this.AddColorChooser)) });
        }

        public abstract RibbonItemCollection Collection { get; }

        public abstract ParControls.Ribbon Ribbon { get; }

        public override DesignerVerbCollection Verbs
        {
            get
            {
                return this.OnGetVerbs();
            }
        }
    }
}

