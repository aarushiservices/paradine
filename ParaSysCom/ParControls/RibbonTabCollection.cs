﻿namespace ParControls
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    public sealed class RibbonTabCollection : List<RibbonTab>
    {
        private Ribbon _owner;

        internal RibbonTabCollection(Ribbon owner)
        {
            if (owner == null)
            {
                throw new ArgumentNullException("null");
            }
            this._owner = owner;
        }

        public void Add(RibbonTab item)
        {
            item.SetOwner(this.Owner);
            base.Add(item);
            this.Owner.OnRegionsChanged();
        }

        public void AddRange(IEnumerable<RibbonTab> items)
        {
            foreach (RibbonTab tab in items)
            {
                tab.SetOwner(this.Owner);
            }
            base.AddRange(items);
            this.Owner.OnRegionsChanged();
        }

        public void Insert(int index, RibbonTab item)
        {
            item.SetOwner(this.Owner);
            base.Insert(index, item);
            this.Owner.OnRegionsChanged();
        }

        public void Remove(RibbonTab context)
        {
            base.Remove(context);
            this.Owner.OnRegionsChanged();
        }

        public int RemoveAll(Predicate<RibbonTab> predicate)
        {
            throw new ApplicationException("RibbonTabCollection.RemoveAll function is not supported");
        }

        public void RemoveAt(int index)
        {
            base.RemoveAt(index);
            this.Owner.OnRegionsChanged();
        }

        public void RemoveRange(int index, int count)
        {
            base.RemoveRange(index, count);
            this.Owner.OnRegionsChanged();
        }

        internal void SetOwner(Ribbon owner)
        {
            this._owner = owner;
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Ribbon Owner
        {
            get
            {
                return this._owner;
            }
        }
    }
}

