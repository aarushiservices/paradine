﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    [Designer(typeof(RibbonItemGroupDesigner))]
    public partial class RibbonItemGroup : RibbonItem, IContainsSelectableRibbonItems, IContainsRibbonComponents    
    {
        private bool _drawBackground;
        private RibbonItemGroupItemCollection _items;

        public RibbonItemGroup()
        {
            this._items = new RibbonItemGroupItemCollection(this);
            this._drawBackground = true;
        }

        public RibbonItemGroup(IEnumerable<RibbonItem> items)
            : this()
        {
            this._items.AddRange(items);
        }

        public IEnumerable<Component> GetAllChildComponents()
        {
            return this.Items.ToArray();
        }

        public Rectangle GetContentBounds()
        {
            return Rectangle.FromLTRB(base.Bounds.Left + 1, base.Bounds.Top + 1, base.Bounds.Right - 1, base.Bounds.Bottom);
        }

        public IEnumerable<RibbonItem> GetItems()
        {
            return this.Items;
        }

        public override Size MeasureSize(object sender, RibbonElementMeasureSizeEventArgs e)
        {
            int num = 0x10;
            int num2 = 0;
            int num3 = 0x10;
            foreach (RibbonItem item in this.Items)
            {
                Size size = item.MeasureSize(this, new RibbonElementMeasureSizeEventArgs(e.Graphics, RibbonElementSizeMode.Compact));
                num2 += size.Width + 1;
                num3 = Math.Max(num3, size.Height);
            }
            num2--;
            num2 = Math.Max(num2, num);
            if ((this.Site != null) && this.Site.DesignMode)
            {
                num2 += 10;
            }
            Size size2 = new Size(num2, num3);
            base.SetLastMeasuredSize(size2);
            return size2;
        }

        public override void OnPaint(object sender, RibbonElementPaintEventArgs e)
        {
            if (this.DrawBackground)
            {
                base.Owner.Renderer.OnRenderRibbonItem(new RibbonItemRenderEventArgs(base.Owner, e.Graphics, e.Clip, this));
            }
            foreach (RibbonItem item in this.Items)
            {
                item.OnPaint(this, new RibbonElementPaintEventArgs(item.Bounds, e.Graphics, RibbonElementSizeMode.Compact));
            }
            if (this.DrawBackground)
            {
                base.Owner.Renderer.OnRenderRibbonItemBorder(new RibbonItemRenderEventArgs(base.Owner, e.Graphics, e.Clip, this));
            }
        }

        public override void SetBounds(Rectangle bounds)
        {
            base.SetBounds(bounds);
            int left = bounds.Left;
            foreach (RibbonItem item in this.Items)
            {
                item.SetBounds(new Rectangle(new Point(left, bounds.Top), item.LastMeasuredSize));
                left = item.Bounds.Right + 1;
            }
        }

        internal override void SetOwner(Ribbon owner)
        {
            base.SetOwner(owner);
            this.Items.SetOwner(owner);
        }

        internal override void SetOwnerPanel(RibbonPanel ownerPanel)
        {
            base.SetOwnerPanel(ownerPanel);
            this.Items.SetOwnerPanel(ownerPanel);
        }

        internal override void SetOwnerTab(RibbonTab ownerTab)
        {
            base.SetOwnerTab(ownerTab);
            this.Items.SetOwnerTab(ownerTab);
        }

        internal override void SetSizeMode(RibbonElementSizeMode sizeMode)
        {
            base.SetSizeMode(sizeMode);
            foreach (RibbonItem item in this.Items)
            {
                item.SetSizeMode(RibbonElementSizeMode.Compact);
            }
        }

        public override string ToString()
        {
            return ("Group: " + this.Items.Count + " item(s)");
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public override bool Checked
        {
            get
            {
                return base.Checked;
            }
            set
            {
                base.Checked = value;
            }
        }

        [DefaultValue(true), Description("Background drawing should be avoided when group contains only TextBoxes and ComboBoxes")]
        public bool DrawBackground
        {
            get
            {
                return this._drawBackground;
            }
            set
            {
                this._drawBackground = value;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public RibbonItem FirstItem
        {
            get
            {
                if (this.Items.Count > 0)
                {
                    return this.Items[0];
                }
                return null;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public RibbonItemGroupItemCollection Items
        {
            get
            {
                return this._items;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public RibbonItem LastItem
        {
            get
            {
                if (this.Items.Count > 0)
                {
                    return this.Items[this.Items.Count - 1];
                }
                return null;
            }
        }
    }
}
