﻿namespace ParControls
{
    using System.Collections.Generic;
    using System.Drawing;

    public interface IContainsSelectableRibbonItems
    {
        Rectangle GetContentBounds();
        IEnumerable<RibbonItem> GetItems();
    }
}

