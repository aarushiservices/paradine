﻿namespace ParControls
{
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;

    public class RibbonRenderer
    {
        private static ColorMatrix _disabledImageColorMatrix;

        public static Image CreateDisabledImage(Image normalImage)
        {
            ImageAttributes imageAttr = new ImageAttributes();
            imageAttr.ClearColorKey();
            imageAttr.SetColorMatrix(DisabledImageColorMatrix);
            Size size = normalImage.Size;
            Bitmap image = new Bitmap(size.Width, size.Height);
            Graphics graphics = Graphics.FromImage(image);
            graphics.DrawImage(normalImage, new Rectangle(0, 0, size.Width, size.Height), 0, 0, size.Width, size.Height, GraphicsUnit.Pixel, imageAttr);
            graphics.Dispose();
            return image;
        }

        internal static ColorMatrix MultiplyColorMatrix(float[][] matrix1, float[][] matrix2)
        {
            int num = 5;
            float[][] newColorMatrix = new float[num][];
            for (int i = 0; i < num; i++)
            {
                newColorMatrix[i] = new float[num];
            }
            float[] numArray2 = new float[num];
            for (int j = 0; j < num; j++)
            {
                for (int k = 0; k < num; k++)
                {
                    numArray2[k] = matrix1[k][j];
                }
                for (int m = 0; m < num; m++)
                {
                    float[] numArray3 = matrix2[m];
                    float num6 = 0f;
                    for (int n = 0; n < num; n++)
                    {
                        num6 += numArray3[n] * numArray2[n];
                    }
                    newColorMatrix[m][j] = num6;
                }
            }
            return new ColorMatrix(newColorMatrix);
        }

        public virtual void OnRenderDropDownBackground(RibbonCanvasEventArgs e)
        {
        }

        public virtual void OnRenderOrbDropDownBackground(RibbonOrbDropDownEventArgs e)
        {
        }

        public virtual void OnRenderPanelPopupBackground(RibbonCanvasEventArgs e)
        {
        }

        public virtual void OnRenderRibbonBackground(RibbonRenderEventArgs e)
        {
        }

        public virtual void OnRenderRibbonCaptionBar(RibbonRenderEventArgs e)
        {
        }

        public virtual void OnRenderRibbonItem(RibbonItemRenderEventArgs e)
        {
        }

        public virtual void OnRenderRibbonItemBorder(RibbonItemRenderEventArgs e)
        {
        }

        public virtual void OnRenderRibbonItemImage(RibbonItemBoundsEventArgs e)
        {
        }

        public virtual void OnRenderRibbonItemText(RibbonTextEventArgs e)
        {
        }

        public virtual void OnRenderRibbonOrb(RibbonRenderEventArgs e)
        {
        }

        public virtual void OnRenderRibbonPanelBackground(RibbonPanelRenderEventArgs e)
        {
        }

        public virtual void OnRenderRibbonPanelText(RibbonPanelRenderEventArgs e)
        {
        }

        public virtual void OnRenderRibbonQuickAccessToolbarBackground(RibbonRenderEventArgs e)
        {
        }

        public virtual void OnRenderRibbonTab(RibbonTabRenderEventArgs e)
        {
        }

        public virtual void OnRenderRibbonTabContentBackground(RibbonTabRenderEventArgs e)
        {
        }

        public virtual void OnRenderRibbonTabText(RibbonTabRenderEventArgs e)
        {
        }

        public virtual void OnRenderTabScrollButtons(RibbonTabRenderEventArgs e)
        {
        }

        private static ColorMatrix DisabledImageColorMatrix
        {
            get
            {
                if (_disabledImageColorMatrix == null)
                {
                    float[][] numArray = new float[5][];
                    numArray[0] = new float[] { 0.2125f, 0.2125f, 0.2125f, 0f, 0f };
                    numArray[1] = new float[] { 0.2577f, 0.2577f, 0.2577f, 0f, 0f };
                    numArray[2] = new float[] { 0.0361f, 0.0361f, 0.0361f, 0f, 0f };
                    float[] numArray2 = new float[5];
                    numArray2[3] = 1f;
                    numArray[3] = numArray2;
                    numArray[4] = new float[] { 0.38f, 0.38f, 0.38f, 0f, 1f };
                    float[][] numArray3 = new float[5][];
                    float[] numArray4 = new float[5];
                    numArray4[0] = 1f;
                    numArray3[0] = numArray4;
                    float[] numArray5 = new float[5];
                    numArray5[1] = 1f;
                    numArray3[1] = numArray5;
                    float[] numArray6 = new float[5];
                    numArray6[2] = 1f;
                    numArray3[2] = numArray6;
                    float[] numArray7 = new float[5];
                    numArray7[3] = 0.7f;
                    numArray3[3] = numArray7;
                    numArray3[4] = new float[5];
                    _disabledImageColorMatrix = MultiplyColorMatrix(numArray3, numArray);
                }
                return _disabledImageColorMatrix;
            }
        }
    }
}

