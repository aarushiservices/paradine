﻿namespace ParControls
{
    using System;
    using System.Drawing;

    public interface IRibbonElement
    {
        Size MeasureSize(object sender, RibbonElementMeasureSizeEventArgs e);
        void OnPaint(object sender, RibbonElementPaintEventArgs e);
        void SetBounds(Rectangle bounds);

        Rectangle Bounds { get; }
    }
}

