﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ParaSysCom;
using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace ParControls
{
    [Designer(typeof(ViewerDesigner)), ToolboxItem(true), ToolboxBitmap(typeof(Tabpanel))]
    public partial class Tabpanel : UserControl
    {
        public ParButton btnBack;
        public ParButton btnNext;
      //private Container components = null;
        private ParPanel parPanel1;
        private ParPanel pnlButtons;
        private ParButton vActiveButton = null;
        private ViewerPage vActivePage = null;
        private ViewerButtonCollection vButtons;
        private ViewerCollection vPages;

        public event CancelEventHandler CloseFromCancel;

        public Tabpanel()
        {
            this.vPages = new ViewerCollection(this);
            this.vButtons = new ViewerButtonCollection(this);
            this.InitializeComponent();
        }

        protected internal void ActivateButton(ParButton button)
        {
            this.vActiveButton = button;
            if (this.vActiveButton != null)
            {
                if (!base.Contains(this.vActiveButton))
                {
                    this.vActiveButton.Click += new EventHandler(this.Glb_Click);
                    this.pnlButtons.Controls.Add(this.vActiveButton);
                }
                this.vActiveButton.Dock = DockStyle.Left;
                this.vActiveButton.Visible = true;
            }
            if (this.vActiveButton != null)
            {
                this.vActiveButton.Invalidate();
            }
            else
            {
                base.Invalidate();
            }
        }

        protected internal void ActivateButton(int index)
        {
            ParButton button = this.vButtons[index];
            this.ActivateButton(button);
        }

        protected internal void ActivatePage(ViewerPage page)
        {
            this.vActivePage = page;
            if (this.vActivePage != null)
            {
                this.vActivePage.Parent = this;
                if (!base.Contains(this.vActivePage))
                {
                    base.Container.Add(this.vActivePage);
                }
                this.vActivePage.Dock = DockStyle.Fill;
                this.vActivePage.BringToFront();
                this.vActivePage.FocusFirstTabIndex();
            }
            if (this.PageIndex > 0)
            {
                this.btnBack.Enabled = true;
            }
            else
            {
                this.btnBack.Enabled = false;
            }
            if (!((this.vPages.IndexOf(this.vActivePage) >= (this.vPages.Count - 1)) || this.vActivePage.IsFinishPage))
            {
                this.btnNext.Enabled = true;
                this.btnNext.DialogResult = DialogResult.None;
            }
            else
            {
                this.btnNext.Enabled = false;
            }
            if (this.vActivePage != null)
            {
                this.vActivePage.Invalidate();
            }
            else
            {
                base.Invalidate();
            }
        }

        protected internal void ActivatePage(int index)
        {
            if ((index < 0) || (index >= this.vPages.Count))
            {
                this.btnNext.Enabled = false;
                this.btnBack.Enabled = false;
            }
            else
            {
                ViewerPage page = this.vPages[index];
                this.ActivatePage(page);
            }
        }

        public void Back()
        {
            Debug.Assert(this.PageIndex < this.vPages.Count, "Page Index was beyond Maximum pages");
            Debug.Assert((this.PageIndex > 0) && (this.PageIndex < this.vPages.Count), "Attempted to go back to a page that doesn't exist");
            int index = this.vActivePage.OnCloseFromBack(this);
            this.ActivatePage(index);
            this.vActivePage.OnShowFromBack(this);
        }

        public void BackTo(ViewerPage page)
        {
            this.ActivatePage(page);
            this.vActivePage.OnShowFromNext(this);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Back();
        }

        private void btnBack_MouseDown(object sender, MouseEventArgs e)
        {
            if (base.DesignMode)
            {
                this.Back();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            CancelEventArgs args = new CancelEventArgs();
            if (this.CloseFromCancel != null)
            {
                this.CloseFromCancel(this, args);
            }
            if (!args.Cancel)
            {
                base.FindForm().Close();
            }
        }

        private void btnConfiguration_Click(object sender, EventArgs e)
        {
            this.ActivatePage(2);
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            this.ActivatePage(1);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            this.Next();
        }

        private void btnNext_MouseDown(object sender, MouseEventArgs e)
        {
            if (base.DesignMode)
            {
                this.Next();
            }
        }

        private void btnReports_Click(object sender, EventArgs e)
        {
            this.ActivatePage(4);
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            this.ActivatePage(3);
        }

        private void btnTools_Click(object sender, EventArgs e)
        {
            this.ActivatePage(5);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.ActivatePage(6);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.ActivatePage(0);
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.ActivatePage(7);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.ActivatePage(3);
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            this.ActivatePage(8);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.ActivatePage(1);
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            this.ActivatePage(9);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.ActivatePage(4);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.ActivatePage(2);
        }

        private void Glb_Click(object sender, EventArgs e)
        {
            foreach (ViewerPage page in this.vPages)
            {
                if ((page.Tag != null) && (((Control)sender).Tag.ToString() == page.Tag.ToString()))
                {
                    this.ActivatePage(page);
                }
            }
        }

        private void GLB_MouseEnter(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            button.Size += new Size(10, 10);
            button.Location = new Point(button.Location.X - 5, button.Location.Y - 5);
        }

        private void GLB_MouseLeave(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            button.Size -= new Size(10, 10);
            button.Location = new Point(button.Location.X + 5, button.Location.Y + 5);
        }

     

        public void Next()
        {
            Debug.Assert(this.PageIndex >= 0, "Page Index was below 0");
            int index = this.vActivePage.OnCloseFromNext(this);
            if ((this.PageIndex < (this.vPages.Count - 1)) && (!this.vActivePage.IsFinishPage || base.DesignMode))
            {
                this.ActivatePage(index);
                this.vActivePage.OnShowFromNext(this);
            }
            else
            {
                Debug.Assert(this.PageIndex < this.vPages.Count, "Error I've just gone past the finish", "btnNext_Click tried to go to page " + Convert.ToString((int)(this.PageIndex + 1)) + ", but I only have " + Convert.ToString(this.vPages.Count));
                if (!base.DesignMode)
                {
                    base.ParentForm.Close();
                }
            }
        }

        public void NextTo(ViewerPage page)
        {
            this.ActivatePage(page);
            this.vActivePage.OnShowFromNext(this);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            if (base.DesignMode)
            {
                SizeF ef = e.Graphics.MeasureString("No wizard pages inside the wizard.", this.Font);
                RectangleF layoutRectangle = new RectangleF((base.Width - ef.Width) / 2f, (this.pnlButtons.Top - ef.Height) / 2f, ef.Width, ef.Height);
                Pen pen = (Pen)SystemPens.GrayText.Clone();
                pen.DashStyle = DashStyle.Dash;
                e.Graphics.DrawRectangle(pen, (int)(base.Left + 8), (int)(base.Top + 8), (int)(base.Width - 0x11), (int)(this.pnlButtons.Top - 0x11));
                e.Graphics.DrawString("No wizard pages inside the wizard.", this.Font, new SolidBrush(SystemColors.GrayText), layoutRectangle);
            }
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            if (base.DesignMode)
            {
                base.Invalidate();
            }
        }

        private void pnlButtons_Paint(object sender, PaintEventArgs e)
        {
        }

        private void Wizard_Load(object sender, EventArgs e)
        {
            this.ActivatePage(0);
            foreach (ParButton button in this.vButtons)
            {
                this.ActivateButton(button);
            }
            Form form = base.FindForm();
        }

        [Category("Wizard"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool BackEnabled
        {
            get
            {
                return this.btnBack.Enabled;
            }
            set
            {
                this.btnBack.Enabled = value;
            }
        }

        public ParButton button
        {
            get
            {
                return this.vActiveButton;
            }
        }

        [Category("Wizard")]
        internal int ButtonIndex
        {
            get
            {
                return this.vButtons.IndexOf(this.vActiveButton);
            }
            set
            {
                if (this.vButtons.Count == 0)
                {
                    this.ActivateButton(-1);
                }
                else
                {
                    if ((value < -1) || (value >= this.vButtons.Count))
                    {
                        throw new ArgumentOutOfRangeException("ButtonIndex", value, "The button index must be between 0 and " + Convert.ToString((int)(this.vPages.Count - 1)));
                    }
                    this.ActivateButton(value);
                }
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content), Category("Wizard")]
        public ViewerButtonCollection Buttons
        {
            get
            {
                return this.vButtons;
            }
        }

        [Category("Wizard"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool NextEnabled
        {
            get
            {
                return this.btnNext.Enabled;
            }
            set
            {
                this.btnNext.Enabled = value;
            }
        }

        public ViewerPage Page
        {
            get
            {
                return this.vActivePage;
            }
        }

        [Category("Wizard")]
        internal int PageIndex
        {
            get
            {
                return this.vPages.IndexOf(this.vActivePage);
            }
            set
            {
                if (this.vPages.Count == 0)
                {
                    this.ActivatePage(-1);
                }
                else
                {
                    if ((value < -1) || (value >= this.vPages.Count))
                    {
                        throw new ArgumentOutOfRangeException("PageIndex", value, "The page index must be between 0 and " + Convert.ToString((int)(this.vPages.Count - 1)));
                    }
                    this.ActivatePage(value);
                }
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content), Category("Wizard")]
        public ViewerCollection Pages
        {
            get
            {
                return this.vPages;
            }
        }
    }
}
