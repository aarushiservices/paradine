﻿namespace ParControls
{
    using System;
    using System.Drawing;

    public interface IDropDownRibbonItem
    {
        Rectangle DropDownButtonBounds { get; }

        bool DropDownButtonPressed { get; }

        bool DropDownButtonSelected { get; }

        bool DropDownButtonVisible { get; }

        RibbonItemCollection DropDownItems { get; }
    }
}

