﻿using ParaSysCom;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace ParControls
{
    partial class Tabpanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(Tabpanel));
            this.pnlButtons = new ParPanel();
            this.parPanel1 = new ParPanel();
            this.btnNext = new ParButton();
            this.btnBack = new ParButton();
            this.pnlButtons.SuspendLayout();
            this.parPanel1.SuspendLayout();
            base.SuspendLayout();
            this.pnlButtons.BorderColor = Color.Gray;
            this.pnlButtons.Controls.Add(this.parPanel1);
            this.pnlButtons.Dock = DockStyle.Top;
            this.pnlButtons.GradientEndColor = Color.FromArgb(0xbf, 0xdb, 0xff);
            this.pnlButtons.GradientStartColor = Color.White;
            this.pnlButtons.Image = null;
            this.pnlButtons.ImageLocation = new Point(4, 4);
            this.pnlButtons.Location = new Point(0, 0);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.RoundCornerRadius = 2;
            this.pnlButtons.ShadowOffSet = 0;
            this.pnlButtons.Size = new Size(0x1ff, 0x1d);
            this.pnlButtons.TabIndex = 6;
            this.pnlButtons.Tag = "PnlButtons";
            this.parPanel1.BackColor = Color.Transparent;
            this.parPanel1.BorderColor = Color.Transparent;
            this.parPanel1.Controls.Add(this.btnBack);
            this.parPanel1.Controls.Add(this.btnNext);
            this.parPanel1.Dock = DockStyle.Right;
            this.parPanel1.GradientEndColor = Color.Transparent;
            this.parPanel1.GradientStartColor = Color.Transparent;
            this.parPanel1.Image = null;
            this.parPanel1.ImageLocation = new Point(4, 4);
            this.parPanel1.Location = new Point(0x178, 0);
            this.parPanel1.Name = "parPanel1";
            this.parPanel1.RoundCornerRadius = 2;
            this.parPanel1.ShadowOffSet = 0;
            this.parPanel1.Size = new Size(0x87, 0x1d);
            this.parPanel1.TabIndex = 5;
            this.btnNext.BackColor = Color.Teal;
            this.btnNext.BackgroundImage = (Image)manager.GetObject("btnNext.BackgroundImage");
            this.btnNext.BackgroundImageLayout = ImageLayout.Stretch;
            this.btnNext.Dock = DockStyle.Right;
            this.btnNext.FlatAppearance.BorderSize = 0;
            this.btnNext.FlatStyle = FlatStyle.Flat;
            this.btnNext.Location = new Point(70, 0);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new Size(0x41, 0x1d);
            this.btnNext.TabIndex = 0;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new EventHandler(this.btnNext_Click);
            this.btnBack.BackColor = Color.Teal;
            this.btnBack.BackgroundImage = (Image)manager.GetObject("btnBack.BackgroundImage");
            this.btnBack.BackgroundImageLayout = ImageLayout.Stretch;
            this.btnBack.Dock = DockStyle.Right;
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.FlatStyle = FlatStyle.Flat;
            this.btnBack.Location = new Point(5, 0);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new Size(0x41, 0x1d);
            this.btnBack.TabIndex = 1;
            this.btnBack.Text = "Previous";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new EventHandler(this.btnBack_Click);
            this.BackColor = Color.White;
            base.Controls.Add(this.pnlButtons);
            this.Font = new Font("Tahoma", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            base.Name = "Viewer";
            base.Size = new Size(0x1ff, 0x139);
            base.Load += new EventHandler(this.Wizard_Load);
            this.pnlButtons.ResumeLayout(false);
            this.parPanel1.ResumeLayout(false);
            base.ResumeLayout(false);
        }

        #endregion
    }
}
