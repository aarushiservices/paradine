﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    [ToolboxItem(false)]
    public partial class RibbonContext : Component
    {
        private Color _glowColor;
        private Ribbon _owner;
        private RibbonTabCollection _tabs;
        private string _text;

        public RibbonContext(Ribbon owner)
        {
            this._tabs = new RibbonTabCollection(owner);
        }

        internal void SetOwner(Ribbon owner)
        {
            this._owner = owner;
            this._tabs.SetOwner(owner);
        }

        public Color GlowColor
        {
            get
            {
                return this._glowColor;
            }
            set
            {
                this._glowColor = value;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public Ribbon Owner
        {
            get
            {
                return this._owner;
            }
        }

        public RibbonTabCollection Tabs
        {
            get
            {
                return this._tabs;
            }
        }

        public string Text
        {
            get
            {
                return this._text;
            }
            set
            {
                this._text = value;
            }
        }
    }
}
