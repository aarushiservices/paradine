﻿namespace ParControls
{
    using System;
    using System.Drawing;

    public class RibbonItemRenderEventArgs : RibbonRenderEventArgs
    {
        private RibbonItem _item;

        public RibbonItemRenderEventArgs(Ribbon owner, Graphics g, Rectangle clip, RibbonItem item) : base(owner, g, clip)
        {
            this.Item = item;
        }

        public RibbonItem Item
        {
            get
            {
                return this._item;
            }
            set
            {
                this._item = value;
            }
        }
    }
}

