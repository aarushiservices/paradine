﻿namespace ParControls
{
    using System;
    using System.Drawing;

    public class RibbonItemBoundsEventArgs : RibbonItemRenderEventArgs
    {
        private Rectangle _bounds;

        public RibbonItemBoundsEventArgs(Ribbon owner, Graphics g, Rectangle clip, RibbonItem item, Rectangle bounds) : base(owner, g, clip, item)
        {
            this.Bounds = bounds;
        }

        public Rectangle Bounds
        {
            get
            {
                return this._bounds;
            }
            set
            {
                this._bounds = value;
            }
        }
    }
}

