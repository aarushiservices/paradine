﻿namespace ParControls
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    public sealed class RibbonContextCollection : List<RibbonContext>
    {
        private Ribbon _owner;

        internal RibbonContextCollection(Ribbon owner)
        {
            if (owner == null)
            {
                throw new ArgumentNullException("owner");
            }
            this._owner = owner;
        }

        public new void Add(RibbonContext item)
        {
            item.SetOwner(this.Owner);
            this.Owner.Tabs.AddRange(item.Tabs);
            base.Add(item);
        }

        public new void AddRange(IEnumerable<RibbonContext> items)
        {
            foreach (RibbonContext context in items)
            {
                context.SetOwner(this.Owner);
                this.Owner.Tabs.AddRange(context.Tabs);
            }
            base.AddRange(items);
        }

        public new  void Insert(int index, RibbonContext item)
        {
            item.SetOwner(this.Owner);
            this.Owner.Tabs.InsertRange(index, item.Tabs);
            base.Insert(index, item);
        }

        public new void Remove(RibbonContext context)
        {
            base.Remove(context);
            foreach (RibbonTab tab in context.Tabs)
            {
                this.Owner.Tabs.Remove(tab);
            }
        }

        public new int RemoveAll(Predicate<RibbonContext> predicate)
        {
            throw new ApplicationException("RibbonContextCollectin.RemoveAll function is not supported");
        }

        public new void RemoveAt(int index)
        {
            base.RemoveAt(index);
            RibbonContext context = base[index];
            foreach (RibbonTab tab in context.Tabs)
            {
                this.Owner.Tabs.Remove(tab);
            }
        }

        public new void RemoveRange(int index, int count)
        {
            throw new ApplicationException("RibbonContextCollection.RemoveRange function is not supported");
        }

        internal void SetOwner(Ribbon owner)
        {
            if (owner == null)
            {
                throw new ArgumentNullException("owner");
            }
            this._owner = owner;
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public Ribbon Owner
        {
            get
            {
                return this._owner;
            }
        }
    }
}

