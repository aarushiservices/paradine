﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;

namespace ParControls.RibbonHelpers
{
    public static class WinApi
    {
        public const int BI_RGB = 0;
        public const int CS_DROPSHADOW = 0x20000;
        public const int DIB_RGB_COLORS = 0;
        private const int DT_CENTER = 1;
        private const int DT_NOPREFIX = 0x800;
        private const int DT_SINGLELINE = 0x20;
        private const int DT_VCENTER = 4;
        private const int DTT_COMPOSITED = 0x2000;
        private const int DTT_GLOWSIZE = 0x800;
        public const int SRCCOPY = 0xcc0020;
        public const byte VK_CAPITAL = 20;
        public const byte VK_NUMLOCK = 0x90;
        public const byte VK_SHIFT = 0x10;
        public const int WH_KEYBOARD = 2;
        public const int WH_KEYBOARD_LL = 13;
        public const int WH_MOUSE = 7;
        public const int WH_MOUSE_LL = 14;
        public const int WM_ERASEBKGND = 20;
        public const int WM_KEYDOWN = 0x100;
        public const int WM_KEYUP = 0x101;
        public const int WM_LBUTTONDBLCLK = 0x203;
        public const int WM_LBUTTONDOWN = 0x201;
        public const int WM_LBUTTONUP = 0x202;
        public const int WM_MBUTTONDBLCLK = 0x209;
        public const int WM_MBUTTONDOWN = 0x207;
        public const int WM_MBUTTONUP = 520;
        public const int WM_MOUSEFIRST = 0x200;
        public const int WM_MOUSELAST = 0x20d;
        public const int WM_MOUSEMOVE = 0x200;
        public const int WM_MOUSEWHEEL = 0x20a;
        public const int WM_NCCALCSIZE = 0x83;
        public const int WM_NCHITTEST = 0x84;
        public const int WM_NCLBUTTONUP = 0xa2;
        public const int WM_NCMOUSELEAVE = 0x2a2;
        public const int WM_NCMOUSEMOVE = 160;
        public const int WM_RBUTTONDBLCLK = 0x206;
        public const int WM_RBUTTONDOWN = 0x204;
        public const int WM_RBUTTONUP = 0x205;
        public const int WM_SIZE = 5;
        public const int WM_SYSKEYDOWN = 260;
        public const int WM_SYSKEYUP = 0x105;
        public const int WM_XBUTTONDBLCLK = 0x20d;
        public const int WM_XBUTTONDOWN = 0x20b;
        public const int WM_XBUTTONUP = 0x20c;

        [DllImport("gdi32.dll")]
        internal static extern bool BitBlt(IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, uint dwRop);
        [DllImport("user32.dll")]
        internal static extern int CallNextHookEx(int idHook, int nCode, IntPtr wParam, IntPtr lParam);
        [DllImport("gdi32.dll")]
        internal static extern IntPtr CreateCompatibleDC(IntPtr hDC);
        [DllImport("gdi32.dll")]
        private static extern IntPtr CreateDIBSection(IntPtr hdc, ref BITMAPINFO pbmi, uint iUsage, int ppvBits, IntPtr hSection, uint dwOffset);
        [DllImport("gdi32.dll")]
        internal static extern bool DeleteDC(IntPtr hdc);
        [DllImport("gdi32.dll")]
        internal static extern bool DeleteObject(IntPtr hObject);
        public static void DrawTextOnGlass(IntPtr hwnd, string text, Font font, Rectangle ctlrct, int iglowSize)
        {
            if (IsGlassEnabled)
            {
                RECT rect = new RECT();
                RECT pRect = new RECT();
                rect.Left = ctlrct.Left;
                rect.Right = ctlrct.Right;
                rect.Top = ctlrct.Top;
                rect.Bottom = ctlrct.Bottom;
                pRect.Left = 0;
                pRect.Top = 0;
                pRect.Right = rect.Right - rect.Left;
                pRect.Bottom = rect.Bottom - rect.Top;
                IntPtr dC = GetDC(hwnd);
                IntPtr hdc = CreateCompatibleDC(dC);
                IntPtr zero = IntPtr.Zero;
                int dwFlags = 0x825;
                BITMAPINFO pbmi = new BITMAPINFO();
                pbmi.bmiHeader.biHeight = -(rect.Bottom - rect.Top);
                pbmi.bmiHeader.biWidth = rect.Right - rect.Left;
                pbmi.bmiHeader.biPlanes = 1;
                pbmi.bmiHeader.biSize = Marshal.SizeOf(typeof(BITMAPINFOHEADER));
                pbmi.bmiHeader.biBitCount = 0x20;
                pbmi.bmiHeader.biCompression = 0;
                if (SaveDC(hdc) != 0)
                {
                    IntPtr hObject = CreateDIBSection(hdc, ref pbmi, 0, 0, IntPtr.Zero, 0);
                    if (hObject != IntPtr.Zero)
                    {
                        zero = SelectObject(hdc, hObject);
                        IntPtr ptr6 = font.ToHfont();
                        IntPtr ptr5 = SelectObject(hdc, ptr6);
                        try
                        {
                            VisualStyleRenderer renderer = new VisualStyleRenderer(VisualStyleElement.Window.Caption.Active);
                            DTTOPTS pOptions = new DTTOPTS();
                            pOptions.dwSize = (uint)Marshal.SizeOf(typeof(DTTOPTS));
                            pOptions.dwFlags = 0x2800;
                            pOptions.iGlowSize = iglowSize;
                            int num2 = DrawThemeTextEx(renderer.Handle, hdc, 0, 0, text, -1, dwFlags, ref pRect, ref pOptions);
                            if (!BitBlt(dC, rect.Left, rect.Top, rect.Right - rect.Left, rect.Bottom - rect.Top, hdc, 0, 0, 0xcc0020))
                            {
                            }
                        }
                        catch (Exception exception)
                        {
                            Console.WriteLine(exception.ToString());
                        }
                        SelectObject(hdc, zero);
                        SelectObject(hdc, ptr5);
                        DeleteObject(hObject);
                        DeleteObject(ptr6);
                        ReleaseDC(hdc, -1);
                        DeleteDC(hdc);
                    }
                }
            }
        }

        [DllImport("UxTheme.dll")]
        internal static extern int DrawThemeText(IntPtr hTheme, IntPtr hdc, int iPartId, int iStateId, string text, int iCharCount, int dwFlags1, int dwFlags2, ref RECT pRect);
        [DllImport("UxTheme.dll", CharSet = CharSet.Unicode)]
        private static extern int DrawThemeTextEx(IntPtr hTheme, IntPtr hdc, int iPartId, int iStateId, string text, int iCharCount, int dwFlags, ref RECT pRect, ref DTTOPTS pOptions);
        [DllImport("dwmapi.dll")]
        internal static extern int DwmDefWindowProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, out IntPtr result);
        [DllImport("dwmapi.dll")]
        internal static extern int DwmExtendFrameIntoClientArea(IntPtr hdc, ref MARGINS marInset);
        [DllImport("dwmapi.dll")]
        internal static extern int DwmIsCompositionEnabled(ref int pfEnabled);
        public static void FillForGlass(Graphics g, Rectangle r)
        {
            RECT rect = new RECT();
            rect.Left = r.Left;
            rect.Right = r.Right;
            rect.Top = r.Top;
            rect.Bottom = r.Bottom;
            IntPtr hdc = g.GetHdc();
            IntPtr ptr2 = CreateCompatibleDC(hdc);
            IntPtr zero = IntPtr.Zero;
            BITMAPINFO pbmi = new BITMAPINFO();
            pbmi.bmiHeader.biHeight = -(rect.Bottom - rect.Top);
            pbmi.bmiHeader.biWidth = rect.Right - rect.Left;
            pbmi.bmiHeader.biPlanes = 1;
            pbmi.bmiHeader.biSize = Marshal.SizeOf(typeof(BITMAPINFOHEADER));
            pbmi.bmiHeader.biBitCount = 0x20;
            pbmi.bmiHeader.biCompression = 0;
            if (SaveDC(ptr2) != 0)
            {
                IntPtr hObject = CreateDIBSection(ptr2, ref pbmi, 0, 0, IntPtr.Zero, 0);
                if (!(hObject == IntPtr.Zero))
                {
                    zero = SelectObject(ptr2, hObject);
                    BitBlt(hdc, rect.Left, rect.Top, rect.Right - rect.Left, rect.Bottom - rect.Top, ptr2, 0, 0, 0xcc0020);
                }
                SelectObject(ptr2, zero);
                DeleteObject(hObject);
                ReleaseDC(ptr2, -1);
                DeleteDC(ptr2);
            }
            g.ReleaseHdc();
        }

        [DllImport("user32")]
        internal static extern bool GetCursorPos(out POINT lpPoint);
        [DllImport("user32.dll")]
        internal static extern IntPtr GetDC(IntPtr hdc);
        [DllImport("user32")]
        internal static extern int GetKeyboardState(byte[] pbKeyState);
        [DllImport("user32.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Auto)]
        internal static extern short GetKeyState(int vKey);
        public static int HiWord(int dwValue)
        {
            return ((dwValue >> 0x10) & 0xffff);
        }

        public static int LoWord(int dwValue)
        {
            return (dwValue & 0xffff);
        }

        public static IntPtr MakeLParam(int LoWord, int HiWord)
        {
            return new IntPtr((HiWord << 0x10) | (LoWord & 0xffff));
        }

        [DllImport("user32.dll")]
        internal static extern int ReleaseDC(IntPtr hdc, int state);
        [DllImport("gdi32.dll")]
        internal static extern int SaveDC(IntPtr hdc);
        [DllImport("gdi32.dll")]
        internal static extern IntPtr SelectObject(IntPtr hDC, IntPtr hObject);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        internal static extern IntPtr SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);
        [DllImport("user32.dll")]
        internal static extern int SetWindowsHookEx(int idHook, GlobalHook.HookProcCallBack lpfn, IntPtr hInstance, int threadId);
        [DllImport("user32")]
        internal static extern int ToAscii(int uVirtKey, int uScanCode, byte[] lpbKeyState, byte[] lpwTransKey, int fuState);
        [DllImport("user32.dll")]
        internal static extern bool UnhookWindowsHookEx(int idHook);

        public static bool IsGlassEnabled
        {
            get
            {
                if (IsVista)
                {
                    int pfEnabled = 0;
                    int num2 = DwmIsCompositionEnabled(ref pfEnabled);
                    return (pfEnabled > 0);
                }
                return false;
            }
        }

        public static bool IsVista
        {
            get
            {
                return (IsWindows && (Environment.OSVersion.Version.Major >= 6));
            }
        }

        public static bool IsWindows
        {
            get
            {
                return (Environment.OSVersion.Platform == PlatformID.Win32NT);
            }
        }

        public static bool IsXP
        {
            get
            {
                return (IsWindows && (Environment.OSVersion.Version.Major >= 5));
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct BITMAPINFO
        {
            public WinApi.BITMAPINFOHEADER bmiHeader;
            public WinApi.RGBQUAD bmiColors;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct BITMAPINFOHEADER
        {
            public int biSize;
            public int biWidth;
            public int biHeight;
            public short biPlanes;
            public short biBitCount;
            public int biCompression;
            public int biSizeImage;
            public int biXPelsPerMeter;
            public int biYPelsPerMeter;
            public int biClrUsed;
            public int biClrImportant;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct DTTOPTS
        {
            public uint dwSize;
            public uint dwFlags;
            public uint crText;
            public uint crBorder;
            public uint crShadow;
            public int iTextShadowType;
            public WinApi.POINT ptShadowOffset;
            public int iBorderSize;
            public int iFontPropId;
            public int iColorPropId;
            public int iStateId;
            public int fApplyOverlay;
            public int iGlowSize;
            public IntPtr pfnDrawTextCallback;
            public int lParam;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal class KeyboardLLHookStruct
        {
            public int vkCode;
            public int scanCode;
            public int flags;
            public int time;
            public int dwExtraInfo;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct MARGINS
        {
            public int cxLeftWidth;
            public int cxRightWidth;
            public int cyTopHeight;
            public int cyBottomHeight;
            public MARGINS(int Left, int Right, int Top, int Bottom)
            {
                this.cxLeftWidth = Left;
                this.cxRightWidth = Right;
                this.cyTopHeight = Top;
                this.cyBottomHeight = Bottom;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        internal class MouseHookStruct
        {
            public WinApi.POINT pt;
            public int hwnd;
            public int wHitTestCode;
            public int dwExtraInfo;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal class MouseLLHookStruct
        {
            public WinApi.POINT pt;
            public int mouseData;
            public int flags;
            public int time;
            public int extraInfo;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct NCCALCSIZE_PARAMS
        {
            public WinApi.RECT rect0;
            public WinApi.RECT rect1;
            public WinApi.RECT rect2;
            public IntPtr lppos;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct POINT
        {
            public int x;
            public int y;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct RGBQUAD
        {
            public byte rgbBlue;
            public byte rgbGreen;
            public byte rgbRed;
            public byte rgbReserved;
        }
    }
}
