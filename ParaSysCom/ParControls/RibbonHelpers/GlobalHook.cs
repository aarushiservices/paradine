﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls.RibbonHelpers
{
    public class GlobalHook : IDisposable
    {
        private int _hHook;
        private HookProcCallBack _HookProc;
        private HookTypes _hookType;

        public event KeyEventHandler KeyDown;

        public event KeyPressEventHandler KeyPress;

        public event KeyEventHandler KeyUp;

        public event MouseEventHandler MouseClick;

        public event MouseEventHandler MouseDoubleClick;

        public event MouseEventHandler MouseDown;

        public event MouseEventHandler MouseMove;

        public event MouseEventHandler MouseUp;

        public event MouseEventHandler MouseWheel;

        public GlobalHook(HookTypes hookType)
        {
            this._hookType = hookType;
            this.InstallHook();
        }

        public void Dispose()
        {
            if (this.Handle != 0)
            {
                this.Unhook();
            }
        }

        ~GlobalHook()
        {
            if (this.Handle != 0)
            {
                this.Unhook();
            }
        }

        private int HookProc(int code, IntPtr wParam, IntPtr lParam)
        {
            if (code < 0)
            {
                return WinApi.CallNextHookEx(this.Handle, code, wParam, lParam);
            }
            switch (this.HookType)
            {
                case HookTypes.Mouse:
                    return this.MouseProc(code, wParam, lParam);

                case HookTypes.Keyboard:
                    return this.KeyboardProc(code, wParam, lParam);
            }
            throw new Exception("HookType not supported");
        }

        private void InstallHook()
        {
            if (this.Handle != 0)
            {
                throw new Exception("Hook is already installed");
            }
            int idHook = 0;
            switch (this.HookType)
            {
                case HookTypes.Mouse:
                    idHook = 14;
                    break;

                case HookTypes.Keyboard:
                    idHook = 13;
                    break;

                default:
                    throw new Exception("HookType is not supported");
            }
            this._HookProc = new HookProcCallBack(this.HookProc);
            this._hHook = WinApi.SetWindowsHookEx(idHook, this._HookProc, Marshal.GetHINSTANCE(Assembly.GetExecutingAssembly().GetModules()[0]), 0);
            if (this.Handle == 0)
            {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }
        }

        private int KeyboardProc(int code, IntPtr wParam, IntPtr lParam)
        {
            KeyEventArgs args;
            WinApi.KeyboardLLHookStruct struct2 = (WinApi.KeyboardLLHookStruct)Marshal.PtrToStructure(lParam, typeof(WinApi.KeyboardLLHookStruct));
            int num = wParam.ToInt32();
            bool handled = false;
            if ((num == 0x100) || (num == 260))
            {
                args = new KeyEventArgs((Keys)struct2.vkCode);
                this.OnKeyDown(args);
                handled = args.Handled;
            }
            else if ((num == 0x101) || (num == 0x105))
            {
                args = new KeyEventArgs((Keys)struct2.vkCode);
                this.OnKeyUp(args);
                handled = args.Handled;
            }
            if ((num == 0x100) && (this.KeyPress != null))
            {
                byte[] pbKeyState = new byte[0x100];
                byte[] lpwTransKey = new byte[2];
                WinApi.GetKeyboardState(pbKeyState);
                switch (WinApi.ToAscii(struct2.vkCode, struct2.scanCode, pbKeyState, lpwTransKey, struct2.flags))
                {
                    case 1:
                    case 2:
                        {
                            bool flag2 = (WinApi.GetKeyState(0x10) & 0x80) == 0x80;
                            bool flag3 = WinApi.GetKeyState(20) != 0;
                            char c = (char)lpwTransKey[0];
                            if ((flag2 ^ flag3) && char.IsLetter(c))
                            {
                                c = char.ToUpper(c);
                            }
                            KeyPressEventArgs e = new KeyPressEventArgs(c);
                            this.OnKeyPress(e);
                            handled |= e.Handled;
                            break;
                        }
                }
            }
            return (handled ? 1 : WinApi.CallNextHookEx(this.Handle, code, wParam, lParam));
        }

        private int MouseProc(int code, IntPtr wParam, IntPtr lParam)
        {
            WinApi.MouseLLHookStruct struct2 = (WinApi.MouseLLHookStruct)Marshal.PtrToStructure(lParam, typeof(WinApi.MouseLLHookStruct));
            int num = wParam.ToInt32();
            int x = struct2.pt.x;
            int y = struct2.pt.y;
            int delta = (short)((struct2.mouseData >> 0x10) & 0xffff);
            switch (num)
            {
                case 0x20a:
                    this.OnMouseWheel(new MouseEventArgs(MouseButtons.None, 0, x, y, delta));
                    break;

                case 0x200:
                    this.OnMouseMove(new MouseEventArgs(MouseButtons.None, 0, x, y, delta));
                    break;

                case 0x203:
                    this.OnMouseDoubleClick(new MouseEventArgs(MouseButtons.Left, 0, x, y, delta));
                    break;

                case 0x201:
                    this.OnMouseDown(new MouseEventArgs(MouseButtons.Left, 0, x, y, delta));
                    break;

                case 0x202:
                    this.OnMouseUp(new MouseEventArgs(MouseButtons.Left, 0, x, y, delta));
                    this.OnMouseClick(new MouseEventArgs(MouseButtons.Left, 0, x, y, delta));
                    break;

                case 0x209:
                    this.OnMouseDoubleClick(new MouseEventArgs(MouseButtons.Middle, 0, x, y, delta));
                    break;

                case 0x207:
                    this.OnMouseDown(new MouseEventArgs(MouseButtons.Middle, 0, x, y, delta));
                    break;

                case 520:
                    this.OnMouseUp(new MouseEventArgs(MouseButtons.Middle, 0, x, y, delta));
                    break;

                case 0x206:
                    this.OnMouseDoubleClick(new MouseEventArgs(MouseButtons.Right, 0, x, y, delta));
                    break;

                case 0x204:
                    this.OnMouseDown(new MouseEventArgs(MouseButtons.Right, 0, x, y, delta));
                    break;

                case 0x205:
                    this.OnMouseUp(new MouseEventArgs(MouseButtons.Right, 0, x, y, delta));
                    break;

                case 0x20d:
                    this.OnMouseDoubleClick(new MouseEventArgs(MouseButtons.XButton1, 0, x, y, delta));
                    break;

                case 0x20b:
                    this.OnMouseDown(new MouseEventArgs(MouseButtons.XButton1, 0, x, y, delta));
                    break;

                case 0x20c:
                    this.OnMouseUp(new MouseEventArgs(MouseButtons.XButton1, 0, x, y, delta));
                    break;
            }
            return WinApi.CallNextHookEx(this.Handle, code, wParam, lParam);
        }

        protected virtual void OnKeyDown(KeyEventArgs e)
        {
            if (this.KeyDown != null)
            {
                this.KeyDown(this, e);
            }
        }

        protected virtual void OnKeyPress(KeyPressEventArgs e)
        {
            if (this.KeyPress != null)
            {
                this.KeyPress(this, e);
            }
        }

        protected virtual void OnKeyUp(KeyEventArgs e)
        {
            if (this.KeyUp != null)
            {
                this.KeyUp(this, e);
            }
        }

        protected virtual void OnMouseClick(MouseEventArgs e)
        {
            if (this.MouseClick != null)
            {
                this.MouseClick(this, e);
            }
        }

        protected virtual void OnMouseDoubleClick(MouseEventArgs e)
        {
            if (this.MouseDoubleClick != null)
            {
                this.MouseDoubleClick(this, e);
            }
        }

        protected virtual void OnMouseDown(MouseEventArgs e)
        {
            if (this.MouseDown != null)
            {
                this.MouseDown(this, e);
            }
        }

        protected virtual void OnMouseMove(MouseEventArgs e)
        {
            if (this.MouseMove != null)
            {
                this.MouseMove(this, e);
            }
        }

        protected virtual void OnMouseUp(MouseEventArgs e)
        {
            if (this.MouseUp != null)
            {
                this.MouseUp(this, e);
            }
        }

        protected virtual void OnMouseWheel(MouseEventArgs e)
        {
            if (this.MouseWheel != null)
            {
                this.MouseWheel(this, e);
            }
        }

        private void Unhook()
        {
            if ((this.Handle != 0) && !WinApi.UnhookWindowsHookEx(this.Handle))
            {
                this._hHook = 0;
            }
        }

        public int Handle
        {
            get
            {
                return this._hHook;
            }
        }

        public HookTypes HookType
        {
            get
            {
                return this._hookType;
            }
        }

        internal delegate int HookProcCallBack(int nCode, IntPtr wParam, IntPtr lParam);

        public enum HookTypes
        {
            Mouse,
            Keyboard
        }
    }
}
