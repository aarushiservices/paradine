﻿using ParControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    [Designer(typeof(RibbonOrbMenuItemDesigner))]
    public partial class RibbonOrbMenuItem : RibbonButton
    {
        public RibbonOrbMenuItem()
        {
            base.DropDownArrowDirection = RibbonArrowDirection.Left;
            base.SetDropDownMargin(new Padding(10));
            base.DropDownShowing += new EventHandler(this.RibbonOrbMenuItem_DropDownShowing);
        }

        public RibbonOrbMenuItem(string text)
            : this()
        {
            this.Text = text;
        }

        internal override Point OnGetDropDownMenuLocation()
        {
            if (base.Owner == null)
            {
                return base.OnGetDropDownMenuLocation();
            }
            Rectangle rectangle = base.Owner.RectangleToScreen(base.Bounds);
            Rectangle rectangle2 = base.Owner.OrbDropDown.RectangleToScreen(base.Owner.OrbDropDown.ContentRecentItemsBounds);
            return new Point(rectangle.Right, rectangle2.Top);
        }

        internal override Size OnGetDropDownMenuSize()
        {
            Rectangle contentRecentItemsBounds = base.Owner.OrbDropDown.ContentRecentItemsBounds;
            contentRecentItemsBounds.Inflate(-1, -1);
            return contentRecentItemsBounds.Size;
        }

        public override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            if (RibbonDesigner.Current == null)
            {
            }
        }

        public override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
        }

        private void RibbonOrbMenuItem_DropDownShowing(object sender, EventArgs e)
        {
            if (base.DropDown != null)
            {
                base.DropDown.DrawIconsBar = false;
            }
        }

        public override System.Drawing.Image Image
        {
            get
            {
                return base.Image;
            }
            set
            {
                base.Image = value;
                this.SmallImage = value;
            }
        }

        [Browsable(false)]
        public override System.Drawing.Image SmallImage
        {
            get
            {
                return base.SmallImage;
            }
            set
            {
                base.SmallImage = value;
            }
        }
    }
}
