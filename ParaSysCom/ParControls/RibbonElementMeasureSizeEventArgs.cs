﻿namespace ParControls
{
    using System;
    using System.Drawing;

    public class RibbonElementMeasureSizeEventArgs : EventArgs
    {
        private System.Drawing.Graphics _graphics;
        private RibbonElementSizeMode _sizeMode;

        internal RibbonElementMeasureSizeEventArgs(System.Drawing.Graphics graphics, RibbonElementSizeMode sizeMode)
        {
            this._graphics = graphics;
            this._sizeMode = sizeMode;
        }

        public System.Drawing.Graphics Graphics
        {
            get
            {
                return this._graphics;
            }
        }

        public RibbonElementSizeMode SizeMode
        {
            get
            {
                return this._sizeMode;
            }
        }
    }
}

