﻿namespace ParControls
{
    using System;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Windows.Forms;
    using System.Windows.Forms.Design.Behavior;

    public class RibbonQuickAccessToolbarGlyph : Glyph
    {
        private BehaviorService _behaviorService;
        private RibbonDesigner _componentDesigner;
        private Ribbon _ribbon;

        public RibbonQuickAccessToolbarGlyph(BehaviorService behaviorService, RibbonDesigner designer, Ribbon ribbon) : base(new RibbonQuickAccessGlyphBehavior(designer, ribbon))
        {
            this._behaviorService = behaviorService;
            this._componentDesigner = designer;
            this._ribbon = ribbon;
        }

        public override Cursor GetHitTest(Point p)
        {
            if (this.Bounds.Contains(p))
            {
                return Cursors.Hand;
            }
            return null;
        }

        public override void Paint(PaintEventArgs pe)
        {
            SmoothingMode smoothingMode = pe.Graphics.SmoothingMode;
            pe.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            using (SolidBrush brush = new SolidBrush(Color.FromArgb(50, Color.Blue)))
            {
                pe.Graphics.FillEllipse(brush, this.Bounds);
            }
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;
            pe.Graphics.DrawString("+", SystemFonts.DefaultFont, Brushes.White, this.Bounds, format);
            pe.Graphics.SmoothingMode = smoothingMode;
        }

        public override Rectangle Bounds
        {
            get
            {
                Point point = this._behaviorService.ControlToAdornerWindow(this._ribbon);
                return new Rectangle((((point.X + this._ribbon.QuickAcessToolbar.Bounds.Right) + (this._ribbon.QuickAcessToolbar.Bounds.Height / 2)) + 4) + this._ribbon.QuickAcessToolbar.DropDownButton.Bounds.Width, point.Y + this._ribbon.QuickAcessToolbar.Bounds.Top, this._ribbon.QuickAcessToolbar.Bounds.Height, this._ribbon.QuickAcessToolbar.Bounds.Height);
            }
        }
    }
}

