﻿using ParControls.RibbonHelpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    public partial class RibbonForm : Form, IRibbonForm
    {
        private RibbonFormHelper _helper;

        public RibbonForm()
        {
            if (!(!WinApi.IsWindows || WinApi.IsGlassEnabled))
            {
                base.SetStyle(ControlStyles.ResizeRedraw, true);
                base.SetStyle(ControlStyles.Opaque, WinApi.IsGlassEnabled);
                base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
                this.DoubleBuffered = true;
            }
            this._helper = new RibbonFormHelper(this);
        }

        protected override void OnNotifyMessage(Message m)
        {
            base.OnNotifyMessage(m);
            Console.WriteLine(m.ToString());
        }

        protected override void WndProc(ref Message m)
        {
            if (!this.Helper.WndProc(ref m))
            {
                base.WndProc(ref m);
            }
        }

        public RibbonFormHelper Helper
        {
            get
            {
                return this._helper;
            }
        }
    }
}
