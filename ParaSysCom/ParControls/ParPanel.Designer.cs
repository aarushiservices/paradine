﻿using System.Drawing;
using System.Windows.Forms;
namespace ParControls
{
    partial class ParPanel : Panel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private Color _borderColor = Color.Gray;
        private int _borderWidth = 1;
        private Color _gradientEndColor = Color.Gray;
        private Color _gradientStartColor = Color.White;
        private int _HeaderBorderHeight = 30;
        private string _HeaderText = "";
        private short _HeaderTextFontSize = 10;
        private StringAlignment _HearderAlignment = StringAlignment.Center;
        private System.Drawing.Image _image;
        private Point _imageLocation = new Point(4, 4);
        private int _roundCornerRadius = 4;
        private int _shadowOffSet = 5;      
       // public bool Licensed = false;

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
        }

        #endregion
    }
}
