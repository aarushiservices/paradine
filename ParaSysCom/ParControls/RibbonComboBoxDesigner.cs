﻿namespace ParControls
{
    internal class RibbonComboBoxDesigner : RibbonElementWithItemCollectionDesigner
    {
        public override RibbonItemCollection Collection
        {
            get
            {
                if (base.Component is RibbonComboBox)
                {
                    return (base.Component as RibbonComboBox).DropDownItems;
                }
                return null;
            }
        }

        public override ParControls.Ribbon Ribbon
        {
            get
            {
                if (base.Component is RibbonComboBox)
                {
                    return (base.Component as RibbonComboBox).Owner;
                }
                return null;
            }
        }
    }
}

