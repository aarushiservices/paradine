﻿using ParControls.RibbonHelpers;
using System.Drawing;
using System.Windows.Forms;
namespace ParControls
{
    partial class Ribbon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        private RibbonTab _activeTab;
        private RibbonWindowMode _actualBorderMode;
        private RibbonWindowMode _borderMode;
        private bool _CaptionButtonsVisible;
        private RibbonCaptionButton _CloseButton;
        private RibbonContextCollection _contexts;
        private Padding _dropDownMargin;
        private Padding _itemMargin;
        private Padding _itemPadding;
        private GlobalHook _keyboardHook;
        private RibbonTab _lastSelectedTab;
        private Size _lastSizeMeasured;
        private RibbonCaptionButton _MaximizeRestoreButton;
        private RibbonCaptionButton _MinimizeButton;
        private bool _minimized;
        private GlobalHook _mouseHook;
        private RibbonOrbDropDown _orbDropDown;
        private Image _orbImage;
        private bool _orbPressed;
        private bool _orbSelected;
        private bool _orbVisible;
        private Padding _panelMargin;
        private Padding _panelPadding;
        private int _panelSpacing;
        private RibbonQuickAccessToolbar _quickAcessToolbar;
        private bool _quickAcessVisible;
        private RibbonRenderer _renderer;
        private RibbonMouseSensor _sensor;
        private Padding _tabContentMargin;
        private Padding _tabContentPadding;
        private RibbonTabCollection _tabs;
        private Padding _tabsMargin;
        private int _tabSpacing;
        private Padding _tabsPadding;
        private float _tabSum;
        private Padding _tabTextMargin;
        private bool _updatingSuspended;
        public static int CaptionBarHeight = 0x18;
        internal bool ForceOrbMenu;

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        //private void InitializeComponent()
        //{
        //    components = new System.ComponentModel.Container();
        //}

        #endregion
    }
}
