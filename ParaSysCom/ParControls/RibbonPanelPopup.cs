﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParControls
{
    [ToolboxItem(false)]
    public partial class RibbonPanelPopup : RibbonPopup
    {
        private bool _ignoreNext;
        private RibbonPanel _panel;
        private RibbonMouseSensor _sensor;

        internal RibbonPanelPopup(RibbonPanel panel)
        {
            this.DoubleBuffered = true;
            this._sensor = new RibbonMouseSensor(this, panel.Owner, panel.Items);
            this._sensor.PanelLimit = panel;
            this._panel = panel;
            this._panel.PopUp = this;
            panel.Owner.SuspendSensor();
            using (Graphics graphics = base.CreateGraphics())
            {
                panel.overflowBoundsBuffer = panel.Bounds;
                Size size = panel.SwitchToSize(this, graphics, this.GetSizeMode(panel));
                size.Width += 100;
                size.Height += 100;
                base.Size = size;
            }
            foreach (RibbonItem item in panel.Items)
            {
                item.SetCanvas(this);
            }
        }

        public RibbonElementSizeMode GetSizeMode(RibbonPanel pnl)
        {
            if (pnl.FlowsTo == RibbonPanelFlowDirection.Right)
            {
                return RibbonElementSizeMode.Medium;
            }
            return RibbonElementSizeMode.Large;
        }

        public void IgnoreNextClickDeactivation()
        {
            this._ignoreNext = true;
        }

        protected override void OnClosed(EventArgs e)
        {
            foreach (RibbonItem item in this._panel.Items)
            {
                item.SetCanvas(null);
            }
            this.Panel.SetPressed(false);
            this.Panel.SetSelected(false);
            this.Panel.Owner.UpdateRegions();
            this.Panel.Owner.Refresh();
            this.Panel.PopUp = null;
            this.Panel.Owner.ResumeSensor();
            this.Panel.PopupShowed = false;
            this.Panel.Owner.RedrawArea(this.Panel.Bounds);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            if (this._ignoreNext)
            {
                this._ignoreNext = false;
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            this.Panel.Owner.Renderer.OnRenderPanelPopupBackground(new RibbonCanvasEventArgs(this.Panel.Owner, e.Graphics, new Rectangle(Point.Empty, base.ClientSize), this, this.Panel));
            foreach (RibbonItem item in this.Panel.Items)
            {
                item.OnPaint(this, new RibbonElementPaintEventArgs(e.ClipRectangle, e.Graphics, RibbonElementSizeMode.Large));
            }
            this.Panel.Owner.Renderer.OnRenderRibbonPanelText(new RibbonPanelRenderEventArgs(this.Panel.Owner, e.Graphics, e.ClipRectangle, this.Panel, this));
        }

        public RibbonPanel Panel
        {
            get
            {
                return this._panel;
            }
        }

        public RibbonMouseSensor Sensor
        {
            get
            {
                return this._sensor;
            }
        }
    }
}
