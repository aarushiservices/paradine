﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaSysCom.ParaSysCom
{
    partial class MsgBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            base.SuspendLayout();
            this.BackgroundImageLayout = ImageLayout.Stretch;
            base.ClientSize = new Size(0x37b, 0x93);
            base.ControlBox = false;
//            base.FormBorderStyle = FormBorderStyle.FixedSingle;
            base.Name = "MsgBox";
            base.Load += new EventHandler(this.PIMsgBox_Load);
            base.ResumeLayout(false);
        }

        #endregion
    }
}