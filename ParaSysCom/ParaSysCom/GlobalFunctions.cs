﻿namespace ParaSysCom
{
    using System;
    using System.Data;
    using System.Data.OleDb;
    using System.Data.SqlClient;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Net;
    using System.Reflection;
    using System.Windows.Forms;

    public class GlobalFunctions : GlobalVariables
    {
        private const char CharFill = 'Z';
        public static Form Localfrm = new Form();
        private static string StrConvert = @"ABCDEFGHIJKLMNOPQRSTUVWXYabcdefghijklmnopqrstuvwxy0123456789;\";

        public static void AddCompHandler(Panel pnl)
        {
            try
            {
                for (int i = 0; i <= (pnl.Controls.Count - 1); i++)
                {
                    if (pnl.Controls[i].GetType() != typeof(Label))
                    {
                        if (((pnl.Controls[i].GetType() == typeof(GroupBox)) || (pnl.Controls[i].GetType() == typeof(Panel))) || (pnl.Controls[i].GetType() == typeof(TabPage)))
                        {
                            for (int j = 0; j <= (pnl.Controls[i].Controls.Count - 1); j++)
                            {
                                if (pnl.Controls[i].GetType() != typeof(Label))
                                {
                                    HandleComp(pnl.Controls[i].Controls[j]);
                                }
                            }
                        }
                        else
                        {
                            HandleComp(pnl.Controls[i]);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "AddHandler Function");
            }
        }

        public static void ApplyShape(Form frm)
        {
            int num = 0x210;
            int num2 = 20;
            int num3 = Convert.ToInt16((int) ((frm.Height * num2) / num));
            int x = Convert.ToInt16((int) ((num3 * 12) / num2));
            frm.TransparencyKey = Color.FromArgb(0x40, 0, 0);
            Label label = new Label();
            Label label2 = new Label();
            label.Text = "";
            label.BackColor = Color.FromArgb(0x40, 0, 0);
            label.Width = frm.Width;
            label.Height = num3;
            label.Location = new Point(0, 0);
            label2.BackColor = Color.FromArgb(0x40, 0, 0);
            label2.Width = frm.Width;
            label2.Height = num3;
            label2.Location = new Point(0, frm.Height - label2.Height);
            PictureBox box = new PictureBox();
            box.BackColor = SystemColors.ButtonHighlight;
            box.BackgroundImage = GlobalVariables.Theme_img;
            box.BackgroundImageLayout = ImageLayout.Stretch;
            box.BorderStyle = BorderStyle.FixedSingle;
            box.Name = "picToolBar1";
            box.TabIndex = 0x26;
            box.TabStop = false;
            PictureBox box2 = new PictureBox();
            box2.BackColor = SystemColors.ButtonHighlight;
            box2.BackgroundImage = GlobalVariables.Theme_img;
            box2.BackgroundImageLayout = ImageLayout.Stretch;
            box2.BorderStyle = BorderStyle.FixedSingle;
            box2.Name = "PicToolBar2";
            box2.TabIndex = 0x3a;
            box2.TabStop = false;
            label2.SendToBack();
            label.SendToBack();
            box.BringToFront();
            box2.BringToFront();
            box.Location = new Point(x, x);
            frm.Controls.Add(box);
            frm.Controls.Add(box2);
            frm.Controls.Add(label);
            frm.Controls.Add(label2);
            box.Size = new Size(20, 20);
            box2.Size = new Size(20, 20);
            box2.Location = new Point((frm.Width - x) - box2.Width, (frm.Height - x) - box2.Height);
            frm.BackgroundImage = GlobalVariables.ThemeFrmBackGroundImage;
            frm.BackgroundImageLayout = ImageLayout.Stretch;
            frm.StartPosition = FormStartPosition.CenterParent;
        }

        public static void ApplyTheme(Form grpFields)
        {
            try
            {
                if (GlobalVariables.Theme != 0)
                {
                    string str;
                    if (grpFields.Tag == null)
                    {
                        str = "0";
                    }
                    else
                    {
                        str = Convert.ToString(grpFields.Tag);
                    }
                    if (str != "NoTheme")
                    {
                        grpFields.BackColor = GlobalVariables.theme_color;
                    }
                    for (int i = 0; i <= (grpFields.Controls.Count - 1); i++)
                    {
                        if (grpFields.Tag == null)
                        {
                            str = "0";
                        }
                        else
                        {
                            str = Convert.ToString(grpFields.Tag);
                        }
                        if (str != "NoTheme")
                        {
                            if ((grpFields.Controls[i] is GroupBox) || (grpFields.Controls[i] is Panel))
                            {
                                if (str != "NoTheme")
                                {
                                    for (int j = 0; j <= (grpFields.Controls[i].Controls.Count - 1); j++)
                                    {
                                        if (grpFields.Controls[i].Tag == null)
                                        {
                                            str = "0";
                                        }
                                        else
                                        {
                                            str = Convert.ToString(grpFields.Controls[i].Tag);
                                        }
                                        if (str != "NoTheme")
                                        {
                                            ApplyThemeChild(grpFields.Controls[i].Controls[j]);
                                            grpFields.Controls[i].BackColor = GlobalVariables.theme_color;
                                        }
                                    }
                                }
                            }
                            else if (str != "NoTheme")
                            {
                                ApplyThemeChild(grpFields.Controls[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Apply Theme");
            }
        }

        public static void ApplyThemeChild(Control cntrl)
        {
            try
            {
                if (cntrl.GetType() == typeof(TextBox))
                {
                    if (((TextBox) cntrl).BackColor != Color.White)
                    {
                        ((TextBox) cntrl).BackColor = GlobalVariables.Compulsory_Color;
                    }
                }
                else if (cntrl.GetType() != typeof(PictureBox))
                {
                    if (cntrl.GetType() == typeof(ComboBox))
                    {
                        if (((ComboBox) cntrl).BackColor != Color.White)
                        {
                            ((ComboBox) cntrl).BackColor = GlobalVariables.Compulsory_Color;
                        }
                    }
                    else if (((cntrl.GetType() != typeof(ListView)) && (cntrl.GetType() != typeof(DataGrid))) && (cntrl.GetType() != typeof(DataGridView)))
                    {
                        string str;
                        if (cntrl.GetType() == typeof(Button))
                        {
                            if (cntrl.Tag == null)
                            {
                                str = "0";
                            }
                            else
                            {
                                str = Convert.ToString(cntrl.Tag);
                            }
                            if (str == "NoTheme")
                            {
                                return;
                            }
                            ((Button) cntrl).FlatStyle = FlatStyle.Flat;
                            ((Button) cntrl).FlatAppearance.BorderSize = 0;
                            ((Button) cntrl).Cursor = Cursors.Hand;
                            cntrl.BackgroundImage = GlobalVariables.Theme_img;
                            cntrl.BackgroundImageLayout = ImageLayout.Stretch;
                            if (cntrl.Text.ToUpper() == "&ADD")
                            {
                                ((Button) cntrl).ImageIndex = 4;
                                ((Button) cntrl).ImageAlign = ContentAlignment.MiddleLeft;
                            }
                            else if (cntrl.Text.ToUpper() == "&EDIT")
                            {
                                ((Button) cntrl).ImageIndex = 2;
                                ((Button) cntrl).ImageAlign = ContentAlignment.MiddleLeft;
                            }
                            else if (cntrl.Text.ToUpper() == "&DELETE")
                            {
                                ((Button) cntrl).ImageIndex = 5;
                                ((Button) cntrl).ImageAlign = ContentAlignment.MiddleLeft;
                            }
                            else if (cntrl.Text.ToUpper() == "E&XIT")
                            {
                                ((Button) cntrl).ImageIndex = 6;
                                ((Button) cntrl).ImageAlign = ContentAlignment.MiddleLeft;
                            }
                        }
                        else
                        {
                            if (cntrl.Tag == null)
                            {
                                str = "0";
                            }
                            else
                            {
                                str = Convert.ToString(cntrl.Tag);
                            }
                            if (!(str == "NoTheme"))
                            {
                                cntrl.BackColor = GlobalVariables.theme_color;
                            }
                        }
                        for (int i = 0; i <= (cntrl.Controls.Count - 1); i++)
                        {
                            ApplyThemeChild(cntrl.Controls[i]);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Apply Theme");
            }
        }

        private static bool AreEqual(object obj1, object obj2)
        {
            if (object.ReferenceEquals(obj1, null) & object.ReferenceEquals(obj2, DBNull.Value))
            {
                return true;
            }
            if (object.ReferenceEquals(obj1, null) | object.ReferenceEquals(obj2, DBNull.Value))
            {
                return false;
            }
            return (obj1.ToString() == obj2.ToString());
        }

        private static void AssignClientSettings()
        {
            try
            {
                GlobalVariables.DS = new DataSet();
                if (File.Exists(Application.StartupPath + @"\ClientSettings.xml"))
                {
                    GlobalVariables.DS.ReadXml(Application.StartupPath + @"\ClientSettings.xml");
                    foreach (DataRow row in GlobalVariables.DS.Tables["ClientSettings"].Rows)
                    {
                        GlobalVariables.ServerName = Convert.ToString(row["SERVERNAME"]);
                        GlobalVariables.TerminalNo = Convert.ToInt16(row["TERMINALNO"]);
                        GlobalVariables.DBaseName = Convert.ToString(row["DATABASE"]);
                        GlobalVariables.StrBackEnd = Convert.ToString(row["BACKEND"]);
                        GlobalVariables.ConnStr = Convert.ToString(row["connstr"]);
                        GlobalVariables.PRINTERNAME = "";
                    }
                }
                else if (File.Exists(Application.StartupPath + @"\DBPath"))
                {
                    GlobalVariables.ConnStr = Decode(File.ReadAllText(Application.StartupPath + @"\DBPath")).Split(new char[] { '|' })[0];
                    GlobalVariables.StrBackEnd = "SQLEXPRESS";
                }
                else
                {
                    new frmDBConfiguration().ShowDialog();
                    GlobalVariables.ConnStr = Decode(File.ReadAllText(Application.StartupPath + @"\DBPath")).Split(new char[] { '|' })[0];
                    GlobalVariables.StrBackEnd = "SQLEXPRESS";
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Unable to Load Database Configurations, Contact Administrator");
            }
        }

        public void AssignTheme(Form paramFrm)
        {
            try
            {
                if (paramFrm.IsMdiContainer)
                {
                    paramFrm.BackColor = Color.FromName(GlobalVariables.GlbBackColor);
                    foreach (Control control in paramFrm.Controls)
                    {
                        MessageBox.Show(control.GetType().ToString());
                        if (control.GetType().ToString().ToUpper() == "SYSTEM.WINDOWS.FORMS.PANEL")
                        {
                        }
                    }
                }
                else
                {
                    paramFrm.BackColor = Color.FromName(GlobalVariables.GlbFrontColor);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "AssignTheme");
                throw;
            }
        }

        public static int CheckDBNull(int paramInt)
        {
            if (Convert.IsDBNull(paramInt))
            {
                return 0;
            }
            return paramInt;
        }

        public static string CheckDBNull(string paramStr)
        {
            if (Convert.IsDBNull(paramStr))
            {
                return "";
            }
            if (paramStr == null)
            {
                return "";
            }
            return paramStr;
        }

        public bool ClearFields(Control pnlClr)
        {
            try
            {
                pnlClr.Tag = -1;
                foreach (Control control in pnlClr.Controls)
                {
                    if (!Convert.ToString(control.Tag).Contains("NoClear"))
                    {
                        if ((control.GetType() == typeof(TextBox)) || (control.GetType() == typeof(ComboBox)))
                        {
                            control.Text = "";
                            control.Tag = null;
                        }
                        else if (control.GetType() == typeof(CheckBox))
                        {
                            ((CheckBox) control).Checked = false;
                        }
                        else if (control.GetType() == typeof(RadioButton))
                        {
                            ((RadioButton) control).Checked = false;
                        }
                        else if (control.GetType() == typeof(ListView))
                        {
                            ((ListView) control).Clear();
                        }
                        else if (control.GetType() == typeof(TreeView))
                        {
                            ((TreeView) control).Nodes.Clear();
                        }
                        if (control.Controls.Count > 0)
                        {
                            this.ClearFields(control);
                        }
                    }
                }
                return true;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "CLEAR FIEDS");
                return false;
            }
        }

        public static SqlConnection Connection()
        {
            try
            {
                AssignClientSettings();
                SqlConnection connection = new SqlConnection(GlobalVariables.ConnStr);
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Open();
                return connection;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message + "Could not Establish Connection", "connection()");
                return null;
            }
        }

        public static string Decode(string data)
        {
            string str = "";
            int length = data.Length;
            for (int i = 0; i < length; i++)
            {
                char index = (char) StrConvert.IndexOf(data[i]);
                i++;
                char ch2 = (char) StrConvert.IndexOf(data[i]);
                index = (char) ((index << 2) | ((ch2 >> 4) & '\x0003'));
                str = str + index;
                if (++i < length)
                {
                    index = data[i];
                    if ('Z' == index)
                    {
                        return str;
                    }
                    index = (char) StrConvert.IndexOf(index);
                    ch2 = (char) (((ch2 << 4) & '\x00f0') | ((index >> 2) & '\x000f'));
                    str = str + ch2;
                }
                if (++i < length)
                {
                    ch2 = data[i];
                    if ('Z' == ch2)
                    {
                        return str;
                    }
                    ch2 = (char) StrConvert.IndexOf(ch2);
                    index = (char) (((index << 6) & '\x00c0') | ch2);
                    str = str + index;
                }
            }
            return str;
        }

        public static string Encode(string data)
        {
            int length = data.Length;
            string str = "";
            for (int i = 0; i < length; i++)
            {
                int num2 = (data[i] >> 2) & '?';
                str = str + StrConvert[num2];
                num2 = (data[i] << 4) & '?';
                if (++i < length)
                {
                    num2 |= (data[i] >> 4) & '\x000f';
                }
                str = str + StrConvert[num2];
                if (i < length)
                {
                    num2 = (data[i] << 2) & '?';
                    if (++i < length)
                    {
                        num2 |= (data[i] >> 6) & '\x0003';
                    }
                    str = str + StrConvert[num2];
                }
                else
                {
                    i++;
                    str = str + 'Z';
                }
                if (i < length)
                {
                    num2 = data[i] & '?';
                    str = str + StrConvert[num2];
                }
                else
                {
                    str = str + 'Z';
                }
            }
            return str;
        }

        public static string GetAmtInWords(double Amt)
        {
            try
            {
                GlobalVariables.SqlCmd = new SqlCommand("select dbo.wordcurrency(" + Amt + ")", GlobalVariables.SqlConn);
                return Convert.ToString(GlobalVariables.SqlCmd.ExecuteScalar());
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                return "";
            }
        }

        public object GetClone(string paramSql)
        {
            DataTable dataTable = new DataTable();
            GlobalVariables.SDA = new SqlDataAdapter(paramSql, GlobalVariables.SqlConn);
            GlobalVariables.SDA.Fill(dataTable);
            return dataTable.Clone();
        }

        public string GetCollectionString(DataTable paramdtCl, DataColumn paramdtCol)
        {
            string str = "";
            if (paramdtCl.Columns.Contains(paramdtCol.ColumnName))
            {
                foreach (DataRow row in paramdtCl.Rows)
                {
                    if (str.Trim() != "")
                    {
                        str = str + ",";
                    }
                    str = str + Convert.ToString(row[paramdtCol]);
                }
            }
            return str;
        }

        public static double GetColumnTotal(DataTable Dt, int ColPos)
        {
            double num = 0.0;
            foreach (DataRow row in Dt.Rows)
            {
                num += Convert.ToDouble(row[ColPos]);
            }
            return num;
        }

        public static double GetColumnTotal(DataTable Dt, string ColName)
        {
            double num = 0.0;
            if (Dt.Columns.Contains(ColName))
            {
                foreach (DataRow row in Dt.Rows)
                {
                    if (row[ColName] != DBNull.Value)
                    {
                        num += Convert.ToDouble(row[ColName]);
                    }
                }
            }
            return num;
        }

        public static double GetColumnTotal(DataTable Dt, string ColName, string StrCond)
        {
            double num = 0.0;
            foreach (DataRow row in Dt.Select(StrCond))
            {
                num += Convert.ToDouble(row[ColName]);
            }
            return num;
        }

        public static bool GetConnection()
        {
            try
            {
                AssignClientSettings();
                GlobalVariables.SqlConn = new SqlConnection(GlobalVariables.ConnStr);
                if (GlobalVariables.SqlConn.State == ConnectionState.Open)
                {
                    GlobalVariables.SqlConn.Close();
                }
                GlobalVariables.SqlConn.Open();
                return true;
            }
            catch (Exception exception)
            {
                MessageBox.Show("Sorry, The Database cannot be connected. This is because of a particular reason\nlike network problem, Antivirus blocking database request, firewall setting etc,\nPlease check with your administrator of contact support.   A network related or\ninstance specific occured  while  establishing  a  connection to SQL Server. \nThe server  was not  found or was not  accessible.  Verify the instance name is \ncorrect and that SQL Server is configured to allow remote connections." + "\n\n " + exception.Message, "Unable to Connect Database");
                return false;
            }
        }

        public static DataTable GetCrossTabTable(DataTable dt, string Col1, string CrossColStr, string Col1Desc, string ValColStr, string AdditionalColstr)
        {
            DataColumn column2;
            DataTable table = new DataTable();
            DataTable table2 = GetDistinctTable("row", dt, Col1);
            DataColumn column = new DataColumn();
            column.ColumnName = Col1Desc;
            column.DataType = typeof(string);
            table.Columns.Add(column);
            foreach (string str in CrossColStr.Split(new char[] { ',' }))
            {
                column2 = new DataColumn();
                column2.ColumnName = str;
                column2.DataType = typeof(double);
                table.Columns.Add(column2);
            }
            foreach (string str in AdditionalColstr.Split(new char[] { ',' }))
            {
                column2 = new DataColumn();
                column2.ColumnName = str;
                column2.DataType = typeof(double);
                table.Columns.Add(column2);
            }
            foreach (DataRow row in table2.Rows)
            {
                DataRow row2 = table.NewRow();
                row2[Col1Desc] = row[Col1];
                table.Rows.Add(row2);
            }
            foreach (DataRow row3 in table.Rows)
            {
                foreach (DataRow row in dt.Select("group1str = '" + Convert.ToString(row3[Col1Desc]) + "'"))
                {
                    row3[Convert.ToString(row["group2str"])] = row[ValColStr];
                }
            }
            foreach (DataRow row3 in table.Rows)
            {
                foreach (DataRow row in dt.Select("group1str = '" + Convert.ToString(row3[Col1Desc]) + "'"))
                {
                    foreach (string str in AdditionalColstr.Split(new char[] { ',' }))
                    {
                        row3[str] = row[str];
                    }
                }
            }
            return table;
        }

        public static DataTable GetDistinctTable(string table, DataTable source, string field)
        {
            DataTable table2 = new DataTable(table);
            table2.Columns.Add(field, source.Columns[field].DataType);
            DataRow[] rowArray = source.Select("", field);
            object obj2 = null;
            foreach (DataRow row in rowArray)
            {
                if (!AreEqual(obj2, row[field]))
                {
                    obj2 = row[field];
                    table2.Rows.Add(new object[] { obj2 });
                }
            }
            return table2;
        }

        public static DateTime GetEndTime(DateTime paramDt)
        {
            paramDt = paramDt.AddHours((double) (0x17 - paramDt.Hour));
            paramDt = paramDt.AddMinutes((double) (0x3b - paramDt.Minute));
            paramDt = paramDt.AddSeconds((double) (0x3b - paramDt.Second));
            return paramDt;
        }

        public static string GetErrorMessage(string ErrorMessage)
        {
            foreach (DataRow row in GlobalVariables.ErrorDt.Rows)
            {
                if (ErrorMessage.ToUpper().Contains(row[1].ToString().ToUpper()) && ErrorMessage.ToUpper().Contains(row[2].ToString().ToUpper()))
                {
                    return (row[3].ToString() + "/Ref No : " + row[0].ToString());
                }
            }
            return ErrorMessage;
        }

        public DataTable GetExceldata(string filelocation)
        {
            try
            {
                OleDbCommand command = new OleDbCommand();
                OleDbDataAdapter adapter = new OleDbDataAdapter();
                OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filelocation + "; Extended Properties =Excel 8.0;");
                connection.Open();
                object[] restrictions = new object[4];
                restrictions[3] = "TABLE";
                DataTable oleDbSchemaTable = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, restrictions);
                DataTable dataTable = new DataTable();
                command = new OleDbCommand("SELECT MOBILENO,MESSAGE FROM [SHEET1$]", connection);
                adapter.SelectCommand = command;
                adapter.Fill(dataTable);
                return dataTable;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                return null;
            }
        }

        public long GetExecuteQuery(string StrSql)
        {
            GlobalVariables.SqlCmd = new SqlCommand(StrSql, GlobalVariables.SqlConn);
            return (long) GlobalVariables.SqlCmd.ExecuteNonQuery();
        }

        public static byte[] GetImageByte(string paramImgLoc)
        {
            if (paramImgLoc.Trim() != "")
            {
                FileStream stream = new FileStream(paramImgLoc, FileMode.Open);
                byte[] buffer = new byte[stream.Length];
                stream.Read(buffer, 0, (int) stream.Length);
                stream.Close();
                return buffer;
            }
            return null;
        }

        public static byte[] GetImageByte(PictureBox paramPicBox)
        {
            if (paramPicBox.Image != null)
            {
                MemoryStream stream = new MemoryStream();
                paramPicBox.Image.Save(stream, ImageFormat.Bmp);
                return stream.ToArray();
            }
            return null;
        }

        public static int GetIndexOfEmbededImage(string FileName)
        {
            for (int i = 0; i < 100; i++)
            {
                if ((GlobalVariables.EmbededImageCollection[i] != null) && (GlobalVariables.EmbededImageCollection[i].Tag.ToString().ToUpper() == FileName.ToUpper()))
                {
                    return i;
                }
            }
            return 0;
        }

        public double GetMaxValue(DataTable Dt, int ColPos)
        {
            double num = 1.0;
            foreach (DataRow row in Dt.Rows)
            {
                if (Convert.ToDouble(row[ColPos]) > num)
                {
                    num = Convert.ToDouble(row[ColPos]);
                }
            }
            return num;
        }

        public static object GetQueryValue(string StrSql)
        {
            GlobalVariables.SqlCmd = new SqlCommand(StrSql, GlobalVariables.SqlConn);
            GlobalVariables.SqlCmd.CommandTimeout = 0xf4240;
            return GlobalVariables.SqlCmd.ExecuteScalar();
        }

        public static object GetQueryValue(string StrSql, SqlTransaction SqlTrans)
        {
            GlobalVariables.SqlCmd = new SqlCommand(StrSql, GlobalVariables.SqlConn);
            GlobalVariables.SqlCmd.CommandTimeout = 0xf4240;
            GlobalVariables.SqlCmd.Transaction = SqlTrans;
            return GlobalVariables.SqlCmd.ExecuteScalar();
        }

        public int GetReplace(bool paramBool)
        {
            if (paramBool)
            {
                return 1;
            }
            return 0;
        }

        public string GetSMSBalance(string UserName, string Pwd)
        {
            Uri requestUri = new Uri(GlobalVariables.BalanceTemplate.Replace("{USERNAME}", UserName).Replace("{PASSWORD}", Pwd));
            WebRequest request = WebRequest.Create(requestUri);
            request.Method = "GET";
            StreamReader reader = new StreamReader(request.GetResponse().GetResponseStream());
            return reader.ReadToEnd();
        }

        public static DateTime GetStartTime(DateTime paramDt)
        {
            TimeSpan span = new TimeSpan(paramDt.Hour, paramDt.Minute, paramDt.Second);
            paramDt = paramDt.Subtract(span);
            return paramDt;
        }

        public void Glb_keyDown(KeyEventArgs e)
        {
        }

        public static void GLB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        public void GlbKeydownEvent(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        public void glbToolBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                GlobalVariables.PointClicked = new Point(e.X, e.Y);
            }
        }

        public void glbToolBar_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Localfrm.Opacity = 0.5;
                Point point = Localfrm.PointToScreen(new Point(e.X, e.Y));
                point.Offset(-GlobalVariables.PointClicked.X, -GlobalVariables.PointClicked.Y);
                Localfrm.Location = point;
            }
            else
            {
                Localfrm.Opacity = 1.0;
            }
        }

        private static void HandleComp(Control cntrl)
        {
            if (!Convert.ToString(cntrl.Tag).Contains("NoHandler"))
            {
                if (cntrl.GetType() == typeof(DateTimePicker))
                {
                    ((DateTimePicker) cntrl).KeyDown += new KeyEventHandler(GlobalFunctions.GLB_KeyDown);
                    if (!((DateTimePicker) cntrl).Enabled)
                    {
                        ((DateTimePicker) cntrl).TabStop = false;
                    }
                }
                else if (cntrl.GetType() == typeof(TextBox))
                {
                    ((TextBox) cntrl).KeyDown += new KeyEventHandler(GlobalFunctions.GLB_KeyDown);
                    if (!((TextBox) cntrl).Enabled)
                    {
                        ((TextBox) cntrl).TabStop = false;
                    }
                }
                else if (cntrl.GetType() == typeof(NumControl))
                {
                    ((NumControl) cntrl).KeyDown += new KeyEventHandler(GlobalFunctions.GLB_KeyDown);
                    if (!((NumControl) cntrl).Enabled)
                    {
                        ((NumControl) cntrl).TabStop = false;
                    }
                }
                else if (cntrl.GetType() == typeof(CheckBox))
                {
                    ((CheckBox) cntrl).KeyDown += new KeyEventHandler(GlobalFunctions.GLB_KeyDown);
                    if (!((CheckBox) cntrl).Enabled)
                    {
                        ((CheckBox) cntrl).TabStop = false;
                    }
                }
                else if (cntrl.GetType() == typeof(ComboBox))
                {
                    ((ComboBox) cntrl).KeyDown += new KeyEventHandler(GlobalFunctions.GLB_KeyDown);
                    if (!((ComboBox) cntrl).Enabled)
                    {
                        ((ComboBox) cntrl).TabStop = false;
                    }
                }
                else if (cntrl.GetType() == typeof(RadioButton))
                {
                    ((RadioButton) cntrl).KeyDown += new KeyEventHandler(GlobalFunctions.GLB_KeyDown);
                    if (!((RadioButton) cntrl).Enabled)
                    {
                        ((RadioButton) cntrl).TabStop = false;
                    }
                }
                else if (cntrl.GetType() == typeof(NumericUpDown))
                {
                    ((NumericUpDown) cntrl).KeyDown += new KeyEventHandler(GlobalFunctions.GLB_KeyDown);
                    if (!((NumericUpDown) cntrl).Enabled)
                    {
                        ((NumericUpDown) cntrl).TabStop = false;
                    }
                }
                else if (cntrl.GetType() == typeof(MaskedTextBox))
                {
                    ((MaskedTextBox) cntrl).KeyDown += new KeyEventHandler(GlobalFunctions.GLB_KeyDown);
                    if (!((MaskedTextBox) cntrl).Enabled)
                    {
                        ((MaskedTextBox) cntrl).TabStop = false;
                    }
                }
            }
        }

        private static void LoadEmbededImages()
        {
            Assembly executingAssembly = Assembly.GetExecutingAssembly();
            string[] manifestResourceNames = executingAssembly.GetManifestResourceNames();
            int index = 0;
            foreach (string str in manifestResourceNames)
            {
                if (str.Contains("Scheme.Images."))
                {
                    Stream manifestResourceStream = executingAssembly.GetManifestResourceStream(str);
                    GlobalVariables.EmbededImageCollection[index] = Image.FromStream(manifestResourceStream);
                    GlobalVariables.EmbededImageCollection[index].Tag = str.Replace("Scheme.Images.", "");
                    index++;
                }
            }
        }

        public static void LoadImageWithByte(byte[] data, PictureBox paramPicBox)
        {
            if (data != null)
            {
                Stream stream = new MemoryStream(data);
                paramPicBox.Image = Image.FromStream(stream);
            }
        }

        public void LoadPermissions(string paramStr)
        {
        }

        public string SendSMS(string Message, string DestinationNo, string SId)
        {
            Uri requestUri = new Uri(GlobalVariables.SMSTemplate.Replace("{USERNAME}", GlobalVariables.SMSUserName).Replace("{PASSWORD}", GlobalVariables.SmsPassWord).Replace("{SENDER}", SId).Replace("{MESSAGE}", Message).Replace("{MOBILENO}", DestinationNo));
            WebRequest request = WebRequest.Create(requestUri);
            request.Method = "GET";
            StreamReader reader = new StreamReader(request.GetResponse().GetResponseStream());
            return reader.ReadToEnd();
        }

        public void SendSMSHTTP(string Message, string DestinationNo)
        {
        }

        public static void SetToolbar(Form Pnl, string str, bool Movable)
        {
            foreach (Control control in Pnl.Controls)
            {
                if (control.GetType() == typeof(Label))
                {
                    if (control.Name == "lblLeft")
                    {
                        control.Visible = false;
                    }
                    else if (control.Name == "lblTop")
                    {
                        control.Visible = false;
                    }
                    else if (control.Name == "lblRight")
                    {
                        control.Visible = false;
                    }
                    else if (control.Name == "lblBottom")
                    {
                        control.Visible = false;
                    }
                }
            }
            Label label = new Label();
            Label label2 = new Label();
            Label label3 = new Label();
            Label label4 = new Label();
            label.ForeColor = GlobalVariables.Header_ForeColor;
            label.BackColor = GlobalVariables.Header_BackColor;
            label.BorderStyle = BorderStyle.None;
            label.Dock = DockStyle.Top;
            label.FlatStyle = FlatStyle.Flat;
            label.Font = new Font("Nina", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
            label.Location = new Point(0, 0);
            label.Size = new Size(0x15d, 20);
            label.Name = "lblTop";
            label.Text = str;
            label.Tag = "NoTheme";
            label.TextAlign = ContentAlignment.MiddleCenter;
            label2.ForeColor = GlobalVariables.Header_ForeColor;
            label2.BackColor = GlobalVariables.Header_BackColor;
            label2.BorderStyle = BorderStyle.None;
            label2.Dock = DockStyle.Bottom;
            label2.Font = new Font("Nina", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
            label2.Location = new Point(0, 0x1c5);
            label2.Name = "lblBottom";
            label2.Size = new Size(0x15d, 10);
            label2.Tag = "NoTheme";
            label4.ForeColor = GlobalVariables.Header_ForeColor;
            label4.BackColor = GlobalVariables.Header_BackColor;
            label4.BorderStyle = BorderStyle.None;
            label4.Dock = DockStyle.Right;
            label4.Font = new Font("Nina", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
            label4.Location = new Point(0x158, 20);
            label4.Name = "lblRight";
            label4.Size = new Size(5, 0x1b1);
            label4.Tag = "NoTheme";
            label3.ForeColor = GlobalVariables.Header_ForeColor;
            label3.BackColor = GlobalVariables.Header_BackColor;
            label3.BorderStyle = BorderStyle.None;
            label3.Dock = DockStyle.Left;
            label3.Font = new Font("Nina", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
            label3.Location = new Point(0, 20);
            label3.Name = "lblLeft";
            label3.Size = new Size(4, 0x1b1);
            label3.Tag = "NoTheme";
            Pnl.Controls.Add(label4);
            Pnl.Controls.Add(label3);
            Pnl.Controls.Add(label);
            Pnl.Controls.Add(label2);
        }

        public static void SetToolbar(Panel Pnl, string str, bool Movable)
        {
            Label label = new Label();
            Label label2 = new Label();
            Label label3 = new Label();
            Label label4 = new Label();
            label.ForeColor = GlobalVariables.Header_ForeColor;
            label.BackColor = GlobalVariables.Header_BackColor;
            label.BorderStyle = BorderStyle.None;
            label.Dock = DockStyle.Top;
            label.FlatStyle = FlatStyle.Flat;
            label.Font = new Font("Nina", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
            label.Location = new Point(0, 0);
            label.Size = new Size(0x15d, 20);
            label.Name = "lblTop";
            label.Text = str;
            label.Tag = "NoTheme";
            label.TextAlign = ContentAlignment.MiddleCenter;
            label2.ForeColor = GlobalVariables.Header_ForeColor;
            label2.BackColor = GlobalVariables.Header_BackColor;
            label2.BorderStyle = BorderStyle.None;
            label2.Dock = DockStyle.Bottom;
            label2.Font = new Font("Nina", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
            label2.Location = new Point(0, 0x1c5);
            label2.Name = "lblBottom";
            label2.Size = new Size(0x15d, 10);
            label2.Tag = "NoTheme";
            label4.ForeColor = GlobalVariables.Header_ForeColor;
            label4.BackColor = GlobalVariables.Header_BackColor;
            label4.BorderStyle = BorderStyle.None;
            label4.Dock = DockStyle.Right;
            label4.Font = new Font("Nina", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
            label4.Location = new Point(0x158, 20);
            label4.Name = "lblRight";
            label4.Size = new Size(5, 0x1b1);
            label4.Tag = "NoTheme";
            label3.ForeColor = GlobalVariables.Header_ForeColor;
            label3.BackColor = GlobalVariables.Header_BackColor;
            label3.BorderStyle = BorderStyle.None;
            label3.Dock = DockStyle.Left;
            label3.Font = new Font("Nina", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
            label3.Location = new Point(0, 20);
            label3.Name = "lblLeft";
            label3.Size = new Size(4, 0x1b1);
            label3.Tag = "NoTheme";
            Pnl.Controls.Add(label4);
            Pnl.Controls.Add(label3);
            Pnl.Controls.Add(label);
            Pnl.Controls.Add(label2);
        }
    }
}

