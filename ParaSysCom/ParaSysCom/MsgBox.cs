﻿using ParControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaSysCom.ParaSysCom
{
    public partial class MsgBox : Form
    {
        private static Button btnAbort;
        private static Button btnCancel;
        private static Button btnHelp;
        private static Button btnIgnore;
        private static Button btnNo;
        private static Button btnOK;
        private static Button btnRetry;
        private static Button btnYes;
       // private IContainer components;
        private static FlowLayoutPanel flpButtons;
        private static Label LblMessage;
        private static Label LblRef;
        private static Label LblTitle;
        private static Helptopic MainHelpTopic;
        private static PictureBox MainIconPB;
        private static MsgBox ObjMsgBox;
        private static DialogResult PIReturnButton;
        private static ParPanel PnlMain;
        private static Panel RefPanel;
        private static ParPanel TitlePanel;

        private static void btnAbort_Click(object sender, EventArgs e)
        {
            PIReturnButton = DialogResult.Abort;
            ObjMsgBox.Close();
        }

        private static void btnCancel_Click(object sender, EventArgs e)
        {
            PIReturnButton = DialogResult.Cancel;
            ObjMsgBox.Close();
        }

        private static void btnHelp_Click(object sender, EventArgs e)
        {
            Help.ShowHelp(ObjMsgBox, Application.StartupPath + @"\cbhelp.chm", MainHelpTopic.ToString() + ".htm");
        }

        private static void btnIgnore_Click(object sender, EventArgs e)
        {
            PIReturnButton = DialogResult.Ignore;
            ObjMsgBox.Close();
        }

        private static void btnNo_Click(object sender, EventArgs e)
        {
            PIReturnButton = DialogResult.No;
            ObjMsgBox.Close();
        }

        private static void btnOK_Click(object sender, EventArgs e)
        {
            PIReturnButton = DialogResult.OK;
            ObjMsgBox.Close();
        }

        private static void btnRetry_Click(object sender, EventArgs e)
        {
            PIReturnButton = DialogResult.Retry;
            ObjMsgBox.Close();
        }

        private static void btnYes_Click(object sender, EventArgs e)
        {
            PIReturnButton = DialogResult.Yes;
            ObjMsgBox.Close();
        }

        private static void BuildLayout(string title)
        {
        }

        private static void BuildMessageBox(string title)
        {
            ObjMsgBox = new MsgBox();
            PnlMain = new ParPanel();
            ObjMsgBox.FormBorderStyle = FormBorderStyle.None;
            ObjMsgBox.StartPosition = FormStartPosition.CenterScreen;
            PnlMain.BackgroundImageLayout = ImageLayout.Stretch;
            PnlMain.BorderStyle = BorderStyle.None;
            PnlMain.BackgroundImageLayout = ImageLayout.Stretch;
            TableLayoutPanel panel = new TableLayoutPanel();
            panel.RowCount = 3;
            panel.ColumnCount = 0;
            panel.Dock = DockStyle.Top;
            panel.RowStyles.Add(new RowStyle(SizeType.Absolute, 70f));
            panel.RowStyles.Add(new RowStyle(SizeType.Absolute, 43f));
            panel.RowStyles.Add(new RowStyle(SizeType.Absolute, 10f));
            panel.BackColor = Color.Transparent;
            panel.Padding = new Padding(2, 8, 2, 2);
            LblTitle = new Label();
            LblTitle.Dock = DockStyle.Top;
            LblTitle.Size = new Size(100, 30);
            LblTitle.BackColor = Color.Transparent;
            LblTitle.ForeColor = Color.White;
            LblTitle.Font = new Font("Arial", 8f, FontStyle.Bold);
            LblTitle.Text = title;
            LblMessage = new Label();
            LblMessage.Dock = DockStyle.Fill;
            LblMessage.BackColor = Color.Transparent;
            LblMessage.Font = new Font("Arial", 8f, FontStyle.Bold);
            LblMessage.Text = "hiii";
            LblMessage.TextAlign = ContentAlignment.MiddleCenter;
            LblMessage.Padding = new Padding(0, 15, 0, 0);
            MainIconPB = new PictureBox();
            flpButtons = new FlowLayoutPanel();
            flpButtons.FlowDirection = FlowDirection.RightToLeft;
            flpButtons.Padding = new Padding(0, 5, 5, 0);
            flpButtons.Dock = DockStyle.Fill;
            flpButtons.BackColor = Color.Transparent;
            TableLayoutPanel panel2 = new TableLayoutPanel();
            panel2.BackColor = Color.Transparent;
            panel2.Dock = DockStyle.Fill;
            panel2.ColumnCount = 2;
            panel2.RowCount = 0;
            panel2.Padding = new Padding(4, 5, 4, 4);
            panel2.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 50f));
            panel2.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100f));
            panel2.Controls.Add(MainIconPB);
            panel2.Controls.Add(LblMessage);
            MainIconPB.MouseDown += new MouseEventHandler(MsgBox.ImgMouseDown);
            MainIconPB.MouseMove += new MouseEventHandler(MsgBox.ImgMouseMove);
            panel.Controls.Add(panel2);
            panel.Controls.Add(flpButtons);
            LblRef = new Label();
            LblRef.Dock = DockStyle.Fill;
            LblRef.BackColor = Color.Transparent;
            LblRef.ForeColor = Color.Black;
            LblRef.Font = new Font("Calibri", 8f, FontStyle.Bold);
            RefPanel = new Panel();
            RefPanel.Dock = DockStyle.Fill;
            RefPanel.Controls.Add(LblRef);
            TitlePanel = new ParPanel();
            TitlePanel.Dock = DockStyle.Top;
            TitlePanel.Height = 30;
            TitlePanel.Width = 30;
            TitlePanel.HeaderText = LblTitle.Text;
            TitlePanel.HeaderAlignment = StringAlignment.Near;
            TitlePanel.ShadowOffSet = 0;
            TitlePanel.RoundCornerRadius = 0;
            TitlePanel.BorderColor = Color.DarkGray;
            PnlMain.ShadowOffSet = 0;
            PnlMain.RoundCornerRadius = 0;
            PnlMain.BorderColor = Color.DarkGray;
            panel.Controls.Add(RefPanel);
            PnlMain.Controls.Add(TitlePanel);
            PnlMain.Controls.Add(panel);
            PnlMain.Dock = DockStyle.Fill;
            PnlMain.BackColor = Color.MintCream;
            ObjMsgBox.Controls.Add(PnlMain);
            ObjMsgBox.Size = new Size(800, 150);
            panel.Dock = DockStyle.Fill;
        }

        private static void ImgMouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                GlobalVariables.PointClicked = new Point(e.X, e.Y);
            }
            else
            {
                ObjMsgBox.Opacity = 1.0;
            }
        }

        private static void ImgMouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ObjMsgBox.Opacity = 0.75;
                Point point = ObjMsgBox.PointToScreen(new Point(e.X, e.Y));
                point.Offset(-GlobalVariables.PointClicked.X, -GlobalVariables.PointClicked.Y);
                ObjMsgBox.Location = point;
            }
            else
            {
                ObjMsgBox.Opacity = 1.0;
            }
        }

       

        private static void LoadButtons(PIButtons MButtons)
        {
            switch (MButtons)
            {
                case PIButtons.AbortRetryIgnore:
                    ShowIgnoreButton();
                    ShowRetryButton();
                    ShowAbortButton();
                    break;

                case PIButtons.OK:
                    ShowOKButton();
                    break;

                case PIButtons.OKCancel:
                    ShowCancelButton();
                    ShowOKButton();
                    break;

                case PIButtons.RetryCancel:
                    ShowCancelButton();
                    ShowRetryButton();
                    break;

                case PIButtons.YesNo:
                    ShowYesButton();
                    ShowNoButton();
                    break;

                case PIButtons.NoYes:
                    ShowNoButton();
                    ShowYesButton();
                    break;

                case PIButtons.YesNoCancel:
                    ShowCancelButton();
                    ShowNoButton();
                    ShowYesButton();
                    break;

                case PIButtons.AbortRetryIgnoreHelp:
                    ShowHelpButton();
                    LoadButtons(PIButtons.AbortRetryIgnore);
                    break;

                case PIButtons.OKHelp:
                    ShowHelpButton();
                    ShowOKButton();
                    break;

                case PIButtons.OKCancelHelp:
                    ShowHelpButton();
                    LoadButtons(PIButtons.OKCancel);
                    break;

                case PIButtons.RetryCancelHelp:
                    ShowHelpButton();
                    LoadButtons(PIButtons.RetryCancel);
                    break;

                case PIButtons.YesNoHelp:
                    LoadButtons(PIButtons.YesNo);
                    ShowHelpButton();
                    break;

                case PIButtons.NoYesNoHelp:
                    LoadButtons(PIButtons.NoYes);
                    ShowHelpButton();
                    break;

                case PIButtons.YesNoCancelHelp:
                    ShowHelpButton();
                    LoadButtons(PIButtons.YesNoCancel);
                    break;
            }
        }

        private static Image LoadIconImage(PIIcon MIcon)
        {
            switch (MIcon)
            {
                case PIIcon.Error:
                    MessageBeep(30);
                    return GlobalVariables.EmbededImageCollection[GlobalFunctions.GetIndexOfEmbededImage("Error.bmp")];

                case PIIcon.Find:
                    return GlobalVariables.EmbededImageCollection[GlobalFunctions.GetIndexOfEmbededImage("Find.bmp")];

                case PIIcon.Information:
                    return GlobalVariables.EmbededImageCollection[GlobalFunctions.GetIndexOfEmbededImage("Information.bmp")];

                case PIIcon.Print:
                    return GlobalVariables.EmbededImageCollection[GlobalFunctions.GetIndexOfEmbededImage("Print.bmp")];

                case PIIcon.Question:
                    return GlobalVariables.EmbededImageCollection[GlobalFunctions.GetIndexOfEmbededImage("Question.bmp")];

                case PIIcon.Stop:
                    return GlobalVariables.EmbededImageCollection[GlobalFunctions.GetIndexOfEmbededImage("Stop.bmp")];

                case PIIcon.User:
                    return GlobalVariables.EmbededImageCollection[GlobalFunctions.GetIndexOfEmbededImage("User.bmp")];

                case PIIcon.Warning:
                    return GlobalVariables.EmbededImageCollection[GlobalFunctions.GetIndexOfEmbededImage("Warning.bmp")];

                case PIIcon.Saved:
                    return GlobalVariables.EmbededImageCollection[GlobalFunctions.GetIndexOfEmbededImage("Saved.bmp")];
            }
            return null;
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern bool MessageBeep(uint type);
        private static void ObjPIMsgBox_Paint(object sender, PaintEventArgs e)
        {
        }

        private void PIMsgBox_Load(object sender, EventArgs e)
        {
        }

        public static DialogResult Show(string Message)
        {
            BuildMessageBox("");
            string[] strArray = Message.Split(new char[] { '/' });
            LblMessage.Text = strArray[0];
            if (strArray.GetUpperBound(0) > 0)
            {
                LblRef.Text = strArray[1];
            }
            ShowOKButton();
            BuildLayout("");
            ObjMsgBox.ShowDialog();
            return PIReturnButton;
        }

        public static DialogResult Show(string Message, string Title)
        {
            BuildMessageBox(Title);
            string[] strArray = Message.Split(new char[] { '/' });
            LblMessage.Text = strArray[0];
            if (strArray.GetUpperBound(0) > 0)
            {
                LblRef.Text = strArray[1];
            }
            ShowOKButton();
            BuildLayout(Title);
            ObjMsgBox.ShowDialog();
            return PIReturnButton;
        }

        public static DialogResult Show(string Message, string Title, PIButtons MButtons)
        {
            BuildMessageBox(Title);
            string[] strArray = Message.Split(new char[] { '/' });
            LblMessage.Text = strArray[0];
            if (strArray.GetUpperBound(0) > 0)
            {
                LblRef.Text = strArray[1];
            }
            LoadButtons(MButtons);
            BuildLayout(Title);
            ObjMsgBox.ShowDialog();
            return PIReturnButton;
        }

        public static DialogResult Show(string Message, string Title, PIButtons MButtons, PIIcon MIcon)
        {
            BuildMessageBox(Title);
            string[] strArray = Message.Split(new char[] { '/' });
            LblMessage.Text = strArray[0];
            if (strArray.GetUpperBound(0) > 0)
            {
                LblRef.Text = strArray[1];
            }
            LoadButtons(MButtons);
            MainIconPB.Image = LoadIconImage(MIcon);
            BuildLayout(Title);
            ObjMsgBox.ShowDialog();
            return PIReturnButton;
        }

        public static DialogResult Show(string Message, string Title, PIButtons MButtons, PIIcon MIcon, Helptopic MHelp)
        {
            BuildMessageBox(Title);
            string[] strArray = Message.Split(new char[] { '/' });
            LblMessage.Text = strArray[0];
            if (strArray.GetUpperBound(0) > 0)
            {
                LblRef.Text = strArray[1];
            }
            LoadButtons(MButtons);
            MainIconPB.Image = LoadIconImage(MIcon);
            BuildLayout(Title);
            MainHelpTopic = MHelp;
            ObjMsgBox.ShowDialog();
            return PIReturnButton;
        }

        private static void ShowAbortButton()
        {
            btnAbort = new Button();
            btnAbort.Text = "&Abort";
            btnAbort.Size = new Size(80, 0x19);
            btnAbort.BackColor = Color.FromArgb(0xff, 0xff, 0xff);
            btnAbort.Font = new Font("Arial", 8f, FontStyle.Bold);
            btnAbort.Click += new EventHandler(MsgBox.btnAbort_Click);
            flpButtons.Controls.Add(btnAbort);
        }

        private static void ShowCancelButton()
        {
            btnCancel = new Button();
            btnCancel.Text = "&Cancel";
            btnCancel.Size = new Size(80, 0x19);
            btnCancel.BackColor = Color.FromArgb(0xff, 0xff, 0xff);
            btnCancel.Font = new Font("Arial", 8f, FontStyle.Bold);
            btnCancel.Click += new EventHandler(MsgBox.btnCancel_Click);
            flpButtons.Controls.Add(btnCancel);
        }

        private static void ShowHelpButton()
        {
            btnHelp = new Button();
            btnHelp.Text = "&Help";
            btnHelp.Size = new Size(80, 0x19);
            btnHelp.BackColor = Color.FromArgb(0xff, 0xff, 0xff);
            btnHelp.Font = new Font("Arial", 8f, FontStyle.Bold);
            btnHelp.Click += new EventHandler(MsgBox.btnHelp_Click);
            flpButtons.Controls.Add(btnHelp);
        }

        private static void ShowIgnoreButton()
        {
            btnIgnore = new Button();
            btnIgnore.Text = "&Ignore";
            btnIgnore.Size = new Size(80, 0x19);
            btnIgnore.BackColor = Color.FromArgb(0xff, 0xff, 0xff);
            btnIgnore.Font = new Font("Arial", 8f, FontStyle.Bold);
            btnIgnore.Click += new EventHandler(MsgBox.btnIgnore_Click);
            flpButtons.Controls.Add(btnIgnore);
        }

        private static void ShowNoButton()
        {
            btnNo = new Button();
            btnNo.Text = "&No";
            btnNo.Size = new Size(80, 0x19);
            btnNo.BackColor = Color.FromArgb(0xff, 0xff, 0xff);
            btnNo.Font = new Font("Arial", 8f, FontStyle.Bold);
            btnNo.Click += new EventHandler(MsgBox.btnNo_Click);
            flpButtons.Controls.Add(btnNo);
        }

        private static void ShowOKButton()
        {
            btnOK = new Button();
            btnOK.Text = "&OK";
            btnOK.Size = new Size(80, 0x19);
            btnOK.BackColor = Color.FromArgb(0xff, 0xff, 0xff);
            btnOK.Font = new Font("Arial", 8f, FontStyle.Bold);
            btnOK.Click += new EventHandler(MsgBox.btnOK_Click);
            flpButtons.Controls.Add(btnOK);
        }

        private static void ShowRetryButton()
        {
            btnRetry = new Button();
            btnRetry.Text = "&Retry";
            btnRetry.Size = new Size(80, 0x19);
            btnRetry.BackColor = Color.FromArgb(0xff, 0xff, 0xff);
            btnRetry.Font = new Font("Arial", 8f, FontStyle.Bold);
            btnRetry.Click += new EventHandler(MsgBox.btnRetry_Click);
            flpButtons.Controls.Add(btnRetry);
        }

        private static void ShowYesButton()
        {
            btnYes = new Button();
            btnYes.Text = "&Yes";
            btnYes.Size = new Size(80, 0x19);
            btnYes.BackColor = Color.FromArgb(0xff, 0xff, 0xff);
            btnYes.Font = new Font("Arial", 8f, FontStyle.Bold);
            btnYes.Click += new EventHandler(MsgBox.btnYes_Click);
            flpButtons.Controls.Add(btnYes);
        }

        public enum Helptopic
        {
            Institution,
            Branch,
            Class,
            Division,
            DivisionAllotment,
            DivisionSetting,
            Registration,
            Admision,
            Student,
            Teacher,
            Driver,
            Attendence,
            Receipt,
            ClassFeeSchedule,
            StudentFeeSchedule,
            Structure
        }

        public enum PIButtons
        {
            AbortRetryIgnore,
            OK,
            OKCancel,
            RetryCancel,
            YesNo,
            NoYes,
            YesNoCancel,
            AbortRetryIgnoreHelp,
            OKHelp,
            OKCancelHelp,
            RetryCancelHelp,
            YesNoHelp,
            NoYesNoHelp,
            YesNoCancelHelp
        }

        public enum PIIcon
        {
            Error,
            Find,
            Information,
            Print,
            Question,
            Stop,
            User,
            Warning,
            Saved
        }
    }
}
