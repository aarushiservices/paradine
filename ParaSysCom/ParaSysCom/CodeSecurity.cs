﻿namespace ParaSysCom
{
    using Microsoft.Win32;
    using System;
    using System.Management;
    using System.Security.Cryptography;
    using System.Text;
    using System.Windows.Forms;

    public class CodeSecurity
    {
        public static string ActivationCode = string.Empty;
        private const char CharFill = 'Z';
        public TripleDESCryptoServiceProvider DES;
        private static string fingerPrint = string.Empty;
        public static string InstallationSerialNo = string.Empty;
        public MD5CryptoServiceProvider MD5;
        private static string StrConvert = "ABCDEFGHIJKLMNOPQRSTUVWXYabcdefghijklmnopqrstuvwxy0123456789";

        private static string baseId()
        {
            return (identifier("Win32_BaseBoard", "Model") + identifier("Win32_BaseBoard", "Manufacturer") + identifier("Win32_BaseBoard", "Name") + identifier("Win32_BaseBoard", "SerialNumber"));
        }

        private static string biosId()
        {
            return (identifier("Win32_BIOS", "Manufacturer") + identifier("Win32_BIOS", "SMBIOSBIOSVersion") + identifier("Win32_BIOS", "IdentificationCode") + identifier("Win32_BIOS", "SerialNumber") + identifier("Win32_BIOS", "ReleaseDate") + identifier("Win32_BIOS", "Version"));
        }

        private static string cpuId()
        {
            string str = identifier("Win32_Processor", "UniqueId");
            if (!(str == ""))
            {
                return str;
            }
            str = identifier("Win32_Processor", "ProcessorId");
            if (!(str == ""))
            {
                return str;
            }
            str = identifier("Win32_Processor", "Name");
            if (str == "")
            {
                str = identifier("Win32_Processor", "Manufacturer");
            }
            return (str + identifier("Win32_Processor", "MaxClockSpeed"));
        }

        public static string Decode(string data)
        {
            string str = "";
            int length = data.Length;
            for (int i = 0; i < length; i++)
            {
                char index = (char) StrConvert.IndexOf(data[i]);
                i++;
                char ch2 = (char) StrConvert.IndexOf(data[i]);
                index = (char) ((index << 2) | ((ch2 >> 4) & '\x0003'));
                str = str + index;
                if (++i < length)
                {
                    index = data[i];
                    if ('Z' == index)
                    {
                        return str;
                    }
                    index = (char) StrConvert.IndexOf(index);
                    ch2 = (char) (((ch2 << 4) & '\x00f0') | ((index >> 2) & '\x000f'));
                    str = str + ch2;
                }
                if (++i < length)
                {
                    ch2 = data[i];
                    if ('Z' == ch2)
                    {
                        return str;
                    }
                    ch2 = (char) StrConvert.IndexOf(ch2);
                    index = (char) (((index << 6) & '\x00c0') | ch2);
                    str = str + index;
                }
            }
            return str;
        }

        public string Decrypt(string encryptedString, string key)
        {
            try
            {
                this.DES.Key = this.MD5Hash(key);
                this.DES.Mode = CipherMode.ECB;
                byte[] inputBuffer = Convert.FromBase64String(encryptedString);
                return Encoding.ASCII.GetString(this.DES.CreateDecryptor().TransformFinalBlock(inputBuffer, 0, inputBuffer.Length));
            }
            catch
            {
                MessageBox.Show("Invalid Password", "Decryption Failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return "";
            }
        }

        private static string diskId()
        {
            return (identifier("Win32_DiskDrive", "Model") + identifier("Win32_DiskDrive", "Manufacturer") + identifier("Win32_DiskDrive", "Signature") + identifier("Win32_DiskDrive", "TotalHeads"));
        }

        public static string Encode(string data)
        {
            int length = data.Length;
            string str = "";
            for (int i = 0; i < length; i++)
            {
                int num2 = (data[i] >> 2) & '?';
                str = str + StrConvert[num2];
                num2 = (data[i] << 4) & '?';
                if (++i < length)
                {
                    num2 |= (data[i] >> 4) & '\x000f';
                }
                str = str + StrConvert[num2];
                if (i < length)
                {
                    num2 = (data[i] << 2) & '?';
                    if (++i < length)
                    {
                        num2 |= (data[i] >> 6) & '\x0003';
                    }
                    str = str + StrConvert[num2];
                }
                else
                {
                    i++;
                    str = str + 'Z';
                }
                if (i < length)
                {
                    num2 = data[i] & '?';
                    str = str + StrConvert[num2];
                }
                else
                {
                    str = str + 'Z';
                }
            }
            return str;
        }

        public string Encrypt(string stringToEncrypt, string key)
        {
            try
            {
                this.DES.Key = this.MD5Hash(key);
                this.DES.Mode = CipherMode.ECB;
                byte[] bytes = Encoding.ASCII.GetBytes(stringToEncrypt);
                return Convert.ToBase64String(this.DES.CreateEncryptor().TransformFinalBlock(bytes, 0, bytes.Length));
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Encryption Failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return "";
            }
        }

        public static string GetActivationCode(string code)
        {
            return GetHashDecode(code);
        }

        private static string GetAsciiKey(string paramKey)
        {
            string str2;
            try
            {
                long num = 0L;
                foreach (char ch in paramKey)
                {
                    num += (long) ch;
                }
                while (num.ToString().Length > 1)
                {
                    string str = num.ToString();
                    num = 0L;
                    for (int i = 0; i < str.Length; i++)
                    {
                        num += Convert.ToInt16(str.Substring(i, 1));
                    }
                }
                str2 = num.ToString();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return str2;
        }

        public static string GetFingerPrint()
        {
            if (string.IsNullOrEmpty(fingerPrint))
            {
                fingerPrint = "DISK >> " + diskId();
            }
            return fingerPrint;
        }

        private static string GetHash(string s)
        {
            System.Security.Cryptography.MD5 md = new MD5CryptoServiceProvider();
            byte[] bytes = new ASCIIEncoding().GetBytes(s);
            return GetHexString(md.ComputeHash(bytes));
        }

        private static string GetHashDecode(string s)
        {
            s = s + s;
            s = s + s;
            System.Security.Cryptography.MD5 md = new MD5CryptoServiceProvider();
            byte[] bytes = new ASCIIEncoding().GetBytes(s);
            return GetHexString(md.ComputeHash(bytes));
        }

        private static string GetHexString(byte[] bt)
        {
            string str = string.Empty;
            for (int i = 0; i < bt.Length; i++)
            {
                byte num2 = bt[i];
                int num3 = num2;
                int num4 = num3 & 15;
                int num5 = (num3 >> 4) & 15;
                if (num5 > 9)
                {
                    char ch = (char) ((num5 - 10) + 0x41);
                    str = str + ch.ToString();
                }
                else
                {
                    str = str + num5.ToString();
                }
                if (num4 > 9)
                {
                    str = str + ((char) ((num4 - 10) + 0x41)).ToString();
                }
                else
                {
                    str = str + num4.ToString();
                }
                if (((i + 1) != bt.Length) && (((i + 1) % 2) == 0))
                {
                    str = str + "-";
                }
            }
            return str;
        }

        public static string GetSystemInstallationNo()
        {
            return GetHash(GetFingerPrint());
        }

        private static string identifier(string wmiClass, string wmiProperty)
        {
            string str = "";
            ManagementObjectCollection instances = new ManagementClass(wmiClass).GetInstances();
            foreach (ManagementObject obj2 in instances)
            {
                if (str == "")
                {
                    try
                    {
                        return obj2[wmiProperty].ToString();
                    }
                    catch
                    {
                    }
                }
            }
            return str;
        }

        private static string identifier(string wmiClass, string wmiProperty, string wmiMustBeTrue)
        {
            string str = "";
            ManagementObjectCollection instances = new ManagementClass(wmiClass).GetInstances();
            foreach (ManagementObject obj2 in instances)
            {
                if ((obj2[wmiMustBeTrue].ToString() == "True") && (str == ""))
                {
                    try
                    {
                        return obj2[wmiProperty].ToString();
                    }
                    catch
                    {
                    }
                }
            }
            return str;
        }

        public static void LoadFields()
        {
            InstallationSerialNo = GetSystemInstallationNo();
            ActivationCode = GetActivationCode(InstallationSerialNo + "Srinivas 1978 Born 2002 Married");
            string data = ActivationCode.Replace("-", "");
            data = DateTime.Now.Day.ToString("00") + DateTime.Now.Month.ToString("00") + data;
            Clipboard.SetData(DataFormats.Text, data);
        }

        private static string macId()
        {
            return identifier("Win32_NetworkAdapterConfiguration", "MACAddress", "IPEnabled");
        }

        private byte[] MD5Hash(string value)
        {
            return this.MD5.ComputeHash(Encoding.ASCII.GetBytes(value));
        }

        public static string r()
        {
            string text = Convert.ToString(RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, "srinivas").GetValue(@"Software\Microsoft\.NETFramework"));
            MessageBox.Show(text);
            return text;
        }

        public static bool VerifyLicense()
        {
            bool flag = false;
            try
            {
                LoadFields();
                RegistryKey key = Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\.NETFramework", true);
                if (key == null)
                {
                    return flag;
                }
                string str = Convert.ToString(key.GetValue("InstallMap"));
                if (str.Length > 0x26)
                {
                    if (str.ToString().Substring(0, 0x27) == ActivationCode)
                    {
                        flag = true;
                    }
                    else
                    {
                        flag = false;
                    }
                    return flag;
                }
                flag = false;
            }
            catch
            {
                return flag;
            }
            return flag;
        }

        private static string videoId()
        {
            return (identifier("Win32_VideoController", "DriverVersion") + identifier("Win32_VideoController", "Name"));
        }
    }
}

