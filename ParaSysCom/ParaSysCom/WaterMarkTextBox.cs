﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaSysCom
{
    public partial class WaterMarkTextBox : TextBox
    {
        private Color _waterMarkColor = Color.Gray;
        private string _waterMarkText = "Water Mark";
        private Font oldFont = null;
        private bool waterMarkTextEnabled = false;

        public WaterMarkTextBox()
        {
            this.JoinEvents(true);
        }

        private void DisbaleWaterMark()
        {
            this.waterMarkTextEnabled = false;
            base.SetStyle(ControlStyles.UserPaint, false);
            if (this.oldFont != null)
            {
                this.Font = new Font(this.oldFont.FontFamily, this.oldFont.Size, this.oldFont.Style, this.oldFont.Unit);
            }
        }

        private void EnableWaterMark()
        {
            this.oldFont = new Font(this.Font.FontFamily, this.Font.Size, this.Font.Style, this.Font.Unit);
            base.SetStyle(ControlStyles.UserPaint, true);
            this.waterMarkTextEnabled = true;
            this.Refresh();
        }

        private void JoinEvents(bool join)
        {
            if (join)
            {
                base.TextChanged += new EventHandler(this.WaterMark_Toggel);
                base.LostFocus += new EventHandler(this.WaterMark_Toggel);
                base.FontChanged += new EventHandler(this.WaterMark_FontChanged);
            }
        }

        protected override void OnCreateControl()
        {
            base.OnCreateControl();
            this.WaterMark_Toggel(null, null);
        }

        protected override void OnPaint(PaintEventArgs args)
        {
            Font font = new Font(this.Font.FontFamily, this.Font.Size, this.Font.Style, this.Font.Unit);
            SolidBrush brush = new SolidBrush(this.WaterMarkColor);
            args.Graphics.DrawString(this.waterMarkTextEnabled ? this.WaterMarkText : this.Text, font, brush, new PointF(0f, 0f));
            base.OnPaint(args);
        }

        private void WaterMark_FontChanged(object sender, EventArgs args)
        {
            if (this.waterMarkTextEnabled)
            {
                this.oldFont = new Font(this.Font.FontFamily, this.Font.Size, this.Font.Style, this.Font.Unit);
                this.Refresh();
            }
        }

        private void WaterMark_Toggel(object sender, EventArgs args)
        {
            if (this.Text.Length <= 0)
            {
                this.EnableWaterMark();
            }
            else
            {
                this.DisbaleWaterMark();
            }
        }

        public Color WaterMarkColor
        {
            get
            {
                return this._waterMarkColor;
            }
            set
            {
                this._waterMarkColor = value;
                base.Invalidate();
            }
        }

        public string WaterMarkText
        {
            get
            {
                return this._waterMarkText;
            }
            set
            {
                this._waterMarkText = value;
                base.Invalidate();
            }
        }
    }
}
