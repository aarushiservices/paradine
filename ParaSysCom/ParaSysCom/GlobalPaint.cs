﻿namespace ParaSysCom
{
    using System;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Windows.Forms;

    public class GlobalPaint : GlobalVariables
    {
        public static void PaintDataGridViewColumnHeader(DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                Brush brush = new LinearGradientBrush(e.CellBounds, Color.White, GlobalVariables.MainThemeColor, 90f);
                e.Graphics.FillRectangle(brush, e.CellBounds);
                e.Graphics.DrawRectangle(new Pen(Color.Gray), new Rectangle(e.CellBounds.X - 1, e.CellBounds.Y, e.CellBounds.Width, e.CellBounds.Height - 1));
                brush.Dispose();
                e.PaintContent(e.CellBounds);
                e.Handled = true;
            }
        }
    }
}

