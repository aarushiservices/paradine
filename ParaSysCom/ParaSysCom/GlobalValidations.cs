﻿namespace ParaSysCom
{
    using System;
    using System.Windows.Forms;

    public class GlobalValidations : GlobalVariables
    {
        public static bool ValidateFields(Control paramPnl, ToolTip TTip)
        {
            try
            {
                foreach (Control control in paramPnl.Controls)
                {
                    if (control.BackColor == GlobalVariables.Compulsory_Color)
                    {
                        if (control.GetType() == typeof(TextBox))
                        {
                            if (control.Text.Trim() == "")
                            {
                                MessageBox.Show(TTip.GetToolTip(control), control.Name);
                                control.Focus();
                                return false;
                            }
                        }
                        else if (control.GetType() == typeof(NumControl))
                        {
                            if (((NumControl) control).Value <= 0M)
                            {
                                MessageBox.Show(TTip.GetToolTip(control), control.Name);
                                control.Focus();
                                return false;
                            }
                        }
                        else if (control.GetType() == typeof(ComboBox))
                        {
                            ComboBox box = (ComboBox) control;
                            if ((box.DataSource != null) && (Convert.ToInt64(box.SelectedValue) == 0L))
                            {
                                MessageBox.Show(TTip.GetToolTip(control), control.Name);
                                control.Focus();
                                return false;
                            }
                            if (box.Text.Trim() == "")
                            {
                                MessageBox.Show(TTip.GetToolTip(control), control.Name);
                                control.Focus();
                                return false;
                            }
                        }
                    }
                    if ((control.Controls.Count > 0) && !ValidateFields(control, TTip))
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                return false;
            }
        }

        public static void ValidateKeys(KeyPressEventArgs paramEvent, bool AllowAlphabets, bool AllowNumerics, bool AllowSpecialChars, string OptionalString)
        {
            if ((OptionalString.Trim().Length > 0) && OptionalString.Contains(paramEvent.KeyChar.ToString()))
            {
                paramEvent.Handled = false;
            }
            else if (!(AllowAlphabets || (((paramEvent.KeyChar < 'A') || (paramEvent.KeyChar > 'Z')) && ((paramEvent.KeyChar < 'a') || (paramEvent.KeyChar > 'z')))))
            {
                paramEvent.Handled = true;
            }
            else if (!(AllowNumerics || ((paramEvent.KeyChar < '0') || (paramEvent.KeyChar > '9'))))
            {
                paramEvent.Handled = true;
            }
            else if (!(AllowSpecialChars || (((((paramEvent.KeyChar < '!') || (paramEvent.KeyChar > '/')) && ((paramEvent.KeyChar < ':') || (paramEvent.KeyChar > '@'))) && ((paramEvent.KeyChar < '[') || (paramEvent.KeyChar > '`'))) && ((paramEvent.KeyChar < '{') || (paramEvent.KeyChar > '~')))))
            {
                paramEvent.Handled = true;
            }
            else
            {
                paramEvent.Handled = false;
            }
        }
    }
}

