﻿namespace ParaSysCom
{
    using System;
    using System.Data;
    using System.Data.SqlClient; 
    using System.Windows.Forms;
     
    public class GlobalFill : GlobalVariables
    {
        public static void FillCombo(DataTable DtCombo, ComboBox paramCombo)
        {
            try
            {
                if (DtCombo.Rows.Count > 0)
                {
                    int num = 0;
                    paramCombo.DataSource = DtCombo;
                    paramCombo.ValueMember = DtCombo.Columns[0].Caption;
                    paramCombo.DisplayMember = DtCombo.Columns[1].Caption;
                    if (num > 0)
                    {
                        paramCombo.SelectedValue = num;
                    }
                    else
                    {
                        paramCombo.SelectedValue = 0;
                    }
                    if (!object.Equals(GlobalVariables.SDA, null))
                    {
                        GlobalVariables.SDA.Dispose();
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "FillCombo");
            }
        }

        public static void FillCombo(string paramStrSql, ComboBox paramCombo)
        {
            try
            {
                GlobalVariables.SDA = new SqlDataAdapter(paramStrSql, GlobalVariables.SqlConn);
                DataSet dataSet = new DataSet();
                GlobalVariables.SDA.Fill(dataSet, paramCombo.Name);
                if (dataSet.Tables[paramCombo.Name].Rows.Count > 0)
                {
                    long num = 0L;
                    num = Convert.ToInt64(paramCombo.SelectedValue);
                    paramCombo.DataSource = dataSet.Tables[paramCombo.Name];
                    paramCombo.ValueMember = dataSet.Tables[paramCombo.Name].Columns[0].Caption;
                    paramCombo.DisplayMember = dataSet.Tables[paramCombo.Name].Columns[1].Caption;
                    if (num > 0L)
                    {
                        paramCombo.SelectedValue = num;
                    }
                    else
                    {
                        paramCombo.SelectedValue = 0;
                    }
                    if (!object.Equals(GlobalVariables.SDA, null))
                    {
                        GlobalVariables.SDA.Dispose();
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "FillCombo");
            }
        }

        public void FillDataSet(string tblName, DataSet DS, SqlDataAdapter SDA)
        {
            if (!object.ReferenceEquals(DS.Tables[tblName], null))
            {
                DS.Tables[tblName].Clear();
            }
            if (object.ReferenceEquals(SDA.SelectCommand.Connection, null))
            {
                throw new Exception("Connection not Assigned to this Adapter");
            }
            SDA.SelectCommand.CommandTimeout = 0xf4240;
            SDA.Fill(DS, tblName);
        }

        public static void FillDataSet(string paramSql, string tblName, DataSet DS, SqlDataAdapter SDA)
        {
            if (!object.ReferenceEquals(DS.Tables[tblName], null))
            {
                DS.Tables[tblName].Clear();
                DS.Tables[tblName].Dispose();
            }
            SDA = new SqlDataAdapter(paramSql, GlobalVariables.SqlConn);
            SDA.SelectCommand.CommandTimeout = 0xf4240;
            SDA.Fill(DS, tblName);
        }

        public static DataTable FillDataTable(string paramSql)
        {
            GlobalVariables.SDA = new SqlDataAdapter(paramSql, GlobalVariables.SqlConn);
            GlobalVariables.SDA.SelectCommand.CommandTimeout = 0xf4240;
            DataTable dataTable = new DataTable();
            GlobalVariables.SDA.Fill(dataTable);
            return dataTable;
        }

        public static DataTable FillDataTable(string paramSql, SqlConnection sqlConn)
        {
            GlobalVariables.SDA = new SqlDataAdapter(paramSql, sqlConn);
            GlobalVariables.SDA.SelectCommand.CommandTimeout = 0xf4240;
            DataTable dataTable = new DataTable();
            GlobalVariables.SDA.Fill(dataTable);
            return dataTable;
        }

        public static void FillGridView(DataGridView dgView, DataTable DT)
        {
            int index = 0;
            dgView.DataSource = DT;
            if (dgView.SelectedRows.Count > 0)
            {
                index = dgView.SelectedRows[0].Index;
            }
            foreach (DataGridViewColumn column in dgView.Columns)
            {
                if ((column.ValueType == typeof(double)) || (column.ValueType == typeof(decimal)))
                {
                    column.DefaultCellStyle.Format = "N2";
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
                else if (column.ValueType == typeof(DateTime))
                {
                    column.DefaultCellStyle.Format = "dd/MMM/yy hh:m";
                }
            }
            dgView.Refresh();
            if ((dgView.Rows.Count > 0) && (dgView.SelectionMode == DataGridViewSelectionMode.FullRowSelect))
            {
                dgView.Rows[index].Selected = true;
                dgView.FirstDisplayedScrollingRowIndex = dgView.SelectedRows[0].Index;
            }
        }

        public static void FillListView(ListView lstview, DataTable DT)
        {
            ListViewItem item = new ListViewItem();
            lstview.Items.Clear();
            lstview.Columns.Clear();
            lstview.View = View.Details;
            lstview.AllowColumnReorder = true;
            lstview.FullRowSelect = true;
            lstview.GridLines = true;
            foreach (DataColumn column in DT.Columns)
            {
                if (column.Ordinal > 0)
                {
                    lstview.Columns.Add(column.ColumnName);
                    lstview.Columns[column.Ordinal - 1].Width = 150;
                    if ((column.DataType == typeof(double)) || (column.DataType == typeof(int)))
                    {
                        lstview.Columns[column.Ordinal - 1].TextAlign = HorizontalAlignment.Right;
                    }
                }
            }
            for (int i = 0; i <= (DT.Rows.Count - 1); i++)
            {
                lstview.Items.Add(DT.Rows[i].ItemArray.GetValue(1).ToString());
                lstview.Items[i].Tag = DT.Rows[i].ItemArray.GetValue(0).ToString();
                for (int j = 2; j <= (DT.Columns.Count - 1); j++)
                {
                    string str = "";
                    str = DT.Rows[i].ItemArray.GetValue(j).ToString();
                    if (DT.Columns[j].DataType == typeof(double))
                    {
                        str = string.Format("{0:#,##0.00}", Convert.ToDouble(str));
                    }
                    lstview.Items[i].SubItems.Add(str);
                }
            }
        }

        public static void FillTree(DataTable dtTree, TreeView pTrView, string pHeading)
        {
            try
            {
                pTrView.Nodes.Clear();
                pTrView.Nodes.Add(pHeading);
                foreach (DataRow row in dtTree.Rows)
                {
                    TreeNode node = new TreeNode(row[1].ToString());
                    node.Tag = row[0];
                    pTrView.Nodes[0].Nodes.Add(node);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "FillTree");
            }
        }

        public static void FillTree(string paramStrSql, TreeView pTrView, string pHeading)
        {
            try
            {
                GlobalVariables.SDA = new SqlDataAdapter(paramStrSql, GlobalVariables.SqlConn);
                DataTable dataTable = new DataTable();
                GlobalVariables.SDA.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    pTrView.Nodes.Clear();
                    pTrView.Nodes.Add(pHeading);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        TreeNode node = new TreeNode(row[1].ToString());
                        node.Tag = row[0];
                        pTrView.Nodes[0].Nodes.Add(node);
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "FillTree");
            }
        }
    }
}

