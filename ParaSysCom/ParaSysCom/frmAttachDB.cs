﻿using Microsoft.SqlServer.Management.Smo;
using ParControls;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaSysCom
{
    public partial class frmAttachDB : Form
    {
        //private IContainer components = null;
        private Label label1;
        private Label label2;
        private Label label3;
        private OpenFileDialog ofd;
        private ParButton parButton1;
        private ParButton parButton2;
        private ParButton parButton3;
        private ParPanel parPanel1;
        private TextBox txtAttachas;
        private TextBox txtDbfile;
        private TextBox txtserver;

        public frmAttachDB()
        {
            this.InitializeComponent();
        }
        private void parButton1_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void parButton2_Click(object sender, EventArgs e)
        {
            this.ofd.Filter = "(Databese File)|*.mdf";
            this.ofd.ShowDialog();
            this.txtDbfile.Text = this.ofd.FileName;
        }

        private void parButton3_Click(object sender, EventArgs e)
        {
            Server server = new Server("");


            //Server server = new Server(this.txtserver.Text.ToString());
            StringCollection files = new StringCollection();
            files.Add(this.txtDbfile.Text);
            if (server.Databases.Contains(this.txtAttachas.Text) && (MessageBox.Show("Database exist in Server, do you want Detach Old Database and Attach New Database", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes))
            {
                server.KillAllProcesses(this.txtAttachas.Text);
                server.DetachDatabase(this.txtAttachas.Text, true);
            }
            else
            {
                server.AttachDatabase(this.txtAttachas.Text, files, AttachOptions.None);
                MessageBox.Show("Database attached Sucessfully");
                base.Close();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
