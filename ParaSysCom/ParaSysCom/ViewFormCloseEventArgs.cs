﻿namespace ParaSysCom
{
    using System;

    public class ViewFormCloseEventArgs : EventArgs
    {
        private string FormTagstr;

        public ViewFormCloseEventArgs(string sFormTag)
        {
            this.FormTagstr = sFormTag;
        }

        public string FormTag
        {
            get
            {
                return this.FormTagstr;
            }
        }
    }
}

