﻿using ParControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaSysCom
{
    public partial  class ParButton : Button
    {
        private bool ButtonPressed = false;
        private bool ButtonSelected = false;
        private RibbonProfesionalRendererColorTable ColorTable;
     // private IContainer components = null;

        public ParButton()
        {
            this.InitializeComponent();
            base.FlatStyle = FlatStyle.Flat;
            base.FlatAppearance.BorderSize = 0;
            this.ColorTable = new RibbonProfesionalRendererColorTable();
            base.MouseEnter += new EventHandler(this.parbutton_MouseEnter);
            base.MouseLeave += new EventHandler(this.parbutton_MouseLeave);
        }

        private void button1_Paint(object sender, PaintEventArgs e)
        {
            Rectangle rect = new Rectangle(0, 0, base.Width - 1, base.Height - 1);
            e.Graphics.DrawRectangle(new Pen(this.BackColor), rect);
        }
    
        public void DrawButton(Graphics g, Rectangle bounds, Corners corners)
        {
            if ((bounds.Height > 0) && (bounds.Width > 0))
            {
                Rectangle r = Rectangle.FromLTRB(bounds.Left, bounds.Top, bounds.Right - 1, bounds.Bottom - 1);
                Rectangle rectangle2 = Rectangle.FromLTRB(bounds.Left + 1, bounds.Top + 1, bounds.Right - 2, bounds.Bottom - 2);
                Rectangle rectangle3 = Rectangle.FromLTRB(bounds.Left + 1, bounds.Top + 1, bounds.Right - 2, bounds.Top + Convert.ToInt32((double)(bounds.Height * 0.36)));
                using (GraphicsPath path = RoundRectangle(r, 3, corners))
                {
                    GraphicsPath path2;
                    Pen pen;
                    using (SolidBrush brush = new SolidBrush(this.ColorTable.ButtonBgOut))
                    {
                        g.FillPath(brush, path);
                    }
                    using (path2 = new GraphicsPath())
                    {
                        path2.AddEllipse(new Rectangle(bounds.Left, bounds.Top, bounds.Width, bounds.Height * 2));
                        path2.CloseFigure();
                        using (PathGradientBrush brush2 = new PathGradientBrush(path2))
                        {
                            brush2.WrapMode = WrapMode.Clamp;
                            brush2.CenterPoint = new PointF(Convert.ToSingle((int)(bounds.Left + (bounds.Width / 2))), Convert.ToSingle(bounds.Bottom));
                            brush2.CenterColor = this.ColorTable.ButtonBgCenter;
                            brush2.SurroundColors = new Color[] { this.ColorTable.ButtonBgOut };
                            Blend blend = new Blend(3);
                            float[] numArray = new float[3];
                            numArray[1] = 0.8f;
                            blend.Factors = numArray;
                            numArray = new float[3];
                            numArray[1] = 0.3f;
                            numArray[2] = 1f;
                            blend.Positions = numArray;
                            Region clip = g.Clip;
                            Region region2 = new Region(path);
                            region2.Intersect(clip);
                            g.SetClip(region2.GetBounds(g));
                            g.FillPath(brush2, path2);
                            g.Clip = clip;
                        }
                    }
                    using (pen = new Pen(this.ColorTable.ButtonBorderOut))
                    {
                        g.DrawPath(pen, path);
                    }
                    using (path2 = RoundRectangle(rectangle2, 3, corners))
                    {
                        using (pen = new Pen(this.ColorTable.ButtonBorderIn))
                        {
                            g.DrawPath(pen, path2);
                        }
                    }
                    using (path2 = RoundRectangle(rectangle3, 3, (corners & Corners.NorthWest) | (corners & Corners.NorthEast)))
                    {
                        if ((rectangle3.Width > 0) && (rectangle3.Height > 0))
                        {
                            LinearGradientBrush brush3 = new LinearGradientBrush(rectangle3, this.ColorTable.ButtonGlossyNorth, this.ColorTable.ButtonGlossySouth, 90f);
                            brush3.WrapMode = WrapMode.TileFlipXY;
                            g.FillPath(brush3, path2);
                        }
                    }
                    StringFormat format = new StringFormat();
                    format.Alignment = StringAlignment.Center;
                    format.Trimming = StringTrimming.EllipsisCharacter;
                    format.LineAlignment = StringAlignment.Center;
                    format.FormatFlags |= StringFormatFlags.NoWrap;
                    using (Brush brush4 = new SolidBrush(this.ColorTable.TabActiveText))
                    {
                        g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                        if (base.Enabled)
                        {
                            g.DrawString(this.Text.Replace('&', ' ').Trim().ToString(), new Font("Segoe UI", 9f), brush4, bounds, format);
                        }
                        else
                        {
                            g.DrawString(this.Text.Replace('&', ' ').Trim().ToString(), new Font("Segoe UI", 9f), new SolidBrush(this.ColorTable.ArrowDisabled), bounds, format);
                        }
                    }
                    if (base.Image != null)
                    {
                        Point point = new Point(4, 4);
                        g.DrawImageUnscaled(base.Image, point);
                    }
                }
            }
        }

        public void DrawButtonPressed(Graphics g, Rectangle bounds, Corners corners)
        {
            Rectangle r = Rectangle.FromLTRB(bounds.Left, bounds.Top, bounds.Right - 1, bounds.Bottom - 1);
            Rectangle rectangle2 = Rectangle.FromLTRB(bounds.Left + 1, bounds.Top + 1, bounds.Right - 2, bounds.Bottom - 2);
            Rectangle rectangle3 = Rectangle.FromLTRB(bounds.Left + 1, bounds.Top + 1, bounds.Right - 2, bounds.Top + Convert.ToInt32((double)(bounds.Height * 0.36)));
            using (GraphicsPath path = RoundRectangle(r, 3, corners))
            {
                GraphicsPath path2;
                Pen pen;
                using (SolidBrush brush = new SolidBrush(this.ColorTable.ButtonPressedBgOut))
                {
                    g.FillPath(brush, path);
                }
                using (path2 = new GraphicsPath())
                {
                    path2.AddEllipse(new Rectangle(bounds.Left, bounds.Top, bounds.Width, bounds.Height * 2));
                    path2.CloseFigure();
                    using (PathGradientBrush brush2 = new PathGradientBrush(path2))
                    {
                        brush2.WrapMode = WrapMode.Clamp;
                        brush2.CenterPoint = new PointF(Convert.ToSingle((int)(bounds.Left + (bounds.Width / 2))), Convert.ToSingle(bounds.Bottom));
                        brush2.CenterColor = this.ColorTable.ButtonPressedBgCenter;
                        brush2.SurroundColors = new Color[] { this.ColorTable.ButtonPressedBgOut };
                        Blend blend = new Blend(3);
                        float[] numArray = new float[3];
                        numArray[1] = 0.8f;
                        blend.Factors = numArray;
                        numArray = new float[3];
                        numArray[1] = 0.3f;
                        numArray[2] = 1f;
                        blend.Positions = numArray;
                        Region clip = g.Clip;
                        Region region2 = new Region(path);
                        region2.Intersect(clip);
                        g.SetClip(region2.GetBounds(g));
                        g.FillPath(brush2, path2);
                        g.Clip = clip;
                    }
                }
                using (pen = new Pen(this.ColorTable.ButtonPressedBorderOut))
                {
                    g.DrawPath(pen, path);
                }
                using (path2 = RoundRectangle(rectangle2, 3, corners))
                {
                    using (pen = new Pen(this.ColorTable.ButtonPressedBorderIn))
                    {
                        g.DrawPath(pen, path2);
                    }
                }
                using (path2 = RoundRectangle(rectangle3, 3, (corners & Corners.NorthWest) | (corners & Corners.NorthEast)))
                {
                    using (LinearGradientBrush brush3 = new LinearGradientBrush(rectangle3, this.ColorTable.ButtonPressedGlossyNorth, this.ColorTable.ButtonPressedGlossySouth, 90f))
                    {
                        brush3.WrapMode = WrapMode.TileFlipXY;
                        g.FillPath(brush3, path2);
                    }
                }
                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.Trimming = StringTrimming.EllipsisCharacter;
                format.LineAlignment = StringAlignment.Center;
                format.FormatFlags |= StringFormatFlags.NoWrap;
                using (Brush brush4 = new SolidBrush(this.ColorTable.TabActiveText))
                {
                    g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                    g.DrawString(this.Text.Replace('&', ' ').Trim().ToString(), new Font("Segoe UI", 9f), brush4, bounds, format);
                }
                if (base.Image != null)
                {
                    Point point = new Point(4, 4);
                    g.DrawImageUnscaled(base.Image, point);
                }
            }
        }

        public void DrawButtonSelected(Graphics g, Rectangle bounds, Corners corners)
        {
            Rectangle r = Rectangle.FromLTRB(bounds.Left, bounds.Top, bounds.Right - 1, bounds.Bottom - 1);
            Rectangle rectangle2 = Rectangle.FromLTRB(bounds.Left + 1, bounds.Top + 1, bounds.Right - 2, bounds.Bottom - 2);
            Rectangle rectangle3 = Rectangle.FromLTRB(bounds.Left + 1, bounds.Top + 1, bounds.Right - 2, bounds.Top + Convert.ToInt32((double)(bounds.Height * 0.36)));
            using (GraphicsPath path = RoundRectangle(r, 3, corners))
            {
                GraphicsPath path2;
                Pen pen;
                using (SolidBrush brush = new SolidBrush(this.ColorTable.ButtonSelectedBgOut))
                {
                    g.FillPath(brush, path);
                }
                using (path2 = new GraphicsPath())
                {
                    path2.AddEllipse(new Rectangle(bounds.Left, bounds.Top, bounds.Width, bounds.Height * 2));
                    path2.CloseFigure();
                    using (PathGradientBrush brush2 = new PathGradientBrush(path2))
                    {
                        brush2.WrapMode = WrapMode.Clamp;
                        brush2.CenterPoint = new PointF(Convert.ToSingle((int)(bounds.Left + (bounds.Width / 2))), Convert.ToSingle(bounds.Bottom));
                        brush2.CenterColor = this.ColorTable.ButtonSelectedBgCenter;
                        brush2.SurroundColors = new Color[] { this.ColorTable.ButtonSelectedBgOut };
                        Blend blend = new Blend(3);
                        float[] numArray = new float[3];
                        numArray[1] = 0.8f;
                        blend.Factors = numArray;
                        numArray = new float[3];
                        numArray[1] = 0.3f;
                        numArray[2] = 1f;
                        blend.Positions = numArray;
                        Region clip = g.Clip;
                        Region region2 = new Region(path);
                        region2.Intersect(clip);
                        g.SetClip(region2.GetBounds(g));
                        g.FillPath(brush2, path2);
                        g.Clip = clip;
                    }
                }
                using (pen = new Pen(this.ColorTable.ButtonSelectedBorderOut))
                {
                    g.DrawPath(pen, path);
                }
                using (path2 = RoundRectangle(rectangle2, 3, corners))
                {
                    using (pen = new Pen(this.ColorTable.ButtonSelectedBorderIn))
                    {
                        g.DrawPath(pen, path2);
                    }
                }
                using (path2 = RoundRectangle(rectangle3, 3, (corners & Corners.NorthWest) | (corners & Corners.NorthEast)))
                {
                    using (LinearGradientBrush brush3 = new LinearGradientBrush(rectangle3, this.ColorTable.ButtonSelectedGlossyNorth, this.ColorTable.ButtonSelectedGlossySouth, 90f))
                    {
                        brush3.WrapMode = WrapMode.TileFlipXY;
                        g.FillPath(brush3, path2);
                    }
                }
                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.Trimming = StringTrimming.EllipsisCharacter;
                format.LineAlignment = StringAlignment.Center;
                format.FormatFlags |= StringFormatFlags.NoWrap;
                using (Brush brush4 = new SolidBrush(this.ColorTable.TabActiveText))
                {
                    g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                    g.DrawString(this.Text.Replace('&', ' ').Trim().ToString(), new Font("Segoe UI", 9f), brush4, bounds, format);
                }
                if (base.Image != null)
                {
                    Point point = new Point(4, 4);
                    g.DrawImageUnscaled(base.Image, point);
                }
            }
        }
        protected override void InitLayout()
        {
            base.InitLayout();
        }

        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);
            this.ButtonPressed = true;
        }

        protected override void OnPaint(PaintEventArgs pevent)
        {
            base.OnPaint(pevent);
            if (base.DesignMode && !GlobalVariables.CodeLicensed)
            {
                Application.Exit();
            }
            if (this.ButtonSelected)
            {
                this.DrawButtonSelected(pevent.Graphics, pevent.ClipRectangle, Corners.None);
            }
            else
            {
                this.DrawButton(pevent.Graphics, pevent.ClipRectangle, Corners.None);
            }
            Rectangle rect = new Rectangle(0, 0, base.Width - 1, base.Height - 1);
            pevent.Graphics.DrawRectangle(new Pen(this.BackColor), rect);
        }

        private void parbutton_MouseEnter(object sender, EventArgs e)
        {
            this.ButtonSelected = true;
        }

        private void parbutton_MouseLeave(object sender, EventArgs e)
        {
            this.ButtonSelected = false;
        }

        public static GraphicsPath RoundRectangle(Rectangle r, int radius, Corners corners)
        {
            GraphicsPath path = new GraphicsPath();
            int num = radius * 2;
            int num2 = ((corners & Corners.NorthWest) == Corners.NorthWest) ? num : 0;
            int num3 = ((corners & Corners.NorthEast) == Corners.NorthEast) ? num : 0;
            int num4 = ((corners & Corners.SouthEast) == Corners.SouthEast) ? num : 0;
            int num5 = ((corners & Corners.SouthWest) == Corners.SouthWest) ? num : 0;
            path.AddLine(r.Left + num2, r.Top, r.Right - num3, r.Top);
            if (num3 > 0)
            {
                path.AddArc(Rectangle.FromLTRB(r.Right - num3, r.Top, r.Right, r.Top + num3), -90f, 90f);
            }
            path.AddLine(r.Right, r.Top + num3, r.Right, r.Bottom - num4);
            if (num4 > 0)
            {
                path.AddArc(Rectangle.FromLTRB(r.Right - num4, r.Bottom - num4, r.Right, r.Bottom), 0f, 90f);
            }
            path.AddLine(r.Right - num4, r.Bottom, r.Left + num5, r.Bottom);
            if (num5 > 0)
            {
                path.AddArc(Rectangle.FromLTRB(r.Left, r.Bottom - num5, r.Left + num5, r.Bottom), 90f, 90f);
            }
            path.AddLine(r.Left, r.Bottom - num5, r.Left, r.Top + num2);
            if (num2 > 0)
            {
                path.AddArc(Rectangle.FromLTRB(r.Left, r.Top, r.Left + num2, r.Top + num2), 180f, 90f);
            }
            path.CloseFigure();
            return path;
        }

        public enum Corners
        {
            All = 30,
            East = 12,
            None = 0,
            North = 6,
            NorthEast = 4,
            NorthWest = 2,
            South = 0x18,
            SouthEast = 8,
            SouthWest = 0x10,
            West = 0x12
        }
    }
}
