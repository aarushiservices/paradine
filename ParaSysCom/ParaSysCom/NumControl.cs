﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System;
namespace ParaSysCom
{
   public partial class NumControl : TextBox
    {
        //private IContainer components = null;
        private bool mDecimalRequired;
        private bool mSymbolRequired;
        private decimal mValue;

        public NumControl()
        {
          InitializeComponent();
            this.Value = 0M;
        }         
        private void NumControl_Click(object sender, EventArgs e)
        {
        }

        private void NumControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.NumControl_Validating(sender, new CancelEventArgs());
            }
        }

        private void NumControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!this.DecimalRequired && (e.KeyChar == '.'))
            {
                e.Handled = true;
            }
            if (((!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar)) && (e.KeyChar != '.')) && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == '.') && this.Text.Contains("."))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == '.') && (this.Text.Length < 1))
            {
                e.Handled = true;
            }
        }

        private void NumControl_TextChanged(object sender, EventArgs e)
        {
        }

        private void NumControl_Validated(object sender, EventArgs e)
        {
        }

        private void NumControl_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                string text = this.Text;
                if (text == "")
                {
                    if (this.DecimalRequired)
                    {
                        text = "0.00";
                    }
                    else
                    {
                        text = "0";
                    }
                }
                if (text.Contains("`"))
                {
                    text = text.Remove(text.IndexOf('`'), 1);
                }
                decimal num = Convert.ToDecimal(text);
                this.Value = num;
                if (this.SymbolRequired)
                {
                    if (this.DecimalRequired)
                    {
                        string introduced4 = num.ToString("`");
                        this.Text = introduced4 + num.ToString("0.00");
                    }
                    else
                    {
                        string introduced5 = num.ToString("`");
                        this.Text = introduced5 + num.ToString("0");
                    }
                }
                else if (this.DecimalRequired)
                {
                    this.Text = num.ToString("0.00");
                }
                else
                {
                    this.Text = num.ToString("0");
                }
            }
            catch (Exception)
            {
            }
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
            if (base.DesignMode && !GlobalVariables.CodeLicensed)
            {
                Application.Exit();
            }
        }

        private void showValue()
        {
            decimal num = Convert.ToDecimal(this.mValue);
            if (this.SymbolRequired)
            {
                if (this.DecimalRequired)
                {
                    string introduced2 = num.ToString("`");
                    this.Text = introduced2 + num.ToString("0.00");
                }
                else
                {
                    string introduced3 = num.ToString("`");
                    this.Text = introduced3 + num.ToString("0");
                }
            }
            else if (this.DecimalRequired)
            {
                this.Text = num.ToString("0.00");
            }
            else
            {
                this.Text = num.ToString("0");
            }
        }

        public bool DecimalRequired
        {
            get
            {
                return this.mDecimalRequired;
            }
            set
            {
                this.mDecimalRequired = value;
                this.showValue();
            }
        }

        public bool SymbolRequired
        {
            get
            {
                return this.mSymbolRequired;
            }
            set
            {
                this.mSymbolRequired = value;
                this.showValue();
            }
        }

        public decimal Value
        {
            get
            {
                return this.mValue;
            }
            set
            {
                this.mValue = value;
                this.showValue();
            }
        }
    }
}
