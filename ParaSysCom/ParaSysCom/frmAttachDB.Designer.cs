﻿using ParControls;
using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaSysCom
{
    partial class frmAttachDB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.parPanel1 = new ParPanel();
            this.txtserver = new TextBox();
            this.label3 = new Label();
            this.txtAttachas = new TextBox();
            this.txtDbfile = new TextBox();
            this.parButton2 = new ParButton();
            this.parButton1 = new ParButton();
            this.parButton3 = new ParButton();
            this.label2 = new Label();
            this.label1 = new Label();
            this.ofd = new OpenFileDialog();
            this.parPanel1.SuspendLayout();
            base.SuspendLayout();
            this.parPanel1.BorderColor = Color.Gray;
            this.parPanel1.Controls.Add(this.txtserver);
            this.parPanel1.Controls.Add(this.label3);
            this.parPanel1.Controls.Add(this.txtAttachas);
            this.parPanel1.Controls.Add(this.txtDbfile);
            this.parPanel1.Controls.Add(this.parButton2);
            this.parPanel1.Controls.Add(this.parButton1);
            this.parPanel1.Controls.Add(this.parButton3);
            this.parPanel1.Controls.Add(this.label2);
            this.parPanel1.Controls.Add(this.label1);
            this.parPanel1.Dock = DockStyle.Fill;
            this.parPanel1.GradientEndColor = Color.FromArgb(0xbf, 0xdb, 0xff);
            this.parPanel1.GradientStartColor = Color.White;
            this.parPanel1.HeaderAlignment = StringAlignment.Near;
            this.parPanel1.HeaderText = "Attach Database";
            this.parPanel1.HeaderTextFontSize = 10;
            this.parPanel1.Image = null;
            this.parPanel1.ImageLocation = new Point(4, 4);
            this.parPanel1.Location = new Point(0, 0);
            this.parPanel1.Name = "parPanel1";
            this.parPanel1.RoundCornerRadius = 2;
            this.parPanel1.ShadowOffSet = 0;
            this.parPanel1.Size = new Size(0x199, 0xb8);
            this.parPanel1.TabIndex = 1;
            this.txtserver.Location = new Point(0x59, 0x35);
            this.txtserver.Name = "txtserver";
            this.txtserver.Size = new Size(220, 20);
            this.txtserver.TabIndex = 0x16;
            this.label3.AutoSize = true;
            this.label3.BackColor = Color.Transparent;
            this.label3.Location = new Point(0x1b, 0x77);
            this.label3.Name = "label3";
            this.label3.Size = new Size(0x34, 13);
            this.label3.TabIndex = 0x15;
            this.label3.Text = "Attach as";
            this.txtAttachas.Location = new Point(0x59, 0x74);
            this.txtAttachas.Name = "txtAttachas";
            this.txtAttachas.Size = new Size(0x6a, 20);
            this.txtAttachas.TabIndex = 20;
            this.txtDbfile.Location = new Point(0x59, 0x55);
            this.txtDbfile.Name = "txtDbfile";
            this.txtDbfile.Size = new Size(220, 20);
            this.txtDbfile.TabIndex = 0x13;
            this.parButton2.BackColor = Color.Teal;
            this.parButton2.BackgroundImageLayout = ImageLayout.Stretch;
            this.parButton2.FlatAppearance.BorderSize = 0;
            this.parButton2.FlatStyle = FlatStyle.Flat;
            this.parButton2.Location = new Point(0x13b, 0x53);
            this.parButton2.Name = "parButton2";
            this.parButton2.Size = new Size(0x41, 0x19);
            this.parButton2.TabIndex = 0x12;
            this.parButton2.Text = "Browse";
            this.parButton2.UseVisualStyleBackColor = true;
            this.parButton2.Click += new EventHandler(this.parButton2_Click);
            this.parButton1.BackColor = Color.Teal;
            this.parButton1.BackgroundImageLayout = ImageLayout.Stretch;
            this.parButton1.FlatAppearance.BorderSize = 0;
            this.parButton1.FlatStyle = FlatStyle.Flat;
            this.parButton1.Location = new Point(0xf1, 0x8f);
            this.parButton1.Name = "parButton1";
            this.parButton1.Size = new Size(0x44, 0x1d);
            this.parButton1.TabIndex = 0x11;
            this.parButton1.Text = "Close";
            this.parButton1.UseVisualStyleBackColor = true;
            this.parButton1.Click += new EventHandler(this.parButton1_Click);
            this.parButton3.BackColor = Color.Teal;
            this.parButton3.BackgroundImageLayout = ImageLayout.Stretch;
            this.parButton3.FlatAppearance.BorderSize = 0;
            this.parButton3.FlatStyle = FlatStyle.Flat;
            this.parButton3.Location = new Point(0x13b, 0x8f);
            this.parButton3.Name = "parButton3";
            this.parButton3.Size = new Size(0x44, 0x1d);
            this.parButton3.TabIndex = 0x10;
            this.parButton3.Text = "Attach";
            this.parButton3.UseVisualStyleBackColor = true;
            this.parButton3.Click += new EventHandler(this.parButton3_Click);
            this.label2.AutoSize = true;
            this.label2.BackColor = Color.Transparent;
            this.label2.Location = new Point(12, 0x59);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0x48, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Database File";
            this.label1.AutoSize = true;
            this.label1.BackColor = Color.Transparent;
            this.label1.Location = new Point(0x24, 0x38);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Server ";
            this.ofd.FileName = "openFileDialog1";
            base.AutoScaleDimensions = new SizeF(6f, 13f);
//            base.AutoScaleMode = AutoScaleMode.Font;
            this.BackColor = Color.White;
            base.ClientSize = new Size(0x199, 0xb8);
            base.Controls.Add(this.parPanel1);
//            base.FormBorderStyle = FormBorderStyle.None;
            base.Name = "frmAttachDB";
            base.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "frmAttachDB";
            this.parPanel1.ResumeLayout(false);
            this.parPanel1.PerformLayout();
            base.ResumeLayout(false);
        }

        #endregion
    }
}