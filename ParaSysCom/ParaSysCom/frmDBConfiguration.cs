﻿using ParControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaSysCom
{
    public partial class frmDBConfiguration : Form
    {
        //private IContainer components = null;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private ParButton parButton1;
        private ParButton parButton2;
        private ParButton parButton3;
        private ParPanel parPanel1;
        private ParPanel parPanel2;
        private RadioButton rdsqlA;
        private RadioButton rdWa;
        private TextBox txtdbname;
        private TextBox txtpwd;
        private TextBox txtserver;
        private TextBox txtun;

        public frmDBConfiguration()
        {
            this.InitializeComponent();
        }

        private void cmbEdition_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void frmDBConfiguration_Load(object sender, EventArgs e)
        {
        }

        

        private void parButton1_Click(object sender, EventArgs e)
        {
            string data = "";
            if (this.rdWa.Checked)
            {
                data = "data source = " + this.txtserver.Text.ToString() + " ; initial catalog = " + this.txtdbname.Text.ToString() + ";Integrated Security=True;; Trusted_Connection=true;Connect Timeout=1000000;";
                File.WriteAllText(Application.StartupPath + @"\DBPath", GlobalFunctions.Encode(data));
            }
            else
            {
                data = "data source = " + this.txtserver.Text.ToString() + " ; initial catalog = " + this.txtdbname.Text.ToString() + "; User Id = " + this.txtun.Text + "; pwd = " + this.txtpwd.Text + "; Trusted_Connection=true;Connect Timeout=1000000;";
                File.WriteAllText(Application.StartupPath + @"\DBPath", GlobalFunctions.Encode(data));
            }
            base.Close();
        }

        private void parButton2_Click(object sender, EventArgs e)
        {
        }

        private void parButton2_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void parButton3_Click(object sender, EventArgs e)
        {
            new frmAttachDB().ShowDialog();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (this.SQLServerSelected())
            {
                SqlConnection connection;
                DataTable schema;
                if (this.rdsqlA.Checked)
                {
                    if (this.UserDetailsEntered())
                    {
                        connection = new SqlConnection("Data Source=" + this.txtserver.Text + "; User Id = " + this.txtun.Text + "; pwd = " + this.txtpwd.Text + ";");
                        connection.Open();
                        schema = connection.GetSchema("Databases");
                        connection.Close();
                    }
                    else
                    {
                        MessageBox.Show("A Username/Password Must Be Entered To View Database Information");
                    }
                }
                else
                {
                    connection = new SqlConnection("Data Source=" + this.txtserver.Text.ToString() + "; Integrated Security=True;");
                    connection.Open();
                    schema = connection.GetSchema("Databases");
                    connection.Close();
                }
            }
            else
            {
                MessageBox.Show("A SQL Server Nnstance Must Be Selected To View Database Information");
            }
        }

        private void rdsqlA_CheckedChanged(object sender, EventArgs e)
        {
            this.txtun.Enabled = this.rdsqlA.Checked;
            this.txtpwd.Enabled = this.rdsqlA.Checked;
        }

        private void rdWa_CheckedChanged(object sender, EventArgs e)
        {
            this.txtun.Enabled = this.rdsqlA.Checked;
            this.txtpwd.Enabled = this.rdsqlA.Checked;
        }

        private bool SQLServerSelected()
        {
            if (this.txtserver.Text == "")
            {
                return false;
            }
            return true;
        }

        private bool UserDetailsEntered()
        {
            return ((this.txtun.Text != "") && (this.txtpwd.Text != ""));
        }
    }
}
