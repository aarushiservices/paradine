﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParaSysCom
{
    public partial class ParHeaderPanel : Panel
    {
       // private IContainer components = null;

        public ParHeaderPanel()
        {
            this.InitializeComponent();
        }

        protected override void InitLayout()
        {
            base.InitLayout();
        }

        protected override void OnMouseEnter(EventArgs e)
        {
        }

        protected override void OnMouseLeave(EventArgs e)
        {
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Rectangle rect = new Rectangle(0, 0, base.Width - 1, base.Height - 1);
            e.Graphics.DrawRectangle(new Pen(this.BackColor), rect);
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            format.Trimming = StringTrimming.EllipsisCharacter;
            format.LineAlignment = StringAlignment.Center;
            format.FormatFlags |= StringFormatFlags.NoWrap;
            Rectangle clipRectangle = e.ClipRectangle;
            using (Brush brush = new SolidBrush(Color.FromArgb(0xff, 0x15, 0x42, 0x8b)))
            {
                e.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                e.Graphics.DrawString((base.Tag == null) ? " " : base.Tag.ToString(), new Font("Segoe UI", 10f, FontStyle.Bold), brush, clipRectangle, format);
            }
        }
    }
}
