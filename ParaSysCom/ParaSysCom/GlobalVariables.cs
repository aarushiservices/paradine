﻿namespace ParaSysCom
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Drawing;
    using System.Globalization;

    public class GlobalVariables
    {
        public static bool AllowAdd;
        public static bool AllowDelete;
        public static bool AllowModify;
        public static string BalanceTemplate = "";
        public static Color BtnTextColor = Color.Black;
        public static DateTime BussDate = DateTime.Now;
        public static bool CodeLicensed = CodeSecurity.VerifyLicense();
        public static string Comp_Address;
        public static string Comp_Name;
        public static Color Compulsory_Color = Color.Ivory;
        public static string ConnStr = "";
        public const string CounterTransEntName = "CounterTrans";
        public static long CurrCustomerID;
        public static string CurrGroup;
        public static long CurrReceiptID;
        public static long CurrVoucherID;
        public static string DBaseName = "";
        public static DataSet DS = new DataSet();
        public static DataTable DT = new DataTable();
        public static Image[] EmbededImageCollection = new Image[100];
        public static DataTable ErrorDt = new DataTable();
        public static string GlbBackColor = "WhiteSmoke";
        public static string glbBank;
        public static DateTime glbChequeDate = BusinessDate;
        public static string glbChequeNo;
        public static string GlbFrontColor = "204,204,204";
        public static Color GridHeaderBackColor;
        public static Color GridHeaderForeColor;
        public static Color GridRowBackColor;
        public static Color GridRowForeColor;
        public static Color Header_BackColor = Color.LightCoral;
        public static Color Header_ForeColor = Color.Black;
        public const string HintEntName = "Hint";
        public static bool IsLicensed = false;
        public static bool IsUnderAMC = false;
        public static Color LabelHeadColor = Color.White;
        public const string LevelEntName = "Level";
        public static Color MainThemeColor = FromHex("#BFDBFF");
        public static short PackVolume = 10;
        public const string PaymentModeEntName = "PaymentMode";
        public const string PermissionEntName = "Permission";
        public static Point PointClicked;
        public static string PRINTERNAME = "";
        public static string ReceiptSeries;
        public static SqlDataAdapter SDA;
        public static SqlDataReader SDR;
        public static string SenderID = "";
        public static string ServerName = @"IBMSERVER\IBMSERVER2005";
        public static string SmsPassWord = "";
        public static string SMSTemplate = "";
        public static string SMSUserName = "";
        public static SqlCommand SqlCmd;
        public static SqlConnection SqlConn;
        protected static string StrBackEnd = "";
        public static string StrCompName = "";
        public static string SysPwd = "";
        public const string TerminalEntName = "Terminal";
        public static short TerminalNo = 1;
        public static int Theme = 0;
        public static Color theme_color = Color.SeaShell;
        public static Image Theme_img;
        public static Image ThemeFrmBackGroundImage;
        public const string UserEntName = "User";
        public static short UserID;

        internal static Color FromHex(string hex)
        {
            if (hex.StartsWith("#"))
            {
                hex = hex.Substring(1);
            }
            if (hex.Length != 6)
            {
                throw new Exception("Color not valid");
            }
            return Color.FromArgb(int.Parse(hex.Substring(0, 2), NumberStyles.HexNumber), int.Parse(hex.Substring(2, 2), NumberStyles.HexNumber), int.Parse(hex.Substring(4, 2), NumberStyles.HexNumber));
        }

        public static DateTime BusinessDate
        {
            get
            {
                if (DateTime.Now.Date.CompareTo(BussDate.Date) != 0)
                {
                    return new DateTime(BussDate.Year, BussDate.Month, BussDate.Day, 0x17, 0x3b, 0x3b);
                }
                return new DateTime(BussDate.Year, BussDate.Month, BussDate.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            }
            set
            {
                BussDate = value;
            }
        }
    }
}

