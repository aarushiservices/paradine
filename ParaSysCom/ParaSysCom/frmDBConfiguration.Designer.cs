﻿using ParControls;
using System;
using System.Drawing;
using System.Windows.Forms;
namespace ParaSysCom
{
    partial class frmDBConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.parPanel1 = new ParPanel();
            this.parPanel2 = new ParPanel();
            this.rdsqlA = new RadioButton();
            this.label1 = new Label();
            this.rdWa = new RadioButton();
            this.txtserver = new TextBox();
            this.txtdbname = new TextBox();
            this.label2 = new Label();
            this.txtun = new TextBox();
            this.label4 = new Label();
            this.txtpwd = new TextBox();
            this.label3 = new Label();
            this.parButton2 = new ParButton();
            this.parButton3 = new ParButton();
            this.parButton1 = new ParButton();
            this.parPanel1.SuspendLayout();
            this.parPanel2.SuspendLayout();
            base.SuspendLayout();
            this.parPanel1.BorderColor = Color.Gray;
            this.parPanel1.Controls.Add(this.parPanel2);
            this.parPanel1.Controls.Add(this.parButton2);
            this.parPanel1.Controls.Add(this.parButton3);
            this.parPanel1.Controls.Add(this.parButton1);
            this.parPanel1.Dock = DockStyle.Fill;
            this.parPanel1.GradientEndColor = Color.FromArgb(0xbf, 0xdb, 0xff);
            this.parPanel1.GradientStartColor = Color.White;
            this.parPanel1.HeaderAlignment = StringAlignment.Near;
            this.parPanel1.HeaderText = "Database Configuration";
            this.parPanel1.HeaderTextFontSize = 10;
            this.parPanel1.Image = null;
            this.parPanel1.ImageLocation = new Point(4, 4);
            this.parPanel1.Location = new Point(0, 0);
            this.parPanel1.Name = "parPanel1";
            this.parPanel1.RoundCornerRadius = 2;
            this.parPanel1.ShadowOffSet = 0;
            this.parPanel1.Size = new Size(0x1e3, 310);
            this.parPanel1.TabIndex = 0;
            this.parPanel2.BorderColor = Color.Gray;
            this.parPanel2.Controls.Add(this.rdsqlA);
            this.parPanel2.Controls.Add(this.label1);
            this.parPanel2.Controls.Add(this.rdWa);
            this.parPanel2.Controls.Add(this.txtserver);
            this.parPanel2.Controls.Add(this.txtdbname);
            this.parPanel2.Controls.Add(this.label2);
            this.parPanel2.Controls.Add(this.txtun);
            this.parPanel2.Controls.Add(this.label4);
            this.parPanel2.Controls.Add(this.txtpwd);
            this.parPanel2.Controls.Add(this.label3);
            this.parPanel2.GradientEndColor = Color.FromArgb(0xbf, 0xdb, 0xff);
            this.parPanel2.GradientStartColor = Color.White;
            this.parPanel2.HeaderTextFontSize = 10;
            this.parPanel2.Image = null;
            this.parPanel2.ImageLocation = new Point(4, 4);
            this.parPanel2.Location = new Point(0x27, 0x44);
            this.parPanel2.Name = "parPanel2";
            this.parPanel2.RoundCornerRadius = 2;
            this.parPanel2.ShadowOffSet = 0;
            this.parPanel2.Size = new Size(0x194, 0x98);
            this.parPanel2.TabIndex = 0x16;
            this.rdsqlA.AutoSize = true;
            this.rdsqlA.BackColor = Color.Transparent;
            this.rdsqlA.Location = new Point(0xc4, 0x34);
            this.rdsqlA.Name = "rdsqlA";
            this.rdsqlA.Size = new Size(0x97, 0x11);
            this.rdsqlA.TabIndex = 15;
            this.rdsqlA.Text = "SQL Server Authentication";
            this.rdsqlA.UseVisualStyleBackColor = false;
            this.rdsqlA.CheckedChanged += new EventHandler(this.rdsqlA_CheckedChanged);
            this.label1.AutoSize = true;
            this.label1.BackColor = Color.Transparent;
            this.label1.Location = new Point(0x10, 0x12);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Server ";
            this.rdWa.AutoSize = true;
            this.rdWa.BackColor = Color.Transparent;
            this.rdWa.Checked = true;
            this.rdWa.Location = new Point(0x2e, 0x34);
            this.rdWa.Name = "rdWa";
            this.rdWa.Size = new Size(140, 0x11);
            this.rdWa.TabIndex = 14;
            this.rdWa.TabStop = true;
            this.rdWa.Text = "Windows Authentication";
            this.rdWa.UseVisualStyleBackColor = false;
            this.rdWa.CheckedChanged += new EventHandler(this.rdWa_CheckedChanged);
            this.txtserver.Location = new Point(0x41, 14);
            this.txtserver.Name = "txtserver";
            this.txtserver.Size = new Size(0x132, 20);
            this.txtserver.TabIndex = 0x11;
            this.txtdbname.Location = new Point(0x58, 0x77);
            this.txtdbname.Name = "txtdbname";
            this.txtdbname.Size = new Size(300, 20);
            this.txtdbname.TabIndex = 0x12;
            this.label2.AutoSize = true;
            this.label2.BackColor = Color.Transparent;
            this.label2.Location = new Point(0x1d, 0x7e);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0x35, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Database";
            this.txtun.Location = new Point(0x58, 0x57);
            this.txtun.Name = "txtun";
            this.txtun.Size = new Size(100, 20);
            this.txtun.TabIndex = 5;
            this.label4.AutoSize = true;
            this.label4.BackColor = Color.Transparent;
            this.label4.Location = new Point(0xdf, 0x59);
            this.label4.Name = "label4";
            this.label4.Size = new Size(0x35, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Password";
            this.txtpwd.Location = new Point(0x120, 0x56);
            this.txtpwd.Name = "txtpwd";
            this.txtpwd.PasswordChar = '*';
            this.txtpwd.Size = new Size(100, 20);
            this.txtpwd.TabIndex = 6;
            this.label3.AutoSize = true;
            this.label3.BackColor = Color.Transparent;
            this.label3.Location = new Point(0x17, 0x59);
            this.label3.Name = "label3";
            this.label3.Size = new Size(60, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "User Name";
            this.parButton2.BackColor = Color.Teal;
            this.parButton2.BackgroundImageLayout = ImageLayout.Stretch;
            this.parButton2.FlatAppearance.BorderSize = 0;
            this.parButton2.FlatStyle = FlatStyle.Flat;
            this.parButton2.Location = new Point(0x17e, 0x101);
            this.parButton2.Name = "parButton2";
            this.parButton2.Size = new Size(0x4c, 0x1d);
            this.parButton2.TabIndex = 0x15;
            this.parButton2.Text = "Close";
            this.parButton2.UseVisualStyleBackColor = true;
            this.parButton2.Click += new EventHandler(this.parButton2_Click_1);
            this.parButton3.BackColor = Color.Teal;
            this.parButton3.BackgroundImageLayout = ImageLayout.Stretch;
            this.parButton3.FlatAppearance.BorderSize = 0;
            this.parButton3.FlatStyle = FlatStyle.Flat;
            this.parButton3.Location = new Point(0x21, 0x101);
            this.parButton3.Name = "parButton3";
            this.parButton3.Size = new Size(0x72, 0x1d);
            this.parButton3.TabIndex = 0x10;
            this.parButton3.Text = "Attach Database";
            this.parButton3.UseVisualStyleBackColor = true;
            this.parButton3.Click += new EventHandler(this.parButton3_Click);
            this.parButton1.BackColor = Color.Teal;
            this.parButton1.BackgroundImageLayout = ImageLayout.Stretch;
            this.parButton1.FlatAppearance.BorderSize = 0;
            this.parButton1.FlatStyle = FlatStyle.Flat;
            this.parButton1.Location = new Point(300, 0x101);
            this.parButton1.Name = "parButton1";
            this.parButton1.Size = new Size(0x4c, 0x1d);
            this.parButton1.TabIndex = 10;
            this.parButton1.Text = "Apply";
            this.parButton1.UseVisualStyleBackColor = true;
            this.parButton1.Click += new EventHandler(this.parButton1_Click);
            base.AutoScaleDimensions = new SizeF(6f, 13f);
//          base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x1e3, 310);
            base.Controls.Add(this.parPanel1);
//          base.FormBorderStyle = FormBorderStyle.None;
            base.Name = "frmDBConfiguration";
            base.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "frmDBConfiguration";
            base.Load += new EventHandler(this.frmDBConfiguration_Load);
            this.parPanel1.ResumeLayout(false);
            this.parPanel2.ResumeLayout(false);
            this.parPanel2.PerformLayout();
            base.ResumeLayout(false);
        }

        #endregion
    }
}