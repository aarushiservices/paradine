﻿using Microsoft.Office.Interop.Excel;
using System;
using System.ComponentModel;
using System.Windows.Forms;
namespace ParaSysCom
{
    partial class NumControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
       private void InitializeComponent()
        {
            base.SuspendLayout();
           // this.Font = new Font("Rupee Foradian", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            base.TextAlign = HorizontalAlignment.Right;
            base.TextChanged += new EventHandler(this.NumControl_TextChanged);
            base.Validated += new EventHandler(this.NumControl_Validated);
            base.Click += new EventHandler(this.NumControl_Click);
            base.KeyDown += new KeyEventHandler(this.NumControl_KeyDown);
            base.KeyPress += new KeyPressEventHandler(this.NumControl_KeyPress);
            base.Validating += new CancelEventHandler(this.NumControl_Validating);
            base.ResumeLayout(false);
        }

        #endregion
    }
}
