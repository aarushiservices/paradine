﻿//Modified by Bharath
namespace ParaSysCom
{
    using Microsoft.Office.Interop.Excel;
    using System;
    using System.Data;
    using System.Drawing;
    using System.Windows.Forms;

    public class ExcelComponent
    {
        private Color _AlterNativeRowBackColor = Color.White;
        private string _DisplayColumnsList;
        private string _ReportFooter = "";
        private string _ReportHeader = "";
        private string _ReportTitle = "";
        private bool _ReportTitleFontBold = false;
        private int _ReportTitleFontSize = 10;
        private bool _SNoRequired = false;
        private string _WorkBookName;
        private Microsoft.Office.Interop.Excel.Application app = null;
        private string[] ColNames = new string[] { 
            "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", 
            "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
         };
        public System.Data.DataTable dtSource = new System.Data.DataTable();
        private Workbook workbook = null;
        private Worksheet worksheet = null;
        private Range workSheet_range = null;
        private Worksheet get_Cells = null;

        public ExcelComponent()
        {
            this.createDoc();
        }
        public void addData(int row, int col, string data, string cell1, string cell2, string format)
        {
            this.worksheet.Cells[row, col] = data;  //.get_Cells().set__Default(row, col, data);
            this.workSheet_range = this.worksheet.get_Range(cell1, cell2);
            this.workSheet_range.Borders.Color = Color.Black.ToArgb();  //.get_Borders().set_Color(Color.Black.ToArgb());
            this.workSheet_range.NumberFormat = format;
        }
        private string CellName(int No)
        {
            int num2;
            int index = 0;
            index = Math.DivRem(Convert.ToInt32(No), 0x1a, out num2);
            return (this.ColNames[index] + this.ColNames[num2]);
        }
        public void createDoc()
        {
            try
            {
                try
                {
                    //Modified By bharath
                    this.app = new Microsoft.Office.Interop.Excel.Application();
                    this.app.Visible=true; //.set_Visible(true);
                    this.app.SheetsInNewWorkbook = 1;
                    this.workbook = this.app.Workbooks.Add();
                    this.worksheet = (Worksheet)this.workbook.Worksheets.get_Item(1);
                    this.worksheet.Activate();
                }
                catch (Exception)
                {
                    Console.Write("Error");
                }
            }
            finally
            {

            }
        }
        //Modified by Bharath
        public void createHeaders(int row, int col, string htext, string cell1, string cell2, int mergeColumns, string b, bool font, int size, string fcolor)
        {
            this.worksheet.Cells[row, col]= htext;
            this.workSheet_range = this.worksheet.get_Range(cell1, cell2);
            this.workSheet_range.Merge(mergeColumns);
            string str = b;
            if (str != null)
            {
                if (!(str == "YELLOW"))
                {
                    if (str == "GRAY")
                    {
                        this.workSheet_range.Interior.Color = Color.Gray.ToArgb();
                    }
                    else if (str == "GAINSBORO")
                    {
                        this.workSheet_range.Interior.Color = Color.Gainsboro.ToArgb();
                    }
                    else if (str == "Turquoise")
                    {
                        this.workSheet_range.Interior.Color = Color.Turquoise.ToArgb();
                    }
                    else if (str == "PeachPuff")
                    {
                        this.workSheet_range.Interior.Color = Color.PeachPuff.ToArgb();
                    }
                }
                else
                {
                    this.workSheet_range.Interior.Color = Color.Yellow.ToArgb();
                }
            }
            this.workSheet_range.Borders.Color = Color.Black.ToArgb();
            this.workSheet_range.Font.Bold = font;
            this.workSheet_range.ColumnWidth = size;
            if (fcolor.Equals(""))
            {
                this.workSheet_range.Font.Color = Color.White.ToArgb();
            }
            else
            {
                this.workSheet_range.Font.Color=Color.Black.ToArgb();
            }
        }
        //Modified by Bharath
        public void GenerateSheet()
        {            
            short num = 1;
            this.worksheet.Cells[num, 1] = _ReportTitle;
            string str = this.CellName(1) + num.ToString();
            string[] strArray = this._DisplayColumnsList.Split(new char[] { ';' });
            string str2 = this.CellName(strArray.GetLength(0)) + num.ToString();
            this.workSheet_range = this.worksheet.get_Range(str, str2);
            this.workSheet_range.Font.Size=this._ReportTitleFontSize;
            this.workSheet_range.Font.Bold=this._ReportTitleFontBold;
            this.workSheet_range.Interior.Color = Color.Yellow.ToArgb();
            num = (short) (num + 1);
            num = (short) (num + 1);
            int result = 2;
            foreach (string str3 in this._DisplayColumnsList.Split(new char[] { ';' }))
            {
                this.worksheet.Cells[num, result] = str3;
                result++;
            }
            num = (short) (num + 1);
            foreach (DataRow row in this.dtSource.Rows)
            {
                result = 2;
                foreach (string str3 in this._DisplayColumnsList.Split(new char[] { ';' }))
                {
                    this.worksheet.Cells[num, result] = row[this.dtSource.Columns.IndexOf(str3)].ToString();
                    result++;
                }
                str = this.CellName(1) + num.ToString();
                str2 = this.CellName(result - 1) + num.ToString();
                Math.DivRem(Convert.ToInt32(num), 2, out result);
                if (result == 0)
                {
                    this.workSheet_range = this.worksheet.get_Range(str, str2);
                    this.workSheet_range.Interior.Color=this._AlterNativeRowBackColor.ToArgb();
                }
                num = (short) (num + 1);
            }
        }

        public Color AlterNaativeRowBackClor
        {
            get
            {
                return this._AlterNativeRowBackColor;
            }
            set
            {
                this._AlterNativeRowBackColor = value;
            }
        }

        public string DisplayColumsList
        {
            get
            {
                return this._DisplayColumnsList;
            }
            set
            {
                this._DisplayColumnsList = value;
            }
        }

        public string ReportTitle
        {
            get
            {
                return this._ReportTitle;
            }
            set
            {
                this._ReportTitle = value;
            }
        }

        public bool ReportTitleFontBold
        {
            get
            {
                return this._ReportTitleFontBold;
            }
            set
            {
                this._ReportTitleFontBold = value;
            }
        }

        public int ReportTitleFontSize
        {
            get
            {
                return this._ReportTitleFontSize;
            }
            set
            {
                this._ReportTitleFontSize = value;
            }
        }

        public bool SerialNoRequired
        {
            get
            {
                return this._SNoRequired;
            }
            set
            {
                this._SNoRequired = value;
            }
        }

        public string WorkBookName
        {
            get
            {
                return this._WorkBookName;
            }
            set
            {
                this._WorkBookName = value;
            }
        }
    }
}

