﻿namespace ParaSysCom
{
    using System;
    using System.Runtime.InteropServices;

    public class Printer
    {
        [DllImport("winspool.drv", CallingConvention=CallingConvention.StdCall, CharSet=CharSet.Unicode, ExactSpelling=true)]
        public static extern long ClosePrinter(IntPtr hPrinter);
        [DllImport("winspool.drv", CallingConvention=CallingConvention.StdCall, CharSet=CharSet.Unicode, ExactSpelling=true)]
        public static extern long EndDocPrinter(IntPtr hPrinter);
        [DllImport("winspool.drv", CallingConvention=CallingConvention.StdCall, CharSet=CharSet.Unicode, ExactSpelling=true)]
        public static extern long EndPagePrinter(IntPtr hPrinter);
        [DllImport("winspool.drv", CallingConvention=CallingConvention.StdCall, CharSet=CharSet.Unicode)]
        public static extern long OpenPrinter(string pPrinterName, ref IntPtr phPrinter, int pDefault);
        public static void PrintString(string PrnStr)
        {
            IntPtr phPrinter = new IntPtr();
            DOCINFO pDocInfo = new DOCINFO();
            int pcWritten = 0;
            pDocInfo.pDocName = "my test document";
            pDocInfo.pDataType = "RAW";
            OpenPrinter("Generic / Text Only", ref phPrinter, 0);
            StartDocPrinter(phPrinter, 1, ref pDocInfo);
            StartPagePrinter(phPrinter);
            try
            {
                WritePrinter(phPrinter, PrnStr, PrnStr.Length, ref pcWritten);
            }
            catch (Exception)
            {
            }
            EndPagePrinter(phPrinter);
            EndDocPrinter(phPrinter);
            ClosePrinter(phPrinter);
        }

        [DllImport("winspool.drv", CallingConvention=CallingConvention.StdCall, CharSet=CharSet.Unicode)]
        public static extern long StartDocPrinter(IntPtr hPrinter, int Level, ref DOCINFO pDocInfo);
        [DllImport("winspool.drv", CallingConvention=CallingConvention.StdCall, CharSet=CharSet.Unicode, ExactSpelling=true)]
        public static extern long StartPagePrinter(IntPtr hPrinter);
        [DllImport("winspool.drv", CallingConvention=CallingConvention.StdCall, CharSet=CharSet.Ansi, ExactSpelling=true)]
        public static extern long WritePrinter(IntPtr hPrinter, string data, int buf, ref int pcWritten);
    }
}

