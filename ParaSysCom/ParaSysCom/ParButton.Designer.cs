﻿using System.Drawing;
using System.Windows.Forms;
namespace ParaSysCom
{
    partial class ParButton
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            base.SuspendLayout();
            this.BackColor = Color.Teal;
            this.BackgroundImageLayout = ImageLayout.Stretch;
            base.Size = new Size(0x70, 0x1d);
            base.UseVisualStyleBackColor = false;
            base.ResumeLayout(false);
        }

        #endregion
    }
}
