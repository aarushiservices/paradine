﻿namespace ParaSysCom
{
    using System;

    public class ViewFormCallBackEventArgs : EventArgs
    {
        private string ActionTypeStr;
        private string StrQuery;

        public ViewFormCallBackEventArgs(string sStrQuery, string sActionType)
        {
            this.StrQuery = sStrQuery;
            this.ActionTypeStr = sActionType;
        }

        public string ActionType
        {
            get
            {
                return this.ActionTypeStr;
            }
        }

        public string QueryString
        {
            get
            {
                return this.StrQuery;
            }
        }
    }
}

