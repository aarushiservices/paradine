﻿namespace ParaSysCom
{
    using System;
    using System.Drawing;

    public class ViewFormOpenEventArgs : EventArgs
    {
        private string FormTagstr;
        private Image Imgv;

        public ViewFormOpenEventArgs(string sFormTag, Image sImg)
        {
            this.FormTagstr = sFormTag;
            this.Imgv = sImg;
        }

        public string FormTag
        {
            get
            {
                return this.FormTagstr;
            }
        }

        public Image Img
        {
            get
            {
                return this.Imgv;
            }
        }
    }
}

