﻿namespace ParaSysCom
{
    using System;
    using System.Runtime.InteropServices;
    using System.Text;

    public class SQLInfoEnumerator
    {
        private const short DEFAULT_RESULT_SIZE = 0x400;
        private const string END_STR = "}";
        private string m_Password;
        private string m_SQLServer;
        private string m_Username;
        private const int SQL_ATTR_ODBC_VERSION = 200;
        private const string SQL_DRIVER_STR = "DRIVER=SQL SERVER";
        private const short SQL_HANDLE_DBC = 2;
        private const short SQL_HANDLE_ENV = 1;
        private const short SQL_NEED_DATA = 0x63;
        private const int SQL_OV_ODBC3 = 3;
        private const short SQL_SUCCESS = 0;
        private const string START_STR = "{";

        public string[] EnumerateSQLServers()
        {
            return this.RetrieveInformation("DRIVER=SQL SERVER");
        }

        public string[] EnumerateSQLServersDatabases()
        {
            return this.RetrieveInformation("DRIVER=SQL SERVER;SERVER=" + this.m_SQLServer + ";UID=" + this.m_Username + ";PWD=" + this.m_Password);
        }

        private void FreeConnection(IntPtr handleToFree)
        {
            if (handleToFree != IntPtr.Zero)
            {
                SQLFreeHandle(2, handleToFree);
            }
        }

        private string[] ParseSQLOutConnection(string outConnection)
        {
            int startIndex = outConnection.IndexOf("{") + 1;
            int length = outConnection.IndexOf("}") - startIndex;
            if ((startIndex > 0) && (length > 0))
            {
                outConnection = outConnection.Substring(startIndex, length);
            }
            else
            {
                outConnection = string.Empty;
            }
            return outConnection.Split(",".ToCharArray());
        }

        private string[] RetrieveInformation(string InputParam)
        {
            IntPtr zero = IntPtr.Zero;
            IntPtr outputHandlePtr = IntPtr.Zero;
            StringBuilder inConnection = new StringBuilder(InputParam);
            short length = (short) inConnection.Length;
            StringBuilder outConnection = new StringBuilder(0x400);
            short num2 = 0;
            try
            {
                if ((((0 == SQLAllocHandle(1, zero, out zero)) && (0 == SQLSetEnvAttr(zero, 200, (IntPtr) 3, 0))) && ((0 == SQLAllocHandle(2, zero, out outputHandlePtr)) && (0x63 == SQLBrowseConnect(outputHandlePtr, inConnection, length, outConnection, 0x400, out num2)))) && (0x63 != SQLBrowseConnect(outputHandlePtr, inConnection, length, outConnection, 0x400, out num2)))
                {
                    throw new ApplicationException("No Data Returned.");
                }
            }
            catch (Exception)
            {
                throw new ApplicationException("Cannot Locate SQL Server.");
            }
            finally
            {
                this.FreeConnection(outputHandlePtr);
                this.FreeConnection(zero);
            }
            if (outConnection.ToString() != "")
            {
                return this.ParseSQLOutConnection(outConnection.ToString());
            }
            return null;
        }

        [DllImport("odbc32.dll")]
        private static extern short SQLAllocHandle(short handleType, IntPtr inputHandle, out IntPtr outputHandlePtr);
        [DllImport("odbc32.dll", CharSet=CharSet.Ansi)]
        private static extern short SQLBrowseConnect(IntPtr handleConnection, StringBuilder inConnection, short stringLength, StringBuilder outConnection, short bufferLength, out short stringLength2Ptr);
        [DllImport("odbc32.dll")]
        private static extern short SQLFreeHandle(short hType, IntPtr Handle);
        [DllImport("odbc32.dll")]
        private static extern short SQLSetEnvAttr(IntPtr environmentHandle, int attribute, IntPtr valuePtr, int stringLength);

        public string Password
        {
            set
            {
                this.m_Password = value;
            }
        }

        public string SQLServer
        {
            set
            {
                this.m_SQLServer = value;
            }
        }

        public string Username
        {
            set
            {
                this.m_Username = value;
            }
        }
    }
}

